define(["troopjs-browser/component/widget",
	"jquery",
	"template!./index.html",
	"json2"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				$me.find('.icheck').iCheck({
					checkboxClass: 'icheckbox_square',
					radioClass: 'iradio_square',
					increaseArea: '20%'
				});
				// Check default
				$('input[name=invoice-type]:eq(0)').iCheck('check');
			});
		},
		"dom:[data-action=companyinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (!e.currentTarget.checked) {
				return;
			}
			$me.find("#invoice-companyname").focus();
		},
		"dom:[data-action=needinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (e.currentTarget.checked) {
				$me.find(".invoice-sub").show();
			} else {
				$me.find(".invoice-sub").hide();
			}

			// Trigger "height change" event on "auto follower"
			//me.publish("autoFollower/resize");
		},
		"hub/order/invoice/submit": function(orderId) {
			var me = this;

			var $me = me.$element;
			var invoiceType;
			var postData;

			if ($me.find(".icheckbox_square").hasClass("checked")) {
				invoiceType = $me.find(".iradio_square.checked > input").val();
				postData = {
					orderId: orderId,
					invoiceType: invoiceType,
					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
				};
			}

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;

		}
	});
});