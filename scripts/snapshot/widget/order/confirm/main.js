define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";

    var PRICE_DELIVERY = 25;
    var URI_ORDER_CONFIRM = api.get("order_confirm");
    var EXPIRED_CODE = 5;
    var URI_ORDER_PAYMENT = api.get("orderpayment");
    // Transfer to blurb
    var invoiceDict = {
        "1": 1158,
        "2": 1159
    };

    return Widget.extend(function($element, widgetName, step) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.step = step;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            // Load order detail, so ignore order step
            if (me.step === "order") {
                return;
            }
            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }

            var hashMap = troopHash.parse(hashPath);
            var orderId = me.orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Special logic for GiftCard
            var sourceCode = me.sourceCode = hashMap.event || "";

            me.html(template, {
                "orderId": orderId,
                "sourceCode": sourceCode
            });
        },
        "getPayment": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/paymenttype/submit", me.orderId).spread(function(dataPayment) {
                return when.resolve(dataPayment);
            });
        },
        "getInvoice": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/invoice/submit", me.orderId).spread(function(dataInvoice) {
                return when.resolve(dataInvoice);
            });
        },
        "getPromotion": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/promotion/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "getGiftInfo": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/gift/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "dom:[data-action=confirmorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;
            var $el = $(e.currentTarget);

            var postData = {
                "OrderId": me.orderId
            };

            when.join(me.getPayment(), me.getInvoice(), me.getPromotion(), me.getGiftInfo()).then(function(data) {
                postData.Payment = data[0];
                postData.Invoice = data[1];
                postData.Promotion = data[2];
                postData.GiftInfo = data[3];

                _.extend(postData, {
                    "SourceCode": $me.find("#order-sourcecode").val() || ""
                });

                ajaxQuery({
                    url: URI_ORDER_CONFIRM,
                    data: JSON.stringify(postData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                }, $el.parent()).then(function(data) {
                    // Success Confirm Order (save Payment type & Invoice info)
                    //
                    // Redirect by payment type
                    var paymentType = postData.Payment.payment;
                    // URL redirect
                    if (paymentType.toString() === "1") {
                        pageUrl.goto("/payment/topay", "orderid/" + me.orderId);
                    } else {
                        // Show success info
                        pageUrl.goto("/order/success", "orderid/" + me.orderId, {
                            "orderid": me.orderId
                        });
                    }
                });
            });
        }
    });
});