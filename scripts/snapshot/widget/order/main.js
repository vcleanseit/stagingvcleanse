define(["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/cookie/main",
    "template!./index.html",
    "json2"
], function(Widget, $, _, when, context, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, cookie, template) {
    "use strict";

    var URI_ORDER_CREATE = api.get("order_create");

    // Time function
    var INS_TIME = new Time();

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template);
        },
        "publishDeliveryList": function(isDailyUpdate) {
            var me = this;
            // Publish to order confirm, list price list
            // Publish to calendar, render order on calendar            
            me.publish("order/deliveryList", me.deliveryList, isDailyUpdate);
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            // Save delivery start date
            me.deliveryStartDate = deliveryStartDate;

            // Mixin calendar/order, rendering
            // Without orders & package, should be customize
            // Publish to order confirm, list price list
            me.publishDeliveryList();
        },
        // Daily order updated
        "hub/order/dailyUpdate": function(dailyOrder) {
            var me = this;

            var i = 0;
            var iLens = me.deliveryList.length;
            var isNew = true;
            var isDelete = false;

            if (!dailyOrder) {
                return;
            }
            // Delete day which without products
            if (!dailyOrder.PackageRuleId &&
                (!dailyOrder.Products || dailyOrder.Products.length <= 0)) {
                isDelete = true;
            }

            for (i = 0; i < iLens; i++) {
                if (INS_TIME.compareDate(me.deliveryList[i].Date, dailyOrder.Date) === 0) {
                    isNew = false;
                    // Delete day which without products
                    if (isDelete) {
                        me.deliveryList.splice(i, 1);
                        break;
                    } else {
                        me.deliveryList[i] = dailyOrder;
                    }
                }
            }
            // Order on new date
            // & with products
            if (isNew && dailyOrder &&
                (dailyOrder.PackageRuleId || dailyOrder.Products && dailyOrder.Products.length > 0)) {
                me.deliveryList.push(dailyOrder);
            }

            // Publish to order confirm, list price list
            me.publishDeliveryList(true);
        },
        // Submit order
        "hub/order/submit": function() {
            var me = this;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1147
                }).open();
                // Do not submit order
                return;
            }
            // Format Date to string
            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                // Use strDate for cookie
                me.deliveryList[i].Date = INS_TIME.getStrDate(me.deliveryList[i].Date);
            }

            // Set customize order data to cookie
            cookie.set("customize", JSON.stringify(me.deliveryList), {
                path: "/"
            });

            // Next step
            pageUrl.goto("/order/receiver",
                "packageid/" + 0 +
                "/groupid/" + 0 +
                "/deliveryStartDate/" + INS_TIME.getStrDate(me.deliveryList[0].Date, "-"));
        }
    });
});