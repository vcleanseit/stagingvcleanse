define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, number, ajaxQuery, api, pageUrl, template) {
    "use strict";

    return Widget.extend(function($element, widgetName, step) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.step = step;
    }, {
        "render": function(data) {
            var me = this;

            me.html(template, data);
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        "hub:memory/order/deliveryList": function(deliveryList) {
            var me = this;

            // Loop delivery list
            var i = 0;
            var iLens = deliveryList.length;
            var j = 0;
            var jLens;
            var priceProducts = 0;

            var iDeliveryList;
            var iProducts;

            for (i = 0; i < iLens; i++) {
                iDeliveryList = deliveryList[i];

                // Prevent undefined
                iDeliveryList.Products = iDeliveryList.Products || [];
                iDeliveryList.PackageProducts = iDeliveryList.PackageProducts || [];

                iProducts = iDeliveryList.Products.concat(iDeliveryList.PackageProducts);
                jLens = iProducts.length;
                for (j = 0; j < jLens; j++) {
                    priceProducts += iProducts[j].Count * iProducts[j].Price;
                }
            }
            me.render({
                "step": me.step,
                "product": number.getDecimal(priceProducts),
                "total": number.getDecimal(priceProducts)
            });
        }
    });
});