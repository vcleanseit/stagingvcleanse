define(["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/popupbox/main",
	"template!./index.html",
	"template!./banklist.html",
	"json2",
	"underscore"
], function(Widget, $, when, context, ajaxQuery, api, PopupBox, template, templateBanklist) {
	"use strict";

	var URI_PAYMENT = api.get("payment");

	return Widget.extend(function($element, widgetName, sourceCode) {
		var me = this;

		me.sourceCode = sourceCode || "";
		me.deferred = when.defer();
		// order
		// confirm
		// orderdetail
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_PAYMENT,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}).then(function(data) {
				var paymentTypes = data.Values;

				// Rendering
				me.html(template, {
					"paymentType": paymentTypes,
					"sourceCode": me.sourceCode,
					"cacheServer": context.cacheServer,
					"isMobile": context.isMobile
				}).then(function() {
					$me.find('.icheck').iCheck({
						checkboxClass: 'icheckbox_square',
						radioClass: 'iradio_square',
						checkedClass: 'checked',
						increaseArea: '20%'
					});

					// Rendered
					me.deferred.resolver.resolve();
				});
			});
		},
		"hidePayonDelivery": function() {
			var me = this;
			var $me = me.$element;

			// Hide "Cash on Delivery"
			$me.find("[data-action=orderconfirm-payment-2]").remove();
		},
		"hub/order/orderdetail": function(order) {
			var me = this;

			if (!order || !order.DeliverList) {
				return;
			}

			// Waiting for payment rendered
			me.deferred.promise.then(function() {
				_.each(order.DeliverList, function(e) {
					// Without "payment on delivery"
					// -----------------------------
					// 维果清上海商城店 - 3
					// Z&B Fitness Studio - 4
					// 泰生厨房 - 6
					// -----------------------------
					// 上海顺丰标准B - 8
					// 空运 - 9
					// 空运次晨 - 10
					// 空运B - 12
					if ((e.IsSelfPickUp &&
							e.StoreInfo &&
							_.indexOf([3, 4, 6], e.StoreInfo.StoreId) >= 0) ||
						_.indexOf([8, 9, 10, 12], e.ExpressCompanyId) >= 0) {
						me.hidePayonDelivery();
						return;
					}
				});
			});
		},
		"hub/order/paymenttype/submit": function(orderId) {
			var me = this;
			var $me = me.$element;

			var paymentType = $me.find(".iradio_square.checked > input").val();

			var postData = {
				orderId: orderId,
				payment: paymentType,
				language: context.language
			};

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;
		},
		"dom:[data-action=showallbanks]/click": function() {
			var me = this;

			var $templateBanklist = $(templateBanklist({
				"cacheServer": context.cacheServer
			}));
			// Lightbox configuration
			var popupboxBanklist = new PopupBox({
				msg: $templateBanklist,
				closeble: true,
				closeInner: true,
				zIndex: 1050,
				closeButtonList: ['[data-action=close]'],
				closeCallback: function() {}
			});

			popupboxBanklist.open();
		}
	});
});