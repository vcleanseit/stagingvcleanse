define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/pageurl/main",
    "widget/time/main",
    "jquery.ui.datepicker",
    "jquery.ui.datepicker-zh-CN"
], function(Widget, $, context, pageUrl, Time) {
    "use strict";

    // To order from tomorrow, should order before 8:00 am
    var HOUR_AHEAD = 5;
    // Official holiday
    var HOLIDAY = [
		new Date(2016, 3, 1),
		new Date(2016, 3, 2),
		new Date(2016, 3, 3),
		new Date(2016, 3, 4),
    ];

    // Time function
    var INS_TIME = new Time();

    function getAvailableStartDate() {
        var now = new Date();
        var date = INS_TIME.getZeroDate(now);
        var deltaDay = 0;

        var hour = now.getHours();
        var startDate = hour >= HOUR_AHEAD ? INS_TIME.addDay(date, 2) : INS_TIME.addDay(date, 1);

        if (HOLIDAY && HOLIDAY.length > 0) {
            // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
            // 节假日后一天的订单，会放到节假日当天一起配送
            // 所以节假日前一天给厨房生产之后，不能订餐到节假日后一天的订单
            (function() {
                var i = 0;
                var iLens = HOLIDAY.length;
                var holiday;

                for (i = 0; i < iLens; i++) {
                    holiday = INS_TIME.addDay(INS_TIME.getZeroDate(HOLIDAY[i]), 1);
                    if (INS_TIME.compareDate(holiday, startDate) === 0) {
                        startDate = INS_TIME.addDay(startDate, 1);
                        continue;
                    }
                }
            })();
        }

        return startDate;
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Available first date for order
            var dateAvailable = getAvailableStartDate();
            // Initialize start date from today
            me.publish("deliveryStartDate/change", {
                // Date info
                "availableStartDate": dateAvailable,
                "selectedDate": dateAvailable
            });
        }
    });
});