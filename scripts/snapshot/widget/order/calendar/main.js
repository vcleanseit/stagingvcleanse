define(["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "widget/blurb/main",
    "widget/time/main",
    "widget/customize/main",
    "template!./index.html",
    "json2"
], function(Widget, $, _, when, Blurb, Time, Customize, template) {
    "use strict";

    // Manually initialize "Customize", set $el = $body
    (new Customize($(document.body))).start();

    var blurb = new Blurb($(document.body));
    // Time function
    var INS_TIME = new Time();

    var WEEKS_ONE_PAGE = 2;
    /* create an array of days which need to be disabled */
    var DISABLEDDAYS = [
        "2-2-2016",
        "2-3-2016",
        "2-4-2016",
        "2-5-2016",
    ];

    // 不配送日期
    function offDays(date) {
        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();
        var weekday = date.getDay();

        if (DISABLEDDAYS) {
            if ($.inArray((m + 1) + '-' + d + '-' + y, DISABLEDDAYS) != -1 || new Date() > date) {
                return false;
            }
        }

        return true;
    }

    function getOrderCalendar(availableStartDate, baseDate) {
        var weekStartDate = INS_TIME.getWeekStartDate(baseDate);
        var baseDateDay = INS_TIME.getDayOfWeekFromMonday(baseDate);

        return {
            // true: can't prev anymore
            zero: false,
            // 0: base date contained in current two weeks
            page: 0,
            getCurrentWeeks: function() {
                var me = this;
                // Current week start date count from baseDate week
                var i = 0;
                var iLens = WEEKS_ONE_PAGE * 7;
                // Pages
                var delta = me.page * WEEKS_ONE_PAGE * 7;
                var j = 0;
                var twoWeeks = [];
                var date;

                for (i; i < iLens; i++) {
                    date = INS_TIME.addDay(weekStartDate, i + delta);

                    twoWeeks.push({
                        date: date,
                        weekName: INS_TIME.getWeekName(date.getDay(), true),
                        shortDate: INS_TIME.getShortDate(date),
                        isAvailable: INS_TIME.compareDate(availableStartDate, date) >= 0 ? offDays(date) : false,
                        isDeliveryStartDate: INS_TIME.compareDate(baseDate, date) === 0 ? true : false
                    });
                    if (i === 0) {
                        me.zero = INS_TIME.compareDate(availableStartDate, date) <= 0 ? true : false;
                    }
                }
                return twoWeeks;
            },
            currentWeeks: function() {
                var me = this;

                return me.getCurrentWeeks();
            },
            nextWeeks: function() {
                var me = this;

                me.page++;
                return me.getCurrentWeeks();
            },
            prevWeeks: function() {
                var me = this;

                if (me.zero) {
                    return;
                }
                me.page--;
                return me.getCurrentWeeks();
            }
        };
    }

    function getAddressString(address) {
        // Get address string from object
        if (!address) {
            return "";
        }
        var strAddress = "";

        if (address.FirstName) {
            strAddress += address.FirstName + " ";
        }
        if (address.LastName) {
            strAddress += address.LastName;
        }
        if (address.Line1) {
            strAddress += ", " + address.Line1;
        }
        if (address.Line2) {
            strAddress += ", " + address.Line2;
        }

        return strAddress;
    }

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];

        // Deferred for startData
        me.startDateDeferred = when.defer();
    }, {
        // Render calendar with order/receiver info
        "render": function() {
            var me = this;
            var $me = me.$element;

            if (!me.twoWeeks) {
                return;
            }

            var calendarOrder = me.twoWeeks.currentWeeks();
            var deliveryList = me.deliveryList;
            var i = 0;
            var iLens = calendarOrder.length;
            var j = 0;
            var jLens = deliveryList.length;

            var iCalendarOrder;
            var jDeliveryList;
            // Has products in order
            if (jLens > 0) {
                // Build data for template
                for (i = 0; i < iLens; i++) {
                    // Loop package data
                    for (j = 0; j < jLens; j++) {
                        iCalendarOrder = calendarOrder[i];
                        jDeliveryList = deliveryList[j];
                        // Append package data to corresponding date
                        if (INS_TIME.compareDate(jDeliveryList.Date, iCalendarOrder.date) === 0) {
                            // Prevent undefined
                            jDeliveryList.Products = jDeliveryList.Products || [];
                            jDeliveryList.PackageProducts = jDeliveryList.PackageProducts || [];
                            // Copy
                            iCalendarOrder.products = jDeliveryList.Products.concat(jDeliveryList.PackageProducts);
                            iCalendarOrder.receiverAddress = getAddressString(jDeliveryList.Receiver);
                            iCalendarOrder.isDeliver = jDeliveryList.IsDeliver;
                            iCalendarOrder.deliverTime = jDeliveryList.DeliverTimeName;
                        }
                    }
                }
            }
            // Render Two Weeks Calendar
            me.html(template, {
                "calendar": calendarOrder,
                "isStartWeek": me.twoWeeks.zero
            }).then(function() {
                $me.find("[data-toggle=popover]").popover({
                    "trigger": "hover",
                    "html": true,
                    "title": blurb.get(1609),
                    "placement": "right",
                    "delay": {
                        "show": 0,
                        "hide": 50
                    }
                });
            });
        },
        "hub:memory/order/deliveryList": function(deliveryList, isDailyUpdate) {
            var me = this;

            me.deliveryList = deliveryList;

            // Render calendar
            if (!isDailyUpdate) {
                // Render after week data updated
                me.startDateDeferred.promise.then(function() {
                    me.render();
                });
            } else {
                me.render();
            }
        },
        "hub:memory/deliveryStartDate/change": function(args) {
            var me = this;

            // Date info
            var availableStartDate = args.availableStartDate;
            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;

            // Get two week deliver data, and render calendar
            me.twoWeeks = getOrderCalendar(availableStartDate, deliveryStartDate);

            // Week data updated
            me.startDateDeferred.resolver.resolve();
        },
        "dom:[data-action=prevweek]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Move calendar
            me.twoWeeks.prevWeeks();
            // Mixin calendar/order, rendering
            me.render();
        },
        "dom:[data-action=nextweek]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Move calendar
            me.twoWeeks.nextWeeks();
            // Mixin calendar/order, rendering
            me.render();
        },
        // Edit daily order
        "dom:[data-action=editorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $e = $(e.currentTarget);
            // Get date from DOM, return while failure
            var strDate = $e.data("date");
            if (!strDate) {
                return;
            }
            var clickDate = new Date(strDate);
            var i = 0;
            var iLens = me.deliveryList.length;
            var dailyOrder;

            for (i; i < iLens; i++) {
                if (INS_TIME.compareDate(me.deliveryList[i].Date, clickDate) === 0) {
                    dailyOrder = me.deliveryList[i];
                    break;
                }
            }
            // Set a temp daily Order, if no order in current day
            if (!dailyOrder) {
                dailyOrder = {
                    Date: clickDate,
                    IsDeliver: true
                };
            }

            // Popup order-edit lightbox.
            me.publish("customize/popup", dailyOrder);
        }
    });
});