define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/pageurl/main",
    "template!./index.html",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, Blurb, pageUrl, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_PROMOTION_CODE = api.get("order_promotion_code");

    function getStatus(code) {
        var status = [];
        status["1"] = "1242";
        status["2"] = "1270";
        status["3"] = "1242";
        status["4"] = "1367";
        status["5"] = "1368";
        status["6"] = "1369";
        status["7"] = "1388";
        status["8"] = "1389";
        status["9"] = "1390";
        status["10"] = "1391";
        status["11"] = "1468";
        status["12"] = "1625";

        return blurb.get(status[code]) || "";
    }

    function getSuccessStatus(code) {
        // Here code means promotion rule id
        var status = [];
        status["1"] = "1370";
        status["2"] = "1370";
        status["3"] = "1371";
        status["4"] = "1372";
        status["5"] = "1373";
        status["6"] = "1384";
        status["7"] = "1385";
        status["8"] = "1602";
        status["9"] = "1603";
        status["10"] = "1604";

        return blurb.get(status[code]) || "";
    }

    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;

        me.orderId = parseInt(orderId, 10);
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template);
        },
        "dom:form/submit": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            var $code = $me.find("#order-promotioncode");
            var $submitBtn = $me.find("[data-action=applypromotioncode]");

            var code = _.trim($code.val());

            var postData = {
                OrderId: me.orderId,
                Code: code
            };

            ajaxQuery({
                url: URI_PROMOTION_CODE,
                data: JSON.stringify(postData),
                contentType: "application/json; charset=UTF-8",
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                $errMsg.text(getSuccessStatus(data.RuleId)).addClass("order-promotioncode-valid");

                $code.attr("baga", code);
                // Change order price summary
                me.publish("order/promotion", data.Order);
            }, function(e) {
                $errMsg.text(getStatus(e)).removeClass("order-promotioncode-valid");

                $code.removeAttr("baga");
                // Change order price summary reflesh
                me.publish("order/promotion");
            });
        },
        "dom:#order-promotioncode/focus": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            $errMsg.empty().removeClass("order-promotioncode-valid");
        },
        "hub/order/promotion/submit": function(orderId) {
            var me = this;

            var $me = me.$element;

            var $code = $me.find("#order-promotioncode");

            var code = _.trim($code.attr("baga"));
            var postData;

            if (code) {
                postData = {
                    OrderId: me.orderId,
                    Code: code
                };
            }

            var promiseValue = [];
            promiseValue.push(postData);

            return promiseValue;
        }
    });
});