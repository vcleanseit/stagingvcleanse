define(["troopjs-browser/component/widget",
    "jquery",
    "widget/api/main",
    "widget/sitemap/main",
    "widget/ajaxquery/main",
    "widget/cookie/main",
    "template!./index.html"
], function(Widget, $, api, sitemap, ajaxQuery, cookie, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var pathName = location.pathname;
            var pathKey = sitemap.reversematch(pathName);

            if (!pathKey) {
                return;
            }

            // Array[0]: ""
            // Array[1]: main menu
            // Array[2]: sub menu
            var arrPath = pathKey.split("/");

            // Has cookie
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Rendering
                    me.html(template, {
                        "firstName": data.FirstName,
                        "path": arrPath
                    });
                }, function() {
                    // Rendering
                    me.html(template, {
                        "path": arrPath
                    });
                });
            }, function() {
                // Rendering
                me.html(template, {
                    "path": arrPath
                });
            });

        }
    });
});