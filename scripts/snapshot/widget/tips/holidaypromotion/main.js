define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, $, context, PopupBox, template) {
    "use strict";

    var END_DATE = new Date("2015-02-19 00:00:00");

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var now = new Date();
            if(now >= END_DATE){
                return;
            }
            // Render
            me.html(template, {
                "isMobile": context.isMobile,
                "cacheServer": context.cacheServer,
                "language": context.language
            });
        },
        "dom:[data-action=showpromotionrule]/click": function(e) {
            e.preventDefault();

            var me = this;
            var path = context.isMobile ? "/images/event/holidaypromotion/mobile/rule.jpg" : "/images/event/holidaypromotion/rule.jpg";
            var $el = "<div><img src='/images/blank.png' data-src=" + path + " " +
                "data-weave='widget/basic/imageswitch(byLanguage)' data-by-language='true'" +
                (context.isMobile ? "width='100%'" : "width='800' height='306'") +
                "></div>";

            // Lightbox configuration
            var popupRule = new PopupBox({
                msg: $el,
                closeble: true,
                closeInner: true,
                zIndex: 10,
                // TODO:
                closeCallback: function() {}
            });

            // Manually weave template
            // TODO: Show lightbox first
            popupRule.open().then(function() {});

        }
    });
});