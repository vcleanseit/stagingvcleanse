define(["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html",
    "swipe"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                var $carousel = $me.find('.giftcard-carousel');

                //                    var carouselSwipe = new Swipe($carousel.get(1));
                $carousel.Swipe();
            });
        },
        "dom:.carousel-indicators li/click": function(e) {
            var me = this;
            var $me = me.$element;
            if ($(e.currentTarget).index() === 0) {
                var $carousel = $me.find('.giftcard-carousel');
                //                    setTimeout(function(){
                //                        new Swipe($carousel.get(0));
                //                    },10);
                new Swipe($carousel.get(0));
                //                $carousel.Swipe();
            }
        }
    });
});