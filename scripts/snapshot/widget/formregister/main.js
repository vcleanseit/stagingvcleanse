define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/basic/regex",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "template!./index.html",
    "widget/basic/jqueryext",
    "jquery.validation",
    "jquery.formatter",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, regex, ajaxQuery, api, InfoBox, pageUrl, Blurb, template) {
    "use strict";

    var URI_PHONECHECK = api.get("phonecheck");
    var URI_ACCOUNT_CREATE = api.get("account_create");
    var URI_SENDVERIFICATIONCODE = api.get("sendverificationcode");

    var SENDTIME = 2;

    var blurb = new Blurb($(document.body));

    function getStatus(code) {
        var status = [];
        status["1"] = "1187";
        status["10"] = "1181";
        status["11"] = "1182";
        status["12"] = "1183";
        status["13"] = "1184";
        status["-1"] = "1185";
        status["-2"] = "1186";
        status["-3"] = "1188";
        status["-4"] = "1189";
        status["-5"] = "1190";
        status["-6"] = "1191";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            }).then(function() {
                var $form = $me.find("form");
                // Validation
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1211 = $form.find("#blurb-1211").val();
            var m1212 = $form.find("#blurb-1212").val();
            var m1213 = $form.find("#blurb-1213").val();
            var m1214 = $form.find("#blurb-1214").val();
            var m1215 = $form.find("#blurb-1215").val();
            var m1216 = $form.find("#blurb-1216").val();
            var m1217 = $form.find("#blurb-1217").val();
            var m1345 = $form.find("#blurb-1345").val();

            $form.validate({
                rules: {
                    "signup-verificationcode": "required",
                    "sendsms-image-verificationcode": "required",
                    "signup-image-verificationcode": "required",
                    "signup-password": "required",
                    "signup-passwordrepeat": {
                        required: true,
                        equalTo: "#signup-password"
                    },
                    "signup-mobile": {
                        required: true,
                        number: true,
                        mobile: true,
                        remote: {
                            url: URI_PHONECHECK,
                            type: "GET",
                            data: {
                                phone: function() {
                                    return $me.find("#signup-mobile").val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    "signup-verificationcode": m1211,
                    "sendsms-image-verificationcode": m1345,
                    "signup-image-verificationcode": m1345,
                    "signup-password": m1212,
                    "signup-passwordrepeat": {
                        "required": m1213,
                        "equalTo": m1214
                    },
                    "signup-mobile": {
                        "required": m1215,
                        "number": m1216,
                        "remote": m1217
                    }
                },
                submitHandler: function(form) {
                    var registerData = {
                        "password": _.trim($form.find("#signup-password").val()),
                        "mobile": _.trim($form.find("#signup-mobile").val()),
                        "ValidationCode": _.trim($form.find("#signup-verificationcode").val()),
                        "ValidateType": 1 // Mobile
                    };

                    if (SENDTIME <= 0) {
                        _.extend(registerData, {
                            "ValidationCode": _.trim($form.find("#signup-image-verificationcode").val()),
                            "ValidateType": 0 // Image
                        });
                    }

                    ajaxQuery({
                        url: URI_ACCOUNT_CREATE,
                        data: JSON.stringify(registerData),
                        contentType: "application/json; charset=UTF-8",
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Direct to enrollment success page
                        pageUrl.goto("/enrollment/success");

                        refreshSecurity($("[data-action=refreshimagesecurity]"));
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();

                        refreshSecurity($("[data-action=refreshimagesecurity]:eq(1)"));
                    });
                }
            });
        },
        "dom:button[data-action=sendverificationcode]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var $sending = $me.find(".sending");
            var $imageVerification = $(".imageverificationcode");

            var $mobile = $me.find("#signup-mobile");
            var mobile = _.trim($mobile.val());

            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            var verificationCode = _.trim($verificationCode.val());

            if ($mobile.valid()) {
                if (SENDTIME > 0) {
                    if ($verificationCode.valid()) {
                        ajaxQuery({
                            url: URI_SENDVERIFICATIONCODE,
                            data: {
                                "mobile": mobile,
                                language: context.language,
                                verifyCode: verificationCode
                            },
                            type: "POST",
                            dataType: "json"
                        }).then(function(data) {
                            $me.find("#signup-verificationcode").focus();

                            //
                            $el.hide();
                            $imageVerification.hide();
                            $verificationCode.val("");
                            $sending.show();
                            var $countdown = $("[data-action=countdown]");
                            var countdownMax = 60;

                            (function countdownSending() {
                                if (countdownMax <= 0) {
                                    clearTimeout(insTimeout);

                                    $countdown.text("");
                                    $sending.hide();
                                    $el.show();
                                    if (SENDTIME > 0) {
                                        $el.text(blurb.get("1342"));
                                        refreshSecurity($("[data-action=refreshimagesecurity]:eq(0)"));
                                        $imageVerification.show();
                                    } else {
                                        $el.text(blurb.get("1343"));
                                    }

                                    return;
                                }
                                var insTimeout = setTimeout(function() {
                                    $countdown.text(countdownMax);
                                    countdownSending();
                                }, 1000);
                                countdownMax--;
                            })();

                            // Send time count, while API success
                            SENDTIME--;
                        }, function(e) {
                            // Show error msg as a lightbox
                            new InfoBox({
                                title: 1144,
                                content: getStatus(e)
                            }).open();
                        });
                    }
                } else {
                    $(".mobilesecuritycode").hide();
                    refreshSecurity($("[data-action=refreshimagesecurity]:eq(1)"));
                    $(".imagesecuritycode").show();
                }
            }
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});