define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/cookie/main",
    "template!./index.html"
], function(Widget, $, context, cookie, template) {
    "use strict";

    var LANGUAGE_CSSCLASS_PREFIX = "lng-";
    var COOKIE_LNG_EXPIRES = 365;

    function saveLngToCookie(toLng) {
        cookie.set("lng", toLng, {
            expires: COOKIE_LNG_EXPIRES,
            path: "/"
        });
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var language = context.language;

            // Styling by language
            $(document.documentElement).addClass(LANGUAGE_CSSCLASS_PREFIX + language);

            // Rendering
            me.html(template, {
                "language": language
            });
        },
        "dom:[data-action=changelanguage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            var toLng = $el.data("lng");
            // Store in cookie
            saveLngToCookie(toLng);
            // Refresh page
            location.reload();
        }
    });
});