define(["troopjs-core/component/base",
    "jquery",
    "widget/cookie/main"
], function(Component, $, cookie) {
    "use strict";

    var LANGUAGE_EN_US = "en-us";
    var LANGUAGE_ZH_CN = "zh-cn";

    var COOKIE_LNG_EXPIRES = 365;

    function saveLngToCookie(toLng) {
        cookie.set('lng', toLng, {
            expires: COOKIE_LNG_EXPIRES,
            path: '/'
        });
    }

    return Component.extend({
        "init": function() {
            // Get browser language
            var language = navigator.language;
            // For IE
            if (!language) {
                language = navigator.browserLanguage;
            }
            // CN
            if (language.toLowerCase() === LANGUAGE_EN_US) {
                saveLngToCookie("en");
                return "en";
            } else {
                saveLngToCookie("cn");
                return "cn";
            }
        }
    });

});