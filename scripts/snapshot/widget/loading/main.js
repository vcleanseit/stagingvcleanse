define(["troopjs-core/component/gadget",
    "jquery",
    "template!./index.html"
], function(Gadget, $, template) {
    "use strict";

    return Gadget.extend(function($el) {
        var me = this;
        me.$el = $el;
    }, {
        "sig/start": function() {
            var me = this;

            if (!me.$el || me.$el.length <= 0) {
                return;
            }

            me.$el.html(template);
        },
        "error": function(msg){
            var me = this;

            var $loadingSpinner = me.$el.find(".loading-spinner");
            $loadingSpinner.addClass("error");

            // Append error msg
            if(msg){
                $loadingSpinner.find("> p").text(msg);
            }
        }
    });
});