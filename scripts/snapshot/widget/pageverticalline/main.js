define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            setTimeout(function() {
                var heightLeft = $me.prev().height();
                var heightRight = $me.next().height();
                var heightMe = ((heightLeft > heightRight) ? heightLeft : heightRight) + 20;
                $me.height(heightMe);
            }, 250);

        }
    });
});