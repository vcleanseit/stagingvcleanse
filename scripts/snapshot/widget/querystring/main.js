define(["jquery",
    "when"
], function($, when) {
    "use strict";

    return {
        // Get Request from URL, retrun an object {query1: value1, query2: value2}
        getRequest: function() {
            var url = location.search;
            var theRequest = {};
            if (url.indexOf("?") !== -1) {
                var str = url.substr(1);
                var strs = str.split("&");
                for (var i = 0, l = strs.length; i < l; i++) {
                    theRequest[(strs[i].split("=")[0]).toLowerCase()] = unescape(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
    };
});