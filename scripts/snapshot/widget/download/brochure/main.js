define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/download/main",
    "template!./index.html"
], function(Widget, $, context, Download, template) {
    "use strict";

    return Download.extend({
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template, context);
        }
    });
});