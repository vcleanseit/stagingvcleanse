define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/blurb/main",
    "template!./index.html",
    "widget/basic/jqueryext",
    "jquery.validation",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, pageUrl, cookie, Blurb, template) {
    "use strict";

    var URI_PASSWORD_CODE = api.get("forgotpasswordverificationcode");
    var URI_RESET_PASSWORD = api.get("resetpassword");

    var blurb = new Blurb($(document.body));

    function getVerificationStatus(code) {
        var status = [];
        status["-4"] = "1273";
        status["-6"] = "1182";
        status["11"] = "1191";
        status["-5"] = "1190";

        return status[code] || "";
    }

    function getStatus(code) {
        var status = [];
        status["-5"] = "1274";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            }).then(function() {
                var $form = $me.find("form");
                var $account = $form.find('[name=signup-mobile]');
                // Get account from cookie
                cookie.get("account").then(function(account) {
                    // Auto filled account
                    $account.val(account);
                });
                // Validation            
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1211 = $form.find("#blurb-1211").val();
            var m1215 = $form.find("#blurb-1215").val();
            var m1216 = $form.find("#blurb-1216").val();
            var m1345 = $form.find("#blurb-1345").val();
            $form.validate({
                rules: {
                    "signup-verificationcode": "required",
                    "sendsms-image-verificationcode": "required",
                    "signup-mobile": {
                        required: true,
                        number: true,
                        mobile: true
                    }
                },
                messages: {
                    "signup-verificationcode": m1211,
                    "sendsms-image-verificationcode": m1345,
                    "signup-mobile": {
                        required: m1215,
                        number: m1216
                    }
                },
                submitHandler: function(form) {
                    var resetPasswordData = {
                        "mobile": _.trim($form.find("#signup-mobile").val()),
                        "ValidationCode": _.trim($form.find("#signup-verificationcode").val()),
                        "language": context.language
                    };

                    ajaxQuery({
                        url: URI_RESET_PASSWORD,
                        data: resetPasswordData,
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Show msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1160,
                            closeCallback: function() {
                                pageUrl.goto("/enrollment/signin");
                            }
                        }).open();
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();
                    });
                }
            });
        },
        "dom:button[data-action=sendverificationcode]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var $sending = $me.find(".sending");
            var $imageVerification = $(".imageverificationcode");

            var $mobile = $me.find("#signup-mobile");
            var mobile = _.trim($mobile.val());

            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            var verificationCode = _.trim($verificationCode.val());

            if ($mobile.valid()&&$verificationCode.valid()) {
                ajaxQuery({
                    url: URI_PASSWORD_CODE,
                    data: {
                        "mobile": mobile,
                        "language": context.language,
                        "verifyCode": verificationCode
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("#signup-verificationcode").focus();

                        //
                        $el.hide();
                        $imageVerification.hide();
                        $sending.show();
                        var $countdown = $("[data-action=countdown]");
                        var countdownMax = 60;

                        (function countdownSending() {
                            if (countdownMax <= 0) {
                                clearTimeout(insTimeout);

                                $countdown.text("");
                                $sending.hide();
                                $imageVerification.show();
                                $el.show().text(blurb.get("1342"));

                                return;
                            }
                            var insTimeout = setTimeout(function() {
                                $countdown.text(countdownMax);
                                countdownSending();
                            }, 1000);
                            countdownMax--;
                        })();
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getVerificationStatus(e)
                    }).open();
                });
            }
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});