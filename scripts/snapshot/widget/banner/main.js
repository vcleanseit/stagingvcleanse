define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/sitemap/main",
    "template!./index.html"
], function (Widget, $, context, sitemap, template) {
    "use strict";

    return Widget.extend(function ($element, widgetName, pathName) {
        var me = this;

        me.pathName = pathName || null;
    }, {
        "sig/start": function () {
            var me = this;
            var pathName = me.pathName || location.pathname;
            var pathKey = sitemap.reversematch(pathName);

            if (!pathKey) {
                return;
            }

            // Array[0]: ""
            // Array[1]: main menu
            // Array[2]: sub menu
            var arrPath = pathKey.split("/");
            // Rendering
            me.html(template, {
                "path": arrPath,
                "isMobile": context.isMobile,
                "cacheServer": context.cacheServer,
                "language": context.language
            });

        }
    });
});