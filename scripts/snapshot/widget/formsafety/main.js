define(["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"widget/cookie/main",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"template!./index.html",
	"jquery.validation",
	"jquery.ui.datepicker",
	"json2",
	"underscore.string"
], function(Widget, weave, $, context, cookie, ajaxQuery, api, InfoBox, template) {
	"use strict";

	var URI_ACCOUNT = api.get("account");
	var URI_ACCOUNT_CHGPWD = api.get("account_chgpwd");

	function getStatus(code) {
		var status = [];
		status["-1"] = "1462";
		status["1000"] = "1462";
		status["-5"] = "1190";

		return status[code] || "";
	}

	function refreshSecurity() {
		var $image = $("[data-action=refreshimagesecurity]");

		var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
		$image.attr("src", src);
	}

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;


			cookie.get("memberid").then(function() {
				me.html(template, {
					serviceDomain: context.serviceDomain
				}).then(function() {
					me.validation();
				});
			});
		},
		"validation": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var m1213 = $form.find("#blurb-1213").val();
			var m1214 = $form.find("#blurb-1214").val();


			var $image = $("[data-action=refreshimagesecurity]");
			var $vcode = $form.find("#signup-image-verificationcode");
			var $pwd = $form.find("#signup-password");
			var $pwdre = $form.find("#signup-passwordrepeat");

			$form.validate({
				rules: {
					"signup-passwordrepeat": {
						equalTo: "#signup-password"
					}
				},
				messages: {
					"signup-passwordrepeat": {
						required: m1213,
						equalTo: m1214
					}
				},
				submitHandler: function(form) {
					var registerData = {
						password: _.trim($pwd.val()),
						validationcode: _.trim($vcode.val())
					};

					cookie.get("memberid").then(function() {
						ajaxQuery({
							url: URI_ACCOUNT_CHGPWD,
							data: JSON.stringify(registerData),
							type: "POST",
							contentType: "application/json; charset=UTF-8",
							dataType: "json"
						}).then(function(data) {
							// Show response msg as a lightbox
							new InfoBox({
								title: 1148,
								content: 1465
							}).open().then(function() {
								refreshSecurity($image);
								$vcode.val("");
								$pwd.val("");
								$pwdre.val("");
							});
						}, function(e) {
							// Show error msg as a lightbox
							new InfoBox({
								title: 1144,
								content: getStatus(e)
							}).open().then(function() {
								refreshSecurity($image);
								$vcode.val("");
								if (e === "-5") {
									$pwd.val("");
									$pwdre.val("");
								}
							});
						});
					});
				}
			});
		},
		"dom:[data-action=refreshimagesecurity]/click": function(e) {
			e.preventDefault();

			var me = this;
			refreshSecurity();
		}
	});
});