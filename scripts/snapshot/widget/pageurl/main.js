define(["jquery",
    "widget/sitemap/main"
], function($, sitemap) {
    "use strict";

    return {
        "goto": function(key, hash, query) {
            var href;

            href = sitemap.get(key);

            if (query) {
                href += "?" + $.param(query);
            }
            if (hash) {
                href += "#" + hash;
            }
            location.href = href;
        },
        "winopen": function(key, hash, query) {
            var href;

            href = sitemap.get(key);

            if (query) {
                href += "?" + $.param(query);
            }
            if (hash) {
                href += "#" + hash;
            }
            window.open(href);
        }
    };
});