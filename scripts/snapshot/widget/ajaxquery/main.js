define(["jquery",
    "when",
    "context",
    "widget/loading/main"
], function($, when, context, Loading) {
    "use strict";

    return function(option, $el) {
        var me = this;
        var deferred = when.defer();

        var loading;
        if ($el && $el.length > 0) {
            loading = new Loading($el);
        }

        // Parameters check
        if (!option) {
            console.log('Ajax call without option!');
            deferred.reject(new Error('Ajax call without option!'));
        }
        if (!option.url) {
            console.log('Ajax call without url!');
            deferred.reject(new Error('Ajax call without url!'));
        }

        // Apply cachekey while start a HTTP GET request
        if (option.type.toUpperCase() === "GET" && context.serviceCacheKey) {
            // Add version
            $.extend(true, option, {
                data: {
                    "v": context.serviceCacheKey
                }
            });
        } /*else if (option.type.toUpperCase() === "POST") {
            // Add content type
            // Only JSON format as post data
            $.extend(true, option, {
                contentType: "application/json; charset=UTF-8"
            });
        }*/
        // For local test environment
        if (!context.serviceDomain) {
            // USE HTTP GET
            $.extend(true, option, {
                type: "GET"
            });
        }

        // Ajax
        var request = $.ajax(option);

        function ajaxReturnHandler() {
            request.done(function(data) {
                var statusCode;
                var description;
                if (data && data.IsSuccess) {
                    deferred.resolve(data);
                } else {
                    statusCode = data.StatusCode || -999;
                    description = data.Description || "";
                    if (loading) {
                        loading.error("Service with error! [" + statusCode + "]");
                    }
                    deferred.reject(statusCode);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                if (loading) {
                    loading.error("Service with error!");
                }

                deferred.reject(new Error('Ajax call failed.'));
            });
        }

        // Add loading icon
        if (loading) {
            loading.start().then(ajaxReturnHandler);
        } else {
            // Execute ajax result
            ajaxReturnHandler();
        }

        return deferred.promise;
    };

});