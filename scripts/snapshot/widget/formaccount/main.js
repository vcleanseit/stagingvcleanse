define(["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"template!./index.html",
	"jquery.validation",
	"jquery.ui.datepicker",
	"jquery.ui.datepicker-zh-CN",
	"json2",
	"underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, template) {
	"use strict";

	var URI_ACCOUNT = api.get("account");
	var URI_EMAILCHECK = api.get("emailcheck");
	var URI_PHONECHECK = api.get("phonecheck");
	var URI_ACCOUNT_UPDATE = api.get("account_update");

	function geti18n() {
		var lngMap = {
			"en": "",
			"cn": "zh-CN"
		};
		var lng = context.language || "en";
		return lngMap[lng];
	}

	function getStatus(code) {
		var status = [];
		status["-1"] = "1185";

		return status[code] || "";
	}

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_ACCOUNT,
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				//Render
				me.html(template, data).then(function() {

					// Generate datepicker by jQuery-UI
					var $datepicker = $me.find("[data-action=datepicker]");
					$(document.body).addClass("jquery-ui-lightness");

					// Localize
					$.datepicker.setDefaults($.datepicker.regional[geti18n()]);
					// Datepicker
					$datepicker.datepicker({
						"firstDay": 1,
						"showAnim": "slideDown",
						"changeMonth": true,
						"changeYear": true,
						"dateFormat": "yy-mm-dd",
						"yearRange": "1940:2014",
						"defaultDate": "1980-06-15"
					});

					// Validate & submit
					me.validation();

				});
			});
		},
		"validation": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var m1215 = $form.find("#blurb-1215").val();
			var m1216 = $form.find("#blurb-1216").val();
			var m1275 = $form.find("#blurb-1275").val();
			var m1276 = $form.find("#blurb-1276").val();
			var m1277 = $form.find("#blurb-1277").val();

			$form.validate({
				rules: {
					"signup-firstname": "required",
					"signup-lastname": "required",
					"signup-email": {
						required: true
					},
					"signup-phonenumber": {
						required: true,
						number: true
					},
					"signup-birthday": {
						dateISO: true
					}
				},
				messages: {
					"signup-firstname": m1275,
					"signup-lastname": m1276,
					"signup-email": {
						required: m1277
					},
					"signup-phonenumber": {
						required: m1215,
						number: m1216
					},
					"signup-birthday": {
						dateISO: "e.g. 2000-01-01"
					}
				},
				submitHandler: function(form) {
					var birthday = $form.find("#signup-birthday").val();
					var registerData = {
						firstName: _.trim($form.find("#signup-firstname").val()),
						lastName: _.trim($form.find("#signup-lastname").val()),
						validationcode:"",
						mobile: _.trim($form.find("#signup-phonenumber").val()),
						EMail: _.trim($form.find("#signup-email").val()),
						Gender: $form.find("[name=signup-gender]:checked").val(),
						Birthday: birthday ? new Date(birthday) : ""
					};

					ajaxQuery({
						url: URI_ACCOUNT_UPDATE,
						data: JSON.stringify(registerData),
						type: "POST",
						contentType: "application/json; charset=UTF-8",
						dataType: "json"
					}).then(function(data) {
						// Show response msg as a lightbox
						new InfoBox({
							title: 1148,
							content: 1153
						}).open();
					}, function(e) {
						// Show error msg as a lightbox
						new InfoBox({
							title: 1144,
							content: getStatus(e)
						}).open();
					});
				}
			});
		}
	});
});