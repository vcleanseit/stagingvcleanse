define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./selfpickup.html",
    "json2",
    "underscore.string"
], function(Widget, weave, when, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_STORES = api.get("stores");
    
    
    function getCities(stores) {
        var i = 0;
        var iLens = stores.length;
        var cities = [];
        var storeCityId = 0;

        function searchCities() {
            return _.find(cities, function(e, j) {
                return e.Id === storeCityId;
            });
        }

        for (i = 0; i < iLens; i++) {
            storeCityId = stores[i].StoreCityId;
            // City exist
            if (!stores[i].IsActive || searchCities()) {
                continue;
            }
            // City not exist
            cities.push({
                "Id": storeCityId,
                "Name": stores[i].StoreCityName
            });
        }

        return cities;
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;
        me.selectedStoreId = "";
        me.stores = [];
        me.dataAccount = "";
    }, {
        "render": function(stores, dataAccount) {
            var me = this;
            var $me = me.$element;

            dataAccount = dataAccount || {};
            me.stores = stores;
            me.dataAccount = dataAccount;

            // Rendering
            me.html(template, {
                "account": dataAccount,
                "stores": me.stores,
                "cities": getCities(stores),
                "selectedStoreId":me.selectedStoreId
            }).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    checkedClass: 'checked',
                    increaseArea: '20%'
                });
                // Check default
                $('input[name=selfpickup-store]:eq(0)').iCheck('check');
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // GET deliver time
            ajaxQuery({
                url: URI_STORES,
                type: "GET",
                data: {
                    "language": context.language
                },
                dataType: "json"
            }).then(function(data) {
                var stores = data.Stores;

                // Signin? Write default account info.
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.render(stores, data);
                }, function() {
                    //
                    me.render(stores);
                });

            });
        },
        "getIsSelfPickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/pickuptype").spread(function(dataPickupType) {
                return when.resolve(dataPickupType);
            });
        },
        "hub/order/selfpickup": function() {
            var me = this;
            var $me = me.$element;

            return me.getIsSelfPickup().then(function(isSelfPickup) {
                var storeId;
                var firstName;
                var mobile;
                var selfpickupInfo = {};
                var promiseValue = [];

                if (isSelfPickup) {
                    storeId = $me.find(".iradio_square.checked > input").val();
                    firstName = _.trim($me.find("#selfpickup-firstname").val());
                    mobile = _.trim($me.find("#selfpickup-phonenumber").val());

                    // Validate mobile
                    if (!regex.mobile(mobile)) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1408
                        }).open();

                        return when.reject();
                    }

                    promiseValue.push({
                        "isSelfPickup": true,
                        "selfpickupInfo": {
                            "storeId": storeId,
                            "firstName": firstName,
                            "mobile": mobile
                        }
                    });
                } else {
                    promiseValue.push({
                        "isSelfPickup": false,
                        "selfpickupInfo": {}
                    });
                }
                return promiseValue;
            });
        },
        "dom:[data-action=changeStore]/change":function(e){
        	var me = this;
        	var $me = me.$element;
        	me.selectedStoreId = $(e.currentTarget).val();
        	me.render(me.stores, me.dataAccount);
        },
        "dom:[data-toggle=tab]/click": function(e) {
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var href = $el.attr("href");

            // Check default
            $me.find(href + ' input[name=selfpickup-store]:eq(0)').iCheck('check');
            
        }
    });
});