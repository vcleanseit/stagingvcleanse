/*订单提交第一步主逻辑
 * from widget/delivery/order
 **/
define(["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    //"template!./delivery_order.html",
    "json2"
], function(Widget, $, _, when, context, cloneObject, cookie, ajaxQuery, api, pageUrl, InfoBox, Time) {//, template
    "use strict";

    var URI_RECEIVER = api.get("receiver");
    var URI_ORDER_CREATE = api.get("order_create");
    var URI_DELIVER_TIME = api.get("deliver_time");
    var URI_ACCOUNT = api.get("account");

    // Time function
    var INS_TIME = new Time();

    function getStatus(code) {
        var status = [];
        status["1000"] = "1360"; //参数错误
        status["-100"] = "1360"; //没有产品
        status["-1"] = "1360"; //创建失败
        status["1001"] = "1360"; //未知错误
        status["-200"] = "1365"; //页面过期
        status["-400"] = "1469"; //地区不配送

        return status[code] || "";
    }

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Append Template
            //me.html(template);
        },
        // Default receiver
        "getDefaultReceiver": function() {
            var me = this;

            var deferred = when.defer();

            if (me.defaultReceiver) {
                deferred.resolve(me.defaultReceiver);
            } else {
                ajaxQuery({
                    url: URI_RECEIVER,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    var receivers = data.Receivers || [];
                    var i = 0;
                    var iLens = receivers.length;
                    // No deliver address
                    if (iLens <= 0) {
                        deferred.resolve();
                        return;
                    }
                    for (i = 0; i < iLens; i++) {
                        if (receivers[i].IsDefault) {
                            // Save to widget
                            me.defaultReceiver = receivers[i];
                            deferred.resolve(me.defaultReceiver);
                            return;
                        }
                    }
                });
            }
            return deferred.promise;
        },
        "initDeliveryList": function(myPackage) {
            var me = this;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }
            if (!myPackage) {
                return;
            }

            // With a package selected, render package to calendar.
            me.getDefaultReceiver()
                .then(function(defaultReceiver) {

                    // Memory Order
                    var deliveryList = me.deliveryList;
                    var k = 0;
                    var kLens = myPackage.length;

                    // Own package
                    if (kLens <= 0) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1146
                        }).open();
                    } else {
                        // Copy package info & default order info
                        for (k = 0; k < kLens; k++) {
                            if (!deliveryList[k]) {
                                deliveryList[k] = {};
                            }
                            // Init Products
                            deliveryList[k].Products = [];
                            // Package info
                            // Day: day start from 0
                            deliveryList[k].Date = INS_TIME.addDay(me.deliveryStartDate, myPackage[k].Day);
                            deliveryList[k].PackageRuleId = myPackage[k].Id;
                            // Customize (subscription) === 0
                            if (!myPackage[k].Id) {
                                deliveryList[k].Products = myPackage[k].Products;
                            }
                            //deliveryList[k].PackageProducts = myPackage[k].Products;
                            // Default 1 package per(one) time
                            deliveryList[k].PackageRuleCount = 1;

                            // Attach default address to all date
                            if (defaultReceiver && !deliveryList[k].ReceiverId) {
                                // Receiver info
                                deliveryList[k].ReceiverId = defaultReceiver.Id;
                                deliveryList[k].Receiver = defaultReceiver;
                            }
                        }
                    }
                    
                    
                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Init Delivery List");
                });
        },
        // Generate order
        "generateOrder": function(deltaDay) {
            var me = this;
            deltaDay = deltaDay || 0;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }

            function setDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Set new date
                    me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                }
            }

            function resetDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                if (isNaN(deltaDay) || deltaDay === 0) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Reset new date by add delta days
                    if (me.deliveryList[i].Date) {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryList[i].Date, deltaDay);
                    } else {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                    }
                }
            }

            // With orders
            if (me.deliveryList && me.deliveryList.length > 0) {
                // Reset date in deliver list
                if (deltaDay) {
                    resetDeliveryDate();
                } else if (!me.deliveryList[0].Date) {
                    setDeliveryDate();
                }

                // Publish to order confirm, list price list
                me.publishDeliveryList("Generate Order with Delivery List");
            } else {
                // Not initial run
                // Check whether it's a package
                if (me.myPackage) {
                    me.initDeliveryList(me.myPackage);
                } else {
                    // Without orders & package, should be customize
                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Generate Order Without Delivery List");
                }
            }
        },
        "publishDeliveryList": function(sign) {
            var me = this;
            
            // 当页面数据都完成的时候触发summary变化
            me.publish("order/updateSummary");
            
            // Tracking code for debug
            console.log("//-----=====-----// " + sign + " //-----=====-----//");
            console.log(me.deliveryList);
        },
        // Calendar events
        "hub/order/package": function(myPackage) {
            var me = this;

            if (!myPackage) {
                // Customize order
                cookie.get("customize").then(function(customizePackage) {
                    // Memory Order
                    me.deliveryList = JSON.parse(customizePackage);
                }, function() {});
                return;
            }

            // Save
            me.myPackage = cloneObject(myPackage);
            // Mixin calendar/order, rendering
            // Args "myPackage" for FORCE re-render package
            me.initDeliveryList(me.myPackage);
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            if (!args || !args.selectedDate) {
                return;
            }

            // For packageRule caculate
            me.publish("deliveryStartDate/change/next", args);

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            // Gap days between new date & last date
            var deltaDay = 0;
            if (me.deliveryStartDate) {
                deltaDay = INS_TIME.dayDiff(me.deliveryStartDate, deliveryStartDate);
            }
            // Save delivery start date
            me.deliveryStartDate = deliveryStartDate;

            // Mixin calendar/order, rendering
            me.generateOrder(deltaDay);
        },
        "hub/receiver/changeDeliverTime": function(deliverTime) {
            var me = this;

            if (!deliverTime) {
                return;
            }

            // Memory
            me.deliverTime = deliverTime;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].DeliverTimeId = deliverTime.Id;
                me.deliveryList[i].DeliverTimeName = deliverTime.Name;
            }
            // Publish
            me.publishDeliveryList("Deliver Time Change");
        },
        "hub/receiver/changeDeliverAddress": function(receiver) {
            var me = this;

            if (!receiver) {
                return;
            }
            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].Receiver = receiver;
                me.deliveryList[i].ReceiverId = receiver.Id;
            }
            // 
            // Publish
            me.publishDeliveryList("Deliver Address Change");
        },
        "hub/order/addon": function(arrayAddon) {
            var me = this;

            var j = 0;
            var jLens = arrayAddon.length;
            var addonId = 0;

            me.addon = [];

            for (j = 0; j < jLens; j++) {
                addonId = parseInt(arrayAddon[j], 10);
                if (addonId !== 0) {
                    me.addon.push({
                        "Id": addonId,
                        "Count": 1
                    });
                }
            }
        },
        "hub/order/source": function(eventCode) {
            var me = this;

            // Save source code to Memory
            me.eventCode = eventCode || "";
        },
        "getReceiver": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/receiver/nonlogin/submit").spread(function(dataReceiver) {
                return when.resolve(dataReceiver);
            });
        },
        "getSelfpickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/selfpickup").spread(function(dataSelfpickupInfo) {
                return when.resolve(dataSelfpickupInfo);
            });
        },
        "orderColloection": function() {
            var me = this;

            function updateDeliveryList(i) {
                if (me.deliverTime) {
                    me.deliveryList[i].DeliverTimeId = me.deliverTime.Id;
                    me.deliveryList[i].DeliverTimeName = me.deliverTime.Name;
                }

                // strDate used for log, we have a bug on date loss
                me.deliveryList[i].strDate = me.deliveryList[i].Date;
                me.deliveryList[i].Date = INS_TIME.getStrDate(me.deliveryList[i].Date);

                if (me.addon && me.addon.length > 0) {
                    me.deliveryList[i].Products = me.deliveryList[i].Products.concat(me.addon);
                }
            }

            function nonloginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;

                return me.getReceiver().then(function(dataReceiver) {

                    if (!dataReceiver || !dataReceiver.firstName || !dataReceiver.line1 || !dataReceiver.mobile) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    for (i = 0; i < iLens; i++) {
                        // General update delivery list
                        updateDeliveryList(i);

                        me.deliveryList[i].IsSelfPickup = false;
                        me.deliveryList[i].SelfPickupInfo = {};
                        me.deliveryList[i].Contact = dataReceiver;
                        me.deliveryList[i].ReceiverId = 0;
                    }

                    return when.resolve();
                });
            }

            function loginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;
                var withDeliveryAddress = true;

                return me.getSelfpickup().then(function(selfpickup) {

                    var isSelfPickup = selfpickup.isSelfPickup;
                    var selfpickupInfo = selfpickup.selfpickupInfo;

                    if (isSelfPickup) {
                        if (!selfpickupInfo.firstName || !selfpickupInfo.mobile) {
                            // Give a warning: Without receiver
                            // Show error msg as a lightbox
                            new InfoBox({
                                title: 1144,
                                content: 1386
                            }).open();

                            return when.reject();
                        }
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = true;
                            me.deliveryList[i].SelfPickupInfo = selfpickupInfo;
                            me.deliveryList[i].Contact = {};
                            me.deliveryList[i].ReceiverId = 0;
                        }
                    } else {
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = false;
                            me.deliveryList[i].SelfPickupInfo = {};
                            me.deliveryList[i].Contact = {};

                            // Has Delivery Address?
                            if (!me.deliveryList[i].ReceiverId) {
                                withDeliveryAddress = false;
                            }
                        }
                    }

                    if (!withDeliveryAddress) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    return when.resolve();
                });
            }

            // Signin?
            return cookie.get("memberid").then(function() {
                // Get User Account
                return ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(loginSubmit, nonloginSubmit);
            }, nonloginSubmit);
        },
        
        "hub/order/updateSummary":function(){
        	var me = this;
        	var summaryData = {
        		PromotionCode:"",
        		DeliveryList: []
        	};
        	
        	//拿promotion数据
        	me.publish("order/getPromotionCode").spread(function(promotionCode){
            	//拿develiver数据
        		summaryData.PromotionCode = promotionCode;
        		
        		me.getSelfpickup().then(function(selfpickup) {
        			var IsSelfPickup = selfpickup.isSelfPickup;

        			//对原来的Delivery做一个重新的format
                	for(var i = 0;i<me.deliveryList.length;i++){
                		var delivery = {};
                		delivery.PackageRuleId = me.deliveryList[i].PackageRuleId;
                		delivery.PackageRuleCount = me.deliveryList[i].PackageRuleCount;
                		delivery.AreaId = me.deliveryList[i].Receiver.AreaId;
                		delivery.IsSelfPickup = IsSelfPickup;
                		delivery.ProductList = me.deliveryList[i].Products.concat(me.addon);
                		
                		
                		summaryData.DeliveryList.push(delivery);
                	}
        			me.publish("order/summary",summaryData);
        		});
        		//拿product数据

            	//还给summary
        		
        		
        		
        		  
        	});

        	
        	          	
        },
        // Submit order
        "hub/order/submit": function() {
            var me = this;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1147
                }).open();
                // Do not submit order
                return;
            }

            me.orderColloection().then(function orderSubmit() {
                // /order/confirm#xxxx
                var hashSourceCode = "";
                var days;

                /*----------------HARDCODE----------------*/
                // Source code logic
                if (me.eventCode) {
                    switch (me.eventCode.toUpperCase()) {
                        case "GIFT":
                            // Special logic for GiftCard
                            days = me.deliveryList.length || 1;
                            me.deliveryList.splice(1);
                            me.deliveryList[0].PackageRuleCount = days;
                            break;
                        default:
                            hashSourceCode = "/event/" + me.eventCode.toUpperCase();
                            break;
                    }
                }
                /*----------------HARDCODE----------------*/

                // Generate data to submit
                var orderData = {
                    Order_id: 0,
                    SourceCode: me.eventCode,
                    DeliverList: me.deliveryList
                };

                // Ajax submit
                ajaxQuery({
                    url: URI_ORDER_CREATE,
                    data: JSON.stringify(orderData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                	// TODO:这里不要跳到第二步，直接进行第二个页面的提交
                	 me.publish("order/confirmorder",data.Order_id);
                    //pageUrl.goto("/order/confirm", "orderid/" + data.Order_id + hashSourceCode);
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            });
        }
    });
});