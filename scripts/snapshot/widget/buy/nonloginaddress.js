define(["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"widget/enrollment/signin/main",
	"template!./nonloginaddress.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, Signin, template) {
	"use strict";

	(new Signin($(document.body))).start();

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName) {
		var me = this;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				// Popup order-edit lightbox.
				me.publish("enrollment/signin/popup");
			});
		},
		"dom:[data-action=updateregion]/reset": function(e, region) {
			e.preventDefault();
			e.stopPropagation();

			var me = this;
			var $el = $(e.currentTarget);

			if (!region || !region.District) {
				return;
			}

			// Reload Delivery Time Span
			me.publish("deliverTime/reload", region.District.Id);
			me.publish("expressCompany/reload", region.District.ExpressCompanyId);
		},
		"hub/order/receiver/nonlogin/submit": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: region.City.Id,
				areaId: region.District.Id
			};

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return when.reject();
			}

			var promiseValue = [];
			promiseValue.push(addressData);
			return promiseValue;
		},
		"dom:[data-action=signin]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			// Popup order-edit lightbox.
			me.publish("enrollment/signin/popup");
		}
	});
});