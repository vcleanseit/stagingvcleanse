define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./summary.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, number, ajaxQuery, api, pageUrl, template) {
    "use strict";

    var URI_ORDERDETAIL = api.get("orderdetail");
    var URL_SUMMARY = api.get("summary");
    
    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.orderId = orderId;
    }, {
        "render": function(dataOrder) {
            var me = this;
            var $me = me.$element;

            /*
            TotalPrice
            PreferentialPrice
            DeliverFee
            PreferentialLogisticsFee
            Total
             */
            var renderData = {
                    "orderId": "",
                    "product": number.getDecimal(dataOrder.TotalPrice),
                    "delivery": number.getDecimal(dataOrder.DeliverFee),
                    "total": number.getDecimal(dataOrder.Total)
                };
            
//            var renderData = {
//                "orderId": dataOrder.OrderId,
//                "product": number.getDecimal(dataOrder.TotalPrice),
//                "delivery": number.getDecimal(dataOrder.DeliverFee),
//                "total": number.getDecimal(dataOrder.PreferentialPrice + dataOrder.PreferentialLogisticsFee)
//            };

            // Append preferential logic
            if (dataOrder.TotalPrice !== dataOrder.PreferentialPrice) {
                _.extend(renderData, {
                    "preferentialProduct": number.getDecimal(dataOrder.PreferentialPrice)
                });
            }
            if (dataOrder.DeliverFee !== dataOrder.PreferentialLogisticsFee) {
                _.extend(renderData, {
                    "preferentialDelivery": number.getDecimal(dataOrder.PreferentialLogisticsFee)
                });
            }

            me.html(template, renderData);
        },
        //老的第二步提交放在了这里
        "queryOrder": function() {
            var me = this;
            var $me = me.$element;

            var orderId = me.orderId;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var order = data.Order;

                if (order.OrderStatusCode === -10){
                     pageUrl.goto("/payment/topay", "orderid/" + order.OrderID);
                     return;
                }
                else if (order.OrderStatusCode > 0){
                    pageUrl.goto("/order/success", "orderid/" + order.OrderID, {
                        "orderid": order.OrderID
                    });
                    return;
                }

                // Rendering
                me.render(order);
                // Publish order detail
                me.publish("order/orderdetail", order);
            });
        },
        "sig/start": function() {
            var me = this;
            //me.queryOrder();
        },
        // 刷新统计数据
        "hub/order/summary":function(summaryData){
        	var me = this;
        	var $me = me.$element;
        	
        	 ajaxQuery({
                 url: URL_SUMMARY,
                 data: JSON.stringify(summaryData),
                 contentType: "application/json; charset=UTF-8",
                 type: "POST",
                 dataType: "json"
             }, $me).then(function(data) {
            	 var ruleResult = {
                       StatusCodes: data.StatusCodes,
                       RuleId: data.RuleId
                   };
            	 me.publish("order/promotion/error", ruleResult);
            	 me.render(data);
             },function(data){
            	 
            	 
            	 //me.render(data);
             });
        },
        "hub/order/promotion": function(data) {
            var me = this;
            if (data) {
                me.render(data);
            } else {
                me.queryOrder();
            }
        }
    });
});