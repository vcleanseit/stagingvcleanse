define(["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/cookie/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/troophash/main",
    "json2"
], function(Widget, $, _, when, context, cloneObject, ajaxQuery, api, cookie, pageUrl, InfoBox, Time, troopHash) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    var WEEK_DAYS = 7;

    var URI_PACKAGE = api.get("package");
    var URI_PACKAGEBY_ID = api.get("package_loadbyid");

    var PACKAGE_DAYS = [1, 3, 5, 7];
    var PACKAGE_DEFAULT_DAY = 1;

    function parseIntArray(arrString) {
        if (!arrString || arrString.length <= 0) {
            return;
        }

        var i = 0;
        var iLens = arrString.length;
        var arrInt = [];

        for (i = 0; i < iLens; i++) {
            arrInt.push(parseInt(arrString[i], 10));
        }

        return arrInt;
    }

    function getRuleByDay(rulesOrigin, dayOfWeekFromMonday, day) {
        var iWeek = 0;
        day = day || 0;

        iWeek = dayOfWeekFromMonday + day;
        iWeek = iWeek >= WEEK_DAYS ? iWeek - WEEK_DAYS : iWeek;
        return rulesOrigin[iWeek];
    }

    function getPackageDayRules(dataRules, days, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = days || getPackageDefaultDays();
        var packageDayRules = [];

        // Week day
        var dayOfWeekFromMonday;

        // Rule index
        var k = 0;
        var kLens = dataRules.length;
        // Loop round
        var turn = 0;

        if (startDate) { // By week
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                packageDayRules.push(cloneObject(getRuleByDay(dataRules, dayOfWeekFromMonday, i)));
                packageDayRules[i].Day = i;
            }
        } else { // Not by week. Looping
            for (i = 0; i < iLens; i++) {
                // Rule round 
                turn = Math.floor(i / kLens);
                // k: Rule index
                k = i % kLens;

                // i: day index
                packageDayRules.push(cloneObject(dataRules[k]));

                // Day start from 0 - (Default from 1 in DB)
                if (turn === 0) {
                    // First round of rules 
                    packageDayRules[i].Day = dataRules[k].Day - 1;
                } else {
                    // > 1st round 
                    packageDayRules[i].Day = turn * dataRules[kLens - 1].Day + dataRules[k].Day - 1;
                }
            }
        }

        return packageDayRules;
    }

    function getSubscriptionPackageDayRules(dataRules, weekDays, duration, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = duration;
        var j = 0;
        var jLens = weekDays.length;
        var k = 0;
        var kLens = dataRules.length;
        var packageDayRules = [];
        var dayOfWeekFromMonday;
        var weekDay;
        var deltaWeekDay = 0;

        if (startDate) { // By week
            // 0 = Monday; 6 = Sunday;
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    if (deltaWeekDay < 0) {
                        break;
                    }

                    packageDayRules.push(cloneObject(getRuleByDay(dataRules, weekDay)));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;
                }
            }
        } else {
            dayOfWeekFromMonday = weekDays[0] === 0 ? 6 : weekDays[0] - 1;
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    packageDayRules.push(cloneObject(dataRules[k]));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;

                    // k: package index
                    if (k < (kLens - 1)) {
                        k++;
                    } else {
                        k = 0;
                    }

                }
            }
        }

        return packageDayRules;
    }

    function getPackageDefaultDays() {
        return PACKAGE_DEFAULT_DAY;
    }

    function getPackageDays(dataRules, days) {
        // Get days in package
        var dataDays = [];
        // Looping package rules, get days. 1,3,5,7...
        var k = 1;
        var kLens = PACKAGE_DAYS.length;
        for (k = 0; k < kLens; k++) {
            if (days) {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === days
                });
            } else {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === getPackageDefaultDays()
                });
            }
        }
        return dataDays;
    }

    function getPackageGroup(dataPackage, packageId, groupId) {
        // Get package by packageId & groupId
        var dataGroups = [];
        var dataGroup;
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                dataGroups = dataPackage[i].Groups;
                if (dataGroup) {
                    break;
                }
                for (var j = 0, jLens = dataGroups.length; j < jLens; j++) {
                    if (dataGroups[j].Id.toString() === groupId.toString()) {
                        dataGroup = dataGroups[j];
                        break;
                    }
                }
            }
        }
        return dataGroup;
    }

    function getPackageTitle(dataPackage, packageId) {
        // Get package name by packageId
        var title = "";
        var description = "";
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                title = dataPackage[i].Title;
                description = dataPackage[i].Description;
                break;
            }
        }
        return {
            "title": title || "",
            "description": description || ""
        };
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.startDateDeferred = when.defer();
    }, {
        "getPackage": function(packageId) {
            var me = this;
            var $me = me.$element;

            var deferred = when.defer();

            if (me.dataPackage) {
                deferred.resolve(me.dataPackage);
            } else {
                // Get Package
                ajaxQuery({
                    url: packageId ? URI_PACKAGEBY_ID : URI_PACKAGE,
                    data: (packageId ? {
                        packageId: packageId,
                        language: context.language
                    } : {
                        language: context.language
                    }),
                    type: "GET",
                    dataType: "json"
                }).then(function(dataPackage) {
                    me.dataPackage = dataPackage.Packages;
                    deferred.resolve(me.dataPackage);
                    return;
                });
            }
            return deferred.promise;
        },
        "render": function(startDate) {
            var me = this;

            var packageDayRules;

            if (!me.packageRules) {
                return;
            }

            if (me.weekDays && me.duration) {
                // Subscription
                packageDayRules = getSubscriptionPackageDayRules(me.packageRules, me.weekDays, me.duration, startDate);
            } else if (me.packageDays) {
                // Standard
                packageDayRules = getPackageDayRules(me.packageRules, me.packageDays, startDate);
            }
            if (!packageDayRules || packageDayRules.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1146
                }).open();

                return;
            }
            // Publish package package
            me.publish("order/package", packageDayRules);
        },
        "readyForRender": function() {
            var me = this;

            if (me.packageGroup.ByWeek) {
                me.startDateDeferred.promise.then(function(startDate) {
                    me.render(startDate);
                });
            } else {
                // Rendering
                me.render();
            }
        },
        "hub/deliveryStartDate/change/next": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            var status = me.startDateDeferred.promise.inspect();

            if (status.state === "fulfilled" && me.packageGroup && me.packageGroup.ByWeek) {
                me.startDateDeferred = when.defer();
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
                me.readyForRender();
            } else {
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
            }
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            packageid
            groupid
            days
            addon
            -----------
            event
            -----------
            weekDays
            duration
            */

            var packageId = parseInt(hashMap.packageid, 10);
            var groupId = parseInt(hashMap.groupid, 10);

            // General cleanse order
            var packageDays = parseInt(hashMap.days, 10) || getPackageDefaultDays();

            // Subscription
            var weekDays = hashMap.weekDays ? parseIntArray(hashMap.weekDays.split(",")) : [];
            var duration = parseInt(hashMap.duration, 10) || 0;

            // Addon
            var arrayAddon = hashMap.addon ? hashMap.addon.split(",") : [];

            // General cleanse order
            var eventCode = hashMap.event || "";

            // packageId & groupId is required
            // or will direct to root page
            if (isNaN(packageId) || isNaN(groupId)) {
                pageUrl.goto("/");
                return;
            }

            // Trigger event while following param changes
            if (me.packageId === packageId &&
                me.groupId === groupId &&
                me.packageDays === packageDays &&
                _.difference(weekDays, me.weekDays).length === 0 &&
                me.duration === duration &&
                _.difference(arrayAddon, me.arrayAddon).length === 0 &&
                me.eventCode === eventCode
            ) {
                return;
            }
            me.packageDays = packageDays || 0;
            me.weekDays = weekDays;
            me.duration = duration || 0;
            me.arrayAddon = arrayAddon;
            me.eventCode = eventCode;

            // Update Addon
            me.publish("order/addon", arrayAddon);

            // Publish source code
            me.publish("order/source", eventCode);

            me.publish("goods/refreshList",packageId,groupId,packageDays,arrayAddon);
            // Package OR Customize
            if (packageId !== 0 && groupId !== 0) {
                // Clear customize order data from cookie
                cookie.rm("customize", {
                    path: "/"
                });
                // Clear customize order data from cookie
                cookie.rm("subscription_cust", {
                    path: "/"
                });

                // Compare with last hash, return while without change
                if (me.packageId && me.groupId && me.packageId === packageId && me.groupId === groupId) {
                    // Ready for render
                    me.readyForRender();
                    return;
                }
                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;

                // Get Package
                me.getPackage(packageId).then(function(dataPackage) {
                    // Looping package, get the right rule
                    // Save to widget at same time
                    var packageGroup = me.packageGroup = getPackageGroup(dataPackage, packageId, groupId);
                    me.packageTitle = getPackageTitle(dataPackage, packageId);

                    if (!packageGroup) {
                        return;
                    }

                    me.packageRules = packageGroup.Rules;

                    // Ready for render
                    me.readyForRender();
                });
            } else if (weekDays && duration) {
                // Get customize content from cookie
                cookie.get("subscription_cust").then(function(customizeRule) {
                    me.packageRules = [{
                        "Id": 0,
                        "Day": 1,
                        "Name": "Subscription customize",
                        "Products": JSON.parse(customizeRule)
                    }];
                    // Not by week
                    me.render();
                }, function() {});

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            } else { // Customize
                // Publish package package
                me.publish("order/package");

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            }
        }
    });
});