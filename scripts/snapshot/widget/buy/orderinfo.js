// from [delivery/main]
define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/enrollment/signin/main",
    "template!./orderinfo.html",
    "underscore",
    "json2"
], function(Widget, weave, when, $, context, cookie, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, Signin, template) {
    "use strict";
    (new Signin($(document.body))).start();
    
    var URI_ACCOUNT = api.get("account");
 // Time function
    var INS_TIME = new Time();

    var PRICE_DELIVERY = 25;
    
    
    //从confirm里来
    var URI_ORDER_CONFIRM = api.get("order_confirm");
    var EXPIRED_CODE = 5;
    var URI_ORDER_PAYMENT = api.get("orderpayment");
    // Transfer to blurb
    var invoiceDict = {
        "1": 1158,
        "2": 1159
    };
    
    function getIsSelfPickup($el) {
        // Hardcode pickup type
        // 1: Select a shipping address
        // 2: Pick up in store
        return $el.data("pickup").toString() === "2";
    }
    
    return Widget.extend(function(){
    	var me = this;
    	me.orderId = "";
    },{
    	"render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("receiver/hasLogin");
                    }, function() {
                        //
                        me.publish("receiver/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("receiver/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;

            me.render();
        },
        "hub/order/warningNoReceiver": function() {
            var me = this;
            var $me = me.$element;

            var $el = $me.find("[data-action=showErrorMsg-noReceiver]");
            // Show
            $el.show().fadeIn(400);
            // Hide
            setTimeout(function() {
                $el.fadeOut(400, function() {
                    $el.hide();
                });
            }, 3 * 1000);
        },
        // Already login, is a member
        "hub/receiver/hasLogin": function() {
            var me = this;
            me.publish("receiver/refreshAddress"); 
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;
            var $me = me.$element;
             
            // 如果没有登录则弹出登录窗口 
            //me.publish("enrollment/signin/popup");  
            //me.publish("receiver/editAddress");
            // 如果没有登录则显示不登录的地址编辑器
            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            $delivery.html("<div data-weave=\"widget/buy/nonloginaddress()\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	$("#consignee1").removeClass("hide");
            	//隐藏新增收货地址按钮。
            	$("[data-action='addAddress']").hide();
            });
            
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";
            
            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            
            $delivery.html("<ul id='consignee-list' data-weave=\"widget/buy/receiver(receiverid)\" data-receiverid=\"" + receiverId + "\"></ul>");
            
            //$delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	me.publish("receiver/closeAddress");
                $("[data-action=submitorder]").show();
            });
        },
        "hub/goods/refreshList": function(packageId,groupId,packageDays,arrayAddon) {
        	 var me = this;
             var $me = me.$element;
             
             var $goods = $me.find("[data-action=goodscontainer]");
             
             $goods.html("<div class='goods-list' data-weave=\"widget/buy/goodslist(" + packageId + "," + groupId + "," + packageDays + "," + arrayAddon.toString()+")\"></div>");
             
             weave.apply($goods.find("[data-weave]")).then(function() {
            	 	//alert('what happen');
             });
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=editaddresscontainer]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            //$delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            $delivery.html("<div data-weave=\"widget/buy/editaddress(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	me.publish("receiver/openAddress");
                $("[data-action=submitorder]").hide();
            });
        },
        // 配送方式选择器
        "hub/order/pickuptype": function() {
            var me = this;
            var $me = me.$element;

            //var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.active"));
            var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.curr"));

            var promiseValue = [];
            promiseValue.push(isSelfPickup);
            return promiseValue;
        },
		"dom:[data-action=signin]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;
			me.publish("receiver/closeAddress");
			// Popup order-edit lightbox.
			me.publish("enrollment/signin/popup");
		},
		 // Manage delivery address
        "dom:[data-action=addAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress");
        },
        
		//打开地址浮层
		"hub/receiver/openAddress": function(e) {
			if(isLogin){
				$(".order-address-box .tips").hide();
			}else{
				$(".order-address-box .tips").show();
			}
			
			$(".order-address-box").show().css("opacity", 0)
				.animate({"opacity": 1}, 300);
			
			$(".backdrop").css("z-index",14);
			$(".backdrop").show().css("opacity", 0)
			.animate({"opacity": 0.8}, 300);
		},
		
		// 关掉地址
		"dom:[data-action=closeaddress]/click":function(e){
			var me = this;
			me.publish("receiver/closeAddress");
		},
		//关闭地址浮层
		"hub/receiver/closeAddress":function(e){
			$(".order-address-box").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				$(".order-address-box").hide();
			});
			$(".backdrop").css("z-index",-100);
			$(".backdrop").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				
			});
		},
		
		"dom:[data-action=switchshipment]/click":function(e){
			var me = this;
			me.doSwithTab('pay');
		},
		"dom:[data-action=switchselfpack]/click":function(e){
			var me = this;
			me.doSwithTab('picksite');
		},
		"doSwithTab":function(mode){
		//"hub/shipment/change":function(mode){
			var me = this;
			
			$("#express_shipment_item").removeClass('curr');
			$("#pick_shipment_item").removeClass('curr');
			
			$("#express_shipment").addClass("hide");
			$("#selfpick_shipment").addClass("hide");

			switch(mode){
			case "pay":
				$("#express_shipment").removeClass("hide");
				$("#express_shipment_item").addClass('curr');
				break;
			case "picksite":
				$("#selfpick_shipment").removeClass("hide");
				$("#pick_shipment_item").addClass('curr');
				break;
			}
			me.publish("order/updateSummary");
		},
		
		
		//以下从confirm/main里来
		 "getPayment": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/paymenttype/submit", me.orderId).spread(function(dataPayment) {
                return when.resolve(dataPayment);
            });
        },
        "getInvoice": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/invoice/submit", me.orderId).spread(function(dataInvoice) {
                return when.resolve(dataInvoice);
            });
        },
        "getPromotion": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/promotion/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "getGiftInfo": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/gift/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        //TODO:这里是原本第二步的点击提交，所以这里需要改一下
        //"dom:[data-action=confirmorder]/click": function(e) {
        "hub/order/confirmorder": function(orderId) {
            //e.preventDefault();
            var me = this;
            var $me = me.$element;
            //var $el = $(e.currentTarget);

            me.orderId = orderId;
            var postData = {
                "OrderId": me.orderId
            };

            when.join(me.getPayment(), me.getInvoice(), me.getPromotion(), me.getGiftInfo()).then(function(data) {
                postData.Payment = data[0];
                postData.Invoice = data[1];
                postData.Promotion = data[2];
                postData.GiftInfo = data[3];

                _.extend(postData, {
                    "SourceCode": $me.find("#order-sourcecode").val() || ""
                });

                ajaxQuery({
                    url: URI_ORDER_CONFIRM,
                    data: JSON.stringify(postData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                //}, $el.parent()).then(function(data) {
                }).then(function(data) {
                    // Success Confirm Order (save Payment type & Invoice info)
                    //
                    // Redirect by payment type
                    var paymentType = postData.Payment.payment;
                    // URL redirect
                    if (paymentType.toString() === "1") {
                        pageUrl.goto("/payment/topay", "orderid/" + me.orderId);
                    } else {
                        // Show success info
                        pageUrl.goto("/order/success", "orderid/" + me.orderId, {
                            "orderid": me.orderId
                        });
                    }
                });
            });
        }
    });
});


