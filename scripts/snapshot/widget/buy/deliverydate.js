define(["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/time/main",
    "template!./deliverydate.html",
    "json2",
    "underscore"
], function(Widget, when, $, context, cookie, troopHash, Time, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 12;

    //  Holiday configuration
    // Note: Month start from 0
    // Downtown
    var HOLIDAY_MAIN = [
		new Date(2016, 2, 1),
        new Date(2016, 2, 2),
        new Date(2016, 2, 3),
        new Date(2016, 2, 4),
       
    ];
    // Outtown
    var HOLIDAY_OUT_MAIN = [
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
        new Date(2016, 3, 3),
        new Date(2016, 3, 4),
    ];

    var VISIBLE_DATEPICKER = true;
    var VISIBLE_DELIVERYTIME = true;

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.deferred = when.defer();
    }, {
        "sig/start": function() {
            var me = this;
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            // Only Rendering for package, not for customize
            me.html(template, {
                "deliveryStartDate": deliveryStartDate
            }).then(function() {
                if (deliveryStartDate) {
                    // Publish pre-setted date
                    me.publish("deliveryStartDate/change", {
                        "selectedDate": deliveryStartDate
                    });
                }
            });
            */

            var deliveryStartDate = hashMap.deliveryStartDate ? new Date(hashMap.deliveryStartDate) : null;

            var eventCode = hashMap.event || "";
            var packageId = parseInt(hashMap.packageid, 10);
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Hide datepicker while deliveryStartDate exists
            VISIBLE_DATEPICKER = deliveryStartDate ? false : true;

            if (deliveryStartDate) {
                if (deliveryStartDate === me.deliveryStartDate) {
                    return;
                } else {
                    me.deliveryStartDate = deliveryStartDate;
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        // Preset Start Date
                        me.publish("deliveryStartDate/change", {
                            "selectedDate": deliveryStartDate
                        });

                        return when.resolve();
                    });
                }
            } else {
                if (me.rendered) {
                    // Reset startdate
                    me.publish("startdate/set", {
                        "eventCode": eventCode,
                        "days": days,
                        "weekDays": wdays
                    });
                } else {
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        return when.resolve({
                            "eventCode": eventCode,
                            "days": days,
                            "weekDays": wdays
                        });
                    });
                }
            }
        },
        "visibleControl": function(binVisible) {
            var me = this;
            var $me = me.$element;

            var $deliveryTime = $me.find("[data-action=deliveryTimeContainer]");

            if (VISIBLE_DELIVERYTIME) {
                $me.show();
                $deliveryTime.show();
            } else if (VISIBLE_DATEPICKER) {
                $me.show();
                $deliveryTime.hide();
            } else {
                $me.hide();
                $deliveryTime.hide();
            }
        },
        "hub/expressCompany/reload": function(expressCompanyId) {
            var me = this;

            // 以下两个外送公司需要提前到早晨5点截单
            var hourAhead = HOUR_AHEAD;

            // 上海顺丰标准B - 8
            // 上海顺丰空运 - 9
            // 10  顺丰次晨
            // 12  顺丰次晨B
            if (_.indexOf([8, 9, 10, 12], expressCompanyId) >= 0) {
                hourAhead = 10;
            }

            // 上海、北京初五开始配送，其它地区3月1号开始配送
            var holiday = HOLIDAY_OUT_MAIN;

            // 黑猫 - 1
            // 上海顺丰即时达 - 3
            // 北京顺丰冷链 - 7
            if (_.indexOf([1, 3, 7], expressCompanyId) >= 0) {
                holiday = HOLIDAY_MAIN;
            }

            // Reset "Start Date" by Express Company
            /*troopHash.extend({
                "hour": hourAhead
            });*/
            // Reset startdate
            me.deferred.then(function(startDateConfig) {
                startDateConfig = _.extend(startDateConfig, {
                    "hourAhead": hourAhead,
                    "holiday": holiday
                });
                me.publish("startdate/set", startDateConfig);
            });
        },
        // Show/Hide
        "hub/order/showDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = true;
            me.visibleControl();
        },
        "hub/order/hideDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = false;
            me.visibleControl();
        }
    });
});