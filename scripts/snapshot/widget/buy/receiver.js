define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/blurb/main",
    "widget/api/main",
    "template!./receiver.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, Blurb,api, template) {
    "use strict";
    var URI_RECEIVER = api.get("receiver");
    var blurb = new Blurb($(document.body));
    
    function getAddressString(address) {
        if (!address) {
          return "";
        }
        var strAddress = "";

        if (context.language === "en") {
          if (address.FirstName) {
            strAddress += address.FirstName;
          }
          if (address.LastName) {
            strAddress += " " + address.LastName;
          }
          strAddress += ",";
        } else {
          if (address.LastName) {
            strAddress += address.LastName;
          }
          if (address.FirstName) {
            strAddress += " " + address.FirstName;
          }
          strAddress += "，";
        }

        strAddress += " " + address.CityName;
        strAddress += " " + address.AreaName;
        if (address.Line1) {
          strAddress += " " + address.Line1;
        }
        if (address.Line2) {
          strAddress += " " + address.Line2;
        }
        // Deliverble
        if(!address.IsActive){
          strAddress += " (" + blurb.get(1469) + ")";
        }

        return strAddress;
      }
    return Widget.extend(function($element, widgetName, receiverId) {
        var me = this;

        me.receiverId = receiverId || 0;
    }, {
        "sig/start": function() {
            var me = this;
            
            ajaxQuery({
                url: URI_RECEIVER,
                data: {
                  "t": (new Date()).getTime()
                },
                type: "GET",
                dataType: "json"
              }).then(function(data) {
            	  $("#consignee1").removeClass("hide");
            	  	var defaultReceiver;

                  // With/Without receiver
                  if (data.Receivers && data.Receivers.length > 0) {
                	  $("#btnNewAddr").addClass("hide");
                    _.each(data.Receivers, function(receiver) {
                      // Set "Name"
                      receiver.Name = getAddressString(receiver);
                      // Find default receiver
                      if (me.receiverId) {
                        if (receiver.Id === me.receiverId) {
                          defaultReceiver = receiver;
                        }
                      } else if (receiver.IsDefault) {
                        // Set receiverId
                        me.receiverId = receiver.Id;
                        defaultReceiver = receiver;
                      }
                    });
                    
                 // Store options for trigger event
                    me.options = data.Receivers;
                    
                    // Rendering
                    me.html(template, {
                        receiverId: me.receiverId,
                        "options": me.options,
                        "selectedId": me.receiverId,
                        "disabled": me.disabled
                    }).then(function() {
                    	me.reset(me.receiverId);
                    });
                    
                  }
                  
                  me.publish("receiver/default", defaultReceiver);
              });
           
        },
        "hub/receiver/default": function(defaultReceiver) {
            // TODO:
            var me = this;
            var $me = me.$element;
            var $receiverControl = $me.find("[data-action=receivercontrol]");

            if (defaultReceiver) {
                // Set default value.
                me.publish("receiver/changeDeliverAddress", defaultReceiver);
                me.publish("deliverTime/reload", defaultReceiver.AreaId);
                me.publish("expressCompany/reload", defaultReceiver.ExpressCompanyId);

                $receiverControl.show();
            } else {
                $receiverControl.hide();
            }
        },
        
        "dom:[data-action=hoveraddress]/mouseenter":function(e){
        	$("#consignee-list li").removeClass("li-hover");
    		$(e.currentTarget).addClass("li-hover");
        },
        "dom:[data-action=hoveraddress]/mouseleave":function(e){
        	$("#consignee-list li").removeClass("li-hover");
        },
        // 切换选择地址
        "dom:[data-action=receivercontrol]/click": function(e) {
        	var me = this;
            var $el = $(e.currentTarget);
            var optionId = $el.attr('data-option-id');
            var receiver = me.options[optionId];
            
        	$("#consignee-list li").removeClass("ui-switchable-panel-selected");
        	$("#consignee-list .consignee-item").removeClass("item-selected");
        	$el.parent().addClass("ui-switchable-panel-selected");
        	$el.addClass("item-selected");
    		//TODO:更新收货人组件
            

            if (!receiver) {
                return;
            }

            me.receiverId = receiver.Id;
            me.publish("receiver/changeDeliverAddress", receiver);
            me.publish("deliverTime/reload", receiver.AreaId);
            me.publish("expressCompany/reload", receiver.ExpressCompanyId);
        },
       
        // Edit selected address
        "dom:[data-action=editAddress]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $el = $(e.currentTarget);
            me.receiverId = $el.attr('data-receiver-id');
            
            me.publish("receiver/editAddress", {
                isEdit: true,
                receiverId: me.receiverId
            });
        }
    });
});