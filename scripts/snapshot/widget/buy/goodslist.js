define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./goodslist.html",
    "json2",
    "underscore.string"
], function(Widget, weave, when, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";
    var URL_PACKAGE = "/static/data/cn/package33.js";
	var addonDict = [];
	addonDict["22"] = {
		"blur":1292,
        "cn": "每次外送加一瓶芳香椰子水",
        "en": "Fragrant Clear x 1",
        "price":50
    };
    
	addonDict["20"] = {
		"blur":1291,
        "cn": "每次外送加一瓶大师清体柠檬水",
        "en": "Spicy Lemonade x 1",
        "price":25
    };
    
	addonDict["89"] = {
		"blur":1482,
        "cn": "每次外送加一瓶紫甘蓝靓汤",
        "en": "Purple Broth x 1",
        "price":50
    };
    return Widget.extend(function($element, widgetName,packageId,groupId,days,addon1,addon2,addon3) {
        var me = this;
        me.packageId = packageId;
        me.groupId = groupId;
        me.days = days;
        me.arrayAddon = [addonDict[addon1],addonDict[addon2],addonDict[addon3]];
    }, {
    	"render": function(data) {
            var me = this;
            var $me = me.$element;
            
            me.Packages = data.Packages[0];
        	me.Group = {};
        	for(var i=0;i<me.Packages.Groups.length;i++){
        		if(me.Packages.Groups[i].Id === me.groupId){
        			me.Group = me.Packages.Groups[i]; 
        		}
        	}
        	
            me.html(template, {
                "Packages": me.Packages,
                "Group": me.Group,
                "Days":me.days,
                "Addon":me.arrayAddon
            }).then(function(d) {
            	console.log(d);
            },function(d){
            	console.log(d);
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;
            //me.html(template,{"aa":'aa'});
            me.referesh(me.packageId,me.groupId);
        },
        "referesh": function(packageId, groupId) {
        	var me = this;
            var $me = me.$element;
            
            me.packageId = packageId;
            me.groupId = groupId;
            //me.arrayAddon = arrayAddon;
            
        	// 静态页面里关于本产品的数据
        	var statUrl = "/static/data/"+context.language+"/package"+packageId+".js";
        	console.log(statUrl);
            // GET deliver time
            ajaxQuery({
                url: statUrl,
                type: "GET",
                dataType: "json"
            }).then(function(data) {
           	 // Rendering
            	me.render(data);
            },function(data){
            	console.log('goodslist');
            });

        }
    });
});