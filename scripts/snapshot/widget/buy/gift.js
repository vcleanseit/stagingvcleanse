define(["troopjs-browser/component/widget",
    "jquery",
    "widget/cookie/main",
    "template!./gift.html",
    "json2"
], function(Widget, $, cookie, template) {
    "use strict";

    return Widget.extend({
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            me.html(template, data).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            cookie.get("gift_info").then(function(giftInfo) {
                var objGiftInfo = JSON.parse(giftInfo);

                me.render({
                    "isGift": objGiftInfo.isGift,
                    "blessing": objGiftInfo.blessing
                });
            }, function() {
                me.render({
                    "isGift": false,
                    "blessing": ""
                });
            });
        },
        "dom:[data-action=needgift]/ifChanged": function(e) {
            var me = this;
            var $me = me.$element;

            if (e.currentTarget.checked) {
                $me.find(".blessing-sub").show();
            } else {
                $me.find(".blessing-sub").hide();
            }
        },
        "hub/order/gift/update": function(giftInfo) {
            cookie.set("gift_info", JSON.stringify(giftInfo), {
                expires: 7,
                path: "/"
            });
        },
        "hub/order/gift/remove": function() {
            cookie.rm("gift_info", {
                path: "/"
            });
        },
        "hub/order/gift/submit": function(orderId) {
            var me = this;

            var $me = me.$element;
            var isGift;
            var postData;

            isGift = $me.find(".icheckbox_square").hasClass("checked");

            postData = {
                "orderId": orderId,
                "IsGift": isGift,
                "Blessing": isGift === true ? $me.find("#gift-blessing").val() : ""
            };

            var promiseValue = [];
            promiseValue.push(postData);
            return promiseValue;
        }
    });
});