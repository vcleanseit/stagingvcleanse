define(["troopjs-browser/component/widget",
	"jquery",
	"template!./invoice.html",
	"json2"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				$me.find('.icheck').iCheck({
					checkboxClass: 'icheckbox_square',
					radioClass: 'iradio_square',
					increaseArea: '20%'
				});
				// Check default
				$('input[name=invoice-type]:eq(0)').iCheck('check');
			});
		},
		"dom:[data-action=companyinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (!e.currentTarget.checked) {
				return;
			}
			$me.find("#invoice-companyname").focus();
		},
		"dom:[data-action=needinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (e.currentTarget.checked) {
				$me.find(".invoice-sub").show();
			} else {
				$me.find(".invoice-sub").hide();
			}

			// Trigger "height change" event on "auto follower"
			//me.publish("autoFollower/resize");
		},
		//发票的效果
		"dom:[data-action=selectinvoice]/click":function(e){
			var $e = $(e.currentTarget);
			$('#invoice-list .online-payment').removeClass('item-selected');
			$('#invoice-companyname').hide();
			$('#btn-invoice-confirm').hide();
			$e.addClass('item-selected');
			if($e.attr("id") == "invoice-company"){
				// 发票抬头的效果
				$('#invoice-companyname').show();
				$('#btn-invoice-confirm').show();
			}
		},
		// 发票提交
		"dom:[data-action=confirminvoice]/click":function(e){
			var $e = $(e.currentTarget);
			if($e.val() == "OK"){
				if($('#invoice-companyname').val() !== ''){
					$('#invoice-companyname').addClass('rd-only');
					$('#invoice-companyname').attr("readonly",true);
					$e.val("修改");
				}else{
					
				}
			}else{
				$('#invoice-companyname').removeClass('rd-only');
				$('#invoice-companyname').attr("readonly",false);
				$e.val("OK");
				$('#invoice-companyname').focus();
			}
		},
		"hub/order/invoice/submit": function(orderId) {
			var me = this;

			var $me = me.$element;
			var invoiceType;
			var postData;

			if($me.find("#invoice-needinvoice").hasClass("item-selected")){
			}else{
				invoiceType = $me.find(".item-selected").attr("value");
				postData = {
					orderId: orderId,
					invoiceType: invoiceType,
					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
				};
			}
//			if ($me.find(".icheckbox_square").hasClass("checked")) {
//				invoiceType = $me.find(".iradio_square.checked > input").val();
//				postData = {
//					orderId: orderId,
//					invoiceType: invoiceType,
//					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
//				};
//			}

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;

		}
	});
});