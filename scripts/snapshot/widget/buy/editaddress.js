define(["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"template!./editaddress.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, template) {
	"use strict";

	var URI_RECEIVER = api.get("receiver");
	var URI_RECEIVER_ADD = api.get("receiver_create");
	var URI_RECEIVER_UPDATE = api.get("receiver_update");

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName, receiverId) {
		var me = this;

		me.receiverId = receiverId;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;
			var deferred = when.defer();

			if (me.receiverId) {
				ajaxQuery({
					url: URI_RECEIVER,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					var receivers = data.Receivers;
					var i = 0;
					var iLens = receivers.length;
					for (i = 0; i < iLens; i++) {
						if (receivers[i].Id === me.receiverId) {
							me.html(template, receivers[i]);
							break;
						}
					}
					deferred.resolve();
				});
			} else {
				me.html(template);
				deferred.resolve();
			}

			return deferred.promise;
		},
		"dom:[data-action=cancelupdateaddress]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			me.publish("receiver/closeAddress");
		},
		"dom:form/submit": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			} else {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1623
				}).open();

				return;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: cityId,
				areaId: areaId,
				provinceId: $form.find("select[name=provincepicker]").val()
			};

			var URI_ADDRESS = URI_RECEIVER_ADD;
			if (me.receiverId) {
				URI_ADDRESS = URI_RECEIVER_UPDATE;
				$.extend(true, addressData, {
					id: me.receiverId
				});
			}

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return;
			}

			// Update / Add delivery address
			ajaxQuery({
				url: URI_ADDRESS,
				data: JSON.stringify(addressData),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}).then(function(data) {
				// Send update address Id
				me.publish("receiver/refreshAddress", me.receiverId || data.ReceiverID);
			}, function(e) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: getStatus(e)
				}).open();
			});
		}
	});
});