define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/blurb/main",
    "widget/basic/regex",
    "template!./index.html",
    "template!./address.html",
    "jquery.validation",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, Blurb, regex, template, templateAddress) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_RECEIVER = api.get("receiver");
    var URI_RECEIVER_ADD = api.get("receiver_create");
    var URI_RECEIVER_UPDATE = api.get("receiver_update");
    var URI_RECEIVER_DELETE = api.get("receiver_delete");
    var URI_RECEIVER_DEFAULT = api.get("receiver_setdefault");

    function getStatus(code) {
        var status = [];
        status["11"] = "1278";

        return status[code] || "";
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.render();
        },
        "render": function() {
            var me = this;
            var $me = me.$element;

            ajaxQuery({
                url: URI_RECEIVER,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                me.html(template, data.Receivers);
                // Add delivery address if no address
                if (data.Receivers.length <= 0) {
                    var $address = $(templateAddress());
                    // Initial inner data-weave
                    weave.apply($address.find("[data-weave]"));
                    // Append elements
                    $me.find("[data-action=addnewaddress]").prepend($address);
                    $me.find("[data-action=submitvisiblecontrol]").show();
                }
            });
        },
        "dom:[data-action=addaddress]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $address = $(templateAddress());
            // Initial inner data-weave
            weave.apply($address.find("[data-weave]"));
            // Append elements
            $me.find("[data-action=addaddressvisiblecontrol]").before($address);
            $me.find("[data-action=addaddressvisiblecontrol]").hide();
            $me.find("[data-action=submitvisiblecontrol]").show();
        },
        "collectData": function($form) {
            if (!$form || $form.length <= 0) {
                return;
            }

            var region = $form.find("[data-action=updateregion]").data("value");
            var provinceId = 0;
            var cityId = 0;
            var areaId = 0;

            if (region && region.Province && region.City && region.District) {
                provinceId = region.Province.Id;
                cityId = region.City.Id;
                areaId = region.District.Id;
            }

            var data = {
                firstName: _.trim($form.find("[name=signup-firstname]").val()),
                lastName: _.trim($form.find("[name=signup-lastname]").val()),
                mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
                zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
                line1: _.trim($form.find("[name=signup-line1]").val()),
                line2: _.trim($form.find("[name=signup-line2]").val()),
                cityId: cityId,
                areaId: areaId,
                provinceId: provinceId
            };
            return data;
        },
        "dom:[data-action=editaddress]/submit": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).find(".form-deliveryaddress");

            var addressData = me.collectData($form);
            _.extend(addressData, {
                id: $form.data("id")
            });

            // Validate mobile
            if (!regex.mobile(addressData.mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            ajaxQuery({
                url: URI_RECEIVER_UPDATE,
                data: JSON.stringify(addressData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1151
                }).open();
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });

        },
        "dom:[data-action=deleteaddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).parents(".form-deliveryaddress");
            var addressData = {
                id: $form.data("id")
            };

            if (confirm(blurb.get(1249) || "Sure to delete this address?")) {
                ajaxQuery({
                    url: URI_RECEIVER_DELETE,
                    data: addressData,
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    me.render();
                    // Show response msg as a lightbox
                    new InfoBox({
                        title: 1148,
                        content: 1150
                    }).open();
                });
            }

        },
        "dom:[data-action=setaddressdefault]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).parents(".form-deliveryaddress");
            var addressData = {
                id: $form.data("id")
            };

            ajaxQuery({
                url: URI_RECEIVER_DEFAULT,
                data: addressData,
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                me.render();
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1149
                }).open();
            });
        },
        "dom:[data-action=addnewaddress]/submit": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $form = $me.find(".form-adddeliveryaddress");

            var addressData = me.collectData($form);

            // Validate mobile
            if (!regex.mobile(addressData.mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            ajaxQuery({
                url: URI_RECEIVER_ADD,
                data: JSON.stringify(addressData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                me.render();
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1152
                }).open();
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});