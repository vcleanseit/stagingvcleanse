define(["jquery"], function($) {
    "use strict";

    return {
        "getDecimal": function(numeral, digits) {
            digits = digits || 2;
            // 18.00
            numeral = parseFloat(numeral);
            if (isNaN(numeral)) {
                return false;
            }

            /*var times = Math.pow(10, digits);

            // get two decimal while decimal > 2
            // 23.3456 --> 23.35
            var floatNumeral = Math.round(numeral * times) / times;
            var stringNumeral = floatNumeral.toString();
            var pos = stringNumeral.indexOf('.');
            if (pos < 0) {
                pos = stringNumeral.length;
                stringNumeral += '.';
            }
            while (stringNumeral.length <= pos + digits) {
                stringNumeral += '0';
            }
            return stringNumeral;*/

            // USE toFixed()
            return numeral.toFixed(digits);
        }
    };
});