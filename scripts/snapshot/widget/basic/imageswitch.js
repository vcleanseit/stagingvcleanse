define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "underscore",
    "underscore.string"
], function(Widget, $, context) {
    "use strict";

    function isBj(cityName) {
        if (!cityName) {
            return;
        }

        return cityName.indexOf("北京") >= 0 ||
            cityName.indexOf("天津") >= 0;
    }

    return Widget.extend(function($element, widgetName, byLanguage) {
        var me = this;
        me.byLanguage = byLanguage || false;
    }, {
        "render": function(src) {
            var me = this;
            var $me = me.$element;

            if (!src) {
                return;
            }

            // Attach Language
            if (me.byLanguage) {
                src = _.strLeftBack(src, ".") + "_" + context.language + "." + _.strRightBack(src, ".");
            }

            // Add version control
            src = src + "?v=" + context.cacheKey;

            // Add Cache Server
            src = context.cacheServer + "/" + src;

            // Apply to Img src Tag
            $me.attr("src", src);
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var src = $me.data("src") || $me.attr("src");

            // GEO
            var geolocation = $me.data("geolocation") || "";
            if (geolocation && BMap) {
                // USE Baidu Map
                // Require - http://api.map.baidu.com/api?v=2.0
                (new BMap.LocalCity()).get(function(city) {
                    if (isBj(city.name)) {
                        src = _.strLeftBack(src, ".") + "_bj." + _.strRightBack(src, ".");
                    }

                    // Render
                    me.render(src);
                });
            } else {
                // Render
                me.render(src);
            }
        }
    });
});