define(["jquery",
    "underscore",
    "underscore.string"
], function($) {
    "use strict";

    return {
        "email": function(email) {
            email = _.clean(email).replace(" ", "");

            var re = /\w@\w*\.\w/;
            return re.test(email);
        },
        "mobile": function(mobile) {
            mobile = _.clean(mobile).replace(" ", "");

            var re = /^((\+86)|(86))?(1[3|4|5|7|8])\d{9}$/;
            return re.test(mobile);
        }
    };
});