define(["jquery",
    "widget/blurb/main",
    "widget/basic/regex",
    "jquery.validation"
], function($, Blurb, regex) {
    "use strict";

    var blurb = new Blurb($(document.body));

    jQuery.validator.addMethod("mobile", function(value, element) { 
        return regex.mobile(value); 
    }, blurb.get("1408"));
});