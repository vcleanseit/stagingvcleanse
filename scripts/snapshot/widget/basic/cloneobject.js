define(["jquery",
    "underscore"
], function($, _) {
    "use strict";

    return function cloneObject(myObj) {

        if (typeof(myObj) !== 'object') {
            return myObj;
        }
        if (myObj === null) {
            return myObj;
        }

        var myNewObj = _.isArray(myObj) ? [] : {};
        var assignment = false;

        for (var i in myObj) {
            if (myObj.hasOwnProperty(i)) {
                myNewObj[i] = cloneObject(myObj[i]);

                assignment = true;
            }
        }

        return assignment ? myNewObj : myObj;
    };

});