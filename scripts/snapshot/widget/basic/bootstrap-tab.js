define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend(function(){
        var me = this;

        me.timeInstance = null;
    },{
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            //mouseover 切换tab
            $me.find("[data-toggle=tab]").on('mouseenter', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var me = this;

                var $e = $(e.currentTarget);
                me.timeInstance = setTimeout(function(){
                    $e.trigger("click");
                }, 150);
            });
            $me.find("[data-toggle=tab]").on('mouseout', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var me = this;

                clearTimeout(me.timeInstance);
            });
        }
    });
});