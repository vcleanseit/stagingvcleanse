define(["troopjs-browser/component/widget",
  "jquery",
  "context",
  "template!./index.html",
  "underscore"
], function(Widget, $, context, template) {
  "use strict";

  var CAROUSEL_INTERVAL = 10000;
  var CAROUSEL_HEIGHT = 500;
  var CAROUSEL_HEIGHT_MIN = 360;

  /*{
    "type": "spring",
    "blank": false,
    "url": "/cleanse",
    "src": "images/home/spring/spring_content.jpg",
    "title": "维果清祝您新年快乐！购买5天清体疗程，便获得一天礼品卡！",
    "color": "#fff",
    "endDate": new Date("2015-02-19 00:00:00")
  }, {
    "type": "map",
    "id": "workshopmap",
    "blank": true,
    "url": [{
      "shape": "rect",
      "coords": "640,395,845,465",
      "href": "/workshop",
      "alt": "报名入口"
    }],
    "src": "images/home/homeslider_workshop_03.jpg",
    "title": "报名参加一场免费健康Lifestyle工作坊吧！",
    "color": "#fff"
  }*/
  // type: edm/picture/customize
  var EVENTS = [{
    "type": "picture",
    "id": "brand",
    "blank": false,
    "url": "/cleanse",
    "src": "images/home/homeslider_brand.jpg",
    "title": "真正的百分百冷压清体蔬果汁",
    "color": "#fff"
  }, {
    "type": "picture",
    "id": "juicemonday",
    "blank": false,
    "url": "/juicemonday",
    "src": "images/home/homeslider_juicemonday.jpg",
    "title": "周一果汁日：请提一下，清凉一夏！",
    "color": "#fff"
  }, {
    "type": "picture",
    "id": "workshopmap",
    "blank": false,
    "url": "/workshop",
    "src": "images/home/homeslider_workshop_03.jpg",
    "title": "报名参加一场免费健康Lifestyle工作坊吧！",
    "color": "#fff"
  }];

  // Homepage, at least visible area
  function getCarouselHeight() {
    var $homepage = $(".homepage");
    if (!$homepage || $homepage.length <= 0) {
      return CAROUSEL_HEIGHT;
    }

    var viewHeight = $(window).height() || 0;
    var headerHeight = $(".body").position().top || 0;
    var eventHeight = $(".home-event").height() || 0;
    // Slider height
    var carouselHeight = viewHeight - headerHeight - eventHeight;

    if (carouselHeight < CAROUSEL_HEIGHT_MIN) {
      carouselHeight = CAROUSEL_HEIGHT_MIN;
    } else if (carouselHeight > CAROUSEL_HEIGHT) {
      carouselHeight = CAROUSEL_HEIGHT;
    }

    return carouselHeight;
  }

  return Widget.extend({
    "sig/start": function() {
      var me = this;
      var $me = me.$element;

      var domain = context.domain;
      var cacheServer = context.cacheServer;
      var language = context.language;

      var now = new Date();
      var activeEvents = [];
      _.each(EVENTS, function(e, i) {
        // Update cache
        e.src += "?v=" + context.cacheKey;
        // Goto next loop while expired
        if (!e.endDate || now < e.endDate) {
          activeEvents.push(e);
        }
      });

      me.html(template, {
        "events": activeEvents,
        "carouselHeight": getCarouselHeight(),
        "domain": domain,
        "cacheServer": cacheServer,
        "language": language
      }).then(function() {
        $me.find('#home-carousel').carousel({
          interval: CAROUSEL_INTERVAL
        });
      });
    }
  });
});