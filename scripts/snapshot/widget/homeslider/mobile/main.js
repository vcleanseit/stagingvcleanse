define(["troopjs-browser/component/widget",
  "jquery",
  "context",
  "template!./index.html",
  "swipe"
], function(Widget, $, context, template) {
  "use strict";
  
  /*{
    "type": "picture",
    "url": "/m/cleanse/choose#packageid/19",
    "src": "images/home/mobile/homeslider_spring.jpg",
    "title": "维果清祝您新年快乐！购买5天清体疗程，便获得一天礼品卡！",
    "endDate": new Date("2015-02-19 00:00:00")
  }*/
  var EVENTS = [{
    "type": "map",
    "id": "workshopmap",
    "blank": true,
    "url": [{
      "shape": "rect",
      "coords": "250,280,430,340",
      "href": "http://www.wenjuan.com/s/uEnmA3/",
      "alt": "个人报名入口"
    }, {
      "shape": "rect",
      "coords": "440,280,620,340",
      "href": "http://www.wenjuan.com/s/JvI73y/",
      "alt": "团体报名入口"
    }],
    "src": "images/home/mobile/homeslider_workshop.jpg",
    "title": "报名参加一场免费健康Lifestyle工作坊吧！",
    "color": "#fff"
  }, {
    "type": "picture",
    "url": "/juicemonday",
    "src": "images/home/mobile/homeslider_juicemonday.jpg",
    "title": "周一果汁日：请提一下，清凉一夏！"
  }];

  return Widget.extend(function($element, widgetName) {
    var me = this;
  }, {
    "sig/start": function() {
      var me = this;
      var $me = me.$element;

      var domain = context.domain;
      var cacheServer = context.cacheServer;
      var language = context.language;

      var now = new Date();
      var activeEvents = [];
      _.each(EVENTS, function(e, i){
        // Update cache
        e.src +=  "?v=" + context.cacheKey;
        // Goto next loop while expired
        if(!e.endDate || now < e.endDate){
          activeEvents.push(e);
        }
      });

      me.html(template, {
        "events": activeEvents,
        "domain": domain,
        "offsetWidth": document.body.offsetWidth,
        "cacheServer": cacheServer,
        "language": language
      }).then(function() {});

    }
  });
});