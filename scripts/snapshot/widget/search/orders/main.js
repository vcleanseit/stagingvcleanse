define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/infobox/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "template!./index.html",
    "jquery.validation",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, InfoBox, cookie, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders_bymobile");
    var URI_SENDVERIFICATIONCODE = api.get("sendverificationcode");

    var PAGE_COUNT = 30;

    function getStatus(code) {
        var status = [];
        status["1003"] = "1362"; //手机号错误 or 没有订单
        status["1"] = "1187";
        status["10"] = "1181";
        status["11"] = "1182";
        status["12"] = "1183";
        status["13"] = "1184";
        status["-1"] = "1185";
        status["-2"] = "1186";
        status["-3"] = "1188";
        status["-4"] = "1189";
        status["-5"] = "1190";
        status["-6"] = "1191";
        status["-7"] = "1274";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend(function($element, widgetName, pageCount) {
        var me = this;

        me.index = 1;
        me.pageCount = pageCount || PAGE_COUNT;
    }, {
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1345 = $form.find("#blurb-1345").val();
            var m1139 = $form.find("#blurb-1139").val();

            $form.validate({
                rules: {
                    "sendsms-image-verificationcode": "required",
                    "search-verificationcode": "required"
                },
                messages: {
                    "sendsms-image-verificationcode": m1345,
                    "search-verificationcode": m1139
                },
                submitHandler: function(form) {
                    var code = _.trim($form.find("#search-verificationcode").val());
                    if (!code) {
                        return;
                    }

                    me.renderResult(code);
                }
            });
        },
        "renderResult": function(code) {
            var me = this;
            var $me = me.$element;

            // Ajax Query
            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    "mobile": me.mobile,
                    "startIndex": (me.index - 1) * PAGE_COUNT + 1,
                    "count": PAGE_COUNT,
                    "ValidationCode": code,
                    "language": context.language
                },
                type: "GET",
                dataType: "json"
            }).then(function(data) {
                if (!code) {
                    // Publish
                    me.publish("orders/list", {
                        "orders": data.Orders,
                        "disabled": true,
                        "urlDetail": "/order/orderdetail",
                        "pages": Math.ceil(data.Total / me.pageCount),
                        "index": me.index
                    }).then(function() {
                        // Set search mobile to cookie
                        cookie.set("search", me.mobile);
                    });
                } else {
                    me.html(template, {
                        cacheServer: context.cacheServer,
                        serviceDomain: context.serviceDomain,
                        mobile: me.mobile,
                        verified: true
                    }).then(function() {
                        me.publish("orders/list", {
                            "orders": data.Orders,
                            "disabled": true,
                            "urlDetail": "/order/orderdetail",
                            "pages": Math.ceil(data.Total / me.pageCount),
                            "index": me.index
                        }).then(function() {
                            // Set search mobile to cookie
                            cookie.set("search", me.mobile);
                        });
                    });
                }

            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        },
        "sendVerificationCode": function() {
            var me = this;
            var $me = me.$element;

            var $btnSend = $me.find("[data-action=send-verificationcode]");
            var $sendInfo = $btnSend.parent("p");
            var $sending = $me.find(".sending");
            var $countdown = $me.find("[data-action=countdown]");

            var $imageVerification = $(".imageverificationcode");
            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            if ($verificationCode.valid()) {
                var verificationCode = _.trim($verificationCode.val());
                ajaxQuery({
                    url: URI_SENDVERIFICATIONCODE,
                    data: {
                        "mobile": me.mobile,
                        language: context.language,
                        verifyCode: verificationCode
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $("#search-verificationcode").focus();
                    $sendInfo.hide();
                    $imageVerification.hide();
                    $sending.show();
                    var countdownMax = 60;

                    (function countdownSending() {
                        if (countdownMax <= 0) {
                            clearTimeout(insTimeout);

                            $countdown.text("");
                            $sending.hide();
                            refreshSecurity($("[data-action=refreshimagesecurity]"));
                            $imageVerification.show();
                            $sendInfo.show();
                            $btnSend.text(blurb.get("1342"));

                            return;
                        }
                        var insTimeout = setTimeout(function() {
                            $countdown.text(countdownMax);
                            countdownSending();
                        }, 1000);
                        countdownMax--;
                    })();
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            }
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;
            var hashPath = uriPath.path;
            var mobile;
            var index;
            if (!hashPath || hashPath.length < 2) {
                return;
            }

            var hashMap = troopHash.parse(hashPath);

            mobile = hashMap.mobile;
            index = hashMap.page || 1;

            if (!mobile) {
                return;
            }
            // Store in memory
            me.mobile = mobile;
            // Apply to me
            me.index = parseInt(index, 10);

            function renderVerification() {
                me.html(template, {
                    cacheServer: context.cacheServer,
                    serviceDomain: context.serviceDomain,
                    mobile: mobile,
                    verified: false
                }).then(function() {
                    me.validation();
                });
            }

            // Get account from cookie
            cookie.get("search").then(function(mobile) {
                if (me.mobile === mobile) {
                    // Session Cookie, don't verify
                    me.html(template, {
                        cacheServer: context.cacheServer,
                        serviceDomain: context.serviceDomain,
                        mobile: mobile,
                        verified: true
                    }).then(function() {
                        me.renderResult();
                    });
                } else {
                    renderVerification();
                }
            }, function() {
                renderVerification();
            });
        },
        "dom:[data-action=send-verificationcode]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            // Return while hidden
            if (!$(e.currentTarget).is(":visible")) {
                return;
            }

            me.sendVerificationCode();
        },
        "dom:form/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $form = $(e.currentTarget);

            var code = _.trim($form.find("#search-verificationcode").val());

            if (!code) {
                return;
            }

            me.renderResult(code);
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=topay]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            // URL redirect to payment page
            pageUrl.goto("/payment/topay", "orderid/" + orderId);
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});
