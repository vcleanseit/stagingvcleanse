define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/regex",
    "widget/infobox/main",
    "widget/pageurl/main",
    "template!./index.html"
], function(Widget, $, context, regex, InfoBox, pageUrl, template) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            });
        },
        "dom:[data-action=search]/click": function(e) {
            e.preventDefault();

            var $input = $("input[data-action=searchinput]");
            var mobile = $input.val();

            // Validate mobile
            if (!regex.mobile(mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            // Redirect to search page
            pageUrl.goto("/order/orders",
                "mobile/" + mobile);
        }
    });
});