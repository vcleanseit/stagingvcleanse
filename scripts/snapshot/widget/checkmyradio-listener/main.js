define(["troopjs-browser/component/widget",
	"jquery"
], function(Widget, $) {
	"use strict";

	return Widget.extend({
		"hub:memory/checkMyRadio/change": function(for_el_id) {
			var me = this;
			var $me = me.$element;

			var elMe = $me[0];
			if (elMe.id !== for_el_id) {
				return;
			}

			// Auto check input(radio button)
			if ($me.hasClass("icheck")) {
				$me.iCheck('check');
			} else {
				elMe.checked = true;
			}
		}
	});
});