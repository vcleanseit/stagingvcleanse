define(["troopjs-core/component/gadget",
  "jquery",
  "context"
], function(Gadget, $, context) {
  "use strict";

  var WEEK_START_DAY = 1; // Monday

  function geti18n() {
    var lngMap = {
      "en": {
        "week": [
          ['Sunday', 'Sun'],
          ['Monday', 'Mon'],
          ['Tuesday', 'Tue'],
          ['Wednesday', 'Wed'],
          ['Thursday', 'Thu'],
          ['Friday', 'Fri'],
          ['Saturday', 'Sat']
        ]
      },
      "cn": {
        "week": [
          ['星期日', '日'],
          ['星期一', '一'],
          ['星期二', '二'],
          ['星期三', '三'],
          ['星期四', '四'],
          ['星期五', '五'],
          ['星期六', '六']
        ]
      }
    };
    var lng = context.language || "en";
    return lngMap[lng];
  }

  return Gadget.extend({
    "getZeroDate": function(date) {
      // all date should formatted in: 00:00:00
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    },
    "getShortDate": function(date) {
      return (date.getMonth() + 1) + "/" + date.getDate();
    },
    "getWeekStartDate": function(date) {
      var me = this;
      var inst = date.getDay() - WEEK_START_DAY;
      inst = inst >= 0 ? inst : (inst + 7);
      return me.addDay(date, inst * -1);
    },
    "getDayOfWeekFromMonday": function(date) {
      // 0 = Monday; 6 = Sunday;
      return date.getDay() === 0 ? 6 : date.getDay() - 1;
    },
    "getStrDate": function(date, sep) {
      // 2013/10/10
      if (!date) {
        return "";
      }
      // Reinit Date Object
      date = new Date(date);
      sep = sep || "/";
      // all date should formatted in: 2013/10/10
      return date.getFullYear().toString() +
        sep +
        (date.getMonth() + 1).toString() +
        sep +
        date.getDate().toString();
    },
    "getWeekName": function(day, isAbbr) {
      day = (!isNaN(day) && (day < 7)) ? day : 0;
      var i = isAbbr ? 1 : 0;
      var arrWeekName = geti18n().week;
      return arrWeekName[day][i];
    },
    "addDay": function(date, inst) {
      var temp = new Date(date);
      return new Date(temp.setDate(temp.getDate() + inst));
    },
    "addMonth": function(date, inst) {
      var temp = new Date(date);
      return new Date(temp.setMonth(temp.getMonth() + inst));
    },
    "getDateByString": function(strDate) {
      // "2013-09-10" --> 1378771200000 ms
      var d = new Date(strDate);
      d.setHours(0);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);

      return d;
    },
    "compareDate": function(baseDate, toCompareDate) {
      // Compare two dates and returns:
      //  -1 : if toCompareDate < baseDate
      //   0 : if toCompareDate === baseDate
      //   1 : if toCompareDate > baseDate
      // NaN : if baseDate or toCompareDate is an illegal date
      // NOTE: The code inside isFinite does an assignment (=).
      return (
        isFinite(baseDate = baseDate.valueOf()) &&
        isFinite(toCompareDate = toCompareDate.valueOf()) ?
        (toCompareDate > baseDate) - (toCompareDate < baseDate) :
        NaN);
    },
    "between": function(baseDate, fromDate, toDate) {
      var me = this;

      if (me.compareDate(baseDate, fromDate) <= 0 && me.compareDate(baseDate, toDate) >= 0) {
        return true;
      } else {
        return false;
      }
    },
    "dayDiff": function(baseDate, toCompareDate) {
      // Get diff days between two Date
      return parseInt((toCompareDate - baseDate) / (1000 * 60 * 60 * 24), 10);
    },
    "weekDiff": function(baseDate, toCompareDate) {
      // Get diff weeks between two Date
      return parseInt((toCompareDate - baseDate) / (1000 * 60 * 60 * 24 * 7), 10);
    },
    "getNearestWeekdayDate": function(date, wdays) {
      var me = this;
      if (!date) {
        return date;
      }

      var day = date.getDay();
      // Has weekdays, select recent date(the nearest date after startdate) match weekdays
      if (wdays && wdays.length > 0) {
        do {
          date = me.addDay(date, 1);
          day = date.getDay();
        } while (wdays.indexOf(day) < 0);
      }

      return date;
    }
  });
});