define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, api, template) {
    "use strict";

    return Widget.extend(function($element, widgetName, receiverId) {
        var me = this;

        me.receiverId = receiverId || 0;
    }, {
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template, {
                receiverId: me.receiverId
            }).then(function() {
            });
        },
        "hub/receiver/default": function(defaultReceiver) {
            // TODO:
            var me = this;
            var $me = me.$element;
            var $receiverControl = $me.find("[data-action=receivercontrol]");

            if (defaultReceiver) {
                // Set default value.
                me.publish("receiver/changeDeliverAddress", defaultReceiver);
                me.publish("deliverTime/reload", defaultReceiver.AreaId);
                me.publish("expressCompany/reload", defaultReceiver.ExpressCompanyId);

                $receiverControl.show();
            } else {
                $receiverControl.hide();
            }
        },
        // Change to another address
        "dom:[data-action=receivercontrol]/change": function(e, receiver) {
            var me = this;
            var $el = $(e.currentTarget);

            if (!receiver) {
                return;
            }

            me.receiverId = receiver.Id;
            me.publish("receiver/changeDeliverAddress", receiver);
            me.publish("deliverTime/reload", receiver.AreaId);
            me.publish("expressCompany/reload", receiver.ExpressCompanyId);
        },
        // Manage delivery address
        "dom:[data-action=addAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress");
        },
        // Edit selected address
        "dom:[data-action=editAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress", {
                isEdit: true,
                receiverId: me.receiverId
            });
        }
    });
});