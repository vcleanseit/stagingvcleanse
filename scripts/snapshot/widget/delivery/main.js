define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "underscore",
    "json2"
], function(Widget, weave, when, $, context, cookie, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");

    // Time function
    var INS_TIME = new Time();

    var PRICE_DELIVERY = 25;

    function getIsSelfPickup($el) {
        // Hardcode pickup type
        // 1: Select a shipping address
        // 2: Pick up in store
        return $el.data("pickup").toString() === "2";
    }

    return Widget.extend(function() {
        var me = this;
    }, {
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("receiver/hasLogin");
                    }, function() {
                        //
                        me.publish("receiver/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("receiver/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;

            me.render();
        },
        "hub/order/warningNoReceiver": function() {
            var me = this;
            var $me = me.$element;

            var $el = $me.find("[data-action=showErrorMsg-noReceiver]");
            // Show
            $el.show().fadeIn(400);
            // Hide
            setTimeout(function() {
                $el.fadeOut(400, function() {
                    $el.hide();
                });
            }, 3 * 1000);
        },
        // Already login, is a member
        "hub/receiver/hasLogin": function() {
            var me = this;
            me.publish("receiver/refreshAddress");
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");

            $delivery.html("<div data-weave=\"widget/delivery/nonloginaddress/main()\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {});
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            $delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
                $("[data-action=submitorder]").show();
            });
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            $delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
                $("[data-action=submitorder]").hide();
            });
        },
        "hub/order/pickuptype": function() {
            var me = this;
            var $me = me.$element;

            var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.active"));

            var promiseValue = [];
            promiseValue.push(isSelfPickup);
            return promiseValue;
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        "dom:a[data-action=signin]/click": function(e) {
            e.preventDefault();

            var me = this;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        },
        "dom:[data-action=changepickuptype] [data-toggle=tab]/click": function(e) {
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);

            var isSelfPickup = getIsSelfPickup($el.parent("li"));
            if (isSelfPickup) {
                me.publish("order/hideDeliveryTime");
            } else {
                me.publish("order/showDeliveryTime");
            }
        }
    });
});