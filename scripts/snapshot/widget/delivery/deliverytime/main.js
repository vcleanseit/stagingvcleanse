define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, api, template) {
    "use strict";

    var URI_DELIVER_TIME = api.get("deliver_timebyarea");

    return Widget.extend(function($element, widgetName, deliverTimeId) {
        var me = this;

        me.DeliverTimeId = parseInt(deliverTimeId, 10) || 0;
    }, {
        "hub/deliverTime/reload": function(districtId) {
            var me = this;

            if (!districtId) {
                return;
            }

            // GET deliver time
            ajaxQuery({
                url: URI_DELIVER_TIME,
                type: "GET",
                data: {
                    "areaId": districtId,
                    "language": context.language
                },
                dataType: "json"
            }).then(function(dataDeliverTimes) {
                var deliverTime = me.deliverTime = dataDeliverTimes.Values;

                var i = 0;
                var iLens = deliverTime.length;
                var defaultDeliverTime;

                if (me.DeliverTimeId) {
                    for (i = 0; i < iLens; i++) {
                        if (deliverTime[i].Id === me.DeliverTimeId) {
                            deliverTime[i].Selected = true;
                            defaultDeliverTime = deliverTime[i];
                            break;
                        }
                    }

                    if (!defaultDeliverTime) {
                        deliverTime[0].Selected = true;
                        defaultDeliverTime = deliverTime[0];
                    }
                } else {
                    // Choose first one as default deliver time
                    deliverTime[0].Selected = true;
                    defaultDeliverTime = deliverTime[0];
                }
                // Rendering
                me.html(template, {
                    deliverTime: deliverTime
                }).then(function() {
                    // Set default value.
                    me.publish("receiver/changeDeliverTime", defaultDeliverTime);
                });
            });
        },
        // Change to another deliver time span
        "dom:[name=deliverytime]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $e = $(e.currentTarget);

            var selectDeliverTimeId = parseInt($e.val(), 10);

            var i = 0;
            var iLens = me.deliverTime.length;
            for (i = 0; i < iLens; i++) {
                if (parseInt(me.deliverTime[i].Id, 10) === selectDeliverTimeId) {
                    me.publish("receiver/changeDeliverTime", me.deliverTime[i]);
                    break;
                }
            }
        }
    });
});