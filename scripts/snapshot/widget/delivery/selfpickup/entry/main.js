define(["troopjs-browser/component/widget",
    "jquery",
    "widget/cookie/main",
    "template!./index.html",
    "json2"
], function(Widget, $, cookie, template) {
    "use strict";

    var VISIBLE_DATEPICKER = true;
    var VISIBLE_DELIVERYTIME = true;

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template, {
                "isLogin": false
            });
        },
        "hub/receiver/hasLogin": function() {
            var me = this;

            me.html(template, {
                "isLogin": true
            });
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;

            me.html(template, {
                "isLogin": false
            });
        }
    });
});
