define(["troopjs-browser/component/widget",
    "jquery",
    "widget/infobox/main",
    "widget/blurb/main",
    "template!./index.html",
    "jquery.textchange"
], function(Widget, $, InfoBox, Blurb, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    return Widget.extend(function($element, widgetName, id, count, countMin, countMax, price, disable) {
        var me = this;
        // get parameters
        me.id = id;
        me.countDefault = count || 0;
        me.count = count || 0;
        me.countMin = countMin || 0;
        me.countMax = countMax || 0;
        me.price = price || 0;
        me.disable = disable || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                "count": me.count,
                "countMin": me.countMin,
                "countMax": me.countMax,
                "price": me.price,
                "disable": me.disable
            }).then(function() {
                var $number = me.$number = $me.find("input.numbercontroller-input");
                me.$amount = $me.find(".numbercontroller-amount");

                $number.on("textchange", function(e) {
                    var $el = $(e.currentTarget);
                    var eVal = $el.val();

                    if (!eVal) {
                        me.count = me.countDefault;
                        return;
                    }
                    if (isNaN(eVal)) {
                        $el.val(me.count);
                        return;
                    }
                    if (eVal < me.countMin || eVal > me.countMax) {
                        $el.val(me.count);
                        return;
                    }
                    me.count = parseInt(eVal, 10);
                    // Maintain order
                    me.changeProduct();
                });
            });
        },
        "changeProduct": function() {
            var me = this;

            // Amount change
            me.$amount.text(me.count * me.price);
        },
        "hub/numbercontroller/get": function() {
            var me = this;
            var $me = me.$element;

            var postData = {
                "id": me.id,
                "price": me.price,
                "count": me.count
            };

            var promiseValue = [];
            promiseValue.push(postData);
            return promiseValue;
        },
        "dom:.numbercontroller-plus/click": function(e) {
            e.preventDefault();
            var me = this;
            if (me.disable) {
                return;
            }

            if (me.countMax > 0 && me.count > me.countMax - 1) {
                // Show error msg as a lightbox
                new InfoBox({
                    "title": 1144,
                    "content": blurb.get(1204, me.countMax),
                    "overLap": "overlap"
                }).open();
                return;
            }
            me.count++;
            me.$number.val(me.count);
            // Maintain order
            me.changeProduct();
        },
        "dom:.numbercontroller-minus/click": function(e) {
            e.preventDefault();
            var me = this;
            if (me.disable) {
                return;
            }

            if (me.count < me.countMin + 1) {
                return;
            }
            me.count--;
            me.$number.val(me.count);
            // Maintain order
            me.changeProduct();
        }
    });
});