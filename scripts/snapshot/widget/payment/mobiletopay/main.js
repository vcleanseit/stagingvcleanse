define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";
    var URI_ACCOUNT = api.get("account");
    var URI_ORDERDETAIL = api.get("orderdetail");

    var PLATFORM_ALI = 1;
    var PLATFORM_UCF = 2;
    var PLATFORM_SELFPICK = 3;
    var PLATFORM_ALIWAP = 4;
    var PLATFORM_UNION = 5;

    function getPaymentOnline(payments) {
        if (!payments) {
            return -1;
        }

        for (var i = 0, iLens = payments.length; i < iLens; i++) {
            var payment = payments[i];

            if (payment.IsOnlinePay && !payment.IsPaid) {
                return payment.Payment;
            }
        }

        return -1;
    }

    return Widget.extend(function() {
        var me = this;
        me.bank = "";
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = me.orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            var postData = {
                "language": context.language,
                "orderid": orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var dataOrder = data.Order;
                
                var onlinePayment = getPaymentOnline(dataOrder.OrderPayments);

                me.html(template, {
                    "cacheServer": context.cacheServer,
                    "id": dataOrder.OrderID,
                    "hasOnlinePay": onlinePayment >= 0 ? true : false,
                    "onlinePayment": onlinePayment
                });
            });
        },
        "dom:[data-action=confirmpayment]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            var paymentplatform = "";

            paymentplatform = PLATFORM_ALIWAP;
            pageUrl.goto("/payment/pay", null, {
                "orderid": me.orderId,
                "paymentplatform": paymentplatform
            });
        }
    });
});