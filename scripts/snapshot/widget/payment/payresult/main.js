define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./index.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, troopHash, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_EMAILCHECK = api.get("emailcheck");
    var URI_ACCOUNT_UPDATE = api.get("account_update");

    function getStatus(code) {
        var status = [];
        status["-1"] = "1406";

        return status[code] || "";
    }

    function getMessage(id) {
        var lan = context.language;
        var blurbDict = {
            "0": {
                "en": "",
                "cn": ""
            },
            "1": {
                "en": "Sign in",
                "cn": "Order price is error!"
            },
            "2": {
                "en": "Order has expired",
                "cn": "支付订单已经过期"
            },
            "3": {
                "en": "Order is not exists!",
                "cn": "支付订单不存在"
            },
            "4": {
                "en": "trade error",
                "cn": "trade失败"
            },
            "5": {
                "en": "Verify error",
                "cn": "验证失败"
            },
            "6": {
                "en": "No Return params!",
                "cn": "无返回参数"
            }
        };
        return (blurbDict[id] || "0")[lan];
    }

    return Widget.extend(function($element, widgetName, paySuccess) {
        var me = this;

        me.paySuccess = paySuccess;
        me.registerData = {};
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = parseInt(hashMap.orderid, 10);

            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }
            var result = hashMap.result;
            if (result) {
                result = getMessage(result);
            }

            // Signin?
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {

                    if (!data.FirstName) {
                        data.FirstName = "";
                    }
                    if (!data.LastName) {
                        data.LastName = "";
                    }
                    me.registerData = data;

                    // Signin
                    me.html(template, {
                        "cacheServer": context.cacheServer,
                        "signin": true,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId,
                        "result": result,
                        "hasEmail": data.Email ? true : false
                    });
                }, function() {
                    //
                    me.html(template, {
                        "cacheServer": context.cacheServer,
                        "signin": false,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId,
                        "result": result
                    });
                });
            }, function() {
                //
                me.html(template, {
                    "cacheServer": context.cacheServer,
                    "signin": false,
                    "paySuccess": me.paySuccess,
                    "orderId": orderId,
                    "result": result
                });
            });
        },
        "hub:memory/order/checklist": function(order) {
            var me = this;
            var $me = me.$element;

            var $survey = $me.find("[data-action=rendersurvey]");
            if ($survey.length <= 0) {
                return;
            }

            var orderSourceCode = order.OrderSourceCode || "";
            if(!orderSourceCode){
                return;
            }

            if (orderSourceCode.toUpperCase() === "WARM") {
                $survey.show();
            }
        },
        "dom:form[data-action=signin-subscribe-email]/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var $form = $(e.currentTarget);
            var $successLink = $me.find("[data-action=enrollmentsuccess-link]");

            var email = _.trim($form.find("#signup-email").val());

            if (!regex.email(email)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1407
                }).open();

                return;
            }

            _.extend(me.registerData, {
                Email: email
            });

            ajaxQuery({
                url: URI_ACCOUNT_UPDATE,
                data: JSON.stringify(me.registerData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                $form.hide();
                $successLink.show().removeClass("out").addClass("in");
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});