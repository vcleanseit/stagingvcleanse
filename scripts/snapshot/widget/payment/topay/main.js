define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/popupbox/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!./result.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, PopupBox, troopHash, template, templateResult) {
    "use strict";
    var URI_ACCOUNT = api.get("account");
    var URI_ORDERDETAIL = api.get("orderdetail");

    var PLATFORM_ALI = 1;
    var PLATFORM_UCF = 2;
    var PLATFORM_SELFPICK = 3;
    var PLATFORM_ALIWAP = 4;
    var PLATFORM_UNION = 5;

    //
    var QRTYPE_NA = -1;
    var QRTYPE_SIMPREPOSE = 0; //
    var QRTYPE_PREPOSE = 1; //iframe url
    var QRTYPE_SKIP = 2;
    var QRTYPE_MINI = 3;

    function getPaymentOnline(payments) {
        if (!payments) {
            return -1;
        }

        for (var i = 0, iLens = payments.length; i < iLens; i++) {
            var payment = payments[i];

            if (payment.IsOnlinePay && !payment.IsPaid) {
                return payment.Payment;
            }
        }

        return -1;
    }

    return Widget.extend(function() {
        var me = this;
        me.bank = "";
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = me.OrderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            var postData = {
                "language": context.language,
                "orderid": orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var dataOrder = data.Order;

                var onlinePayment = getPaymentOnline(dataOrder.OrderPayments);

                me.html(template, {
                    "cacheServer": context.cacheServer,
                    "orderId": dataOrder.OrderID,
                    "hasOnlinePay": onlinePayment >= 0 ? true : false,
                    "onlinePayment": onlinePayment
                });
            });
        },
        "renderModel": function(isSignin) {
            var me = this;

            var $templateResult = $(templateResult({
                "signin": isSignin
            }));
            // Lightbox configuration
            var popupboxResult = new PopupBox({
                msg: $templateResult,
                closeble: true,
                closeInner: true,
                zIndex: 1050,
                closeButtonList: ['[data-action=close]'],
                closeCallback: function() {}
            });

            popupboxResult.open();
        },
        "showModel": function() {
            var me = this;

            // Signin?
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.renderModel(true);
                }, function() {
                    //
                    me.renderModel(false);
                });
            }, function() {
                //
                me.renderModel(false);
            });
        },
        "dom:[data-action=confirmpayment]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            //var OrderId=$me.find("[data-action=orderid]").data("id");

            var paymentplatform = "";
            var bankId = "";
            var qr = QRTYPE_NA;

            if (!me.bank) {
                // Show payment bank select hint
                $tip.show().addClass("pop");
                setTimeout(function() {
                    $tip.removeClass("pop");
                }, 1000);
                return;
            }

            if (me.bank === "ALIPAY") {
                paymentplatform = PLATFORM_ALI;
            } else if (me.bank === "UCFPAY") {
                paymentplatform = PLATFORM_UCF;
            } else if (me.bank === "ALIPAY_QR") {
                paymentplatform = PLATFORM_ALI;
                qr = QRTYPE_SKIP;
            } else {
                paymentplatform = PLATFORM_ALI;
                bankId = me.bank;
            }

            // Go to bank window
            pageUrl.winopen("/payment/pay", null, {
                "orderid": me.OrderId,
                "paymentplatform": paymentplatform,
                "bank": bankId,
                "qr": qr
            });
            // Show payment result choice popup
            me.showModel();
        },
        "dom:[data-action=selectbank]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $el = $(e.currentTarget);
            var $li = $me.find("ul.banklist > li");
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            // Select bank - UI
            $li.removeClass("active");
            $el.addClass("active");
            // Save selected bank
            me.bank = $el.data("id");
            // Remove payment bank select hint
            $tip.hide();
        },
        "dom:[data-action=collapsebanks]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var cssClass_down = "glyphicon-chevron-down";
            var cssClass_up = "glyphicon-chevron-up";

            var $el = $(e.currentTarget);
            var $elGlyphIcon = $el.find(".glyphicon");
            var $liMore = $("ul.banklist-more > li");
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            if ($elGlyphIcon.hasClass(cssClass_down)) {
                // Extend
                $elGlyphIcon.removeClass(cssClass_down);
                $elGlyphIcon.addClass(cssClass_up);
            } else {
                // Shrink
                $elGlyphIcon.removeClass(cssClass_up);
                $elGlyphIcon.addClass(cssClass_down);
                // Update bank selection
                if ($liMore.filter(".active").length > 0) {
                    me.bank = "";
                    $liMore.removeClass("active");
                    // Show payment bank select hint
                    $tip.show();
                }
            }
        }
    });
});