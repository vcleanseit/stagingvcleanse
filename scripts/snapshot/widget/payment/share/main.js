define(["troopjs-browser/component/widget",
	"jquery",
    "context",
	"template!./index.html"
], function(Widget, $, context, template) {
	"use strict";

	return Widget.extend({
		"hub:memory/order/checklist": function(order) {
			var me = this;
			var $me = me.$element;

			var orderSourceCode = order.OrderSourceCode || "";

			if(!orderSourceCode){
				return;
			}

			// Rendering
			me.html(template, {
	            "domain": context.domain,
	            "cacheServer": context.cacheServer,
				"orderSourceCode": orderSourceCode
			});
		}
	});
});