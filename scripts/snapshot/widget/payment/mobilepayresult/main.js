define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");

    return Widget.extend(function($element, widgetName, paySuccess) {
        var me = this;

        me.paySuccess = paySuccess;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Signin?
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.html(template, {
                        "signin": true,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId
                    });
                }, function() {
                    //
                    me.html(template, {
                        "signin": false,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId
                    });
                });
            }, function() {
                //
                me.html(template, {
                    "signin": false,
                    "paySuccess": me.paySuccess,
                    "orderId": orderId
                });
            });
        }
    });
});