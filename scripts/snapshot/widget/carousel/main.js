define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";
    
    var CAROUSEL_INTERVAL = 5000;

    return Widget.extend(function(){
        var me = this;
    },{
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            $me.carousel({
              interval: CAROUSEL_INTERVAL
            });
        }
    });
});