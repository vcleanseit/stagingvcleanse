define(["troopjs-browser/component/widget",
	"jquery",
	"context",
	"when",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, $, context, when, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_SURVEY_REPORT = api.get("report_survey");

	return Widget.extend(function($element, widgetName, surveyId) {
		var me = this;

		me.surveyId = surveyId || 0;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_SURVEY_REPORT,
				data: JSON.stringify({
					"id": me.surveyId,
					"language": context.language
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}, $me).then(function(data) {
				me.html(template, data.Report);
			}, function(e) {
				me.html(template);
			});
		}
	});
});