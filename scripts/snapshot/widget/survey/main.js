define(["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"when",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, weave, $, context, when, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_LOAD_SURVEY = api.get("load_survey");
	var URI_SAVE_SURVEY = api.get("save_survey");

	var ITEM_TYPE = {
		"SigleSelect": 0,
		"MultiSelect": 1,
		"Text": 2
	};

	return Widget.extend(function($element, widgetName, surveyId) {
		var me = this;

		me.surveyId = surveyId || 0;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_LOAD_SURVEY,
				data: JSON.stringify({
					"id": me.surveyId,
					"language": context.language
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}, $me).then(function(data) {
				if (data.Survey) {
					me.html(template, data.Survey);
				} else {
					$me.empty();
				}
			});
		},
		"collectPostDataItem": function($obj) {
			var me = this;

			if (!$obj) {
				return;
			}
			var itemid = $obj.data("id");
			var itemtype = $obj.data("type");
			var text = "";

			if (!itemid) {
				return;
			}

			if (itemtype === ITEM_TYPE.SigleSelect) {
				var $radio = $obj.find(".radio-inline > input");
				$radio.each(function(i) {
					if ($radio[i].checked) {
						text += $radio[i].value;
					}
				});
			} else if (itemtype === ITEM_TYPE.MultiSelect) {
				var $checkbox = $obj.find(".checkbox-inline > input");
				$checkbox.each(function(i) {
					if ($checkbox[i].checked) {
						text += $checkbox[i].value + ",";
					}
				});
				text = text.replace(/(.*)[,，]$/, '$1');
			} else if (itemtype === ITEM_TYPE.Text) {
				text = $obj.find(".inputtext > textarea").val();
			} else {
				return;
			}

			return text ? {
				"ItemId": itemid,
				"ItemTypeId": itemtype,
				"ResultText": text
			} : null;
		},
		"collectPostData": function() {
			var me = this;
			var $me = me.$element;
			var deferred = when.defer();

			var postData = [];

			var $obj = $me.find("[data-action=surveyitem]");

			$obj.each(function(i, e) {
				var $el = $(e);
				var dataItem = me.collectPostDataItem($el);
				if (dataItem) {
					postData.push(dataItem);
				}
			});

			if (postData.length > 0) {
				deferred.resolve(postData);
			} else {
				deferred.reject();
			}

			return deferred.promise;
		},
		"dom:form/submit": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			me.collectPostData().then(function(postData) {
				var data = {
					"SurveyId": me.surveyId,
					"Details": postData
				};

				// Ajax Query
				ajaxQuery({
					url: URI_SAVE_SURVEY,
					data: JSON.stringify(data),
					type: "POST",
					contentType: "application/json; charset=UTF-8",
					dataType: "json"
				}).then(function(data) {
					var $report = $me.find("[data-action=survey-report]");
					var $form = $me.find("[data-action=survey-form]");

					$report.html("<div data-weave=\"widget/survey/report/main(surveyId)\" data-survey-id=\"" + me.surveyId + "\"></div>");
					weave.apply($report.find("[data-weave]")).then(function() {
						$form.hide();
					});
				});
			}, function() {

			});

		}
	});
});