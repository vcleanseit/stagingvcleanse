define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./index.html",
    "underscore"
], function(Widget, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_EMAILCHECK = api.get("emailcheck");
    var URI_ACCOUNT_UPDATE = api.get("account_update");

    function getStatus(code) {
        var status = [];
        status["-1"] = "1406";

        return status[code] || "";
    }

    return Widget.extend(function() {
        var me = this;

        me.registerData = {};
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Get user account info
            ajaxQuery({
                url: URI_ACCOUNT,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {

                if(!data.FirstName){
                    data.FirstName = "";
                }
                if(!data.LastName){
                    data.LastName = "";
                }
                me.registerData = data;

                // Rendering
                me.html(template, {
                    "cacheServer": context.cacheServer
                }).then(function() {});
            });
        },
        "dom:form/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var $form = $(e.currentTarget);
            var $successLink = $me.find("[data-action=enrollmentsuccess-link]");

            var email = _.trim($form.find("#signup-email").val());

            if(!regex.email(email)){
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1407
                }).open();
                
                return;
            }

            _.extend(me.registerData, {
                Email: email
            });

            ajaxQuery({
                url: URI_ACCOUNT_UPDATE,
                data: JSON.stringify(me.registerData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                $form.hide();
                $successLink.show().removeClass("out").addClass("in");
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});