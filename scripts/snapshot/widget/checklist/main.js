define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, number, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";

    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend({
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                // Format price
                data.Order.Subtotal = number.getDecimal(data.Order.PreferentialPrice + data.Order.PreferentialLogisticsFee);
                // Rendering
                me.html(template, data.Order);
                // Pulish OrderInfo
                me.publish("order/checklist", data.Order);
            });
        }
    });
});