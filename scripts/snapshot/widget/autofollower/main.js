define(["troopjs-browser/component/widget",
  "jquery"
], function(Widget, $) {

  var DELAY = 1000;

  return Widget.extend({
    "sig/start": function() {
      var me = this;
      var $me = me.$element;

      // Check after 400ms, waiting for async content
      setTimeout(function() {
        me.autoFollower();
      }, DELAY);
    },
    "autoFollower": function() {
      var me = this;
      var $me = me.$element;

      // Original position of current element
      var originPostion = $me.position();
      var originTop = originPostion.top;
      var originLeft = originPostion.left;
      var originWidth = $me.width();
      // Add 30px buffer, for x-scroll-bar case
      var originHeight = $me.height() + 30;
      // Parent height
      // * Important: if parent height is full fill with current element,
      // do not auto-follow, because auto-follow will set postion to "fixed",
      // this property will break parent's height
      var parentHeight = $me.parent().height();
      if (parentHeight <= originHeight) {
        return;
      }
      // Calculate scroll max size
      var windowInnerHeight = window.innerHeight;
      // Scroll content height <compare with> window inner height
      var scrollDelta = originHeight - windowInnerHeight;
      scrollDelta = scrollDelta > 0 ? scrollDelta : 0;
      // Apply initial css
      $me.css($.extend(originPostion, {
        // Position when fixed
        "top": scrollDelta * -1,
        "left": originLeft
      }));

      var isPositionFixed = false;
      var scrollBase = 0;

      // Scroll event
      $(window).scroll(function(event) {
        var $w = $(this);
        var scrollTop = $w.scrollTop();
        var scrollLeft = $w.scrollLeft();
        var style = "";

        if (scrollLeft !== 0) {
          // Reset left
          $me.css({
            "left": originLeft - scrollLeft
          });
        }

        // Check every 10px, by this to prevent check every pixel
        // this condition will increase performance
        if (Math.abs(scrollTop - scrollBase) < 10) {
          return;
        }
        scrollBase = scrollTop;

        // Check position while page scroll
        if (scrollTop >= scrollDelta + originTop) {
          if (isPositionFixed) {
            return;
          }
          $me.css({
            "position": "fixed",
            "width": originWidth
          });
          isPositionFixed = true;
        } else {
          if (!isPositionFixed) {
            return;
          }
          style = $me.attr("style").replace("width: " + originWidth + "px", "");
          $me.attr("style", style);
          $me.css({
            "position": "static"
          });
          isPositionFixed = false;
        }
      });
    },
    "hub/autoFollower/resize": function() {
      var me = this;

      me.autoFollower();
    }
  });

});