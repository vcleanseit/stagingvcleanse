define(["jquery",
    "context",
    "poly",
    "underscore.string"
], function($, context) {
    "use strict";

    var urlDict = {
        "/": "/",

        // Public
        "/intl/about": "/intl/about",
        "/intl/codepressed": "/intl/codepressed",
        "/intl/questions": "/intl/questions",
        "/intl/theory": "/intl/theory",
        "/intl/announce": "/intl/announce",

        // Enrollment
        "/enrollment/signin": "/enrollment/signin",
        "/enrollment/register": "/enrollment/register",
        "/enrollment/success": "/enrollment/success",
        "/enrollment/forgotpassword": "/enrollment/forgotpassword",

        // Account
        "/account/basic": "/account/basic",
        "/account/receiver": "/account/receiver",
        "/account/orders": "/account/orders",
        "/account/orderdetail": "/account/orderdetail",
        "/account/logoff": "/account/logoff",
        "/account/safety": "/account/safety",

        // Juice
        "/juice": "/juice",
        "/juice/detail": "/juice/detail",
        "/juice/chailatte": "/juice/chailatte",
        "/juice/chiagreen": "/juice/chiagreen",
        "/juice/classicblack": "/juice/classicblack",
        "/juice/classicgreen": "/juice/classicgreen",
        "/juice/classicwhite": "/juice/classicwhite",
        "/juice/cranberrymilk": "/juice/cranberrymilk",
        "/juice/darkgreen": "/juice/darkgreen",
        "/juice/darkwhite": "/juice/darkwhite",
        "/juice/darkyellow": "/juice/darkyellow",
        "/juice/easygreen": "/juice/easygreen",
        "/juice/fragrantclear": "/juice/fragrantclear",
        "/juice/fragrantred": "/juice/fragrantred",
        "/juice/gingercarrotsoup": "/juice/gingercarrotsoup",
        "/juice/greengoddesssoup": "/juice/greengoddesssoup",
        "/juice/hibiscusinfusion": "/juice/hibiscusinfusion",
        "/juice/intensegreen": "/juice/intensegreen",
        "/juice/luohanguoinfusion": "/juice/luohanguoinfusion",
        "/juice/matchamilk": "/juice/matchamilk",
        "/juice/mushroomsoup": "/juice/mushroomsoup",
        "/juice/passionateyellow": "/juice/passionateyellow",
        "/juice/passionfruit": "/juice/passionfruit",
        "/juice/pearandginger": "/juice/pearandginger",
        "/juice/persimmoninfusion": "/juice/persimmoninfusion",
        "/juice/purplebroth": "/juice/purplebroth",
        "/juice/redbeetsoup": "/juice/redbeetsoup",
        "/juice/spicylemonade": "/juice/spicylemonade",
        "/juice/splitpeasoup": "/juice/splitpeasoup",
        "/juice/strawberryvanilla": "/juice/strawberryvanilla",
        "/juice/sweetpotatopumpkinsoup": "/juice/sweetpotatopumpkinsoup",
        "/juice/vlatte": "/juice/vlatte",

        // Cleanse
        "/cleanse": "/cleanse",
        "/cleanse/detail": "/cleanse/detail",

        // Subscriptions
        "/subscription": "/subscription",

        // Gift cards
        "/giftcard": "/giftcard",

        // Order
        "/order": "/order",
        "/order/receiver": "/order/receiver",
        "/buy/orderinfo": "/buy/orderinfo",
        "/order/confirm": "/order/confirm",
        "/order/success": "/order/success",

        // Search
        "/order/search": "/order/search",
        "/order/orders": "/order/orders",
        "/order/orderdetail": "/order/orderdetail",

        // Payment
        "/payment/topay": "/payment/topay",
        "/payment/pay": "/payment/pay",
        "/payment/paysuccess": "/payment/paysuccess",
        "/payment/payfailed": "/payment/payfailed",

        // Stores
        "/stores": "/stores",

        // Join US
        "/jobs": "/jobs",
        "/jobs/digitalmarketing_en": "/jobs/digitalmarketing_en",
        "/jobs/digitalmarketing_cn": "/jobs/digitalmarketing_cn",
        "/jobs/salesandmarketingassistant_en": "/jobs/salesandmarketingassistant_en",
        "/jobs/salesandmarketingassistant_cn": "/jobs/salesandmarketingassistant_cn",
        "/jobs/salesrepresentative_en": "/jobs/salesrepresentative_en",
        "/jobs/salesrepresentative_cn": "/jobs/salesrepresentative_cn",
        "/jobs/workshopleaderparttime_en": "/jobs/workshopleaderparttime_en",
        "/jobs/workshopleaderparttime_cn": "/jobs/workshopleaderparttime_cn",
        "/jobs/storeassistant_en": "/jobs/storeassistant_en",
        "/jobs/storeassistant_cn": "/jobs/storeassistant_cn",
        "/jobs/supply_en": "/jobs/supply_en",
        "/jobs/supply_cn": "/jobs/supply_cn",
        "/jobs/productmanager_en": "/jobs/productmanager_en",
        "/jobs/productmanager_cn": "/jobs/productmanager_cn",

        // Event
        "/paws": "/paws",
        "/warmingday": "/warmingday",
        "/valentine": "/valentine",
        "/womensday": "/womensday",
        "/moonbear": "/moonbear",
        "/workshop": "/workshop",
        "/workshop/office": "/workshop/office"
    };

    // For Mobile environment
    if (context.isMobile) {
        $.extend(urlDict, {
            "/": "/m/cleanse",

            // Enrollment
            "/enrollment/signin": "/m/enrollment/signin",
            "/enrollment/register": "/m/enrollment/register",
            "/enrollment/success": "/m/enrollment/success",
            "/enrollment/forgotpassword": "/m/enrollment/forgotpassword",

            // Cleanse
            "/cleanse": "/m/cleanse",
            "/cleanse/detail": "/m/cleanse/detail",
            "/cleanse/choose": "/m/cleanse/choose",

            // Gift cards
            "/giftcard": "/m/giftcard",

            // Order
            "/order": "/m/cleanse",
            "/order/receiver": "/m/order/receiver",
            "/order/confirm": "/m/order/confirm",
            "/order/success": "/m/order/success",

            // Payment
            "/payment/topay": "/m/payment/topay",
            "/payment/paysuccess": "/m/payment/paysuccess",
            "/payment/payfailed": "/m/payment/payfailed",

            // Stores
            "/stores": "/m/stores",

            // Join US
            "/jobs": "/m/jobs",
            "/jobs/prmanager_en": "/m/jobs/prmanager_en",
            "/jobs/prmanager_cn": "/m/jobs/prmanager_cn",
            "/jobs/supply_en": "/m/jobs/supply_en",
            "/jobs/supply_cn": "/m/jobs/supply_cn",
            "/jobs/productmanager_en": "/m/jobs/productmanager_en",
            "/jobs/productmanager_cn": "/m/jobs/productmanager_cn",
            "/jobs/onlineoperation_en": "/m/jobs/onlineoperation_en",
            "/jobs/onlineoperation_cn": "/m/jobs/onlineoperation_cn",
            "/jobs/senioruidesigner_en": "/m/jobs/senioruidesigner_en",
            "/jobs/senioruidesigner_cn": "/m/jobs/senioruidesigner_cn",
            "/jobs/socialmediaoperation_en": "/m/jobs/socialmediaoperation_en",
            "/jobs/socialmediaoperation_cn": "/m/jobs/socialmediaoperation_cn",
            "/jobs/salesandmarketingassistant_en": "/m/jobs/salesandmarketingassistant_en",
            "/jobs/salesandmarketingassistant_cn": "/m/jobs/salesandmarketingassistant_cn",
            "/jobs/salesrepresentative_en": "/m/jobs/salesrepresentative_en",
            "/jobs/salesrepresentative_cn": "/m/jobs/salesrepresentative_cn",
            "/jobs/workshopleaderparttime_en": "/m/jobs/workshopleaderparttime_en",
            "/jobs/workshopleaderparttime_cn": "/m/jobs/workshopleaderparttime_cn",
            "/jobs/storeassistant_en": "/m/jobs/storeassistant_en",
            "/jobs/storeassistant_cn": "/m/jobs/storeassistant_cn",

            // Event
            "/paws": "/m/paws",
            "/warmingday": "/m/warmingday",

            // Account
            "/account/orders": "/m/account/orders",
            "/account/orderdetail": "/m/account/orderdetail",
            "/account/logoff": "/m/account/logoff"
        });
    }

    // Get path name by recursive

    function getPathName(key) {
        var isMatch = false;
        while (urlDict[key] && urlDict[key] !== key) {
            key = urlDict[key];
            isMatch = true;
        }
        return isMatch ? key : "";
    }

    return {
        "get": function(key) {
            key = key.toLowerCase();

            var path = getPathName(key);

            return path ? (path.indexOf("http://") >= 0 ? path : (context.domain + path)) : key;
        },
        "reversematch": function(pathName) {
            pathName = pathName.toLowerCase();
            if (context.virtualDirectory) {
                pathName = pathName.replace(context.virtualDirectory, "");
            }

            var keyMatch = "";
            var pathNameLens = pathName.length;

            // Remove last "/"
            if (pathNameLens > 1 && pathName.charAt(pathNameLens - 1) === "/") {
                pathName = pathName.substr(0, pathNameLens - 1);
            }

            // Fill default action like "/xxx/index"
            if (_.endsWith(pathName, "/index")) {
                pathName = _.strLeft(pathName, "/index");
            }

            // Static logic for order package(temp homepage)
            if (pathName === "" || pathName === "/") {
                keyMatch = "/";
            } else {
                for (var key in urlDict) {
                    if (urlDict.hasOwnProperty(key) && urlDict[key] === pathName) {
                        keyMatch = key;
                        break;
                    }
                }
            }

            return keyMatch;
        }
    };
});