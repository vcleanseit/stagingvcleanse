define(["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"widget/troophash/main",
	"template!./index.html"
], function(Widget, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, template) {
	"use strict";

	var URI_PRODUCT_CATEGORY = api.get("product_category");

	return Widget.extend(function($element, widgetName) {
		var me = this;

		me.preventRoute = false;
	}, {
		"hub:memory/route": function(uriPath, uriEvent) {
			var me = this;
			var $me = me.$element;

			var hashPath = uriPath.path || "category/1".split("/");
			var hashMap = troopHash.parse(hashPath);
			var categoryId = parseInt(hashMap.category, 10);

			// prvrt: Prevent hash route event
			if (me.preventRoute) {
				return;
			}

			ajaxQuery({
				url: URI_PRODUCT_CATEGORY,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				var categories = data.Categories;

				// Set default
				var i = 0;
				var iLens = categories.length;
				for (i = 0; i < iLens; i++) {
					if (categories[i].Id === categoryId) {
						categories[i].IsDefault = true;
					}
				}

				// Render
				me.html(template, {
					"categories": categories
				}).then(function() {});
			});
		},
		"dom:a[data-toggle=tab]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;
			var $el = $(e.currentTarget);

			var categoryId = $el.data("id");

			//return;
			me.preventRoute = true;
			troopHash.extend({
				"category": categoryId
			});
		}
	});
});