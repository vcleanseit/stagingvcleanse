define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, template) {
    "use strict";

    var URI_PRODUCT = api.get("product_loadbycategory");

    return Widget.extend(function($element, widgetName, categoryId) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            ajaxQuery({
                url: URI_PRODUCT,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var Products = data.Products;

                me.html(template, {
                    "products": Products,
                    "cacheServer": context.cacheServer,
                    "language": context.language
                }).then(function () {

                });
            });

        }
    });
});