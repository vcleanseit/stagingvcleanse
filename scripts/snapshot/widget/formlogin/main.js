define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/querystring/main",
    "template!./index.html",
    "jquery.validation",
    "jquery.formatter",
    "json2",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, InfoBox, pageUrl, cookie, queryString, template) {
    "use strict";

    var URI_LOGIN = api.get("login");

    function getStatus(code) {
        var status = [];
        status["-2"] = "1178";
        status["0"] = "1179";
        status["-10"] = "1180";

        return status[code] || "";
    }

    return Widget.extend(function($element, widgetName, redirect) {
        var me = this;

        me.redirect = redirect;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var req = queryString.getRequest();
            if(req.referrer){
                me.redirect = req.referrer;
            }

            // Rendering
            me.html(template).then(function() {
                var $form = $me.find("form");
                var $account = $form.find('[name=login-account]');
                // Get account from cookie
                cookie.get("account").then(function(account) {
                    // Auto filled account
                    $account.val(account);
                });
                // Validation
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1271 = $form.find("#blurb-1271").val();
            var m1272 = $form.find("#blurb-1272").val();

            $form.validate({
                rules: {
                    "login-account": {
                        required: true
                    },
                    "login-password": "required"
                },
                messages: {
                    "login-account": {
                        required: m1271
                    },
                    "login-password": m1272
                },
                submitHandler: function(form) {
                    var account = _.trim($form.find("[name=login-account]").val());
                    var password = _.trim($form.find("[name=login-password]").val());
                    var loginData = {
                        email: account,
                        password: password
                    };
                    // Post data
                    ajaxQuery({
                        url: URI_LOGIN,
                        data: loginData,
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Set account to cookie
                        cookie.set("account", account);
                        
                        // 用于前端测试用的memberid写入cooike
                        if(data.IsFrontendTest !== undefined && data.IsFrontendTest === true){
                        	cookie.set("memberid", data.MemberId,{
                                path: "/"
                            });
                        }
                        
                        if (me.redirect) {
                            // Direct to Homepage after login
                            pageUrl.goto(me.redirect);
                        } else {
                            // Reload current page
                            window.location.reload(true);
                        }
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();
                    });
                }
            });
        }
    });
});