define(["jquery",
    "widget/popupbox/main",
    "template!./index.html"
], function($, PopupBox, template) {
    "use strict";

    var TYPE_SUCCESS = "success";
    var TYPE_INFO = "info";
    var TYPE_WARNING = "warning";
    var TYPE_DANGER = "danger";

    return PopupBox.extend(function(args) {
        var me = this;

        me.el = args.$el || "body";

        me.position = args.position || "fixed";
        me.zIndex = args.zIndex || 20;

        me.closeble = (typeof args.closeble === "undefined") ? true : args.closeble;
        me.closeInner = true;
        me.closeButtonList = ['[data-action=close]'];
        me.overLap = "overlap";
        var closeCallback = args.closeCallback || $.noop;
        // TODO: 
        me.closeCallback = function() {
            // Callback from arguments
            closeCallback();
            // Destory self
            me.stop();
        };

        var info = {
            type: (args.type || TYPE_INFO).toLowerCase(),
            title: args.title || "",
            content: args.content || "",
            closeble: me.closeble
        };
        me.msg = template(info);
        
    });
});