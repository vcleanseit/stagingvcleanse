define(["jquery",
	"when",
	"jquery.cookie"
], function($, when) {
	"use strict";

	// Cookie dictionary, use lowercase for all key
	var cookieDict = {
		"account": "Account", // user name
		"memberid": "MemberId", // user id
		"lng": "Language",
		"pkg_group_1": "Pkg_group_1", // ~ detox (male / female)
		"pkg_group_2": "Pkg_group_2", // ~ slim (male / female)
		"pkg_group_3": "Pkg_group_3", // ~ health (male / female)
		"pkg_group_5": "Pkg_group_5", // ~ detox (male / female)
		"pkg_group_6": "Pkg_group_6", // ~ slim (male / female)
		"pkg_group_7": "Pkg_group_7", // ~ health (male / female)
		"pkg_group_8": "Pkg_group_8", // ~ detox (male / female)
		"pkg_group_9": "Pkg_group_9", // ~ slim (male / female)
		"pkg_group_10": "Pkg_group_10", // ~ health (male / female)
		"hide_announcement": "Hide_announcement",
		"customize": "Customize",
		"subscription_cust": "Subscription_cust",
		"search": "Search",
		"gift_info": "Gift_info"
	};

	return {
		"set": function(key, value, opt) {
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			if (value === undefined) {
				return when.reject("value is undefined");
			}

			key = cookieDict[key.toLowerCase()];
			if (!key) {
				return when.reject("key is not registed");
			}

			$.cookie(key, value, opt);

			return when.resolve();
		},
		"get": function(key) {
			//Read all available cookies
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			key = cookieDict[key.toLowerCase()];

			var val = key ? $.cookie(key) : "";

			return val ? when.resolve(val) : when.reject("No cookie for " + key);
		},
		"getVal": function(key) {
			//Read all available cookies
			if (key === undefined) {
				return;
			}
			key = cookieDict[key.toLowerCase()];

			var val = key ? $.cookie(key) : "";

			return val;
		},
		"getAll": function() {
			return when.resolve($.cookie());
		},
		"rm": function(key, opt) {
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			key = cookieDict[key.toLowerCase()];
			if (!key) {
				return when.reject("key is not registed");
			}

			$.removeCookie(key, opt);

			return when.resolve();
		}
	};

});