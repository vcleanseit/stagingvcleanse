define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    var MYCONTENT_CLASS = ".showmycontent-content";
    var INFO_ACCOUNT = ".info-account";
    var MYCONTENT_DISAPPEAR_SEC = 0.2 * 1000;

    return Widget.extend(function($element, widgetName, dir) {
        var me = this;

        me.dir = dir || "b";
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            // Prevent events bubble to $me
            $myContent.click(function(e) {
                e.stopPropagation();
            });
        },
        "dom/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            if ($myContent.length <= 0) {
                return;
            }

            me.hideOtherLoginForm();

            if ($myContent.hasClass('Actived')) {
                $myContent.removeClass('Actived');
            } else {
                $myContent.addClass('Actived');
            }

            var contentWidth = $myContent.outerWidth();
            var contentHeight = $myContent.outerHeight();
            var eventWidth = $me.outerWidth();
            var eventHeight = $me.outerHeight();
            // Position reset
            switch (me.dir) {
                default:
                case "b":
                    $myContent
                        .css("left", -1 * (contentWidth - eventWidth) / 2)
                        .css("top", eventHeight);
                    break;
                case "t":
                    $myContent
                        .css("left", -1 * (contentWidth - eventWidth) / 2)
                        .css("top", -1 * (contentHeight));
                    break;
            }
            // Show or Hide Content
            $myContent.toggle();
        },
        "dom/mouseleave": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            if ($myContent.length <= 0) {
                return;
            }
            if (me.loginFormIsActive()) {
                return;
            }
            setTimeout(function() {
                $myContent.hide();
            }, MYCONTENT_DISAPPEAR_SEC);
        },
        "loginFormIsActive": function() {
            var me = this;
            var $me = me.$element;
            var $myContent = $me.find(MYCONTENT_CLASS);
            var isActive = false;
            $myContent.find("input").each(function(i) {
                if ($(this).is(":focus")) {
                    isActive = true;
                }
            });
            return isActive;
        },
        "hideOtherLoginForm": function() {
            var me = this;
            var $me = me.$element;
            $(INFO_ACCOUNT).find(MYCONTENT_CLASS).each(function(i) {
                if ($(this).hasClass('Actived')) {
                    $(this).removeClass('Actived');
                    $(this).hide();
                }
            });
        }
    });
});