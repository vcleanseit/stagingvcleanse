define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders");

    var PAGE_COUNT = 10;

    return Widget.extend(function($element, widgetName, pageCount) {
        var me = this;

        me.index = 1;
        me.pageCount = pageCount || PAGE_COUNT;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath) || {
                "page": 1
            };
            var index = hashMap.page || 1;

            // Apply to me
            me.index = parseInt(index, 10);

            // Ajax
            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    "startIndex": (me.index - 1) * me.pageCount + 1,
                    "count": me.pageCount,
                    "language": context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                // Rendering
                me.html(template).then(function(){
                    // Publish
                    me.publish("orders/list", {
                        "orders": data.Orders,
                        "disabled": false,
                        "urlDetail": "/account/orderdetail",
                        "pages": Math.ceil(data.Total / me.pageCount),
                        "index": me.index
                    });
                });
            });
        }
    });
});