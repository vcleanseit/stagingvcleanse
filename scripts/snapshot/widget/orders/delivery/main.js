define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/popupbox/main",
    "widget/infobox/main",
    "template!./index.html"
], function(Widget, weave, when, $, ajaxQuery, api, PopupBox, InfoBox, template) {
    "use strict";

    var DELIVERY_ADDRESS;
    var DELIVERY_TIME;

    var URI_ORDER_UPDATERECEIVER = api.get("order_updatereceiver");

    function getStatus(code) {
        var status = [];
        status["-100"] = "1470"; //参数错误
        status["-1"] = "1470"; //参数不正确
        status["-300"] = "1471"; //运费增加
        status["-400"] = "1469"; //地区不配送
        return status[code] || "";
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "hub/orders/changeDeliveryAddress/popup": function(orderId, dailyOrderId, deliveryTimeId) {
            var me = this;

            me.dailyOrderId = dailyOrderId;
            me.orderId = orderId;

            // Create template
            var $me = $(template({
                "deliveryTimeId": deliveryTimeId
            }));

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("changeDeliveryAddress/updateOrder");
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open().then(function() {
                me.publish("receiver/refreshAddress");
            });
        },
        "hub/receiver/changeDeliverAddress": function(receiver) {
            var me = this;

            if (!receiver) {
                return;
            }

            DELIVERY_ADDRESS = receiver;
        },
        "hub/receiver/changeDeliverTime": function(deliverTime) {
            var me = this;

            if (!deliverTime) {
                return;
            }

            DELIVERY_TIME = deliverTime;
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            $me.find("[data-action=tweakdeliverytime]").hide();
            var $delivery = $me.find("[data-action=changedeliveryaddress-container]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            $delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
                me.popupOrderEdit.centralize();
            });
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";

            $me.find("[data-action=tweakdeliverytime]").show();
            var $delivery = $me.find("[data-action=changedeliveryaddress-container]");
            $delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
                me.popupOrderEdit.centralize();
            });
        },
        "dom:[data-action=changedeliveryaddress-save]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var postData = {
                "OrderId": me.orderId,
                "SubOrderId": me.dailyOrderId,
                "ReceiverId": DELIVERY_ADDRESS.Id,
                "DeliveryTimeId": DELIVERY_TIME.Id
            };

            ajaxQuery({
                url: URI_ORDER_UPDATERECEIVER,
                data: JSON.stringify(postData),
                contentType: "application/json; charset=UTF-8",
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                // Close popup
                me.popupOrderEdit.close().then(function() {
                    me.publish("order/detail/update");
                });
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});