define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/infobox/main",
    "template!./index.html"
], function(Widget, $, context, ajaxQuery, api, Blurb, InfoBox, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_ORDER_DELETE = api.get("order_delete");

    function getStatus(code) {
        var status = [];
        status["1"] = "1624"; //没有权限
        status["2"] = "1624"; //无法删除

        return status[code] || "";
    }

    return Widget.extend(function(){
        var me = this;

        me.index = 0;
    },{
        "hub/orders/list": function(data) {
            var me = this;

            me.index = data.index || 1;
            // Rendering
            return me.html(template, data);
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "page/" + index;
        },
        "dom:[data-action=deleteorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            if (confirm(blurb.get(1248) || "Sure to delete this order?")) {
                // Delete order by orderId
                // ...
                ajaxQuery({
                    url: URI_ORDER_DELETE,
                    data: {
                        orderId: orderId,
                        language: context.language
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("tr[data-id=" + orderId + "]").hide("slow", function() {
                        $(this).remove();
                    });
                }, function(e){
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            }
        }
    });
});