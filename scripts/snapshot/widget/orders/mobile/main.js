define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/cookie/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
], function(Widget, weave, $, ajaxQuery, api, cookie, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");

    return Widget.extend({
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("mine/hasLogin");
                    }, function() {
                        //noLogin
                        me.publish("mine/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("mine/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.render();
        },
        // Already login, is a member
        "hub/mine/hasLogin": function() {
            var me = this;
            var $me = me.$element;

            var $orders = $me.find("[data-action=myorders]");

            $orders.html("<div data-weave=\"widget/orders/mobile/orders/main()\"></div>");
            // Manually weave template
            weave.apply($orders.find("[data-weave]")).then(function() {});
        },
        // Render no login
        "hub/mine/nonLogin": function() {
            var me = this;
            var $me = me.$element;

            var $orders = $me.find("[data-action=myorders]");

            $orders.html('<div data-weave="widget/orders/mobile/nonlogin/main()"></div>');
            // Manually weave template
            weave.apply($orders.find("[data-weave]")).then(function() {});
        }
    });
});