define(["troopjs-browser/component/widget",
    "jquery",
    "widget/ajaxquery/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
], function(Widget, $, ajaxQuery, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                me.publish("enrollment/signin/popup");
            });
        },
        "dom:[data-action=signin]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        }
    });
});