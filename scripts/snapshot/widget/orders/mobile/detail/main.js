define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/popupbox/main",
    "widget/pageurl/main",
    "widget/orders/delivery/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!../../../map/index.html",
    "jquery.cookie",
    "json2"
], function(Widget, $, context, ajaxQuery, api, PopupBox, pageUrl, Delivery, troopHash, template, templateMap) {
    "use strict";

    (new Delivery($(document.body))).start();
    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend(function($element, widgetName, editble) {
        var me = this;

        me.editble = editble || false;
    }, {
        "render": function(orderId) {
            var me = this;
            var $me = me.$element;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                me.html(template, {
                    "order": data.Order,
                    "editble": me.editble
                });
            });
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            var orderId = me.orderId = hashMap.orderid;
            // orderId is required
            // or will direct to: account/orders.html
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Render
            me.render(orderId);
        },
        "hub/order/detail/update": function() {
            var me = this;
            var orderId = me.orderId;

            // Render
            me.render(orderId);
        },
        "dom:[data-action=changeDeliveryAddress]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);
            var dailyOrderId = parseInt($el.data("ordersmId"), 10);

            // Popup order-edit lightbox.
            me.publish("orders/changeDeliveryAddress/popup", dailyOrderId);
        }
    });
});