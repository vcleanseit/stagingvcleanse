define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders");
    var URI_ORDER_DELETE = api.get("order_delete");
    var PAGE_COUNT = 10;

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.index = 1;
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath) || {
                "page": 1
            };
            var index = hashMap.page || 1;

            // Apply to me
            me.index = parseInt(index, 10);

            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    startIndex: (me.index - 1) * PAGE_COUNT + 1,
                    count: PAGE_COUNT,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {

                me.html(template, {
                    orders: data.Orders,
                    pages: Math.ceil(data.Total / PAGE_COUNT),
                    index: me.index
                });

            });
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "page/" + index;
        },
        "dom:[data-action=topay]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            // URL redirect to payment page
            pageUrl.goto("/payment/topay", "orderid/" + orderId);
        },
        "dom:[data-action=deleteorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            if (confirm(blurb.get(1248) || "Sure to delete this order?")) {
                // Delete order by orderId
                // ...
                ajaxQuery({
                    url: URI_ORDER_DELETE,
                    data: {
                        orderId: orderId,
                        language: context.language
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("tr[data-id=" + orderId + "]").hide("slow", function() {
                        $(this).remove();
                    });
                });
            }
        }
    });
});