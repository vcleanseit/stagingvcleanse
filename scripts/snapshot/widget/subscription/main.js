define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/blurb/main",
    "widget/time/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!./summary.html",
    "underscore"
], function(Widget, weave, $, context, cookie, Blurb, Time, pageUrl, InfoBox, troopHash, template, templateSummary) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // Time function
    var INS_TIME = new Time();

    var MONTH_WEEKS = 4;
    var MONTH_DURATION = 1;

    function parseStr(arr, sep) {
        if (!arr || arr.length <= 0) {
            return;
        }
        sep = sep || ",";
        var strArr = "";
        var iLens = arr.length;

        _.each(arr, function(e, i) {
            strArr += e;
            if (i < iLens - 1) {
                strArr += sep;
            }
        });

        return strArr;
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;

        // Default plan
        me.gender = 2;
        me.weekDays = [1];
        me.deliveryStartDate = null;
        me.deliveryEndDate = null;
        me.duration = MONTH_DURATION * MONTH_WEEKS;
        // Initial package
        me.packageInfo = {};
        me.packageId = 0;
        me.packageName = "-";
        me.packageGroupId = 0;
        // Daily price, not contain addon
        me.dailyPrice = 0;
        me.addon = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Render
            me.html(template, {
                "monthDuration": MONTH_DURATION
            }).then(function() {
                // Pre settings
                /*troopHash.extend({
                    "wdays": 1
                });*/
                me.publish("startdate/set", {
                    "weekDays": "1"
                });

                // Reset products
                me.publish("subscription/gender/render", me.gender);
            });
        },
        "hub/subscription/summary/update": function() {
            var me = this;
            var $me = me.$element;

            // Summary
            var $summary = $me.find("[data-action=summarycontainer]");
            $summary.html(templateSummary({
                "gender": me.gender,
                "weekDays": me.weekDays,
                "deliveryStartDate": INS_TIME.getStrDate(me.deliveryStartDate),
                "deliveryEndDate": INS_TIME.getStrDate(me.deliveryEndDate),
                "duration": me.duration,
                "packageId": me.packageId,
                "packageName": me.packageName,
                "language": context.language
            }));
            weave.apply($summary.find("[data-weave]"));

            // Amount
            var $amount = $me.find("[data-action=showtotalmoney]");
            var days = me.weekDays.length * me.duration;
            var dailyPrice = me.dailyPrice;
            if (me.addon.length > 0) {
                _.each(me.addon, function(e, i) {
                    dailyPrice += e.price;
                });
            }
            var amount = days * dailyPrice;
            $amount.html("&yen; " + amount);
        },
        // Plan
        "dom:[data-action=changeweekdays]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $checkedWeekDays = $me.find("input[data-action=changeweekdays]:checked");
            me.weekDays = [];

            $checkedWeekDays.each(function(i, e) {
                var weekDay = parseInt(e.value, 10);
                me.weekDays.push(weekDay);
            });

            // Only first week day is available
            var startWeekDay = -1;

            if (me.weekDays.length > 0) {
                startWeekDay = me.weekDays.join(",");
            }
            // Reset startdate
            /*troopHash.extend({
                "wdays": startWeekDay
            });*/
            me.publish("startdate/set", {
                "weekDays": startWeekDay
            });

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            me.deliveryStartDate = args.selectedDate;

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/deliveryEndDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryEndDate = me.deliveryEndDate = args.selectedDate;

            // Head + Tail = +1
            me.duration = INS_TIME.weekDiff(me.deliveryStartDate, deliveryEndDate) + 1;

            // Update
            me.publish("subscription/summary/update");
        },
        /*
        "dom:[data-action=changeduration]/change": function(e) {
            e.preventDefault();
            var me = this;

            // 4 weeks per month
            me.duration = e.currentTarget.value * MONTH_WEEKS;

            // Update
            me.publish("subscription/summary/update");
        },*/
        "hub/subscription/gender/update": function(gender) {
            var me = this;

            me.gender = gender;

            // Update dailyprice which related to gender
            if (me.packageId !== 0) {
                me.publish("subscription/package/update", me.packageInfo).then(function() {
                    // Update
                    me.publish("subscription/summary/update");
                });
            } else {
                // Update
                me.publish("subscription/summary/update");
            }
        },
        // Package
        "hub/subscription/packagecategory/change": function(packageCategoryId) {
            var me = this;
            var $me = me.$element;

            switch (packageCategoryId) {
                case 0:
                    me.packageInfo = {};
                    me.packageId = 0;
                    me.packageName = blurb.get("p1004");
                    me.packageGroupId = 0;
                    me.dailyPrice = 0; // TODO:
                    me.addon = [];

                    // Reset products
                    me.publish("subscription/customize/init").then(function() {
                        // Update
                        me.publish("subscription/summary/update");
                    });

                    break;
                default:
                    me.packageInfo = {};
                    me.packageId = 0;
                    me.packageName = "-";
                    me.packageGroupId = 0;
                    me.dailyPrice = 0;
                    me.addon = [];

                    // Reset Gender
                    me.publish("subscription/gender/render", me.gender);
                    // Reset Addon
                    me.publish("subscription/addon/reset");

                    break;
            }
        },
        "hub/subscription/addon/update": function(addon) {
            var me = this;

            me.addon = addon || [];

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/subscription/package/update": function(packageInfo) {
            var me = this;

            if (!packageInfo) {
                return;
            }

            me.packageInfo = packageInfo;
            me.packageId = packageInfo.Id;
            me.packageName = packageInfo.Title;
            me.packageGroupId = packageInfo.Groups[me.gender - 1].Id;
            // First day price as daily price
            me.dailyPrice = packageInfo.Groups[me.gender - 1].Rules[0].PackagePrice;

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/subscription/customize/update": function(dailyProducts) {
            var me = this;
            var dailyPrice = 0;
            _.each(dailyProducts, function(e, i) {
                dailyPrice += e.Price * e.Count;
            });

            // Reset daily price
            me.dailyPrice = dailyPrice;
            // Set customize order data to cookie
            cookie.set("subscription_cust", JSON.stringify(dailyProducts), {
                path: "/"
            });

            // Update
            me.publish("subscription/summary/update");
        },
        // Submit
        "dom:[data-action=submitsubsciption]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            // Without day & duration
            if (!me.weekDays || me.weekDays.length <= 0 || me.duration <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: "1144",
                    content: "p1030"
                }).open();

                return;
            }
            // Without package/product
            if (me.dailyPrice <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: "1144",
                    content: "p1031"
                }).open();

                return;
            }

            var hash = "packageid/" + me.packageId +
                "/groupid/" + me.packageGroupId +
                "/weekDays/" + parseStr(me.weekDays) +
                "/deliveryStartDate/" + INS_TIME.getStrDate(me.deliveryStartDate, "-") +
                "/duration/" + me.duration;

            // Handle addon
            var i = 0;
            var iLens = 0;
            var addonIdArr = [];
            if (me.addon && me.addon.length > 0) {
                iLens = me.addon.length;
                for (i = 0; i < iLens; i++) {
                    addonIdArr.push(me.addon[i].id);
                }
                hash = hash + "/addon/" + parseStr(addonIdArr);
            }

            // Source Code
            hash += "/event/subs";

            pageUrl.goto("/order/receiver", hash);
        }
    });
});