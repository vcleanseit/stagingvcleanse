define(["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    // 1: male
    // 2: female
    var GENDER_DEFAULT = 2;

    return Widget.extend(function() {
        var me = this;
    }, {
        "render": function(gender){
            var me = this;
            var $me = me.$element;

            // Default: female
            gender = gender || GENDER_DEFAULT;

            // Render
            me.html(template, gender).then(function(){
                $me.find("[data-toggle=popover]").popover();
            });
        },
        "sig/start": function() {
            var me = this;
            
            me.render();
        },
        "hub/subscription/gender/render": function(gender) {
            var me = this;

            me.render(gender);
        },
        "dom:[data-action=changegender]/change": function(e) {
            e.preventDefault();
            var me = this;

            var gender = parseInt(e.currentTarget.value, 10);

            me.publish("subscription/gender/update", gender);
        }
    });
});