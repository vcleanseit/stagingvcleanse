define(["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend(function() {
        var me = this;

        me.addon = [];
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
            });
        },
        "dom:[data-action=changeaddon]/ifChanged": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            me.update({
                "id": parseInt($e.data("id"), 10),
                "price": parseInt($e.data("price"), 10)
            }, e.currentTarget.checked);
        },
        "hub/subscription/addon/reset": function(gender) {
            var me = this;
            var $me = me.$element;

            $me.find('.icheck').iCheck('uncheck');
        },
        "update": function(data, isAppend) {
            var me = this;
            var $me = me.$element;

            var i = 0;
            var iLens = me.addon.length;

            if (isAppend) {
                me.addon.push(data);
            } else {
                for (i = 0; i < iLens; i++) {
                    if (me.addon[i].id === data.id) {
                        me.addon.splice(i, 1);
                        break;
                    }
                }
            }

            me.publish("subscription/addon/update", me.addon);
        }
    });
});