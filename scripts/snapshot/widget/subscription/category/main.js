define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
    "underscore"
], function(Widget, weave, $, context, ajaxQuery, api, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    var URI_PACKAGE_CATEGORY = api.get("package_category");
    // Own program, packageId = 0, groupId = 0
    var PACKAGE_CUSTOMIZE = {
        "Id": 0,
        "Name": blurb.get(1155),
        "Description": blurb.get(1352)
    };

    return Widget.extend(function() {
        var me = this;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path || "category/1".split("/");
            var hashMap = troopHash.parse(hashPath);

            // Set defatul = 2, warming package first
            var categoryId = parseInt(hashMap.category, 10) || 2;
            var col = parseInt(hashMap.col, 10) || 1;

            // Prevent reload while other hash changes
            if (me.categoryId && me.col && categoryId === me.categoryId && me.col === col) {
                return;
            }
            me.categoryId = categoryId;
            me.col = col;

            ajaxQuery({
                url: URI_PACKAGE_CATEGORY,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var categories = data.Categories;
                // Append customize package
                categories.push(PACKAGE_CUSTOMIZE);

                // Set default
                var i = 0;
                var iLens = categories.length;
                for (i = 0; i < iLens; i++) {
                    if (categories[i].Id === categoryId) {
                        categories[i].IsDefault = true;
                    }
                }

                // Render
                me.html(template, {
                    "categories": categories,
                    "col": col
                }).then(function() {
                    me.publish("subscription/packagecategory/change", categoryId);
                });
            });
        },
        // Package
        "dom:[data-action=changepackagetab]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);

            var packageCategoryId = parseInt($e.data("categoryId"), 10);

            me.publish("subscription/packagecategory/change", packageCategoryId);
        }
    });
});