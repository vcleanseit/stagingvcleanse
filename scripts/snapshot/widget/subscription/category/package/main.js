define(["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "json2"
], function(Widget, when, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_PACKAGE_BYCATEGORY = api.get("package_loadbycategory");

    return Widget.extend(function($element, widgetName, categoryId, col) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
        me.col = col || 1;

        me.deferred = when.defer();
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            if (!me.categoryId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE_BYCATEGORY,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = me.packages = data.Packages;

                // Render
                me.html(template, {
                    "packages": packages,
                    "cacheServer": context.cacheServer,
                    "language": context.language,
                    "col": me.col
                }).then(function(){
                    me.deferred.resolver.resolve();
                });
            });
        },
        "hub/subscription/packagecategory/change": function(packageCategoryId) {
            var me = this;

            if (me.categoryId !== packageCategoryId) {
                return;
            }

            me.deferred.promise.then(function() {
                // Choose first package as default
                me.publish("subscription/package/update", me.packages[me.col - 1]);
            });
        },
        "dom:[data-action=selectpackage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagecontainer]");

            $packageContainer.siblings().removeClass("active");
            $packageContainer.addClass("active");

            // Package data
            var packageId = parseInt($packageContainer.data("packageId"), 10);

            var packages = me.packages;
            var i = 0;
            var iLens = packages.length;

            var iPackage;
            for (i = 0; i < iLens; i++) {
                if (packages[i].Id === packageId) {
                    iPackage = packages[i];
                    break;
                }
            }

            // publish
            me.publish("subscription/package/update", iPackage);
        },
        "dom:[data-action=showcleansedetail]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeIn(200);
        },
        "dom:[data-action=showcleansedetail]/mouseleave": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeOut(200);
        }
    });
});