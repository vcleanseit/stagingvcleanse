define(["troopjs-browser/component/widget",
  "jquery",
  "template!./index.html"
], function(Widget, $, template) {
  "use strict";

  return Widget.extend(function() {
    var me = this;

    me.dailyProducts = [];
  }, {
    "hub/subscription/customize/init": function() {
      var me = this;

      return me.html(template).then(function() {
        // Get products
        me.publish("orderProduct/get");
      });
    },
    // Products change
    "hub/customize/changeProduct": function(args) {
      var me = this;

      // {
      //     id: xxx,
      //     price: xxx,
      //     count: xxx
      // }
      if (!args) {
        return;
      }
      var id = args.id;
      var price = args.price;
      var count = args.count;

      var j = 0;
      var jLens = me.dailyProducts.length;

      var isNewProduct = true;

      // Already has products
      if (jLens) {
        for (j = 0; j < jLens; j++) {
          if (me.dailyProducts[j].Id === id) {
            isNewProduct = false;

            // Update only when products count > 0
            if (count > 0) {
              me.dailyProducts[j].Count = count;
            } else {
              // Remove current product
              me.dailyProducts.splice(j, 1);
            }
            break;
          }
        }
        if (isNewProduct && count > 0) {
          // count should always > 0
          me.dailyProducts.push({
            "Id": id,
            "Price": price,
            "Count": count
          });
        }
      } else if (count > 0) {
        // count should always > 0
        me.dailyProducts.push({
          "Id": id,
          "Price": price,
          "Count": count
        });
      }

      // publish
      me.publish("subscription/customize/update", me.dailyProducts);
    }
  });
});