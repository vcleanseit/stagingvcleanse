define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/pageurl/main",
    "widget/time/main",
    "widget/infobox/main",
    "widget/troophash/main",
    "template!./index.html",
    "jquery.ui.datepicker",
    "jquery.ui.datepicker-zh-CN",
    "underscore"
], function(Widget, $, when, context, cloneObject, pageUrl, Time, InfoBox, troopHash, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 10;

    // Month duration
    // Default: 1 month
    var MONTH_RANGE = 1;

    // Official holiday
    var HOLIDAY = [
        // Note: Month start from 0
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
		new Date(2016, 3, 3),
		new Date(2016, 3, 4),
    ];

    function geti18n() {
        var lngMap = {
            "en": "",
            "cn": "zh-CN"
        };
        var lng = context.language || "en";
        return lngMap[lng];
    }

    function transformDateArray(DateArray) {
        if (!DateArray && DateArray.length <= 0) {
            return;
        }

        var disableDays = [];
        _.each(DateArray, function(e) {
            // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
            // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
            var d = INS_TIME.addDay(e, 1);
            disableDays.push(INS_TIME.getStrDate(d, "-"));
        });
        return disableDays;
    }

    return Widget.extend(function($element, widgetName, range, rangeMonth) {
        var me = this;

        me.range = range || false;
        me.rangeMonth = 0;
        if (range) {
            // Default: 3 months
            me.rangeMonth = parseInt(rangeMonth, 10) || MONTH_RANGE;
        }

        me._holiday = HOLIDAY;
    }, {
        "sig/start": function() {
            var me = this;
        },
        // 饮用周期大于1天时，需要考虑假期是否在饮用周期内
        "preForbidDate": function(days) {
            var me = this;

            days = days || 1;
            if (!me._holiday && me._holiday.length <= 0) {
                return;
            }
            var holidayStartDate = me._holiday[0];
            var holidayMax = cloneObject(me._holiday);
            var i = 1;
            for (i = 1; i < days; i++) {
                holidayMax.unshift(INS_TIME.addDay(holidayStartDate, i * -1));
            }
            /* create an array of days which need to be disabled */
            me._disableDays = transformDateArray(holidayMax);
        },
        // wdays[array]
        "preForbidWeekDays": function(wdays) {
            var me = this;

            if (!wdays) {
                return;
            }

            var arrWeekDays = [0, 1, 2, 3, 4, 5, 6];

            if (wdays.length > 0) {
                me._disableWeekDays = _.difference(arrWeekDays, wdays);
            } else {
                me._disableWeekDays = [];
            }
        },
        "getAvailableStartDate": function(hourAhead, days, wdays) {
            var me = this;

            hourAhead = hourAhead || HOUR_AHEAD;
            days = days || 1;
            var now = new Date();
            var dateNow = INS_TIME.getZeroDate(now);

            var hour = now.getHours();
            var startDate = hour >= hourAhead ? INS_TIME.addDay(dateNow, 2) : INS_TIME.addDay(dateNow, 1);
            var endDate = INS_TIME.addDay(startDate, days - 1);

            if (me._holiday && me._holiday.length > 0) {
                // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
                // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
                (function() {
                    var i = 0;
                    var iLens = me._holiday.length;
                    var holiday;

                    for (i = 0; i < iLens; i++) {
                        holiday = INS_TIME.addDay(INS_TIME.getZeroDate(me._holiday[i]), 1);

                        // startdate <= holiday <= enddate
                        // move startdate to the day after holiday
                        if (INS_TIME.compareDate(holiday, startDate) <= 0 && INS_TIME.compareDate(holiday, endDate) >= 0) {
                            // Reset dateRange
                            startDate = INS_TIME.addDay(holiday, 1);
                            endDate = INS_TIME.addDay(startDate, days - 1);
                            continue;
                        }
                    }
                })();
            }

            // Has weekdays, select recent date(the nearest date after startdate) match weekdays
            return INS_TIME.getNearestWeekdayDate(startDate, wdays);
        },
        // 不配送日期
        "offDays": function(date, disableWeekdays) {
            var me = this;

            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();
            var weekday = date.getDay();

            // Disable weedays, union preForbid & argument
            disableWeekdays = disableWeekdays || [];
            disableWeekdays = _.union(disableWeekdays, me._disableWeekDays);

            if (me._disableDays && me._disableDays.length > 0) {
                if ($.inArray(y + '-' + (m + 1) + '-' + d, me._disableDays) != -1 || new Date() > date) {
                    return [false];
                }
            }
            if (disableWeekdays && disableWeekdays.length > 0) {
                if ($.inArray(weekday, disableWeekdays) != -1 || new Date() > date) {
                    return [false];
                }
            }

            return [true];
        },
        "render": function(dateAvailable, extOption, disabled) {
            var me = this;
            var $me = me.$element;

            // Extend
            if (extOption && extOption.minDate) {
                dateAvailable = extOption.minDate;
            }

            // Initialize start date from today
            me.publish("deliveryStartDate/change", {
                // Date info
                "availableStartDate": dateAvailable,
                "selectedDate": dateAvailable
            });

            // Prepare for rendering
            var dataOption = {
                "dateFrom": INS_TIME.getStrDate(dateAvailable, "-"),
                "disabled": disabled,
                "language": context.language,
                "range": me.range
            };

            var dateTo;
            var weekDaysLens = me._arrDays.length || 0;
            var arrLastWeekDay = [];
            if (me.range) {
                if (weekDaysLens > 0) {
                    arrLastWeekDay.push(me._arrDays[weekDaysLens - 1]);
                }
                dateTo = INS_TIME.getNearestWeekdayDate(INS_TIME.addMonth(dateAvailable, me.rangeMonth),
                    arrLastWeekDay);
                _.extend(dataOption, {
                    "dateTo": INS_TIME.getStrDate(dateTo, "-")
                });

                // Initialize end date from today
                me.publish("deliveryEndDate/change", {
                    // Date info
                    "selectedDate": dateTo
                });
            }

            // Rendering
            me.html(template, dataOption).then(function() {
                // Generate datepicker by jQuery-UI
                $(document.body).addClass("jquery-ui-lightness");
                // Localize
                $.datepicker.setDefaults($.datepicker.regional[geti18n()]);
                // datepicker options
                var datepickerOptionFrom = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+3m",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryStartDate/change", {
                            // Date info
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "availableStartDate": dateAvailable,
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                var datepickerOptionTo = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+1y",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryEndDate/change", {
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                if (extOption) {
                    datepickerOptionFrom = _.extend(datepickerOptionFrom, extOption);
                    datepickerOptionTo = _.extend(datepickerOptionTo, extOption);
                }

                var $datepickerFrom = $me.find("[data-action=datepickerfrom]");
                var $datepickerTo;

                if (me.range) {
                    $datepickerTo = $me.find("[data-action=datepickerto]");

                    // Extend datepicker options
                    $.extend(datepickerOptionFrom, {
                        "beforeShowDay": function(date) {
                            // Only first active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.rest(me._arrDays)) : me.offDays(date);
                        },
                        "onSelect": function(strDate, inst) {
                            me.publish("deliveryStartDate/change", {
                                // Date info
                                // All date format to "xxxx-xx-xx 00:00:00"
                                "availableStartDate": dateAvailable,
                                "selectedDate": INS_TIME.getDateByString(strDate)
                            }).then(function() {
                                // Focus datepicker to element
                                $datepickerTo.focus();
                            });
                        },
                        "onClose": function(selectedDate) {
                            $datepickerTo.datepicker("option", "minDate", selectedDate);
                        }
                    });
                    $.extend(datepickerOptionTo, {
                        "beforeShowDay": function(date) {
                            // Only last active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.initial(me._arrDays)) : me.offDays(date);
                        },
                        "onClose": function(selectedDate) {
                            $datepickerFrom.datepicker("option", "maxDate", selectedDate);
                        }
                    });

                    // Datepicker To
                    $datepickerTo.datepicker(datepickerOptionTo);
                }

                // Datepicker From
                $datepickerFrom.datepicker(datepickerOptionFrom);
            });
        },
        // eventCode[string]
        // days[int]
        // wdays[array]
        "process": function(hourAhead, eventCode, days, wdays) {
            var me = this;

            var dateAvailable;
            var dateEvent;
            var dateEventEnd;
            var dateNow = INS_TIME.getZeroDate(new Date());

            days = days || 1; // 1 day cleanse

            // Forbit date before Holiday while multi days
            me.preForbidDate(days);

            // Forbit days in week
            if (wdays && wdays.length > 0) {
                me.preForbidWeekDays(wdays);
            }

            // Available first date for order
            // First weekdays is a special requirement from subscription
            var firstWeekday = [];
            if (wdays && wdays.length > 0) {
                firstWeekday.push(wdays[0]);
            }
            dateAvailable = me.getAvailableStartDate(hourAhead, days, firstWeekday);

            // Switch package
            switch (eventCode.toUpperCase()) {
                case "CFAC": // Clease for a Cause
                    dateEvent = new Date(2014, 7, 30);
                    dateEventEnd = new Date(2014, 7, 30);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, true);
                    }
                    break;
                case "WARM": // Warming Day
                    dateEvent = new Date(2014, 11, 16);
                    dateEventEnd = new Date(2015, 0, 4);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, false);
                    }
                    break;
                case "VALE": // Valentine's Day
                    dateEvent = new Date(2015, 1, 11);
                    dateEventEnd = new Date(2015, 1, 18);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "WOMN": // Women's Day
                    dateEvent = new Date(2015, 2, 3);
                    dateEventEnd = new Date(2015, 2, 9);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "BEAR": // Moonbear
                    dateEvent = new Date(2014, 9, 10);
                    if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        });
                    }
                    break;
                case "BRDL": // Bridal
                    if (dateAvailable.getDay() !== 1) {
                        dateEvent = INS_TIME.addDay(dateAvailable, 8 - dateAvailable.getDay());
                    } else {
                        dateEvent = dateAvailable;
                    }
                    // Rendering
                    me.render(dateAvailable, {
                        "minDate": dateEvent,
                        "beforeShowDay": function(date) {
                            return offDays(date, [0, 2, 3, 4, 5, 6]);
                        }
                    });
                    break;
                default:
                    // Rendering
                    me.render(dateAvailable);
                    return;
            }
        },
        // Special/Event order listner
        /*"hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath);

            var eventCode = hashMap.event || "";
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.wdays === wdays &&
                me.hourAhead === hourAhead
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.wdays = wdays;
            me.hourAhead = hourAhead;

            var arrPreDays;
            var arrDays = [];

            if (wdays) {
                arrPreDays = wdays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(hourAhead, eventCode, days, arrDays);
        },
        */
        "hub:memory/startdate/set": function(dateOption) {
            var me = this;

            if (!dateOption) {
                return;
            }

            var eventCode = dateOption.eventCode || "";
            var days = parseInt(dateOption.days, 10) || 1;
            var weekDays = (dateOption.weekDays || "").toString();
            var hourAhead = dateOption.hourAhead || HOUR_AHEAD;
            var holiday = dateOption.holiday || HOLIDAY;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.weekDays === weekDays &&
                me.hourAhead === hourAhead &&
                me._holiday.length == holiday.length &&
                _.difference(holiday, me._holiday).length <= 0
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.weekDays = weekDays;
            me.hourAhead = hourAhead;
            me._holiday = holiday;

            var arrPreDays;
            var arrDays = [];

            if (weekDays) {
                arrPreDays = weekDays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(me.hourAhead, me.eventCode, me.days, me._arrDays);

            return when.resolve();
        }
    });
});