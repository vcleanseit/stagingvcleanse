define(["troopjs-browser/component/widget",
    "jquery",
    "widget/sitemap/main",
    "underscore"
], function(Widget, $, sitemap) {
    "use strict";

    return Widget.extend(function($element, widgetName, path) {
        var me = this;

        // Use "-" to prevent "/"
        path = path || "-";
        me.pathList = path.split("/");
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var pathName = location.pathname;
            var pathKey = sitemap.reversematch(pathName);

            if (!pathKey) {
                return;
            }

            // Array[0]: ""
            // Array[1]: main menu
            // Array[2]: sub menu
            var arrPath = pathKey.split("/");

            if (_.indexOf(me.pathList, arrPath[1]) < 0) {
                return;
            }

            // Active current page path
            $me.addClass("active").siblings().removeClass("active");
        },
        "dom/click": function(e) {
            var $e = $(e.currentTarget);
            var target = $e.data("target");

            // link
            if (!target) {
                return;
            }

            e.preventDefault();

            // Show target
            var $target = $(target);
            $target.fadeIn();
            $target.find("> ul").css("bottom", "50px");

            // Cancel event: hide target
            $target.on("click", "[data-action=cancelsetting]", function(e) {
                e.preventDefault();

                $target.find("> ul").css("bottom", "-250px");
                setTimeout(function() {
                    $target.fadeOut();
                }, 100);
            });
        }
    });
});