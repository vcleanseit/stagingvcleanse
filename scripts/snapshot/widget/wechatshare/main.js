define(["troopjs-browser/component/widget",
	"jquery",
	"widget/popupbox/main",
	"widget/querystring/main",
	"template!./index.html"
], function(Widget, $, PopupBox, queryString, template) {
	"use strict";

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			// Load order detail, so ignore order step
			var request = queryString.getRequest();
			if (!request) {
				pageUrl.goto("/");
			}

			var isShare = (request.share === "yes") ? true : false;

			if (!isShare) {
				return;
			}

			// Lightbox configuration
			me.html(template);
		},
		"dom:[data-action=wechatshare-cover]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			$me.remove();
		}
	});
});