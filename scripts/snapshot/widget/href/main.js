define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/sitemap/main"
], function(Widget, $, context, sitemap) {
    "use strict";

    return Widget.extend(function($element, widgetName, byLanguage) {
        var me = this;
        me.byLanguage = byLanguage || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var href = $me.attr("href");
            var query = $me.data("query");
            var hash = $me.data("hash");

            if (!href ||
                href.indexOf("http://") >= 0 ||
                href.indexOf("https://") >= 0
            ) {
                return;
            }

            // Attach Language
            if (me.byLanguage) {
                href = href + "_" + context.language;
            }

            href = sitemap.get(href);
            if (query) {
                href += "?" + query;
            }
            if (hash) {
                href += "#" + hash;
            }

            // Reset href
            $me.attr("href", href);
        }
    });
});