define(["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/cookie/main",
	"widget/pageurl/main",
	"jquery.textchange"
], function(Widget, $, context, cookie, pageUrl) {
	"use strict";

	return Widget.extend(function() {
		var me = this;

		// gift / self
		me.typeCode = "gift";
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			var $sense = $me.find(".warmingday-sense");
			if (context.language === "cn") {
				$sense.slideDown();
			} else {
				$sense.hide();
			}
		},
		"dom:a[data-action=buywarmingday]/click": function(e) {
			// Do not preventDefault
			var me = this;
			var $me = me.$element;

			var $blessing;
			var blessing = "";

			if (me.typeCode === "gift") {
				$blessing = $me.find("[data-action=inputblessing]");
				blessing = $blessing.val() || "";

				cookie.set("gift_info", JSON.stringify({
					"isGift": true,
					"blessing": blessing
				}), {
					expires: 7,
					path: "/"
				});
			} else {
				cookie.rm("gift_info", {
					path: "/"
				});
			}
		},
		"dom:#carousel-example-generic/slide.bs.carousel": function() {
			var me = this;
			var $me = me.$element;

			var index = $me.find(".carousel-indicators li.active").index();
			var $sense = $me.find(".warmingday-sense");
			var $control = $me.find("#carousel-example-generic .carousel-control");
			var lang = context.language;

			// Self
			if (index === 0) {
				$control.eq(0).fadeIn();
				$control.eq(1).hide();
				$sense.slideUp();

				// IsGift?
				me.typeCode = "self";
			}
			// Gift
			else {
				$control.eq(0).hide();
				$control.eq(1).fadeIn();
				if (lang === "cn") {
					$sense.slideDown();
				} else {
					$sense.slideUp();
				}

				// IsGift?
				me.typeCode = "gift";
			}
		}
	});
});