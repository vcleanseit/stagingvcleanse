define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "dom/click": function(e) {
            e.preventDefault();
            var me = this;

            var obj_ID = me.$element.data("for");
            me.publish("checkMyRadio/change", obj_ID);
        }
    });
});