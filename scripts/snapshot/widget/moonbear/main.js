define(["troopjs-browser/component/widget",
	"jquery",
	"widget/cookie/main",
	"widget/pageurl/main",
	"template!./index.html"
], function(Widget, $, cookie, pageUrl, template) {
	"use strict";

	return Widget.extend(function($element, widgetName, isBlock){
		var me = this;

		me.isBlock = isBlock;
	},{
		"sig/start": function() {
			var me = this;

			me.html(template, {
				"isBlock": me.isBlock
			});
		},
		//data-weave="widget/href/main" href="/order/receiver" data-hash="packageid/17/groupid/27/days/30/addon/0,0" 
		"dom:button/click": function(e) {
			e.preventDefault();

			var me = this;
			var deliveryList = [];

			// Publish submit order event
			me.publish("numbercontroller/get").spread(function(dataNumber) {
				deliveryList.push({
					"IsDeliver": true,
					"PackageProducts": [],
					"Products": [dataNumber]
				});

				// Set customize order data to cookie
				cookie.set("customize", JSON.stringify(deliveryList), {
					path: "/"
				});

				// Next step
				pageUrl.goto("/order/receiver",
					"packageid/" + 0 +
					"/groupid/" + 0 +
					"/event/bear");
			});
		}
	});
});