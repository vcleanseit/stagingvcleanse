define(["troopjs-browser/component/widget",
	"jquery",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, $, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_SUBSCRIBE = api.get("subscribe");

	function getStatus(code) {
		var status = [];
		status["-1"] = "1181";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName, isInternal) {
		var me = this;

		me.isInternal = isInternal || false;
	}, {
		"sig/start": function() {
			var me = this;

			me.html(template);
		},
		"dom:form[data-action=anonymous-subscribe-email]/submit": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			var $form = $(e.currentTarget);
			var $successLink = $me.find("[data-action=emailsubscribe-success]");
			var email = _.trim($form.find("[data-action=subscribe-email]").val());

			if (!regex.email(email)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1407
				}).open();

				return;
			}
			ajaxQuery({
				url: URI_SUBSCRIBE,
				data: JSON.stringify({
					"email": email
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}).then(function(data) {
				$form.hide();
				$successLink.show().removeClass("out").addClass("in");
			}, function(e) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: getStatus(e)
				}).open();
			});
		}
	});
});