define(["jquery"], function($) {
    "use strict";

    return {
        "parse": function(hashPath) {
            if (!hashPath || hashPath.length <= 0) {
                return false;
            }

            var hashMap = {};

            var i = 0;
            var iLens = hashPath.length;

            for (i = 0; i < iLens; i += 2) {
                hashMap[hashPath[i]] = hashPath[i + 1];
            }

            return hashMap;
        },
        "extend": function(hashExt) {
            var me = this;

            if (!hashExt) {
                return;
            }
            var hash = location.hash || "";
            // #package/1/packagegroup/1
            var hashMap = hash ? me.parse(hash.substr(1).split("/")) : {};

            // Extend
            _.extend(hashMap, hashExt);

            var hashKey;
            var hashVal;
            var hashStr = "";
            for (hashKey in hashMap) {
                hashVal = hashMap[hashKey];
                if (hashVal || !isNaN(hashVal)) {
                    hashStr += "/" + hashKey + "/" + hashMap[hashKey];
                }
            }

            // Refresh hash
            location.hash = hashStr.substr(1);
        }
    };
});