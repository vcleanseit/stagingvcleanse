define(["widget/select/main",
  "jquery",
  "context",
  "widget/api/main",
  "widget/blurb/main",
  "widget/ajaxquery/main",
  "widget/infobox/main",
  "template!../index.html",
  "json2",
  "underscore"
], function(Select, $, context, api, Blurb, ajaxQuery, InfoBox, template) {
  "use strict";

  var URI_RECEIVER = api.get("receiver");
  var blurb = new Blurb($(document.body));

  function getAddressString(address) {
    if (!address) {
      return "";
    }
    var strAddress = "";

    if (context.language === "en") {
      if (address.FirstName) {
        strAddress += address.FirstName;
      }
      if (address.LastName) {
        strAddress += " " + address.LastName;
      }
      strAddress += ",";
    } else {
      if (address.LastName) {
        strAddress += address.LastName;
      }
      if (address.FirstName) {
        strAddress += " " + address.FirstName;
      }
      strAddress += "，";
    }

    strAddress += " " + address.CityName;
    strAddress += " " + address.AreaName;
    if (address.Line1) {
      strAddress += " " + address.Line1;
    }
    if (address.Line2) {
      strAddress += " " + address.Line2;
    }
    // Deliverble
    if(!address.IsActive){
      strAddress += " (" + blurb.get(1469) + ")";
    }

    return strAddress;
  }

  return Select.extend(function($element, widgetName, receiverId, disabled) {
    var me = this;

    me.receiverId = parseInt(receiverId, 10);
    me.disabled = disabled;
  }, {
    "sig/start": function() {
      var me = this;

      ajaxQuery({
        url: URI_RECEIVER,
        data: {
          "t": (new Date()).getTime()
        },
        type: "GET",
        dataType: "json"
      }).then(function(data) {

        var defaultReceiver;

        // With/Without receiver
        if (data.Receivers && data.Receivers.length > 0) {
          _.each(data.Receivers, function(receiver) {
            // Set "Name"
            receiver.Name = getAddressString(receiver);
            // Find default receiver
            if (me.receiverId) {
              if (receiver.Id === me.receiverId) {
                defaultReceiver = receiver;
              }
            } else if (receiver.IsDefault) {
              // Set receiverId
              me.receiverId = receiver.Id;
              defaultReceiver = receiver;
            }
          });

          // Store options for trigger event
          me.options = data.Receivers;

          // Rendering
          me.html(template, {
            "options": me.options,
            "selectedId": me.receiverId,
            "disabled": me.disabled
          }).then(function() {
            // Init default selected option
            me.reset(me.receiverId);
          });
        }

        me.publish("receiver/default", defaultReceiver);

      });
    }
  });
});