define(["widget/select/main",
	"jquery",
	"widget/select/region/helper",
	"template!../../index.html"
], function(Select, $, helper,template) {
	"use strict";

	var INSTANCE_ID = 0;

	return Select.extend(function($element, widgetName, districtId, disabled) {
		var me = this;

		INSTANCE_ID++;
		me.instance_id = INSTANCE_ID;
		me.districtId = parseInt(districtId, 0);
		me.disabled = disabled;
	}, {
		"hub:memory/region/district/reload": function(msg) {
			var me = this;
			var $me = me.$element;

			if (msg.id === me.instance_id) {

				if (msg.data && msg.data.length > 0) {
                    // Option group changed
					if (!(_.find(msg.data, function(e, i) {
						return e.Id === me.districtId;
					}))) {
						// Do not select a default area, force to select one
						me.districtId = 0;//msg.data[0].Id;
					}
				}
				$me.data("value", me.districtId);

				// Store options for trigger event
				me.options = msg.data;

				// Rendering
				me.html(template, {
					"options": me.options,
					"selectedId": me.districtId,
					"disabled": me.disabled
				}).then(function(){
                    // Trigger event, apply value to parent
                    $me.find("[data-action=changeoption]").trigger("change");
				});
			}
		}
	});
});