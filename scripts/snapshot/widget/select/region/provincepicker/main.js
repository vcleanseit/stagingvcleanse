define(["widget/select/main",
    "jquery",
    "widget/select/region/helper",
    "template!../../index.html"
], function(Select, $, helper, template) {
    "use strict";

    var INSTANCE_ID = 0;

    return Select.extend(function($element, widgetName, provinceId, disabled) {
        var me = this;

        INSTANCE_ID++;
        me.instance_id = INSTANCE_ID;
        me.provinceId = parseInt(provinceId, 0);
        me.disabled = disabled;
    }, {
        "hub:memory/region/province/reload": function(msg) {
            var me = this;
            var $me = me.$element;

            if (msg.data && msg.data.length > 0) {
                // Option group changed
                if (!(_.find(msg.data, function(e, i) {
                    return e.Id === me.provinceId;
                }))) {
                    // Do not select a default area, force to select one
                    me.provinceId = 0; //msg.data[0].Id;
                }
            }
            $me.data("value", me.provinceId);

            // Store options for trigger event
            me.options = msg.data;

            // Rendering
            me.html(template, {
                "options": me.options,
                "selectedId": me.provinceId,
                "disabled": me.disabled
            }).then(function() {
                // Trigger event, apply value to parent
                $me.find("[data-action=changeoption]").trigger("change");
            });
        },
        "dom:select/change": function(e, fnCallback) {
            e.preventDefault();
            e.stopPropagation();

            var me = this;

            var provinceId = parseInt(e.currentTarget.value, 10);

            me.publish("region/city/reload", {
                id: me.instance_id,
                data: helper.getCitiesByProvinceId(me.options, provinceId)
            }).then(fnCallback);
        }
    });
});