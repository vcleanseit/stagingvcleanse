define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "when",
    "context",
    "widget/api/main",
    "widget/ajaxquery/main",
    "widget/select/region/helper",
    "template!./index.html",
    "json2"
], function(Widget, weave, $, when, context, api, ajaxQuery, helper, template) {
    "use strict";

    var URI_REGION_PROVINCE = api.get("region_with_province");

    return Widget.extend(function($element, widgetName, provinceId, cityId, areaId, disabled) {
        var me = this;

        me.provinceId = parseInt(provinceId, 10);
        me.cityId = parseInt(cityId, 10);
        me.areaId = parseInt(areaId, 10);
        me.disabled = disabled || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Ajax
            ajaxQuery({
                "url": URI_REGION_PROVINCE,
                "data": {
                    "language": context.language,
                    "version": context.cacheKey
                },
                "type": "GET",
                "dataType": "json"
            }).then(function(data) {
                me.provinces = data.Provinces;
                me.currCities = helper.getCityByCityId(me.provinces, me.cityId);

                if (me.provinceId === 0) {
                    me.provinceId = helper.getProvinceIdByCityId(me.provinces, me.cityId);
                }

                me.html(template, {
                    "provinceId": me.provinceId,
                    "cityId": me.cityId,
                    "areaId": me.areaId,
                    "disabled": me.disabled
                }).then(function() {
                    me.publish("region/province/reload", {
                        data: me.provinces
                    });
                });
            });
        },
        "dom:[data-action=updatecityarea]/change": function(e, distict) {
            e.preventDefault();
            e.stopPropagation();

            var me = this;
            var $me = me.$element;

            if (!distict) {
                return;
            }

            var districtId = distict.Id;

            var provinces = me.provinces;
            var i = 0;
            var iLens = provinces.length;

            var cities;
            var j = 0;
            var jLens = 0;

            var districts;
            var k = 0;
            var kLens = 0;

            // Default Region
            var region = {
                Province: {
                    Id: 0,
                    Name: ""
                },
                City: {
                    Id: 0,
                    Name: ""
                },
                District: {
                    Id: 0,
                    Name: ""
                }
            };

            for (i = 0; i < iLens; i++) {
                cities = provinces[i].Cities;
                jLens = cities.length;
                for (j = 0; j < jLens; j++) {
                    districts = cities[j].District;
                    kLens = districts.length;
                    for (k = 0; k < kLens; k++) {
                        if (districts[k].Id === districtId) {
                            region.Province = {
                                "Id": provinces[i].Id,
                                "Name": provinces[i].Name
                            };
                            region.City = {
                                "Id": cities[j].Id,
                                "Name": cities[j].Name
                            };
                            region.District = {
                                "Id": districts[k].Id,
                                "Name": districts[k].Name,
                                "ExpressCompanyId": districts[k].ExpressCompanyId
                            };
                        }
                    }
                }
            }

            //
            $me
                .data("value", region)
                .trigger("reset", region);
        }
    });
});