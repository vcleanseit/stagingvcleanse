define(["jquery"], function($) {
    "use strict";

    return {
        "getCityByCityId": function(provinces, cityId) {
            var i = 0;
            var iLens = provinces.length;
            var j = 0;
            var jLens = 0;
            var cities = {};
            var province = {};

            for (i; i < iLens; i++) {
                province = provinces[i];
                cities = province.Cities;
                j = 0;
                jLens = cities.length;
                for (j; j < jLens; j++) {
                    if (cities[j].Id === cityId) {
                        return cities;
                    }
                }
            }
            return {};
        },
        "getDistrictsByCityId": function(city, cityId) {
            var i = 0;
            var iLens = city.length;
            for (i; i < iLens; i++) {
                if (city[i].Id === cityId) {
                    return city[i].District;
                }
            }
        },
        "getCitiesByProvinceId": function(provinces, provinceId) {
            var i = 0;
            var iLens = provinces.length;
            for (i; i < iLens; i++) {
                if (provinces[i].Id === provinceId) {
                    return provinces[i].Cities;
                }
            }
        },
         "getProvinceIdByCityId": function(provinces, cityId) {
            var i = 0;
            var iLens = provinces.length;
            var j = 0;
            var jLens = 0;
            var cities = {};
            var province = {};

            for (i; i < iLens; i++) {
                province = provinces[i];
                cities = province.Cities;
                j = 0;
                jLens = cities.length;
                for (j; j < jLens; j++) {
                    if (cities[j].Id === cityId) {
                        return province.Id;
                    }
                }
            }
            return 0;
        },
    };
});