define(["troopjs-browser/component/widget",
  "jquery",
  "underscore"
], function(Widget, $) {
  "use strict";

  return Widget.extend({
    "copyName": function(srcKey) {
      var me = this;

      if (!me.options || me.options.length <= 0) {
        return;
      }

      // Copy
      _.each(me.options, function(el, i) {
        el.Name = el[srcKey];
      });
    },
    "reset": function(selectedId) {
      var me = this;
      var $me = me.$element;

      if(!selectedId){
        return;
      }

      // Get selected option(object)
      var selectedOption = null;
      if (me.options && me.options.length > 0) {
        _.each(me.options, function(el, i) {
          if (el.Id === selectedId) {
            selectedOption = el;
            return;
          }
        });
      }

      // Set value to parent element
      // Trigger parent element customize "change" event
      $me
        .data("value", selectedId)
        .trigger("change", selectedOption);
    },
    "dom:[data-action=changeoption]/change": function(e) {
      e.preventDefault();

      var me = this;
      var $me = me.$element;

      var val = e.currentTarget.value;

      // Get selected option(object)
      var selectedOption = null;
      if (me.options && me.options.length > 0) {
        _.each(me.options, function(el, i) {
          if (el.Id.toString() === val) {
            selectedOption = el;
            return;
          }
        });
      }

      // Set value to parent element
      // Trigger parent element customize "change" event
      $me
        .data("value", val)
        .trigger("change", selectedOption);
    }
  });

});