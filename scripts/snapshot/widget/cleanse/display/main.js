define(["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "template!./display.html"
], function(Widget, when, $, context, cookie, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "hub/package/display": function(packageGroup) {
            var me = this;

            // Rendering
            me.render(packageGroup);
        },
        "render": function(group) {
            var me = this;

            var rules = group.Rules;
            if (!rules || rules.length <= 0) {
                return;
            }

            // Rendering
            me.html(template, {
                "cacheServer": context.cacheServer,
                "byWeek": group.ByWeek,
                "rules": rules
            });
        }
    });
});