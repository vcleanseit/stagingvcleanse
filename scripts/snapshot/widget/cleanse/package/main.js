define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/popupbox/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, number, ajaxQuery, api, pageUrl, cookie, troopHash, PopupBox, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");
    var URI_PACKAGE = api.get("package");
    var URI_PACKAGE_BYCATEGORY = api.get("package_loadbycategory");
    var COOKIE_PKG_GROUP_PREFIX = "pkg_group_";

    // 2  - 冷压纤体
    // 3  - 冷压经典
    // 9  - 全绿系列
    // 19 - 暖体经典 V2.0
    // 20 - 暖体纤体 V2.0
    // 23 - 暖体大师清体
    // 25 - 闪电无姜
    var HOLIDAY_PROMOTION_PACKAGES = []; //[2, 3, 9, 19, 20, 23, 25];

    function getPackageGroup(dataPackage, packageId, groupId) {
        // Get package by packageId & groupId
        var dataGroups = [];
        var dataGroup;
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                dataGroups = dataPackage[i].Groups;
                if (dataGroup) {
                    break;
                }
                for (var j = 0, jLens = dataGroups.length; j < jLens; j++) {
                    if (dataGroups[j].Id.toString() === groupId.toString()) {
                        dataGroup = dataGroups[j];
                        break;
                    }
                }
            }
        }
        return dataGroup;
    }

    function getPrice(packageGroup, days) {
        var rule = packageGroup.Rules;
        if (!rule || rule.length <= 0) {
            return;
        }

        var iRule = rule[0];
        return iRule.PackagePrice * days;
    }

    return Widget.extend(function($element, widgetName, categoryId, col) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
        me.col = col || 1;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            if (!me.categoryId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE_BYCATEGORY,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = data.Packages;
                me.render(packages);
            });
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath);

            var packageId = parseInt(hashMap.packageid, 10);

            if (!packageId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = data.Packages;

                var i = 0;
                var iLens = packages.length;
                var currentPackages = [];

                for (i = 0; i < iLens; i++) {
                    if (packages[i].Id === packageId) {
                        currentPackages.push(packages[i]);
                        break;
                    }
                }

                me.render(currentPackages);
            });
        },
        "render": function(packages) {
            var me = this;
            var $me = me.$element;

            // Initialize packages (default group)
            me.packages = me.processingPackage(packages);
            // Render
            me.html(template, {
                "packages": me.packages,
                "cacheServer": context.cacheServer,
                "language": context.language,
                "col": me.col,
                "holidayPromotionPackages": HOLIDAY_PROMOTION_PACKAGES
            }).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
                // Update price
                $me.find("[data-action=packagebox]").each(function(i, e) {
                    var $e = $(e);
                    me.updatePrice($e);
                });
                // Publish packages data
                me.publish("package/data", me.packages);
            });
        },
        "processingPackage": function(packages) {
            var me = this;

            var i = 0;
            var iLens = packages.length;
            var j = 0;
            var jLens;
            var groups;
            var cookie_pkg_group_id;

            for (i = 0; i < iLens; i++) {
                groups = packages[i].Groups;
                if (!groups) {
                    break;
                }
                jLens = groups.length;
                cookie_pkg_group_id = cookie.getVal(COOKIE_PKG_GROUP_PREFIX + packages[i].Id) || 0;
                // Select default from cookie, or "0"
                if (cookie_pkg_group_id) {
                    for (j = 0; j < jLens; j++) {
                        if (groups[j].Id === parseInt(cookie_pkg_group_id, 10)) {
                            // Set default
                            groups[j].Selected = true;
                            // Save default
                            packages[i].defaultGroupId = groups[j].Id;
                            break;
                        }
                    }
                }
                // Set default while no default choosen
                // Female as Default
                // TODO: move this logic to backend
                if (!packages[i].defaultGroupId) {
                    // Set default
                    groups[1].Selected = true;
                    // Save default
                    packages[i].defaultGroupId = groups[1].Id;
                }
            }
            return packages;
        },
        "dom:a[data-action=selectpackage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            function fnNext() {
                var packageId = parseInt($packageContainer.data("packageId"), 10);
                var groupId = parseInt($packageContainer.find("[data-action=package-group-container] > label.active > input").val(), 10) || 0;
                var days = parseInt($packageContainer.find("[data-action=package-days-container] > label.active > input").val(), 10) || 5;

                // Append addon
                var addOn = "";
                $packageContainer.find("[data-action=packageaddon]").each(function(i, e) {
                    var $e = $(e);
                    if (e.checked) {
                        addOn += (addOn !== "" ? "," : "") + ($e.data("id") || 0);
                    }
                });

                var hash = "packageid/" + packageId +
                    "/groupid/" + groupId +
                    "/days/" + days +
                    "/addon/" + addOn;

                // TODO: Hardcode - Holiday promotion
                if (HOLIDAY_PROMOTION_PACKAGES.indexOf(packageId) >= 0 && days === 5) {
                    hash += "/event/holi";
                }

                pageUrl.goto("/order/receiver", hash);
            }

            fnNext();

            return;
        },
        "dom:input[name=radio-package-group]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            var packageId = $packageContainer.data("packageId");
            var groupId = $e.val();
            if (!groupId) {
                return;
            }

            me.updatePrice($packageContainer, groupId);
            // Publish to display widget
            me.publish("package/display", packageId, groupId);
            // Save current group to cookie
            cookie.set(COOKIE_PKG_GROUP_PREFIX + packageId, groupId);
        },
        "dom:input[name=package-days]/change": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");
            var days = parseInt($e.val(), 10);

            me.updatePrice($packageContainer, undefined, days);
        },
        "dom:[data-action=packageaddon]/ifChanged": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            me.updatePrice($packageContainer);
        },
        "updatePrice": function($packageContainer, groupId, days) {
            var me = this;

            var packageId = $packageContainer.data("packageId");
            if (!groupId) {
                groupId = $packageContainer.find("[data-action=package-group-container] > label.active > input").val() || 0;
            }
            if (!days) {
                days = $packageContainer.find("[data-action=package-days-container] > label.active > input").val() || 5;
            }

            var addOnPrice = 0;
            $packageContainer.find("[data-action=packageaddon]").each(function(i, e) {
                var $e = $(e);

                if (e.checked) {
                    addOnPrice += parseInt($e.data("price"), 10) || 0;
                }
            });

            var packageGroup = getPackageGroup(me.packages, packageId, groupId);
            var packagePrice = getPrice(packageGroup, days);
            var amount = number.getDecimal(packagePrice + addOnPrice * days);

            // DOM
            $packageContainer.find(".price").html("&yen;" + amount);
        },
        "dom:[data-action=packagebox]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $packageContainer = $(e.currentTarget);

            $packageContainer.siblings().removeClass("active");
            $packageContainer.addClass("active");
        },
        "dom:[data-action=showcleansedetail]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeIn(200);
        },
        "dom:[data-action=showcleansedetail]/mouseleave": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeOut(200);
        }
    });
});