define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "template!./index.html",
    "underscore"
], function(Widget, weave, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, template) {
    "use strict";

    var URI_PACKAGEBY_ID = api.get("package_loadbyid");

    // Get uniqle products
    function getUniqProducts(products) {
        var uniqProductsId = [];
        var uniqProducts = [];

        _.each(products, function(e) {
            if (uniqProductsId.indexOf(e.Id) >= 0) {
                return;
            }
            uniqProductsId.push(e.Id);
            uniqProducts.push(e);
        });

        return uniqProducts;
    }

    return Widget.extend({
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var packageId = parseInt(hashMap.packageid, 10);

            if (!packageId) {
                return;
            }

            // HARDCODE
            ajaxQuery({
                url: URI_PACKAGEBY_ID,
                data: {
                    packageId: packageId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var Packages = data.Packages;
                if (!Packages || Packages.length <= 0) {
                    return;
                }

                me.iPackage = Packages[0];
                if (!me.iPackage) {
                    return;
                }

                // Get default group (female first)
                var defaultGroup = me.iPackage.Groups[0];
                _.each(me.iPackage.Groups, function(e) {
                    // TODO: HARDCODE; male:1; female: 2
                    if (e.GroupTypeId === 2) {
                        defaultGroup = e;
                    }
                });

                // Render
                me.html(template, {
                    "package": me.iPackage,
                    "defaultGroupId": defaultGroup.Id,
                    "byWeek": defaultGroup.ByWeek,
                    "language": context.language
                }).then(function() {
                    // Publish current package data
                    me.publish("package/display", defaultGroup);
                    // Rendering
                    var $defaultProductTab = $me.find("[data-action=producttab].active > a");
                    me.renderProduct($defaultProductTab);
                });
            });
        },
        "renderProduct": function($els) {
            var me = this;
            var $me = me.$element;

            $els.each(function(i, e) {
                var $e = $(e);
                var productUrl = context.domain + "/juice/" + $e.data("url") + " #product-detail";
                var href = $e.attr("href");
                var $juiceCont = $me.find(href);

                $juiceCont.load(productUrl, function(responseText, status, xhr) {
                    // Weaving
                    weave.apply($juiceCont.find("[data-weave]"));
                });
            });
        },
        "dom:a[data-action=loadproduct]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            // Rendering
            me.renderProduct($e);
        },
        "dom:a[data-action=reloaddisplay]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            var groupId = parseInt($e.data("groupId"), 10) || 0;
            var selectGroup = me.iPackage.Groups[0];

            _.each(me.iPackage.Groups, function(e) {
                // TODO: HARDCODE; male:1; female: 2
                if (e.Id === groupId) {
                    selectGroup = e;
                }
            });

            // Publish current package data
            me.publish("package/display", selectGroup);
        }
    });
});