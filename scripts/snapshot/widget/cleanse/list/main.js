define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, ajaxQuery, api, pageUrl, template) {
    "use strict";

    var URI_PACKAGE = api.get("package");

    return Widget.extend(function($element, widgetName, cols, kick){
        var me = this;

        me.cols = cols || 2;
        me.kick = parseInt(kick, 10);
    },{
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            ajaxQuery({
                url: URI_PACKAGE,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = me.packages = data.Packages;

                // Render
                me.html(template, {
                    "cols": me.cols,
                    "kick": me.kick,
                    "cacheServer": context.cacheServer,
                    "packages": packages
                }).then(function() {
                    // Publish packages data
                    me.publish("package/data", packages);
                });
            });
        },
        "dom:[data-action=packagecontainer]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var $e = $(e.currentTarget);

            var packageId = $e.data("packageId");

            pageUrl.goto("/cleanse/choose", "packageid/" + packageId);
        }
    });
});