define(["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"widget/troophash/main",
	"widget/blurb/main",
	"template!./index.html"
], function(Widget, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, Blurb, template) {
	"use strict";

	var blurb = new Blurb($(document.body));
	var URI_PACKAGE_CATEGORY = api.get("package_category");

	// Own program, packageId = 0, groupId = 0
	var PACKAGE_CUSTOMIZE = {
		"Id": 0,
		"Name": blurb.get(1155),
		"Description": blurb.get(1352)
	};

	return Widget.extend(function($element, widgetName) {
        var me = this;

        me.preventRoute = false;
    }, {
		"hub:memory/route": function(uriPath, uriEvent) {
			var me = this;
			var $me = me.$element;

			var hashPath = uriPath.path || "category/2".split("/");
			var hashMap = troopHash.parse(hashPath);

			// prvrt: Prevent hash route event
			if (me.preventRoute) {
				return;
			}

			var categoryId = parseInt(hashMap.category, 10);
			var col = parseInt(hashMap.col, 10) || 1;

			ajaxQuery({
				url: URI_PACKAGE_CATEGORY,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				var categories = data.Categories;
				// Append customize package
				categories.push(PACKAGE_CUSTOMIZE);

				// Set default
				var i = 0;
				var iLens = categories.length;
				for (i = 0; i < iLens; i++) {
					if (categories[i].Id === categoryId) {
						categories[i].IsDefault = true;
					}
				}

				// Render
				me.html(template, {
					"cacheServer": context.cacheServer,
					"categories": categories,
					"col": col
				}).then(function() {});
			});
		},
		"dom:a[data-toggle=tab]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;
			var $el = $(e.currentTarget);

			var categoryId = $el.data("id");

			//return;
			me.preventRoute = true;
			troopHash.extend({
				"category": categoryId
			});
		},
		"dom:a[data-action=customize]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			// Has cookie
			/*
			cookie.get("memberid").then(function() {
				// Get User Account
				ajaxQuery({
					url: URI_ACCOUNT,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					// Do nothing
				}, function() {
					// Popup order-edit lightbox.
					me.publish("enrollment/signin/popup");
				});
			}, function() {
				// Popup order-edit lightbox.
				me.publish("enrollment/signin/popup");
			});*/
		}
	});
});