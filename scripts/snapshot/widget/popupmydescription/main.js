define(["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "dom/click": function() {
            var me = this;

            var obj_ID = me.$element.attr("data-for");
            me.publish("popupMyDescription/popup", obj_ID);
        }
    });
});