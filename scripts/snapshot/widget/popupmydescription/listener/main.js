define(["troopjs-browser/component/widget",
  "jquery",
  "widget/popupbox/main"
], function(Widget, $, PopupBox) {
  "use strict";

  return Widget.extend({
    "hub:memory/popupMyDescription/popup": function(for_el_id) {
      var me = this;

      var $me = me.$element;

      if ($me[0].id !== for_el_id) {
        return;
      }

      var block = new PopupBox({
        msg: $me.clone(),
        closeble: true,
        closeInner: true,
        zIndex: 10
      });
      block.open();
    }
  });
});