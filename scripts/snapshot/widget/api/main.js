define(["jquery",
	"context"
], function($, context) {
	"use strict";

	var serviceDomain = context.serviceDomain;
	var apiDict = {
		// Account
		"account": serviceDomain + "/personal/myregisterinfo",
		"account_create": serviceDomain + "/account/regist",
		"account_update": serviceDomain + "/personal/updatemyinfo",
		"account_chgpwd": serviceDomain + "/personal/changepassword",
		"emailcheck": serviceDomain + "/dataprovider/isemailduplicated",
		"phonecheck": serviceDomain + "/dataprovider/isphoneduplicated",
		"sendverificationcode": serviceDomain + "/account/sendvalidationcodebyweb",
		"login": serviceDomain + "/account/logon",
		"forgotpasswordverificationcode": serviceDomain + "/enrollment/sendvalidationcode",
		"resetpassword": serviceDomain + "/enrollment/resetpassword",
		// Address
		"region": serviceDomain + "/dataprovider/region",
		"region_with_province": serviceDomain + "/dataprovider/RegionWithProvince",
		"receiver": serviceDomain + "/personal/myreceivers",
		"receiver_create": serviceDomain + "/personal/addreceiver",
		"receiver_delete": serviceDomain + "/personal/deletereceiver",
		"receiver_update": serviceDomain + "/personal/updatereceiver",
		"receiver_setdefault": serviceDomain + "/personal/setasdefaultreceiver",
		// Order
		"package_category": serviceDomain + "/dataprovider/PackageCategory",
		"package_loadbycategory": serviceDomain + "/dataprovider/LoadPackageByCategoryId",
		"package": serviceDomain + "/dataprovider/package",
		"package_loadbyid": serviceDomain + "/dataprovider/LoadPackageByPackageId",
		"product": serviceDomain + "/dataprovider/products",
		"deliver_time": serviceDomain + "/dataprovider/loaddelivertime",
		"deliver_timebyarea": serviceDomain + "/dataprovider/LoadDeliverTimeByArea",
		"payment": serviceDomain + "/dataprovider/loadpayment",
		"orders": serviceDomain + "/order/loadmyorders",
		"orders_bymobile": serviceDomain + "/order/LoadOrderByMobile",
		"order_create": serviceDomain + "/order/createorder",
		"order_confirm": serviceDomain + "/order/confirmorder",
		"order_promotion_code": serviceDomain + "/order/applypromotion",
		"orderdetail": serviceDomain + "/order/loadorderbyid",
		"order_delete": serviceDomain + "/order/deleteorder",
		"order_updatereceiver": serviceDomain + "/order/UpdateOrdersmReceiver",
		"summary":serviceDomain+"/buy/summary",
		// Juices
		"product_category": serviceDomain + "/dataprovider/ProductCategory",
		"product_loadbycategory": serviceDomain + "/dataprovider/LoadProductsByCategoryId",
		"product_loadbyday": serviceDomain + "/dataprovider/LoadProductsByDayId",
		// Stores
		"stores": serviceDomain + "/dataprovider/LoadAllStores",
		// Events
		"quick_order": serviceDomain + "/order/CreateEventOrder",
		"load_survey": serviceDomain + "/Survey/LoadSurveyById",
		"save_survey": serviceDomain + "/Survey/Save",
		"report_survey": serviceDomain + "/Survey/Report",
		// Email subscribebscript
		"subscribe": serviceDomain + "/Subscription/Subscribe",
		"un_subscribe": serviceDomain + "/Subscription/Unsubscribe"
	};

	// For local test environment
	if (!serviceDomain) {
		$.extend(apiDict, {
			// Account
			"account": "/api/account.json",
			"login": "/api/logon.json",
			// Address
			"region": "/api/region.json",
			"region_with_province": "/api/RegionWithProvince.json",	//serviceDomain + "/dataprovider/RegionWithProvince"
			"receiver": "/api/receiver.json",
			"receiver_create": "/api/addreceiver.json",//serviceDomain + "/personal/addreceiver",
//			"receiver_delete": serviceDomain + "/personal/deletereceiver",
			"receiver_update": "/api/updatereceiver.json",//serviceDomain + "/personal/updatereceiver",
//			"receiver_setdefault": serviceDomain + "/personal/setasdefaultreceiver",
			// Order
			"package_category": "/api/PackageCategory.json",
			"package_loadbycategory": "/api/LoadPackageByCategoryId.json",
			"package": "/api/package.json",
			"package_loadbyid": "/api/LoadPackageByPackageId.json",
			"product": "/api/products.json",
			"deliver_time": "/api/loaddelivertime.json",
			"deliver_timebyarea": "/api/LoadDeliverTimeByArea.json",
			"order_create": "/api/createorder.json",
			"payment": "/api/LoadPayment.json",
			"order_confirm": "/api/confirmorder.json",//serviceDomain + "/order/confirmorder",
			"rder_promotion_code": "/api/applypromotion.json",
			// Juices
			"product_category": "/api/ProductCategory.json",
			"product_loadbycategory": "/api/LoadProductsByCategoryId.json",

			"orders": "/api/loadmyorders.json",
			"orderdetail": "/api/loadorderinfo.json",
			// Stores
			"stores": "/api/LoadAllStores.json"
				
		});
	}

	return {
		"get": function(key) {
			key = key.toLowerCase();

			return apiDict[key];
		}
	};

});