define(["troopjs-browser/component/widget",
    "jquery",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, $, PopupBox, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "dom/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            var url = $el.data("url");

            if (!url) {
                return;
            }

            var $map = $(template({
                "url": url
            }));

            // Lightbox configuration
            var mapPopup = new PopupBox({
                msg: $map,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("orderEdit/updateOrder");
                    $map.remove();
                }
            });

            mapPopup.open();
        }
    });
});