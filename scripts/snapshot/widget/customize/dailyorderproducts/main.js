define(["troopjs-browser/component/widget",
	"jquery",
	"widget/ajaxquery/main",
	"template!./index.html"
], function(Widget, $, ajaxQuery, template) {
	"use strict";

	return Widget.extend(function($element, widgetName) {}, {
		"hub:memory/dailyOrderProducts/get": function(dailyProducts) {
			var me = this;

			// Render
			me.html(template, {
				"dailyProducts": dailyProducts
			}).then(function() {});
		}
	});
});