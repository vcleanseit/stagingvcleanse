define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "widget/basic/cloneobject",
    "widget/popupbox/main",
    "widget/time/main",
    "template!./index.html"
], function(Widget, weave, when, $, cloneObject, PopupBox, Time, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    return Widget.extend(function() {
        var me = this;

        me.packageGroupDeferred = when.defer();
    }, {
        "setHeight": function($el) {
            var minHeight = 400;
            var viewHeight = window.innerHeight;
            var elHeight = viewHeight - 100;
            elHeight = elHeight <= minHeight ? minHeight : elHeight;
            // Set Customize box height
            $el.height(elHeight);
        },
        "hub/customize/popup": function(dailyOrder) {
            if (!dailyOrder) {
                return;
            }
            var me = this;

            // Run everything after make sure current order is from package or customize
            var activeDate = dailyOrder.Date;
            dailyOrder.stringDate = INS_TIME.getStrDate(activeDate, " / ");
            var dayId = activeDate.getDay();
            var $me = $(template({
                dailyOrder: dailyOrder,
                dayId: dayId,
                weekName: INS_TIME.getWeekName(dayId, false)
            }));

            // Set popup box height
            me.setHeight($me);

            // Save in memory
            me.$me = $me;
            me.dailyOrder = dailyOrder;
            me.date = activeDate;

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 10,
                // TODO:
                closeCallback: function() {
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open().then(function(){
                // Get products
                me.publish("orderProducts/get", dailyOrder.Products);
                me.publish("dailyOrderProducts/get", dailyOrder.Products);
            });
        },
        "hub/customize/resize": function(args) {
            var me = this;

            me.popupOrderEdit.centralize();
        },
        // Products change
        "hub/customize/changeProduct": function(args) {
            var me = this;

            // {
            //     id: xxx,
            //     count: xxx
            // }
            if (!args) {
                return;
            }
            var id = args.id;
            var name = args.name;
            var price = args.price;
            var count = args.count;

            var dailyProducts = me.dailyOrder.Products || [];
            var j = 0;
            var jLens = dailyProducts.length;

            var isNewProduct = true;

            // Already has products
            if (jLens) {
                for (j = 0; j < jLens; j++) {
                    if (dailyProducts[j].Id === id) {
                        isNewProduct = false;

                        // Update only when products count > 0
                        if (count > 0) {
                            dailyProducts[j].Count = count;
                        } else {
                            // Remove current product
                            dailyProducts.splice(j, 1);
                        }
                        break;
                    }
                }
                if (isNewProduct) {
                    // count should always > 0
                    dailyProducts.push({
                        "Id": id,
                        "Name": name,
                        "Price": price,
                        "Count": count
                    });
                }
            } else {
                // count should always > 0
                me.dailyOrder.Products = [];
                me.dailyOrder.Products.push({
                    "Id": id,
                    "Name": name,
                    "Price": price,
                    "Count": count
                });
            }
            me.publish("dailyOrderProducts/get", me.dailyOrder.Products);
        },
        // Remove current day from order
        "dom:[data-action=removeday]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Clear/Empty current order
            me.dailyOrder.Products = null;
            me.dailyOrder.PackageRuleId = null;
            me.dailyOrder.PackageProducts = null;

            // Submit order information on current day
            me.publish("order/dailyUpdate", me.dailyOrder);

            // Close popup
            me.popupOrderEdit.close();
        },
        // Submit data
        "dom:[data-action=updateorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            var dailyOrder = me.dailyOrder;
            // Submit order information on current day
            me.publish("order/dailyUpdate", me.dailyOrder);

            // Close popup
            me.popupOrderEdit.close();
        }
    });
});