define(["troopjs-browser/component/widget",
	"jquery",
	"template!./index.html"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend(function($element, widgetName, dayId) {
		var me = this;
		me.dayId = dayId;
	}, {
		"sig/start": function() {
			var me = this;

			// Render
			me.html(template, {
				"dayId": me.dayId
			}).then(function() {});
		}
	});
});