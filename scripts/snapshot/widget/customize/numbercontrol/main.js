define(["troopjs-browser/component/widget",
    "jquery",
    "widget/infobox/main",
    "widget/blurb/main",
    "template!./index.html"
], function(Widget, $, InfoBox, Blurb, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    return Widget.extend(function($element, widgetName, id, name, count, countMax, price, disable) {
        var me = this;
        // get parameters
        me.id = id;
        me.name = name || "";
        me.count = count || 0;
        me.countMax = countMax || 0;
        me.price = price || 0;
        me.disable = disable || false;
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template, {
                count: me.count,
                disable: me.disable
            });
            me.$number = me.$element.find(".numbercontrol-number");
        },
        "changeProduct": function() {
            var me = this;
            // Maintain order
            me.publish("customize/changeProduct", {
                id: me.id,
                name: me.name,
                price: me.price,
                count: me.count
            });
        },
        "dom:.numbercontrol-plus/click": function(e) {
            e.preventDefault();
            var me = this;
            if(me.disable){
                return;
            }

            if (me.countMax > 0 && me.count >= me.countMax) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: blurb.get(1204, me.countMax),
                    overLap: "overlap"
                }).open();
                return;
            }
            me.count++;
            me.$number.text(me.count);
            // Maintain order
            me.changeProduct();
        },
        "dom:.numbercontrol-minus/click": function(e) {
            e.preventDefault();
            var me = this;
            if(me.disable){
                return;
            }

            if (me.count <= 0) {
                return;
            }
            me.count--;
            me.$number.text(me.count);
            // Maintain order
            me.changeProduct();
        }
    });
});