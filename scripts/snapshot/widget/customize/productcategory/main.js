define(["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, pageUrl, cookie, template) {
	"use strict";

	var URI_PRODUCT_CATEGORY = api.get("product_category");
	var URI_PRODUCT = api.get("product_loadbyday");

	return Widget.extend(function($element, widgetName, dayId) {
		var me = this;

		dayId = parseInt(dayId, 10);
		if (isNaN(dayId)) {
			dayId = -1;
		}
		me.dayId = dayId === 0 ? 7 : dayId;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			when.join(ajaxQuery({
				url: URI_PRODUCT_CATEGORY,
				data: {
					"language": context.language
				},
				type: "GET",
				dataType: "json"
			}), ajaxQuery({
				url: URI_PRODUCT,
				data: {
					//categoryId: me.categoryId,
					// -1: load all products
					"dayId": me.dayId,
					"language": context.language
				},
				type: "GET",
				dataType: "json"
			})).then(function(data) {
				var categories = data[0].Categories;
				var products = data[1].Products;

				// Render
				me.html(template, {
					"dayId": me.dayId,
					"categories": categories
				}).then(function() {
					me.publish("allproducts/get", products);
                    			me.publish("orderProducts/get", products);
				});
			});
		}
	});
});