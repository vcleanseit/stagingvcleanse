define(["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, template) {
    "use strict";

    var URI_PRODUCT = api.get("product_loadbyday");

    return Widget.extend(function($element, widgetName, categoryId) {
        var me = this;

        me.deferred = when.defer();
        me.categoryId = categoryId;
    }, {
        "hub/allproducts/get": function(products) {
            var me = this;
            me.deferred.resolver.resolve(products);
        },
        "hub:memory/orderProducts/get": function(orderProducts) {
            var me = this;
            var $me = me.$element;

            me.deferred.promise.then(function(products) {
                me.render(products, orderProducts);
            });
        },
        "render": function(products, orderProducts) {
            var me = this;
            var $me = me.$element;

            var i = 0;
            var iLens;
            var j = 0;
            var jLens;
            var categoryProducts = [];

            jLens = products.length;
            for (j = 0; j < jLens; j++) {
                // From change order events
                // Append products count to products object
                if (orderProducts) {
                    iLens = orderProducts.length;
                    for (i = 0; i < iLens; i++) {
                        if (orderProducts[i].Id === products[j].Id) {
                            products[j].Count = orderProducts[i].Count;
                            break;
                        }
                    }
                }
                if (products[j].ProductTypeId === me.categoryId) {
                    categoryProducts.push(products[j]);
                }
            }

            me.html(template, {
                "cacheServer": context.cacheServer,
                "products": categoryProducts
            }).then(function() {
                // Resolve to tell order-edit centralize
                me.publish("customize/resize");
            });
        },
        "hub/customize/changeProduct": function(data) {
            var me = this;
            var $me = me.$element;

            if (!data) {
                return;
            }
            var productId = data.id;
            var count = data.count;

            var $product = $me.find("[data-action=productcontainer][data-id=" + productId + "]");

            if (count > 0) {
                $product.addClass("active");
            } else {
                $product.removeClass("active");
            }
        }
    });
});