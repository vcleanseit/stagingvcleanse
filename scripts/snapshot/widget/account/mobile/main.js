define(["troopjs-browser/component/widget",
	"jquery",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/cookie/main",
    "widget/enrollment/signin/main",
	"template!./index.html"
], function(Widget, $, api, ajaxQuery, cookie, Signin, template) {
	"use strict";

    (new Signin($(document.body))).start();
	var URI_ACCOUNT = api.get("account");

	return Widget.extend({
		"sig/start": function() {
			var me = this;

			// Has cookie
			cookie.get("memberid").then(function() {
				// Get User Account
				ajaxQuery({
					url: URI_ACCOUNT,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					// Rendering
					me.html(template, {
						"logged": true
					});
				}, function() {
					// Rendering
					me.html(template, {
						"logged": false
					});
				});
			}, function() {
				// Rendering
				me.html(template, {
					"logged": false
				});
			});
		},
        "dom:a[data-action=signin]/click": function(e) {
            e.preventDefault();

            var me = this;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        }
	});
});