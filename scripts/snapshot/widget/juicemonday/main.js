define(["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/time/main",
	"template!./index.html"
], function(Widget, $, context, Time, template) {
	"use strict";

	// Time function
	var INS_TIME = new Time();

	return Widget.extend({
		"sig/start": function() {
			var me = this;

			var now = new Date();
			var startMonday;
			if (INS_TIME.getDayOfWeekFromMonday(now) < 6) {
				startMonday = INS_TIME.getWeekStartDate(INS_TIME.addDay(now, 7));
			} else {
				startMonday = INS_TIME.addDay(now, 8);
			}

			var juiceMondays = [];
			for (var i = 0; i < 5; i++) {
				juiceMondays.push(INS_TIME.getStrDate(INS_TIME.addDay(startMonday, 7 * i)));
			}

			// Render
			me.html(template, {
				"startDate": INS_TIME.getStrDate(startMonday, "-"),
				"mondays": juiceMondays.join("、")
			});
		}
	});
});