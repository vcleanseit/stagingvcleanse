define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, weave, when, $, context, PopupBox, template) {
    "use strict";

    return Widget.extend(function() {
        var me = this;
    }, {
        "hub/enrollment/signin/popup": function() {
            var me = this;
            var $me = $(template({
                "isMobi": context.isMobile
            }));

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("orderEdit/updateOrder");
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open();
        }
    });
});