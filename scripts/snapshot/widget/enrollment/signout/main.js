define(["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main"
], function(Widget, weave, when, $, context,cookie) {
    "use strict";

    return Widget.extend(function() {
        var me = this;
        cookie.rm("memberid", {
            path: "/"
        });
        window.location.href = '/';
    });
});