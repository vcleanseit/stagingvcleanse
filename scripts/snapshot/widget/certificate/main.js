define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/pageurl/main",
    "widget/querystring/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, pageUrl, queryString, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Load order detail, so ignore order step
            var request = queryString.getRequest();
            if(!request){
                pageUrl.goto("/");
            }

            // Rendering
            me.html(template, {
                "cfc": "/paws/image?orderid=" + request.orderid,
                "cacheServer": context.cacheServer,
                "isMobile": context.isMobile
            }).then(function(){
                
            });
        }
    });
});