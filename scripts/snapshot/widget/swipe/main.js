define(["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html",
    "swipe"
], function (Widget, $, template) {
    "use strict";

    return Widget.extend(function ($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function () {
            var me = this;
            var $me = me.$element;

            var startSlide = parseInt($me.data("startSlide"), 10) || 0;
            var speed = parseInt($me.data("speed"), 10) || 300;
            var auto = parseInt($me.data("auto"), 10) || 0;
            var continuous = $me.data("continuous") || true;
            var disableScroll = $me.data("disableScroll") || false;
            var stopPropagation = $me.data("stopPropagation") || false;
            // center, left, right
            var bulletsPos = $me.data("bulletsPos") || "center";

            var swipeLens = $me.find(".swipe-wrap").children().length || 0;

            var $swipeBullets = $(template({
                "swipeLens": swipeLens,
                "startSlide": startSlide,
                "bulletsPos": bulletsPos
            }));
            $me.append($swipeBullets);
            var $bullets = $swipeBullets.find("> li.bullets-item");

            $me.Swipe({
                startSlide: startSlide,
                speed: speed,
                auto: auto,
                continuous: continuous,
                disableScroll: disableScroll,
                stopPropagation: stopPropagation,
                callback: function (i, e) {
                    $bullets.removeClass("on");
                    $bullets.eq(i).addClass("on");
                },
                transitionEnd: function (index, elem) {
                }
            });

        }
    });
});