define(["troopjs-browser/component/widget",
    "jquery",
    "context",
    "underscore"
], function(Widget, $, context) {

    var BLURB = window.BLURB || {};

    var blurbDict = _.extend(BLURB, {
        "1000": {
            "en": "Sign in",
            "cn": "登录"
        },
        "1001": {
            "en": "My VCLEANSE",
            "cn": "我的维果清"
        },
        "1002": {
            "en": "Questions? Contact us (9AM - 7PM):",
            "cn": "欢迎咨询我们的健康管家（上午9点 - 晚上7点）："
        },
        "1003": {
            "en": "Your Order",
            "cn": "开始订购"
        },
        "1004": {
            "en": "Welcome, ",
            "cn": "欢迎您，"
        },
        "1005": {
            "en": "STEP 1:",
            "cn": "第一步："
        },
        "1006": {
            "en": "STEP 2:",
            "cn": "第二步："
        },
        "1007": {
            "en": "STEP 3:",
            "cn": "第三步："
        },
        "1008": {
            "en": "Select start date",
            "cn": "套餐开始日期"
        },
        "1009": {
            "en": "Confirm Order",
            "cn": "确认订单"
        },
        "1010": {
            "en": "Pay",
            "cn": "付款"
        },
        "1011": {
            "en": "Retrieve Password",
            "cn": "找回密码"
        },
        "1012": {
            "en": "Create your VCLEANSE account",
            "cn": "创建您的维果清帐号"
        },
        "1013": {
            "en": "Basic Information",
            "cn": "个人基本信息"
        },
        "1014": {
            "en": "Delivery Addresses",
            "cn": "收件人地址"
        },
        "1015": {
            "en": "My Orders",
            "cn": "我的订单"
        },
        "1016": {
            "en": "Surpass Court, 570 Yongjia Road, Shanghai",
            "cn": "上海市永嘉路570号永嘉庭"
        },
        "1017": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1018": {
            "en": "Select program",
            "cn": "选择您的清体套餐"
        },
        "1019": {
            "en": "Choose one of our programs, or create your own.",
            "cn": "选择适合您的套餐，享受订购吧！"
        },
        "1020": {
            "en": "Create an account",
            "cn": "注册"
        },
        "1021": {
            "en": "Sign out",
            "cn": "退出"
        },
        "1022": {
            "en": "Contact us",
            "cn": "联系我们"
        },
        "1024": {
            "en": "When would you like to start your cleanse?",
            "cn": "请选择一天开始您的维果清清体之旅？"
        },
        "1025": {
            "en": "Past two weeks",
            "cn": "上两周"
        },
        "1026": {
            "en": "Next two weeks",
            "cn": "下两周"
        },
        "1027": {
            "en": "Pick up in store",
            "cn": "来店自取"
        },
        "1028": {
            "en": "Change",
            "cn": "修改"
        },
        "1029": {
            "en": "Add",
            "cn": "添加"
        },
        "1030": {
            "en": "Too late <br>to order",
            "cn": "您错过了~"
        },
        "1031": {
            "en": "Confirm",
            "cn": "确认"
        },
        "1032": {
            "en": "Order",
            "cn": "您的订单"
        },
        "1033": {
            "en": "Product:",
            "cn": "产品："
        },
        "1034": {
            "en": "Delivery:",
            "cn": "配送："
        },
        "1035": {
            "en": "Payment Total:",
            "cn": "总价："
        },
        "1036": {
            "en": "Company invoice:",
            "cn": "公司发票："
        },
        "1037": {
            "en": "Personal invoice:",
            "cn": "个人发票："
        },
        "1038": {
            "en": "Invoice amount:",
            "cn": "发票金额："
        },
        "1039": {
            "en": "Invoice title:",
            "cn": "发票抬头："
        },
        "1040": {
            "en": "Total:",
            "cn": "总价："
        },
        "1041": {
            "en": "Your tax invoice (fapiao)",
            "cn": "发票信息"
        },
        "1043": {
            "en": "VCLEANSE will issue an invoice for products. For an invoice for delivery fees, please contact the delivery company",
            "cn": "维果清发票金额仅为产品，若需运费发票，请与外送公司联系。"
        },
        "1044": {
            "en": "Yes, I would like an invoice",
            "cn": "需要发票"
        },
        "1045": {
            "en": "Personal invoice",
            "cn": "个人发票"
        },
        "1046": {
            "en": "Company invoice",
            "cn": "公司发票"
        },
        "1047": {
            "en": "Company title",
            "cn": "公司名称"
        },
        "1048": {
            "en": "Purchase a gift card",
            "cn": "购买礼品卡"
        },
        "1049": {
            "en": "Give the gift of health and happiness!",
            "cn": "送出健康快乐的礼物"
        },
        "1050": {
            "en": "BUY",
            "cn": "购买"
        },
        "1051": {
            "en": "Find your juice type",
            "cn": "选购果汁类型"
        },
        "1052": {
            "en": "LEARN MORE",
            "cn": "更多选择"
        },
        "1053": {
            "en": "WeChat:",
            "cn": "微信："
        },
        "1054": {
            "en": "First Name:",
            "cn": "名字："
        },
        "1055": {
            "en": "Last name:",
            "cn": "姓："
        },
        "1056": {
            "en": "Mobile:",
            "cn": "手机号码："
        },
        "1057": {
            "en": "Shipping address:",
            "cn": "收件人地址："
        },
        "1058": {
            "en": "Pick up in store:",
            "cn": "来店自取："
        },
        "1059": {
            "en": "Thank you for your order! If you need to change or cancel your order, please give us a call at",
            "cn": "感谢您的选购。如果您需要修改或者取消订单，请给我们来电"
        },
        "1060": {
            "en": "Order details",
            "cn": "订单详情"
        },
        "1061": {
            "en": "Order number:",
            "cn": "订单号："
        },
        "1062": {
            "en": "Payment Summary:",
            "cn": "支付详情："
        },
        "1063": {
            "en": "If you have any questions, please give us a call at",
            "cn": "如果您在购买过程中遇到任何问题请联系我们清体管家"
        },
        "1064": {
            "en": "Thanks for your order",
            "cn": "感谢您的选购"
        },
        "1065": {
            "en": "VCLEANSE Team",
            "cn": "维果清小组"
        },
        "1066": {
            "en": "Deliver to:",
            "cn": "发送到："
        },
        "1067": {
            "en": "Add Another Address",
            "cn": "添加地址"
        },
        "1068": {
            "en": "CREATE ACCOUNT",
            "cn": "创建账户"
        },
        "1069": {
            "en": "UPDATE",
            "cn": "更新"
        },
        "1070": {
            "en": "DELETE",
            "cn": "删除"
        },
        "1071": {
            "en": "SET AS DEFAULT",
            "cn": "设置为默认地址"
        },
        "1072": {
            "en": "Default address",
            "cn": "默认地址"
        },
        "1073": {
            "en": "Edit delivery address",
            "cn": "修改收件人地址"
        },
        "1074": {
            "en": "Add a delivery address",
            "cn": "添加收件人地址"
        },
        "1075": {
            "en": "CANCEL",
            "cn": "取消"
        },
        "1076": {
            "en": "Email:",
            "cn": "电子邮箱："
        },
        "1077": {
            "en": "New password:",
            "cn": "新的密码："
        },
        "1078": {
            "en": "Confirm password:",
            "cn": "确认密码："
        },
        "1079": {
            "en": "SAVE",
            "cn": "保存"
        },
        "1080": {
            "en": "SIGN IN",
            "cn": "登录"
        },
        "1081": {
            "en": "I have a VCLEANSE account",
            "cn": "已经有维果清账号"
        },
        "1083": {
            "en": "Forgot password?",
            "cn": "忘记密码？"
        },
        "1084": {
            "en": "SEND VERIFICATION CODE",
            "cn": "发送验证码"
        },
        "1085": {
            "en": "Security code:",
            "cn": "验证码："
        },
        "1086": {
            "en": "Preferred delivery window?",
            "cn": "选择理想配送时间段"
        },
        "1087": {
            "en": "Deliver to this address",
            "cn": "配送到此地址"
        },
        "1088": {
            "en": "Edit this address",
            "cn": "修改地址"
        },
        "1090": {
            "en": "REMOVE THIS DAY FROM ORDER",
            "cn": "删除"
        },
        "1091": {
            "en": "UPDATE ORDER",
            "cn": "更新订单"
        },
        "1092": {
            "en": "Order status:",
            "cn": "订单状态："
        },
        "1093": {
            "en": "Tracking number:",
            "cn": "物流单号："
        },
        "1094": {
            "en": "+ Change",
            "cn": "+ 修改"
        },
        "1096": {
            "en": "+ See more",
            "cn": "+ 查看更多"
        },
        "1097": {
            "en": "+ Learn more",
            "cn": "+ 更多信息"
        },
        "1098": {
            "en": "Order number",
            "cn": "订单号"
        },
        "1099": {
            "en": "Order date",
            "cn": "订单日期"
        },
        "1100": {
            "en": "Start date",
            "cn": "开始饮用日期"
        },
        "1101": {
            "en": "Total Amount (RMB)",
            "cn": "总价（元）"
        },
        "1102": {
            "en": "Order Status",
            "cn": "订单状态"
        },
        "1103": {
            "en": "+ Learn more",
            "cn": "+ 了解更多"
        },
        "1104": {
            "en": "BUY",
            "cn": "购买"
        },
        "1105": {
            "en": "Questions about your delivery?",
            "cn": "有配送问题？"
        },
        "1106": {
            "en": "Call our delivery company Yamato Express at",
            "cn": "拨打我们配送伙伴宅急便"
        },
        "1107": {
            "en": "Have your order tracking number ready when you call. They speak English and Chinese.",
            "cn": "他们提供中英文服务帮助您跟踪订单状态"
        },
        "1108": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1109": {
            "en": "Click on the button below to pay via Alipay!",
            "cn": "选择下面的支付方式完成支付!"
        },
        "1110": {
            "en": "Alipay",
            "cn": "支付宝"
        },
        "1111": {
            "en": "Payment method",
            "cn": "支付方式"
        },
        "1112": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1113": {
            "en": "Payment on delivery",
            "cn": "货到付款"
        },
        "1114": {
            "en": "NEXT",
            "cn": "下一步"
        },
        "1115": {
            "en": "CONFIRM",
            "cn": "确认"
        },
        "1116": {
            "en": "Tel:",
            "cn": "联系电话："
        },
        "1117": {
            "en": "CLICK HERE TO ADD ANOTHER ORDER",
            "cn": "点击开始您的订购"
        },
        "1118": {
            "en": "RETURN TO MY ORDERS",
            "cn": "返回我的订单"
        },
        "1119": {
            "en": "You can cancel your order online at any time up to 24 hours before your start date. If you are within 24 hours of your start date and need to modify your order, please call us at 4001-V-HAPPY. If you selected online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.",
            "cn": "您可以在订单配送前24小时取消您的订单。为了确保准时配送，请在配送前24小时完成支付。"
        },
        "1120": {
            "en": "Gender:",
            "cn": "性别："
        },
        "1121": {
            "en": "Female",
            "cn": "女"
        },
        "1122": {
            "en": "Male",
            "cn": "男"
        },
        "1123": {
            "en": "Birthday:",
            "cn": "生日："
        },
        "1124": {
            "en": "Payment Status",
            "cn": "支付状态"
        },
        "1125": {
            "en": "Payment Status:",
            "cn": "支付状态："
        },
        "1126": {
            "en": "PAY ONLINE",
            "cn": "付款"
        },
        "1127": {
            "en": "Home",
            "cn": "首页"
        },
        "1128": {
            "en": "Store address:",
            "cn": "门店地址："
        },
        "1129": {
            "en": "OK",
            "cn": "确定"
        },
        "1130": {
            "en": "Password",
            "cn": "密码"
        },
        "1131": {
            "en": "Confirm password",
            "cn": "确认密码"
        },
        "1132": {
            "en": "Your Name Here",
            "cn": "您的姓名"
        },
        "1133": {
            "en": "Your Last Name Here",
            "cn": "您的姓氏"
        },
        "1134": {
            "en": "Mobile number is required",
            "cn": "11位手机号码，必填"
        },
        "1136": {
            "en": "Full address is required",
            "cn": "完整收件地址（不需要重复填写市、区），必填"
        },
        "1137": {
            "en": "Your mobile phone",
            "cn": "您的手机号码"
        },
        "1138": {
            "en": "Your Email Address",
            "cn": "您的电子邮件"
        },
        "1139": {
            "en": "Input verification code sent to your mobile phone",
            "cn": "输入您的手机收到的验证码"
        },
        "1140": {
            "en": "Mobile",
            "cn": "手机号码"
        },
        "1141": {
            "en": "Order successful submit, payment as soon as possible please!",
            "cn": "订单提交成功，请您尽快付款！"
        },
        "1142": {
            "en": "Amount Due (RMB):",
            "cn": "应付金额（元）："
        },
        "1143": {
            "en": "Please complete payment within 24 hours. Orders will be cancelled after 24 hours with no payment.",
            "cn": "立即支付{0}元，即可完成订单。 请您在24小时内完成支付，否则订单会被自动取消。"
        },
        "1144": {
            "en": "Reminder!",
            "cn": "注意！"
        },
        "1145": {
            "en": "Dear V-Cleanser, please pickup your orders in the store until you have a delivery address! <br>Click here to <a href=\"\/account\/receiver\">add delivery address</a>, or you can also edit delivery address by click \"+ Change\" on calendar.",
            "cn": "您还没有填写收件人地址哦！如果不添加请不要忘记来门店提取您的维果清！<br>点击这里<a href=\"\/account\/receiver\">添加收件人地址</a>，或者您也可以在对应日期点击 “+ 修改” ，然后编辑收件人地址。"
        },
        "1146": {
            "en": "Dear V-Cleanser, you will choose your own programs. <br>Click \"+ Add\" on calendar to start your order!",
            "cn": "您选择创建自己的维果清套餐，请在对应的日期点击 “+ 添加” 开始您的订购！"
        },
        "1147": {
            "en": "Dear V-Cleanser, please order again from order page.",
            "cn": "您选购的商品信息有误，请回到订购页面重新订购。"
        },
        "1148": {
            "en": "Success!",
            "cn": "成功！"
        },
        "1149": {
            "en": "Successfully set this delivery address to default!",
            "cn": "成功设置为默认地址！"
        },
        "1150": {
            "en": "Successfully delete this delivery address!",
            "cn": "成功删除该收件人地址！"
        },
        "1151": {
            "en": "Successfully update this delivery address!",
            "cn": "成功更新该收件人地址！"
        },
        "1152": {
            "en": "Successfully add this new delivery address!",
            "cn": "成功增加新的收件人地址！"
        },
        "1153": {
            "en": "Successfully update my basic information!",
            "cn": "成功更新我的基本信息！"
        },
        "1154": {
            "en": "Change password",
            "cn": "如果您需要更改密码，请在此输入"
        },
        "1155": {
            "en": "Customize",
            "cn": "单品购买"
        },
        "1156": {
            "en": "Design your own cleanse program!",
            "cn": "创建您自己的清体套餐！"
        },
        "1157": {
            "en": "Invoice type:",
            "cn": "发票类型："
        },
        "1158": {
            "en": "Company",
            "cn": "公司发票"
        },
        "1159": {
            "en": "Personal",
            "cn": "个人发票"
        },
        "1160": {
            "en": "New VCLEANSE password has sent to your mobile phone, check please!",
            "cn": "新的VCLEANSE密码已经发送到您的手机，请查收！"
        },
        "1161": {
            "en": "Choose your cleanse program duration:",
            "cn": "选择清体套餐理想天数："
        },
        "1162": {
            "en": "Not yet delivered",
            "cn": "未配送"
        },
        "1163": {
            "en": "Delivering",
            "cn": "配送中"
        },
        "1164": {
            "en": "Delivered",
            "cn": "已配送"
        },
        "1165": {
            "en": "Dear new V-Cleanser,",
            "cn": "亲爱的新晋维果清达人，"
        },
        "1166": {
            "en": "Congrats! You are about to embark on a fun journey with us!",
            "cn": "恭喜，你将跟我们开始一段非常有趣的新旅程！"
        },
        "1167": {
            "en": "Who says living healthily is hard? In our eyes, it can be stylish, hip, and full of sparks.",
            "cn": "健康生活绝不枯燥，在我们眼中，它是有趣的，充满时尚感和惊喜。"
        },
        "1168": {
            "en": "Finish your <a href=\"/account/basic\">VCleanse profile</a>.",
            "cn": "补充完整您的<a href=\"/account/basic\">信息</a>。"
        },
        "1169": {
            "en": "Place your <a href=\"order\" data-weave=\"widget/href/main\">first order</a>.",
            "cn": "来首次<a href=\"order\" data-weave=\"widget/href/main\">订购</a>。"
        },
        "1170": {
            "en": "Questions?",
            "cn": "有问题？"
        },
        "1171": {
            "en": "We are just one WeChat away!",
            "cn": "我们的客服在微信服务号替您解答。"
        },
        "1172": {
            "en": "Green is the new black. Health is the priceless luxury. Staying fit is the ultimate sexy.",
            "cn": "绿色正在重新诠释经典。健康是最无价的奢侈。保持活力是由内到外性感的最佳注脚。"
        },
        "1173": {
            "en": "Congrats!",
            "cn": "感谢您！"
        },
        "1174": {
            "en": "We have received your order.",
            "cn": "我们已经收到您的订单。"
        },
        "1175": {
            "en": "Click here",
            "cn": "点击这里"
        },
        "1176": {
            "en": "to see all of your orders.",
            "cn": "返回我的订单。"
        },
        "1177": {
            "en": "RESET PASSWORD",
            "cn": "重置密码"
        },
        "1178": {
            "en": "Account needs verification",
            "cn": "账号未经验证"
        },
        "1179": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1180": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1181": {
            "en": "Invalid email",
            "cn": "无效的邮箱地址"
        },
        "1182": {
            "en": "Invalid cell phone",
            "cn": "请填写正确的手机号码"
        },
        "1183": {
            "en": "Your first name",
            "cn": "请填写您的名字"
        },
        "1184": {
            "en": "Your last name",
            "cn": "请填写您的姓"
        },
        "1185": {
            "en": "Duplicated email",
            "cn": "该邮箱已被注册"
        },
        "1186": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1187": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1188": {
            "en": "Invalid host",
            "cn": "无效的服务"
        },
        "1189": {
            "en": "Too frequent",
            "cn": "操作太过频繁"
        },
        "1190": {
            "en": "Invalid validation code",
            "cn": "验证码错误"
        },
        "1191": {
            "en": "cellphone not exist",
            "cn": "该手机号不存在"
        },
        "1200": {
            "en": "Delivery window depends on the delivery area.",
            "cn": "配送时间段取决于配送地区。"
        },
        "1201": {
            "en": "Your invoice will be included with your delivery. Please check your juice package.",
            "cn": "您的发票将放在果汁包内一起发出，请注意查收！"
        },
        "1202": {
            "en": "VCLEANSE",
            "cn": "维果清饮品（上海）有限公司"
        },
        "1203": {
            "en": "Online ordering is now available for customers in Shanghai, Beijing, Tianjin, Jiangsu, Zhejiang. Customers from other cities are welcome to call us for delivery inquiries.",
            "cn": "目前江浙沪、京津地区已开通在线订购！其他地区的客户可通过客服热线咨询订购。"
        },
        "1204": {
            "en": "You can order {0} bottles at most.",
            "cn": "每天限量供应{0}瓶"
        },
        "1205": {
            "en": "If you would like to hear from us, please leave your information <a href=\"\/enrollment\/register\">here</a>.",
            "cn": "如您愿意收到维果清的最新资讯，请 <a href=\"\/enrollment\/register\">注册</a> 并留下您的信息。"
        },
        "1206": {
            "en": "Package via:",
            "cn": "外送公司："
        },
        "1207": {
            "en": "Zip code:",
            "cn": "邮编："
        },
        "1208": {
            "en": "Zip code",
            "cn": "邮编，选填"
        },
        "1209": {
            "en": "Pick more",
            "cn": "想要更多"
        },
        "1210": {
            "en": "I don't want package for this day",
            "cn": "我不要套餐"
        },
        "1211": {
            "en": "Please type the verification code we send to your mobile phone.",
            "cn": "请输入您的手机收到的验证码。"
        },
        "1212": {
            "en": "Please type a password, and then retype it to confirm.",
            "cn": "请输入密码。"
        },
        "1213": {
            "en": "Please retype password to confirm.",
            "cn": "请重新输入您的密码，以便确认。"
        },
        "1214": {
            "en": "These passwords don't match.",
            "cn": "两次输入的密码不匹配。"
        },
        "1215": {
            "en": "Phone number is required.",
            "cn": "手机号码是您的注册凭据，请输入。"
        },
        "1216": {
            "en": "Verify that you've entered the correct phone number.",
            "cn": "请确认您输入了正确的手机号码。"
        },
        "1217": {
            "en": "Have you previously ordered VCLEANSE by email or phone? If so, please click <a href=\"\/enrollment\/forgotpassword\"><strong>here</strong></a> to reset your password.",
            "cn": "您之前是否通过其他途径（邮件、电话）购买过维果清？如果是，请点击 <a href=\"\/enrollment\/forgotpassword\"><strong>这里</strong></a> 重置密码。"
        },
        "1218": {
            "en": "Package",
            "cn": "选择套餐"
        },
        "1219": {
            "en": "Cancel",
            "cn": "取消"
        },
        "1220": {
            "en": "ORDER NOW",
            "cn": "订购"
        },
        "1221": {
            "en": "Pay",
            "cn": "付款"
        },
        "1222": {
            "en": "The following payment platform are supported:",
            "cn": "在线支付目前支持如下支付平台："
        },
        "1223": {
            "en": "to place a new order.",
            "cn": "继续订购。"
        },
        "1224": {
            "en": "Your order has been successfully placed.",
            "cn": "您的订单已经成功订购。"
        },
        "1225": {
            "en": "If you have any questions, <br>call our customer service at",
            "cn": "需要帮助？欢迎拨打我们的客服热线"
        },
        "1226": {
            "en": "VCLEANSE Brochure",
            "cn": "维果清手册"
        },
        "1227": {
            "en": "VCLEANSE Checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1228": {
            "en": "VCLEANSE brochure",
            "cn": "维果清手册"
        },
        "1229": {
            "en": "VCLEANSE order details and pricing",
            "cn": "维果清订单详情与价格"
        },
        "1230": {
            "en": "VCLEANSE checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1231": {
            "en": "Click to download:",
            "cn": "点击下面的链接下载："
        },
        "1232": {
            "en": "Download is only available after you <a href=\"\/enrollment\/register\">sign in</a>. New here? <a href=\"\/enrollment\/register\">Sign Up</a>",
            "cn": "下载前请先<a href=\"\/enrollment\/register\">登录</a>。没有账户？<a href=\"\/enrollment\/register\">注册</a>"
        },
        "1233": {
            "en": "ECO VILLAGE STORE, Unit 111, 485 Fenlin Road, Shanghai",
            "cn": "上海市枫林路485号11号楼1室，靠近中山南二路"
        },
        "1234": {
            "en": "Please at least add one receiver address, or choose \"Pick up in store\".",
            "cn": "请至少添加一个收件人地址，或者选择 “我希望来门店自取”。"
        },
        "1235": {
            "en": "Total Amount (RMB):",
            "cn": "总价（元）："
        },
        "1236": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1237": {
            "en": "Product(Preferential):",
            "cn": "产品（优惠后）"
        },
        "1238": {
            "en": "Delivery(Preferential):",
            "cn": "配送（优惠后）"
        },
        "1239": {
            "en": "Apply promotion code",
            "cn": "输入优惠码"
        },
        "1240": {
            "en": "Have a promotional code? Enter it below (Not case-sensitive)",
            "cn": "拥有优惠码？请输入（不区分大小写）"
        },
        "1241": {
            "en": "APPLY",
            "cn": "输入"
        },
        "1242": {
            "en": "Please verify that you have typed the code correctly and that you have not used this promotion code previously.",
            "cn": "优惠码输入错误，请确认之前没有使用并重试。"
        },
        "1243": {
            "en": "Design your own cleanse program!",
            "cn": "每天一瓶绿，保证维生素摄入，提高抵抗力（可保存3天）；每天一瓶椰子水，保证补充电解质，延缓衰老（可保存3天）"
        },
        "1244": {
            "en": "About VCLEANSE",
            "cn": "关于维果清"
        },
        "1245": {
            "en": "Follow us:",
            "cn": "找到我们："
        },
        "1246": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1247": {
            "en": "Labor Day holiday notice!",
            "cn": "维果清五一劳动节放假通知！"
        },
        "1248": {
            "en": "Sure to delete this order?",
            "cn": "确定删除该订单吗？"
        },
        "1249": {
            "en": "Sure to delete this address?",
            "cn": "确定删除该地址吗？"
        },
        "1250": {
            "en": "No address",
            "cn": "无收件人地址"
        },
        "1251": {
            "en": "Dear V-Cleanser, you don't have a fully delivery address! <br><br>",
            "cn": "请完善您的收件人地址！<br><br>"
        },
        "1252": {
            "en": "Feedback on website?",
            "cn": "网站使用问题或建议？"
        },
        "1253": {
            "en": "Send Email to our technical support team:",
            "cn": "给我们的技术支持发送邮件："
        },
        "1254": {
            "en": "Tax invoice:",
            "cn": "发票信息："
        },
        "1255": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1256": {
            "en": "Invite code:",
            "cn": "邀请码："
        },
        "1257": {
            "en": "Invite code",
            "cn": "邀请码"
        },
        "1258": {
            "en": "Dear customer, our earth day activity has been fully booked. To continue order, please <a href=\"\/order\/package\">click here</a>! Or contact our customer service.",
            "cn": "尊敬的顾客，您好！我们地球日的活动已经订满了，无法继续订购。如果您还需要继续订购请<a href=\"\/order\/package\">点击这里</a>到首页完成订购！或者联系我们的客服！"
        },
        "1259": {
            "en": "Check your mobile number, please!",
            "cn": "您的手机号码输入有误，请核对！"
        },
        "1260": {
            "en": "Check your invite code, please!",
            "cn": "您的邀请码输入有误，请核对！"
        },
        "1261": {
            "en": "Check your input, please!",
            "cn": "您的输入有误，请核对！"
        },
        "1262": {
            "en": "Please remember your order number!",
            "cn": "请牢记您的订单号！"
        },
        "1263": {
            "en": "Amount Paid (RMB):",
            "cn": "已付金额（元）："
        },
        "1264": {
            "en": "You choose pay on delivery.",
            "cn": "您选择的付款方式为货到付款。"
        },
        "1265": {
            "en": "Back to Home",
            "cn": "回到首页"
        },
        "1266": {
            "en": "Sorry, earth day activity was closed on April 19. Now you can fill in the following form join our waitlist!",
            "cn": "很抱歉，活动订购已于4月19日截止。现在，您可以填写下面的表单将全绿系列加入心愿单，下一个全绿系列就是你的哦！"
        },
        "1267": {
            "en": "Join Waitlist",
            "cn": "加入心愿单"
        },
        "1268": {
            "en": "We have received your wish.",
            "cn": "我们已经收到您的心愿单。"
        },
        "1269": {
            "en": "Sorry, earth day activity was closed on April 19.",
            "cn": "很抱歉，活动订购已于4月19日截止。"
        },
        "1270": {
            "en": "Please verify that your promotion code can be applied to current order (check order subtotal).",
            "cn": "该优惠码不适用于当前订单价格。"
        },
        "1271": {
            "en": "Mobile is required",
            "cn": "请输入手机号"
        },
        "1272": {
            "en": "Password is required",
            "cn": "请输入密码"
        },
        "1273": {
            "en": "Sorry, your operating too frequently.",
            "cn": "抱歉，您的操作过于频繁。"
        },
        "1274": {
            "en": "Sorry, wrong validation code.",
            "cn": "抱歉，您输入的手机验证码错误。"
        },
        "1275": {
            "en": "First name is required",
            "cn": "请输入您的名字"
        },
        "1276": {
            "en": "Last name is required",
            "cn": "请输入您的姓氏"
        },
        "1277": {
            "en": "Email is required",
            "cn": "请输入您的电子邮件"
        },
        "1278": {
            "en": "Sorry, wrong mobile number.",
            "cn": "请输入正确的手机号码。（如果需要留下固话，请留在地址栏）"
        },
        "1279": {
            "en": "Password:",
            "cn": "密码："
        },
        "1280": {
            "en": "Successfully Ordered",
            "cn": "订购成功"
        },
        "1281": {
            "en": "Delivery Address & Start Date",
            "cn": "收件信息"
        },
        "1282": {
            "en": "Store",
            "cn": "在线商店"
        },
        "1283": {
            "en": "Cleanse",
            "cn": "清体套餐"
        },
        "1284": {
            "en": "Subscriptions",
            "cn": "清体计划"
        },
        "1285": {
            "en": "Gift Cards",
            "cn": "礼品卡"
        },
        "1286": {
            "en": "News & Events",
            "cn": "最新资讯"
        },
        "1287": {
            "en": "Healthy Office",
            "cn": "健康办公"
        },
        "1288": {
            "en": "Days",
            "cn": "天"
        },
        "1289": {
            "en": "Juices",
            "cn": "缤纷果饮"
        },
        "1290": {
            "en": "See All Of Our Products",
            "cn": "查看我们所有果汁"
        },
        "1291": {
            "en": "Spicy Lemonade",
            "cn": "每次配送加一瓶大师清体柠檬水"
        },
        "1292": {
            "en": "Fragrant Clear",
            "cn": "每次配送加一瓶芳香椰子水"
        },
        "1293": {
            "en": "All Products",
            "cn": "所有产品"
        },
        "1294": {
            "en": "Nutrition Center",
            "cn": "营养中心"
        },
        "1295": {
            "en": "About Us",
            "cn": "关于我们"
        },
        "1296": {
            "en": "Our Story",
            "cn": "关于维果清"
        },
        "1297": {
            "en": "Philosophy",
            "cn": "公司理念"
        },
        "1298": {
            "en": "Detox 101",
            "cn": "Detox 101"
        },
        "1299": {
            "en": "Detox Quiz",
            "cn": "Detox Quiz"
        },
        "1300": {
            "en": "Q&amp;A",
            "cn": "问答"
        },
        "1301": {
            "en": "Survial Guide",
            "cn": "Survial Guide"
        },
        "1302": {
            "en": "Store Locations",
            "cn": "门店地址"
        },
        "1304": {
            "en": "Join VCLEANSE",
            "cn": "加入维果清"
        },
        "1305": {
            "en": "Black",
            "cn": "黑"
        },
        "1306": {
            "en": "Green",
            "cn": "绿"
        },
        "1307": {
            "en": "White",
            "cn": "白"
        },
        "1308": {
            "en": "Red",
            "cn": "红"
        },
        "1309": {
            "en": "Yellow",
            "cn": "黄"
        },
        "1310": {
            "en": "New customer, signup!",
            "cn": "新客户，注册！"
        },
        "1311": {
            "en": "BACK TO CLEANSE",
            "cn": "回到清体套餐"
        },
        "1312": {
            "en": "CLEANSES",
            "cn": "在线选购"
        },
        "1313": {
            "en": "Already had an account? Please <a class=\"link-fn\" data-action=\"signin\">sign in</a><br><br>You can place your oder successfully as a guest, but you can track your order status and change your oders more freely only if you own a Vcleanse account.<a href=\"/enrollment/register\" data-weave=\"widget/href/main\">Register Now</a>",
            "cn": "拥有维果清账号？请<a class=\"link-fn\" data-action=\"signin\">登录</a>。<br><br>您也可以直接购买，拥有维果清账号可以帮助您更好地追踪和管理您的订单。<a href=\"/enrollment/register\" data-weave=\"widget/href/main\">立即注册</a>"
        },
        "1314": {
            "en": "GG",
            "cn": "GG"
        },
        "1315": {
            "en": "No Matching Event Id",
            "cn": "目前无活动"
        },
        "1316": {
            "en": "Event has not started",
            "cn": "活动未开始"
        },
        "1317": {
            "en": "Event has already ended",
            "cn": "活动已结束"
        },
        "1318": {
            "en": "Enter any referral codes",
            "cn": "有“团体/VIP”号码？请输入（作为订单来源凭据使用，输入即可）"
        },
        "1319": {
            "en": "(free shipping)",
            "cn": "（包邮）"
        },
        "1340": {
            "cn": "查看详情",
            "en": "Details"
        },
        "1341": {
            "en": "Sending...",
            "cn": "发送中..."
        },
        "1342": {
            "en": "SEND AGAIN",
            "cn": "再次发送"
        },
        "1343": {
            "en": "CAN'T GET VERIFICATION CODE? SKIP!",
            "cn": "无法获取短信验证码？跳过手机验证！"
        },
        "1344": {
            "en": "Input verification code",
            "cn": "输入右侧图片中验证码"
        },
        "1345": {
            "en": "Please type the verification code.",
            "cn": "请输入验证码。"
        },
        "1346": {
            "en": "Delivery Address",
            "cn": "收件人地址"
        },
        "1347": {
            "en": "Delivery Date",
            "cn": "配送时间"
        },
        "1348": {
            "en": "SAVE CHANGES",
            "cn": "保存更改"
        },
        "1349": {
            "en": "GO BACK",
            "cn": "返回"
        },
        "1350": {
            "en": "Find My Orders",
            "cn": "查询订单"
        },
        "1351": {
            "en": "Users Instrutions",
            "cn": "套餐使用建议"
        },
        "1352": {
            "en": "Cleanse aficionados can design their own custom cleanse.  Please select the flavors you would like.",
            "cn": "随心所欲，定制属于你自己的清体达人套餐。"
        },
        "1353": {
            "en": "SEARCH",
            "cn": "搜索"
        },
        "1354": {
            "en": "Search by the mobile phone number you entered with your order",
            "cn": "根据收件人手机号码查询"
        },
        "1355": {
            "en": "New Customers?",
            "cn": "没有账户？"
        },
        "1356": {
            "en": "You do not need to create an account to checkout. You can checkout as a guest and create an account later.",
            "cn": "您可以以访客的身份继续浏览或者完成订购。"
        },
        "1357": {
            "en": "Continue as Guest",
            "cn": "以访客身份继续"
        },
        "1358": {
            "en": "Payment and Invoice",
            "cn": "支付及发票"
        },
        "1359": {
            "en": "to search my orders",
            "cn": "查询我的订单。"
        },
        "1360": {
            "en": "Order create failed!",
            "cn": "订单创建失败。"
        },
        "1361": {
            "en": "Cold Pressed",
            "cn": "冷榨液压"
        },
        "1362": {
            "en": "No records.",
            "cn": "该手机号码没有订单记录。"
        },
        "1363": {
            "en": "Wechat:",
            "cn": "微信人工客服："
        },
        "1364": {
            "en": "Scan qr-code!",
            "cn": "扫描二维码，随时随地语音咨询！"
        },
        "1365": {
            "en": "Page has expired. Please refresh page and try again!",
            "cn": "页面已过期，刷新页面后请重试。"
        },
        "1366": {
            "en": "Order has been locked. You can't modify!",
            "cn": "订单已经锁定，不能修改。"
        },
        "1367": {
            "en": "Invalid promotion code.",
            "cn": "无效的优惠码。"
        },
        "1368": {
            "en": "This promotion code has expired.",
            "cn": "该优惠码已过期。"
        },
        "1369": {
            "en": "Invalid order.",
            "cn": "无效的订单号。"
        },
        "1370": {
            "en": "Thanks for using promotion code.",
            "cn": "感谢您使用优惠码。"
        },
        "1371": {
            "en": "Free delivery for this order.",
            "cn": "运费已减免。"
        },
        "1372": {
            "en": "Free \"Fragrant Clear\" everyday for this order.",
            "cn": "已经为该订单添加免费的椰子水。"
        },
        "1373": {
            "en": "Free delivery and free \"Fragrant Clear\" everyday for this order.",
            "cn": "您的订单已经免运费，送椰子水。"
        },
        "1374": {
            "en": "Learn about <a href=\"/juice/index\">our juices</a> here.",
            "cn": "快了解一下我们的<a href=\"/juice/index\">缤纷果汁</a>吧。"
        },
        "1375": {
            "en": "To protect your privacy, we need to verify the phone number.",
            "cn": "为了保护您的隐私，我们需要验证手机号码。"
        },
        "1376": {
            "en": "Your payment failed.",
            "cn": "付款失败。"
        },
        "1377": {
            "en": "Sorry!",
            "cn": "抱歉！"
        },
        "1378": {
            "en": "Pay failed",
            "cn": "付款失败"
        },
        "1379": {
            "en": "Select a shipping address",
            "cn": "物流配送"
        },
        "1380": {
            "en": "Shanghai",
            "cn": "上海"
        },
        "1381": {
            "en": "View map",
            "cn": "查看地图"
        },
        "1382": {
            "en": "Business hours:",
            "cn": "营业时间："
        },
        "1383": {
            "en": "Leave your contact information",
            "cn": "留下您的联系方式"
        },
        "1384": {
            "en": "Free \"Spicy Lemonade\" everyday for this order.",
            "cn": "已经为该订单添加免费的柠檬水。"
        },
        "1385": {
            "en": "Free delivery and free \"Spicy Lemonade\" everyday for this order.",
            "cn": "您的订单已经免运费，送柠檬水。"
        },
        "1386": {
            "en": "Dear V-Cleanser, you don't have a fully contact information! <br><br>",
            "cn": "请完善您的联系方式！<br><br>"
        },
        "1387": {
            "en": "Contact tel:",
            "cn": "联系电话："
        },
        "1388": {
            "en": "This code has used",
            "cn": "优惠码已经使用"
        },
        "1389": {
            "en": "Has use disposable code",
            "cn": "已经使用过一次性优惠码了"
        },
        "1390": {
            "en": "Anonymous cannot uset his rule",
            "cn": "匿名用户不能用,请登录后重新下单"
        },
        "1391": {
            "en": "Sorry You have used this type of promotion",
            "cn": "对不起, 你的账户已经使用过这种类型的优惠码"
        },
        "1392": {
            "en": "Sure to use this code",
            "cn": "是否使用该优惠码？"
        },
        "1393": {
            "en": "Sorry, this activity was ended. Please choose our regular packages!",
            "cn": "很抱歉，活动订购已截止。您可以选择我们的常规套餐！"
        },
        "1394": {
            "en": "Package Detail",
            "cn": "套餐详情"
        },
        "1395": {
            "en": "Receiver name:",
            "cn": "收件人姓名："
        },
        "1396": {
            "en": "Receiver's name is required",
            "cn": "收件人真实姓名，必填"
        },
        "1397": {
            "en": "Name:",
            "cn": "联系人姓名："
        },
        "1398": {
            "en": "Pickup contactor's name is required",
            "cn": "自取联系人真实姓名，必填"
        },
        "1399": {
            "en": "From",
            "cn": "从"
        },
        "1400": {
            "en": "to",
            "cn": "至"
        },
        "1401": {
            "en": "password:",
            "cn": "密码："
        },
        "1402": {
            "en": "Your first name here",
            "cn": "您的名字"
        },
        "1403": {
            "en": "Subscribe",
            "cn": "订阅"
        },
        "1404": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，新鲜资讯抢先看"
        },
        "1405": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "订阅成功！"
        },
        "1406": {
            "en": "Duplicated email",
            "cn": "该邮箱已被订阅"
        },
        "1407": {
            "en": "Your email address has an invalid email address format. Please correct and try again.",
            "cn": "电子邮件格式有误。请尝试重新输入！"
        },
        "1408": {
            "en": "Your mobile number has an invalid format. Please correct and try again.",
            "cn": "手机格式有误。请尝试重新输入！"
        },
        "1409": {
            "en": "Order confirmation has been sent to your mobile via SMS, please check!",
            "cn": "订单确认信息已经通过短信发送到您的注册手机号，请注意查收！"
        },
        "1410": {
            "en": "Leave your email to get order information",
            "cn": "留下邮箱，获取订单信息，新鲜资讯抢先看！"
        },
        "1411": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，维果清新鲜资讯抢先看"
        },
        "1412": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "您已经成功订阅维果清资讯！"
        },
        "1451": {
            "en": "Check all online payment methods",
            "cn": "查看全部在线支付方式"
        },
        "1452": {
            "en": "I understand",
            "cn": "我知道了"
        },
        "1454": {
            "en": "Payment completed",
            "cn": "已经完成支付"
        },
        "1455": {
            "en": "Have a problem",
            "cn": "支付遇到问题"
        },
        "1456": {
            "en": "Comfirm",
            "cn": "付款确认"
        },
        "1457": {
            "en": "Close",
            "cn": "关闭"
        },
        "1458": {
            "en": "Pay now",
            "cn": "去付款"
        },
        "1459": {
            "en": "V Station",
            "cn": "维果清门店"
        },
        "1460": {
            "en": "Quantity",
            "cn": "选择购买数量"
        },
        "1461": {
            "en": "Account Safety",
            "cn": "账户安全"
        },
        "1462": {
            "en": "Failed",
            "cn": "更新失败"
        },
        "1463": {
            "en": "Change",
            "cn": "换一个"
        },
        "1464": {
            "en": "Code here",
            "cn": "输入验证码"
        },
        "1465": {
            "en": "Successfully change password!",
            "cn": "成功更新密码！"
        },
        "1466": {
            "en": "Please hoover your mouse to the image below, and click on the button on the top right to share this on WeChat and Weibo!",
            "cn": "将鼠标移到下图上，点击右上角的图标，把这份温暖分享到微信、微博吧！"
        },
        "1467": {
            "en": "* Delivery fee of RMB 25 is not included.",
            "cn": "* 每日25元运费另计。"
        },
        "1468": {
            "en": "Only for warming day",
            "cn": "优惠码只能对暖体日使用."
        },
        "1469": {
            "en": "This area is not available",
            "cn": "该地区暂时不能配送"
        },
        "1470": {
            "en": "Edit address failed!",
            "cn": "地址修改失败!"
        },
        "1471": {
            "en": "The new address will generate additional freight!",
            "cn": "要修改的地址将产生额外运费!"
        },
        "1472": {
            "en": "Order Review",
            "cn": "查看订单"
        },
        "1473": {
            "en": "Mine",
            "cn": "我的"
        },
        "1474": {
            "en": "Others",
            "cn": "其它"
        },
        "1475": {
            "en": "Delivery date:",
            "cn": "配送日期："
        },
        "1476": {
            "en": "Delivery address:",
            "cn": "配送地址："
        },
        "1477": {
            "en": "Detailed list:",
            "cn": "清单："
        },
        "1478": {
            "en": "Status:",
            "cn": "状态："
        },
        "1479": {
            "en": "The child orders",
            "cn": "子订单"
        },
        "1480": {
            "en": "Day",
            "cn": "天"
        },
        "1481": {
            "en": "Invalid payment request!",
            "cn": "无效的付款请求。"
        },
        "1482": {
            "en": "Purple Broth",
            "cn": "每次配送加一瓶紫甘蓝靓汤"
        },
        "1483": {
            "en": "Workshops",
            "cn": "维果清工作坊"
        },
        "1484": {
            "en": "Individual",
            "cn": "个人报名入口"
        },
        "1485": {
            "en": "Group or Corporate",
            "cn": "团体报名入口"
        },
        "1600": {
            "en": "SUBSCRIPTIONS",
            "cn": "预订"
        },
        "1601": {
            "en": "SELECT",
            "cn": "选择"
        },
        "1602": {
            "en": "Free \"Coconut Water\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶椰子水。"
        },
        "1603": {
            "en": "Free \"Spicy Lemonade\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶柠檬水。"
        },
        "1604": {
            "en": "Free \"Moon Bear's Honey Tree\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶月熊蜂蜜树。"
        },
        "1605": {
            "en": "Not available on the day",
            "cn": "当天不供应"
        },
        "1606": {
            "en": "This is a gift",
            "cn": "这是礼品"
        },
        "1607": {
            "en": "Gift Blessing",
            "cn": "礼品赠言"
        },
        "1608": {
            "en": "You have to order:",
            "cn": "您已订购："
        },
        "1609": {
            "en": "Products Info",
            "cn": "产品详情"
        },
        "1610": {
            "en": "Submit",
            "cn": "提交"
        },
        "1611": {
            "en": "Thanks for your support!",
            "cn": "谢谢您的参与！"
        },
        "1612": {
            "en": "Please enter...",
            "cn": "请输入..."
        },
        "1613": {
            "en": "(Multiple Choice)",
            "cn": "（多选题）"
        },
        "1614": {
            "en": "Image verification code:",
            "cn": "图片验证码："
        },
        "1615": {
            "en": "VERIFY AND SEARCH",
            "cn": "验证并查询"
        },
        "1616": {
            "en": "to:",
            "cn": "到："
        },
        "1617": {
            "en": "Security code:",
            "cn": "手机验证码："
        },
        "1618": {
            "en": "Not paid yet",
            "cn": "未支付"
        },
        "1619": {
            "en": "Paid",
            "cn": "已支付"
        },
        "1620": {
            "en": "Action",
            "cn": "操作"
        },
        "1621": {
            "en": "Details",
            "cn": "查看"
        },
        "1622": {
            "en": "Hotline[9AM - 7PM]:",
            "cn": "客服热线[上午9点 - 晚上7点]："
        },
        "1623": {
            "en": "Invalid shipping address",
            "cn": "无效的收件地址"
        },
        "1624": {
            "en": "This order cannot be deleted, please contact customer service.",
            "cn": "该订单无法删除，请联系客服。"
        },
        "1625": {
            "en": "You have ordered before, you can not use the coupon code.",
            "cn": "您已经有过订单，无法使用该优惠码"
        }

    });
    /* String help function */
    // Format: "Hello, {0}. {1}".format("Mr","DJ")

    function stringFormat(s, args) {
        var i = 0;
        var iLens = args.length;
        for (i = 0; i < iLens; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, args[i]);
        }
        return s;
    }

    return Widget.extend(function($element, widgetName, blurbId, attr) {
        var me = this;

        me.blurbId = blurbId;
        me.attr = attr || "";
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var format = $me.data("format");

            var translation = me.get(me.blurbId, format);

            if (!translation) {
                return;
            }

            // Attr or text
            if (me.attr) {
                $me.attr(me.attr, translation);
            } else {
                me.html(translation);
            }

            // Log blurb ID
            //$me.attr("blurb", me.blurbId);
        },
        "get": function(blurbId, format) {
            var me = this;

            var language = context.language || "en";
            var translationSet;
            var translation = "";

            if (!blurbId) {
                return "";
            }

            translationSet = blurbDict[blurbId.toString()];

            if (!translationSet) {
                return "";
            }

            if (context.isDebug) {
                translation += "[" + blurbId + "]";
            }
            translation += translationSet[language];

            // format: "x, y, z" --> ['x', 'y', 'z']
            // Replace {0} {1}
            if (translation.indexOf("{0}") >= 0 && format !== undefined) {
                format = format.toString().split(",");
                translation = stringFormat(translation, format);
            }

            return translation;
        }
    });
});