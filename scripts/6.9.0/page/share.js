﻿(function(_win) {

    var stopRun = false;

    _win.attachWeixinJSBridgeReady = function(imgUrl, lineLink, descContent, shareTitle, appid) {

        if (stopRun) {
            return;
        }

        imgUrl = imgUrl || "http://www.vcleanse.com/images/wechat/logo_wechat.jpg";
        lineLink = lineLink || "http://m.vcleanse.com/";
        descContent = descContent || "维果清以纯天然食物，清体果蔬汁为本，结合中西营养学精华，倡导健康饮食的全新生活方式!";
        shareTitle = shareTitle || "维果清 VCLEANSE - 真正的冷压清体蔬果汁";
        appid = appid || "";

        function shareFriend() {
            WeixinJSBridge.invoke("sendAppMessage", {
                "appid": appid,
                "img_url": imgUrl,
                "img_width": "640",
                "img_height": "640",
                "link": lineLink,
                "desc": descContent,
                "title": shareTitle
            }, function(res) {
                _report("send_msg", res.err_msg);
            });
        }

        function shareTimeline() {
            WeixinJSBridge.invoke("shareTimeline", {
                "img_url": imgUrl,
                "img_width": "640",
                "img_height": "640",
                "link": lineLink,
                "desc": descContent,
                "title": shareTitle
            }, function(res) {
                _report("timeline", res.err_msg);
            });
        }

        function shareWeibo() {
            WeixinJSBridge.invoke("shareWeibo", {
                "content": descContent,
                "url": lineLink
            }, function(res) {
                _report("weibo", res.err_msg);
            });
        }

        // 当微信内置浏览器完成内部初始化后会触发WeixinJSBridgeReady事件。  
        document.addEventListener("WeixinJSBridgeReady", function onBridgeReady() {
            // 发送给好友  
            WeixinJSBridge.on("menu:share:appmessage", function(argv) {
                shareFriend();
            });
            // 分享到朋友圈  
            WeixinJSBridge.on("menu:share:timeline", function(argv) {
                shareTimeline();
            });
            // 分享到微博  
            WeixinJSBridge.on("menu:share:weibo", function(argv) {
                shareWeibo();
            });
        }, false);

        // Only run one time
        stopRun = true;

    };

})(this);