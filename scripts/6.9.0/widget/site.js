define('widget/api/main',["jquery",
	"context"
], function($, context) {
	"use strict";

	var serviceDomain = context.serviceDomain;
	var apiDict = {
		// Account
		"account": serviceDomain + "/personal/myregisterinfo",
		"account_create": serviceDomain + "/account/regist",
		"account_update": serviceDomain + "/personal/updatemyinfo",
		"account_chgpwd": serviceDomain + "/personal/changepassword",
		"emailcheck": serviceDomain + "/dataprovider/isemailduplicated",
		"phonecheck": serviceDomain + "/dataprovider/isphoneduplicated",
		"sendverificationcode": serviceDomain + "/account/sendvalidationcodebyweb",
		"login": serviceDomain + "/account/logon",
		"forgotpasswordverificationcode": serviceDomain + "/enrollment/sendvalidationcode",
		"resetpassword": serviceDomain + "/enrollment/resetpassword",
		// Address
		"region": serviceDomain + "/dataprovider/region",
		"region_with_province": serviceDomain + "/dataprovider/RegionWithProvince",
		"receiver": serviceDomain + "/personal/myreceivers",
		"receiver_create": serviceDomain + "/personal/addreceiver",
		"receiver_delete": serviceDomain + "/personal/deletereceiver",
		"receiver_update": serviceDomain + "/personal/updatereceiver",
		"receiver_setdefault": serviceDomain + "/personal/setasdefaultreceiver",
		// Order
		"package_category": serviceDomain + "/dataprovider/PackageCategory",
		"package_loadbycategory": serviceDomain + "/dataprovider/LoadPackageByCategoryId",
		"package": serviceDomain + "/dataprovider/package",
		"package_loadbyid": serviceDomain + "/dataprovider/LoadPackageByPackageId",
		"product": serviceDomain + "/dataprovider/products",
		"deliver_time": serviceDomain + "/dataprovider/loaddelivertime",
		"deliver_timebyarea": serviceDomain + "/dataprovider/LoadDeliverTimeByArea",
		"payment": serviceDomain + "/dataprovider/loadpayment",
		"orders": serviceDomain + "/order/loadmyorders",
		"orders_bymobile": serviceDomain + "/order/LoadOrderByMobile",
		"order_create": serviceDomain + "/order/createorder",
		"order_confirm": serviceDomain + "/order/confirmorder",
		"order_promotion_code": serviceDomain + "/order/applypromotion",
		"orderdetail": serviceDomain + "/order/loadorderbyid",
		"order_delete": serviceDomain + "/order/deleteorder",
		"order_updatereceiver": serviceDomain + "/order/UpdateOrdersmReceiver",
		"summary":serviceDomain+"/buy/summary",
		// Juices
		"product_category": serviceDomain + "/dataprovider/ProductCategory",
		"product_loadbycategory": serviceDomain + "/dataprovider/LoadProductsByCategoryId",
		"product_loadbyday": serviceDomain + "/dataprovider/LoadProductsByDayId",
		// Stores
		"stores": serviceDomain + "/dataprovider/LoadAllStores",
		// Events
		"quick_order": serviceDomain + "/order/CreateEventOrder",
		"load_survey": serviceDomain + "/Survey/LoadSurveyById",
		"save_survey": serviceDomain + "/Survey/Save",
		"report_survey": serviceDomain + "/Survey/Report",
		// Email subscribebscript
		"subscribe": serviceDomain + "/Subscription/Subscribe",
		"un_subscribe": serviceDomain + "/Subscription/Unsubscribe"
	};

	// For local test environment
	if (!serviceDomain) {
		$.extend(apiDict, {
			// Account
			"account": "/api/account.json",
			"login": "/api/logon.json",
			// Address
			"region": "/api/region.json",
			"region_with_province": "/api/RegionWithProvince.json",	//serviceDomain + "/dataprovider/RegionWithProvince"
			"receiver": "/api/receiver.json",
			"receiver_create": "/api/addreceiver.json",//serviceDomain + "/personal/addreceiver",
//			"receiver_delete": serviceDomain + "/personal/deletereceiver",
			"receiver_update": "/api/updatereceiver.json",//serviceDomain + "/personal/updatereceiver",
//			"receiver_setdefault": serviceDomain + "/personal/setasdefaultreceiver",
			// Order
			"package_category": "/api/PackageCategory.json",
			"package_loadbycategory": "/api/LoadPackageByCategoryId.json",
			"package": "/api/package.json",
			"package_loadbyid": "/api/LoadPackageByPackageId.json",
			"product": "/api/products.json",
			"deliver_time": "/api/loaddelivertime.json",
			"deliver_timebyarea": "/api/LoadDeliverTimeByArea.json",
			"order_create": "/api/createorder.json",
			"payment": "/api/LoadPayment.json",
			"order_confirm": "/api/confirmorder.json",//serviceDomain + "/order/confirmorder",
			"rder_promotion_code": "/api/applypromotion.json",
			// Juices
			"product_category": "/api/ProductCategory.json",
			"product_loadbycategory": "/api/LoadProductsByCategoryId.json",

			"orders": "/api/loadmyorders.json",
			"orderdetail": "/api/loadorderinfo.json",
			// Stores
			"stores": "/api/LoadAllStores.json"
				
		});
	}

	return {
		"get": function(key) {
			key = key.toLowerCase();

			return apiDict[key];
		}
	};

});

define('troopjs-requirejs/template!widget/loading/index.html',[],function() { return function template(data) { var o = "<div class=\"loading-spinner\">\r\n    <div class=\"loading-spinner-logo\">\r\n        <div class=\"loading-spinner-icon\"></div>\r\n    </div>\r\n    <p></p>\r\n</div>"; return o; }; });
define('widget/loading/main',["troopjs-core/component/gadget",
    "jquery",
    "template!./index.html"
], function(Gadget, $, template) {
    "use strict";

    return Gadget.extend(function($el) {
        var me = this;
        me.$el = $el;
    }, {
        "sig/start": function() {
            var me = this;

            if (!me.$el || me.$el.length <= 0) {
                return;
            }

            me.$el.html(template);
        },
        "error": function(msg){
            var me = this;

            var $loadingSpinner = me.$el.find(".loading-spinner");
            $loadingSpinner.addClass("error");

            // Append error msg
            if(msg){
                $loadingSpinner.find("> p").text(msg);
            }
        }
    });
});
define('widget/ajaxquery/main',["jquery",
    "when",
    "context",
    "widget/loading/main"
], function($, when, context, Loading) {
    "use strict";

    return function(option, $el) {
        var me = this;
        var deferred = when.defer();

        var loading;
        if ($el && $el.length > 0) {
            loading = new Loading($el);
        }

        // Parameters check
        if (!option) {
            console.log('Ajax call without option!');
            deferred.reject(new Error('Ajax call without option!'));
        }
        if (!option.url) {
            console.log('Ajax call without url!');
            deferred.reject(new Error('Ajax call without url!'));
        }

        // Apply cachekey while start a HTTP GET request
        if (option.type.toUpperCase() === "GET" && context.serviceCacheKey) {
            // Add version
            $.extend(true, option, {
                data: {
                    "v": context.serviceCacheKey
                }
            });
        } /*else if (option.type.toUpperCase() === "POST") {
            // Add content type
            // Only JSON format as post data
            $.extend(true, option, {
                contentType: "application/json; charset=UTF-8"
            });
        }*/
        // For local test environment
        if (!context.serviceDomain) {
            // USE HTTP GET
            $.extend(true, option, {
                type: "GET"
            });
        }

        // Ajax
        var request = $.ajax(option);

        function ajaxReturnHandler() {
            request.done(function(data) {
                var statusCode;
                var description;
                if (data && data.IsSuccess) {
                    deferred.resolve(data);
                } else {
                    statusCode = data.StatusCode || -999;
                    description = data.Description || "";
                    if (loading) {
                        loading.error("Service with error! [" + statusCode + "]");
                    }
                    deferred.reject(statusCode);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                if (loading) {
                    loading.error("Service with error!");
                }

                deferred.reject(new Error('Ajax call failed.'));
            });
        }

        // Add loading icon
        if (loading) {
            loading.start().then(ajaxReturnHandler);
        } else {
            // Execute ajax result
            ajaxReturnHandler();
        }

        return deferred.promise;
    };

});
define('widget/cookie/main',["jquery",
	"when",
	"jquery.cookie"
], function($, when) {
	"use strict";

	// Cookie dictionary, use lowercase for all key
	var cookieDict = {
		"account": "Account", // user name
		"memberid": "MemberId", // user id
		"lng": "Language",
		"pkg_group_1": "Pkg_group_1", // ~ detox (male / female)
		"pkg_group_2": "Pkg_group_2", // ~ slim (male / female)
		"pkg_group_3": "Pkg_group_3", // ~ health (male / female)
		"pkg_group_5": "Pkg_group_5", // ~ detox (male / female)
		"pkg_group_6": "Pkg_group_6", // ~ slim (male / female)
		"pkg_group_7": "Pkg_group_7", // ~ health (male / female)
		"pkg_group_8": "Pkg_group_8", // ~ detox (male / female)
		"pkg_group_9": "Pkg_group_9", // ~ slim (male / female)
		"pkg_group_10": "Pkg_group_10", // ~ health (male / female)
		"hide_announcement": "Hide_announcement",
		"customize": "Customize",
		"subscription_cust": "Subscription_cust",
		"search": "Search",
		"gift_info": "Gift_info"
	};

	return {
		"set": function(key, value, opt) {
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			if (value === undefined) {
				return when.reject("value is undefined");
			}

			key = cookieDict[key.toLowerCase()];
			if (!key) {
				return when.reject("key is not registed");
			}

			$.cookie(key, value, opt);

			return when.resolve();
		},
		"get": function(key) {
			//Read all available cookies
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			key = cookieDict[key.toLowerCase()];

			var val = key ? $.cookie(key) : "";

			return val ? when.resolve(val) : when.reject("No cookie for " + key);
		},
		"getVal": function(key) {
			//Read all available cookies
			if (key === undefined) {
				return;
			}
			key = cookieDict[key.toLowerCase()];

			var val = key ? $.cookie(key) : "";

			return val;
		},
		"getAll": function() {
			return when.resolve($.cookie());
		},
		"rm": function(key, opt) {
			if (key === undefined) {
				return when.reject("key is undefined");
			}
			key = cookieDict[key.toLowerCase()];
			if (!key) {
				return when.reject("key is not registed");
			}

			$.removeCookie(key, opt);

			return when.resolve();
		}
	};

});

define('troopjs-requirejs/template!widget/popupbox/index.html',[],function() { return function template(data) { var o = "<div class=\"popupbox-cover\" style=\"\r\n    position: " + (data.position || "fixed") + ";\r\n    z-index: " + (data.zIndex || 1) + ";\r\n    background-color: " + (data.bgColor || "#000") + ";\r\n    opacity: " + (data.opacity || 0.8) + ";\r\n    _filter: alpha(opacity=" + (data.opacity || 0.8) * 100 + ");\r\n    margin-top: " + (data.top || 0) + "px;\r\n\">\r\n    "; if (data.keySupport) { o += "\r\n        <style>\r\n            input.popupbox-keysupport\r\n            {\r\n                width: 1px;\r\n                height: 1px;\r\n                border-width: 0;\r\n                overflow: hidden;\r\n                background-color: transparent;\r\n                outline: none;\r\n                position: absolute;\r\n                top: -1px;\r\n            }\r\n        </style>\r\n        <input class=\"popupbox-keysupport\" type=\"text\" />\r\n    "; } o += "\r\n</div>\r\n<div class=\"popupbox\" style=\"\r\n    position: " + (data.position || "fixed") + ";\r\n    z-index: " + (data.zIndex || 1) + ";\r\n    margin-top: " + (data.top || 0) + "px;\r\n\">\r\n    <div class=\"popupbox-main\" "; if(data.closeble){ o += "data-action=\"closepopupbox\""; } o += " style=\"\r\n        height: 100%;\r\n        "; if(!data.fullSize) { o += "\r\n            text-align: center;\r\n        "; } o += "\r\n    \">\r\n        "; if(!data.isInnerClose) { closeButton(data.hasCloseButton); } o += "\r\n        <div class=\"popupbox-content popupbox-out\" style=\"\r\n            "; if(!data.fullSize) { o += "\r\n                text-align: left;\r\n                display: inline-block;\r\n                *display: inline;\r\n                *zoom: 1;\r\n            "; } o += "\r\n        \">\r\n            "; if(data.isInnerClose) { closeButton(data.hasCloseButton); } o += "\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n"; function closeButton(exist) { o += "\r\n    "; if (exist) { o += "\r\n        <style>\r\n            .popupbox-close\r\n            {\r\n                position: absolute;\r\n                right: 0;\r\n                top: 0;\r\n                cursor: pointer;\r\n            }\r\n        </style>\r\n        <a class=\"popupbox-close hidden-print\" title=\"Close\">&times;</a>\r\n    "; } o += "\r\n"; } o += "\r\n<style>\r\n    style\r\n    {\r\n        display: none;\r\n    }\r\n    /*\r\n    * Animation for pop effect\r\n    */\r\n    /* No animation */\r\n    .popupbox-in\r\n    {\r\n        visibility: visible;\r\n    }\r\n    .popupbox-out\r\n    {\r\n        visibility: hidden;\r\n    }\r\n    /* With animation */\r\n    .popupbox-pop {\r\n        -webkit-transform-origin: 50% 50%;\r\n        -moz-transform-origin: 50% 50%;\r\n    }\r\n    .popupbox-pop.popupbox-in {\r\n        visibility: visible;\r\n        opacity: 1;\r\n        box-shadow: 0 0 30px #666;\r\n        -webkit-transform: scale(1);\r\n\r\n        -webkit-animation-name: popin;\r\n        -moz-animation-name: popin;\r\n        -webkit-animation-duration: 200ms;\r\n        -moz-animation-duration: 200ms;\r\n        -webkit-animation-timing-function: ease-out;\r\n        -moz-animation-timing-function: ease-out;\r\n    }\r\n    .popupbox-pop.popupbox-out {\r\n        visibility: visible;\r\n        opacity: 0;\r\n        -webkit-transform: scale(.8);\r\n\r\n        -webkit-animation-name: popout;\r\n        -moz-animation-name: popout;\r\n        -webkit-animation-duration: 100ms;\r\n        -moz-animation-duration: 100ms;\r\n        -webkit-animation-timing-function: ease-in;\r\n        -moz-animation-timing-function: ease-in;\r\n    }\r\n    @-webkit-keyframes popin {\r\n        from {\r\n            -webkit-transform: scale(.8);\r\n            opacity: 0;\r\n        }\r\n        to {\r\n            -webkit-transform: scale(1);\r\n            opacity: 1;\r\n        }\r\n    }\r\n    @-webkit-keyframes popout {\r\n        from {\r\n            -webkit-transform: scale(1);\r\n            opacity: 1;\r\n        }\r\n        to {\r\n            -webkit-transform: scale(.8);\r\n            opacity: 0;\r\n        }\r\n    }\r\n    /* evc style */\r\n    .popupbox-cover,\r\n    .popupbox\r\n    {\r\n        position: fixed;\r\n        left: 0;\r\n        top: 0;\r\n        width: 100%;\r\n        height: 100%;\r\n        overflow: hidden;\r\n    }\r\n    .popupbox-main,\r\n    .popupbox-content\r\n    {\r\n        position: relative;\r\n    }\r\n    .popupbox-main\r\n    {\r\n        overflow-y: auto;\r\n        overflow-x: hidden;\r\n    }\r\n    .popupbox-content\r\n    {\r\n        -webkit-transition: margin-top .3s ease-out;\r\n        -moz-transition: margin-top .3s ease-out;\r\n        -o-transition: margin-top .3s ease-out;\r\n        transition: margin-top .3s ease-out;\r\n    }\r\n</style>"; return o; }; });
define('widget/popupbox/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "template!./index.html"
], function(Widget, weave, $, template) {
    "use strict";

    function keySupport() {
        var ua = navigator.userAgent;
        return ua.indexOf("Windows NT") >= 0 || ua.indexOf("Macintosh") >= 0;
    }

    function cssAnimate($el, noAnimation) {
        if (!$el || $el.length <= 0) {
            return {
                "popIn": $.noop,
                "popOut": $.noop
            };
        }
        // Add animate CSS?
        if (noAnimation) {
            $el.removeClass("popupbox-pop");
        } else {
            $el.addClass("popupbox-pop");
        }

        return {
            "popIn": function() {
                $el.removeClass("popupbox-out").addClass("popupbox-in");
            },
            "popOut": function() {
                $el.removeClass("popupbox-in").addClass("popupbox-out");
            }
        };
    }

    return Widget.extend(function(args) {
        var me = this;
        // Arguments
        me.el = args.$el || "body";
        me.msg = args.msg || "";
        // overLap: "cancel"/"replace"/"overlap"
        me.overLap = args.overLap || "replace";
        // stick at top of the popup
        me.stick = args.stick || false;
        // In stick mode, keep a gap to top
        me.stickTop = args.stickTop || 0;
        // Gap between object element & popup box(e.g. Show Header while popup)
        me.top = args.top || 0;
        // Support animation?
        me.noAnimation = args.noAnimation || false;
        // styling
        me.bgColor = args.bgColor || "#fff";
        me.opacity = args.opacity || 0.6;
        me.position = args.position || "fixed";
        me.zIndex = args.zIndex || 1;
        me.fullSize = args.fullSize || false;
        // button options
        me.closeble = args.closeble || false;
        me.closeButtonHide = args.closeButtonHide || false;
        me.closeButtonList = args.closeButtonList || [];
        me.closeInner = args.closeInner || false;
        me.closeCallback = args.closeCallback || $.noop;
    }, {
        "open": function() {
            var me = this;
            var deferred = $.Deferred();
            var $container = $(me.el);

            function evClose() {
                if (me.closeButtonList.length > 0) {
                    // Read all the buttons
                    $.each(me.closeButtonList, function(i, el) {
                        // Events
                        me.$popupBox.on("click", "[data-action=closepopupbox]", function(e) {
                            //e.preventDefault();

                            if ($(e.target).data("action") !== "closepopupbox") {
                                return;
                            }
                            me.close();
                        });
                        me.$popupBox.on("click", el, function(e) {
                            e.preventDefault();
                            me.close();
                        });
                    });
                }
            }

            function escClose() {
                var $keySupportInput = me.$popupBoxCover.find("input.popupbox-keysupport");
                if ($keySupportInput.length > 0) {
                    $keySupportInput.focus().keydown(function(e) {
                        if (e.which == 27) {
                            me.close();
                        }
                        e.preventDefault();
                    });
                }
            }

            function resize() {
                $(window).resize(function(e) {
                    me.centralize();
                });
            }

            // If the container doesn't exist, use body instead.
            if ($container.length <= 0) {
                $container = $("body");
            }
            // overlap
            var $existPopupBox = $container.find(" > div.popupbox, > div.popupbox-cover");
            if ($existPopupBox.length > 0) {
                if (me.overLap === "cancel") {
                    deferred.reject();
                    return deferred.promise();
                } else if (me.overLap === "replace") {
                    $existPopupBox.remove();
                }
            }
            // Generate the html
            me.$popupBoxWrapper = $(template({
                "bgColor": me.bgColor,
                "opacity": me.opacity,
                "position": me.position,
                "zIndex": me.zIndex,
                "top": me.top,
                "fullSize": me.fullSize,
                "keySupport": keySupport(),
                "closeble": me.closeble,
                "hasCloseButton": (me.closeble && !me.closeButtonHide),
                "isInnerClose": me.closeInner
            }));
            me.$popupBoxCover = me.$popupBoxWrapper.filter(".popupbox-cover");
            me.$popupBox = me.$popupBoxWrapper.filter(".popupbox");
            me.$popupBoxMain = me.$popupBox.find(".popupbox-main");
            me.$popupBoxContent = me.$popupBox.find(".popupbox-content");

            // Register window resize event
            if (!me.stick) {
                resize();
            }
            // Transfer msg to jQuery Object
            var $msg = $(me.msg);
            // Append Content, use 'prepend' to make 'close button' render at the end.
            me.$popupBoxContent.prepend($msg);
            // Append to DOM
            me.$popupBoxWrapper.appendTo($container);
            // +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ //
            // Async: Weave msg before append to DOM
            weave.apply($msg.find("[data-weave]")).then(function() {
                // Centralize the content
                me.centralize();
            });
            //me.centralize();
            // Append close button
            if (me.closeble) {
                me.closeButtonList.push("a.popupbox-close");
                // Attach close events
                evClose();
                // Attach ESC key event
                escClose();
            }
            // Show popup box
            if (me.noAnimation) {
                // Show Cover
                me.$popupBoxCover.show();
                // Show Content
                cssAnimate(me.$popupBoxContent, true).popIn();
                // Resolve
                deferred.resolve();
            } else {
                // Show Cover
                me.$popupBoxCover.fadeIn(400, function() {
                    // Show Content after Cover showed
                    cssAnimate(me.$popupBoxContent).popIn();
                    // Resolve
                    deferred.resolve();
                });
            }
            return deferred.promise();
        },
        "close": function(noCallback) {
            var me = this;
            var deferred = $.Deferred();

            function callback() {
                if (!noCallback) {
                    me.closeCallback();
                }
            }
            if (me.noAnimation) {
                // Hide Content
                cssAnimate(me.$popupBoxContent, true).popOut();
                // Hide Cover
                me.$popupBoxCover.hide();
                // Run close callback
                callback();
                // Remove popup elements
                me.$popupBoxWrapper.remove();
                // Resolve
                deferred.resolve();
            } else {
                // Hide Content
                cssAnimate(me.$popupBoxContent).popOut();
                window.setTimeout(function() {
                    // Hide Cover after Content hid
                    me.$popupBoxCover.fadeOut(200, function() {
                        // Run close callback after cover hid
                        callback();
                        // Remove popup elements
                        me.$popupBoxWrapper.remove();
                        // Resolve
                        deferred.resolve();
                    });
                }, 100);
            }
            return deferred.promise();
        },
        "centralize": function() {
            var me = this;
            // $out: Cover
            var $out = me.$popupBox;
            // $in: Content
            var $in = me.$popupBoxContent;
            // Get height of both container & content
            var hOut = $out.height();
            var hIn = $in.outerHeight();
            // calculate: keep top while stick, otherwise centralize the content in vertical
            var mTop = me.stick ? me.stickTop : Math.floor((hOut - hIn) / 2);
            // Never hide or be cut
            mTop = mTop < 0 ? 0 : mTop;
            // Apply to dom
            $in.css("margin-top", mTop);
            return me;
        }
    });
});

define('troopjs-requirejs/template!widget/enrollment/signin/index.html',[],function() { return function template(data) { var o = "";
    var isMobi = data.isMobi;
o += "\r\n<div class=\"v-popup-container popup-signin\">\r\n";
    if(!isMobi){
o += "\r\n    <div class=\"row\">\r\n        <div class=\"popup-signin-new col-md-6\">\r\n            <h3 data-weave=\"widget/blurb/main(1355)\">New Customers</h3>\r\n            <p>\r\n                <a class=\"btn btn-default btn-lg\" href=\"/enrollment/register\" data-weave=\"widget/href/main\">\r\n                    <span data-weave=\"widget/blurb/main(1020)\">Create an account</span>\r\n                </a>\r\n            </p>\r\n            <hr>\r\n            <p data-weave=\"widget/blurb/main(1356)\">You do not need to create an account to checkout. You can checkout as a guest and create an account later.</p>\r\n            <a id=\"su\" class=\"btn btn-default btn-lg\" data-action=\"close\" data-weave=\"widget/blurb/main(1357)\">Continue as Guest</a>\r\n        </div>\r\n        <div class=\"popup-signin-form col-md-6\" data-weave=\"widget/formlogin/main\"></div>\r\n    </div>\r\n";
    }
    else{
o += "\r\n    <div class=\"row\">\r\n        <div class=\"popup-signin-form col-xs-12\" data-weave=\"widget/formlogin/main\"></div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"popup-signin-new col-xs-12\">\r\n            <p>\r\n                <a href=\"/enrollment/register\" data-weave=\"widget/href/main\">\r\n                    <span data-weave=\"widget/blurb/main(1020)\">Create an account</span>\r\n                </a>\r\n                <a id=\"su\" class=\"pull-right\" data-action=\"close\" data-weave=\"widget/blurb/main(1357)\">Continue as Guest</a>\r\n            </p>\r\n        </div>\r\n    </div>\r\n";
    }
o += "\r\n</div>"; return o; }; });
define('widget/enrollment/signin/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, weave, when, $, context, PopupBox, template) {
    "use strict";

    return Widget.extend(function() {
        var me = this;
    }, {
        "hub/enrollment/signin/popup": function() {
            var me = this;
            var $me = $(template({
                "isMobi": context.isMobile
            }));

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("orderEdit/updateOrder");
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open();
        }
    });
});

define('troopjs-requirejs/template!widget/account/index.html',[],function() { return function template(data) { var o = "";
	var logged = data.logged;
    var nickName = data.nickName || "";
o += "\r\n<ul class=\"hor seplist\">\r\n    "; if (!logged) { o += "\r\n        <!--li>\r\n            <a href=\"/\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1127)\">Home</span>\r\n            </a>\r\n        </li-->\r\n        <li>\r\n            <a class=\"link-fn\" data-action=\"signin\">\r\n                <span data-weave=\"widget/blurb/main(1000)\">Sign in</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"/enrollment/register\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1020)\">Create an account</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"/order/search\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1350)\">Find my orders</span>\r\n            </a>\r\n        </li>\r\n    "; } else { o += "\r\n        <!--li>\r\n            <span data-weave=\"widget/blurb/main(1004)\">Welcome, </span><a href=\"/account/orders\" data-weave=\"widget/href/main\"><span>" +nickName+ "</span></a>\r\n        </li-->\r\n        <li>\r\n            <a href=\"/\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1127)\">Home</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"/account/logoff\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1021)\">Sign out</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"/account/orders\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1001)\">My VCLEANSE</span>\r\n            </a>\r\n        </li>\r\n    "; } o += "\r\n</ul>"; return o; }; });
define('widget/account/main',["troopjs-browser/component/widget",
	"jquery",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/cookie/main",
    "widget/enrollment/signin/main",
	"template!./index.html"
], function(Widget, $, api, ajaxQuery, cookie, Signin, template) {
	"use strict";

    (new Signin($(document.body))).start();
	var URI_ACCOUNT = api.get("account");

	return Widget.extend({
		"sig/start": function() {
			var me = this;

			// Has cookie
			cookie.get("memberid").then(function() {
				// Get User Account
				ajaxQuery({
					url: URI_ACCOUNT,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					var account = data;
					var nickName = "";
					if(account.FirstName){
						nickName = account.FirstName;
					}else if(account.Mobile){
						nickName = account.Mobile;
					}
					
					// Rendering
					me.html(template, {
						"logged": true,
						"nickName": nickName
					});
				}, function() {
					// Rendering
					me.html(template, {
						"logged": false
					});
				});
			}, function() {
				// Rendering
				me.html(template, {
					"logged": false
				});
			});
		},
        "dom:a[data-action=signin]/click": function(e) {// 点击signin弹出登录框
            e.preventDefault();

            var me = this;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        }
	});
});
define('widget/autofollower/main',["troopjs-browser/component/widget",
  "jquery"
], function(Widget, $) {

  var DELAY = 1000;

  return Widget.extend({
    "sig/start": function() {
      var me = this;
      var $me = me.$element;

      // Check after 400ms, waiting for async content
      setTimeout(function() {
        me.autoFollower();
      }, DELAY);
    },
    "autoFollower": function() {
      var me = this;
      var $me = me.$element;

      // Original position of current element
      var originPostion = $me.position();
      var originTop = originPostion.top;
      var originLeft = originPostion.left;
      var originWidth = $me.width();
      // Add 30px buffer, for x-scroll-bar case
      var originHeight = $me.height() + 30;
      // Parent height
      // * Important: if parent height is full fill with current element,
      // do not auto-follow, because auto-follow will set postion to "fixed",
      // this property will break parent's height
      var parentHeight = $me.parent().height();
      if (parentHeight <= originHeight) {
        return;
      }
      // Calculate scroll max size
      var windowInnerHeight = window.innerHeight;
      // Scroll content height <compare with> window inner height
      var scrollDelta = originHeight - windowInnerHeight;
      scrollDelta = scrollDelta > 0 ? scrollDelta : 0;
      // Apply initial css
      $me.css($.extend(originPostion, {
        // Position when fixed
        "top": scrollDelta * -1,
        "left": originLeft
      }));

      var isPositionFixed = false;
      var scrollBase = 0;

      // Scroll event
      $(window).scroll(function(event) {
        var $w = $(this);
        var scrollTop = $w.scrollTop();
        var scrollLeft = $w.scrollLeft();
        var style = "";

        if (scrollLeft !== 0) {
          // Reset left
          $me.css({
            "left": originLeft - scrollLeft
          });
        }

        // Check every 10px, by this to prevent check every pixel
        // this condition will increase performance
        if (Math.abs(scrollTop - scrollBase) < 10) {
          return;
        }
        scrollBase = scrollTop;

        // Check position while page scroll
        if (scrollTop >= scrollDelta + originTop) {
          if (isPositionFixed) {
            return;
          }
          $me.css({
            "position": "fixed",
            "width": originWidth
          });
          isPositionFixed = true;
        } else {
          if (!isPositionFixed) {
            return;
          }
          style = $me.attr("style").replace("width: " + originWidth + "px", "");
          $me.attr("style", style);
          $me.css({
            "position": "static"
          });
          isPositionFixed = false;
        }
      });
    },
    "hub/autoFollower/resize": function() {
      var me = this;

      me.autoFollower();
    }
  });

});
define('widget/sitemap/main',["jquery",
    "context",
    "poly",
    "underscore.string"
], function($, context) {
    "use strict";

    var urlDict = {
        "/": "/",

        // Public
        "/intl/about": "/intl/about",
        "/intl/codepressed": "/intl/codepressed",
        "/intl/questions": "/intl/questions",
        "/intl/theory": "/intl/theory",
        "/intl/announce": "/intl/announce",

        // Enrollment
        "/enrollment/signin": "/enrollment/signin",
        "/enrollment/register": "/enrollment/register",
        "/enrollment/success": "/enrollment/success",
        "/enrollment/forgotpassword": "/enrollment/forgotpassword",

        // Account
        "/account/basic": "/account/basic",
        "/account/receiver": "/account/receiver",
        "/account/orders": "/account/orders",
        "/account/orderdetail": "/account/orderdetail",
        "/account/logoff": "/account/logoff",
        "/account/safety": "/account/safety",

        // Juice
        "/juice": "/juice",
        "/juice/detail": "/juice/detail",
        "/juice/chailatte": "/juice/chailatte",
        "/juice/chiagreen": "/juice/chiagreen",
        "/juice/classicblack": "/juice/classicblack",
        "/juice/classicgreen": "/juice/classicgreen",
        "/juice/classicwhite": "/juice/classicwhite",
        "/juice/cranberrymilk": "/juice/cranberrymilk",
        "/juice/darkgreen": "/juice/darkgreen",
        "/juice/darkwhite": "/juice/darkwhite",
        "/juice/darkyellow": "/juice/darkyellow",
        "/juice/easygreen": "/juice/easygreen",
        "/juice/fragrantclear": "/juice/fragrantclear",
        "/juice/fragrantred": "/juice/fragrantred",
        "/juice/gingercarrotsoup": "/juice/gingercarrotsoup",
        "/juice/greengoddesssoup": "/juice/greengoddesssoup",
        "/juice/hibiscusinfusion": "/juice/hibiscusinfusion",
        "/juice/intensegreen": "/juice/intensegreen",
        "/juice/luohanguoinfusion": "/juice/luohanguoinfusion",
        "/juice/matchamilk": "/juice/matchamilk",
        "/juice/mushroomsoup": "/juice/mushroomsoup",
        "/juice/passionateyellow": "/juice/passionateyellow",
        "/juice/passionfruit": "/juice/passionfruit",
        "/juice/pearandginger": "/juice/pearandginger",
        "/juice/persimmoninfusion": "/juice/persimmoninfusion",
        "/juice/purplebroth": "/juice/purplebroth",
        "/juice/redbeetsoup": "/juice/redbeetsoup",
        "/juice/spicylemonade": "/juice/spicylemonade",
        "/juice/splitpeasoup": "/juice/splitpeasoup",
        "/juice/strawberryvanilla": "/juice/strawberryvanilla",
        "/juice/sweetpotatopumpkinsoup": "/juice/sweetpotatopumpkinsoup",
        "/juice/vlatte": "/juice/vlatte",

        // Cleanse
        "/cleanse": "/cleanse",
        "/cleanse/detail": "/cleanse/detail",

        // Subscriptions
        "/subscription": "/subscription",

        // Gift cards
        "/giftcard": "/giftcard",

        // Order
        "/order": "/order",
        "/order/receiver": "/order/receiver",
        "/buy/orderinfo": "/buy/orderinfo",
        "/order/confirm": "/order/confirm",
        "/order/success": "/order/success",

        // Search
        "/order/search": "/order/search",
        "/order/orders": "/order/orders",
        "/order/orderdetail": "/order/orderdetail",

        // Payment
        "/payment/topay": "/payment/topay",
        "/payment/pay": "/payment/pay",
        "/payment/paysuccess": "/payment/paysuccess",
        "/payment/payfailed": "/payment/payfailed",

        // Stores
        "/stores": "/stores",

        // Join US
        "/jobs": "/jobs",
        "/jobs/digitalmarketing_en": "/jobs/digitalmarketing_en",
        "/jobs/digitalmarketing_cn": "/jobs/digitalmarketing_cn",
        "/jobs/salesandmarketingassistant_en": "/jobs/salesandmarketingassistant_en",
        "/jobs/salesandmarketingassistant_cn": "/jobs/salesandmarketingassistant_cn",
        "/jobs/salesrepresentative_en": "/jobs/salesrepresentative_en",
        "/jobs/salesrepresentative_cn": "/jobs/salesrepresentative_cn",
        "/jobs/workshopleaderparttime_en": "/jobs/workshopleaderparttime_en",
        "/jobs/workshopleaderparttime_cn": "/jobs/workshopleaderparttime_cn",
        "/jobs/storeassistant_en": "/jobs/storeassistant_en",
        "/jobs/storeassistant_cn": "/jobs/storeassistant_cn",
        "/jobs/supply_en": "/jobs/supply_en",
        "/jobs/supply_cn": "/jobs/supply_cn",
        "/jobs/productmanager_en": "/jobs/productmanager_en",
        "/jobs/productmanager_cn": "/jobs/productmanager_cn",

        // Event
        "/paws": "/paws",
        "/warmingday": "/warmingday",
        "/valentine": "/valentine",
        "/womensday": "/womensday",
        "/moonbear": "/moonbear",
        "/workshop": "/workshop",
        "/workshop/office": "/workshop/office"
    };

    // For Mobile environment
    if (context.isMobile) {
        $.extend(urlDict, {
            "/": "/m/cleanse",

            // Enrollment
            "/enrollment/signin": "/m/enrollment/signin",
            "/enrollment/register": "/m/enrollment/register",
            "/enrollment/success": "/m/enrollment/success",
            "/enrollment/forgotpassword": "/m/enrollment/forgotpassword",

            // Cleanse
            "/cleanse": "/m/cleanse",
            "/cleanse/detail": "/m/cleanse/detail",
            "/cleanse/choose": "/m/cleanse/choose",

            // Gift cards
            "/giftcard": "/m/giftcard",

            // Order
            "/order": "/m/cleanse",
            "/order/receiver": "/m/order/receiver",
            "/order/confirm": "/m/order/confirm",
            "/order/success": "/m/order/success",

            // Payment
            "/payment/topay": "/m/payment/topay",
            "/payment/paysuccess": "/m/payment/paysuccess",
            "/payment/payfailed": "/m/payment/payfailed",

            // Stores
            "/stores": "/m/stores",

            // Join US
            "/jobs": "/m/jobs",
            "/jobs/prmanager_en": "/m/jobs/prmanager_en",
            "/jobs/prmanager_cn": "/m/jobs/prmanager_cn",
            "/jobs/supply_en": "/m/jobs/supply_en",
            "/jobs/supply_cn": "/m/jobs/supply_cn",
            "/jobs/productmanager_en": "/m/jobs/productmanager_en",
            "/jobs/productmanager_cn": "/m/jobs/productmanager_cn",
            "/jobs/onlineoperation_en": "/m/jobs/onlineoperation_en",
            "/jobs/onlineoperation_cn": "/m/jobs/onlineoperation_cn",
            "/jobs/senioruidesigner_en": "/m/jobs/senioruidesigner_en",
            "/jobs/senioruidesigner_cn": "/m/jobs/senioruidesigner_cn",
            "/jobs/socialmediaoperation_en": "/m/jobs/socialmediaoperation_en",
            "/jobs/socialmediaoperation_cn": "/m/jobs/socialmediaoperation_cn",
            "/jobs/salesandmarketingassistant_en": "/m/jobs/salesandmarketingassistant_en",
            "/jobs/salesandmarketingassistant_cn": "/m/jobs/salesandmarketingassistant_cn",
            "/jobs/salesrepresentative_en": "/m/jobs/salesrepresentative_en",
            "/jobs/salesrepresentative_cn": "/m/jobs/salesrepresentative_cn",
            "/jobs/workshopleaderparttime_en": "/m/jobs/workshopleaderparttime_en",
            "/jobs/workshopleaderparttime_cn": "/m/jobs/workshopleaderparttime_cn",
            "/jobs/storeassistant_en": "/m/jobs/storeassistant_en",
            "/jobs/storeassistant_cn": "/m/jobs/storeassistant_cn",

            // Event
            "/paws": "/m/paws",
            "/warmingday": "/m/warmingday",

            // Account
            "/account/orders": "/m/account/orders",
            "/account/orderdetail": "/m/account/orderdetail",
            "/account/logoff": "/m/account/logoff"
        });
    }

    // Get path name by recursive

    function getPathName(key) {
        var isMatch = false;
        while (urlDict[key] && urlDict[key] !== key) {
            key = urlDict[key];
            isMatch = true;
        }
        return isMatch ? key : "";
    }

    return {
        "get": function(key) {
            key = key.toLowerCase();

            var path = getPathName(key);

            return path ? (path.indexOf("http://") >= 0 ? path : (context.domain + path)) : key;
        },
        "reversematch": function(pathName) {
            pathName = pathName.toLowerCase();
            if (context.virtualDirectory) {
                pathName = pathName.replace(context.virtualDirectory, "");
            }

            var keyMatch = "";
            var pathNameLens = pathName.length;

            // Remove last "/"
            if (pathNameLens > 1 && pathName.charAt(pathNameLens - 1) === "/") {
                pathName = pathName.substr(0, pathNameLens - 1);
            }

            // Fill default action like "/xxx/index"
            if (_.endsWith(pathName, "/index")) {
                pathName = _.strLeft(pathName, "/index");
            }

            // Static logic for order package(temp homepage)
            if (pathName === "" || pathName === "/") {
                keyMatch = "/";
            } else {
                for (var key in urlDict) {
                    if (urlDict.hasOwnProperty(key) && urlDict[key] === pathName) {
                        keyMatch = key;
                        break;
                    }
                }
            }

            return keyMatch;
        }
    };
});

define('troopjs-requirejs/template!widget/banner/index.html',[],function() { return function template(data) { var o = "";
    var path = data.path;
    var isMobile = data.isMobile;
    var cacheServer = data.cacheServer;
    var language = data.language;

    var menu = path[1];
    var subMenu = path[2] || "";

    switch(menu) {
        case "intl":
            renderPublic();
            break;
        case "enrollment":
            renderEnrollment();
            break;
        case "subscription":
            renderSubscription();
            break;
        case "giftcard":
            renderGiftcard();
            break;
        case "cleanse":
            renderCleanse();
            break;
        case "juice":
            renderJuice();
            break;
        case "account":
            renderAccount();
            break;
        case "order":
            renderOrder();
            break;
        case "payment":
            renderPayment();
            break;
        default: break;
    }

    function renderPublic() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderEnrollment() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderSubscription() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderGiftcard() {
        switch (subMenu) {
            default: renderBanner("banner_giftcard");
                break;
        }
    }
    function renderCleanse() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderJuice() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderAccount() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderOrder() {
        switch (subMenu) {
            default: 
                break;
        }
    }
    function renderPayment() {
        switch (subMenu) {
            default: 
                break;
        }
    }
o += "\r\n\r\n"; function renderBanner(bannerPath){ o += "\r\n    <img src=\"" +cacheServer+ "/images/banner/" +((bannerPath + "_" + language) || "default")+ ".jpg\" alt=\"JUICE,HEALTH,HAPPINESS\">\r\n"; }  return o; }; });
define('widget/banner/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/sitemap/main",
    "template!./index.html"
], function (Widget, $, context, sitemap, template) {
    "use strict";

    return Widget.extend(function ($element, widgetName, pathName) {
        var me = this;

        me.pathName = pathName || null;
    }, {
        "sig/start": function () {
            var me = this;
            var pathName = me.pathName || location.pathname;
            var pathKey = sitemap.reversematch(pathName);

            if (!pathKey) {
                return;
            }

            // Array[0]: ""
            // Array[1]: main menu
            // Array[2]: sub menu
            var arrPath = pathKey.split("/");
            // Rendering
            me.html(template, {
                "path": arrPath,
                "isMobile": context.isMobile,
                "cacheServer": context.cacheServer,
                "language": context.language
            });

        }
    });
});
define('widget/basic/bootstrap-tab',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend(function(){
        var me = this;

        me.timeInstance = null;
    },{
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            //mouseover 切换tab
            $me.find("[data-toggle=tab]").on('mouseenter', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var me = this;

                var $e = $(e.currentTarget);
                me.timeInstance = setTimeout(function(){
                    $e.trigger("click");
                }, 150);
            });
            $me.find("[data-toggle=tab]").on('mouseout', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var me = this;

                clearTimeout(me.timeInstance);
            });
        }
    });
});
define('widget/basic/cloneobject',["jquery",
    "underscore"
], function($, _) {
    "use strict";

    return function cloneObject(myObj) {

        if (typeof(myObj) !== 'object') {
            return myObj;
        }
        if (myObj === null) {
            return myObj;
        }

        var myNewObj = _.isArray(myObj) ? [] : {};
        var assignment = false;

        for (var i in myObj) {
            if (myObj.hasOwnProperty(i)) {
                myNewObj[i] = cloneObject(myObj[i]);

                assignment = true;
            }
        }

        return assignment ? myNewObj : myObj;
    };

});
define('widget/basic/imageswitch',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "underscore",
    "underscore.string"
], function(Widget, $, context) {
    "use strict";

    function isBj(cityName) {
        if (!cityName) {
            return;
        }

        return cityName.indexOf("北京") >= 0 ||
            cityName.indexOf("天津") >= 0;
    }

    return Widget.extend(function($element, widgetName, byLanguage) {
        var me = this;
        me.byLanguage = byLanguage || false;
    }, {
        "render": function(src) {
            var me = this;
            var $me = me.$element;

            if (!src) {
                return;
            }

            // Attach Language
            if (me.byLanguage) {
                src = _.strLeftBack(src, ".") + "_" + context.language + "." + _.strRightBack(src, ".");
            }

            // Add version control
            src = src + "?v=" + context.cacheKey;

            // Add Cache Server
            src = context.cacheServer + "/" + src;

            // Apply to Img src Tag
            $me.attr("src", src);
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var src = $me.data("src") || $me.attr("src");

            // GEO
            var geolocation = $me.data("geolocation") || "";
            if (geolocation && BMap) {
                // USE Baidu Map
                // Require - http://api.map.baidu.com/api?v=2.0
                (new BMap.LocalCity()).get(function(city) {
                    if (isBj(city.name)) {
                        src = _.strLeftBack(src, ".") + "_bj." + _.strRightBack(src, ".");
                    }

                    // Render
                    me.render(src);
                });
            } else {
                // Render
                me.render(src);
            }
        }
    });
});
define('widget/blurb/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "underscore"
], function(Widget, $, context) {

    var BLURB = window.BLURB || {};

    var blurbDict = _.extend(BLURB, {
        "1000": {
            "en": "Sign in",
            "cn": "登录"
        },
        "1001": {
            "en": "My VCLEANSE",
            "cn": "我的维果清"
        },
        "1002": {
            "en": "Questions? Contact us (9AM - 7PM):",
            "cn": "欢迎咨询我们的健康管家（上午9点 - 晚上7点）："
        },
        "1003": {
            "en": "Your Order",
            "cn": "开始订餐"
        },
        "1004": {
            "en": "Welcome, ",
            "cn": "欢迎您，"
        },
        "1005": {
            "en": "STEP 1:",
            "cn": "第一步："
        },
        "1006": {
            "en": "STEP 2:",
            "cn": "第二步："
        },
        "1007": {
            "en": "STEP 3:",
            "cn": "第三步："
        },
        "1008": {
            "en": "Select start date",
            "cn": "套餐开始日期"
        },
        "1009": {
            "en": "Confirm Order",
            "cn": "确认订单"
        },
        "1010": {
            "en": "Pay",
            "cn": "付款"
        },
        "1011": {
            "en": "Retrieve Password",
            "cn": "找回密码"
        },
        "1012": {
            "en": "Create your VCLEANSE account",
            "cn": "创建您的维果清帐号"
        },
        "1013": {
            "en": "Basic Information",
            "cn": "个人基本信息"
        },
        "1014": {
            "en": "Delivery Addresses",
            "cn": "收件人地址"
        },
        "1015": {
            "en": "My Orders",
            "cn": "我的订单"
        },
        "1016": {
            "en": "Surpass Court, 570 Yongjia Road, Shanghai",
            "cn": "上海市永嘉路570号永嘉庭"
        },
        "1017": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1018": {
            "en": "Select program",
            "cn": "选择您的清体套餐"
        },
        "1019": {
            "en": "Choose one of our programs, or create your own.",
            "cn": "选择适合您的套餐，享受订餐吧！"
        },
        "1020": {
            "en": "Create an account",
            "cn": "注册"
        },
        "1021": {
            "en": "Sign out",
            "cn": "退出"
        },
        "1022": {
            "en": "Contact us",
            "cn": "联系我们"
        },
        "1024": {
            "en": "When would you like to start your cleanse?",
            "cn": "请选择一天开始您的维果清清体之旅？"
        },
        "1025": {
            "en": "Past two weeks",
            "cn": "上两周"
        },
        "1026": {
            "en": "Next two weeks",
            "cn": "下两周"
        },
        "1027": {
            "en": "Pick up in store",
            "cn": "亲自来店取餐"
        },
        "1028": {
            "en": "Change",
            "cn": "修改"
        },
        "1029": {
            "en": "Add",
            "cn": "添加"
        },
        "1030": {
            "en": "Too late <br>to order",
            "cn": "您错过了~"
        },
        "1031": {
            "en": "Confirm",
            "cn": "确认"
        },
        "1032": {
            "en": "Order",
            "cn": "您的预定"
        },
        "1033": {
            "en": "Product:",
            "cn": "套餐："
        },
        "1034": {
            "en": "Delivery:",
            "cn": "外送费："
        },
        "1035": {
            "en": "Payment Total:",
            "cn": "总价："
        },
        "1036": {
            "en": "Company invoice:",
            "cn": "公司发票："
        },
        "1037": {
            "en": "Personal invoice:",
            "cn": "个人发票："
        },
        "1038": {
            "en": "Invoice amount:",
            "cn": "发票金额："
        },
        "1039": {
            "en": "Invoice title:",
            "cn": "发票抬头："
        },
        "1040": {
            "en": "Total:",
            "cn": "总价："
        },
        "1041": {
            "en": "Your tax invoice (fapiao)",
            "cn": "发票信息"
        },
        "1043": {
            "en": "VCLEANSE will issue an invoice for products. For an invoice for delivery fees, please contact the delivery company",
            "cn": "维果清发票金额仅为产品，若需运费发票，请与外送公司联系。"
        },
        "1044": {
            "en": "Yes, I would like an invoice",
            "cn": "需要发票"
        },
        "1045": {
            "en": "Personal invoice",
            "cn": "个人发票"
        },
        "1046": {
            "en": "Company invoice",
            "cn": "公司发票"
        },
        "1047": {
            "en": "Company title",
            "cn": "公司名称"
        },
        "1048": {
            "en": "Purchase a V-HAPPY card",
            "cn": "购买V-HAPPY卡"
        },
        "1049": {
            "en": "Give the gift of health and happiness!",
            "cn": "送出健康快乐的礼物"
        },
        "1050": {
            "en": "BUY",
            "cn": "购买"
        },
        "1051": {
            "en": "Find your juice type",
            "cn": "选购果汁类型"
        },
        "1052": {
            "en": "LEARN MORE",
            "cn": "更多选择"
        },
        "1053": {
            "en": "WeChat:",
            "cn": "微信："
        },
        "1054": {
            "en": "First Name:",
            "cn": "名字："
        },
        "1055": {
            "en": "Last name:",
            "cn": "姓："
        },
        "1056": {
            "en": "Mobile:",
            "cn": "手机号码："
        },
        "1057": {
            "en": "Shipping address:",
            "cn": "收件人地址："
        },
        "1058": {
            "en": "Pick up in store:",
            "cn": "来店自取："
        },
        "1059": {
            "en": "Thank you for your order! If you need to change or cancel your order, please give us a call at",
            "cn": "感谢您的选购。如果您需要修改或者取消订单，请给我们来电"
        },
        "1060": {
            "en": "Order details",
            "cn": "订单详情"
        },
        "1061": {
            "en": "Order number:",
            "cn": "订单号："
        },
        "1062": {
            "en": "Payment Summary:",
            "cn": "支付详情："
        },
        "1063": {
            "en": "If you have any questions, please give us a call at",
            "cn": "如果您在购买过程中遇到任何问题请联系我们清体管家"
        },
        "1064": {
            "en": "Thanks for your order",
            "cn": "感谢您的选购"
        },
        "1065": {
            "en": "VCLEANSE Team",
            "cn": "维果清小组"
        },
        "1066": {
            "en": "Deliver to:",
            "cn": "发送到："
        },
        "1067": {
            "en": "Add Another Address",
            "cn": "添加地址"
        },
        "1068": {
            "en": "CREATE ACCOUNT",
            "cn": "创建账户"
        },
        "1069": {
            "en": "UPDATE",
            "cn": "更新"
        },
        "1070": {
            "en": "DELETE",
            "cn": "删除"
        },
        "1071": {
            "en": "SET AS DEFAULT",
            "cn": "设置为默认地址"
        },
        "1072": {
            "en": "Default address",
            "cn": "默认地址"
        },
        "1073": {
            "en": "Edit delivery address",
            "cn": "修改收件人地址"
        },
        "1074": {
            "en": "Add a delivery address",
            "cn": "添加地址"
        },
        "1075": {
            "en": "CANCEL",
            "cn": "取消"
        },
        "1076": {
            "en": "Email:",
            "cn": "电子邮箱："
        },
        "1077": {
            "en": "New password:",
            "cn": "新的密码："
        },
        "1078": {
            "en": "Confirm password:",
            "cn": "确认密码："
        },
        "1079": {
            "en": "SAVE",
            "cn": "保存"
        },
        "1080": {
            "en": "SIGN IN",
            "cn": "登录"
        },
        "1081": {
            "en": "I have a VCLEANSE account",
            "cn": "已经有维果清账号"
        },
        "1083": {
            "en": "Forgot password?",
            "cn": "忘记密码？"
        },
        "1084": {
            "en": "SEND VERIFICATION CODE",
            "cn": "发送验证码"
        },
        "1085": {
            "en": "Security code:",
            "cn": "验证码："
        },
        "1086": {
            "en": "Preferred delivery window?",
            "cn": "选择理想送达时间段"
        },
        "1087": {
            "en": "Deliver to this address",
            "cn": "地址"
        },
        "1088": {
            "en": "Edit this address",
            "cn": "修改地址"
        },
        "1090": {
            "en": "REMOVE THIS DAY FROM ORDER",
            "cn": "删除"
        },
        "1091": {
            "en": "UPDATE ORDER",
            "cn": "更新订单"
        },
        "1092": {
            "en": "Order status:",
            "cn": "订单状态："
        },
        "1093": {
            "en": "Tracking number:",
            "cn": "物流单号："
        },
        "1094": {
            "en": "+ Change",
            "cn": "+ 修改"
        },
        "1096": {
            "en": "+ See more",
            "cn": "+ 查看更多"
        },
        "1097": {
            "en": "",//"+ Learn more",
            "cn": "",//"+ 更多信息" 
        },
        "1098": {
            "en": "Order number",
            "cn": "订单号"
        },
        "1099": {
            "en": "Order date",
            "cn": "订单日期"
        },
        "1100": {
            "en": "Start date",
            "cn": "开始饮用日期"
        },
        "1101": {
            "en": "Total Amount (RMB)",
            "cn": "总价（元）"
        },
        "1102": {
            "en": "Order Status",
            "cn": "订单状态"
        },
        "1103": {
            "en": "+ Learn more",
            "cn": "+ 了解更多"
        },
        "1104": {
            "en": "BUY",
            "cn": "预订"
        },
        "1105": {
            "en": "Questions about your delivery?",
            "cn": "有外送问题？"
        },
        "1106": {
            "en": "Call our delivery company Yamato Express at",
            "cn": "拨打我们外送伙伴宅急便"
        },
        "1107": {
            "en": "Have your order tracking number ready when you call. They speak English and Chinese.",
            "cn": "他们提供中英文服务帮助您跟踪订单状态"
        },
        "1108": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1109": {
            "en": "Click on the button below to pay via Alipay!",
            "cn": "选择下面的支付方式完成支付!"
        },
        "1110": {
            "en": "Alipay",
            "cn": "支付宝"
        },
        "1111": {
            "en": "Payment method",
            "cn": "支付方式"
        },
        "1112": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1113": {
            "en": "Payment on delivery",
            "cn": "货到付款"
        },
        "1114": {
            "en": "NEXT",
            "cn": "下一步"
        },
        "1115": {
            "en": "CONFIRM",
            "cn": "确认"
        },
        "1116": {
            "en": "Tel:",
            "cn": "联系电话："
        },
        "1117": {
            "en": "CLICK HERE TO ADD ANOTHER ORDER",
            "cn": "点击开始您的订餐"
        },
        "1118": {
            "en": "RETURN TO MY ORDERS",
            "cn": "返回我的订单"
        },
        "1119": {
            "en": "You can cancel your order online at any time up to 24 hours before your start date. If you are within 24 hours of your start date and need to modify your order, please call us at 4001-V-HAPPY. If you selected online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.",
            "cn": "您可以在订单外送前24小时取消您的订单。为了确保准时外送，请在外送前24小时完成支付。"
        },
        "1120": {
            "en": "Gender:",
            "cn": "性别："
        },
        "1121": {
            "en": "Female",
            "cn": "女"
        },
        "1122": {
            "en": "Male",
            "cn": "男"
        },
        "1123": {
            "en": "Birthday:",
            "cn": "生日："
        },
        "1124": {
            "en": "Payment Status",
            "cn": "支付状态"
        },
        "1125": {
            "en": "Payment Status:",
            "cn": "支付状态："
        },
        "1126": {
            "en": "PAY ONLINE",
            "cn": "付款"
        },
        "1127": {
            "en": "Home",
            "cn": "首页"
        },
        "1128": {
            "en": "Store address:",
            "cn": "门店地址："
        },
        "1129": {
            "en": "OK",
            "cn": "确定"
        },
        "1130": {
            "en": "Password",
            "cn": "密码"
        },
        "1131": {
            "en": "Confirm password",
            "cn": "确认密码"
        },
        "1132": {
            "en": "Your Name Here",
            "cn": "您的姓名"
        },
        "1133": {
            "en": "Your Last Name Here",
            "cn": "您的姓氏"
        },
        "1134": {
            "en": "Mobile number is required",
            "cn": "11位手机号码，必填"
        },
        "1136": {
            "en": "Full address is required",
            "cn": "完整收件地址（不需要重复填写市、区），必填"
        },
        "1137": {
            "en": "Your mobile phone",
            "cn": "您的手机号码"
        },
        "1138": {
            "en": "Your Email Address",
            "cn": "您的电子邮件"
        },
        "1139": {
            "en": "Input verification code sent to your mobile phone",
            "cn": "输入您的手机收到的验证码"
        },
        "1140": {
            "en": "Mobile",
            "cn": "手机号码"
        },
        "1141": {
            "en": "Order successful submit, payment as soon as possible please!",
            "cn": "订单提交成功，请您尽快付款！"
        },
        "1142": {
            "en": "Amount Due (RMB):",
            "cn": "应付金额（元）："
        },
        "1143": {
            "en": "Please complete payment within 24 hours. Orders will be cancelled after 24 hours with no payment.",
            "cn": "立即支付{0}元，即可完成订单。 请您在24小时内完成支付，否则订单会被自动取消。"
        },
        "1144": {
            "en": "Reminder!",
            "cn": "注意！"
        },
        "1145": {
            "en": "Dear V-Cleanser, please pickup your orders in the store until you have a delivery address! <br>Click here to <a href=\"\/account\/receiver\">add delivery address</a>, or you can also edit delivery address by click \"+ Change\" on calendar.",
            "cn": "您还没有填写收件人地址哦！如果不添加请不要忘记来门店提取您的维果清！<br>点击这里<a href=\"\/account\/receiver\">添加收件人地址</a>，或者您也可以在对应日期点击 “+ 修改” ，然后编辑收件人地址。"
        },
        "1146": {
            "en": "Dear V-Cleanser, you will choose your own programs. <br>Click \"+ Add\" on calendar to start your order!",
            "cn": "您选择创建自己的维果清套餐，请在对应的日期点击 “+ 添加” 开始您的订餐！"
        },
        "1147": {
            "en": "Dear V-Cleanser, please order again from order page.",
            "cn": "您选购的商品信息有误，请回到订餐页面重新订餐。"
        },
        "1148": {
            "en": "Success!",
            "cn": "成功！"
        },
        "1149": {
            "en": "Successfully set this delivery address to default!",
            "cn": "成功设置为默认地址！"
        },
        "1150": {
            "en": "Successfully delete this delivery address!",
            "cn": "成功删除该收件人地址！"
        },
        "1151": {
            "en": "Successfully update this delivery address!",
            "cn": "成功更新该收件人地址！"
        },
        "1152": {
            "en": "Successfully add this new delivery address!",
            "cn": "成功增加新的收件人地址！"
        },
        "1153": {
            "en": "Successfully update my basic information!",
            "cn": "成功更新我的基本信息！"
        },
        "1154": {
            "en": "Change password",
            "cn": "如果您需要更改密码，请在此输入"
        },
        "1155": {
            "en": "Customize",
            "cn": "单品"
        },
        "1156": {
            "en": "Design your own cleanse program!",
            "cn": "创建您自己的清体套餐！"
        },
        "1157": {
            "en": "Invoice type:",
            "cn": "发票类型："
        },
        "1158": {
            "en": "Company",
            "cn": "公司发票"
        },
        "1159": {
            "en": "Personal",
            "cn": "个人发票"
        },
        "1160": {
            "en": "New VCLEANSE password has sent to your mobile phone, check please!",
            "cn": "新的VCLEANSE密码已经发送到您的手机，请查收！"
        },
        "1161": {
            "en": "Choose your cleanse program duration:",
            "cn": "选择清体套餐理想天数："
        },
        "1162": {
            "en": "Not yet delivered",
            "cn": "未外送"
        },
        "1163": {
            "en": "Delivering",
            "cn": "外送中"
        },
        "1164": {
            "en": "Delivered",
            "cn": "已外送"
        },
        "1165": {
            "en": "Dear new V-Cleanser,",
            "cn": "亲爱的新晋维果清达人，"
        },
        "1166": {
            "en": "Congrats! You are about to embark on a fun journey with us!",
            "cn": "恭喜，你将跟我们开始一段非常有趣的新旅程！"
        },
        "1167": {
            "en": "Who says living healthily is hard? In our eyes, it can be stylish, hip, and full of sparks.",
            "cn": "健康生活绝不枯燥，在我们眼中，它是有趣的，充满时尚感和惊喜。"
        },
        "1168": {
            "en": "Finish your <a href=\"/account/basic\">VCleanse profile</a>.",
            "cn": "补充完整您的<a href=\"/account/basic\">信息</a>。"
        },
        "1169": {
            "en": "Place your <a href=\"/order\" data-weave=\"widget/href/main\">first order</a>.",
            "cn": "来首次<a href=\"/order\" data-weave=\"widget/href/main\">订餐</a>。"
        },
        "1170": {
            "en": "Questions?",
            "cn": "有问题？"
        },
        "1171": {
            "en": "We are just one WeChat away!",
            "cn": "我们的客服在微信服务号替您解答。"
        },
        "1172": {
            "en": "Green is the new black. Health is the priceless luxury. Staying fit is the ultimate sexy.",
            "cn": "绿色正在重新诠释经典。健康是最无价的奢侈。保持活力是由内到外性感的最佳注脚。"
        },
        "1173": {
            "en": "Congrats!",
            "cn": "感谢您！"
        },
        "1174": {
            "en": "We have received your order.",
            "cn": "我们已经收到您的订单。"
        },
        "1175": {
            "en": "Click here",
            "cn": "点击这里"
        },
        "1176": {
            "en": "to see all of your orders.",
            "cn": "返回我的订单。"
        },
        "1177": {
            "en": "RESET PASSWORD",
            "cn": "重置密码"
        },
        "1178": {
            "en": "Account needs verification",
            "cn": "账号未经验证"
        },
        "1179": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1180": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1181": {
            "en": "Invalid email",
            "cn": "无效的邮箱地址"
        },
        "1182": {
            "en": "Invalid cell phone",
            "cn": "请填写正确的手机号码"
        },
        "1183": {
            "en": "Your first name",
            "cn": "请填写您的名字"
        },
        "1184": {
            "en": "Your last name",
            "cn": "请填写您的姓"
        },
        "1185": {
            "en": "Duplicated email",
            "cn": "该邮箱已被注册"
        },
        "1186": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1187": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1188": {
            "en": "Invalid host",
            "cn": "无效的服务"
        },
        "1189": {
            "en": "Too frequent",
            "cn": "操作太过频繁"
        },
        "1190": {
            "en": "Invalid validation code",
            "cn": "验证码错误"
        },
        "1191": {
            "en": "cellphone not exist",
            "cn": "该手机号不存在"
        },
        "1200": {
            "en": "Delivery window depends on the delivery area.",
            "cn": "送达时间段取决于您所在的地区。"
        },
        "1201": {
            "en": "Your invoice will be included with your delivery. Please check your juice package.",
            "cn": "您的发票将放在果汁包内一起发出，请注意查收！"
        },
        "1202": {
            "en": "VCLEANSE",
            "cn": "维果清饮品（上海）有限公司"
        },
        "1203": {
            "en": "Online ordering is now available for customers in Shanghai, Beijing, Tianjin, Jiangsu, Zhejiang. Customers from other cities are welcome to call us for delivery inquiries.",
            "cn": "目前江浙沪、京津地区已开通在线订餐！其他地区的客户可通过客服热线咨询订餐。"
        },
        "1204": {
            "en": "You can order {0} bottles at most.",
            "cn": "每天限量供应{0}瓶"
        },
        "1205": {
            "en": "If you would like to hear from us, please leave your information <a href=\"\/enrollment\/register\">here</a>.",
            "cn": "如您愿意收到维果清的最新资讯，请 <a href=\"\/enrollment\/register\">注册</a> 并留下您的信息。"
        },
        "1206": {
            "en": "Package via:",
            "cn": "外送公司："
        },
        "1207": {
            "en": "Zip code:",
            "cn": "邮编："
        },
        "1208": {
            "en": "Zip code",
            "cn": "邮编，选填"
        },
        "1209": {
            "en": "Pick more",
            "cn": "想要更多"
        },
        "1210": {
            "en": "I don't want package for this day",
            "cn": "我不要套餐"
        },
        "1211": {
            "en": "Please type the verification code we send to your mobile phone.",
            "cn": "请输入您的手机收到的验证码。"
        },
        "1212": {
            "en": "Please type a password, and then retype it to confirm.",
            "cn": "请输入密码。"
        },
        "1213": {
            "en": "Please retype password to confirm.",
            "cn": "请重新输入您的密码，以便确认。"
        },
        "1214": {
            "en": "These passwords don't match.",
            "cn": "两次输入的密码不匹配。"
        },
        "1215": {
            "en": "Phone number is required.",
            "cn": "手机号码是您的注册凭据，请输入。"
        },
        "1216": {
            "en": "Verify that you've entered the correct phone number.",
            "cn": "请确认您输入了正确的手机号码。"
        },
        "1217": {
            "en": "Have you previously ordered VCLEANSE by email or phone? If so, please click <a href=\"\/enrollment\/forgotpassword\"><strong>here</strong></a> to reset your password.",
            "cn": "您之前是否通过其他途径（邮件、电话）购买过维果清？如果是，请点击 <a href=\"\/enrollment\/forgotpassword\"><strong>这里</strong></a> 重置密码。"
        },
        "1218": {
            "en": "Package",
            "cn": "选择套餐"
        },
        "1219": {
            "en": "Cancel",
            "cn": "取消"
        },
        "1220": {
            "en": "ORDER NOW",
            "cn": "订餐"
        },
        "1221": {
            "en": "Pay",
            "cn": "付款"
        },
        "1222": {
            "en": "The following payment platform are supported:",
            "cn": "在线支付目前支持如下支付平台："
        },
        "1223": {
            "en": "to place a new order.",
            "cn": "继续订餐。"
        },
        "1224": {
            "en": "Your order has been successfully placed.",
            "cn": "您的订单已经成功订餐。"
        },
        "1225": {
            "en": "If you have any questions, <br>call our customer service at",
            "cn": "需要帮助？欢迎拨打我们的客服热线"
        },
        "1226": {
            "en": "VCLEANSE Brochure",
            "cn": "维果清手册"
        },
        "1227": {
            "en": "VCLEANSE Checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1228": {
            "en": "VCLEANSE brochure",
            "cn": "维果清手册"
        },
        "1229": {
            "en": "VCLEANSE order details and pricing",
            "cn": "维果清订单详情与价格"
        },
        "1230": {
            "en": "VCLEANSE checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1231": {
            "en": "Click to download:",
            "cn": "点击下面的链接下载："
        },
        "1232": {
            "en": "Download is only available after you <a href=\"\/enrollment\/register\">sign in</a>. New here? <a href=\"\/enrollment\/register\">Sign Up</a>",
            "cn": "下载前请先<a href=\"\/enrollment\/register\">登录</a>。没有账户？<a href=\"\/enrollment\/register\">注册</a>"
        },
        "1233": {
            "en": "ECO VILLAGE STORE, Unit 111, 485 Fenlin Road, Shanghai",
            "cn": "上海市枫林路485号11号楼1室，靠近中山南二路"
        },
        "1234": {
            "en": "Please at least add one receiver address, or choose \"Pick up in store\".",
            "cn": "请至少添加一个收件人地址，或者选择 “我希望来门店自取”。"
        },
        "1235": {
            "en": "Total Amount (RMB):",
            "cn": "总价（元）："
        },
        "1236": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1237": {
            "en": "Product(Preferential):",
            "cn": "产品（优惠后）"
        },
        "1238": {
            "en": "Delivery(Preferential):",
            "cn": "外送（优惠后）"
        },
        "1239": {
            "en": "Apply promotion code",
            "cn": "输入优惠码"
        },
        "1240": {
            "en": "Have a promotional code? Enter it below (Not case-sensitive)",
            "cn": "拥有优惠码？请输入（不区分大小写）"
        },
        "1241": {
            "en": "APPLY",
            "cn": "输入"
        },
        "1242": {
            "en": "Please verify that you have typed the code correctly and that you have not used this promotion code previously.",
            "cn": "优惠码输入错误，请确认之前没有使用并重试。"
        },
        "1243": {
            "en": "Design your own cleanse program!",
            "cn": "每天一瓶绿，保证维生素摄入，提高抵抗力（可保存3天）；每天一瓶椰子水，保证补充电解质，延缓衰老（可保存3天）"
        },
        "1244": {
            "en": "About VCLEANSE",
            "cn": "关于维果清"
        },
        "1245": {
            "en": "Follow us:",
            "cn": "找到我们："
        },
        "1246": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1247": {
            "en": "Labor Day holiday notice!",
            "cn": "维果清五一劳动节放假通知！"
        },
        "1248": {
            "en": "Sure to delete this order?",
            "cn": "确定删除该订单吗？"
        },
        "1249": {
            "en": "Sure to delete this address?",
            "cn": "确定删除该地址吗？"
        },
        "1250": {
            "en": "No address",
            "cn": "无收件人地址"
        },
        "1251": {
            "en": "Dear V-Cleanser, you don't have a fully delivery address! <br><br>",
            "cn": "请完善您的收件人地址！<br><br>"
        },
        "1252": {
            "en": "Feedback on website?",
            "cn": "网站使用问题或建议？"
        },
        "1253": {
            "en": "Send Email to our technical support team:",
            "cn": "给我们的技术支持发送邮件："
        },
        "1254": {
            "en": "Tax invoice:",
            "cn": "发票信息："
        },
        "1255": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1256": {
            "en": "Invite code:",
            "cn": "邀请码："
        },
        "1257": {
            "en": "Invite code",
            "cn": "邀请码"
        },
        "1258": {
            "en": "Dear customer, our earth day activity has been fully booked. To continue order, please <a href=\"\/order\/package\">click here</a>! Or contact our customer service.",
            "cn": "尊敬的顾客，您好！我们地球日的活动已经订满了，无法继续订餐。如果您还需要继续订餐请<a href=\"\/order\/package\">点击这里</a>到首页完成订餐！或者联系我们的客服！"
        },
        "1259": {
            "en": "Check your mobile number, please!",
            "cn": "您的手机号码输入有误，请核对！"
        },
        "1260": {
            "en": "Check your invite code, please!",
            "cn": "您的邀请码输入有误，请核对！"
        },
        "1261": {
            "en": "Check your input, please!",
            "cn": "您的输入有误，请核对！"
        },
        "1262": {
            "en": "Please remember your order number!",
            "cn": "请牢记您的订单号！"
        },
        "1263": {
            "en": "Amount Paid (RMB):",
            "cn": "已付金额（元）："
        },
        "1264": {
            "en": "You choose pay on delivery.",
            "cn": "您选择的付款方式为货到付款。"
        },
        "1265": {
            "en": "Back to Home",
            "cn": "回到首页"
        },
        "1266": {
            "en": "Sorry, earth day activity was closed on April 19. Now you can fill in the following form join our waitlist!",
            "cn": "很抱歉，活动订餐已于4月19日截止。现在，您可以填写下面的表单将全绿系列加入心愿单，下一个全绿系列就是你的哦！"
        },
        "1267": {
            "en": "Join Waitlist",
            "cn": "加入心愿单"
        },
        "1268": {
            "en": "We have received your wish.",
            "cn": "我们已经收到您的心愿单。"
        },
        "1269": {
            "en": "Sorry, earth day activity was closed on April 19.",
            "cn": "很抱歉，活动订餐已于4月19日截止。"
        },
        "1270": {
            "en": "Please verify that your promotion code can be applied to current order (check order subtotal).",
            "cn": "该优惠码不适用于当前订单价格。"
        },
        "1271": {
            "en": "Mobile is required",
            "cn": "请输入手机号"
        },
        "1272": {
            "en": "Password is required",
            "cn": "请输入密码"
        },
        "1273": {
            "en": "Sorry, your operating too frequently.",
            "cn": "抱歉，您的操作过于频繁。"
        },
        "1274": {
            "en": "Sorry, wrong validation code.",
            "cn": "抱歉，您输入的手机验证码错误。"
        },
        "1275": {
            "en": "First name is required",
            "cn": "请输入您的名字"
        },
        "1276": {
            "en": "Last name is required",
            "cn": "请输入您的姓氏"
        },
        "1277": {
            "en": "Email is required",
            "cn": "请输入您的电子邮件"
        },
        "1278": {
            "en": "Sorry, wrong mobile number.",
            "cn": "请输入正确的手机号码。（如果需要留下固话，请留在地址栏）"
        },
        "1279": {
            "en": "Password:",
            "cn": "密码："
        },
        "1280": {
            "en": "Successfully Ordered",
            "cn": "订餐成功"
        },
        "1281": {
            "en": "Delivery Address & Start Date",
            "cn": "您的信息"
        },
        "1282": {
            "en": "Store",
            "cn": "在线商店"
        },
        "1283": {
            "en": "Cleanse",
            "cn": "清体套餐"
        },
        "1284": {
            "en": "Subscriptions",
            "cn": "清体计划"
        },
        "1285": {
            "en": "V-HAPPY Cards",
            "cn": "V-HAPPY卡"
        },
        "1286": {
            "en": "News",
            "cn": "最新资讯"
        },
        "1287": {
            "en": "Healthy Office",
            "cn": "健康办公"
        },
        "1288": {
            "en": "Days",
            "cn": "天"
        },
        "1289": {
            "en": "Juices",
            "cn": "缤纷果饮"
        },
        "1290": {
            "en": "See All Of Our Products",
            "cn": "查看我们所有果汁"
        },
        "1291": {
            "en": "Spicy Lemonade",
            "cn": "每次外送加一瓶大师清体柠檬水"
        },
        "1292": {
            "en": "Fragrant Clear",
            "cn": "每次外送加一瓶芳香椰子水"
        },
        "1293": {
            "en": "All Products",
            "cn": "套餐及单品"
        },
        "1294": {
            "en": "Nutrition Center",
            "cn": "营养中心"
        },
        "1295": {
            "en": "About Us",
            "cn": "关于我们"
        },
        "1296": {
            "en": "Our Story",
            "cn": "关于维果清"
        },
        "1297": {
            "en": "Philosophy",
            "cn": "公司理念"
        },
        "1298": {
            "en": "Detox 101",
            "cn": "Detox 101"
        },
        "1299": {
            "en": "Detox Quiz",
            "cn": "Detox Quiz"
        },
        "1300": {
            "en": "Q&amp;A",
            "cn": "问答"
        },
        "1301": {
            "en": "Survial Guide",
            "cn": "Survial Guide"
        },
        "1302": {
            "en": "Store Locations",
            "cn": "门店地址"
        },
        "1304": {
            "en": "Join VCLEANSE",
            "cn": "加入维果清"
        },
        "1305": {
            "en": "Black",
            "cn": "黑"
        },
        "1306": {
            "en": "Green",
            "cn": "绿"
        },
        "1307": {
            "en": "White",
            "cn": "白"
        },
        "1308": {
            "en": "Red",
            "cn": "红"
        },
        "1309": {
            "en": "Yellow",
            "cn": "黄"
        },
        "1310": {
            "en": "New customer, signup!",
            "cn": "新客户，注册！"
        },
        "1311": {
            "en": "BACK TO CLEANSE",
            "cn": "回到清体套餐"
        },
        "1312": {
            "en": "CLEANSES",
            "cn": "在线订餐"
        },
        "1313": {
            "en": "You can place your oder successfully as a guest, but you can track your order status and change your oders more freely only if you own a Vcleanse account.&nbsp;<a href=\"/enrollment/register\" data-weave=\"widget/href/main\" target=\"_blank\">Register Now</a>",
            "cn": "您可以直接购买，拥有维果清账号可以帮助您更好地追踪和管理您的订单。<a href=\"/enrollment/register\" data-weave=\"widget/href/main\" target=\"_blank\">立即注册</a>"
        },
        "1314": {
            "en": "GG",
            "cn": "GG"
        },
        "1315": {
            "en": "No Matching Event Id",
            "cn": "目前无活动"
        },
        "1316": {
            "en": "Event has not started",
            "cn": "活动未开始"
        },
        "1317": {
            "en": "Event has already ended",
            "cn": "活动已结束"
        },
        "1318": {
            "en": "Enter any referral codes",
            "cn": "有“团体/VIP”号码？请输入（作为订单来源凭据使用，输入即可）"
        },
        "1319": {
            "en": "(free shipping)",
            "cn": "（包邮）"
        },
        "1340": {
            "cn": "查看详情",
            "en": "Details"
        },
        "1341": {
            "en": "Sending...",
            "cn": "发送中..."
        },
        "1342": {
            "en": "SEND AGAIN",
            "cn": "再次发送"
        },
        "1343": {
            "en": "CAN'T GET VERIFICATION CODE? SKIP!",
            "cn": "无法获取短信验证码？跳过手机验证！"
        },
        "1344": {
            "en": "Input verification code",
            "cn": "输入右侧图片中验证码"
        },
        "1345": {
            "en": "Please type the verification code.",
            "cn": "请输入验证码。"
        },
        "1346": {
            "en": "Delivery Address",
            "cn": "外送信息"
        },
        "1347": {
            "en": "Delivery Date",
            "cn": "外送时间"
        },
        "1348": {
            "en": "SAVE CHANGES",
            "cn": "保存更改"
        },
        "1349": {
            "en": "GO BACK",
            "cn": "返回"
        },
        "1350": {
            "en": "Find My Orders",
            "cn": "查询订单"
        },
        "1351": {
            "en": "Users Instrutions",
            "cn": "套餐使用建议"
        },
        "1352": {
            "en": "Cleanse aficionados can design their own custom cleanse.  Please select the flavors you would like.",
            "cn": "随心所欲，定制属于你自己的清体达人套餐。"
        },
        "1353": {
            "en": "SEARCH",
            "cn": "搜索"
        },
        "1354": {
            "en": "Search by the mobile phone number you entered with your order",
            "cn": "根据收件人手机号码查询"
        },
        "1355": {
            "en": "New Customers?",
            "cn": "没有账户？"
        },
        "1356": {
            "en": "You do not need to create an account to checkout. You can checkout as a guest and create an account later.",
            "cn": "您可以以访客的身份继续浏览或者完成订餐。"
        },
        "1357": {
            "en": "Continue as Guest",
            "cn": "以访客身份继续"
        },
        "1358": {
            "en": "Payment and Invoice",
            "cn": "支付及发票"
        },
        "1359": {
            "en": "to search my orders",
            "cn": "查询我的订单。"
        },
        "1360": {
            "en": "Order create failed!",
            "cn": "订单创建失败。"
        },
        "1361": {
            "en": "Cold Pressed",
            "cn": "冷榨液压"
        },
        "1362": {
            "en": "No records.",
            "cn": "该手机号码没有订单记录。"
        },
        "1363": {
            "en": "Wechat",
            "cn": "微信人工客服"
        },
        "1364": {
            "en": "Scan qr-code!",
            "cn": "扫描二维码，随时随地语音咨询！"
        },
        "1365": {
            "en": "Page has expired. Please refresh page and try again!",
            "cn": "页面已过期，刷新页面后请重试。"
        },
        "1366": {
            "en": "Order has been locked. You can't modify!",
            "cn": "订单已经锁定，不能修改。"
        },
        "1367": {
            "en": "Invalid promotion code.",
            "cn": "无效的优惠码。"
        },
        "1368": {
            "en": "This promotion code has expired.",
            "cn": "该优惠码已过期。"
        },
        "1369": {
            "en": "Invalid order.",
            "cn": "无效的订单号。"
        },
        "1370": {
            "en": "Thanks for using promotion code.",
            "cn": "感谢您使用优惠码。"
        },
        "1371": {
            "en": "Free delivery for this order.",
            "cn": "运费已减免。"
        },
        "1372": {
            "en": "Free \"Fragrant Clear\" everyday for this order.",
            "cn": "已经为该订单添加免费的椰子水。"
        },
        "1373": {
            "en": "Free delivery and free \"Fragrant Clear\" everyday for this order.",
            "cn": "您的订单已经免运费，送椰子水。"
        },
        "1374": {
            "en": "Learn about <a href=\"/juice/index\">our juices</a> here.",
            "cn": "快了解一下我们的<a href=\"/juice/index\">缤纷果汁</a>吧。"
        },
        "1375": {
            "en": "To protect your privacy, we need to verify the phone number.",
            "cn": "为了保护您的隐私，我们需要验证手机号码。"
        },
        "1376": {
            "en": "Your payment failed.",
            "cn": "付款失败。"
        },
        "1377": {
            "en": "Sorry!",
            "cn": "抱歉！"
        },
        "1378": {
            "en": "Pay failed",
            "cn": "付款失败"
        },
        "1379": {
            "en": "Select a shipping address",
            "cn": "委托外送取餐"
        },
        "1380": {
            "en": "Shanghai",
            "cn": "上海"
        },
        "1381": {
            "en": "View detail",
            "cn": "查看详情"
        },
        "1382": {
            "en": "Business hours:",
            "cn": "营业时间："
        },
        "1383": {
            "en": "Leave your contact information",
            "cn": "留下您的联系方式"
        },
        "1384": {
            "en": "Free \"Spicy Lemonade\" everyday for this order.",
            "cn": "已经为该订单添加免费的柠檬水。"
        },
        "1385": {
            "en": "Free delivery and free \"Spicy Lemonade\" everyday for this order.",
            "cn": "您的订单已经免运费，送柠檬水。"
        },
        "1386": {
            "en": "Dear V-Cleanser, you don't have a fully contact information! <br><br>",
            "cn": "请完善您的联系方式！<br><br>"
        },
        "1387": {
            "en": "Contact tel:",
            "cn": "联系电话："
        },
        "1388": {
            "en": "This code has used",
            "cn": "优惠码已经使用"
        },
        "1389": {
            "en": "Has use disposable code",
            "cn": "已经使用过一次性优惠码了"
        },
        "1390": {
            "en": "Anonymous cannot uset his rule",
            "cn": "匿名用户不能用,请登录后重新下单"
        },
        "1391": {
            "en": "Sorry You have used this type of promotion",
            "cn": "对不起, 你的账户已经使用过这种类型的优惠码"
        },
        "1392": {
            "en": "Sure to use this code",
            "cn": "是否使用该优惠码？"
        },
        "1393": {
            "en": "Sorry, this activity was ended. Please choose our regular packages!",
            "cn": "很抱歉，活动订餐已截止。您可以选择我们的常规套餐！"
        },
        "1394": {
            "en": "Package Detail",
            "cn": "套餐详情"
        },
        "1395": {
            "en": "Receiver name:",
            "cn": "收件人姓名："
        },
        "1396": {
            "en": "Receiver's name is required",
            "cn": "收件人真实姓名，必填"
        },
        "1397": {
            "en": "Name:",
            "cn": "联系人姓名："
        },
        "1398": {
            "en": "Pickup contactor's name is required",
            "cn": "自取联系人真实姓名，必填"
        },
        "1399": {
            "en": "From",
            "cn": "从"
        },
        "1400": {
            "en": "to",
            "cn": "至"
        },
        "1401": {
            "en": "password:",
            "cn": "密码："
        },
        "1402": {
            "en": "Your first name here",
            "cn": "您的名字"
        },
        "1403": {
            "en": "Subscribe",
            "cn": "订阅"
        },
        "1404": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，新鲜资讯抢先看"
        },
        "1405": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "订阅成功！"
        },
        "1406": {
            "en": "Duplicated email",
            "cn": "该邮箱已被订阅"
        },
        "1407": {
            "en": "Your email address has an invalid email address format. Please correct and try again.",
            "cn": "电子邮件格式有误。请尝试重新输入！"
        },
        "1408": {
            "en": "Your mobile number has an invalid format. Please correct and try again.",
            "cn": "手机格式有误。请尝试重新输入！"
        },
        "1409": {
            "en": "Order confirmation has been sent to your mobile via SMS, please check!",
            "cn": "订单确认信息已经通过短信发送到您的注册手机号，请注意查收！"
        },
        "1410": {
            "en": "Leave your email to get order information",
            "cn": "留下邮箱，获取订单信息，新鲜资讯抢先看！"
        },
        "1411": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，维果清新鲜资讯抢先看"
        },
        "1412": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "您已经成功订阅维果清资讯！"
        },
        "1451": {
            "en": "Check all online payment methods",
            "cn": "查看全部在线支付方式"
        },
        "1452": {
            "en": "I understand",
            "cn": "我知道了"
        },
        "1454": {
            "en": "Payment completed",
            "cn": "已经完成支付"
        },
        "1455": {
            "en": "Have a problem",
            "cn": "支付遇到问题"
        },
        "1456": {
            "en": "Comfirm",
            "cn": "付款确认"
        },
        "1457": {
            "en": "Close",
            "cn": "关闭"
        },
        "1458": {
            "en": "Pay now",
            "cn": "去付款"
        },
        "1459": {
            "en": "V Station",
            "cn": "维果清门店"
        },
        "1460": {
            "en": "Quantity",
            "cn": "选择购买数量"
        },
        "1461": {
            "en": "Account Safety",
            "cn": "账户安全"
        },
        "1462": {
            "en": "Failed",
            "cn": "更新失败"
        },
        "1463": {
            "en": "Change",
            "cn": "换一个"
        },
        "1464": {
            "en": "Code here",
            "cn": "输入验证码"
        },
        "1465": {
            "en": "Successfully change password!",
            "cn": "成功更新密码！"
        },
        "1466": {
            "en": "Please hoover your mouse to the image below, and click on the button on the top right to share this on WeChat and Weibo!",
            "cn": "将鼠标移到下图上，点击右上角的图标，把这份温暖分享到微信、微博吧！"
        },
        "1467": {
            "en": "* Delivery fee of RMB 25 is not included.",
            "cn": "* 每日25元运费另计。"
        },
        "1468": {
            "en": "Only for warming day",
            "cn": "优惠码只能对暖体日使用."
        },
        "1469": {
            "en": "This area is not available",
            "cn": "该地区暂时不能外送"
        },
        "1470": {
            "en": "Edit address failed!",
            "cn": "地址修改失败!"
        },
        "1471": {
            "en": "The new address will generate additional freight!",
            "cn": "要修改的地址将产生额外运费!"
        },
        "1472": {
            "en": "Order Review",
            "cn": "查看订单"
        },
        "1473": {
            "en": "Mine",
            "cn": "我的"
        },
        "1474": {
            "en": "Others",
            "cn": "其它"
        },
        "1475": {
            "en": "Delivery date:",
            "cn": "外送日期："
        },
        "1476": {
            "en": "Delivery address:",
            "cn": "外送地址："
        },
        "1477": {
            "en": "Detailed list:",
            "cn": "清单："
        },
        "1478": {
            "en": "Status:",
            "cn": "状态："
        },
        "1479": {
            "en": "The child orders",
            "cn": "子订单"
        },
        "1480": {
            "en": "Day",
            "cn": "天"
        },
        "1481": {
            "en": "Invalid payment request!",
            "cn": "无效的付款请求。"
        },
        "1482": {
            "en": "Purple Broth",
            "cn": "每次外送加一瓶紫甘蓝靓汤"
        },
        "1483": {
            "en": "Workshops",
            "cn": "维果清工作坊"
        },
        "1484": {
            "en": "Individual",
            "cn": "个人报名入口"
        },
        "1485": {
            "en": "Group or Corporate",
            "cn": "团体报名入口"
        },
        "1600": {
            "en": "SUBSCRIPTIONS",
            "cn": "预订"
        },
        "1601": {
            "en": "SELECT",
            "cn": "选择"
        },
        "1602": {
            "en": "Free \"Coconut Water\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶椰子水。"
        },
        "1603": {
            "en": "Free \"Spicy Lemonade\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶柠檬水。"
        },
        "1604": {
            "en": "Free \"Moon Bear's Honey Tree\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶月熊蜂蜜树。"
        },
        "1605": {
            "en": "Not available on the day",
            "cn": "当天不供应"
        },
        "1606": {
            "en": "This is a gift",
            "cn": "这是礼品"
        },
        "1607": {
            "en": "Gift Blessing",
            "cn": "礼品赠言"
        },
        "1608": {
            "en": "You have to order:",
            "cn": "您已订餐："
        },
        "1609": {
            "en": "Products Info",
            "cn": "单品详情"
        },
        "1610": {
            "en": "Submit",
            "cn": "提交"
        },
        "1611": {
            "en": "Thanks for your support!",
            "cn": "谢谢您的参与！"
        },
        "1612": {
            "en": "Please enter...",
            "cn": "请输入..."
        },
        "1613": {
            "en": "(Multiple Choice)",
            "cn": "（多选题）"
        },
        "1614": {
            "en": "Image verification code:",
            "cn": "图片验证码："
        },
        "1615": {
            "en": "VERIFY AND SEARCH",
            "cn": "验证并查询"
        },
        "1616": {
            "en": "to:",
            "cn": "到："
        },
        "1617": {
            "en": "Security code:",
            "cn": "手机验证码："
        },
        "1618": {
            "en": "Not paid yet",
            "cn": "未支付"
        },
        "1619": {
            "en": "Paid",
            "cn": "已支付"
        },
        "1620": {
            "en": "Action",
            "cn": "操作"
        },
        "1621": {
            "en": "Details",
            "cn": "查看"
        },
        "1622": {
            "en": "Hotline[9AM - 7PM]:",
            "cn": "客服热线[上午9点 - 晚上7点]："
        },
        "1623": {
            "en": "Invalid shipping address",
            "cn": "无效的收件地址"
        },
        "1624": {
            "en": "This order cannot be deleted, please contact customer service.",
            "cn": "该订单无法删除，请联系客服。"
        },
        "1625": {
            "en": "You have ordered before, you can not use the coupon code.",
            "cn": "您已经有过订单，无法使用该优惠码"
        },
        "1626": {
            "en": "Distribution area",
            "cn": "外送范围"
        },
        "1627": {
            "en": "Workshop",
            "cn": "健康餐厅"
        }
    });
    /* String help function */
    // Format: "Hello, {0}. {1}".format("Mr","DJ")

    function stringFormat(s, args) {
        var i = 0;
        var iLens = args.length;
        for (i = 0; i < iLens; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, args[i]);
        }
        return s;
    }

    return Widget.extend(function($element, widgetName, blurbId, attr) {
        var me = this;

        me.blurbId = blurbId;
        me.attr = attr || "";
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var format = $me.data("format");

            var translation = me.get(me.blurbId, format);

            if (!translation) {
                return;
            }

            // Attr or text
            if (me.attr) {
                $me.attr(me.attr, translation);
            } else {
                me.html(translation);
            }

            // Log blurb ID
            //$me.attr("blurb", me.blurbId);
        },
        "get": function(blurbId, format) {
            var me = this;

            var language = context.language || "en";
            var translationSet;
            var translation = "";

            if (!blurbId) {
                return "";
            }

            translationSet = blurbDict[blurbId.toString()];

            if (!translationSet) {
                return "";
            }

            if (context.isDebug) {
                translation += "[" + blurbId + "]";
            }
            translation += translationSet[language];

            // format: "x, y, z" --> ['x', 'y', 'z']
            // Replace {0} {1}
            if (translation.indexOf("{0}") >= 0 && format !== undefined) {
                format = format.toString().split(",");
                translation = stringFormat(translation, format);
            }

            return translation;
        }
    });
});
define('widget/basic/regex',["jquery",
    "underscore",
    "underscore.string"
], function($) {
    "use strict";

    return {
        "email": function(email) {
            email = _.clean(email).replace(" ", "");

            var re = /\w@\w*\.\w/;
            return re.test(email);
        },
        "mobile": function(mobile) {
            mobile = _.clean(mobile).replace(" ", "");

            var re = /^((\+86)|(86))?(1[3|4|5|7|8])\d{9}$/;
            return re.test(mobile);
        }
    };
});
define('widget/basic/jqueryext',["jquery",
    "widget/blurb/main",
    "widget/basic/regex",
    "jquery.validation"
], function($, Blurb, regex) {
    "use strict";

    var blurb = new Blurb($(document.body));

    jQuery.validator.addMethod("mobile", function(value, element) { 
        return regex.mobile(value); 
    }, blurb.get("1408"));
});
define('widget/basic/number',["jquery"], function($) {
    "use strict";

    return {
        "getDecimal": function(numeral, digits) {
            digits = digits || 2;
            // 18.00
            numeral = parseFloat(numeral);
            if (isNaN(numeral)) {
                return false;
            }

            /*var times = Math.pow(10, digits);

            // get two decimal while decimal > 2
            // 23.3456 --> 23.35
            var floatNumeral = Math.round(numeral * times) / times;
            var stringNumeral = floatNumeral.toString();
            var pos = stringNumeral.indexOf('.');
            if (pos < 0) {
                pos = stringNumeral.length;
                stringNumeral += '.';
            }
            while (stringNumeral.length <= pos + digits) {
                stringNumeral += '0';
            }
            return stringNumeral;*/

            // USE toFixed()
            return numeral.toFixed(digits);
        }
    };
});
define('widget/blurb/main_backup',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "underscore"
], function(Widget, $, context) {

    var BLURB = window.BLURB || {};

    var blurbDict = _.extend(BLURB, {
        "1000": {
            "en": "Sign in",
            "cn": "登录"
        },
        "1001": {
            "en": "My VCLEANSE",
            "cn": "我的维果清"
        },
        "1002": {
            "en": "Questions? Contact us (9AM - 7PM):",
            "cn": "欢迎咨询我们的健康管家（上午9点 - 晚上7点）："
        },
        "1003": {
            "en": "Your Order",
            "cn": "开始订购"
        },
        "1004": {
            "en": "Welcome, ",
            "cn": "欢迎您，"
        },
        "1005": {
            "en": "STEP 1:",
            "cn": "第一步："
        },
        "1006": {
            "en": "STEP 2:",
            "cn": "第二步："
        },
        "1007": {
            "en": "STEP 3:",
            "cn": "第三步："
        },
        "1008": {
            "en": "Select start date",
            "cn": "套餐开始日期"
        },
        "1009": {
            "en": "Confirm Order",
            "cn": "确认订单"
        },
        "1010": {
            "en": "Pay",
            "cn": "付款"
        },
        "1011": {
            "en": "Retrieve Password",
            "cn": "找回密码"
        },
        "1012": {
            "en": "Create your VCLEANSE account",
            "cn": "创建您的维果清帐号"
        },
        "1013": {
            "en": "Basic Information",
            "cn": "个人基本信息"
        },
        "1014": {
            "en": "Delivery Addresses",
            "cn": "收件人地址"
        },
        "1015": {
            "en": "My Orders",
            "cn": "我的订单"
        },
        "1016": {
            "en": "Surpass Court, 570 Yongjia Road, Shanghai",
            "cn": "上海市永嘉路570号永嘉庭"
        },
        "1017": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1018": {
            "en": "Select program",
            "cn": "选择您的清体套餐"
        },
        "1019": {
            "en": "Choose one of our programs, or create your own.",
            "cn": "选择适合您的套餐，享受订购吧！"
        },
        "1020": {
            "en": "Create an account",
            "cn": "注册"
        },
        "1021": {
            "en": "Sign out",
            "cn": "退出"
        },
        "1022": {
            "en": "Contact us",
            "cn": "联系我们"
        },
        "1024": {
            "en": "When would you like to start your cleanse?",
            "cn": "请选择一天开始您的维果清清体之旅？"
        },
        "1025": {
            "en": "Past two weeks",
            "cn": "上两周"
        },
        "1026": {
            "en": "Next two weeks",
            "cn": "下两周"
        },
        "1027": {
            "en": "Pick up in store",
            "cn": "来店自取"
        },
        "1028": {
            "en": "Change",
            "cn": "修改"
        },
        "1029": {
            "en": "Add",
            "cn": "添加"
        },
        "1030": {
            "en": "Too late <br>to order",
            "cn": "您错过了~"
        },
        "1031": {
            "en": "Confirm",
            "cn": "确认"
        },
        "1032": {
            "en": "Order",
            "cn": "您的订单"
        },
        "1033": {
            "en": "Product:",
            "cn": "产品："
        },
        "1034": {
            "en": "Delivery:",
            "cn": "配送："
        },
        "1035": {
            "en": "Payment Total:",
            "cn": "总价："
        },
        "1036": {
            "en": "Company invoice:",
            "cn": "公司发票："
        },
        "1037": {
            "en": "Personal invoice:",
            "cn": "个人发票："
        },
        "1038": {
            "en": "Invoice amount:",
            "cn": "发票金额："
        },
        "1039": {
            "en": "Invoice title:",
            "cn": "发票抬头："
        },
        "1040": {
            "en": "Total:",
            "cn": "总价："
        },
        "1041": {
            "en": "Your tax invoice (fapiao)",
            "cn": "发票信息"
        },
        "1043": {
            "en": "VCLEANSE will issue an invoice for products. For an invoice for delivery fees, please contact the delivery company",
            "cn": "维果清发票金额仅为产品，若需运费发票，请与外送公司联系。"
        },
        "1044": {
            "en": "Yes, I would like an invoice",
            "cn": "需要发票"
        },
        "1045": {
            "en": "Personal invoice",
            "cn": "个人发票"
        },
        "1046": {
            "en": "Company invoice",
            "cn": "公司发票"
        },
        "1047": {
            "en": "Company title",
            "cn": "公司名称"
        },
        "1048": {
            "en": "Purchase a gift card",
            "cn": "购买礼品卡"
        },
        "1049": {
            "en": "Give the gift of health and happiness!",
            "cn": "送出健康快乐的礼物"
        },
        "1050": {
            "en": "BUY",
            "cn": "购买"
        },
        "1051": {
            "en": "Find your juice type",
            "cn": "选购果汁类型"
        },
        "1052": {
            "en": "LEARN MORE",
            "cn": "更多选择"
        },
        "1053": {
            "en": "WeChat:",
            "cn": "微信："
        },
        "1054": {
            "en": "First Name:",
            "cn": "名字："
        },
        "1055": {
            "en": "Last name:",
            "cn": "姓："
        },
        "1056": {
            "en": "Mobile:",
            "cn": "手机号码："
        },
        "1057": {
            "en": "Shipping address:",
            "cn": "收件人地址："
        },
        "1058": {
            "en": "Pick up in store:",
            "cn": "来店自取："
        },
        "1059": {
            "en": "Thank you for your order! If you need to change or cancel your order, please give us a call at",
            "cn": "感谢您的选购。如果您需要修改或者取消订单，请给我们来电"
        },
        "1060": {
            "en": "Order details",
            "cn": "订单详情"
        },
        "1061": {
            "en": "Order number:",
            "cn": "订单号："
        },
        "1062": {
            "en": "Payment Summary:",
            "cn": "支付详情："
        },
        "1063": {
            "en": "If you have any questions, please give us a call at",
            "cn": "如果您在购买过程中遇到任何问题请联系我们清体管家"
        },
        "1064": {
            "en": "Thanks for your order",
            "cn": "感谢您的选购"
        },
        "1065": {
            "en": "VCLEANSE Team",
            "cn": "维果清小组"
        },
        "1066": {
            "en": "Deliver to:",
            "cn": "发送到："
        },
        "1067": {
            "en": "Add Another Address",
            "cn": "添加地址"
        },
        "1068": {
            "en": "CREATE ACCOUNT",
            "cn": "创建账户"
        },
        "1069": {
            "en": "UPDATE",
            "cn": "更新"
        },
        "1070": {
            "en": "DELETE",
            "cn": "删除"
        },
        "1071": {
            "en": "SET AS DEFAULT",
            "cn": "设置为默认地址"
        },
        "1072": {
            "en": "Default address",
            "cn": "默认地址"
        },
        "1073": {
            "en": "Edit delivery address",
            "cn": "修改收件人地址"
        },
        "1074": {
            "en": "Add a delivery address",
            "cn": "添加收件人地址"
        },
        "1075": {
            "en": "CANCEL",
            "cn": "取消"
        },
        "1076": {
            "en": "Email:",
            "cn": "电子邮箱："
        },
        "1077": {
            "en": "New password:",
            "cn": "新的密码："
        },
        "1078": {
            "en": "Confirm password:",
            "cn": "确认密码："
        },
        "1079": {
            "en": "SAVE",
            "cn": "保存"
        },
        "1080": {
            "en": "SIGN IN",
            "cn": "登录"
        },
        "1081": {
            "en": "I have a VCLEANSE account",
            "cn": "已经有维果清账号"
        },
        "1083": {
            "en": "Forgot password?",
            "cn": "忘记密码？"
        },
        "1084": {
            "en": "SEND VERIFICATION CODE",
            "cn": "发送验证码"
        },
        "1085": {
            "en": "Security code:",
            "cn": "验证码："
        },
        "1086": {
            "en": "Preferred delivery window?",
            "cn": "选择理想配送时间段"
        },
        "1087": {
            "en": "Deliver to this address",
            "cn": "配送到此地址"
        },
        "1088": {
            "en": "Edit this address",
            "cn": "修改地址"
        },
        "1090": {
            "en": "REMOVE THIS DAY FROM ORDER",
            "cn": "删除"
        },
        "1091": {
            "en": "UPDATE ORDER",
            "cn": "更新订单"
        },
        "1092": {
            "en": "Order status:",
            "cn": "订单状态："
        },
        "1093": {
            "en": "Tracking number:",
            "cn": "物流单号："
        },
        "1094": {
            "en": "+ Change",
            "cn": "+ 修改"
        },
        "1096": {
            "en": "+ See more",
            "cn": "+ 查看更多"
        },
        "1097": {
            "en": "+ Learn more",
            "cn": "+ 更多信息"
        },
        "1098": {
            "en": "Order number",
            "cn": "订单号"
        },
        "1099": {
            "en": "Order date",
            "cn": "订单日期"
        },
        "1100": {
            "en": "Start date",
            "cn": "开始饮用日期"
        },
        "1101": {
            "en": "Total Amount (RMB)",
            "cn": "总价（元）"
        },
        "1102": {
            "en": "Order Status",
            "cn": "订单状态"
        },
        "1103": {
            "en": "+ Learn more",
            "cn": "+ 了解更多"
        },
        "1104": {
            "en": "BUY",
            "cn": "购买"
        },
        "1105": {
            "en": "Questions about your delivery?",
            "cn": "有配送问题？"
        },
        "1106": {
            "en": "Call our delivery company Yamato Express at",
            "cn": "拨打我们配送伙伴宅急便"
        },
        "1107": {
            "en": "Have your order tracking number ready when you call. They speak English and Chinese.",
            "cn": "他们提供中英文服务帮助您跟踪订单状态"
        },
        "1108": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1109": {
            "en": "Click on the button below to pay via Alipay!",
            "cn": "选择下面的支付方式完成支付!"
        },
        "1110": {
            "en": "Alipay",
            "cn": "支付宝"
        },
        "1111": {
            "en": "Payment method",
            "cn": "支付方式"
        },
        "1112": {
            "en": "Pay online",
            "cn": "在线支付"
        },
        "1113": {
            "en": "Payment on delivery",
            "cn": "货到付款"
        },
        "1114": {
            "en": "NEXT",
            "cn": "下一步"
        },
        "1115": {
            "en": "CONFIRM",
            "cn": "确认"
        },
        "1116": {
            "en": "Tel:",
            "cn": "联系电话："
        },
        "1117": {
            "en": "CLICK HERE TO ADD ANOTHER ORDER",
            "cn": "点击开始您的订购"
        },
        "1118": {
            "en": "RETURN TO MY ORDERS",
            "cn": "返回我的订单"
        },
        "1119": {
            "en": "You can cancel your order online at any time up to 24 hours before your start date. If you are within 24 hours of your start date and need to modify your order, please call us at 4001-V-HAPPY. If you selected online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.",
            "cn": "您可以在订单配送前24小时取消您的订单。为了确保准时配送，请在配送前24小时完成支付。"
        },
        "1120": {
            "en": "Gender:",
            "cn": "性别："
        },
        "1121": {
            "en": "Female",
            "cn": "女"
        },
        "1122": {
            "en": "Male",
            "cn": "男"
        },
        "1123": {
            "en": "Birthday:",
            "cn": "生日："
        },
        "1124": {
            "en": "Payment Status",
            "cn": "支付状态"
        },
        "1125": {
            "en": "Payment Status:",
            "cn": "支付状态："
        },
        "1126": {
            "en": "PAY ONLINE",
            "cn": "付款"
        },
        "1127": {
            "en": "Home",
            "cn": "首页"
        },
        "1128": {
            "en": "Store address:",
            "cn": "门店地址："
        },
        "1129": {
            "en": "OK",
            "cn": "确定"
        },
        "1130": {
            "en": "Password",
            "cn": "密码"
        },
        "1131": {
            "en": "Confirm password",
            "cn": "确认密码"
        },
        "1132": {
            "en": "Your Name Here",
            "cn": "您的姓名"
        },
        "1133": {
            "en": "Your Last Name Here",
            "cn": "您的姓氏"
        },
        "1134": {
            "en": "Mobile number is required",
            "cn": "11位手机号码，必填"
        },
        "1136": {
            "en": "Full address is required",
            "cn": "完整收件地址（不需要重复填写市、区），必填"
        },
        "1137": {
            "en": "Your mobile phone",
            "cn": "您的手机号码"
        },
        "1138": {
            "en": "Your Email Address",
            "cn": "您的电子邮件"
        },
        "1139": {
            "en": "Input verification code sent to your mobile phone",
            "cn": "输入您的手机收到的验证码"
        },
        "1140": {
            "en": "Mobile",
            "cn": "手机号码"
        },
        "1141": {
            "en": "Order successful submit, payment as soon as possible please!",
            "cn": "订单提交成功，请您尽快付款！"
        },
        "1142": {
            "en": "Amount Due (RMB):",
            "cn": "应付金额（元）："
        },
        "1143": {
            "en": "Please complete payment within 24 hours. Orders will be cancelled after 24 hours with no payment.",
            "cn": "立即支付{0}元，即可完成订单。 请您在24小时内完成支付，否则订单会被自动取消。"
        },
        "1144": {
            "en": "Reminder!",
            "cn": "注意！"
        },
        "1145": {
            "en": "Dear V-Cleanser, please pickup your orders in the store until you have a delivery address! <br>Click here to <a href=\"\/account\/receiver\">add delivery address</a>, or you can also edit delivery address by click \"+ Change\" on calendar.",
            "cn": "您还没有填写收件人地址哦！如果不添加请不要忘记来门店提取您的维果清！<br>点击这里<a href=\"\/account\/receiver\">添加收件人地址</a>，或者您也可以在对应日期点击 “+ 修改” ，然后编辑收件人地址。"
        },
        "1146": {
            "en": "Dear V-Cleanser, you will choose your own programs. <br>Click \"+ Add\" on calendar to start your order!",
            "cn": "您选择创建自己的维果清套餐，请在对应的日期点击 “+ 添加” 开始您的订购！"
        },
        "1147": {
            "en": "Dear V-Cleanser, please order again from order page.",
            "cn": "您选购的商品信息有误，请回到订购页面重新订购。"
        },
        "1148": {
            "en": "Success!",
            "cn": "成功！"
        },
        "1149": {
            "en": "Successfully set this delivery address to default!",
            "cn": "成功设置为默认地址！"
        },
        "1150": {
            "en": "Successfully delete this delivery address!",
            "cn": "成功删除该收件人地址！"
        },
        "1151": {
            "en": "Successfully update this delivery address!",
            "cn": "成功更新该收件人地址！"
        },
        "1152": {
            "en": "Successfully add this new delivery address!",
            "cn": "成功增加新的收件人地址！"
        },
        "1153": {
            "en": "Successfully update my basic information!",
            "cn": "成功更新我的基本信息！"
        },
        "1154": {
            "en": "Change password",
            "cn": "如果您需要更改密码，请在此输入"
        },
        "1155": {
            "en": "Customize",
            "cn": "单品购买"
        },
        "1156": {
            "en": "Design your own cleanse program!",
            "cn": "创建您自己的清体套餐！"
        },
        "1157": {
            "en": "Invoice type:",
            "cn": "发票类型："
        },
        "1158": {
            "en": "Company",
            "cn": "公司发票"
        },
        "1159": {
            "en": "Personal",
            "cn": "个人发票"
        },
        "1160": {
            "en": "New VCLEANSE password has sent to your mobile phone, check please!",
            "cn": "新的VCLEANSE密码已经发送到您的手机，请查收！"
        },
        "1161": {
            "en": "Choose your cleanse program duration:",
            "cn": "选择清体套餐理想天数："
        },
        "1162": {
            "en": "Not yet delivered",
            "cn": "未配送"
        },
        "1163": {
            "en": "Delivering",
            "cn": "配送中"
        },
        "1164": {
            "en": "Delivered",
            "cn": "已配送"
        },
        "1165": {
            "en": "Dear new V-Cleanser,",
            "cn": "亲爱的新晋维果清达人，"
        },
        "1166": {
            "en": "Congrats! You are about to embark on a fun journey with us!",
            "cn": "恭喜，你将跟我们开始一段非常有趣的新旅程！"
        },
        "1167": {
            "en": "Who says living healthily is hard? In our eyes, it can be stylish, hip, and full of sparks.",
            "cn": "健康生活绝不枯燥，在我们眼中，它是有趣的，充满时尚感和惊喜。"
        },
        "1168": {
            "en": "Finish your <a href=\"/account/basic\">VCleanse profile</a>.",
            "cn": "补充完整您的<a href=\"/account/basic\">信息</a>。"
        },
        "1169": {
            "en": "Place your <a href=\"order\" data-weave=\"widget/href/main\">first order</a>.",
            "cn": "来首次<a href=\"order\" data-weave=\"widget/href/main\">订购</a>。"
        },
        "1170": {
            "en": "Questions?",
            "cn": "有问题？"
        },
        "1171": {
            "en": "We are just one WeChat away!",
            "cn": "我们的客服在微信服务号替您解答。"
        },
        "1172": {
            "en": "Green is the new black. Health is the priceless luxury. Staying fit is the ultimate sexy.",
            "cn": "绿色正在重新诠释经典。健康是最无价的奢侈。保持活力是由内到外性感的最佳注脚。"
        },
        "1173": {
            "en": "Congrats!",
            "cn": "感谢您！"
        },
        "1174": {
            "en": "We have received your order.",
            "cn": "我们已经收到您的订单。"
        },
        "1175": {
            "en": "Click here",
            "cn": "点击这里"
        },
        "1176": {
            "en": "to see all of your orders.",
            "cn": "返回我的订单。"
        },
        "1177": {
            "en": "RESET PASSWORD",
            "cn": "重置密码"
        },
        "1178": {
            "en": "Account needs verification",
            "cn": "账号未经验证"
        },
        "1179": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1180": {
            "en": "Invalid username or password",
            "cn": "用户名或密码错误"
        },
        "1181": {
            "en": "Invalid email",
            "cn": "无效的邮箱地址"
        },
        "1182": {
            "en": "Invalid cell phone",
            "cn": "请填写正确的手机号码"
        },
        "1183": {
            "en": "Your first name",
            "cn": "请填写您的名字"
        },
        "1184": {
            "en": "Your last name",
            "cn": "请填写您的姓"
        },
        "1185": {
            "en": "Duplicated email",
            "cn": "该邮箱已被注册"
        },
        "1186": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1187": {
            "en": "Duplicated mobile",
            "cn": "该手机号已被注册"
        },
        "1188": {
            "en": "Invalid host",
            "cn": "无效的服务"
        },
        "1189": {
            "en": "Too frequent",
            "cn": "操作太过频繁"
        },
        "1190": {
            "en": "Invalid validation code",
            "cn": "验证码错误"
        },
        "1191": {
            "en": "cellphone not exist",
            "cn": "该手机号不存在"
        },
        "1200": {
            "en": "Delivery window depends on the delivery area.",
            "cn": "配送时间段取决于配送地区。"
        },
        "1201": {
            "en": "Your invoice will be included with your delivery. Please check your juice package.",
            "cn": "您的发票将放在果汁包内一起发出，请注意查收！"
        },
        "1202": {
            "en": "VCLEANSE",
            "cn": "维果清饮品（上海）有限公司"
        },
        "1203": {
            "en": "Online ordering is now available for customers in Shanghai, Beijing, Tianjin, Jiangsu, Zhejiang. Customers from other cities are welcome to call us for delivery inquiries.",
            "cn": "目前江浙沪、京津地区已开通在线订购！其他地区的客户可通过客服热线咨询订购。"
        },
        "1204": {
            "en": "You can order {0} bottles at most.",
            "cn": "每天限量供应{0}瓶"
        },
        "1205": {
            "en": "If you would like to hear from us, please leave your information <a href=\"\/enrollment\/register\">here</a>.",
            "cn": "如您愿意收到维果清的最新资讯，请 <a href=\"\/enrollment\/register\">注册</a> 并留下您的信息。"
        },
        "1206": {
            "en": "Package via:",
            "cn": "外送公司："
        },
        "1207": {
            "en": "Zip code:",
            "cn": "邮编："
        },
        "1208": {
            "en": "Zip code",
            "cn": "邮编，选填"
        },
        "1209": {
            "en": "Pick more",
            "cn": "想要更多"
        },
        "1210": {
            "en": "I don't want package for this day",
            "cn": "我不要套餐"
        },
        "1211": {
            "en": "Please type the verification code we send to your mobile phone.",
            "cn": "请输入您的手机收到的验证码。"
        },
        "1212": {
            "en": "Please type a password, and then retype it to confirm.",
            "cn": "请输入密码。"
        },
        "1213": {
            "en": "Please retype password to confirm.",
            "cn": "请重新输入您的密码，以便确认。"
        },
        "1214": {
            "en": "These passwords don't match.",
            "cn": "两次输入的密码不匹配。"
        },
        "1215": {
            "en": "Phone number is required.",
            "cn": "手机号码是您的注册凭据，请输入。"
        },
        "1216": {
            "en": "Verify that you've entered the correct phone number.",
            "cn": "请确认您输入了正确的手机号码。"
        },
        "1217": {
            "en": "Have you previously ordered VCLEANSE by email or phone? If so, please click <a href=\"\/enrollment\/forgotpassword\"><strong>here</strong></a> to reset your password.",
            "cn": "您之前是否通过其他途径（邮件、电话）购买过维果清？如果是，请点击 <a href=\"\/enrollment\/forgotpassword\"><strong>这里</strong></a> 重置密码。"
        },
        "1218": {
            "en": "Package",
            "cn": "选择套餐"
        },
        "1219": {
            "en": "Cancel",
            "cn": "取消"
        },
        "1220": {
            "en": "ORDER NOW",
            "cn": "订购"
        },
        "1221": {
            "en": "Pay",
            "cn": "付款"
        },
        "1222": {
            "en": "The following payment platform are supported:",
            "cn": "在线支付目前支持如下支付平台："
        },
        "1223": {
            "en": "to place a new order.",
            "cn": "继续订购。"
        },
        "1224": {
            "en": "Your order has been successfully placed.",
            "cn": "您的订单已经成功订购。"
        },
        "1225": {
            "en": "If you have any questions, <br>call our customer service at",
            "cn": "需要帮助？欢迎拨打我们的客服热线"
        },
        "1226": {
            "en": "VCLEANSE Brochure",
            "cn": "维果清手册"
        },
        "1227": {
            "en": "VCLEANSE Checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1228": {
            "en": "VCLEANSE brochure",
            "cn": "维果清手册"
        },
        "1229": {
            "en": "VCLEANSE order details and pricing",
            "cn": "维果清订单详情与价格"
        },
        "1230": {
            "en": "VCLEANSE checklist",
            "cn": "维果清清体套餐备忘录"
        },
        "1231": {
            "en": "Click to download:",
            "cn": "点击下面的链接下载："
        },
        "1232": {
            "en": "Download is only available after you <a href=\"\/enrollment\/register\">sign in</a>. New here? <a href=\"\/enrollment\/register\">Sign Up</a>",
            "cn": "下载前请先<a href=\"\/enrollment\/register\">登录</a>。没有账户？<a href=\"\/enrollment\/register\">注册</a>"
        },
        "1233": {
            "en": "ECO VILLAGE STORE, Unit 111, 485 Fenlin Road, Shanghai",
            "cn": "上海市枫林路485号11号楼1室，靠近中山南二路"
        },
        "1234": {
            "en": "Please at least add one receiver address, or choose \"Pick up in store\".",
            "cn": "请至少添加一个收件人地址，或者选择 “我希望来门店自取”。"
        },
        "1235": {
            "en": "Total Amount (RMB):",
            "cn": "总价（元）："
        },
        "1236": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1237": {
            "en": "Product(Preferential):",
            "cn": "产品（优惠后）"
        },
        "1238": {
            "en": "Delivery(Preferential):",
            "cn": "配送（优惠后）"
        },
        "1239": {
            "en": "Apply promotion code",
            "cn": "输入优惠码"
        },
        "1240": {
            "en": "Have a promotional code? Enter it below (Not case-sensitive)",
            "cn": "拥有优惠码？请输入（不区分大小写）"
        },
        "1241": {
            "en": "APPLY",
            "cn": "输入"
        },
        "1242": {
            "en": "Please verify that you have typed the code correctly and that you have not used this promotion code previously.",
            "cn": "优惠码输入错误，请确认之前没有使用并重试。"
        },
        "1243": {
            "en": "Design your own cleanse program!",
            "cn": "每天一瓶绿，保证维生素摄入，提高抵抗力（可保存3天）；每天一瓶椰子水，保证补充电解质，延缓衰老（可保存3天）"
        },
        "1244": {
            "en": "About VCLEANSE",
            "cn": "关于维果清"
        },
        "1245": {
            "en": "Follow us:",
            "cn": "找到我们："
        },
        "1246": {
            "en": "Hotline:",
            "cn": "客服热线："
        },
        "1247": {
            "en": "Labor Day holiday notice!",
            "cn": "维果清五一劳动节放假通知！"
        },
        "1248": {
            "en": "Sure to delete this order?",
            "cn": "确定删除该订单吗？"
        },
        "1249": {
            "en": "Sure to delete this address?",
            "cn": "确定删除该地址吗？"
        },
        "1250": {
            "en": "No address",
            "cn": "无收件人地址"
        },
        "1251": {
            "en": "Dear V-Cleanser, you don't have a fully delivery address! <br><br>",
            "cn": "请完善您的收件人地址！<br><br>"
        },
        "1252": {
            "en": "Feedback on website?",
            "cn": "网站使用问题或建议？"
        },
        "1253": {
            "en": "Send Email to our technical support team:",
            "cn": "给我们的技术支持发送邮件："
        },
        "1254": {
            "en": "Tax invoice:",
            "cn": "发票信息："
        },
        "1255": {
            "en": "Payment method:",
            "cn": "支付方式："
        },
        "1256": {
            "en": "Invite code:",
            "cn": "邀请码："
        },
        "1257": {
            "en": "Invite code",
            "cn": "邀请码"
        },
        "1258": {
            "en": "Dear customer, our earth day activity has been fully booked. To continue order, please <a href=\"\/order\/package\">click here</a>! Or contact our customer service.",
            "cn": "尊敬的顾客，您好！我们地球日的活动已经订满了，无法继续订购。如果您还需要继续订购请<a href=\"\/order\/package\">点击这里</a>到首页完成订购！或者联系我们的客服！"
        },
        "1259": {
            "en": "Check your mobile number, please!",
            "cn": "您的手机号码输入有误，请核对！"
        },
        "1260": {
            "en": "Check your invite code, please!",
            "cn": "您的邀请码输入有误，请核对！"
        },
        "1261": {
            "en": "Check your input, please!",
            "cn": "您的输入有误，请核对！"
        },
        "1262": {
            "en": "Please remember your order number!",
            "cn": "请牢记您的订单号！"
        },
        "1263": {
            "en": "Amount Paid (RMB):",
            "cn": "已付金额（元）："
        },
        "1264": {
            "en": "You choose pay on delivery.",
            "cn": "您选择的付款方式为货到付款。"
        },
        "1265": {
            "en": "Back to Home",
            "cn": "回到首页"
        },
        "1266": {
            "en": "Sorry, earth day activity was closed on April 19. Now you can fill in the following form join our waitlist!",
            "cn": "很抱歉，活动订购已于4月19日截止。现在，您可以填写下面的表单将全绿系列加入心愿单，下一个全绿系列就是你的哦！"
        },
        "1267": {
            "en": "Join Waitlist",
            "cn": "加入心愿单"
        },
        "1268": {
            "en": "We have received your wish.",
            "cn": "我们已经收到您的心愿单。"
        },
        "1269": {
            "en": "Sorry, earth day activity was closed on April 19.",
            "cn": "很抱歉，活动订购已于4月19日截止。"
        },
        "1270": {
            "en": "Please verify that your promotion code can be applied to current order (check order subtotal).",
            "cn": "该优惠码不适用于当前订单价格。"
        },
        "1271": {
            "en": "Mobile is required",
            "cn": "请输入手机号"
        },
        "1272": {
            "en": "Password is required",
            "cn": "请输入密码"
        },
        "1273": {
            "en": "Sorry, your operating too frequently.",
            "cn": "抱歉，您的操作过于频繁。"
        },
        "1274": {
            "en": "Sorry, wrong validation code.",
            "cn": "抱歉，您输入的手机验证码错误。"
        },
        "1275": {
            "en": "First name is required",
            "cn": "请输入您的名字"
        },
        "1276": {
            "en": "Last name is required",
            "cn": "请输入您的姓氏"
        },
        "1277": {
            "en": "Email is required",
            "cn": "请输入您的电子邮件"
        },
        "1278": {
            "en": "Sorry, wrong mobile number.",
            "cn": "请输入正确的手机号码。（如果需要留下固话，请留在地址栏）"
        },
        "1279": {
            "en": "Password:",
            "cn": "密码："
        },
        "1280": {
            "en": "Successfully Ordered",
            "cn": "订购成功"
        },
        "1281": {
            "en": "Delivery Address & Start Date",
            "cn": "收件信息"
        },
        "1282": {
            "en": "Store",
            "cn": "在线商店"
        },
        "1283": {
            "en": "Cleanse",
            "cn": "清体套餐"
        },
        "1284": {
            "en": "Subscriptions",
            "cn": "清体计划"
        },
        "1285": {
            "en": "Gift Cards",
            "cn": "礼品卡"
        },
        "1286": {
            "en": "News & Events",
            "cn": "最新资讯"
        },
        "1287": {
            "en": "Healthy Office",
            "cn": "健康办公"
        },
        "1288": {
            "en": "Days",
            "cn": "天"
        },
        "1289": {
            "en": "Juices",
            "cn": "缤纷果饮"
        },
        "1290": {
            "en": "See All Of Our Products",
            "cn": "查看我们所有果汁"
        },
        "1291": {
            "en": "Spicy Lemonade",
            "cn": "每次配送加一瓶大师清体柠檬水"
        },
        "1292": {
            "en": "Fragrant Clear",
            "cn": "每次配送加一瓶芳香椰子水"
        },
        "1293": {
            "en": "All Products",
            "cn": "所有产品"
        },
        "1294": {
            "en": "Nutrition Center",
            "cn": "营养中心"
        },
        "1295": {
            "en": "About Us",
            "cn": "关于我们"
        },
        "1296": {
            "en": "Our Story",
            "cn": "关于维果清"
        },
        "1297": {
            "en": "Philosophy",
            "cn": "公司理念"
        },
        "1298": {
            "en": "Detox 101",
            "cn": "Detox 101"
        },
        "1299": {
            "en": "Detox Quiz",
            "cn": "Detox Quiz"
        },
        "1300": {
            "en": "Q&amp;A",
            "cn": "问答"
        },
        "1301": {
            "en": "Survial Guide",
            "cn": "Survial Guide"
        },
        "1302": {
            "en": "Store Locations",
            "cn": "门店地址"
        },
        "1304": {
            "en": "Join VCLEANSE",
            "cn": "加入维果清"
        },
        "1305": {
            "en": "Black",
            "cn": "黑"
        },
        "1306": {
            "en": "Green",
            "cn": "绿"
        },
        "1307": {
            "en": "White",
            "cn": "白"
        },
        "1308": {
            "en": "Red",
            "cn": "红"
        },
        "1309": {
            "en": "Yellow",
            "cn": "黄"
        },
        "1310": {
            "en": "New customer, signup!",
            "cn": "新客户，注册！"
        },
        "1311": {
            "en": "BACK TO CLEANSE",
            "cn": "回到清体套餐"
        },
        "1312": {
            "en": "CLEANSES",
            "cn": "在线选购"
        },
        "1313": {
            "en": "Already had an account? Please <a class=\"link-fn\" data-action=\"signin\">sign in</a><br><br>You can place your oder successfully as a guest, but you can track your order status and change your oders more freely only if you own a Vcleanse account.<a href=\"/enrollment/register\" data-weave=\"widget/href/main\">Register Now</a>",
            "cn": "拥有维果清账号？请<a class=\"link-fn\" data-action=\"signin\">登录</a>。<br><br>您也可以直接购买，拥有维果清账号可以帮助您更好地追踪和管理您的订单。<a href=\"/enrollment/register\" data-weave=\"widget/href/main\">立即注册</a>"
        },
        "1314": {
            "en": "GG",
            "cn": "GG"
        },
        "1315": {
            "en": "No Matching Event Id",
            "cn": "目前无活动"
        },
        "1316": {
            "en": "Event has not started",
            "cn": "活动未开始"
        },
        "1317": {
            "en": "Event has already ended",
            "cn": "活动已结束"
        },
        "1318": {
            "en": "Enter any referral codes",
            "cn": "有“团体/VIP”号码？请输入（作为订单来源凭据使用，输入即可）"
        },
        "1319": {
            "en": "(free shipping)",
            "cn": "（包邮）"
        },
        "1340": {
            "cn": "查看详情",
            "en": "Details"
        },
        "1341": {
            "en": "Sending...",
            "cn": "发送中..."
        },
        "1342": {
            "en": "SEND AGAIN",
            "cn": "再次发送"
        },
        "1343": {
            "en": "CAN'T GET VERIFICATION CODE? SKIP!",
            "cn": "无法获取短信验证码？跳过手机验证！"
        },
        "1344": {
            "en": "Input verification code",
            "cn": "输入右侧图片中验证码"
        },
        "1345": {
            "en": "Please type the verification code.",
            "cn": "请输入验证码。"
        },
        "1346": {
            "en": "Delivery Address",
            "cn": "收件人地址"
        },
        "1347": {
            "en": "Delivery Date",
            "cn": "配送时间"
        },
        "1348": {
            "en": "SAVE CHANGES",
            "cn": "保存更改"
        },
        "1349": {
            "en": "GO BACK",
            "cn": "返回"
        },
        "1350": {
            "en": "Find My Orders",
            "cn": "查询订单"
        },
        "1351": {
            "en": "Users Instrutions",
            "cn": "套餐使用建议"
        },
        "1352": {
            "en": "Cleanse aficionados can design their own custom cleanse.  Please select the flavors you would like.",
            "cn": "随心所欲，定制属于你自己的清体达人套餐。"
        },
        "1353": {
            "en": "SEARCH",
            "cn": "搜索"
        },
        "1354": {
            "en": "Search by the mobile phone number you entered with your order",
            "cn": "根据收件人手机号码查询"
        },
        "1355": {
            "en": "New Customers?",
            "cn": "没有账户？"
        },
        "1356": {
            "en": "You do not need to create an account to checkout. You can checkout as a guest and create an account later.",
            "cn": "您可以以访客的身份继续浏览或者完成订购。"
        },
        "1357": {
            "en": "Continue as Guest",
            "cn": "以访客身份继续"
        },
        "1358": {
            "en": "Payment and Invoice",
            "cn": "支付及发票"
        },
        "1359": {
            "en": "to search my orders",
            "cn": "查询我的订单。"
        },
        "1360": {
            "en": "Order create failed!",
            "cn": "订单创建失败。"
        },
        "1361": {
            "en": "Cold Pressed",
            "cn": "冷榨液压"
        },
        "1362": {
            "en": "No records.",
            "cn": "该手机号码没有订单记录。"
        },
        "1363": {
            "en": "Wechat:",
            "cn": "微信人工客服："
        },
        "1364": {
            "en": "Scan qr-code!",
            "cn": "扫描二维码，随时随地语音咨询！"
        },
        "1365": {
            "en": "Page has expired. Please refresh page and try again!",
            "cn": "页面已过期，刷新页面后请重试。"
        },
        "1366": {
            "en": "Order has been locked. You can't modify!",
            "cn": "订单已经锁定，不能修改。"
        },
        "1367": {
            "en": "Invalid promotion code.",
            "cn": "无效的优惠码。"
        },
        "1368": {
            "en": "This promotion code has expired.",
            "cn": "该优惠码已过期。"
        },
        "1369": {
            "en": "Invalid order.",
            "cn": "无效的订单号。"
        },
        "1370": {
            "en": "Thanks for using promotion code.",
            "cn": "感谢您使用优惠码。"
        },
        "1371": {
            "en": "Free delivery for this order.",
            "cn": "运费已减免。"
        },
        "1372": {
            "en": "Free \"Fragrant Clear\" everyday for this order.",
            "cn": "已经为该订单添加免费的椰子水。"
        },
        "1373": {
            "en": "Free delivery and free \"Fragrant Clear\" everyday for this order.",
            "cn": "您的订单已经免运费，送椰子水。"
        },
        "1374": {
            "en": "Learn about <a href=\"/juice/index\">our juices</a> here.",
            "cn": "快了解一下我们的<a href=\"/juice/index\">缤纷果汁</a>吧。"
        },
        "1375": {
            "en": "To protect your privacy, we need to verify the phone number.",
            "cn": "为了保护您的隐私，我们需要验证手机号码。"
        },
        "1376": {
            "en": "Your payment failed.",
            "cn": "付款失败。"
        },
        "1377": {
            "en": "Sorry!",
            "cn": "抱歉！"
        },
        "1378": {
            "en": "Pay failed",
            "cn": "付款失败"
        },
        "1379": {
            "en": "Select a shipping address",
            "cn": "物流配送"
        },
        "1380": {
            "en": "Shanghai",
            "cn": "上海"
        },
        "1381": {
            "en": "View map",
            "cn": "查看地图"
        },
        "1382": {
            "en": "Business hours:",
            "cn": "营业时间："
        },
        "1383": {
            "en": "Leave your contact information",
            "cn": "留下您的联系方式"
        },
        "1384": {
            "en": "Free \"Spicy Lemonade\" everyday for this order.",
            "cn": "已经为该订单添加免费的柠檬水。"
        },
        "1385": {
            "en": "Free delivery and free \"Spicy Lemonade\" everyday for this order.",
            "cn": "您的订单已经免运费，送柠檬水。"
        },
        "1386": {
            "en": "Dear V-Cleanser, you don't have a fully contact information! <br><br>",
            "cn": "请完善您的联系方式！<br><br>"
        },
        "1387": {
            "en": "Contact tel:",
            "cn": "联系电话："
        },
        "1388": {
            "en": "This code has used",
            "cn": "优惠码已经使用"
        },
        "1389": {
            "en": "Has use disposable code",
            "cn": "已经使用过一次性优惠码了"
        },
        "1390": {
            "en": "Anonymous cannot uset his rule",
            "cn": "匿名用户不能用,请登录后重新下单"
        },
        "1391": {
            "en": "Sorry You have used this type of promotion",
            "cn": "对不起, 你的账户已经使用过这种类型的优惠码"
        },
        "1392": {
            "en": "Sure to use this code",
            "cn": "是否使用该优惠码？"
        },
        "1393": {
            "en": "Sorry, this activity was ended. Please choose our regular packages!",
            "cn": "很抱歉，活动订购已截止。您可以选择我们的常规套餐！"
        },
        "1394": {
            "en": "Package Detail",
            "cn": "套餐详情"
        },
        "1395": {
            "en": "Receiver name:",
            "cn": "收件人姓名："
        },
        "1396": {
            "en": "Receiver's name is required",
            "cn": "收件人真实姓名，必填"
        },
        "1397": {
            "en": "Name:",
            "cn": "联系人姓名："
        },
        "1398": {
            "en": "Pickup contactor's name is required",
            "cn": "自取联系人真实姓名，必填"
        },
        "1399": {
            "en": "From",
            "cn": "从"
        },
        "1400": {
            "en": "to",
            "cn": "至"
        },
        "1401": {
            "en": "password:",
            "cn": "密码："
        },
        "1402": {
            "en": "Your first name here",
            "cn": "您的名字"
        },
        "1403": {
            "en": "Subscribe",
            "cn": "订阅"
        },
        "1404": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，新鲜资讯抢先看"
        },
        "1405": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "订阅成功！"
        },
        "1406": {
            "en": "Duplicated email",
            "cn": "该邮箱已被订阅"
        },
        "1407": {
            "en": "Your email address has an invalid email address format. Please correct and try again.",
            "cn": "电子邮件格式有误。请尝试重新输入！"
        },
        "1408": {
            "en": "Your mobile number has an invalid format. Please correct and try again.",
            "cn": "手机格式有误。请尝试重新输入！"
        },
        "1409": {
            "en": "Order confirmation has been sent to your mobile via SMS, please check!",
            "cn": "订单确认信息已经通过短信发送到您的注册手机号，请注意查收！"
        },
        "1410": {
            "en": "Leave your email to get order information",
            "cn": "留下邮箱，获取订单信息，新鲜资讯抢先看！"
        },
        "1411": {
            "en": "Subscribe to VCLEANSE",
            "cn": "留下邮箱，维果清新鲜资讯抢先看"
        },
        "1412": {
            "en": "Success subscribe to VCLEANSE!",
            "cn": "您已经成功订阅维果清资讯！"
        },
        "1451": {
            "en": "Check all online payment methods",
            "cn": "查看全部在线支付方式"
        },
        "1452": {
            "en": "I understand",
            "cn": "我知道了"
        },
        "1454": {
            "en": "Payment completed",
            "cn": "已经完成支付"
        },
        "1455": {
            "en": "Have a problem",
            "cn": "支付遇到问题"
        },
        "1456": {
            "en": "Comfirm",
            "cn": "付款确认"
        },
        "1457": {
            "en": "Close",
            "cn": "关闭"
        },
        "1458": {
            "en": "Pay now",
            "cn": "去付款"
        },
        "1459": {
            "en": "V Station",
            "cn": "维果清门店"
        },
        "1460": {
            "en": "Quantity",
            "cn": "选择购买数量"
        },
        "1461": {
            "en": "Account Safety",
            "cn": "账户安全"
        },
        "1462": {
            "en": "Failed",
            "cn": "更新失败"
        },
        "1463": {
            "en": "Change",
            "cn": "换一个"
        },
        "1464": {
            "en": "Code here",
            "cn": "输入验证码"
        },
        "1465": {
            "en": "Successfully change password!",
            "cn": "成功更新密码！"
        },
        "1466": {
            "en": "Please hoover your mouse to the image below, and click on the button on the top right to share this on WeChat and Weibo!",
            "cn": "将鼠标移到下图上，点击右上角的图标，把这份温暖分享到微信、微博吧！"
        },
        "1467": {
            "en": "* Delivery fee of RMB 25 is not included.",
            "cn": "* 每日25元运费另计。"
        },
        "1468": {
            "en": "Only for warming day",
            "cn": "优惠码只能对暖体日使用."
        },
        "1469": {
            "en": "This area is not available",
            "cn": "该地区暂时不能配送"
        },
        "1470": {
            "en": "Edit address failed!",
            "cn": "地址修改失败!"
        },
        "1471": {
            "en": "The new address will generate additional freight!",
            "cn": "要修改的地址将产生额外运费!"
        },
        "1472": {
            "en": "Order Review",
            "cn": "查看订单"
        },
        "1473": {
            "en": "Mine",
            "cn": "我的"
        },
        "1474": {
            "en": "Others",
            "cn": "其它"
        },
        "1475": {
            "en": "Delivery date:",
            "cn": "配送日期："
        },
        "1476": {
            "en": "Delivery address:",
            "cn": "配送地址："
        },
        "1477": {
            "en": "Detailed list:",
            "cn": "清单："
        },
        "1478": {
            "en": "Status:",
            "cn": "状态："
        },
        "1479": {
            "en": "The child orders",
            "cn": "子订单"
        },
        "1480": {
            "en": "Day",
            "cn": "天"
        },
        "1481": {
            "en": "Invalid payment request!",
            "cn": "无效的付款请求。"
        },
        "1482": {
            "en": "Purple Broth",
            "cn": "每次配送加一瓶紫甘蓝靓汤"
        },
        "1483": {
            "en": "Workshops",
            "cn": "维果清工作坊"
        },
        "1484": {
            "en": "Individual",
            "cn": "个人报名入口"
        },
        "1485": {
            "en": "Group or Corporate",
            "cn": "团体报名入口"
        },
        "1600": {
            "en": "SUBSCRIPTIONS",
            "cn": "预订"
        },
        "1601": {
            "en": "SELECT",
            "cn": "选择"
        },
        "1602": {
            "en": "Free \"Coconut Water\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶椰子水。"
        },
        "1603": {
            "en": "Free \"Spicy Lemonade\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶柠檬水。"
        },
        "1604": {
            "en": "Free \"Moon Bear's Honey Tree\" firstday for this order.",
            "cn": "本次订单第一天，我们将免费赠送您一瓶月熊蜂蜜树。"
        },
        "1605": {
            "en": "Not available on the day",
            "cn": "当天不供应"
        },
        "1606": {
            "en": "This is a gift",
            "cn": "这是礼品"
        },
        "1607": {
            "en": "Gift Blessing",
            "cn": "礼品赠言"
        },
        "1608": {
            "en": "You have to order:",
            "cn": "您已订购："
        },
        "1609": {
            "en": "Products Info",
            "cn": "产品详情"
        },
        "1610": {
            "en": "Submit",
            "cn": "提交"
        },
        "1611": {
            "en": "Thanks for your support!",
            "cn": "谢谢您的参与！"
        },
        "1612": {
            "en": "Please enter...",
            "cn": "请输入..."
        },
        "1613": {
            "en": "(Multiple Choice)",
            "cn": "（多选题）"
        },
        "1614": {
            "en": "Image verification code:",
            "cn": "图片验证码："
        },
        "1615": {
            "en": "VERIFY AND SEARCH",
            "cn": "验证并查询"
        },
        "1616": {
            "en": "to:",
            "cn": "到："
        },
        "1617": {
            "en": "Security code:",
            "cn": "手机验证码："
        },
        "1618": {
            "en": "Not paid yet",
            "cn": "未支付"
        },
        "1619": {
            "en": "Paid",
            "cn": "已支付"
        },
        "1620": {
            "en": "Action",
            "cn": "操作"
        },
        "1621": {
            "en": "Details",
            "cn": "查看"
        },
        "1622": {
            "en": "Hotline[9AM - 7PM]:",
            "cn": "客服热线[上午9点 - 晚上7点]："
        },
        "1623": {
            "en": "Invalid shipping address",
            "cn": "无效的收件地址"
        },
        "1624": {
            "en": "This order cannot be deleted, please contact customer service.",
            "cn": "该订单无法删除，请联系客服。"
        },
        "1625": {
            "en": "You have ordered before, you can not use the coupon code.",
            "cn": "您已经有过订单，无法使用该优惠码"
        }

    });
    /* String help function */
    // Format: "Hello, {0}. {1}".format("Mr","DJ")

    function stringFormat(s, args) {
        var i = 0;
        var iLens = args.length;
        for (i = 0; i < iLens; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, args[i]);
        }
        return s;
    }

    return Widget.extend(function($element, widgetName, blurbId, attr) {
        var me = this;

        me.blurbId = blurbId;
        me.attr = attr || "";
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var format = $me.data("format");

            var translation = me.get(me.blurbId, format);

            if (!translation) {
                return;
            }

            // Attr or text
            if (me.attr) {
                $me.attr(me.attr, translation);
            } else {
                me.html(translation);
            }

            // Log blurb ID
            //$me.attr("blurb", me.blurbId);
        },
        "get": function(blurbId, format) {
            var me = this;

            var language = context.language || "en";
            var translationSet;
            var translation = "";

            if (!blurbId) {
                return "";
            }

            translationSet = blurbDict[blurbId.toString()];

            if (!translationSet) {
                return "";
            }

            if (context.isDebug) {
                translation += "[" + blurbId + "]";
            }
            translation += translationSet[language];

            // format: "x, y, z" --> ['x', 'y', 'z']
            // Replace {0} {1}
            if (translation.indexOf("{0}") >= 0 && format !== undefined) {
                format = format.toString().split(",");
                translation = stringFormat(translation, format);
            }

            return translation;
        }
    });
});
define('widget/pageurl/main',["jquery",
    "widget/sitemap/main"
], function($, sitemap) {
    "use strict";

    return {
        "goto": function(key, hash, query) {
            var href;

            href = sitemap.get(key);

            if (query) {
                href += "?" + $.param(query);
            }
            if (hash) {
                href += "#" + hash;
            }
            location.href = href;
        },
        "winopen": function(key, hash, query) {
            var href;

            href = sitemap.get(key);

            if (query) {
                href += "?" + $.param(query);
            }
            if (hash) {
                href += "#" + hash;
            }
            window.open(href);
        }
    };
});

define('troopjs-requirejs/template!widget/infobox/index.html',[],function() { return function template(data) { var o = "<div class=\"linebox linebox-xlarge infobox infobox-" +data.type+ "\">\r\n    "; if(data.title && !isNaN(data.title)) { o += "\r\n        <h3 data-weave=\"widget/blurb/main(" +(data.title)+ ")\"></h3>\r\n    "; } else { o += "\r\n        <h3>" +(data.title)+ "</h3>\r\n    "; } o += "\r\n    "; if(data.content) { o += "\r\n        <p data-weave=\"widget/blurb/main(" +(data.content)+ ")\">" +(data.content)+ "</p>\r\n    "; } else { o += "\r\n        <p>" +(data.content)+ "</p>\r\n    "; } o += "\r\n    "; if(data.closeble) { o += "\r\n        <button type=\"button\" class=\"btn btn-default\" data-action=\"close\" data-weave=\"widget/blurb/main(1129)\">OK</button>\r\n    "; } o += "\r\n</div>"; return o; }; });
define('widget/infobox/main',["jquery",
    "widget/popupbox/main",
    "template!./index.html"
], function($, PopupBox, template) {
    "use strict";

    var TYPE_SUCCESS = "success";
    var TYPE_INFO = "info";
    var TYPE_WARNING = "warning";
    var TYPE_DANGER = "danger";

    return PopupBox.extend(function(args) {
        var me = this;

        me.el = args.$el || "body";

        me.position = args.position || "fixed";
        me.zIndex = args.zIndex || 20;

        me.closeble = (typeof args.closeble === "undefined") ? true : args.closeble;
        me.closeInner = true;
        me.closeButtonList = ['[data-action=close]'];
        me.overLap = "overlap";
        var closeCallback = args.closeCallback || $.noop;
        // TODO: 
        me.closeCallback = function() {
            // Callback from arguments
            closeCallback();
            // Destory self
            me.stop();
        };

        var info = {
            type: (args.type || TYPE_INFO).toLowerCase(),
            title: args.title || "",
            content: args.content || "",
            closeble: me.closeble
        };
        me.msg = template(info);
        
    });
});
define('widget/time/main',["troopjs-core/component/gadget",
  "jquery",
  "context"
], function(Gadget, $, context) {
  "use strict";

  var WEEK_START_DAY = 1; // Monday

  function geti18n() {
    var lngMap = {
      "en": {
        "week": [
          ['Sunday', 'Sun'],
          ['Monday', 'Mon'],
          ['Tuesday', 'Tue'],
          ['Wednesday', 'Wed'],
          ['Thursday', 'Thu'],
          ['Friday', 'Fri'],
          ['Saturday', 'Sat']
        ]
      },
      "cn": {
        "week": [
          ['星期日', '日'],
          ['星期一', '一'],
          ['星期二', '二'],
          ['星期三', '三'],
          ['星期四', '四'],
          ['星期五', '五'],
          ['星期六', '六']
        ]
      }
    };
    var lng = context.language || "en";
    return lngMap[lng];
  }

  return Gadget.extend({
    "getZeroDate": function(date) {
      // all date should formatted in: 00:00:00
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    },
    "getShortDate": function(date) {
      return (date.getMonth() + 1) + "/" + date.getDate();
    },
    "getWeekStartDate": function(date) {
      var me = this;
      var inst = date.getDay() - WEEK_START_DAY;
      inst = inst >= 0 ? inst : (inst + 7);
      return me.addDay(date, inst * -1);
    },
    "getDayOfWeekFromMonday": function(date) {
      // 0 = Monday; 6 = Sunday;
      return date.getDay() === 0 ? 6 : date.getDay() - 1;
    },
    "getStrDate": function(date, sep) {
      // 2013/10/10
      if (!date) {
        return "";
      }
      // Reinit Date Object
      date = new Date(date);
      sep = sep || "/";
      // all date should formatted in: 2013/10/10
      return date.getFullYear().toString() +
        sep +
        (date.getMonth() + 1).toString() +
        sep +
        date.getDate().toString();
    },
    "getWeekName": function(day, isAbbr) {
      day = (!isNaN(day) && (day < 7)) ? day : 0;
      var i = isAbbr ? 1 : 0;
      var arrWeekName = geti18n().week;
      return arrWeekName[day][i];
    },
    "addDay": function(date, inst) {
      var temp = new Date(date);
      return new Date(temp.setDate(temp.getDate() + inst));
    },
    "addMonth": function(date, inst) {
      var temp = new Date(date);
      return new Date(temp.setMonth(temp.getMonth() + inst));
    },
    "getDateByString": function(strDate) {
      // "2013-09-10" --> 1378771200000 ms
      var d = new Date(strDate);
      d.setHours(0);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);

      return d;
    },
    "compareDate": function(baseDate, toCompareDate) {
      // Compare two dates and returns:
      //  -1 : if toCompareDate < baseDate
      //   0 : if toCompareDate === baseDate
      //   1 : if toCompareDate > baseDate
      // NaN : if baseDate or toCompareDate is an illegal date
      // NOTE: The code inside isFinite does an assignment (=).
      return (
        isFinite(baseDate = baseDate.valueOf()) &&
        isFinite(toCompareDate = toCompareDate.valueOf()) ?
        (toCompareDate > baseDate) - (toCompareDate < baseDate) :
        NaN);
    },
    "between": function(baseDate, fromDate, toDate) {
      var me = this;

      if (me.compareDate(baseDate, fromDate) <= 0 && me.compareDate(baseDate, toDate) >= 0) {
        return true;
      } else {
        return false;
      }
    },
    "dayDiff": function(baseDate, toCompareDate) {
      // Get diff days between two Date
      return parseInt((toCompareDate - baseDate) / (1000 * 60 * 60 * 24), 10);
    },
    "weekDiff": function(baseDate, toCompareDate) {
      // Get diff weeks between two Date
      return parseInt((toCompareDate - baseDate) / (1000 * 60 * 60 * 24 * 7), 10);
    },
    "getNearestWeekdayDate": function(date, wdays) {
      var me = this;
      if (!date) {
        return date;
      }

      var day = date.getDay();
      // Has weekdays, select recent date(the nearest date after startdate) match weekdays
      if (wdays && wdays.length > 0) {
        do {
          date = me.addDay(date, 1);
          day = date.getDay();
        } while (wdays.indexOf(day) < 0);
      }

      return date;
    }
  });
});
/*订单提交第一步主逻辑
 * from widget/delivery/order
 **/
define('widget/buy/delivery',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    //"template!./delivery_order.html",
    "json2"
], function(Widget, $, _, when, context, cloneObject, cookie, ajaxQuery, api, pageUrl, InfoBox, Time) {//, template
    "use strict";

    var URI_RECEIVER = api.get("receiver");
    var URI_ORDER_CREATE = api.get("order_create");
    var URI_DELIVER_TIME = api.get("deliver_time");
    var URI_ACCOUNT = api.get("account");

    // Time function
    var INS_TIME = new Time();

    function getStatus(code) {
        var status = [];
        status["1000"] = "1360"; //参数错误
        status["-100"] = "1360"; //没有产品
        status["-1"] = "1360"; //创建失败
        status["1001"] = "1360"; //未知错误
        status["-200"] = "1365"; //页面过期
        status["-400"] = "1469"; //地区不配送

        return status[code] || "";
    }

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Append Template
            //me.html(template);
        },
        // Default receiver
        "getDefaultReceiver": function() {
            var me = this;

            var deferred = when.defer();

            if (me.defaultReceiver) {
                deferred.resolve(me.defaultReceiver);
            } else {
                ajaxQuery({
                    url: URI_RECEIVER,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    var receivers = data.Receivers || [];
                    var i = 0;
                    var iLens = receivers.length;
                    // No deliver address
                    if (iLens <= 0) {
                        deferred.resolve();
                        return;
                    }
                    for (i = 0; i < iLens; i++) {
                        if (receivers[i].IsDefault) {
                            // Save to widget
                            me.defaultReceiver = receivers[i];
                            deferred.resolve(me.defaultReceiver);
                            return;
                        }
                    }
                });
            }
            return deferred.promise;
        },
        "initDeliveryList": function(myPackage) {
            var me = this;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }
            if (!myPackage) {
                return;
            }

            // With a package selected, render package to calendar.
            me.getDefaultReceiver()
                .then(function(defaultReceiver) {

                    // Memory Order
                    var deliveryList = me.deliveryList;
                    var k = 0;
                    var kLens = myPackage.length;

                    // Own package
                    if (kLens <= 0) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1146
                        }).open();
                    } else {
                        // Copy package info & default order info
                        for (k = 0; k < kLens; k++) {
                            if (!deliveryList[k]) {
                                deliveryList[k] = {};
                            }
                            // Init Products
                            deliveryList[k].Products = [];
                            // Package info
                            // Day: day start from 0
                            deliveryList[k].Date = INS_TIME.addDay(me.deliveryStartDate, myPackage[k].Day);
                            deliveryList[k].PackageRuleId = myPackage[k].Id;
                            // Customize (subscription) === 0
                            if (!myPackage[k].Id) {
                                deliveryList[k].Products = myPackage[k].Products;
                            }
                            //deliveryList[k].PackageProducts = myPackage[k].Products;
                            // Default 1 package per(one) time
                            deliveryList[k].PackageRuleCount = 1;

                            // Attach default address to all date
                            if (defaultReceiver && !deliveryList[k].ReceiverId) {
                                // Receiver info
                                deliveryList[k].ReceiverId = defaultReceiver.Id;
                                deliveryList[k].Receiver = defaultReceiver;
                            }
                        }
                    }
                    
                    
                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Init Delivery List");
                });
        },
        // Generate order
        "generateOrder": function(deltaDay) {
            var me = this;
            deltaDay = deltaDay || 0;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }

            function setDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Set new date
                    me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                }
            }

            function resetDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                if (isNaN(deltaDay) || deltaDay === 0) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Reset new date by add delta days
                    if (me.deliveryList[i].Date) {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryList[i].Date, deltaDay);
                    } else {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                    }
                }
            }

            // With orders
            if (me.deliveryList && me.deliveryList.length > 0) {
                // Reset date in deliver list
                if (deltaDay) {
                    resetDeliveryDate();
                } else if (!me.deliveryList[0].Date) {
                    setDeliveryDate();
                }

                // Publish to order confirm, list price list
                me.publishDeliveryList("Generate Order with Delivery List");
            } else {
                // Not initial run
                // Check whether it's a package
                if (me.myPackage) {
                    me.initDeliveryList(me.myPackage);
                } else {
                    // Without orders & package, should be customize
                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Generate Order Without Delivery List");
                }
            }
        },
        "publishDeliveryList": function(sign) {
            var me = this;
            
            // 当页面数据都完成的时候触发summary变化
            me.publish("order/updateSummary");
            
            // Tracking code for debug
            console.log("//-----=====-----// " + sign + " //-----=====-----//");
            console.log(me.deliveryList);
        },
        // Calendar events
        "hub/order/package": function(myPackage) {
            var me = this;

            if (!myPackage) {
                // Customize order
                cookie.get("customize").then(function(customizePackage) {
                    // Memory Order
                    me.deliveryList = JSON.parse(customizePackage);
                }, function() {});
                return;
            }

            // Save
            me.myPackage = cloneObject(myPackage);
            // Mixin calendar/order, rendering
            // Args "myPackage" for FORCE re-render package
            me.initDeliveryList(me.myPackage);
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            if (!args || !args.selectedDate) {
                return;
            }

            // For packageRule caculate
            me.publish("deliveryStartDate/change/next", args);

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            // Gap days between new date & last date
            var deltaDay = 0;
            if (me.deliveryStartDate) {
                deltaDay = INS_TIME.dayDiff(me.deliveryStartDate, deliveryStartDate);
            }
            // Save delivery start date
            me.deliveryStartDate = deliveryStartDate;

            // Mixin calendar/order, rendering
            me.generateOrder(deltaDay);
        },
        "hub/receiver/changeDeliverTime": function(deliverTime) {
            var me = this;

            if (!deliverTime) {
                return;
            }

            // Memory
            me.deliverTime = deliverTime;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].DeliverTimeId = deliverTime.Id;
                me.deliveryList[i].DeliverTimeName = deliverTime.Name;
            }
            // Publish
            me.publishDeliveryList("Deliver Time Change");
        },
        "hub/receiver/changeDeliverAddress": function(receiver) {
            var me = this;

            if (!receiver) {
                return;
            }
            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].Receiver = receiver;
                me.deliveryList[i].ReceiverId = receiver.Id;
            }
            // 
            // Publish
            me.publishDeliveryList("Deliver Address Change");
        },
        "hub/order/addon": function(arrayAddon) {
            var me = this;

            var j = 0;
            var jLens = arrayAddon.length;
            var addonId = 0;

            me.addon = [];

            for (j = 0; j < jLens; j++) {
                addonId = parseInt(arrayAddon[j], 10);
                if (addonId !== 0) {
                    me.addon.push({
                        "Id": addonId,
                        "Count": 1
                    });
                }
            }
        },
        "hub/order/source": function(eventCode) {
            var me = this;

            // Save source code to Memory
            me.eventCode = eventCode || "";
        },
        "getReceiver": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/receiver/nonlogin/submit").spread(function(dataReceiver) {
                return when.resolve(dataReceiver);
            });
        },
        "getSelfpickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/selfpickup").spread(function(dataSelfpickupInfo) {
                return when.resolve(dataSelfpickupInfo);
            });
        },
        "orderColloection": function() {
            var me = this;

            function updateDeliveryList(i) {
                if (me.deliverTime) {
                    me.deliveryList[i].DeliverTimeId = me.deliverTime.Id;
                    me.deliveryList[i].DeliverTimeName = me.deliverTime.Name;
                }

                // strDate used for log, we have a bug on date loss
                me.deliveryList[i].strDate = me.deliveryList[i].Date;
                me.deliveryList[i].Date = INS_TIME.getStrDate(me.deliveryList[i].Date);

                if (me.addon && me.addon.length > 0) {
                    me.deliveryList[i].Products = me.deliveryList[i].Products.concat(me.addon);
                }
            }

            function nonloginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;

                return me.getReceiver().then(function(dataReceiver) {

                    if (!dataReceiver || !dataReceiver.firstName || !dataReceiver.line1 || !dataReceiver.mobile) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    for (i = 0; i < iLens; i++) {
                        // General update delivery list
                        updateDeliveryList(i);

                        me.deliveryList[i].IsSelfPickup = false;
                        me.deliveryList[i].SelfPickupInfo = {};
                        me.deliveryList[i].Contact = dataReceiver;
                        me.deliveryList[i].ReceiverId = 0;
                    }

                    return when.resolve();
                });
            }

            function loginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;
                var withDeliveryAddress = true;

                return me.getSelfpickup().then(function(selfpickup) {

                    var isSelfPickup = selfpickup.isSelfPickup;
                    var selfpickupInfo = selfpickup.selfpickupInfo;

                    if (isSelfPickup) {
                        if (!selfpickupInfo.firstName || !selfpickupInfo.mobile) {
                            // Give a warning: Without receiver
                            // Show error msg as a lightbox
                            new InfoBox({
                                title: 1144,
                                content: 1386
                            }).open();

                            return when.reject();
                        }
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = true;
                            me.deliveryList[i].SelfPickupInfo = selfpickupInfo;
                            me.deliveryList[i].Contact = {};
                            me.deliveryList[i].ReceiverId = 0;
                        }
                    } else {
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = false;
                            me.deliveryList[i].SelfPickupInfo = {};
                            me.deliveryList[i].Contact = {};

                            // Has Delivery Address?
                            if (!me.deliveryList[i].ReceiverId) {
                                withDeliveryAddress = false;
                            }
                        }
                    }

                    if (!withDeliveryAddress) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    return when.resolve();
                });
            }

            // Signin?
            return cookie.get("memberid").then(function() {
                // Get User Account
                return ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(loginSubmit, nonloginSubmit);
            }, nonloginSubmit);
        },
        
        "hub/order/updateSummary":function(){
        	var me = this;
        	var summaryData = {
        		PromotionCode:"",
        		DeliveryList: []
        	};
        	
        	//拿promotion数据
        	me.publish("order/getPromotionCode").spread(function(promotionCode){
            	//拿develiver数据
        		summaryData.PromotionCode = promotionCode;
        		
        		me.getSelfpickup().then(function(selfpickup) {
        			var IsSelfPickup = selfpickup.isSelfPickup;

        			//对原来的Delivery做一个重新的format
                	for(var i = 0;i<me.deliveryList.length;i++){
                		var delivery = {};
                		delivery.PackageRuleId = me.deliveryList[i].PackageRuleId;
                		delivery.PackageRuleCount = me.deliveryList[i].PackageRuleCount;
                		delivery.AreaId = me.deliveryList[i].Receiver.AreaId;
                		delivery.IsSelfPickup = IsSelfPickup;
                		delivery.ProductList = me.deliveryList[i].Products.concat(me.addon);
                		
                		
                		summaryData.DeliveryList.push(delivery);
                	}
        			me.publish("order/summary",summaryData);
        		});
        		//拿product数据

            	//还给summary
        		
        		
        		
        		  
        	});

        	
        	          	
        },
        // Submit order
        "hub/order/submit": function() {
            var me = this;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1147
                }).open();
                // Do not submit order
                return;
            }

            me.orderColloection().then(function orderSubmit() {
                // /order/confirm#xxxx
                var hashSourceCode = "";
                var days;

                /*----------------HARDCODE----------------*/
                // Source code logic
                if (me.eventCode) {
                    switch (me.eventCode.toUpperCase()) {
                        case "GIFT":
                            // Special logic for GiftCard
                            days = me.deliveryList.length || 1;
                            me.deliveryList.splice(1);
                            me.deliveryList[0].PackageRuleCount = days;
                            break;
                        default:
                            hashSourceCode = "/event/" + me.eventCode.toUpperCase();
                            break;
                    }
                }
                /*----------------HARDCODE----------------*/

                // Generate data to submit
                var orderData = {
                    Order_id: 0,
                    SourceCode: me.eventCode,
                    DeliverList: me.deliveryList
                };

                // Ajax submit
                ajaxQuery({
                    url: URI_ORDER_CREATE,
                    data: JSON.stringify(orderData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                	// TODO:这里不要跳到第二步，直接进行第二个页面的提交
                	 me.publish("order/confirmorder",data.Order_id);
                    //pageUrl.goto("/order/confirm", "orderid/" + data.Order_id + hashSourceCode);
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            });
        }
    });
});
define('widget/troophash/main',["jquery"], function($) {
    "use strict";

    return {
        "parse": function(hashPath) {
            if (!hashPath || hashPath.length <= 0) {
                return false;
            }

            var hashMap = {};

            var i = 0;
            var iLens = hashPath.length;

            for (i = 0; i < iLens; i += 2) {
                hashMap[hashPath[i]] = hashPath[i + 1];
            }

            return hashMap;
        },
        "extend": function(hashExt) {
            var me = this;

            if (!hashExt) {
                return;
            }
            var hash = location.hash || "";
            // #package/1/packagegroup/1
            var hashMap = hash ? me.parse(hash.substr(1).split("/")) : {};

            // Extend
            _.extend(hashMap, hashExt);

            var hashKey;
            var hashVal;
            var hashStr = "";
            for (hashKey in hashMap) {
                hashVal = hashMap[hashKey];
                if (hashVal || !isNaN(hashVal)) {
                    hashStr += "/" + hashKey + "/" + hashMap[hashKey];
                }
            }

            // Refresh hash
            location.hash = hashStr.substr(1);
        }
    };
});

define('troopjs-requirejs/template!widget/buy/deliverydate.html',[],function() { return function template(data) { var o = "";
	var deliveryStartDate = data.deliveryStartDate;
	var packageId = data.packageId;
o += "\r\n<!--h4 data-weave=\"widget/blurb/main(1347)\">Delivery Date</h4-->\r\n<div class=\"mode-tab-con \" id=\"date_shipment\" >  \r\n\t<ul class=\"mode-list\">\r\n\t\t<li data-action=\"datePickerContainer\">\r\n\t\t\t<span class=\"ftx-03\" data-weave=\"widget/blurb/main(1008)\">套餐开始日期：</span>&nbsp;&nbsp;\r\n\t\t\t<span id='promise311tip'></span>\r\n\t\t\t<div data-weave=\"widget/buy/startdate\"><input class=\"input\" type=\"text\" id=\"startDate\" readonly=\"\" value='2016-1-12'></div>\r\n\t\t</li>\r\n\t</ul>\r\n</div>\r\n<!--\r\n<br>\r\n<div class=\"the-form\">\r\n\t<div class=\"form-section no-border\">\r\n\t\t"; if (!deliveryStartDate) { o += "\r\n\t\t    <div class=\"form-group\" data-action=\"datePickerContainer\">\r\n\t\t\t\t<label class=\"control-label\" data-weave=\"widget/blurb/main(1008)\">\r\n\t\t\t\t\tSelect start date\r\n\t\t\t\t</label>\r\n\t\t\t\t<p data-weave=\"widget/blurb/main(1024)\">When would you like to start your cleanse?</p>\r\n\t\t\t\t<div class=\"calendar-date\" data-weave=\"widget/buy/startdate\"></div>\r\n\t\t\t\t"; if (packageId === 31) { o += "\r\n\t\t\t\t\t<p style=\"color:red\">【夏日套餐】为限量套餐，仅供应至8月31日，若是超出此日期可换购“冷压经典套餐”，有任何问题可拨打4001-842-779进行咨询。</p>\r\n\t\t\t\t"; } o += "\r\n\t\t    </div>\r\n\t\t"; } o += "\r\n\t    <div class=\"form-group\" data-action=\"deliveryTimeContainer\">\r\n\t\t\t<label class=\"control-label\" data-weave=\"widget/blurb/main(1086)\">Preferred delivery window?</label>\r\n\t\t\t<p data-weave=\"widget/blurb/main(1200)\"></p>\r\n\t\t\t<div class=\"orderedit-delivery-content orderedit-delivery-time\" data-weave=\"widget/buy/deliverytime\"></div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n-->"; return o; }; });
define('widget/buy/deliverydate',["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/time/main",
    "template!./deliverydate.html",
    "json2",
    "underscore"
], function(Widget, when, $, context, cookie, troopHash, Time, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 12;

    //  Holiday configuration
    // Note: Month start from 0
    // Downtown
    var HOLIDAY_MAIN = [
		new Date(2016, 2, 1),
        new Date(2016, 2, 2),
        new Date(2016, 2, 3),
        new Date(2016, 2, 4),
       
    ];
    // Outtown
    var HOLIDAY_OUT_MAIN = [
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
        new Date(2016, 3, 3),
        new Date(2016, 3, 4),
    ];

    var VISIBLE_DATEPICKER = true;
    var VISIBLE_DELIVERYTIME = true;

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.deferred = when.defer();
    }, {
        "sig/start": function() {
            var me = this;
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            // Only Rendering for package, not for customize
            me.html(template, {
                "deliveryStartDate": deliveryStartDate
            }).then(function() {
                if (deliveryStartDate) {
                    // Publish pre-setted date
                    me.publish("deliveryStartDate/change", {
                        "selectedDate": deliveryStartDate
                    });
                }
            });
            */

            var deliveryStartDate = hashMap.deliveryStartDate ? new Date(hashMap.deliveryStartDate) : null;

            var eventCode = hashMap.event || "";
            var packageId = parseInt(hashMap.packageid, 10);
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Hide datepicker while deliveryStartDate exists
            VISIBLE_DATEPICKER = deliveryStartDate ? false : true;

            if (deliveryStartDate) {
                if (deliveryStartDate === me.deliveryStartDate) {
                    return;
                } else {
                    me.deliveryStartDate = deliveryStartDate;
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        // Preset Start Date
                        me.publish("deliveryStartDate/change", {
                            "selectedDate": deliveryStartDate
                        });

                        return when.resolve();
                    });
                }
            } else {
                if (me.rendered) {
                    // Reset startdate
                    me.publish("startdate/set", {
                        "eventCode": eventCode,
                        "days": days,
                        "weekDays": wdays
                    });
                } else {
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        return when.resolve({
                            "eventCode": eventCode,
                            "days": days,
                            "weekDays": wdays
                        });
                    });
                }
            }
        },
        "visibleControl": function(binVisible) {
            var me = this;
            var $me = me.$element;

            var $deliveryTime = $me.find("[data-action=deliveryTimeContainer]");

            if (VISIBLE_DELIVERYTIME) {
                $me.show();
                $deliveryTime.show();
            } else if (VISIBLE_DATEPICKER) {
                $me.show();
                $deliveryTime.hide();
            } else {
                $me.hide();
                $deliveryTime.hide();
            }
        },
        "hub/expressCompany/reload": function(expressCompanyId) {
            var me = this;

            // 以下两个外送公司需要提前到早晨5点截单
            var hourAhead = HOUR_AHEAD;

            // 上海顺丰标准B - 8
            // 上海顺丰空运 - 9
            // 10  顺丰次晨
            // 12  顺丰次晨B
            if (_.indexOf([8, 9, 10, 12], expressCompanyId) >= 0) {
                hourAhead = 10;
            }

            // 上海、北京初五开始配送，其它地区3月1号开始配送
            var holiday = HOLIDAY_OUT_MAIN;

            // 黑猫 - 1
            // 上海顺丰即时达 - 3
            // 北京顺丰冷链 - 7
            if (_.indexOf([1, 3, 7], expressCompanyId) >= 0) {
                holiday = HOLIDAY_MAIN;
            }

            // Reset "Start Date" by Express Company
            /*troopHash.extend({
                "hour": hourAhead
            });*/
            // Reset startdate
            me.deferred.then(function(startDateConfig) {
                startDateConfig = _.extend(startDateConfig, {
                    "hourAhead": hourAhead,
                    "holiday": holiday
                });
                me.publish("startdate/set", startDateConfig);
            });
        },
        // Show/Hide
        "hub/order/showDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = true;
            me.visibleControl();
        },
        "hub/order/hideDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = false;
            me.visibleControl();
        }
    });
});

define('troopjs-requirejs/template!widget/buy/deliverytime.html',[],function() { return function template(data) { var o = "";
    var j = 0;
    var deliverTime = data.deliverTime;
    var jLens = deliverTime.length;
o += "\r\n<select class=\"form-control\" id='deliverytime' data-action=\"changedelivertime\" name=\"deliverytime\">\r\n\t";
        for(j = 0; j < jLens; j++){
    o += "\r\n\t<option value=\"" +deliverTime[j].Id+ "\" ";if(deliverTime[j].Selected) { o += "selected"; } o += ">" +deliverTime[j].Name+ "</option>\r\n\t\r\n\t"; } o += "\r\n</select>\r\n<!--\r\n<div class=\"btn-group deliverytime\" data-toggle=\"buttons\" data-action=\"changedelivertime\">\r\n    ";
        for(j = 0; j < jLens; j++){
    o += "\r\n        <label class=\"btn btn-option ";if(deliverTime[j].Selected) { o += "active"; } o += "\">\r\n            <input type=\"radio\" name=\"deliverytime\" value=\"" +deliverTime[j].Id+ "\">" +deliverTime[j].Name+ "\r\n        </label>\r\n    "; } o += "\r\n</div>\r\n-->"; return o; }; });
define('widget/buy/deliverytime',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./deliverytime.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, api, template) {
    "use strict";

    var URI_DELIVER_TIME = api.get("deliver_timebyarea");

    return Widget.extend(function($element, widgetName, deliverTimeId) {
        var me = this;

        me.DeliverTimeId = parseInt(deliverTimeId, 10) || 0;
    }, {
        "hub/deliverTime/reload": function(districtId) {
            var me = this;

            if (!districtId) {
                return;
            }

            // GET deliver time
            ajaxQuery({
                url: URI_DELIVER_TIME,
                type: "GET",
                data: {
                    "areaId": districtId,
                    "language": context.language
                },
                dataType: "json"
            }).then(function(dataDeliverTimes) {
                var deliverTime = me.deliverTime = dataDeliverTimes.Values;

                var i = 0;
                var iLens = deliverTime.length;
                var defaultDeliverTime;

                if (me.DeliverTimeId) {
                    for (i = 0; i < iLens; i++) {
                        if (deliverTime[i].Id === me.DeliverTimeId) {
                            deliverTime[i].Selected = true;
                            defaultDeliverTime = deliverTime[i];
                            break;
                        }
                    }

                    if (!defaultDeliverTime) {
                        deliverTime[0].Selected = true;
                        defaultDeliverTime = deliverTime[0];
                    }
                } else {
                    // Choose first one as default deliver time
                    deliverTime[0].Selected = true;
                    defaultDeliverTime = deliverTime[0];
                }
                // Rendering
                me.html(template, {
                    deliverTime: deliverTime
                }).then(function() {
                    // Set default value.
                    me.publish("receiver/changeDeliverTime", defaultDeliverTime);
                });
            });
        },
        // Change to another deliver time span
        "dom:[name=deliverytime]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $e = $(e.currentTarget);

            var selectDeliverTimeId = parseInt($e.val(), 10);

            var i = 0;
            var iLens = me.deliverTime.length;
            for (i = 0; i < iLens; i++) {
                if (parseInt(me.deliverTime[i].Id, 10) === selectDeliverTimeId) {
                    me.publish("receiver/changeDeliverTime", me.deliverTime[i]);
                    break;
                }
            }
        }
    });
});

define('troopjs-requirejs/template!widget/buy/editaddress.html',[],function() { return function template(data) { var o = "<form id=\"editaddress\" class=\"standardform form-editaddress form-horizontal\">\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading orderedit-delivery-title\">\r\n            "; if (data) { o += "\r\n                <strong data-weave=\"widget/blurb/main(1073)\">Edit delivery address</strong>\r\n            "; } else { o += "\r\n                <strong data-weave=\"widget/blurb/main(1074)\">Add a delivery address</strong>\r\n            "; } o += "\r\n        </div>\r\n        <div class=\"panel-body\">\r\n            ";
                var address = data || {};
                renderAddress(address);
            o += "\r\n            <div class=\"text-center\">\r\n                <button type=\"submit\" class=\"btn btn-default\" data-action=\"updateaddress\" data-weave=\"widget/blurb/main(1115)\">CONFIRM</button>\r\n                <button class=\"btn btn-default\" data-action=\"cancelupdateaddress\" data-weave=\"widget/blurb/main(1075)\">CANCEL</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n<!-- \r\n<div class='tips'>如果您已经是维果清会员，<a href='javascript:;' class='btn-login' data-action='signin'>请点击此处登录</a></div>\r\n\r\n -->\r\n <b class=\"access-nav-close\" data-action='closeaddress'>&times;</b>\r\n"; function renderAddress(address) { o += "\r\n    <div class=\"form-deliveryaddress\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required value=\"" +(address.FirstName || "")+ "\" autofocus data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" maxlength=11 id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Contact Number\" required value=\"" +(address.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-account-address form-group\">\r\n            <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n            <div class=\"col-sm-8\">\r\n                <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"" +(address.ProvinceId||0)+ "\" data-city-id=\"" +(address.CityId||0)+ "\" data-area-id=\"" +(address.AreaId||0)+ "\"></div>\r\n                <br>\r\n                <input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Apartment, suite, unit, building, floor, etc.\" required value=\"" +(address.Line1 || "")+ "\" data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-zipcode\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-zipcode\" name=\"signup-zipcode\" placeholder=\"Zip code\" value=\"" +(address.ZipCode || "")+ "\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/buy/editaddress',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"template!./editaddress.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, template) {
	"use strict";

	var URI_RECEIVER = api.get("receiver");
	var URI_RECEIVER_ADD = api.get("receiver_create");
	var URI_RECEIVER_UPDATE = api.get("receiver_update");

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName, receiverId) {
		var me = this;

		me.receiverId = receiverId;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;
			var deferred = when.defer();

			if (me.receiverId) {
				ajaxQuery({
					url: URI_RECEIVER,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					var receivers = data.Receivers;
					var i = 0;
					var iLens = receivers.length;
					for (i = 0; i < iLens; i++) {
						if (receivers[i].Id === me.receiverId) {
							me.html(template, receivers[i]);
							break;
						}
					}
					deferred.resolve();
				});
			} else {
				me.html(template);
				deferred.resolve();
			}

			return deferred.promise;
		},
		"dom:[data-action=cancelupdateaddress]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			me.publish("receiver/closeAddress");
		},
		"dom:form/submit": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			} else {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1623
				}).open();

				return;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: cityId,
				areaId: areaId,
				provinceId: $form.find("select[name=provincepicker]").val()
			};

			var URI_ADDRESS = URI_RECEIVER_ADD;
			if (me.receiverId) {
				URI_ADDRESS = URI_RECEIVER_UPDATE;
				$.extend(true, addressData, {
					id: me.receiverId
				});
			}

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return;
			}

			// Update / Add delivery address
			ajaxQuery({
				url: URI_ADDRESS,
				data: JSON.stringify(addressData),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}).then(function(data) {
				// Send update address Id
				me.publish("receiver/refreshAddress", me.receiverId || data.ReceiverID);
			}, function(e) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: getStatus(e)
				}).open();
			});
		}
	});
});

define('troopjs-requirejs/template!widget/buy/gift.html',[],function() { return function template(data) { var o = "";
    var isGift = data.isGift || false;
    var blessing = data.blessing || "";
o += "\r\n<h4><strong data-weave=\"widget/blurb/main(1606)\">This is a Gift</strong></h4>\r\n<div class=\"the-form\">\r\n    <div class=\"form-group\">\r\n        <input type=\"checkbox\" class=\"icheck\" id=\"gift-needblessing\" data-action=\"needgift\" value=\"\" "; if (isGift) { o += "checked"; } o += ">\r\n        <label for=\"gift-needblessing\">\r\n            <span data-weave=\"widget/blurb/main(1606)\">This is a Gift</span>\r\n        </label>\r\n    </div>\r\n    <div class=\"form-group blessing-sub "; if (!isGift) { o += "hid"; } o += " row\">\r\n        <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"gift-inputs\">\r\n                <textarea style=\"resize: vertical;\" class=\"form-control\" rows=\"3\" cols=\"10\" data-for=\"gift-blessing\" name=\"gift-blessing\" id=\"gift-blessing\" placeholder=\"礼品赠言\">"; if (isGift && blessing) { o += "" + blessing; } o += "</textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/buy/gift',["troopjs-browser/component/widget",
    "jquery",
    "widget/cookie/main",
    "template!./gift.html",
    "json2"
], function(Widget, $, cookie, template) {
    "use strict";

    return Widget.extend({
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            me.html(template, data).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            cookie.get("gift_info").then(function(giftInfo) {
                var objGiftInfo = JSON.parse(giftInfo);

                me.render({
                    "isGift": objGiftInfo.isGift,
                    "blessing": objGiftInfo.blessing
                });
            }, function() {
                me.render({
                    "isGift": false,
                    "blessing": ""
                });
            });
        },
        "dom:[data-action=needgift]/ifChanged": function(e) {
            var me = this;
            var $me = me.$element;

            if (e.currentTarget.checked) {
                $me.find(".blessing-sub").show();
            } else {
                $me.find(".blessing-sub").hide();
            }
        },
        "hub/order/gift/update": function(giftInfo) {
            cookie.set("gift_info", JSON.stringify(giftInfo), {
                expires: 7,
                path: "/"
            });
        },
        "hub/order/gift/remove": function() {
            cookie.rm("gift_info", {
                path: "/"
            });
        },
        "hub/order/gift/submit": function(orderId) {
            var me = this;

            var $me = me.$element;
            var isGift;
            var postData;

            isGift = $me.find(".icheckbox_square").hasClass("checked");

            postData = {
                "orderId": orderId,
                "IsGift": isGift,
                "Blessing": isGift === true ? $me.find("#gift-blessing").val() : ""
            };

            var promiseValue = [];
            promiseValue.push(postData);
            return promiseValue;
        }
    });
});

define('troopjs-requirejs/template!widget/buy/goodslist.html',[],function() { return function template(data) { var o = "";
    var Packages = data.Packages;
    var Group = data.Group; 
    var arrayAddon = data.Addon;
o += "\r\n\r\n\t\t<div class=\"goods-tit\">\r\n\t\t <a href=\"/cleanse/detail#packageid/" +Packages.Id + "\" target=\"_blank\">\r\n  \t\t\t<h4 class=\"vendor_name_h\" id=\"0\">" +Packages.Title + "</h4>\r\n  \t\t\t </a>\r\n\t\t</div>\r\n\r\n\t    <div class=\"goods-items\">     \t\t\t\t\t\t\t\t\r\n\t\t\t<div class=\"goods-item goods-item-extra\" goods-id=\"173045\">\r\n\t\t\t\t<div class=\"p-img\">\r\n\t\t\t\t\t<a target=\"_blank\" href=\"/cleanse/detail#packageid/" +Packages.Id + "\">\r\n\t\t\t\t\t\t<img src=\"/static/products/" +Group.Rules[0].Gallery[0].img + "\" alt=\"\">\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"goods-msg\">\r\n\t\t\t\t\r\n\t\t        \t<div class=\"goods-msg-gel\">\t\t\t\r\n\t  \t\t\t\t\t<div class=\"p-name\">\r\n\t  \t\t\t\t\t";
	  					var arrPrds = Group.Rules[0].Products; 
	  					for (i=0; i < arrPrds.length; i++) { o += "\r\n    \t  \t\t\t\t\t" +arrPrds[i].Name + " + \r\n\t\t\t\t\t\t"; }o += " \t\t     \r\n\t  \t\t\t\t\t    \r\n\t  \t\t\t\t\t</div>\r\n\t\t\t            <div class=\"p-price\">\r\n\t\t\t            \t<strong class=\"jd-price\">￥" +Group.Rules[0].PackagePrice + ".00</strong>\r\n\t\t\t              \t<span class=\"p-num\"> x " +data.Days + " </span>\r\n\t\t\t                <span class=\"p-state\" data-weave=\"widget/blurb/main(1288)\">Days</span>\r\n\t\t\t            </div>\r\n\t       \t\t\t</div>   \r\n\t       \t\t\t<span class=\"ftx-04\"> " +Group.Name + "  " +Group.Rules[0].Products.length + " 瓶</span>\r\n\t       \t\t\t\r\n\t       \t\t\t<div class=\"goods-msg-gel\">\t\t\r\n\t       \t\t\t\r\n\t       \t\t\t"; for (i=0; i < arrayAddon.length; i++) {
	       				if(arrayAddon[i] != undefined){
	       			o += "\r\n\t  \t\t\t\t\t<div class=\"p-name\">\r\n\t  \t\t\t\t\t\t<span data-weave=\"widget/blurb/main(" +arrayAddon[i].blur + ")\"></span>\r\n\t       \t\t\t\t</div>\r\n\t\t\t            <div class=\"p-price\">\r\n\t\t\t            \t<strong class=\"jd-price\">￥" +arrayAddon[i].price + ".00</strong>\r\n\t\t\t              \t<span class=\"p-num\"> x " +data.Days + " </span>\r\n\t\t\t                <span class=\"p-state\" data-weave=\"widget/blurb/main(1288)\">Days</span>\r\n\t\t\t            </div>\r\n\t\t\t         \r\n\t\t\t         "; }}o += "\r\n\t\t\t         </div> \t\r\n\t\t\t\t</div>\r\n\t    \t\t\r\n\t    \r\n\t\t\t\t<div class=\"clr\"></div>\r\n\t\t\t\t<div class=\"gift-item ftx-03\"  >\r\n\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>       "; return o; }; });
define('widget/buy/goodslist',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./goodslist.html",
    "json2",
    "underscore.string"
], function(Widget, weave, when, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";
    var URL_PACKAGE = "/static/data/cn/package33.js";
	var addonDict = [];
	addonDict["22"] = {
		"blur":1292,
        "cn": "每次外送加一瓶芳香椰子水",
        "en": "Fragrant Clear x 1",
        "price":50
    };
    
	addonDict["20"] = {
		"blur":1291,
        "cn": "每次外送加一瓶大师清体柠檬水",
        "en": "Spicy Lemonade x 1",
        "price":25
    };
    
	addonDict["89"] = {
		"blur":1482,
        "cn": "每次外送加一瓶紫甘蓝靓汤",
        "en": "Purple Broth x 1",
        "price":50
    };
    return Widget.extend(function($element, widgetName,packageId,groupId,days,addon1,addon2,addon3) {
        var me = this;
        me.packageId = packageId;
        me.groupId = groupId;
        me.days = days;
        me.arrayAddon = [addonDict[addon1],addonDict[addon2],addonDict[addon3]];
    }, {
    	"render": function(data) {
            var me = this;
            var $me = me.$element;
            
            me.Packages = data.Packages[0];
        	me.Group = {};
        	for(var i=0;i<me.Packages.Groups.length;i++){
        		if(me.Packages.Groups[i].Id === me.groupId){
        			me.Group = me.Packages.Groups[i]; 
        		}
        	}
        	
            me.html(template, {
                "Packages": me.Packages,
                "Group": me.Group,
                "Days":me.days,
                "Addon":me.arrayAddon
            }).then(function(d) {
            	console.log(d);
            },function(d){
            	console.log(d);
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;
            //me.html(template,{"aa":'aa'});
            me.referesh(me.packageId,me.groupId);
        },
        "referesh": function(packageId, groupId) {
        	var me = this;
            var $me = me.$element;
            
            me.packageId = packageId;
            me.groupId = groupId;
            //me.arrayAddon = arrayAddon;
            
        	// 静态页面里关于本产品的数据
        	var statUrl = "/static/data/"+context.language+"/package"+packageId+".js";
        	console.log(statUrl);
            // GET deliver time
            ajaxQuery({
                url: statUrl,
                type: "GET",
                dataType: "json"
            }).then(function(data) {
           	 // Rendering
            	me.render(data);
            },function(data){
            	console.log('goodslist');
            });

        }
    });
});

define('troopjs-requirejs/template!widget/buy/invoice.html',[],function() { return function template(data) { var o = "<ul id=\"invoice-list\">\r\n\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t<div class=\"payment-item item-selected online-payment \" id=\"invoice-needinvoice\" for=\"pay-method-1\" payname=\"无需发票\"  data-action=\"selectinvoice\" data-weave=\"widget/blurb/main('p2155')\"><b></b>\r\n\t\t\t无需发票</div>\r\n\t</li>\r\n\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t<div class=\"payment-item  online-payment \" id=\"invoice-personal\" for=\"pay-method-4\" value=\"2\" payname=\"个人发票\" data-action=\"selectinvoice\" data-weave=\"widget/blurb/main(1045)\"><b></b>\r\n\t\t\t个人发票</div>\r\n\t</li>\r\n\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t<div class=\"payment-item  online-payment \" id='invoice-company' for=\"pay-method-4\" value=\"1\" payname=\"公司发票\"  data-action=\"selectinvoice\" data-weave=\"widget/blurb/main(1046)\"><b></b>\r\n\t\t\t公司发票</div>\r\n\t</li>\r\n\t<li >\r\n\t\t<input class=\"input form-control\" type=\"text\" id=\"invoice-companyname\" name=\"signup-phonenumber\" placeholder=\"Company title\" required=\"\" value=\"\" data-action=''>\r\n\t\t<input type=\"button\" class=\"btn-add btn\" id=\"btn-invoice-confirm\" data-action=\"confirminvoice\" value=\"OK\" >\r\n\t</li>\r\n</ul>\r\n\r\n<!--\r\n<div class=\"the-form\">\r\n    <div class=\"form-group\">\r\n        <input type=\"checkbox\" class=\"icheck\" id=\"invoice-needinvoice\" data-action=\"needinvoice\" value=\"\">\r\n        <label for=\"invoice-needinvoice\" data-weave=\"widget/blurb/main(1044)\">Yes, I would like invoice</label>\r\n    </div>\r\n    <div class=\"form-group invoice-sub hid row\">\r\n        <div class=\"col-sm-12 col-md-4\">\r\n            <input type=\"radio\" class=\"icheck\" id=\"invoice-personal\" data-action=\"personalinvoice\" name=\"invoice-type\" value=\"2\" checked>\r\n            <label for=\"invoice-personal\" data-weave=\"widget/blurb/main(1045)\">Personal invoice</label>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group invoice-sub hid row\">\r\n        <div class=\"col-sm-12 col-md-4\">\r\n            <input type=\"radio\" class=\"icheck\" id=\"invoice-company\" data-action=\"companyinvoice\" name=\"invoice-type\" value=\"1\" data-weave=\"widget/checkmyradio-listener/main\">\r\n            <label for=\"invoice-company\" data-weave=\"widget/blurb/main(1046)\">Company invoice</label>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-7\">\r\n            <div class=\"invoice-inputs\">\r\n                <input type=\"text\" class=\"form-control\" data-for=\"invoice-company\" id=\"invoice-companyname\" placeholder=\"Company title\" data-weave=\"widget/checkmyradio/main\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"invoice-sub hid\">\r\n        <p class=\"help-block\" data-weave=\"widget/blurb/main(1201)\">Your invoice will be issued on the juice package together, please note that check!</p>\r\n    </div>\r\n</div>\r\n-->"; return o; }; });
define('widget/buy/invoice',["troopjs-browser/component/widget",
	"jquery",
	"template!./invoice.html",
	"json2"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				$me.find('.icheck').iCheck({
					checkboxClass: 'icheckbox_square',
					radioClass: 'iradio_square',
					increaseArea: '20%'
				});
				// Check default
				$('input[name=invoice-type]:eq(0)').iCheck('check');
			});
		},
		"dom:[data-action=companyinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (!e.currentTarget.checked) {
				return;
			}
			$me.find("#invoice-companyname").focus();
		},
		"dom:[data-action=needinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (e.currentTarget.checked) {
				$me.find(".invoice-sub").show();
			} else {
				$me.find(".invoice-sub").hide();
			}

			// Trigger "height change" event on "auto follower"
			//me.publish("autoFollower/resize");
		},
		//发票的效果
		"dom:[data-action=selectinvoice]/click":function(e){
			var $e = $(e.currentTarget);
			$('#invoice-list .online-payment').removeClass('item-selected');
			$('#invoice-companyname').hide();
			$('#btn-invoice-confirm').hide();
			$e.addClass('item-selected');
			if($e.attr("id") == "invoice-company"){
				// 发票抬头的效果
				$('#invoice-companyname').show();
				$('#btn-invoice-confirm').show();
			}
		},
		// 发票提交
		"dom:[data-action=confirminvoice]/click":function(e){
			var $e = $(e.currentTarget);
			if($e.val() == "OK"){
				if($('#invoice-companyname').val() !== ''){
					$('#invoice-companyname').addClass('rd-only');
					$('#invoice-companyname').attr("readonly",true);
					$e.val("修改");
				}else{
					
				}
			}else{
				$('#invoice-companyname').removeClass('rd-only');
				$('#invoice-companyname').attr("readonly",false);
				$e.val("OK");
				$('#invoice-companyname').focus();
			}
		},
		"hub/order/invoice/submit": function(orderId) {
			var me = this;

			var $me = me.$element;
			var invoiceType;
			var postData;

			if($me.find("#invoice-needinvoice").hasClass("item-selected")){
			}else{
				invoiceType = $me.find(".item-selected").attr("value");
				postData = {
					orderId: orderId,
					invoiceType: invoiceType,
					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
				};
			}
//			if ($me.find(".icheckbox_square").hasClass("checked")) {
//				invoiceType = $me.find(".iradio_square.checked > input").val();
//				postData = {
//					orderId: orderId,
//					invoiceType: invoiceType,
//					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
//				};
//			}

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;

		}
	});
});

define('troopjs-requirejs/template!widget/buy/nonloginaddress.html',[],function() { return function template(data) { var o = "<form class=\"standardform form-editaddress form-horizontal\">\r\n\t<div class=\"panel panel-default\">\r\n\t\t<div class=\"panel-heading orderedit-delivery-title\">\r\n\t    \t<strong data-weave=\"widget/blurb/main(1313)\">Sign in to your account</strong>\r\n\t    </div>\r\n    \t<div class=\"panel-body\">\r\n\t\t    ";
		        var address = data || {};
		        renderAddress(address);
		    o += "\r\n    \t</div>\r\n    </div>\r\n</form>\r\n\r\n\r\n"; function renderAddress(address) { o += "\r\n    <div class=\"form-deliveryaddress\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required value=\"" +(address.FirstName || "")+ "\" autofocus data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Mobile Number\" required value=\"" +(address.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-account-address form-group\">\r\n            <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n            <div class=\"col-sm-8\">\r\n                <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"" +(address.ProvinceId||0)+ "\" data-city-id=\"" +(address.CityId||0)+ "\" data-area-id=\"" +(address.AreaId||0)+ "\"></div>\r\n                <br>\r\n                <input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Shipping address\" required value=\"" +(address.Line1 || "")+ "\" data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-zipcode\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-zipcode\" name=\"signup-zipcode\" placeholder=\"Zip code\" value=\"" +(address.ZipCode || "")+ "\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/buy/nonloginaddress',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"widget/enrollment/signin/main",
	"template!./nonloginaddress.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, Signin, template) {
	"use strict";

	(new Signin($(document.body))).start();

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName) {
		var me = this;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				// Popup order-edit lightbox.
				me.publish("enrollment/signin/popup");
			});
		},
		"dom:[data-action=updateregion]/reset": function(e, region) {
			e.preventDefault();
			e.stopPropagation();

			var me = this;
			var $el = $(e.currentTarget);

			if (!region || !region.District) {
				return;
			}

			// Reload Delivery Time Span
			me.publish("deliverTime/reload", region.District.Id);
			me.publish("expressCompany/reload", region.District.ExpressCompanyId);
		},
		"hub/order/receiver/nonlogin/submit": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: region.City.Id,
				areaId: region.District.Id
			};

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return when.reject();
			}

			var promiseValue = [];
			promiseValue.push(addressData);
			return promiseValue;
		},
		"dom:[data-action=signin]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			// Popup order-edit lightbox.
			me.publish("enrollment/signin/popup");
		}
	});
});

define('troopjs-requirejs/template!widget/buy/orderinfo.html',[],function() { return function template(data) { var o = "<div data-weave=\"widget/buy/delivery\"></div><!--订单第一步主逻辑，无界面-->\r\n<div data-weave=\"widget/buy/packagedays\"></div><!--产品包主逻辑，无界面-->\r\n\r\n<!-- 订单地址浮层开始 -->\r\n\t<div class=\"order-address-box\" >\r\n\t\t<!--地址编辑器 buy/editaddress -->\r\n\t\t<div class=\"the-form\" data-action=\"editaddresscontainer\">\r\n\t\t</div>\r\n\t</div>\r\n\t\r\n\t<div class=\"backdrop\"></div>\r\n<!-- 订单地址浮层结束 -->\r\n\r\n<!-- 进度触点开始 -->\r\n<div class=\"w w1 stepflex-header clearfix\">\r\n    <div class=\"stepflex\" id=\"#sflex03\">\r\n        <dl class=\"first doing\">\r\n            <dt class=\"s-num\">1</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(p1281)\">填写核对订单信息<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal \">\r\n            <dt class=\"s-num\">2</dt>\r\n            <dd class=\"s-text\" data-weave=\"widget/blurb/main(p1358)\">支付及确认订单<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal last \">\r\n            <dt class=\"s-num\">3</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(1280)\">成功提交订单<s></s><b></b></dd>\r\n        </dl>\r\n    </div>\r\n</div>\r\n<!-- 进度触点结束 -->\r\n\t\t\r\n\r\n<!-- 订单主框架 -->\r\n<div id=\"container\">\r\n    <div id=\"content\" class=\"w\">\r\n        <div class=\"m\">\r\n            <div class=\"checkout-tit\"><span class=\"tit-txt\"  data-weave=\"widget/blurb/main(p1281)\">填写并核对订单信息</span></div>\r\n            <div class=\"mc\">\r\n                <div class=\"checkout-steps\">\r\n                    <div class=\"step-tit\">\r\n                        <h3 data-weave=\"widget/blurb/main(1087)\">收货人信息</h3>\r\n                        <div class=\"extra-r\">\r\n                            <a href=\"#none\" class=\"ftx-05\" data-action=\"addAddress\" data-weave=\"widget/blurb/main(1074)\">Add a new delivery address</a>\r\n                            <input type=\"hidden\" id=\"del_consignee_type\" value=\"0\"/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"step-cont\" >\r\n                        <div id=\"consignee-addr\" class=\"consignee-content\" >\r\n                        \t<div class=\"consignee-scrollbar\">\r\n\t\t\t\t\t\t\t\t<div class=\"ui-scrollbar-main\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"consignee-scroll\">\r\n\t\t\t\t\t\t\t\t\t\t<button id=\"btnNewAddr\" data-action=\"addAddress\">+</button>\r\n\t\t\t\t\t\t\t\t\t\t<!--地址列表容器 buy/receiver -->\r\n\t\t\t                            <div class=\"consignee-cont hide\" id=\"consignee1\" data-action='deliveryaddresscontainer'>\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t                            </div>\r\n\t\t                            </div>\r\n\t                            </div>\r\n                            </div>\r\n                        </div>\r\n                        \r\n                        <!--更多地址选择器，暂时不用-->\r\n                        <div class=\"addr-switch switch-on hide\"  onclick=\"show_ConsigneeAll()\">\r\n                            <span> + 更多地址</span><b></b>\r\n                        </div>\r\n                        <div class=\"addr-switch switch-off hide\" onclick=\"hide_ConsigneeAll()\">\r\n                            <span> - 收起地址</span><b></b>\r\n                        </div>\r\n                        \r\n                        \r\n                    </div>\r\n                    \r\n                    <div class=\"hr\"></div>\r\n                    \r\n                   \r\n                    <div class=\"step-tit\">\r\n                    \t<h3 data-weave=\"widget/blurb/main(1111)\">Payment method</h3>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"step-cont\" data-weave=\"widget/buy/paymenttype('')\" data-source-code=\"\">\r\n\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"hr\"></div>\r\n\r\n\t\t\t\t\t<div class=\"step-tit\">\r\n\t\t\t\t\t\t<h3  data-weave=\"widget/blurb/main(1477)\">送货清单</h3>\r\n\t\t\t\t\t\t<div class=\"extra-r\"></div>\r\n\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t<div class=\"step-cont\">\r\n\t\t\t\t\t\t<div class=\"shopping-lists\" id=\"shopping-lists\" > \r\n\t\t\t\t\t        <!--定义已购买商品清单LIST-->\r\n\t\t\t\t\t    \t<div class=\"shopping-list ABTest\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<div class=\"goods-list\"  data-action=goodscontainer></div>\r\n\t\t\t\t\t      \t\t\t<div class=\"dis-modes\">\r\n\r\n\t\t\t\t\t                  <div class=\"mode-item mode-tab\">\r\n\t\t\t\t\t                  <div class=\"mode-item-tit\">\r\n\t\t\t\t\t                    <h4 data-weave=\"widget/blurb/main('p2158')\">配送方式</h4>\r\n\t\t\t\t\t                    \r\n\t\t\t\t\t                  </div>\r\n\t\t\t\t\t                  <div class=\"mode-tab-nav\">\r\n\t\t\t\t\t                    <ul data-action=\"changepickuptype\">\r\n\t\t\t\t\t\t\t\t\t\t\t<li class=\"mode-tab-item curr\" id=\"express_shipment_item\" data-action=\"switchshipment\" data-pickup=\"1\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#pickuptype-deliver\" data-toggle=\"tab\" data-weave=\"widget/blurb/main(1379)\">Select a shipping address</a>\r\n\t\t\t\t\t                        </li>\r\n\t\t\t\t\t                        <li class=\"mode-tab-item \" id=\"pick_shipment_item\" data-pickup=\"2\" data-weave=\"widget/delivery/selfpickup/entry/main()\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"m-txt\">亲自来店取餐</span><b></b>\r\n\t\t\t\t\t                      \t</li>\r\n\t\t\t\t\t                    </ul>\r\n\t\t\t\t\t                  </div>\r\n\t\t\t\t\t                  <div data-weave=\"widget/buy/deliverydate\"></div>\r\n\t\t\t\t\t                  \r\n\t\t\t\t\t                  \r\n\t\t\t\t\t\t\t\t\t\t<div class=\"mode-tab-con \" id=\"express_shipment\">  \r\n\t\t\t\t\t\t\t\t\t\t\t<ul class=\"mode-list\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ftx-03\" data-weave=\"widget/blurb/main(1086)\">理想送达时间段：</span>&nbsp;&nbsp;<br>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ftx-03\" data-weave=\"widget/blurb/main(1200)\">理想送达时间段：</span>&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div data-weave=\"widget/buy/deliverytime\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!--TODO\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"fore1\" id=\"jd_shipment_calendar_date\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"ftx-03\">配送时间：</span>&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class='promisetip'>预计:&nbsp;2016-1-12 13:00 - 17:00&nbsp;送达</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t-->\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t\t\t</div>\t\r\n\t\t\t\t\t            \t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"mode-tab-con hide\" id=\"selfpick_shipment\">\t\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"the-form\" data-weave=\"widget/buy/selfpickup\"></div>\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t                \r\n\t\t\t\t\t    \t\t\t\t\t\t   \r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t \t\t\t\t\t \r\n\t\t\t\t\t\t\t\t\t</div><!--dis-modes 结束-->\r\n\t\t\t\t\t\t\t\t<div class=\"clr\"></div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div><!--shopping-list 结束-->\t\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t    <div class=\"clr\"></div>\r\n\t\t\t\t\t  </div>\r\n\t\t\t\t\t  <!--shopping-lists 结束-->  \r\n\t\t\t\t\t</div>\t                        \r\n                    \r\n                    <div class=\"hr\"></div>\r\n                    \r\n                    <div class=\"step-tit\">\r\n                        <h3 data-weave=\"widget/blurb/main(1041)\">Your tax invoice (fapiao)</h3> \r\n                        （<small data-weave=\"widget/blurb/main(1043)\">VCLEANSE will issue an invoice for products, for delivery fees, please contact Yamato Express.</small>)\r\n                    </div>\r\n                    <div class=\"step-cont\">\r\n\t\t\t\t\t  <div class=\"payment-list\" id=\"\">\r\n\t\t\t\t\t    <div class=\"list-cont\" data-weave=\"widget/buy/invoice\">\r\n\t\t\t\t\t      <ul id=\"invoice-list\">\r\n\t\t\t\t\t\t\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t\t\t\t\t\t\t<div class=\"payment-item item-selected online-payment \" for=\"pay-method-1\" payname=\"无需发票\" payid=\"1\"><b></b>\r\n\t\t\t\t\t\t\t\t\t无需发票</div>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t\t\t\t\t\t\t<div class=\"payment-item  online-payment \" for=\"pay-method-4\" payname=\"个人发票\" payid=\"4\"><b></b>\r\n\t\t\t\t\t\t\t\t\t个人发票</div>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li style=\"cursor: pointer;\" onclick=\"\">\r\n\t\t\t\t\t\t\t\t<div class=\"payment-item  online-payment \" id='invoice-company' for=\"pay-method-4\" payname=\"公司发票\" payid=\"4\"><b></b>\r\n\t\t\t\t\t\t\t\t\t公司发票</div>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li >\r\n\t\t\t\t\t\t\t\t<input class=\"input form-control\" type=\"text\" id=\"invoice-title\" name=\"signup-phonenumber\" placeholder=\"公司发票抬头\" required=\"\" value=\"\">\r\n\t\t\t\t\t\t\t\t<input type=\"button\" class=\"btn-add btn\" id=\"btn-invoice-confirm\" value=\"确定\" >\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t    </div>\r\n\t\t\t\t\t  </div>\r\n\t\t\t\t\t</div>\r\n                    <div class=\"clr\"></div>\r\n                    <!--/ /widget/invoice-step/invoice-step.tpl -->\r\n                    <div class=\"hr\"></div>\r\n                   \r\n                </div>\r\n            </div>\r\n        </div>\r\n       \r\n\r\n      \r\n\r\n\t\t\t\t<div class=\"trade-foot\">\r\n\t\t\t\t\t<div class='left'>\r\n\t\t\t\t\t\t<!-- \r\n\t\t\t\t\t\t<div class=\"consignee-foot\">\r\n\t\t\t\t\t\t\t<p class='receive-addr'>收货信息： 黄萌 上海 普陀区 城区 陕西北路1438号时代财富大厦507 133333330181</p>\r\n\t\t\t\t\t\t\t<p class='promisetip'>预计：2016-1-12 13:00 - 17:00</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"clr\"></div>\r\n\t\t\t\t\t\t -->\r\n\t\t\t\t\t\t<div class=\"order-coupon \" data-weave=\"widget/buy/promotion()\">\r\n\t\t\t\t\t\t\t<h4>拥有优惠码？请输入:</h4>\r\n\t\t\t\t\t\t\t<div class=\"group\">\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<input class='itxt' type=\"text\"><input type=\"button\" class=\"btn-add btn\" value=\"使用\" id=\"addGiftCardId\" >\r\n\t\t\t\t\t\t\t<span class=\"field-tip\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class='right order-summary' data-weave=\"widget/buy/summary\">\r\n\t\t\t\t\t\t<div class=\"statistic fr\">\r\n\t\t\t\t\t\t\t<div class=\"list\">\r\n\t\t\t\t\t\t\t\t<span data-weave=\"widget/blurb/main(1033)\">Product:</span> <em\r\n\t\t\t\t\t\t\t\t\tclass=\"price\" id=\"warePriceId\" v=\"11599.00\">－</em>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t<div class=\"list\">\r\n\t\t\t\t\t\t\t\t<span data-weave=\"widget/blurb/main(1034)\">Delivery:</span> <em class=\"price\"\r\n\t\t\t\t\t\t\t\t\tid=\"freightPriceId\"> －</em>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t<div class=\"list\">\r\n\t\t\t\t\t\t\t\t<span>--：</span> <em class=\"price\" id=\"sumPayPriceId\">\r\n\t\t\t\t\t\t\t\t\t－</em>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div id=\"checkout-floatbar\" class=\"group fr\">\r\n\t\t\t\t\t\t\t<div class=\"ui-ceilinglamp checkout-buttons\">\r\n\t\t\t\t\t\t\t\t<div class=\"sticky-placeholder hide\" style=\"display: none;\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"sticky-wrap\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"\">\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t<span class=\"total\" data-weave=\"widget/blurb/main(1040)\">Total:<strong id=\"payPriceId\">－</strong>\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t</div>\r\n        <div id=\"backpanel\">\r\n            <div id=\"backpanel-inner\" class=\"hide switchOn\">\r\n                <div class=\"bp-item bp-item-backtop\" data-top=\"0\">\r\n                    <a href=\"#none\" class=\"backtop\" target=\"_self\">返回顶部</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>\r\n</div>\r\n\r\n\r\n<!-- /main -->"; return o; }; });
// from [delivery/main]
define('widget/buy/orderinfo',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/enrollment/signin/main",
    "template!./orderinfo.html",
    "underscore",
    "json2"
], function(Widget, weave, when, $, context, cookie, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, Signin, template) {
    "use strict";
    (new Signin($(document.body))).start();
    
    var URI_ACCOUNT = api.get("account");
 // Time function
    var INS_TIME = new Time();

    var PRICE_DELIVERY = 25;
    
    
    //从confirm里来
    var URI_ORDER_CONFIRM = api.get("order_confirm");
    var EXPIRED_CODE = 5;
    var URI_ORDER_PAYMENT = api.get("orderpayment");
    // Transfer to blurb
    var invoiceDict = {
        "1": 1158,
        "2": 1159
    };
    
    function getIsSelfPickup($el) {
        // Hardcode pickup type
        // 1: Select a shipping address
        // 2: Pick up in store
        return $el.data("pickup").toString() === "2";
    }
    
    return Widget.extend(function(){
    	var me = this;
    	me.orderId = "";
    },{
    	"render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("receiver/hasLogin");
                    }, function() {
                        //
                        me.publish("receiver/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("receiver/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;

            me.render();
        },
        "hub/order/warningNoReceiver": function() {
            var me = this;
            var $me = me.$element;

            var $el = $me.find("[data-action=showErrorMsg-noReceiver]");
            // Show
            $el.show().fadeIn(400);
            // Hide
            setTimeout(function() {
                $el.fadeOut(400, function() {
                    $el.hide();
                });
            }, 3 * 1000);
        },
        // Already login, is a member
        "hub/receiver/hasLogin": function() {
            var me = this;
            me.publish("receiver/refreshAddress"); 
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;
            var $me = me.$element;
             
            // 如果没有登录则弹出登录窗口 
            //me.publish("enrollment/signin/popup");  
            //me.publish("receiver/editAddress");
            // 如果没有登录则显示不登录的地址编辑器
            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            $delivery.html("<div data-weave=\"widget/buy/nonloginaddress()\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	$("#consignee1").removeClass("hide");
            	//隐藏新增收货地址按钮。
            	$("[data-action='addAddress']").hide();
            });
            
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";
            
            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            
            $delivery.html("<ul id='consignee-list' data-weave=\"widget/buy/receiver(receiverid)\" data-receiverid=\"" + receiverId + "\"></ul>");
            
            //$delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	me.publish("receiver/closeAddress");
                $("[data-action=submitorder]").show();
            });
        },
        "hub/goods/refreshList": function(packageId,groupId,packageDays,arrayAddon) {
        	 var me = this;
             var $me = me.$element;
             
             var $goods = $me.find("[data-action=goodscontainer]");
             
             $goods.html("<div class='goods-list' data-weave=\"widget/buy/goodslist(" + packageId + "," + groupId + "," + packageDays + "," + arrayAddon.toString()+")\"></div>");
             
             weave.apply($goods.find("[data-weave]")).then(function() {
            	 	//alert('what happen');
             });
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=editaddresscontainer]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            //$delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            $delivery.html("<div data-weave=\"widget/buy/editaddress(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
            	me.publish("receiver/openAddress");
                $("[data-action=submitorder]").hide();
            });
        },
        // 配送方式选择器
        "hub/order/pickuptype": function() {
            var me = this;
            var $me = me.$element;

            //var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.active"));
            var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.curr"));

            var promiseValue = [];
            promiseValue.push(isSelfPickup);
            return promiseValue;
        },
		"dom:[data-action=signin]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;
			me.publish("receiver/closeAddress");
			// Popup order-edit lightbox.
			me.publish("enrollment/signin/popup");
		},
		 // Manage delivery address
        "dom:[data-action=addAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress");
        },
        
		//打开地址浮层
		"hub/receiver/openAddress": function(e) {
			if(isLogin){
				$(".order-address-box .tips").hide();
			}else{
				$(".order-address-box .tips").show();
			}
			
			$(".order-address-box").show().css("opacity", 0)
				.animate({"opacity": 1}, 300);
			
			$(".backdrop").css("z-index",14);
			$(".backdrop").show().css("opacity", 0)
			.animate({"opacity": 0.8}, 300);
		},
		
		// 关掉地址
		"dom:[data-action=closeaddress]/click":function(e){
			var me = this;
			me.publish("receiver/closeAddress");
		},
		//关闭地址浮层
		"hub/receiver/closeAddress":function(e){
			$(".order-address-box").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				$(".order-address-box").hide();
			});
			$(".backdrop").css("z-index",-100);
			$(".backdrop").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				
			});
		},
		
		"dom:[data-action=switchshipment]/click":function(e){
			var me = this;
			me.doSwithTab('pay');
		},
		"dom:[data-action=switchselfpack]/click":function(e){
			var me = this;
			me.doSwithTab('picksite');
		},
		"doSwithTab":function(mode){
		//"hub/shipment/change":function(mode){
			var me = this;
			
			$("#express_shipment_item").removeClass('curr');
			$("#pick_shipment_item").removeClass('curr');
			
			$("#express_shipment").addClass("hide");
			$("#selfpick_shipment").addClass("hide");

			switch(mode){
			case "pay":
				$("#express_shipment").removeClass("hide");
				$("#express_shipment_item").addClass('curr');
				break;
			case "picksite":
				$("#selfpick_shipment").removeClass("hide");
				$("#pick_shipment_item").addClass('curr');
				break;
			}
			me.publish("order/updateSummary");
		},
		
		
		//以下从confirm/main里来
		 "getPayment": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/paymenttype/submit", me.orderId).spread(function(dataPayment) {
                return when.resolve(dataPayment);
            });
        },
        "getInvoice": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/invoice/submit", me.orderId).spread(function(dataInvoice) {
                return when.resolve(dataInvoice);
            });
        },
        "getPromotion": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/promotion/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "getGiftInfo": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/gift/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        //TODO:这里是原本第二步的点击提交，所以这里需要改一下
        //"dom:[data-action=confirmorder]/click": function(e) {
        "hub/order/confirmorder": function(orderId) {
            //e.preventDefault();
            var me = this;
            var $me = me.$element;
            //var $el = $(e.currentTarget);

            me.orderId = orderId;
            var postData = {
                "OrderId": me.orderId
            };

            when.join(me.getPayment(), me.getInvoice(), me.getPromotion(), me.getGiftInfo()).then(function(data) {
                postData.Payment = data[0];
                postData.Invoice = data[1];
                postData.Promotion = data[2];
                postData.GiftInfo = data[3];

                _.extend(postData, {
                    "SourceCode": $me.find("#order-sourcecode").val() || ""
                });

                ajaxQuery({
                    url: URI_ORDER_CONFIRM,
                    data: JSON.stringify(postData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                //}, $el.parent()).then(function(data) {
                }).then(function(data) {
                    // Success Confirm Order (save Payment type & Invoice info)
                    //
                    // Redirect by payment type
                    var paymentType = postData.Payment.payment;
                    // URL redirect
                    if (paymentType.toString() === "1") {
                        pageUrl.goto("/payment/topay", "orderid/" + me.orderId);
                    } else {
                        // Show success info
                        pageUrl.goto("/order/success", "orderid/" + me.orderId, {
                            "orderid": me.orderId
                        });
                    }
                });
            });
        }
    });
});



define('widget/buy/packagedays',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/cookie/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/troophash/main",
    "json2"
], function(Widget, $, _, when, context, cloneObject, ajaxQuery, api, cookie, pageUrl, InfoBox, Time, troopHash) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    var WEEK_DAYS = 7;

    var URI_PACKAGE = api.get("package");
    var URI_PACKAGEBY_ID = api.get("package_loadbyid");

    var PACKAGE_DAYS = [1, 3, 5, 7];
    var PACKAGE_DEFAULT_DAY = 1;

    function parseIntArray(arrString) {
        if (!arrString || arrString.length <= 0) {
            return;
        }

        var i = 0;
        var iLens = arrString.length;
        var arrInt = [];

        for (i = 0; i < iLens; i++) {
            arrInt.push(parseInt(arrString[i], 10));
        }

        return arrInt;
    }

    function getRuleByDay(rulesOrigin, dayOfWeekFromMonday, day) {
        var iWeek = 0;
        day = day || 0;

        iWeek = dayOfWeekFromMonday + day;
        iWeek = iWeek >= WEEK_DAYS ? iWeek - WEEK_DAYS : iWeek;
        return rulesOrigin[iWeek];
    }

    function getPackageDayRules(dataRules, days, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = days || getPackageDefaultDays();
        var packageDayRules = [];

        // Week day
        var dayOfWeekFromMonday;

        // Rule index
        var k = 0;
        var kLens = dataRules.length;
        // Loop round
        var turn = 0;

        if (startDate) { // By week
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                packageDayRules.push(cloneObject(getRuleByDay(dataRules, dayOfWeekFromMonday, i)));
                packageDayRules[i].Day = i;
            }
        } else { // Not by week. Looping
            for (i = 0; i < iLens; i++) {
                // Rule round 
                turn = Math.floor(i / kLens);
                // k: Rule index
                k = i % kLens;

                // i: day index
                packageDayRules.push(cloneObject(dataRules[k]));

                // Day start from 0 - (Default from 1 in DB)
                if (turn === 0) {
                    // First round of rules 
                    packageDayRules[i].Day = dataRules[k].Day - 1;
                } else {
                    // > 1st round 
                    packageDayRules[i].Day = turn * dataRules[kLens - 1].Day + dataRules[k].Day - 1;
                }
            }
        }

        return packageDayRules;
    }

    function getSubscriptionPackageDayRules(dataRules, weekDays, duration, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = duration;
        var j = 0;
        var jLens = weekDays.length;
        var k = 0;
        var kLens = dataRules.length;
        var packageDayRules = [];
        var dayOfWeekFromMonday;
        var weekDay;
        var deltaWeekDay = 0;

        if (startDate) { // By week
            // 0 = Monday; 6 = Sunday;
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    if (deltaWeekDay < 0) {
                        break;
                    }

                    packageDayRules.push(cloneObject(getRuleByDay(dataRules, weekDay)));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;
                }
            }
        } else {
            dayOfWeekFromMonday = weekDays[0] === 0 ? 6 : weekDays[0] - 1;
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    packageDayRules.push(cloneObject(dataRules[k]));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;

                    // k: package index
                    if (k < (kLens - 1)) {
                        k++;
                    } else {
                        k = 0;
                    }

                }
            }
        }

        return packageDayRules;
    }

    function getPackageDefaultDays() {
        return PACKAGE_DEFAULT_DAY;
    }

    function getPackageDays(dataRules, days) {
        // Get days in package
        var dataDays = [];
        // Looping package rules, get days. 1,3,5,7...
        var k = 1;
        var kLens = PACKAGE_DAYS.length;
        for (k = 0; k < kLens; k++) {
            if (days) {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === days
                });
            } else {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === getPackageDefaultDays()
                });
            }
        }
        return dataDays;
    }

    function getPackageGroup(dataPackage, packageId, groupId) {
        // Get package by packageId & groupId
        var dataGroups = [];
        var dataGroup;
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                dataGroups = dataPackage[i].Groups;
                if (dataGroup) {
                    break;
                }
                for (var j = 0, jLens = dataGroups.length; j < jLens; j++) {
                    if (dataGroups[j].Id.toString() === groupId.toString()) {
                        dataGroup = dataGroups[j];
                        break;
                    }
                }
            }
        }
        return dataGroup;
    }

    function getPackageTitle(dataPackage, packageId) {
        // Get package name by packageId
        var title = "";
        var description = "";
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                title = dataPackage[i].Title;
                description = dataPackage[i].Description;
                break;
            }
        }
        return {
            "title": title || "",
            "description": description || ""
        };
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.startDateDeferred = when.defer();
    }, {
        "getPackage": function(packageId) {
            var me = this;
            var $me = me.$element;

            var deferred = when.defer();

            if (me.dataPackage) {
                deferred.resolve(me.dataPackage);
            } else {
                // Get Package
                ajaxQuery({
                    url: packageId ? URI_PACKAGEBY_ID : URI_PACKAGE,
                    data: (packageId ? {
                        packageId: packageId,
                        language: context.language
                    } : {
                        language: context.language
                    }),
                    type: "GET",
                    dataType: "json"
                }).then(function(dataPackage) {
                    me.dataPackage = dataPackage.Packages;
                    deferred.resolve(me.dataPackage);
                    return;
                });
            }
            return deferred.promise;
        },
        "render": function(startDate) {
            var me = this;

            var packageDayRules;

            if (!me.packageRules) {
                return;
            }

            if (me.weekDays && me.duration) {
                // Subscription
                packageDayRules = getSubscriptionPackageDayRules(me.packageRules, me.weekDays, me.duration, startDate);
            } else if (me.packageDays) {
                // Standard
                packageDayRules = getPackageDayRules(me.packageRules, me.packageDays, startDate);
            }
            if (!packageDayRules || packageDayRules.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1146
                }).open();

                return;
            }
            // Publish package package
            me.publish("order/package", packageDayRules);
        },
        "readyForRender": function() {
            var me = this;

            if (me.packageGroup.ByWeek) {
                me.startDateDeferred.promise.then(function(startDate) {
                    me.render(startDate);
                });
            } else {
                // Rendering
                me.render();
            }
        },
        "hub/deliveryStartDate/change/next": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            var status = me.startDateDeferred.promise.inspect();

            if (status.state === "fulfilled" && me.packageGroup && me.packageGroup.ByWeek) {
                me.startDateDeferred = when.defer();
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
                me.readyForRender();
            } else {
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
            }
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            packageid
            groupid
            days
            addon
            -----------
            event
            -----------
            weekDays
            duration
            */

            var packageId = parseInt(hashMap.packageid, 10);
            var groupId = parseInt(hashMap.groupid, 10);

            // General cleanse order
            var packageDays = parseInt(hashMap.days, 10) || getPackageDefaultDays();

            // Subscription
            var weekDays = hashMap.weekDays ? parseIntArray(hashMap.weekDays.split(",")) : [];
            var duration = parseInt(hashMap.duration, 10) || 0;

            // Addon
            var arrayAddon = hashMap.addon ? hashMap.addon.split(",") : [];

            // General cleanse order
            var eventCode = hashMap.event || "";

            // packageId & groupId is required
            // or will direct to root page
            if (isNaN(packageId) || isNaN(groupId)) {
                pageUrl.goto("/");
                return;
            }

            // Trigger event while following param changes
            if (me.packageId === packageId &&
                me.groupId === groupId &&
                me.packageDays === packageDays &&
                _.difference(weekDays, me.weekDays).length === 0 &&
                me.duration === duration &&
                _.difference(arrayAddon, me.arrayAddon).length === 0 &&
                me.eventCode === eventCode
            ) {
                return;
            }
            me.packageDays = packageDays || 0;
            me.weekDays = weekDays;
            me.duration = duration || 0;
            me.arrayAddon = arrayAddon;
            me.eventCode = eventCode;

            // Update Addon
            me.publish("order/addon", arrayAddon);

            // Publish source code
            me.publish("order/source", eventCode);

            me.publish("goods/refreshList",packageId,groupId,packageDays,arrayAddon);
            // Package OR Customize
            if (packageId !== 0 && groupId !== 0) {
                // Clear customize order data from cookie
                cookie.rm("customize", {
                    path: "/"
                });
                // Clear customize order data from cookie
                cookie.rm("subscription_cust", {
                    path: "/"
                });

                // Compare with last hash, return while without change
                if (me.packageId && me.groupId && me.packageId === packageId && me.groupId === groupId) {
                    // Ready for render
                    me.readyForRender();
                    return;
                }
                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;

                // Get Package
                me.getPackage(packageId).then(function(dataPackage) {
                    // Looping package, get the right rule
                    // Save to widget at same time
                    var packageGroup = me.packageGroup = getPackageGroup(dataPackage, packageId, groupId);
                    me.packageTitle = getPackageTitle(dataPackage, packageId);

                    if (!packageGroup) {
                        return;
                    }

                    me.packageRules = packageGroup.Rules;

                    // Ready for render
                    me.readyForRender();
                });
            } else if (weekDays && duration) {
                // Get customize content from cookie
                cookie.get("subscription_cust").then(function(customizeRule) {
                    me.packageRules = [{
                        "Id": 0,
                        "Day": 1,
                        "Name": "Subscription customize",
                        "Products": JSON.parse(customizeRule)
                    }];
                    // Not by week
                    me.render();
                }, function() {});

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            } else { // Customize
                // Publish package package
                me.publish("order/package");

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            }
        }
    });
});

define('troopjs-requirejs/template!widget/buy/paymenttype.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var paymentType = data.paymentType;
    var sourceCode = data.sourceCode || "";
    var isMobile = data.isMobile;

    var paymentId = 0;
    /* Default: pay online */
    var defaultPaymentId = 1;
o += "\r\n\t\t<div class=\"payment-list\" id=\"\">\r\n\t    \t<div class=\"list-cont\">\r\n\t    \t\t<ul id=\"payment-list\">\r\n\t    \t\t\t"; 
				        var i = 0;
				        var iLens = paymentType.length;
				        for (i = 0; i < iLens; i++) {
				            paymentId = paymentType[i].Id;
				    o += "\r\n\t\t\t\t\t<li onclick=\"\" >\r\n\t\t\t\t\t\t<div id=\"orderconfirm-payment-" +paymentId+ "\" \r\n\t\t\t\t\t\t\tdata-payment-id = " +paymentId+ "\r\n\t\t\t\t\t\t\tclass=\"payment-item  online-payment "; if (paymentId === defaultPaymentId) { o += "item-selected"; } o += " \" \r\n\t\t\t\t\t\t\tfor=\"pay-method-1\" \r\n\t\t\t\t\t\t\tdata-action='paymentselect'><b></b>\r\n\t\t\t\t\t\t\t" +paymentType[i].Name+ "\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t"; } o += "\r\n\t\t\t\t\t\r\n\t\t\t\t</ul>\r\n\t    \t</div>\r\n\t  \t</div>\r\n\t\t<p class=\"text-warning\">\r\n\t\t\t<small>\r\n\t\t\t[<a class=\"link-fn\" data-action=\"showallbanks\" data-weave=\"widget/blurb/main(1451)\">See all online payment types</a>]\r\n\t\t\t<b>支付宝快捷支付（含信用卡、储蓄卡）</b>已开通大额支付。<a href=\"https://cshall.alipay.com/lab/help_detail.htm?help_id=245345\" target=\"_blank\">支付限额详见&gt;&gt;</a>\r\n\t\t\t</small>\r\n\t\t</p>\r\n<!--\r\n<h4>\r\n    <strong data-weave=\"widget/blurb/main(1111)\">Payment method</strong>\r\n</h4>\r\n<p class=\"text-warning\">\r\n    <small>\r\n        <b>支付宝快捷支付（含信用卡、储蓄卡）</b>已开通大额支付。<a href=\"https://cshall.alipay.com/lab/help_detail.htm?help_id=245345\" target=\"_blank\">支付限额详见&gt;&gt;</a>\r\n    </small>\r\n</p>\r\n<div class=\"the-form\">\r\n    "; 
        var i = 0;
        var iLens = paymentType.length;
        for (i = 0; i < iLens; i++) {
            paymentId = paymentType[i].Id;
    o += "\r\n        <div class=\"form-group\" data-action=\"orderconfirm-payment-" +paymentId+ "\">\r\n            <label for=\"orderconfirm-payment-" +paymentId+ "\">\r\n                <input type=\"radio\" class=\"icheck\" id=\"orderconfirm-payment-" +paymentId+ "\" name=\"orderconfirm-pay-type\" value=\"" +paymentId+ "\" "; if (paymentId === defaultPaymentId) { o += "checked"; } o += ">\r\n                " +paymentType[i].Name+ "\r\n                \r\n                "; if (paymentId === 1) { o += "\r\n                    "; if (!isMobile) { o += "\r\n                        [<a class=\"link-fn\" data-action=\"showallbanks\" data-weave=\"widget/blurb/main(1451)\">See all online payment types</a>]\r\n                    "; } 
                } o += "\r\n            </label>\r\n        </div>\r\n    "; } o += "\r\n</div>\r\n-->"; return o; }; });

define('troopjs-requirejs/template!widget/buy/banklist.html',[],function() { return function template(data) { var o = "";
  var cacheServer = data.cacheServer;
o += "\r\n\r\n<div class=\"v-popup-container\">\r\n  <h6>支付平台：</h6>\r\n  <ul class=\"hor banklist\">\r\n    <li data-action=\"selectbank\" data-id=\"ALIPAY\">\r\n        <img src=\"" +cacheServer+ "/images/payment/alipay.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"ALIPAY_QR\">\r\n        <img src=\"" +cacheServer+ "/images/payment/alipay_saomazhifu.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"UCFPAY\">\r\n        <img src=\"" +cacheServer+ "/images/payment/ucfpay.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n  </ul>\r\n\r\n  <h6>支付网银：</h6>\r\n  <ul class=\"hor banklist\">\r\n    <li data-action=\"selectbank\" data-id=\"CMB\">\r\n        <label class=\"bank-cmbchina\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"BOCB2C\">\r\n        <label class=\"bank-boc\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"ICBCB2C\">\r\n        <label class=\"bank-icbc\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"COMM\">\r\n        <label class=\"bank-bankcomm\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"CCB\">\r\n        <label class=\"bank-ccb\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"CIB\">\r\n        <label class=\"bank-cib\"></label>\r\n    </li>\r\n  </ul>\r\n  <ul class=\"hor banklist banklist-more\">\r\n      <li data-action=\"selectbank\" data-id=\"CMBC\">\r\n          <label class=\"bank-cmbc\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"GDB\">\r\n          <label class=\"bank-cgbchina\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SPDB\">\r\n          <label class=\"bank-spdb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"ABC\">\r\n          <label class=\"bank-abchina\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SPABANK\">\r\n          <label class=\"bank-pingan\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"POSTGC\">\r\n          <label class=\"bank-psbc\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"CITIC\">\r\n          <label class=\"bank-ecitic\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SHRCB\">\r\n          <label class=\"bank-srcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SHBANK\">\r\n          <label class=\"bank-bankofshanghai\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"NBBANK\">\r\n          <label class=\"bank-nbcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"HZCBB2C\">\r\n          <label class=\"bank-hccb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"BJBANK\">\r\n          <label class=\"bank-bankofbeijing\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"BJRCB\">\r\n          <label class=\"bank-bjrcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"WZCBB2C-DEBIT\">\r\n          <label class=\"bank-wzcb\"></label>\r\n      </li>\r\n  </ul>\r\n  <hr>\r\n  <p class=\"text-center\">\r\n    <button type=\"button\" class=\"btn btn-default\" data-action=\"close\" data-weave=\"widget/blurb/main(1452)\">I got it</button>\r\n  </p>\r\n</div>"; return o; }; });
define('widget/buy/paymenttype',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/popupbox/main",
	"template!./paymenttype.html",
	"template!./banklist.html",
	"json2",
	"underscore"
], function(Widget, $, when, context, ajaxQuery, api, PopupBox, template, templateBanklist) {
	"use strict";

	var URI_PAYMENT = api.get("payment");

	return Widget.extend(function($element, widgetName, sourceCode) {
		var me = this;

		me.sourceCode = sourceCode || "";
		me.deferred = when.defer();
		// order
		// confirm
		// orderdetail
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_PAYMENT,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}).then(function(data) {
				var paymentTypes = data.Values;

				// Rendering
				me.html(template, {
					"paymentType": paymentTypes,
					"sourceCode": me.sourceCode,
					"cacheServer": context.cacheServer,
					"isMobile": context.isMobile
				}).then(function() {
					$me.find('.icheck').iCheck({
						checkboxClass: 'icheckbox_square',
						radioClass: 'iradio_square',
						checkedClass: 'checked',
						increaseArea: '20%'
					});

					// Rendered
					me.deferred.resolver.resolve();
				});
			});
		},
		"hidePayonDelivery": function() {
			var me = this;
			var $me = me.$element;

			// Hide "Cash on Delivery"
			$me.find("[data-action=orderconfirm-payment-2]").remove();
		},
		"hub/order/orderdetail": function(order) {
			var me = this;

			if (!order || !order.DeliverList) {
				return;
			}

			// Waiting for payment rendered
			me.deferred.promise.then(function() {
				_.each(order.DeliverList, function(e) {
					// Without "payment on delivery"
					// -----------------------------
					// 维果清上海商城店 - 3
					// Z&B Fitness Studio - 4
					// 泰生厨房 - 6
					// -----------------------------
					// 上海顺丰标准B - 8
					// 空运 - 9
					// 空运次晨 - 10
					// 空运B - 12
					if ((e.IsSelfPickUp &&
							e.StoreInfo &&
							_.indexOf([3, 4, 6], e.StoreInfo.StoreId) >= 0) ||
						_.indexOf([8, 9, 10, 12], e.ExpressCompanyId) >= 0) {
						me.hidePayonDelivery();
						return;
					}
				});
			});
		},
		// 支付选择器钩子
		"hub/order/paymenttype/submit": function(orderId) {
			var me = this;
			var $me = me.$element;

			//var paymentType = $me.find(".iradio_square.checked > input").val();
			//得到当前选择的支付方式
			var paymentType = $me.find("#payment-list .item-selected").attr('data-payment-id');
			
			var postData = {
				orderId: orderId,
				payment: paymentType,
				language: context.language
			};

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;
		},
		"dom:[data-action=showallbanks]/click": function() {
			var me = this;

			var $templateBanklist = $(templateBanklist({
				"cacheServer": context.cacheServer
			}));
			// Lightbox configuration
			var popupboxBanklist = new PopupBox({
				msg: $templateBanklist,
				closeble: true,
				closeInner: true,
				zIndex: 1050,
				closeButtonList: ['[data-action=close]'],
				closeCallback: function() {}
			});

			popupboxBanklist.open();
		},
		// 支付选择的切换效果
		"dom:[data-action=paymentselect]/click":function(e){
			$('#payment-list .online-payment').removeClass('item-selected');
			$(e.currentTarget).addClass('item-selected');
		}
	});
});

define('troopjs-requirejs/template!widget/buy/promotion.html',[],function() { return function template(data) { var o = "<h4 data-weave=\"widget/blurb/main(1240)\">Have any gift cards or promotional claim codes?</h4>\r\n<div class=\"group\">\r\n<form id=\"promotioncode\" class=\"standardform form-promotioncode\">\r\n<input type=\"text\" id=\"order-promotioncode\" placeholder=\"\" required class='itxt' >\r\n\r\n<button class=\"btn-add btn\" type=\"submit\" data-action=\"applypromotioncode\" data-weave=\"widget/blurb/main(1241)\">APPLY</button>\r\n<span class=\"field-tip\"></span>\r\n        <div>\r\n            <p data-action=\"invalidpromotioncode\" class=\"order-promotioncode-msg\"></p>\r\n        </div>\r\n</form>\r\n</div>\r\n\t\t\t\t\t\t\t\r\n<!--\r\n<p>\r\n    <small data-weave=\"widget/blurb/main(1240)\">Have any gift cards or promotional claim codes?</small>\r\n</p>\r\n<div class=\"the-form order-applypromotion\">\r\n    <form id=\"promotioncode\" class=\"standardform form-promotioncode\">\r\n        <div class=\"cf\">\r\n            <input type=\"text\" id=\"order-promotioncode\" placeholder=\"\" required>\r\n            <button class=\"\" type=\"submit\" data-action=\"applypromotioncode\" data-weave=\"widget/blurb/main(1241)\">APPLY</button>\r\n        </div>\r\n        <div>\r\n            <p data-action=\"invalidpromotioncode\" class=\"order-promotioncode-msg\"></p>\r\n        </div>\r\n    </form>\r\n</div>\r\n-->"; return o; }; });
define('widget/buy/promotion',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/pageurl/main",
    "template!./promotion.html",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, Blurb, pageUrl, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_PROMOTION_CODE = api.get("order_promotion_code");

    function getStatus(code) {
        var status = [];
        status["1"] = "1242";
        status["2"] = "1270";
        status["3"] = "1242";
        status["4"] = "1367";
        status["5"] = "1368";
        status["6"] = "1369";
        status["7"] = "1388";
        status["8"] = "1389";
        status["9"] = "1390";
        status["10"] = "1391";
        status["11"] = "1468";
        status["12"] = "1625";

        return blurb.get(status[code]) || "";
    }

    function getSuccessStatus(code) {
        // Here code means promotion rule id
        var status = [];
        status["1"] = "1370";
        status["2"] = "1370";
        status["3"] = "1371";
        status["4"] = "1372";
        status["5"] = "1373";
        status["6"] = "1384";
        status["7"] = "1385";
        status["8"] = "1602";
        status["9"] = "1603";
        status["10"] = "1604";

        return blurb.get(status[code]) || "";
    }

    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;
        me.promotionCode = "";
        me.orderId = parseInt(orderId, 10);
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template);
        },
        "dom:form/submit": function(e) {
        	e.preventDefault();
            var me = this;
            var $me = me.$element;
            
            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            var $code = $me.find("#order-promotioncode");
            var $submitBtn = $me.find("[data-action=applypromotioncode]");

            var code = _.trim($code.val());

            me.promotionCode = code;
            $code.attr("baga", code);
            
            me.publish("order/updateSummary");
        },

        "hub/order/getPromotionCode": function(){
        	var me = this;
        	var promiseValue = [];
        	promiseValue.push(me.promotionCode);
        	return promiseValue;
        	//return me.promotionCode;
        },
        //原来的优惠码提交废弃
//        "domdd:form/submit": function(e) {
//            e.preventDefault();
//            var me = this;
//            var $me = me.$element;
//
//            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
//            var $code = $me.find("#order-promotioncode");
//            var $submitBtn = $me.find("[data-action=applypromotioncode]");
//
//            var code = _.trim($code.val());
//
//            var postData = {
//                OrderId: me.orderId,
//                Code: code
//            };
//
//            ajaxQuery({
//                url: URI_PROMOTION_CODE,
//                data: JSON.stringify(postData),
//                contentType: "application/json; charset=UTF-8",
//                type: "POST",
//                dataType: "json"
//            }).then(function(data) {
//                $errMsg.text(getSuccessStatus(data.RuleId)).addClass("order-promotioncode-valid");
//
//                $code.attr("baga", code);
//                // Change order price summary
//                me.publish("order/promotion", data.Order);
//            }, function(e) {
//                $errMsg.text(getStatus(e)).removeClass("order-promotioncode-valid");
//
//                $code.removeAttr("baga");
//                // Change order price summary reflesh
//                me.publish("order/promotion");
//            });
//        },
        // summary传回promotion错误的信息
        "hub/order/promotion/error":function(ruleResult){
        	var me = this;
        	var $me = me.$element;
        	var $code = $me.find("#order-promotioncode");
        	var code = _.trim($code.val());
        	var $errMsg = $me.find("[data-action=invalidpromotioncode]");
        	if(ruleResult.StatusCodes !== 0){

		       	 $errMsg.text(getStatus(ruleResult.StatusCodes)).removeClass("order-promotioncode-valid");
		       	 $code.removeAttr("baga");
        	}else{
        		$errMsg.text(getSuccessStatus(ruleResult.RuleId)).addClass("order-promotioncode-valid");
        		$code.attr("baga", code);
        	}
        },
        "dom:#order-promotioncode/focus": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            $errMsg.empty().removeClass("order-promotioncode-valid");
        },
        "hub/order/promotion/submit": function(orderId) {
            var me = this;

            var $me = me.$element;

            var $code = $me.find("#order-promotioncode");

            var code = _.trim($code.attr("baga"));
            var postData;

            if (code) {
                postData = {
                    OrderId: orderId,
                    Code: code
                };
            }

            var promiseValue = [];
            promiseValue.push(postData);

            return promiseValue;
        }
    });
});

define('troopjs-requirejs/template!widget/buy/receiver.html',[],function() { return function template(data) { var o = "";
    var receiverId = data.receiverId || 0;
    var arrData = data.options || [];
    var selectedId = data.selectedId;
    var disabled = data.disabled;
o += "\r\n<!--循环每一个地址-->\r\n    ";
        var i = 0;
        var iLens = arrData.length;
        for (i=0; i < iLens; i++) {
    o += "\r\n    \t<li class=\"ui-switchable-panel ui-switchable-panel-selected\" data-action=\"hoveraddress\" value=\"" +arrData[i].Id+ "\">\r\n\t\t\t<div class=\"consignee-item "; if (selectedId === arrData[i].Id) { o += "item-selected"; } o += "\" \r\n\t\t\t\tdata-action=\"receivercontrol\" \r\n\t\t\t\tdata-option-id = \"" +i + "\">\r\n\t\t\t\t<span limit=\"3\">" +arrData[i].Name+ "</span><b></b>\t\t\t\r\n\t\t\t</div>\r\n\t\t\t<div class=\"addr-detail\">\r\n\t\t\t\t<!--\r\n\t\t\t\t<span class=\"addr-name\" limit=\"6\">" +arrData[i].FirstName+ " " +arrData[i].LastName+ "</span>\r\n\t\t\t\t <span class=\"addr-info\" limit=\"45\">" +arrData[i].CityName+ " " +arrData[i].AreaName+ " " +arrData[i].Line1+ "</span>\r\n\t\t\t\t-->      \r\n\t\t\t\t <span class=\"addr-tel\">" +arrData[i].Mobile+ "</span>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"op-btns\">\r\n\t\t\t\t<span></span>\r\n\t\t\t\t<a href=\"#none\" class=\"ftx-05 edit-consignee\" data-action='editAddress' data-receiver-id='" +arrData[i].Id+ "' data-weave=\"widget/blurb/main(1088)\">Edit this address</a>\r\n\t\t\t\t<!--\r\n\t\t\t\t<a href=\"#none\" class=\"ftx-05 del-consignee \" data-action='delAddress' data-receiver-id='" +arrData[i].Id+ "'>删除</a>\r\n\t\t\t\t-->\r\n\t\t\t</div>\r\n\t\t</li>\r\n              \r\n    ";
        }
    o += "\r\n\t\t\r\n<!--\r\n<p class=\"orderedit-delivery-title\" data-weave=\"widget/blurb/main(1087)\">Deliver to this address</p>\r\n<div data-action=\"receivercontrol\" class=\"orderedit-delivery-content orderedit-delivery-address\">\r\n    <div data-action=\"changedeliveraddress\" data-weave=\"widget/select/receiver/main(receiverId)\" data-receiver-id=\"" +receiverId+ "\" data-value=\"" +receiverId+ "\"></div>\r\n    <br>\r\n    <p class=\"orderedit-delivery-address-edit\">\r\n        <span>&rsaquo;</span> <a class=\"link-fn\" data-action=\"editAddress\" data-weave=\"widget/blurb/main(1088)\">Edit this address</a>\r\n    </p>\r\n</div>\r\n<p>\r\n    <span>&rsaquo;</span> <a class=\"link-fn\" data-action=\"addAddress\" data-weave=\"widget/blurb/main(1074)\">Add a new delivery address</a>\r\n</p>\r\n-->"; return o; }; });
define('widget/buy/receiver',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/blurb/main",
    "widget/api/main",
    "template!./receiver.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, Blurb,api, template) {
    "use strict";
    var URI_RECEIVER = api.get("receiver");
    var blurb = new Blurb($(document.body));
    
    function getAddressString(address) {
        if (!address) {
          return "";
        }
        var strAddress = "";

        if (context.language === "en") {
          if (address.FirstName) {
            strAddress += address.FirstName;
          }
          if (address.LastName) {
            strAddress += " " + address.LastName;
          }
          strAddress += ",";
        } else {
          if (address.LastName) {
            strAddress += address.LastName;
          }
          if (address.FirstName) {
            strAddress += " " + address.FirstName;
          }
          strAddress += "，";
        }

        strAddress += " " + address.CityName;
        strAddress += " " + address.AreaName;
        if (address.Line1) {
          strAddress += " " + address.Line1;
        }
        if (address.Line2) {
          strAddress += " " + address.Line2;
        }
        // Deliverble
        if(!address.IsActive){
          strAddress += " (" + blurb.get(1469) + ")";
        }

        return strAddress;
      }
    return Widget.extend(function($element, widgetName, receiverId) {
        var me = this;

        me.receiverId = receiverId || 0;
    }, {
        "sig/start": function() {
            var me = this;
            
            ajaxQuery({
                url: URI_RECEIVER,
                data: {
                  "t": (new Date()).getTime()
                },
                type: "GET",
                dataType: "json"
              }).then(function(data) {
            	  $("#consignee1").removeClass("hide");
            	  	var defaultReceiver;

                  // With/Without receiver
                  if (data.Receivers && data.Receivers.length > 0) {
                	  $("#btnNewAddr").addClass("hide");
                    _.each(data.Receivers, function(receiver) {
                      // Set "Name"
                      receiver.Name = getAddressString(receiver);
                      // Find default receiver
                      if (me.receiverId) {
                        if (receiver.Id === me.receiverId) {
                          defaultReceiver = receiver;
                        }
                      } else if (receiver.IsDefault) {
                        // Set receiverId
                        me.receiverId = receiver.Id;
                        defaultReceiver = receiver;
                      }
                    });
                    
                 // Store options for trigger event
                    me.options = data.Receivers;
                    
                    // Rendering
                    me.html(template, {
                        receiverId: me.receiverId,
                        "options": me.options,
                        "selectedId": me.receiverId,
                        "disabled": me.disabled
                    }).then(function() {
                    	me.reset(me.receiverId);
                    });
                    
                  }
                  
                  me.publish("receiver/default", defaultReceiver);
              });
           
        },
        "hub/receiver/default": function(defaultReceiver) {
            // TODO:
            var me = this;
            var $me = me.$element;
            var $receiverControl = $me.find("[data-action=receivercontrol]");

            if (defaultReceiver) {
                // Set default value.
                me.publish("receiver/changeDeliverAddress", defaultReceiver);
                me.publish("deliverTime/reload", defaultReceiver.AreaId);
                me.publish("expressCompany/reload", defaultReceiver.ExpressCompanyId);

                $receiverControl.show();
            } else {
                $receiverControl.hide();
            }
        },
        
        "dom:[data-action=hoveraddress]/mouseenter":function(e){
        	$("#consignee-list li").removeClass("li-hover");
    		$(e.currentTarget).addClass("li-hover");
        },
        "dom:[data-action=hoveraddress]/mouseleave":function(e){
        	$("#consignee-list li").removeClass("li-hover");
        },
        // 切换选择地址
        "dom:[data-action=receivercontrol]/click": function(e) {
        	var me = this;
            var $el = $(e.currentTarget);
            var optionId = $el.attr('data-option-id');
            var receiver = me.options[optionId];
            
        	$("#consignee-list li").removeClass("ui-switchable-panel-selected");
        	$("#consignee-list .consignee-item").removeClass("item-selected");
        	$el.parent().addClass("ui-switchable-panel-selected");
        	$el.addClass("item-selected");
    		//TODO:更新收货人组件
            

            if (!receiver) {
                return;
            }

            me.receiverId = receiver.Id;
            me.publish("receiver/changeDeliverAddress", receiver);
            me.publish("deliverTime/reload", receiver.AreaId);
            me.publish("expressCompany/reload", receiver.ExpressCompanyId);
        },
       
        // Edit selected address
        "dom:[data-action=editAddress]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $el = $(e.currentTarget);
            me.receiverId = $el.attr('data-receiver-id');
            
            me.publish("receiver/editAddress", {
                isEdit: true,
                receiverId: me.receiverId
            });
        }
    });
});

define('troopjs-requirejs/template!widget/buy/selfpickup.html',[],function() { return function template(data) { var o = "";
    var account = data.account;
    
    var stores = data.stores;
    var j = 0;
    var jLens = stores.length;

    var cities = data.cities;
    var i = 0;
    var iLens = cities.length;
    
    var selectedStoreId = data.selectedStoreId;
o += "\r\n<ul class=\"mode-list\">\t\t\t\t\t\t\t\t\r\n\t<li>\r\n\t\t<span class=\"ftx-03\">选择您最方便的自提点：</span>&nbsp;&nbsp;\r\n\t\t<select class=\"form-control\" id='selfpicupstore' data-action=\"changeStore\">\r\n\t\t\t";
				var selectedIndex = 0;
			    for (j = 0; j < jLens; j++) {
			    	var isSelected = "";
			        store = stores[j];
			        if (store.IsActive) { 
			       		if(store.StoreId == selectedStoreId){
			       			selectedIndex = j;
			       			isSelected = "selected";
			       		}
			o += "\r\n\t\t\t\t\t\t<option value=\"" +store.StoreId+ "\" " +isSelected+ ">\r\n\t\t\t\t\t\t\t" +store.StoreCityName + " " +store.StoreName + "\r\n\t\t\t\t\t\t</option>\r\n\t\t\t"; 
					}
				}
			o += "\r\n\t\t</select>\r\n\t</li>\t\t\t\t\t\t\t\t\t\t\r\n\t<li>\r\n\t\t"; 

				storeInfo = stores[selectedIndex];
			
		
		o += "\r\n\t\t<span class=\"ftx-03\" data-weave=\"widget/blurb/main(1128)\">Store address:</span>\r\n\t\t<span data-shop-field = 'addr'>\r\n\t\t\t" +storeInfo.StoreAddress + "\r\n            "; if (storeInfo.StoreMapUrl) { o += "\r\n                [<a class=\"link-fn\" data-url=\"" +storeInfo.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n            "; } o += "\r\n\t\t</span>\r\n\t\t<div class=\"clr\"></div>\r\n\t\t\r\n        "; if (storeInfo.StoreBusinessHours) { o += "\r\n            <span class=\"ftx-03\"  data-weave=\"widget/blurb/main(1382)\">Business hours:</span>\r\n            <span>" +storeInfo.StoreBusinessHours + "</span>\r\n            <div class=\"clr\"></div>\r\n        "; } o += "\r\n        "; if (storeInfo.StoreTel) { o += "\r\n            <span class=\"ftx-03\" data-weave=\"widget/blurb/main(1387)\">Contact tel:</span>\r\n            <span>" +storeInfo.StoreTel + "</span>\r\n        "; } o += "\r\n\t</li>\t\r\n</ul>\r\n\r\n\r\n\r\n<form class=\"standardform form-selfpickup-info form-horizontal hide\">\r\n    ";
        renderAddress();
    o += "\r\n</form>\r\n\r\n\r\n"; function renderAddress() { o += "\r\n    <p class=\"orderedit-delivery-title\" data-weave=\"widget/blurb/main(1383)\">Leave your contact information</p>\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"selfpickup-firstname\" data-weave=\"widget/blurb/main(1397)\">Name:</label>\r\n        <div class=\"col-sm-8\">\r\n            <input class=\"form-control\" type=\"text\" id=\"selfpickup-firstname\" name=\"selfpickup-firstname\" placeholder=\"Pickup contactor's name is required\" required value=\"" +(account.FirstName || "")+ "\" data-weave=\"widget/blurb/main(1398, 'placeholder')\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"selfpickup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n        <div class=\"col-sm-8\">\r\n            <input class=\"form-control\" type=\"text\" id=\"selfpickup-phonenumber\" name=\"selfpickup-phonenumber\" placeholder=\"Your Contact Number\" required value=\"" +(account.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n        </div>\r\n    </div>\r\n"; } o += "\r\n\r\n<!--\r\n\r\n\r\n<div class=\"selfpicupstore\">\r\n    <ul class=\"nav nav-tabs\">\r\n        ";
            for (i = 0; i < iLens; i++) {
        o += "\r\n            <li class=\""; if (i===0) { o += "active"; } o += "\">\r\n                <a href=\"#selfpicupstore-" +cities[i].Id+ "\" data-toggle=\"tab\">" +cities[i].Name+ "</a>\r\n            </li>\r\n        "; } o += "\r\n    </ul>\r\n    <div class=\"tab-content\">\r\n        ";
            for (i = 0; i < iLens; i++) {
        o += "\r\n            <div class=\"tab-pane "; if (i===0) { o += "active"; } o += "\" id=\"selfpicupstore-" +cities[i].Id+ "\">\r\n                "; renderStore(cities[i].Id); o += "\r\n            </div>\r\n        "; } o += "\r\n   </div>\r\n</div>\r\n\r\n\r\n\r\n";
    function renderStore(cityId) {
        var store;
o += "\r\n    ";
        for (j = 0; j < jLens; j++) {
            store = stores[j];
            if (store.StoreCityId === cityId && store.IsActive) { 
    o += "\r\n                <label class=\"radio selfpicupstore-single\">\r\n                    <input type=\"radio\" class=\"icheck\" value=\"" +store.StoreId+ "\" data-action=\"changeStore\" name=\"selfpickup-store\">\r\n                    <strong class=\"selfpickupstore-name\">" +store.StoreName + "</strong>\r\n                </label>\r\n                <p class=\"selfpickupstore-gap\">\r\n                    <span data-weave=\"widget/blurb/main(1128)\">Store address:</span>\r\n                    " +store.StoreAddress + "\r\n                    "; if (store.StoreMapUrl) { o += "\r\n                        [<a class=\"link-fn\" data-url=\"" +store.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n                    "; } o += "\r\n                </p>\r\n                "; if (store.StoreBusinessHours) { o += "\r\n                    <p class=\"selfpickupstore-gap\">\r\n                        <span data-weave=\"widget/blurb/main(1382)\">Business hours:</span>\r\n                        " +store.StoreBusinessHours + "\r\n                    </p>\r\n                "; } o += "\r\n                "; if (store.StoreTel) { o += "\r\n                    <p class=\"selfpickupstore-gap\">\r\n                        <span data-weave=\"widget/blurb/main(1387)\">Contact tel:</span>\r\n                        " +store.StoreTel + "\r\n                    </p>\r\n                "; } o += "\r\n    ";
            }
        }
    o += "\r\n"; } o += "\r\n\r\n\r\n-->"; return o; }; });
define('widget/buy/selfpickup',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./selfpickup.html",
    "json2",
    "underscore.string"
], function(Widget, weave, when, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_STORES = api.get("stores");
    
    
    function getCities(stores) {
        var i = 0;
        var iLens = stores.length;
        var cities = [];
        var storeCityId = 0;

        function searchCities() {
            return _.find(cities, function(e, j) {
                return e.Id === storeCityId;
            });
        }

        for (i = 0; i < iLens; i++) {
            storeCityId = stores[i].StoreCityId;
            // City exist
            if (!stores[i].IsActive || searchCities()) {
                continue;
            }
            // City not exist
            cities.push({
                "Id": storeCityId,
                "Name": stores[i].StoreCityName
            });
        }

        return cities;
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;
        me.selectedStoreId = "";
        me.stores = [];
        me.dataAccount = "";
    }, {
        "render": function(stores, dataAccount) {
            var me = this;
            var $me = me.$element;

            dataAccount = dataAccount || {};
            me.stores = stores;
            me.dataAccount = dataAccount;

            // Rendering
            me.html(template, {
                "account": dataAccount,
                "stores": me.stores,
                "cities": getCities(stores),
                "selectedStoreId":me.selectedStoreId
            }).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    checkedClass: 'checked',
                    increaseArea: '20%'
                });
                // Check default
                $('input[name=selfpickup-store]:eq(0)').iCheck('check');
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // GET deliver time
            ajaxQuery({
                url: URI_STORES,
                type: "GET",
                data: {
                    "language": context.language
                },
                dataType: "json"
            }).then(function(data) {
                var stores = data.Stores;

                // Signin? Write default account info.
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.render(stores, data);
                }, function() {
                    //
                    me.render(stores);
                });

            });
        },
        "getIsSelfPickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/pickuptype").spread(function(dataPickupType) {
                return when.resolve(dataPickupType);
            });
        },
        "hub/order/selfpickup": function() {
            var me = this;
            var $me = me.$element;

            return me.getIsSelfPickup().then(function(isSelfPickup) {
                var storeId;
                var firstName;
                var mobile;
                var selfpickupInfo = {};
                var promiseValue = [];

                if (isSelfPickup) {
                    storeId = $me.find(".iradio_square.checked > input").val();
                    firstName = _.trim($me.find("#selfpickup-firstname").val());
                    mobile = _.trim($me.find("#selfpickup-phonenumber").val());

                    // Validate mobile
                    if (!regex.mobile(mobile)) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1408
                        }).open();

                        return when.reject();
                    }

                    promiseValue.push({
                        "isSelfPickup": true,
                        "selfpickupInfo": {
                            "storeId": storeId,
                            "firstName": firstName,
                            "mobile": mobile
                        }
                    });
                } else {
                    promiseValue.push({
                        "isSelfPickup": false,
                        "selfpickupInfo": {}
                    });
                }
                return promiseValue;
            });
        },
        "dom:[data-action=changeStore]/change":function(e){
        	var me = this;
        	var $me = me.$element;
        	me.selectedStoreId = $(e.currentTarget).val();
        	me.render(me.stores, me.dataAccount);
        },
        "dom:[data-toggle=tab]/click": function(e) {
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var href = $el.attr("href");

            // Check default
            $me.find(href + ' input[name=selfpickup-store]:eq(0)').iCheck('check');
            
        }
    });
});

define('troopjs-requirejs/template!widget/buy/startdate.html',[],function() { return function template(data) { var o = "";
	var dateFrom = data.dateFrom;
	var disabled = data.disabled;
	var range = data.range;
	var dateTo = data.dateTo;
o += "\r\n"; if(range) { o += "\r\n\t<div class=\"row startdate-range\">\r\n\t\t<div class=\"col-md-6\">\r\n\t\t\t<span data-weave=\"widget/blurb/main(1399)\">From</span>\r\n\t\t\t<input type=\"text\" class=\"form-control datepicker-from\" data-action=\"datepickerfrom\" value=\"" +dateFrom+ "\" "; if(disabled) { o += "disabled"; } o += " onfocus=\"this.blur();\">\r\n\t\t</div>\r\n\t\t<div class=\"col-md-6\">\r\n\t\t\t<span data-weave=\"widget/blurb/main(1400)\">to</span>\r\n\t\t\t<input type=\"text\" class=\"form-control datepicker-to\" data-action=\"datepickerto\" value=\"" +dateTo+ "\" "; if(disabled) { o += "disabled"; } o += ">\r\n\t\t</div>\r\n\t</div>\r\n"; } else { o += "\r\n\t<input type=\"text\" class=\"form-control datepicker-from\" id=\"startDate\" style=\"position: relative; z-index: 2;\" data-action=\"datepickerfrom\" onfocus=\"this.blur();\" value=\"" +dateFrom+ "\" "; if(disabled) { o += "disabled"; } o += ">\r\n"; }  return o; }; });
define('widget/buy/startdate',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/pageurl/main",
    "widget/time/main",
    "widget/infobox/main",
    "widget/troophash/main",
    "template!./startdate.html",
    "jquery.ui.datepicker",
    "jquery.ui.datepicker-zh-CN",
    "underscore"
], function(Widget, $, when, context, cloneObject, pageUrl, Time, InfoBox, troopHash, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 10;

    // Month duration
    // Default: 1 month
    var MONTH_RANGE = 1;

    // Official holiday
    var HOLIDAY = [
        // Note: Month start from 0
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
		new Date(2016, 3, 3),
		new Date(2016, 3, 4),
    ];

    function geti18n() {
        var lngMap = {
            "en": "",
            "cn": "zh-CN"
        };
        var lng = context.language || "en";
        return lngMap[lng];
    }

    function transformDateArray(DateArray) {
        if (!DateArray && DateArray.length <= 0) {
            return;
        }

        var disableDays = [];
        _.each(DateArray, function(e) {
            // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
            // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
            var d = INS_TIME.addDay(e, 1);
            disableDays.push(INS_TIME.getStrDate(d, "-"));
        });
        return disableDays;
    }

    return Widget.extend(function($element, widgetName, range, rangeMonth) {
        var me = this;

        me.range = range || false;
        me.rangeMonth = 0;
        if (range) {
            // Default: 3 months
            me.rangeMonth = parseInt(rangeMonth, 10) || MONTH_RANGE;
        }

        me._holiday = HOLIDAY;
    }, {
        "sig/start": function() {
            var me = this;
        },
        // 饮用周期大于1天时，需要考虑假期是否在饮用周期内
        "preForbidDate": function(days) {
            var me = this;

            days = days || 1;
            if (!me._holiday && me._holiday.length <= 0) {
                return;
            }
            var holidayStartDate = me._holiday[0];
            var holidayMax = cloneObject(me._holiday);
            var i = 1;
            for (i = 1; i < days; i++) {
                holidayMax.unshift(INS_TIME.addDay(holidayStartDate, i * -1));
            }
            /* create an array of days which need to be disabled */
            me._disableDays = transformDateArray(holidayMax);
        },
        // wdays[array]
        "preForbidWeekDays": function(wdays) {
            var me = this;

            if (!wdays) {
                return;
            }

            var arrWeekDays = [0, 1, 2, 3, 4, 5, 6];

            if (wdays.length > 0) {
                me._disableWeekDays = _.difference(arrWeekDays, wdays);
            } else {
                me._disableWeekDays = [];
            }
        },
        "getAvailableStartDate": function(hourAhead, days, wdays) {
            var me = this;

            hourAhead = hourAhead || HOUR_AHEAD;
            days = days || 1;
            var now = new Date();
            var dateNow = INS_TIME.getZeroDate(now);

            var hour = now.getHours();
            var startDate = hour >= hourAhead ? INS_TIME.addDay(dateNow, 2) : INS_TIME.addDay(dateNow, 1);
            var endDate = INS_TIME.addDay(startDate, days - 1);

            if (me._holiday && me._holiday.length > 0) {
                // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
                // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
                (function() {
                    var i = 0;
                    var iLens = me._holiday.length;
                    var holiday;

                    for (i = 0; i < iLens; i++) {
                        holiday = INS_TIME.addDay(INS_TIME.getZeroDate(me._holiday[i]), 1);

                        // startdate <= holiday <= enddate
                        // move startdate to the day after holiday
                        if (INS_TIME.compareDate(holiday, startDate) <= 0 && INS_TIME.compareDate(holiday, endDate) >= 0) {
                            // Reset dateRange
                            startDate = INS_TIME.addDay(holiday, 1);
                            endDate = INS_TIME.addDay(startDate, days - 1);
                            continue;
                        }
                    }
                })();
            }

            // Has weekdays, select recent date(the nearest date after startdate) match weekdays
            return INS_TIME.getNearestWeekdayDate(startDate, wdays);
        },
        // 不配送日期
        "offDays": function(date, disableWeekdays) {
            var me = this;

            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();
            var weekday = date.getDay();

            // Disable weedays, union preForbid & argument
            disableWeekdays = disableWeekdays || [];
            disableWeekdays = _.union(disableWeekdays, me._disableWeekDays);

            if (me._disableDays && me._disableDays.length > 0) {
                if ($.inArray(y + '-' + (m + 1) + '-' + d, me._disableDays) != -1 || new Date() > date) {
                    return [false];
                }
            }
            if (disableWeekdays && disableWeekdays.length > 0) {
                if ($.inArray(weekday, disableWeekdays) != -1 || new Date() > date) {
                    return [false];
                }
            }

            return [true];
        },
        "render": function(dateAvailable, extOption, disabled) {
            var me = this;
            var $me = me.$element;

            // Extend
            if (extOption && extOption.minDate) {
                dateAvailable = extOption.minDate;
            }

            // Initialize start date from today
            me.publish("deliveryStartDate/change", {
                // Date info
                "availableStartDate": dateAvailable,
                "selectedDate": dateAvailable
            });

            // Prepare for rendering
            var dataOption = {
                "dateFrom": INS_TIME.getStrDate(dateAvailable, "-"),
                "disabled": disabled,
                "language": context.language,
                "range": me.range
            };

            var dateTo;
            var weekDaysLens = me._arrDays.length || 0;
            var arrLastWeekDay = [];
            if (me.range) {
                if (weekDaysLens > 0) {
                    arrLastWeekDay.push(me._arrDays[weekDaysLens - 1]);
                }
                dateTo = INS_TIME.getNearestWeekdayDate(INS_TIME.addMonth(dateAvailable, me.rangeMonth),
                    arrLastWeekDay);
                _.extend(dataOption, {
                    "dateTo": INS_TIME.getStrDate(dateTo, "-")
                });

                // Initialize end date from today
                me.publish("deliveryEndDate/change", {
                    // Date info
                    "selectedDate": dateTo
                });
            }

            // Rendering
            me.html(template, dataOption).then(function() {
                // Generate datepicker by jQuery-UI
                $(document.body).addClass("jquery-ui-lightness");
                // Localize
                $.datepicker.setDefaults($.datepicker.regional[geti18n()]);
                // datepicker options
                var datepickerOptionFrom = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+3m",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryStartDate/change", {
                            // Date info
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "availableStartDate": dateAvailable,
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                var datepickerOptionTo = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+1y",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryEndDate/change", {
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                if (extOption) {
                    datepickerOptionFrom = _.extend(datepickerOptionFrom, extOption);
                    datepickerOptionTo = _.extend(datepickerOptionTo, extOption);
                }

                var $datepickerFrom = $me.find("[data-action=datepickerfrom]");
                var $datepickerTo;

                if (me.range) {
                    $datepickerTo = $me.find("[data-action=datepickerto]");

                    // Extend datepicker options
                    $.extend(datepickerOptionFrom, {
                        "beforeShowDay": function(date) {
                            // Only first active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.rest(me._arrDays)) : me.offDays(date);
                        },
                        "onSelect": function(strDate, inst) {
                            me.publish("deliveryStartDate/change", {
                                // Date info
                                // All date format to "xxxx-xx-xx 00:00:00"
                                "availableStartDate": dateAvailable,
                                "selectedDate": INS_TIME.getDateByString(strDate)
                            }).then(function() {
                                // Focus datepicker to element
                                $datepickerTo.focus();
                            });
                        },
                        "onClose": function(selectedDate) {
                            $datepickerTo.datepicker("option", "minDate", selectedDate);
                        }
                    });
                    $.extend(datepickerOptionTo, {
                        "beforeShowDay": function(date) {
                            // Only last active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.initial(me._arrDays)) : me.offDays(date);
                        },
                        "onClose": function(selectedDate) {
                            $datepickerFrom.datepicker("option", "maxDate", selectedDate);
                        }
                    });

                    // Datepicker To
                    $datepickerTo.datepicker(datepickerOptionTo);
                }

                // Datepicker From
                $datepickerFrom.datepicker(datepickerOptionFrom);
            });
        },
        // eventCode[string]
        // days[int]
        // wdays[array]
        "process": function(hourAhead, eventCode, days, wdays) {
            var me = this;

            var dateAvailable;
            var dateEvent;
            var dateEventEnd;
            var dateNow = INS_TIME.getZeroDate(new Date());

            days = days || 1; // 1 day cleanse

            // Forbit date before Holiday while multi days
            me.preForbidDate(days);

            // Forbit days in week
            if (wdays && wdays.length > 0) {
                me.preForbidWeekDays(wdays);
            }

            // Available first date for order
            // First weekdays is a special requirement from subscription
            var firstWeekday = [];
            if (wdays && wdays.length > 0) {
                firstWeekday.push(wdays[0]);
            }
            dateAvailable = me.getAvailableStartDate(hourAhead, days, firstWeekday);

            // Switch package
            switch (eventCode.toUpperCase()) {
                case "CFAC": // Clease for a Cause
                    dateEvent = new Date(2014, 7, 30);
                    dateEventEnd = new Date(2014, 7, 30);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, true);
                    }
                    break;
                case "WARM": // Warming Day
                    dateEvent = new Date(2014, 11, 16);
                    dateEventEnd = new Date(2015, 0, 4);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, false);
                    }
                    break;
                case "VALE": // Valentine's Day
                    dateEvent = new Date(2015, 1, 11);
                    dateEventEnd = new Date(2015, 1, 18);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "WOMN": // Women's Day
                    dateEvent = new Date(2015, 2, 3);
                    dateEventEnd = new Date(2015, 2, 9);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "BEAR": // Moonbear
                    dateEvent = new Date(2014, 9, 10);
                    if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        });
                    }
                    break;
                case "BRDL": // Bridal
                    if (dateAvailable.getDay() !== 1) {
                        dateEvent = INS_TIME.addDay(dateAvailable, 8 - dateAvailable.getDay());
                    } else {
                        dateEvent = dateAvailable;
                    }
                    // Rendering
                    me.render(dateAvailable, {
                        "minDate": dateEvent,
                        "beforeShowDay": function(date) {
                            return offDays(date, [0, 2, 3, 4, 5, 6]);
                        }
                    });
                    break;
                default:
                    // Rendering
                    me.render(dateAvailable);
                    return;
            }
        },
        // Special/Event order listner
        /*"hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath);

            var eventCode = hashMap.event || "";
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.wdays === wdays &&
                me.hourAhead === hourAhead
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.wdays = wdays;
            me.hourAhead = hourAhead;

            var arrPreDays;
            var arrDays = [];

            if (wdays) {
                arrPreDays = wdays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(hourAhead, eventCode, days, arrDays);
        },
        */
        "hub:memory/startdate/set": function(dateOption) {
            var me = this;

            if (!dateOption) {
                return;
            }

            var eventCode = dateOption.eventCode || "";
            var days = parseInt(dateOption.days, 10) || 1;
            var weekDays = (dateOption.weekDays || "").toString();
            var hourAhead = dateOption.hourAhead || HOUR_AHEAD;
            var holiday = dateOption.holiday || HOLIDAY;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.weekDays === weekDays &&
                me.hourAhead === hourAhead &&
                me._holiday.length == holiday.length &&
                _.difference(holiday, me._holiday).length <= 0
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.weekDays = weekDays;
            me.hourAhead = hourAhead;
            me._holiday = holiday;

            var arrPreDays;
            var arrDays = [];

            if (weekDays) {
                arrPreDays = weekDays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(me.hourAhead, me.eventCode, me.days, me._arrDays);

            return when.resolve();
        }
    });
});

define('troopjs-requirejs/template!widget/buy/summary.html',[],function() { return function template(data) { var o = "<div class=\"statistic fr\">\r\n"; if (data.preferentialProduct) { o += "\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1033)\">Product:</span>\r\n\t\t<em class=\"number-list-price price\" >&yen;" +data.product+ "</em>\r\n\t</div>\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1237)\">Product(Preferential):</span>\r\n\t\t<em class=\"price number-price value\">&yen;" +data.preferentialProduct+ "</em>\r\n\t</div>\r\n"; } else { o += "\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1033)\">Product:</span>\r\n\t\t<em class=\"number-price price value\" >&yen;" +data.product+ "</em>\r\n\t</div>\r\n"; } o += "\r\n"; if (data.preferentialDelivery) { o += "\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1034)\">Delivery:</span>\r\n\t\t<em class=\"number-list-price price\" >&yen;" +data.delivery+ "</em>\r\n\t</div>\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1238)\">Delivery(Preferential):</span>\r\n\t\t<em class=\"price number-price value\">&yen;" +data.preferentialDelivery+ "</em>\r\n\t</div>\r\n"; } else { o += "\r\n\t<div class=\"list\">\r\n\t\t<span data-weave=\"widget/blurb/main(1034)\">Delivery:</span>\r\n\t\t<em class=\"number-price price\" >&yen;" +data.delivery+ "</em>\r\n\t</div>\r\n"; } o += "\r\n</div>\r\n\r\n\r\n\r\n<div id=\"checkout-floatbar\" class=\"group fr\">\r\n\t<div class=\"ui-ceilinglamp checkout-buttons\">\r\n\t\t<div class=\"sticky-placeholder hide\" style=\"display: none;\">\r\n\t\t</div>\r\n\t\t<div class=\"sticky-wrap\">\r\n\t\t\t<div class=\"\">\r\n\t\t\t\t<span class=\"total\" ><span data-weave=\"widget/blurb/main(1040)\">Total:</span><strong id=\"payPriceId\">&yen;" +data.total+ "</strong>\r\n\t\t\t\t\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<a href=\"order_topay.html\"  class=\"btn checkout-submit\" data-action=\"submitorder\"  data-weave=\"widget/blurb/main(1610)\">订单提交</a>"; return o; }; });
define('widget/buy/summary',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./summary.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, number, ajaxQuery, api, pageUrl, template) {
    "use strict";

    var URI_ORDERDETAIL = api.get("orderdetail");
    var URL_SUMMARY = api.get("summary");
    
    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.orderId = orderId;
    }, {
        "render": function(dataOrder) {
            var me = this;
            var $me = me.$element;

            /*
            TotalPrice
            PreferentialPrice
            DeliverFee
            PreferentialLogisticsFee
            Total
             */
            var renderData = {
                    "orderId": "",
                    "product": number.getDecimal(dataOrder.TotalPrice),
                    "delivery": number.getDecimal(dataOrder.DeliverFee),
                    "total": number.getDecimal(dataOrder.Total)
                };
            
//            var renderData = {
//                "orderId": dataOrder.OrderId,
//                "product": number.getDecimal(dataOrder.TotalPrice),
//                "delivery": number.getDecimal(dataOrder.DeliverFee),
//                "total": number.getDecimal(dataOrder.PreferentialPrice + dataOrder.PreferentialLogisticsFee)
//            };

            // Append preferential logic
            if (dataOrder.TotalPrice !== dataOrder.PreferentialPrice) {
                _.extend(renderData, {
                    "preferentialProduct": number.getDecimal(dataOrder.PreferentialPrice)
                });
            }
            if (dataOrder.DeliverFee !== dataOrder.PreferentialLogisticsFee) {
                _.extend(renderData, {
                    "preferentialDelivery": number.getDecimal(dataOrder.PreferentialLogisticsFee)
                });
            }

            me.html(template, renderData);
        },
        //老的第二步提交放在了这里
        "queryOrder": function() {
            var me = this;
            var $me = me.$element;

            var orderId = me.orderId;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var order = data.Order;

                if (order.OrderStatusCode === -10){
                     pageUrl.goto("/payment/topay", "orderid/" + order.OrderID);
                     return;
                }
                else if (order.OrderStatusCode > 0){
                    pageUrl.goto("/order/success", "orderid/" + order.OrderID, {
                        "orderid": order.OrderID
                    });
                    return;
                }

                // Rendering
                me.render(order);
                // Publish order detail
                me.publish("order/orderdetail", order);
            });
        },
        "sig/start": function() {
            var me = this;
            //me.queryOrder();
        },
        // 刷新统计数据
        "hub/order/summary":function(summaryData){
        	var me = this;
        	var $me = me.$element;
        	
        	 ajaxQuery({
                 url: URL_SUMMARY,
                 data: JSON.stringify(summaryData),
                 contentType: "application/json; charset=UTF-8",
                 type: "POST",
                 dataType: "json"
             }, $me).then(function(data) {
            	 var ruleResult = {
                       StatusCodes: data.StatusCodes,
                       RuleId: data.RuleId
                   };
            	 me.publish("order/promotion/error", ruleResult);
            	 me.render(data);
             },function(data){
            	 
            	 
            	 //me.render(data);
             });
        },
        "hub/order/promotion": function(data) {
            var me = this;
            if (data) {
                me.render(data);
            } else {
                me.queryOrder();
            }
        }
    });
});
define('widget/carousel/main',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";
    
    var CAROUSEL_INTERVAL = 5000;

    return Widget.extend(function(){
        var me = this;
    },{
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            $me.carousel({
              interval: CAROUSEL_INTERVAL
            });
        }
    });
});
define('widget/querystring/main',["jquery",
    "when"
], function($, when) {
    "use strict";

    return {
        // Get Request from URL, retrun an object {query1: value1, query2: value2}
        getRequest: function() {
            var url = location.search;
            var theRequest = {};
            if (url.indexOf("?") !== -1) {
                var str = url.substr(1);
                var strs = str.split("&");
                for (var i = 0, l = strs.length; i < l; i++) {
                    theRequest[(strs[i].split("=")[0]).toLowerCase()] = unescape(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
    };
});

define('troopjs-requirejs/template!widget/certificate/index.html',[],function() { return function template(data) { var o = "";
    var cfc = data.cfc;
    var cacheServer = data.cacheServer;
    var isMobile = data.isMobile;
o += "\r\n<img src=\"" +(cacheServer + cfc)+ "\" "; if (isMobile) { o += "width=\"100%\""; } else { o += "width=\"680\" height=\"862\" style=\"border: 1px solid #000;\""; } o += " alt=\"@维果清VCLEANSE #爱动物，喝小绿# 我已经购买了维果清限量版爱心套餐，捐献了100元用于流浪动物救助，爱动物的你们快来加入我们吧！http://www.vcleanse.com/paws\" class=\"BSHARE_IMAGE_ALL\">"; return o; }; });
define('widget/certificate/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/pageurl/main",
    "widget/querystring/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, pageUrl, queryString, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Load order detail, so ignore order step
            var request = queryString.getRequest();
            if(!request){
                pageUrl.goto("/");
            }

            // Rendering
            me.html(template, {
                "cfc": "/paws/image?orderid=" + request.orderid,
                "cacheServer": context.cacheServer,
                "isMobile": context.isMobile
            }).then(function(){
                
            });
        }
    });
});

define('troopjs-requirejs/template!widget/checklist/index.html',[],function() { return function template(data) { var o = "";
    var order = data;
o += "\r\n<div class=\"checklist\">\r\n    <h5><strong data-weave=\"widget/blurb/main(1060)\">Order details</strong></h5>\r\n    <p>\r\n        <span data-weave=\"widget/blurb/main(1061)\">Order number: </span>\r\n        <span class=\"text-striking\"><strong>" +order.OrderID+ "</strong></span>\r\n    </p>\r\n    <p>\r\n        <span data-weave=\"widget/blurb/main(1235)\">Total price: </span>\r\n        <span class=\"text-striking number-price\"><strong>&yen;" +order.Subtotal+ "</strong></span>\r\n    </p>\r\n    <p class=\"\">\r\n        <span class=\"\" data-weave=\"widget/blurb/main(1236)\">Payment method: </span>\r\n        <span>\r\n            "; renderOrderPayments(order.Id, order.OrderPayments); o += "\r\n        </span>\r\n    </p>\r\n    "; if (order.InvoiceInfo.InvoiceType) { o += "\r\n        <div class=\"line-hor\"></div>\r\n        <p>\r\n            <span data-weave=\"widget/blurb/main(1038)\">Payment amount:</span>\r\n            <span class=\"text-striking\">" +order.InvoiceInfo.Amount+ "</span>\r\n        </p>\r\n        <p>\r\n            <span data-weave=\"widget/blurb/main(1039)\">Payment title:</span>\r\n            <span class=\"text-striking\">" +order.InvoiceInfo.Title+ "</span>\r\n        </p>\r\n    "; } o += "\r\n    ";
        (function(){
            var i = 0;
            var iLens = order.DeliverList.length;
            for(i = 0; i < iLens; i++){
                renderDailyOrder(order.DeliverList[i]);
            }
        })();
    o += "\r\n</div>\r\n\r\n"; function renderDailyOrder(dailyOrder) { o += "\r\n    <div class=\"checklist-gap\">\r\n        <p>\r\n            <strong>" +dailyOrder.DeliverDate+ ",</strong>\r\n            <span>" +dailyOrder.DeliverTime+ "</span>\r\n        </p>\r\n        ";
            (function(){
                var i = 0;
                var iLens = dailyOrder.ProductList.length;
                for(i = 0; i < iLens; i++){
                    renderDailyProduct(dailyOrder.ProductList[i]);
                }
            })();
        o += "\r\n        "; if(dailyOrder.IsSelfPickUp) { o += "\r\n            <p>\r\n                <strong data-weave=\"widget/blurb/main(1058)\">Pick up in store</strong>\r\n                <span>" +dailyOrder.StoreInfo.StoreName + "</span>\r\n            </p>\r\n            <p>\r\n                <strong data-weave=\"widget/blurb/main(1128)\">Store address:</strong>\r\n                <span>" +dailyOrder.StoreInfo.StoreAddress + "</span>\r\n                "; if (dailyOrder.StoreInfo.StoreMapUrl) { o += "\r\n                    [<a class=\"link-fn\" data-url=\"" +dailyOrder.StoreInfo.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n                "; } o += "\r\n            </p>\r\n        "; } else { o += "\r\n            <p><strong data-weave=\"widget/blurb/main(1066)\">To:</strong> " +dailyOrder.FirstName+ " " +dailyOrder.LastName+ ", " +dailyOrder.Address+ "</p>\r\n        "; } o += "\r\n        <p><strong data-weave=\"widget/blurb/main(1116)\">Tel:</strong> " +dailyOrder.Mobile+ "</p>\r\n        "; if(!dailyOrder.IsSelfPickUp && dailyOrder.TrackingNumber) { o += "\r\n            <p><strong data-weave=\"widget/blurb/main(1093)\">Tracking number:</strong> " +dailyOrder.TrackingNumber+ "</p>\r\n        "; } o += "\r\n    </div>\r\n"; } o += "\r\n"; function renderDailyProduct(dailyProduct) { o += "\r\n    <p>\r\n        <span title=\"&yen;" +dailyProduct.TotalPrice+ "\">\r\n            <strong>" +dailyProduct.Count+ "</strong> * " +dailyProduct.ProductName+ "\r\n        </span>\r\n    </p>\r\n"; } o += "\r\n\r\n";   function renderOrderPayments(orderId,payments){
        for(var i = 0; i< payments.length; i++) { 
            var obj = payments[i];
            var type = obj.PaymentType;
            var typeId = obj.PaymentTypeId;          
            var payment = obj.Payment;
            var isPaid = obj.IsPaid;
            var isOnline = obj.IsOnlinePay;
o += "\r\n                <strong>" +type+ "</strong>&nbsp;\r\n                <strong class=\"number-price\">&yen;" +payment+ "</strong>\r\n                <span>\r\n                    ";  if(isPaid===true){o += "\r\n                        <strong class=\"text-status-completed\" data-weave=\"widget/blurb/main(1619)\">已 付</strong>\r\n                    ";}else if(isPaid===false){o += "\r\n                        <strong class=\"text-status-unpaid\" data-weave=\"widget/blurb/main(1618)\">未 付</strong>\r\n                    ";}o += "\r\n                </span>\r\n"; }
    }
 return o; }; });
define('widget/checklist/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, number, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";

    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend({
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                // Format price
                data.Order.Subtotal = number.getDecimal(data.Order.PreferentialPrice + data.Order.PreferentialLogisticsFee);
                // Rendering
                me.html(template, data.Order);
                // Pulish OrderInfo
                me.publish("order/checklist", data.Order);
            });
        }
    });
});
define('widget/checkmyradio-listener/main',["troopjs-browser/component/widget",
	"jquery"
], function(Widget, $) {
	"use strict";

	return Widget.extend({
		"hub:memory/checkMyRadio/change": function(for_el_id) {
			var me = this;
			var $me = me.$element;

			var elMe = $me[0];
			if (elMe.id !== for_el_id) {
				return;
			}

			// Auto check input(radio button)
			if ($me.hasClass("icheck")) {
				$me.iCheck('check');
			} else {
				elMe.checked = true;
			}
		}
	});
});
define('widget/checkmyradio/main',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "dom/click": function(e) {
            e.preventDefault();
            var me = this;

            var obj_ID = me.$element.data("for");
            me.publish("checkMyRadio/change", obj_ID);
        }
    });
});

define('troopjs-requirejs/template!widget/cleanse/detail/index.html',[],function() { return function template(data) { var o = "";
    var language = data.language;
    var iPackage = data.package;
    var defaultGroupId = data.defaultGroupId;
    var byWeek = data.byWeek;

    var packageGroups = iPackage.Groups;
    var j = 0;
    var jLens = packageGroups.length || 0;
o += "\r\n<div class=\"detail-side\">\r\n    <div class=\"detail-product-image\">\r\n        <!--img data-weave=\"widget/basic/imageswitch\" width=\"332\" alt=\"\" src=\"/images/blank.png\" data-src=\"images/cleanse/detail/" +iPackage.ImageName+ "\"-->\r\n        <div data-weave=\"widget/cleanse/display/main\"></div>\r\n    </div>\r\n    "; if (byWeek) { o += "\r\n        <p data-weave=\"widget/blurb/main(p1710)\"></p>\r\n    "; } o += "\r\n</div>\r\n<div class=\"detail-main\">\r\n    <div class=\"detail-hd\">\r\n        <h1>" +iPackage.Title+ "</h1>\r\n        <p>" +iPackage.Introduction+ "</p>\r\n    </div>\r\n    <div role=\"tabpanel\" class=\"bwtab detail-groups\">\r\n      <ul class=\"nav nav-tabs\" role=\"tablist\">\r\n        ";
            for (j = 0; j < jLens; j++) {
        o += "\r\n            <li role=\"presentation\" class=\"";if(packageGroups[j].Id===defaultGroupId){o += "active";}o += "\">\r\n                <a href=\"#pkg-group-" +packageGroups[j].Id+ "\" aria-controls=\"home\" role=\"tab\" data-toggle=\"tab\" data-group-id=\"" +packageGroups[j].Id+ "\" data-action=\"reloaddisplay\">" +packageGroups[j].Name+ "</a>\r\n            </li>\r\n        "; } o += "\r\n      </ul>\r\n      <div class=\"tab-content\">\r\n        ";
            for (j = 0; j < jLens; j++) {
        o += "\r\n            <div role=\"tabpanel\" class=\"tab-pane ";if(packageGroups[j].Id===defaultGroupId){o += "active";}o += "\" id=\"pkg-group-" +packageGroups[j].Id+ "\">\r\n                ";
                renderProducts(packageGroups[j].Rules[0].Products, j);
                o += "\r\n            </div>\r\n        "; } o += "\r\n      </div>\r\n    </div>    \r\n    <div class=\"detail-action-ordernow detail-action-ordernow-cleanse\">\r\n        <a class=\"btn btn-lg btn-default\" href=\"/cleanse\" data-weave=\"widget/href/main widget/blurb/main(1220)\">\r\n            SHOPPING NOW\r\n        </a>\r\n    </div>\r\n</div>\r\n\r\n"; function renderProducts(products, suffix) { 
    var i = 0;
    var iLens = products.length || 0;
o += "\r\n    <div class=\"detail-bd cf\">\r\n        <ul class=\"nav nav-tabs\">\r\n            ";
                for (i = 0; i < iLens; i++) {
            o += "\r\n                <li data-action=\"producttab\" class=\""; if (i === 0) { o += "active"; } o += "\">\r\n                    <a href=\"#pkg-" +suffix+ "-" +i+ "-product-" +products[i].Id+ "\" data-toggle=\"tab\" data-url=\"" +products[i].Url+ "\" data-action=\"loadproduct\">\r\n                        " +(i+1)+ ".&nbsp;" +products[i].Name+ "\r\n                    </a>\r\n                </li>\r\n            "; } o += "\r\n        </ul>\r\n        <div class=\"tab-content\">\r\n            ";
                for (i = 0; i < iLens; i++) {
            o += "\r\n                <div class=\"tab-pane "; if (i === 0) { o += "active"; } o += "\" id=\"pkg-" +suffix+ "-" +i+ "-product-" +products[i].Id+ "\"></div>\r\n            "; } o += "\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/cleanse/detail/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "template!./index.html",
    "underscore"
], function(Widget, weave, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, template) {
    "use strict";

    var URI_PACKAGEBY_ID = api.get("package_loadbyid");

    // Get uniqle products
    function getUniqProducts(products) {
        var uniqProductsId = [];
        var uniqProducts = [];

        _.each(products, function(e) {
            if (uniqProductsId.indexOf(e.Id) >= 0) {
                return;
            }
            uniqProductsId.push(e.Id);
            uniqProducts.push(e);
        });

        return uniqProducts;
    }

    return Widget.extend({
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var packageId = parseInt(hashMap.packageid, 10);

            if (!packageId) {
                return;
            }

            // HARDCODE
            ajaxQuery({
                url: URI_PACKAGEBY_ID,
                data: {
                    packageId: packageId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var Packages = data.Packages;
                if (!Packages || Packages.length <= 0) {
                    return;
                }

                me.iPackage = Packages[0];
                if (!me.iPackage) {
                    return;
                }

                // Get default group (female first)
                var defaultGroup = me.iPackage.Groups[0];
                _.each(me.iPackage.Groups, function(e) {
                    // TODO: HARDCODE; male:1; female: 2
                    if (e.GroupTypeId === 2) {
                        defaultGroup = e;
                    }
                });

                // Render
                me.html(template, {
                    "package": me.iPackage,
                    "defaultGroupId": defaultGroup.Id,
                    "byWeek": defaultGroup.ByWeek,
                    "language": context.language
                }).then(function() {
                    // Publish current package data
                    me.publish("package/display", defaultGroup);
                    // Rendering
                    var $defaultProductTab = $me.find("[data-action=producttab].active > a");
                    me.renderProduct($defaultProductTab);
                });
            });
        },
        "renderProduct": function($els) {
            var me = this;
            var $me = me.$element;

            $els.each(function(i, e) {
                var $e = $(e);
                var productUrl = context.domain + "/juice/" + $e.data("url") + " #product-detail";
                var href = $e.attr("href");
                var $juiceCont = $me.find(href);

                $juiceCont.load(productUrl, function(responseText, status, xhr) {
                    // Weaving
                    weave.apply($juiceCont.find("[data-weave]"));
                });
            });
        },
        "dom:a[data-action=loadproduct]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            // Rendering
            me.renderProduct($e);
        },
        "dom:a[data-action=reloaddisplay]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            var groupId = parseInt($e.data("groupId"), 10) || 0;
            var selectGroup = me.iPackage.Groups[0];

            _.each(me.iPackage.Groups, function(e) {
                // TODO: HARDCODE; male:1; female: 2
                if (e.Id === groupId) {
                    selectGroup = e;
                }
            });

            // Publish current package data
            me.publish("package/display", selectGroup);
        }
    });
});

define('troopjs-requirejs/template!widget/cleanse/display/display.html',[],function() { return function template(data) { var o = "";
var PIC_URL = data.cacheServer + "/images/juice/33x130/";

var byWeek = data.byWeek;
var rules = data.rules;

var products = rules[0].Products;
var i = 0;
var iLens = products.length;
var product;
var count = 0;
o += "\r\n<ul class=\"hor float-abs juicebottle-list list-unstyled\">\r\n    ";
        for (i = 0; i < iLens; i++) { 
            product = products[i];
            count = product.Count;
            do {
    o += "\r\n        <li>\r\n            <div class=\"juicebottle\">\r\n                <img src=\"" +PIC_URL + product.Image+ "\" alt=\"" +product.Name+ "\" title=\"" +product.Name+ "\" width=\"33\" height=\"130\">\r\n            </div>\r\n        </li>\r\n    ";
                count--;
            } while (count > 0)
        }
    o += "\r\n</ul>"; return o; }; });
define('widget/cleanse/display/main',["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "template!./display.html"
], function(Widget, when, $, context, cookie, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "hub/package/display": function(packageGroup) {
            var me = this;

            // Rendering
            me.render(packageGroup);
        },
        "render": function(group) {
            var me = this;

            var rules = group.Rules;
            if (!rules || rules.length <= 0) {
                return;
            }

            // Rendering
            me.html(template, {
                "cacheServer": context.cacheServer,
                "byWeek": group.ByWeek,
                "rules": rules
            });
        }
    });
});

define('troopjs-requirejs/template!widget/cleanse/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var categories = data.categories;
    var col = data.col;

    var i = 0;
    var iLens = categories.length;

    var j = 0;
    var jLens = categories.length;
o += "\r\n<ul class=\"nav nav-tabs\" data-weave=\"widget/basic/bootstrap-tab\">\r\n    ";
        for(i = 0; i < iLens; i++){
            appendCategory(categories[i], i+1, iLens);
        }
    o += "\r\n</ul>\r\n<div class=\"tab-content\">\r\n    ";
        for(j = 0; j < jLens; j++){
            appendPackage(categories[j]);
        }
    o += "\r\n</div>\r\n<p class=\"cleanse-extinfo hide\" data-weave=\"widget/blurb/main(1467)\">* 每日25元运费另计</p>\r\n\r\n"; function appendCategory(category, sequence, lens) { o += "\r\n    <li class=\""; if (category.IsDefault) { o += "active"; } o += " "; if (sequence === lens) { o += "last"; } o += "\" style=\"width: " +Math.floor(100/lens)+ "%;\">\r\n        "; if (category.Id === 0) { o += "\r\n            <a href=\"#category_" + category.Id + "\" data-id=\"" + category.Id + "\" data-toggle=\"tab\" data-action=\"customize\">" + category.Name + "</a>\r\n        "; } else { o += "\r\n            <a href=\"#category_" + category.Id + "\" data-id=\"" + category.Id + "\" data-toggle=\"tab\">" + category.Name + "</a>\r\n        "; } o += "\r\n        "; if (false) { o += "\r\n            <span class=\"tag tag-category\">\r\n                <img src=\"" +cacheServer+ "/images/tags/tag_new.png\" alt=\"新产品\" title=\"新产品\">\r\n            </span>\r\n        "; } o += "\r\n    </li>\r\n"; } o += "\r\n\r\n"; function appendPackage(category) { o += "\r\n    <div class=\"tab-pane "; if (category.IsDefault) { o += "active"; } o += "\" id=\"category_" + category.Id + "\">\r\n        <div class=\"cleanse-about\">\r\n            " +(category.Description || "&nbsp;")+ "\r\n        </div>\r\n        "; if (category.Id === 0) { o += "\r\n            <div class=\"cleanse-cols inner cf\">\r\n                <div class=\"col-l page-width-l\">\r\n                    <div class=\"order\" data-weave=\"widget/order/main\"></div>\r\n                </div>\r\n                <div class=\"col-l page-width-m line-ver-page\" data-weave=\"widget/pageverticalline/main\"></div>\r\n                <div class=\"col-l page-width-r\" data-weave=\"widget/autofollower/main\">\r\n                    <div class=\"orderconfirm\" data-weave=\"widget/order/next/main\"></div>\r\n                </div>\r\n            </div>\r\n        "; } else { o += "\r\n            <div class=\"cleanse-cols cf\" data-weave=\"widget/cleanse/package/main(categoryId, col)\" data-category-id=\"" + category.Id + "\" data-col=\"" +col+ "\"></div>\r\n        "; } o += "\r\n    </div>\r\n"; }  return o; }; });
define('widget/cleanse/main',["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"widget/troophash/main",
	"widget/blurb/main",
	"template!./index.html"
], function(Widget, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, Blurb, template) {
	"use strict";

	var blurb = new Blurb($(document.body));
	var URI_PACKAGE_CATEGORY = api.get("package_category");

	// Own program, packageId = 0, groupId = 0
	var PACKAGE_CUSTOMIZE = {
		"Id": 0,
		"Name": blurb.get(1155),
		"Description": blurb.get(1352)
	};

	return Widget.extend(function($element, widgetName) {
        var me = this;

        me.preventRoute = false;
    }, {
		"hub:memory/route": function(uriPath, uriEvent) {
			var me = this;
			var $me = me.$element;

			var hashPath = uriPath.path || "category/2".split("/");
			var hashMap = troopHash.parse(hashPath);

			// prvrt: Prevent hash route event
			if (me.preventRoute) {
				return;
			}

			var categoryId = parseInt(hashMap.category, 10);
			var col = parseInt(hashMap.col, 10) || 1;

			ajaxQuery({
				url: URI_PACKAGE_CATEGORY,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				var categories = data.Categories;
				// Append customize package
				categories.push(PACKAGE_CUSTOMIZE);

				// Set default
				var i = 0;
				var iLens = categories.length;
				for (i = 0; i < iLens; i++) {
					if (categories[i].Id === categoryId) {
						categories[i].IsDefault = true;
					}
				}

				// Render
				me.html(template, {
					"cacheServer": context.cacheServer,
					"categories": categories,
					"col": col
				}).then(function() {});
			});
		},
		"dom:a[data-toggle=tab]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;
			var $el = $(e.currentTarget);

			var categoryId = $el.data("id");

			//return;
			me.preventRoute = true;
			troopHash.extend({
				"category": categoryId
			});
		},
		"dom:a[data-action=customize]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			// Has cookie
			/*
			cookie.get("memberid").then(function() {
				// Get User Account
				ajaxQuery({
					url: URI_ACCOUNT,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					// Do nothing
				}, function() {
					// Popup order-edit lightbox.
					me.publish("enrollment/signin/popup");
				});
			}, function() {
				// Popup order-edit lightbox.
				me.publish("enrollment/signin/popup");
			});*/
		}
	});
});

define('troopjs-requirejs/template!widget/cleanse/package/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer || "";
    var language = data.language;
    var col = data.col;
    var holidayPromotionPackages = data.holidayPromotionPackages || [];

    var PIC_PACKAGE_URL = cacheServer + "/images/cleanse/";
    var PIC_TAG_URL = cacheServer + "/images/tags/";
    var PIC_PACKAGE_DEFAULT_URL = cacheServer + "/images/cleanse/default.jpg";
    var COLUMN = 3;

    var packages = data.packages;
    var i = 0;
    var iLens = packages.length;
    var row = 0;
    var rows = Math.ceil(iLens / COLUMN);
    for (row = 0; row < rows; row++) {
        for(i = row * COLUMN; i < (row + 1) * COLUMN; i++){
            if(packages[i]){
                appendPackage(packages[i], i+1, (row + 1) * COLUMN);
            }
        }
    }
o += "\r\n<!-- package is a keyword? -->\r\n"; function appendPackage(pkg, sequence, lens) { o += "\r\n    <div class=\"cleanse-col "; if (sequence === lens) { o += "last"; } o += " "; if (sequence === col) { o += "active"; } o += "\" data-action=\"packagebox\" data-package-id=\"" +pkg.Id+ "\">\r\n        <div class=\"inner\">\r\n            <h2>\r\n                " +(pkg.Title || "&nbsp;")+ "\r\n                "; if (pkg.Tags && pkg.Tags.length > 0) {
                    var j = 0;
                    var jLens = pkg.Tags.length;
                    for (j = 0; j < jLens; j++) {
                o += "\r\n                    <span class=\"tag\">\r\n                        <img src=\"" +PIC_TAG_URL + pkg.Tags[j].ImagePath+ "\" alt=\"" +pkg.Tags[j].Name+ "\" title=\"" +pkg.Tags[j].Name+ "\">\r\n                    </span>\r\n                ";
                        }
                    }
                o += "\r\n            </h2>\r\n            <div class=\"cleanse-img\" data-action=\"showcleansedetail\">\r\n                "; if (pkg.ImageName) { o += "\r\n                    <img src=\"" + PIC_PACKAGE_URL + pkg.ImageName+ "\" alt=\"" +pkg.Title+ "\" title=\"" +pkg.Title+ "\">\r\n                "; } else { o += "\r\n                    <img src=\"" + PIC_PACKAGE_DEFAULT_URL+ "\" alt=\"" +pkg.Title+ "\" title=\"" +pkg.Title+ "\">\r\n                "; } o += "\r\n                <a href=\"/cleanse/detail\" data-hash=\"packageid/" +pkg.Id+ "\" data-weave=\"widget/href/main\" class=\"cleanse-img-cover\">\r\n                    <span data-weave=\"widget/blurb/main(1340)\">Details</span>\r\n                </a>\r\n            </div>\r\n            <div class=\"cleanse-intro\">\r\n                <div class=\"cleanse-intro-des\">\r\n                    " +(pkg.Description || "&nbsp;")+ "\r\n                </div>\r\n            </div>\r\n            <div class=\"btn-group option-days\" data-toggle=\"buttons\" data-action=\"package-days-container\">\r\n                <label class=\"btn btn-option\">\r\n                    <input type=\"radio\" name=\"package-days\" value=\"1\">1 <span data-weave=\"widget/blurb/main(1480)\"></span>\r\n                </label>\r\n                <label class=\"btn btn-option\">\r\n                    <input type=\"radio\" name=\"package-days\" value=\"3\">3 <span data-weave=\"widget/blurb/main(1288)\"></span>\r\n                </label>\r\n                <label class=\"btn btn-option active\">\r\n                    <input type=\"radio\" name=\"package-days\" value=\"5\">5 <span data-weave=\"widget/blurb/main(1288)\"></span>\r\n                    "; if (holidayPromotionPackages.indexOf(pkg.Id) >= 0) { o += "\r\n                        <span data-weave=\"widget/tips/holidaypromotion/main\"></span>\r\n                    "; } o += "\r\n                </label>\r\n            </div>\r\n            <hr>\r\n            <div class=\"btn-group option-gender\" data-toggle=\"buttons\" data-action=\"package-group-container\">\r\n                "; if (pkg.Groups && pkg.Groups.length === 1) { o += "\r\n                    <fieldset disabled>\r\n                        <label class=\"btn btn-option \">\r\n                            <input type=\"radio\" id=\"radio-package-" +pkg.Id+ "-" +sequence+ "-0\" name=\"radio-package-group\" value=\"" +pkg.Groups[0].Id+ "\" "; if(pkg.Groups[0].Selected) { o += "checked"; } o += ">\r\n                            <i class=\"fa female\" title=\"" +pkg.Groups[0].Name+ "\"></i>\r\n                        </label>\r\n                    </fieldset>\r\n                "; } else if (pkg.Groups && pkg.Groups.length > 1) { o += "\r\n                    "; for(var i=0, iLens=pkg.Groups.length; i<iLens; i++) { o += "\r\n                        <label class=\"btn btn-option "; if(pkg.Groups[i].Selected) { o += "active"; } o += "\">\r\n                            <input type=\"radio\" name=\"radio-package-group\" id=\"radio-package-" +pkg.Id+ "-" +sequence+ "-" +i+ "\" value=\"" +pkg.Groups[i].Id+ "\">\r\n                            " +pkg.Groups[i].Name+ "\r\n                        </label>\r\n                    "; } o += "\r\n                "; } else { o += "\r\n                    &nbsp;\r\n                "; } o += "\r\n            </div>\r\n            <hr class=\"\">\r\n            <div class=\"option-addon\">\r\n                <label for=\"package-addon1-" +pkg.Id+ "-" +sequence+ "\">\r\n                    <input type=\"checkbox\" data-id=\"22\" data-price=\"50\" id=\"package-addon1-" +pkg.Id+ "-" +sequence+ "\" class=\"icheck\" data-action=\"packageaddon\">\r\n                    <span data-weave=\"widget/blurb/main(1292)\">Fragrant Clear</span>\r\n                </label>\r\n            </div>\r\n            <hr class=\"\">\r\n            <div class=\"option-addon\">\r\n                <label for=\"package-addon2-" +pkg.Id+ "-" +sequence+ "\">\r\n                    <input type=\"checkbox\" data-id=\"20\" data-price=\"25\" id=\"package-addon2-" +pkg.Id+ "-" +sequence+ "\" class=\"icheck\" data-action=\"packageaddon\">\r\n                    <span data-weave=\"widget/blurb/main(1291)\">Spicy Lemonade</span>\r\n                </label>\r\n            </div>\r\n            <hr class=\"\">\r\n            <div class=\"option-addon\">\r\n                <label for=\"package-addon3-" +pkg.Id+ "-" +sequence+ "\">\r\n                    <input type=\"checkbox\" data-id=\"89\" data-price=\"50\" id=\"package-addon3-" +pkg.Id+ "-" +sequence+ "\" class=\"icheck\" data-action=\"packageaddon\">\r\n                    <span data-weave=\"widget/blurb/main(1482)\">Purple Broth</span>\r\n                </label>\r\n            </div>\r\n            <hr>\r\n            <h3 class=\"price\">\r\n                &yen;0.00\r\n            </h3>\r\n            <a class=\"btn btn-lg btn-default btn-block\" data-action=\"selectpackage\" data-weave=\"widget/blurb/main(1104)\">SELECT</a>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/cleanse/package/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/popupbox/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "json2"
], function(Widget, $, context, number, ajaxQuery, api, pageUrl, cookie, troopHash, PopupBox, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");
    var URI_PACKAGE = api.get("package");
    var URI_PACKAGE_BYCATEGORY = api.get("package_loadbycategory");
    var COOKIE_PKG_GROUP_PREFIX = "pkg_group_";

    // 2  - 冷压纤体
    // 3  - 冷压经典
    // 9  - 全绿系列
    // 19 - 暖体经典 V2.0
    // 20 - 暖体纤体 V2.0
    // 23 - 暖体大师清体
    // 25 - 闪电无姜
    var HOLIDAY_PROMOTION_PACKAGES = []; //[2, 3, 9, 19, 20, 23, 25];

    function getPackageGroup(dataPackage, packageId, groupId) {
        // Get package by packageId & groupId
        var dataGroups = [];
        var dataGroup;
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                dataGroups = dataPackage[i].Groups;
                if (dataGroup) {
                    break;
                }
                for (var j = 0, jLens = dataGroups.length; j < jLens; j++) {
                    if (dataGroups[j].Id.toString() === groupId.toString()) {
                        dataGroup = dataGroups[j];
                        break;
                    }
                }
            }
        }
        return dataGroup;
    }

    function getPrice(packageGroup, days) {
        var rule = packageGroup.Rules;
        if (!rule || rule.length <= 0) {
            return;
        }

        var iRule = rule[0];
        return iRule.PackagePrice * days;
    }

    return Widget.extend(function($element, widgetName, categoryId, col) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
        me.col = col || 1;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            if (!me.categoryId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE_BYCATEGORY,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = data.Packages;
                me.render(packages);
            });
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath);

            var packageId = parseInt(hashMap.packageid, 10);

            if (!packageId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = data.Packages;

                var i = 0;
                var iLens = packages.length;
                var currentPackages = [];

                for (i = 0; i < iLens; i++) {
                    if (packages[i].Id === packageId) {
                        currentPackages.push(packages[i]);
                        break;
                    }
                }

                me.render(currentPackages);
            });
        },
        "render": function(packages) {
            var me = this;
            var $me = me.$element;

            // Initialize packages (default group)
            me.packages = me.processingPackage(packages);
            // Render
            me.html(template, {
                "packages": me.packages,
                "cacheServer": context.cacheServer,
                "language": context.language,
                "col": me.col,
                "holidayPromotionPackages": HOLIDAY_PROMOTION_PACKAGES
            }).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
                // Update price
                $me.find("[data-action=packagebox]").each(function(i, e) {
                    var $e = $(e);
                    me.updatePrice($e);
                });
                // Publish packages data
                me.publish("package/data", me.packages);
            });
        },
        "processingPackage": function(packages) {
            var me = this;

            var i = 0;
            var iLens = packages.length;
            var j = 0;
            var jLens;
            var groups;
            var cookie_pkg_group_id;

            for (i = 0; i < iLens; i++) {
                groups = packages[i].Groups;
                if (!groups) {
                    break;
                }
                jLens = groups.length;
                cookie_pkg_group_id = cookie.getVal(COOKIE_PKG_GROUP_PREFIX + packages[i].Id) || 0;
                // Select default from cookie, or "0"
                if (cookie_pkg_group_id) {
                    for (j = 0; j < jLens; j++) {
                        if (groups[j].Id === parseInt(cookie_pkg_group_id, 10)) {
                            // Set default
                            groups[j].Selected = true;
                            // Save default
                            packages[i].defaultGroupId = groups[j].Id;
                            break;
                        }
                    }
                }
                // Set default while no default choosen
                // Female as Default
                // TODO: move this logic to backend
                if (!packages[i].defaultGroupId) {
                    // Set default
                    groups[1].Selected = true;
                    // Save default
                    packages[i].defaultGroupId = groups[1].Id;
                }
            }
            return packages;
        },
        "dom:a[data-action=selectpackage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            function fnNext() {
                var packageId = parseInt($packageContainer.data("packageId"), 10);
                var groupId = parseInt($packageContainer.find("[data-action=package-group-container] > label.active > input").val(), 10) || 0;
                var days = parseInt($packageContainer.find("[data-action=package-days-container] > label.active > input").val(), 10) || 5;

                // Append addon
                var addOn = "";
                $packageContainer.find("[data-action=packageaddon]").each(function(i, e) {
                    var $e = $(e);
                    if (e.checked) {
                        addOn += (addOn !== "" ? "," : "") + ($e.data("id") || 0);
                    }
                });

                var hash = "packageid/" + packageId +
                    "/groupid/" + groupId +
                    "/days/" + days +
                    "/addon/" + addOn;

                // TODO: Hardcode - Holiday promotion
                if (HOLIDAY_PROMOTION_PACKAGES.indexOf(packageId) >= 0 && days === 5) {
                    hash += "/event/holi";
                }

                pageUrl.goto("/order/receiver", hash);
            }

            fnNext();

            return;
        },
        "dom:input[name=radio-package-group]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            var packageId = $packageContainer.data("packageId");
            var groupId = $e.val();
            if (!groupId) {
                return;
            }

            me.updatePrice($packageContainer, groupId);
            // Publish to display widget
            me.publish("package/display", packageId, groupId);
            // Save current group to cookie
            cookie.set(COOKIE_PKG_GROUP_PREFIX + packageId, groupId);
        },
        "dom:input[name=package-days]/change": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");
            var days = parseInt($e.val(), 10);

            me.updatePrice($packageContainer, undefined, days);
        },
        "dom:[data-action=packageaddon]/ifChanged": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagebox]");

            me.updatePrice($packageContainer);
        },
        "updatePrice": function($packageContainer, groupId, days) {
            var me = this;

            var packageId = $packageContainer.data("packageId");
            if (!groupId) {
                groupId = $packageContainer.find("[data-action=package-group-container] > label.active > input").val() || 0;
            }
            if (!days) {
                days = $packageContainer.find("[data-action=package-days-container] > label.active > input").val() || 5;
            }

            var addOnPrice = 0;
            $packageContainer.find("[data-action=packageaddon]").each(function(i, e) {
                var $e = $(e);

                if (e.checked) {
                    addOnPrice += parseInt($e.data("price"), 10) || 0;
                }
            });

            var packageGroup = getPackageGroup(me.packages, packageId, groupId);
            var packagePrice = getPrice(packageGroup, days);
            var amount = number.getDecimal(packagePrice + addOnPrice * days);

            // DOM
            $packageContainer.find(".price").html("&yen;" + amount);
        },
        "dom:[data-action=packagebox]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $packageContainer = $(e.currentTarget);

            $packageContainer.siblings().removeClass("active");
            $packageContainer.addClass("active");
        },
        "dom:[data-action=showcleansedetail]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeIn(200);
        },
        "dom:[data-action=showcleansedetail]/mouseleave": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeOut(200);
        }
    });
});

define('troopjs-requirejs/template!widget/customize/dailyorderproducts/index.html',[],function() { return function template(data) { var o = "";
    var dailyProducts = data.dailyProducts;
    var i = 0;
    var iLens = 0;

    if(dailyProducts && dailyProducts.length > 0) { 
        iLens = dailyProducts.length;
o += "\r\n            <table class=\"table table-bordered table-striped table-hover table-condensed\">\r\n                <thead>\r\n                    <tr>\r\n                        <th colspan=\"3\" data-weave=\"widget/blurb/main(1608)\">You have to order:</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    "; for(i = 0; i < iLens; i++) { o += "\r\n                        <tr>\r\n                            <td width=\"20%\">" +(i+1)+ "</td>\r\n                            <td>" +dailyProducts[i].Name+ "</td>\r\n                            <td width=\"20%\">&times; " +dailyProducts[i].Count+ "</td>\r\n                        </tr>\r\n                    "; } o += "\r\n                </tbody>\r\n            </table>\r\n"; }  return o; }; });
define('widget/customize/dailyorderproducts/main',["troopjs-browser/component/widget",
	"jquery",
	"widget/ajaxquery/main",
	"template!./index.html"
], function(Widget, $, ajaxQuery, template) {
	"use strict";

	return Widget.extend(function($element, widgetName) {}, {
		"hub:memory/dailyOrderProducts/get": function(dailyProducts) {
			var me = this;

			// Render
			me.html(template, {
				"dailyProducts": dailyProducts
			}).then(function() {});
		}
	});
});

define('troopjs-requirejs/template!widget/customize/dailysoupdrink/index.html',[],function() { return function template(data) { var o = "";
	var dayId = data.dayId;
o += "\r\n\r\n<img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch(byLanguage)\" data-by-language=\"true\" data-src=\"images/juice/dailysoup/" +dayId+ ".jpg\" alt=\"每日例汤、特饮\" height=\"126\">"; return o; }; });
define('widget/customize/dailysoupdrink/main',["troopjs-browser/component/widget",
	"jquery",
	"template!./index.html"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend(function($element, widgetName, dayId) {
		var me = this;
		me.dayId = dayId;
	}, {
		"sig/start": function() {
			var me = this;

			// Render
			me.html(template, {
				"dayId": me.dayId
			}).then(function() {});
		}
	});
});

define('troopjs-requirejs/template!widget/customize/index.html',[],function() { return function template(data) { var o = ""; 
    var dailyOrder = data.dailyOrder;
    var dayId = data.dayId || 0;
    var weekName = data.weekName || "";
o += "\r\n<div class=\"linebox linebox-xlarge orderedit\">\r\n    <h5>\r\n        <strong>\r\n            " +dailyOrder.stringDate+ "&nbsp;&nbsp;<em>(" +weekName+ ")</em>\r\n        </strong>\r\n    </h5>\r\n    <div class=\"orderedit-shopping cf row\">\r\n        <div class=\"col-md-12\" data-weave=\"widget/customize/dailysoupdrink/main(dayId)\" data-day-id=\"" +dayId+ "\"></div>\r\n        <div class=\"col-md-12 orderedit-products\" data-weave=\"widget/customize/productcategory/main(dayId)\" data-day-id=\"" +dayId+ "\"></div>\r\n        <div class=\"col-md-5 \" data-weave=\"widget/customize/dailyorderproducts/main\"></div>\r\n        <div class=\"col-md-12 orderedit-delivery\">\r\n            <form class=\"standardform form-delivery cf\">\r\n                <div class=\"col-r\">\r\n                    <button class=\"btn btn-lg btn-default\" data-action=\"removeday\" data-weave=\"widget/blurb/main(1090)\">REMOVE THIS DAY FROM ORDER</button>\r\n                    <button class=\"btn btn-lg btn-default\" data-action=\"updateorder\" data-weave=\"widget/blurb/main(1091)\">UPDATE ORDER</button>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n    <div class=\"orderedit-products-cover hidden\"></div>\r\n</div>"; return o; }; });
define('widget/customize/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "widget/basic/cloneobject",
    "widget/popupbox/main",
    "widget/time/main",
    "template!./index.html"
], function(Widget, weave, when, $, cloneObject, PopupBox, Time, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    return Widget.extend(function() {
        var me = this;

        me.packageGroupDeferred = when.defer();
    }, {
        "setHeight": function($el) {
            var minHeight = 400;
            var viewHeight = window.innerHeight;
            var elHeight = viewHeight - 100;
            elHeight = elHeight <= minHeight ? minHeight : elHeight;
            // Set Customize box height
            $el.height(elHeight);
        },
        "hub/customize/popup": function(dailyOrder) {
            if (!dailyOrder) {
                return;
            }
            var me = this;

            // Run everything after make sure current order is from package or customize
            var activeDate = dailyOrder.Date;
            dailyOrder.stringDate = INS_TIME.getStrDate(activeDate, " / ");
            var dayId = activeDate.getDay();
            var $me = $(template({
                dailyOrder: dailyOrder,
                dayId: dayId,
                weekName: INS_TIME.getWeekName(dayId, false)
            }));

            // Set popup box height
            me.setHeight($me);

            // Save in memory
            me.$me = $me;
            me.dailyOrder = dailyOrder;
            me.date = activeDate;

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 10,
                // TODO:
                closeCallback: function() {
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open().then(function(){
                // Get products
                me.publish("orderProducts/get", dailyOrder.Products);
                me.publish("dailyOrderProducts/get", dailyOrder.Products);
            });
        },
        "hub/customize/resize": function(args) {
            var me = this;

            me.popupOrderEdit.centralize();
        },
        // Products change
        "hub/customize/changeProduct": function(args) {
            var me = this;

            // {
            //     id: xxx,
            //     count: xxx
            // }
            if (!args) {
                return;
            }
            var id = args.id;
            var name = args.name;
            var price = args.price;
            var count = args.count;

            var dailyProducts = me.dailyOrder.Products || [];
            var j = 0;
            var jLens = dailyProducts.length;

            var isNewProduct = true;

            // Already has products
            if (jLens) {
                for (j = 0; j < jLens; j++) {
                    if (dailyProducts[j].Id === id) {
                        isNewProduct = false;

                        // Update only when products count > 0
                        if (count > 0) {
                            dailyProducts[j].Count = count;
                        } else {
                            // Remove current product
                            dailyProducts.splice(j, 1);
                        }
                        break;
                    }
                }
                if (isNewProduct) {
                    // count should always > 0
                    dailyProducts.push({
                        "Id": id,
                        "Name": name,
                        "Price": price,
                        "Count": count
                    });
                }
            } else {
                // count should always > 0
                me.dailyOrder.Products = [];
                me.dailyOrder.Products.push({
                    "Id": id,
                    "Name": name,
                    "Price": price,
                    "Count": count
                });
            }
            me.publish("dailyOrderProducts/get", me.dailyOrder.Products);
        },
        // Remove current day from order
        "dom:[data-action=removeday]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Clear/Empty current order
            me.dailyOrder.Products = null;
            me.dailyOrder.PackageRuleId = null;
            me.dailyOrder.PackageProducts = null;

            // Submit order information on current day
            me.publish("order/dailyUpdate", me.dailyOrder);

            // Close popup
            me.popupOrderEdit.close();
        },
        // Submit data
        "dom:[data-action=updateorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            var dailyOrder = me.dailyOrder;
            // Submit order information on current day
            me.publish("order/dailyUpdate", me.dailyOrder);

            // Close popup
            me.popupOrderEdit.close();
        }
    });
});

define('troopjs-requirejs/template!widget/customize/numbercontrol/index.html',[],function() { return function template(data) { var o = "<div class=\"numbercontrol\">\r\n  "; if (!data.disable) { o += "\r\n    <a class=\"numbercontrol-plus hand\">\r\n        <span class=\"caret-5\"></span>\r\n    </a>\r\n    <a class=\"numbercontrol-minus hand\">\r\n        <span class=\"caret-5\"></span>\r\n    </a>\r\n  "; } o += "\r\n  <div class=\"numbercontrol-number\">" +data.count+ "</div>\r\n</div>"; return o; }; });
define('widget/customize/numbercontrol/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/infobox/main",
    "widget/blurb/main",
    "template!./index.html"
], function(Widget, $, InfoBox, Blurb, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    return Widget.extend(function($element, widgetName, id, name, count, countMax, price, disable) {
        var me = this;
        // get parameters
        me.id = id;
        me.name = name || "";
        me.count = count || 0;
        me.countMax = countMax || 0;
        me.price = price || 0;
        me.disable = disable || false;
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template, {
                count: me.count,
                disable: me.disable
            });
            me.$number = me.$element.find(".numbercontrol-number");
        },
        "changeProduct": function() {
            var me = this;
            // Maintain order
            me.publish("customize/changeProduct", {
                id: me.id,
                name: me.name,
                price: me.price,
                count: me.count
            });
        },
        "dom:.numbercontrol-plus/click": function(e) {
            e.preventDefault();
            var me = this;
            if(me.disable){
                return;
            }

            if (me.countMax > 0 && me.count >= me.countMax) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: blurb.get(1204, me.countMax),
                    overLap: "overlap"
                }).open();
                return;
            }
            me.count++;
            me.$number.text(me.count);
            // Maintain order
            me.changeProduct();
        },
        "dom:.numbercontrol-minus/click": function(e) {
            e.preventDefault();
            var me = this;
            if(me.disable){
                return;
            }

            if (me.count <= 0) {
                return;
            }
            me.count--;
            me.$number.text(me.count);
            // Maintain order
            me.changeProduct();
        }
    });
});

define('troopjs-requirejs/template!widget/customize/product/index.html',[],function() { return function template(data) { var o = "";
var MAX = 999;
var COLUMN = 5;
var PIC_URL = data.cacheServer + "/images/juice/25x100/";

var products = data.products;
// Products
var i = 0;
var iLens = products.length > MAX ? MAX : products.length;
// Row
var j = 0;
var jLens = Math.ceil(iLens/COLUMN);
// Rroducts in Row
var k = 0;
var kLens = 0;
o += "\r\n"; for (j = 0; j < jLens; j++) { o += "\r\n    <ul class=\"hor float-abs\">\r\n        ";
            k = COLUMN * j;
            kLens = COLUMN + k;
            kLens = kLens > iLens ? iLens : kLens;
            for(i = k; i < kLens; i++) {
                renderProduct(products[i]);
            }
        o += "\r\n    </ul>\r\n"; } o += "\r\n"; if (products.length > MAX) { o += "\r\n    <p class=\"orderedit-products-seemore\">\r\n        <a class=\"link-fn\" data-weave=\"widget/blurb/main(1096)\">+ See more</a>\r\n    </p>\r\n"; } o += "\r\n \r\n"; function renderProduct(product) { o += "\r\n    <li>\r\n        <div class=\"cf float-abs customize-products-single\" data-action=\"productcontainer\" data-id=\"" +product.Id+ "\">\r\n            <div class=\"col-l\" data-weave=\"widget/customize/numbercontrol/main(id, name, count, max, price, disable)\" data-count=\"" +(product.Count || 0)+ "\" data-max=\"" +product.MaxCount+ "\" data-id=\"" +product.Id+ "\" data-name=\"" +product.Name+ "\" data-price=\"" +product.Price+ "\" data-disable=\"" +!product.DayActive+ "\"></div>\r\n            <div class=\"juicebottle juicebottle-100 col-l\"><img src=\"" +PIC_URL + product.Image+ "\" width=\"25\" height=\"100\" alt=\"" +product.Name+ "\" title=\"" +product.Name+ "\"></div>\r\n            <div class=\"col-l orderedit-products-text\">\r\n                <p class=\"orderedit-products-name\"><strong>" +product.Name+ "</strong></p>\r\n                <p class=\"orderedit-products-price number-price\"><strong>&yen; " +product.Price+ "</strong></p>\r\n                <p>\r\n                    <a href=\"/juice/" +(product.Url || '')+ "\" data-weave=\"widget/href/main widget/blurb/main(1097)\" target=\"_blank\" class=\"\"></a>\r\n                </p>\r\n                "; if(!product.DayActive) { o += "\r\n                    <em data-weave=\"widget/blurb/main(1605)\">No supply</em>\r\n                "; } o += "\r\n            </div>\r\n        </div>\r\n    </li>\r\n"; }  return o; }; });
define('widget/customize/product/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, template) {
    "use strict";

    var URI_PRODUCT = api.get("product_loadbyday");

    return Widget.extend(function($element, widgetName, categoryId) {
        var me = this;

        me.deferred = when.defer();
        me.categoryId = categoryId;
    }, {
        "hub/allproducts/get": function(products) {
            var me = this;
            me.deferred.resolver.resolve(products);
        },
        "hub:memory/orderProducts/get": function(orderProducts) {
            var me = this;
            var $me = me.$element;

            me.deferred.promise.then(function(products) {
                me.render(products, orderProducts);
            });
        },
        "render": function(products, orderProducts) {
            var me = this;
            var $me = me.$element;

            var i = 0;
            var iLens;
            var j = 0;
            var jLens;
            var categoryProducts = [];

            jLens = products.length;
            for (j = 0; j < jLens; j++) {
                // From change order events
                // Append products count to products object
                if (orderProducts) {
                    iLens = orderProducts.length;
                    for (i = 0; i < iLens; i++) {
                        if (orderProducts[i].Id === products[j].Id) {
                            products[j].Count = orderProducts[i].Count;
                            break;
                        }
                    }
                }
                if (products[j].ProductTypeId === me.categoryId) {
                    categoryProducts.push(products[j]);
                }
            }

            me.html(template, {
                "cacheServer": context.cacheServer,
                "products": categoryProducts
            }).then(function() {
                // Resolve to tell order-edit centralize
                me.publish("customize/resize");
            });
        },
        "hub/customize/changeProduct": function(data) {
            var me = this;
            var $me = me.$element;

            if (!data) {
                return;
            }
            var productId = data.id;
            var count = data.count;

            var $product = $me.find("[data-action=productcontainer][data-id=" + productId + "]");

            if (count > 0) {
                $product.addClass("active");
            } else {
                $product.removeClass("active");
            }
        }
    });
});

define('troopjs-requirejs/template!widget/customize/productcategory/index.html',[],function() { return function template(data) { var o = "";
	var categories = data.categories;
	var dayId = data.dayId;

    var z = 0;
	if(dayId < 0){
        appendCategoryAll(categories);
	}
    else {
        appendCategoryTabs(categories);
    }
o += "\r\n\r\n"; function appendCategoryAll(categories) { 
    var i = 0;
    var iLens = categories.length;

    for(i = 0; i < iLens; i++) {
o += "\r\n\t\t<div class=\"orderedit-products-category\" data-weave=\"widget/customize/product/main(categoryId)\" data-category-id=\"" + categories[i].Id + "\"></div>\r\n"; }
} o += "\r\n\r\n"; function appendCategoryTabs(categories) { 
    var i = 0;
    var iLens = categories.length;
o += "\r\n\t<div class=\"cleanse-view juice-view\">\r\n\t    <ul class=\"nav nav-tabs\" id=\"myTab\" data-weave=\"widget/basic/bootstrap-tab\">\r\n\t        "; for(i = 0; i < iLens; i++) {o += "\r\n\t\t        <li class=\"";if(i===0){o += "active";}o += "\">\r\n\t\t        \t<a href=\"#target-" +i+ "\" data-toggle=\"tab\">" +categories[i].Name+ "</a>\r\n\t\t        </li>\r\n\t        ";}o += "\r\n\t    </ul>\r\n\t    <div class=\"tab-content\">\r\n\t        "; for(i = 0; i < iLens; i++) {o += "\r\n\t\t        <div class=\"tab-pane ";if(i===0){o += "active";}o += "\" id=\"target-" +i+ "\">\r\n\t\t\t\t\t<div class=\"orderedit-products-category\" data-weave=\"widget/customize/product/main(categoryId)\" data-category-id=\"" + categories[i].Id + "\"></div>\r\n\t\t        </div>\r\n\t        ";}o += "\r\n\t    </div>\r\n\t</div>\r\n"; }  return o; }; });
define('widget/customize/productcategory/main',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, pageUrl, cookie, template) {
	"use strict";

	var URI_PRODUCT_CATEGORY = api.get("product_category");
	var URI_PRODUCT = api.get("product_loadbyday");

	return Widget.extend(function($element, widgetName, dayId) {
		var me = this;

		dayId = parseInt(dayId, 10);
		if (isNaN(dayId)) {
			dayId = -1;
		}
		me.dayId = dayId === 0 ? 7 : dayId;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			when.join(ajaxQuery({
				url: URI_PRODUCT_CATEGORY,
				data: {
					"language": context.language
				},
				type: "GET",
				dataType: "json"
			}), ajaxQuery({
				url: URI_PRODUCT,
				data: {
					//categoryId: me.categoryId,
					// -1: load all products
					"dayId": me.dayId,
					"language": context.language
				},
				type: "GET",
				dataType: "json"
			})).then(function(data) {
				var categories = data[0].Categories;
				var products = data[1].Products;

				// Render
				me.html(template, {
					"dayId": me.dayId,
					"categories": categories
				}).then(function() {
					me.publish("allproducts/get", products);
                    			me.publish("orderProducts/get", products);
				});
			});
		}
	});
});

define('troopjs-requirejs/template!widget/delivery/deliverydate/index.html',[],function() { return function template(data) { var o = "";
	var deliveryStartDate = data.deliveryStartDate;
	var packageId = data.packageId;
o += "\r\n<!--h4 data-weave=\"widget/blurb/main(1347)\">Delivery Date</h4-->\r\n<br>\r\n<div class=\"the-form\">\r\n\t<div class=\"form-section no-border\">\r\n\t\t"; if (!deliveryStartDate) { o += "\r\n\t\t    <div class=\"form-group\" data-action=\"datePickerContainer\">\r\n\t\t\t\t<label class=\"control-label\" data-weave=\"widget/blurb/main(1008)\">\r\n\t\t\t\t\tSelect start date\r\n\t\t\t\t</label>\r\n\t\t\t\t<p data-weave=\"widget/blurb/main(1024)\">When would you like to start your cleanse?</p>\r\n\t\t\t\t<div class=\"calendar-date\" data-weave=\"widget/startdate/main\"></div>\r\n\t\t\t\t"; if (packageId === 31) { o += "\r\n\t\t\t\t\t<p style=\"color:red\">【夏日套餐】为限量套餐，仅供应至8月31日，若是超出此日期可换购“冷压经典套餐”，有任何问题可拨打4001-842-779进行咨询。</p>\r\n\t\t\t\t"; } o += "\r\n\t\t    </div>\r\n\t\t"; } o += "\r\n\t    <div class=\"form-group\" data-action=\"deliveryTimeContainer\">\r\n\t\t\t<label class=\"control-label\" data-weave=\"widget/blurb/main(1086)\">Preferred delivery window?</label>\r\n\t\t\t<p data-weave=\"widget/blurb/main(1200)\"></p>\r\n\t\t\t<div class=\"orderedit-delivery-content orderedit-delivery-time\" data-weave=\"widget/delivery/deliverytime/main\"></div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"; return o; }; });
define('widget/delivery/deliverydate/main',["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/time/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, when, $, context, cookie, troopHash, Time, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 12;

    //  Holiday configuration
    // Note: Month start from 0
    // Downtown
    var HOLIDAY_MAIN = [
		new Date(2016, 2, 1),
        new Date(2016, 2, 2),
        new Date(2016, 2, 3),
        new Date(2016, 2, 4),
       
    ];
    // Outtown
    var HOLIDAY_OUT_MAIN = [
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
        new Date(2016, 3, 3),
        new Date(2016, 3, 4),
    ];

    var VISIBLE_DATEPICKER = true;
    var VISIBLE_DELIVERYTIME = true;

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.deferred = when.defer();
    }, {
        "sig/start": function() {
            var me = this;
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            // Only Rendering for package, not for customize
            me.html(template, {
                "deliveryStartDate": deliveryStartDate
            }).then(function() {
                if (deliveryStartDate) {
                    // Publish pre-setted date
                    me.publish("deliveryStartDate/change", {
                        "selectedDate": deliveryStartDate
                    });
                }
            });
            */

            var deliveryStartDate = hashMap.deliveryStartDate ? new Date(hashMap.deliveryStartDate) : null;

            var eventCode = hashMap.event || "";
            var packageId = parseInt(hashMap.packageid, 10);
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Hide datepicker while deliveryStartDate exists
            VISIBLE_DATEPICKER = deliveryStartDate ? false : true;

            if (deliveryStartDate) {
                if (deliveryStartDate === me.deliveryStartDate) {
                    return;
                } else {
                    me.deliveryStartDate = deliveryStartDate;
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        // Preset Start Date
                        me.publish("deliveryStartDate/change", {
                            "selectedDate": deliveryStartDate
                        });

                        return when.resolve();
                    });
                }
            } else {
                if (me.rendered) {
                    // Reset startdate
                    me.publish("startdate/set", {
                        "eventCode": eventCode,
                        "days": days,
                        "weekDays": wdays
                    });
                } else {
                    // Rendering
                    me.deferred = me.html(template, {
                        "deliveryStartDate": deliveryStartDate,
                        "packageId": packageId
                    }).then(function() {
                        return when.resolve({
                            "eventCode": eventCode,
                            "days": days,
                            "weekDays": wdays
                        });
                    });
                }
            }
        },
        "visibleControl": function(binVisible) {
            var me = this;
            var $me = me.$element;

            var $deliveryTime = $me.find("[data-action=deliveryTimeContainer]");

            if (VISIBLE_DELIVERYTIME) {
                $me.show();
                $deliveryTime.show();
            } else if (VISIBLE_DATEPICKER) {
                $me.show();
                $deliveryTime.hide();
            } else {
                $me.hide();
                $deliveryTime.hide();
            }
        },
        "hub/expressCompany/reload": function(expressCompanyId) {
            var me = this;

            // 以下两个外送公司需要提前到早晨5点截单
            var hourAhead = HOUR_AHEAD;

            // 上海顺丰标准B - 8
            // 上海顺丰空运 - 9
            // 10  顺丰次晨
            // 12  顺丰次晨B
            if (_.indexOf([8, 9, 10, 12], expressCompanyId) >= 0) {
                hourAhead = 10;
            }

            // 上海、北京初五开始配送，其它地区3月1号开始配送
            var holiday = HOLIDAY_OUT_MAIN;

            // 黑猫 - 1
            // 上海顺丰即时达 - 3
            // 北京顺丰冷链 - 7
            if (_.indexOf([1, 3, 7], expressCompanyId) >= 0) {
                holiday = HOLIDAY_MAIN;
            }

            // Reset "Start Date" by Express Company
            /*troopHash.extend({
                "hour": hourAhead
            });*/
            // Reset startdate
            me.deferred.then(function(startDateConfig) {
                startDateConfig = _.extend(startDateConfig, {
                    "hourAhead": hourAhead,
                    "holiday": holiday
                });
                me.publish("startdate/set", startDateConfig);
            });
        },
        // Show/Hide
        "hub/order/showDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = true;
            me.visibleControl();
        },
        "hub/order/hideDeliveryTime": function() {
            var me = this;

            VISIBLE_DELIVERYTIME = false;
            me.visibleControl();
        }
    });
});

define('troopjs-requirejs/template!widget/delivery/deliverytime/index.html',[],function() { return function template(data) { var o = "";
    var j = 0;
    var deliverTime = data.deliverTime;
    var jLens = deliverTime.length;
o += "\r\n<div class=\"btn-group deliverytime\" data-toggle=\"buttons\" data-action=\"changedelivertime\">\r\n    ";
        for(j = 0; j < jLens; j++){
    o += "\r\n        <label class=\"btn btn-option ";if(deliverTime[j].Selected) { o += "active"; } o += "\">\r\n            <input type=\"radio\" name=\"deliverytime\" value=\"" +deliverTime[j].Id+ "\">" +deliverTime[j].Name+ "\r\n        </label>\r\n    "; } o += "\r\n</div>"; return o; }; });
define('widget/delivery/deliverytime/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, api, template) {
    "use strict";

    var URI_DELIVER_TIME = api.get("deliver_timebyarea");

    return Widget.extend(function($element, widgetName, deliverTimeId) {
        var me = this;

        me.DeliverTimeId = parseInt(deliverTimeId, 10) || 0;
    }, {
        "hub/deliverTime/reload": function(districtId) {
            var me = this;

            if (!districtId) {
                return;
            }

            // GET deliver time
            ajaxQuery({
                url: URI_DELIVER_TIME,
                type: "GET",
                data: {
                    "areaId": districtId,
                    "language": context.language
                },
                dataType: "json"
            }).then(function(dataDeliverTimes) {
                var deliverTime = me.deliverTime = dataDeliverTimes.Values;

                var i = 0;
                var iLens = deliverTime.length;
                var defaultDeliverTime;

                if (me.DeliverTimeId) {
                    for (i = 0; i < iLens; i++) {
                        if (deliverTime[i].Id === me.DeliverTimeId) {
                            deliverTime[i].Selected = true;
                            defaultDeliverTime = deliverTime[i];
                            break;
                        }
                    }

                    if (!defaultDeliverTime) {
                        deliverTime[0].Selected = true;
                        defaultDeliverTime = deliverTime[0];
                    }
                } else {
                    // Choose first one as default deliver time
                    deliverTime[0].Selected = true;
                    defaultDeliverTime = deliverTime[0];
                }
                // Rendering
                me.html(template, {
                    deliverTime: deliverTime
                }).then(function() {
                    // Set default value.
                    me.publish("receiver/changeDeliverTime", defaultDeliverTime);
                });
            });
        },
        // Change to another deliver time span
        "dom:[name=deliverytime]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $e = $(e.currentTarget);

            var selectDeliverTimeId = parseInt($e.val(), 10);

            var i = 0;
            var iLens = me.deliverTime.length;
            for (i = 0; i < iLens; i++) {
                if (parseInt(me.deliverTime[i].Id, 10) === selectDeliverTimeId) {
                    me.publish("receiver/changeDeliverTime", me.deliverTime[i]);
                    break;
                }
            }
        }
    });
});

define('troopjs-requirejs/template!widget/delivery/editaddress/index.html',[],function() { return function template(data) { var o = "<form id=\"editaddress\" class=\"standardform form-editaddress form-horizontal\">\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading orderedit-delivery-title\">\r\n            "; if (data) { o += "\r\n                <strong data-weave=\"widget/blurb/main(1073)\">Edit delivery address</strong>\r\n            "; } else { o += "\r\n                <strong data-weave=\"widget/blurb/main(1074)\">Add a delivery address</strong>\r\n            "; } o += "\r\n        </div>\r\n        <div class=\"panel-body\">\r\n            ";
                var address = data || {};
                renderAddress(address);
            o += "\r\n            <div class=\"text-center\">\r\n                <button type=\"submit\" class=\"btn btn-default\" data-action=\"updateaddress\" data-weave=\"widget/blurb/main(1115)\">CONFIRM</button>\r\n                <button class=\"btn btn-default\" data-action=\"cancelupdateaddress\" data-weave=\"widget/blurb/main(1075)\">CANCEL</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n"; function renderAddress(address) { o += "\r\n    <div class=\"form-deliveryaddress\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required value=\"" +(address.FirstName || "")+ "\" autofocus data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Contact Number\" required value=\"" +(address.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-account-address form-group\">\r\n            <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n            <div class=\"col-sm-8\">\r\n                <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"" +(address.ProvinceId||0)+ "\" data-city-id=\"" +(address.CityId||0)+ "\" data-area-id=\"" +(address.AreaId||0)+ "\"></div>\r\n                <br>\r\n                <input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Apartment, suite, unit, building, floor, etc.\" required value=\"" +(address.Line1 || "")+ "\" data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-zipcode\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-zipcode\" name=\"signup-zipcode\" placeholder=\"Zip code\" value=\"" +(address.ZipCode || "")+ "\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/delivery/editaddress/main',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"template!./index.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, template) {
	"use strict";

	var URI_RECEIVER = api.get("receiver");
	var URI_RECEIVER_ADD = api.get("receiver_create");
	var URI_RECEIVER_UPDATE = api.get("receiver_update");

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName, receiverId) {
		var me = this;

		me.receiverId = receiverId;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;
			var deferred = when.defer();

			if (me.receiverId) {
				ajaxQuery({
					url: URI_RECEIVER,
					type: "GET",
					dataType: "json"
				}).then(function(data) {
					var receivers = data.Receivers;
					var i = 0;
					var iLens = receivers.length;
					for (i = 0; i < iLens; i++) {
						if (receivers[i].Id === me.receiverId) {
							me.html(template, receivers[i]);
							break;
						}
					}
					deferred.resolve();
				});
			} else {
				me.html(template);
				deferred.resolve();
			}

			return deferred.promise;
		},
		"dom:[data-action=cancelupdateaddress]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			me.publish("receiver/refreshAddress");
		},
		"dom:form/submit": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			} else {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1623
				}).open();

				return;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: cityId,
				areaId: areaId,
				provinceId: $form.find("select[name=provincepicker]").val()
			};

			var URI_ADDRESS = URI_RECEIVER_ADD;
			if (me.receiverId) {
				URI_ADDRESS = URI_RECEIVER_UPDATE;
				$.extend(true, addressData, {
					id: me.receiverId
				});
			}

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return;
			}

			// Update / Add delivery address
			ajaxQuery({
				url: URI_ADDRESS,
				data: JSON.stringify(addressData),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}).then(function(data) {
				// Send update address Id
				me.publish("receiver/refreshAddress", me.receiverId || data.ReceiverID);
			}, function(e) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: getStatus(e)
				}).open();
			});
		}
	});
});

define('troopjs-requirejs/template!widget/delivery/index.html',[],function() { return function template(data) { var o = "<div data-weave=\"widget/delivery/order/main\"></div>\r\n<div class=\"receiver-box\">\r\n    <div class=\"pickuptype bwtab\">\r\n        <div data-action=\"changepickuptype-container\">\r\n            <ul class=\"nav nav-tabs\" role=\"tablist\" data-action=\"changepickuptype\">\r\n                <li class=\"active\" data-pickup=\"1\">\r\n                    <a href=\"#pickuptype-deliver\" data-toggle=\"tab\" data-weave=\"widget/blurb/main(1379)\">Select a shipping address</a>\r\n                </li>\r\n                <li data-pickup=\"2\" data-weave=\"widget/delivery/selfpickup/entry/main()\"></li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"tab-content\">\r\n            <div class=\"tab-pane active\" id=\"pickuptype-deliver\">\r\n                <h4 data-weave=\"widget/blurb/main(1346)\">Delivery Address</h4>\r\n                <div class=\"the-form\" data-action=\"deliveryaddresscontainer\"></div>\r\n            </div>\r\n            <div class=\"tab-pane\" id=\"pickuptyp-selfpickup\">\r\n                <h4 data-weave=\"widget/blurb/main(1027)\">Pick up in store</h4>\r\n                <div class=\"the-form\" data-weave=\"widget/delivery/selfpickup/main\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"pickuptime\" data-weave=\"widget/delivery/deliverydate/main\"></div>\r\n    <div class=\"err-msg-group\">\r\n        <p class=\"err-msg hid\" data-action=\"showErrorMsg-noReceiver\">\r\n            <span data-weave=\"widget/blurb/main(1234)\">Please at least add one receiver address.</span>\r\n        </p>\r\n    </div>\r\n</div>\r\n<div class=\"cf text-center orderaction-container\">\r\n    <a class=\"btn btn-lg btn-default btn-action\" data-action=\"submitorder\" data-weave=\"widget/blurb/main(1114)\">NEXT</a>\r\n</div>"; return o; }; });
define('widget/delivery/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "underscore",
    "json2"
], function(Widget, weave, when, $, context, cookie, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");

    // Time function
    var INS_TIME = new Time();

    var PRICE_DELIVERY = 25;

    function getIsSelfPickup($el) {
        // Hardcode pickup type
        // 1: Select a shipping address
        // 2: Pick up in store
        return $el.data("pickup").toString() === "2";
    }

    return Widget.extend(function() {
        var me = this;
    }, {
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("receiver/hasLogin");
                    }, function() {
                        //
                        me.publish("receiver/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("receiver/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;

            me.render();
        },
        "hub/order/warningNoReceiver": function() {
            var me = this;
            var $me = me.$element;

            var $el = $me.find("[data-action=showErrorMsg-noReceiver]");
            // Show
            $el.show().fadeIn(400);
            // Hide
            setTimeout(function() {
                $el.fadeOut(400, function() {
                    $el.hide();
                });
            }, 3 * 1000);
        },
        // Already login, is a member
        "hub/receiver/hasLogin": function() {
            var me = this;
            me.publish("receiver/refreshAddress");
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");

            $delivery.html("<div data-weave=\"widget/delivery/nonloginaddress/main()\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {});
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            $delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
                $("[data-action=submitorder]").show();
            });
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            var $delivery = $me.find("[data-action=deliveryaddresscontainer]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            $delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
                $("[data-action=submitorder]").hide();
            });
        },
        "hub/order/pickuptype": function() {
            var me = this;
            var $me = me.$element;

            var isSelfPickup = getIsSelfPickup($me.find("[data-action=changepickuptype] > li.active"));

            var promiseValue = [];
            promiseValue.push(isSelfPickup);
            return promiseValue;
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        "dom:a[data-action=signin]/click": function(e) {
            e.preventDefault();

            var me = this;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        },
        "dom:[data-action=changepickuptype] [data-toggle=tab]/click": function(e) {
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);

            var isSelfPickup = getIsSelfPickup($el.parent("li"));
            if (isSelfPickup) {
                me.publish("order/hideDeliveryTime");
            } else {
                me.publish("order/showDeliveryTime");
            }
        }
    });
});

define('troopjs-requirejs/template!widget/delivery/nonloginaddress/index.html',[],function() { return function template(data) { var o = "<form class=\"form-editaddress form-horizontal\">\r\n    <p data-weave=\"widget/blurb/main(1313)\">Sign in to your account</p>\r\n    ";
        var address = data || {};
        renderAddress(address);
    o += "\r\n</form>\r\n"; function renderAddress(address) { o += "\r\n    <div class=\"form-deliveryaddress\">\r\n       \r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required value=\"" +(address.FirstName || "")+ "\" autofocus data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Mobile Number\" required value=\"" +(address.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-account-address form-group\">\r\n            <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n            <div class=\"col-sm-8\">\r\n                <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"" +(address.ProvinceId||0)+ "\" data-city-id=\"" +(address.CityId||0)+ "\" data-area-id=\"" +(address.AreaId||0)+ "\"></div>\r\n                <br>\r\n                <input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Shipping address\" required value=\"" +(address.Line1 || "")+ "\" data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-zipcode\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" id=\"signup-zipcode\" name=\"signup-zipcode\" placeholder=\"Zip code\" value=\"" +(address.ZipCode || "")+ "\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/delivery/nonloginaddress/main',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"widget/basic/regex",
	"widget/enrollment/signin/main",
	"template!./index.html",
	"json2",
	"underscore.string"
], function(Widget, $, when, context, ajaxQuery, api, InfoBox, regex, Signin, template) {
	"use strict";

	(new Signin($(document.body))).start();

	function getStatus(code) {
		var status = [];
		status["11"] = "1278";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName) {
		var me = this;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				// Popup order-edit lightbox.
				me.publish("enrollment/signin/popup");
			});
		},
		"dom:[data-action=updateregion]/reset": function(e, region) {
			e.preventDefault();
			e.stopPropagation();

			var me = this;
			var $el = $(e.currentTarget);

			if (!region || !region.District) {
				return;
			}

			// Reload Delivery Time Span
			me.publish("deliverTime/reload", region.District.Id);
			me.publish("expressCompany/reload", region.District.ExpressCompanyId);
		},
		"hub/order/receiver/nonlogin/submit": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var region = $form.find("[data-action=updateregion]").data("value");
			var provinceId = 0;
			var cityId = 0;
			var areaId = 0;

			if (region && region.Province && region.City && region.District) {
				provinceId = region.Province.Id;
				cityId = region.City.Id;
				areaId = region.District.Id;
			}

			var addressData = {
				firstName: _.trim($form.find("[name=signup-firstname]").val()),
				//lastName: _.trim($form.find("[name=signup-lastname]").val()),
				mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
				zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
				line1: _.trim($form.find("[name=signup-line1]").val()),
				//line2: _.trim($form.find("[name=signup-line2]").val()),
				cityId: region.City.Id,
				areaId: region.District.Id
			};

			// Validate mobile
			if (!regex.mobile(addressData.mobile)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1408
				}).open();

				return when.reject();
			}

			var promiseValue = [];
			promiseValue.push(addressData);
			return promiseValue;
		},
		"dom:[data-action=signin]/click": function(e) {
			e.preventDefault();
			var me = this;
			var $me = me.$element;

			// Popup order-edit lightbox.
			me.publish("enrollment/signin/popup");
		}
	});
});

define('troopjs-requirejs/template!widget/delivery/order/index.html',[],function() { return function template(data) { var o = "<div data-weave=\"widget/delivery/packagedays/main\"></div>"; return o; }; });
/*订单提交第一步主逻辑*/
define('widget/delivery/order/main',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "template!./index.html",
    "json2"
], function(Widget, $, _, when, context, cloneObject, cookie, ajaxQuery, api, pageUrl, InfoBox, Time, template) {
    "use strict";

    var URI_RECEIVER = api.get("receiver");
    var URI_ORDER_CREATE = api.get("order_create");
    var URI_DELIVER_TIME = api.get("deliver_time");
    var URI_ACCOUNT = api.get("account");

    // Time function
    var INS_TIME = new Time();

    function getStatus(code) {
        var status = [];
        status["1000"] = "1360"; //参数错误
        status["-100"] = "1360"; //没有产品
        status["-1"] = "1360"; //创建失败
        status["1001"] = "1360"; //未知错误
        status["-200"] = "1365"; //页面过期
        status["-400"] = "1469"; //地区不配送

        return status[code] || "";
    }

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Append Template
            me.html(template);
        },
        // Default receiver
        "getDefaultReceiver": function() {
            var me = this;

            var deferred = when.defer();

            if (me.defaultReceiver) {
                deferred.resolve(me.defaultReceiver);
            } else {
                ajaxQuery({
                    url: URI_RECEIVER,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    var receivers = data.Receivers || [];
                    var i = 0;
                    var iLens = receivers.length;
                    // No deliver address
                    if (iLens <= 0) {
                        deferred.resolve();
                        return;
                    }
                    for (i = 0; i < iLens; i++) {
                        if (receivers[i].IsDefault) {
                            // Save to widget
                            me.defaultReceiver = receivers[i];
                            deferred.resolve(me.defaultReceiver);
                            return;
                        }
                    }
                });
            }
            return deferred.promise;
        },
        "initDeliveryList": function(myPackage) {
            var me = this;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }
            if (!myPackage) {
                return;
            }

            // With a package selected, render package to calendar.
            me.getDefaultReceiver()
                .then(function(defaultReceiver) {

                    // Memory Order
                    var deliveryList = me.deliveryList;
                    var k = 0;
                    var kLens = myPackage.length;

                    // Own package
                    if (kLens <= 0) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1146
                        }).open();
                    } else {
                        // Copy package info & default order info
                        for (k = 0; k < kLens; k++) {
                            if (!deliveryList[k]) {
                                deliveryList[k] = {};
                            }
                            // Init Products
                            deliveryList[k].Products = [];
                            // Package info
                            // Day: day start from 0
                            deliveryList[k].Date = INS_TIME.addDay(me.deliveryStartDate, myPackage[k].Day);
                            deliveryList[k].PackageRuleId = myPackage[k].Id;
                            // Customize (subscription) === 0
                            if (!myPackage[k].Id) {
                                deliveryList[k].Products = myPackage[k].Products;
                            }
                            //deliveryList[k].PackageProducts = myPackage[k].Products;
                            // Default 1 package per(one) time
                            deliveryList[k].PackageRuleCount = 1;

                            // Attach default address to all date
                            if (defaultReceiver && !deliveryList[k].ReceiverId) {
                                // Receiver info
                                deliveryList[k].ReceiverId = defaultReceiver.Id;
                                deliveryList[k].Receiver = defaultReceiver;
                            }
                        }
                    }

                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Init Delivery List");
                });
        },
        // Generate order
        "generateOrder": function(deltaDay) {
            var me = this;
            deltaDay = deltaDay || 0;

            // Packages will apply based on start date
            if (!me.deliveryStartDate) {
                return;
            }

            function setDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Set new date
                    me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                }
            }

            function resetDeliveryDate() {
                // Reset date in deliver list
                if (!me.deliveryList) {
                    return;
                }
                if (isNaN(deltaDay) || deltaDay === 0) {
                    return;
                }
                var i = 0;
                var iLens = me.deliveryList.length;

                for (i = 0; i < iLens; i++) {
                    // Reset new date by add delta days
                    if (me.deliveryList[i].Date) {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryList[i].Date, deltaDay);
                    } else {
                        me.deliveryList[i].Date = INS_TIME.addDay(me.deliveryStartDate, i);
                    }
                }
            }

            // With orders
            if (me.deliveryList && me.deliveryList.length > 0) {
                // Reset date in deliver list
                if (deltaDay) {
                    resetDeliveryDate();
                } else if (!me.deliveryList[0].Date) {
                    setDeliveryDate();
                }

                // Publish to order confirm, list price list
                me.publishDeliveryList("Generate Order with Delivery List");
            } else {
                // Not initial run
                // Check whether it's a package
                if (me.myPackage) {
                    me.initDeliveryList(me.myPackage);
                } else {
                    // Without orders & package, should be customize
                    // Publish to order confirm, list price list
                    me.publishDeliveryList("Generate Order Without Delivery List");
                }
            }
        },
        "publishDeliveryList": function(sign) {
            var me = this;

            // Tracking code for debug
            console.log("//-----=====-----// " + sign + " //-----=====-----//");
            console.log(me.deliveryList);
        },
        // Calendar events
        "hub/order/package": function(myPackage) {
            var me = this;

            if (!myPackage) {
                // Customize order
                cookie.get("customize").then(function(customizePackage) {
                    // Memory Order
                    me.deliveryList = JSON.parse(customizePackage);
                }, function() {});
                return;
            }

            // Save
            me.myPackage = cloneObject(myPackage);
            // Mixin calendar/order, rendering
            // Args "myPackage" for FORCE re-render package
            me.initDeliveryList(me.myPackage);
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            if (!args || !args.selectedDate) {
                return;
            }

            // For packageRule caculate
            me.publish("deliveryStartDate/change/next", args);

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            // Gap days between new date & last date
            var deltaDay = 0;
            if (me.deliveryStartDate) {
                deltaDay = INS_TIME.dayDiff(me.deliveryStartDate, deliveryStartDate);
            }
            // Save delivery start date
            me.deliveryStartDate = deliveryStartDate;

            // Mixin calendar/order, rendering
            me.generateOrder(deltaDay);
        },
        "hub/receiver/changeDeliverTime": function(deliverTime) {
            var me = this;

            if (!deliverTime) {
                return;
            }

            // Memory
            me.deliverTime = deliverTime;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].DeliverTimeId = deliverTime.Id;
                me.deliveryList[i].DeliverTimeName = deliverTime.Name;
            }
            // Publish
            me.publishDeliveryList("Deliver Time Change");
        },
        "hub/receiver/changeDeliverAddress": function(receiver) {
            var me = this;

            if (!receiver) {
                return;
            }
            if (!me.deliveryList || me.deliveryList.length <= 0) {
                return;
            }

            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                me.deliveryList[i].Receiver = receiver;
                me.deliveryList[i].ReceiverId = receiver.Id;
            }
            // Publish
            me.publishDeliveryList("Deliver Address Change");
        },
        "hub/order/addon": function(arrayAddon) {
            var me = this;

            var j = 0;
            var jLens = arrayAddon.length;
            var addonId = 0;

            me.addon = [];

            for (j = 0; j < jLens; j++) {
                addonId = parseInt(arrayAddon[j], 10);
                if (addonId !== 0) {
                    me.addon.push({
                        "Id": addonId,
                        "Count": 1
                    });
                }
            }
        },
        "hub/order/source": function(eventCode) {
            var me = this;

            // Save source code to Memory
            me.eventCode = eventCode || "";
        },
        "getReceiver": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/receiver/nonlogin/submit").spread(function(dataReceiver) {
                return when.resolve(dataReceiver);
            });
        },
        "getSelfpickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/selfpickup").spread(function(dataSelfpickupInfo) {
                return when.resolve(dataSelfpickupInfo);
            });
        },
        "orderColloection": function() {
            var me = this;

            function updateDeliveryList(i) {
                if (me.deliverTime) {
                    me.deliveryList[i].DeliverTimeId = me.deliverTime.Id;
                    me.deliveryList[i].DeliverTimeName = me.deliverTime.Name;
                }

                // strDate used for log, we have a bug on date loss
                me.deliveryList[i].strDate = me.deliveryList[i].Date;
                me.deliveryList[i].Date = INS_TIME.getStrDate(me.deliveryList[i].Date);

                if (me.addon && me.addon.length > 0) {
                    me.deliveryList[i].Products = me.deliveryList[i].Products.concat(me.addon);
                }
            }

            function nonloginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;

                return me.getReceiver().then(function(dataReceiver) {

                    if (!dataReceiver || !dataReceiver.firstName || !dataReceiver.line1 || !dataReceiver.mobile) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    for (i = 0; i < iLens; i++) {
                        // General update delivery list
                        updateDeliveryList(i);

                        me.deliveryList[i].IsSelfPickup = false;
                        me.deliveryList[i].SelfPickupInfo = {};
                        me.deliveryList[i].Contact = dataReceiver;
                        me.deliveryList[i].ReceiverId = 0;
                    }

                    return when.resolve();
                });
            }

            function loginSubmit() {
                // Format Date to string
                var i = 0;
                var iLens = me.deliveryList.length;
                var withDeliveryAddress = true;

                return me.getSelfpickup().then(function(selfpickup) {

                    var isSelfPickup = selfpickup.isSelfPickup;
                    var selfpickupInfo = selfpickup.selfpickupInfo;

                    if (isSelfPickup) {
                        if (!selfpickupInfo.firstName || !selfpickupInfo.mobile) {
                            // Give a warning: Without receiver
                            // Show error msg as a lightbox
                            new InfoBox({
                                title: 1144,
                                content: 1386
                            }).open();

                            return when.reject();
                        }
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = true;
                            me.deliveryList[i].SelfPickupInfo = selfpickupInfo;
                            me.deliveryList[i].Contact = {};
                            me.deliveryList[i].ReceiverId = 0;
                        }
                    } else {
                        for (i = 0; i < iLens; i++) {
                            // General update delivery list
                            updateDeliveryList(i);

                            me.deliveryList[i].IsSelfPickup = false;
                            me.deliveryList[i].SelfPickupInfo = {};
                            me.deliveryList[i].Contact = {};

                            // Has Delivery Address?
                            if (!me.deliveryList[i].ReceiverId) {
                                withDeliveryAddress = false;
                            }
                        }
                    }

                    if (!withDeliveryAddress) {
                        // Give a warning: Without receiver
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1251
                        }).open();

                        return when.reject();
                    }

                    return when.resolve();
                });
            }

            // Signin?
            return cookie.get("memberid").then(function() {
                // Get User Account
                return ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(loginSubmit, nonloginSubmit);
            }, nonloginSubmit);
        },
        // Submit order
        "hub/order/submit": function() {
            var me = this;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1147
                }).open();
                // Do not submit order
                return;
            }

            me.orderColloection().then(function orderSubmit() {
                // /order/confirm#xxxx
                var hashSourceCode = "";
                var days;

                /*----------------HARDCODE----------------*/
                // Source code logic
                if (me.eventCode) {
                    switch (me.eventCode.toUpperCase()) {
                        case "GIFT":
                            // Special logic for GiftCard
                            days = me.deliveryList.length || 1;
                            me.deliveryList.splice(1);
                            me.deliveryList[0].PackageRuleCount = days;
                            break;
                        default:
                            hashSourceCode = "/event/" + me.eventCode.toUpperCase();
                            break;
                    }
                }
                /*----------------HARDCODE----------------*/

                // Generate data to submit
                var orderData = {
                    Order_id: 0,
                    SourceCode: me.eventCode,
                    DeliverList: me.deliveryList
                };

                // Ajax submit
                ajaxQuery({
                    url: URI_ORDER_CREATE,
                    data: JSON.stringify(orderData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    pageUrl.goto("/order/confirm", "orderid/" + data.Order_id + hashSourceCode);
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            });
        }
    });
});
define('widget/delivery/packagedays/main',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/cookie/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/troophash/main",
    "json2"
], function(Widget, $, _, when, context, cloneObject, ajaxQuery, api, cookie, pageUrl, InfoBox, Time, troopHash) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    var WEEK_DAYS = 7;

    var URI_PACKAGE = api.get("package");
    var URI_PACKAGEBY_ID = api.get("package_loadbyid");

    var PACKAGE_DAYS = [1, 3, 5, 7];
    var PACKAGE_DEFAULT_DAY = 1;

    function parseIntArray(arrString) {
        if (!arrString || arrString.length <= 0) {
            return;
        }

        var i = 0;
        var iLens = arrString.length;
        var arrInt = [];

        for (i = 0; i < iLens; i++) {
            arrInt.push(parseInt(arrString[i], 10));
        }

        return arrInt;
    }

    function getRuleByDay(rulesOrigin, dayOfWeekFromMonday, day) {
        var iWeek = 0;
        day = day || 0;

        iWeek = dayOfWeekFromMonday + day;
        iWeek = iWeek >= WEEK_DAYS ? iWeek - WEEK_DAYS : iWeek;
        return rulesOrigin[iWeek];
    }

    function getPackageDayRules(dataRules, days, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = days || getPackageDefaultDays();
        var packageDayRules = [];

        // Week day
        var dayOfWeekFromMonday;

        // Rule index
        var k = 0;
        var kLens = dataRules.length;
        // Loop round
        var turn = 0;

        if (startDate) { // By week
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                packageDayRules.push(cloneObject(getRuleByDay(dataRules, dayOfWeekFromMonday, i)));
                packageDayRules[i].Day = i;
            }
        } else { // Not by week. Looping
            for (i = 0; i < iLens; i++) {
                // Rule round 
                turn = Math.floor(i / kLens);
                // k: Rule index
                k = i % kLens;

                // i: day index
                packageDayRules.push(cloneObject(dataRules[k]));

                // Day start from 0 - (Default from 1 in DB)
                if (turn === 0) {
                    // First round of rules 
                    packageDayRules[i].Day = dataRules[k].Day - 1;
                } else {
                    // > 1st round 
                    packageDayRules[i].Day = turn * dataRules[kLens - 1].Day + dataRules[k].Day - 1;
                }
            }
        }

        return packageDayRules;
    }

    function getSubscriptionPackageDayRules(dataRules, weekDays, duration, startDate) {
        // Looping package rules, get the right rule by days. 1,3,5,7...
        var i = 0;
        var iLens = duration;
        var j = 0;
        var jLens = weekDays.length;
        var k = 0;
        var kLens = dataRules.length;
        var packageDayRules = [];
        var dayOfWeekFromMonday;
        var weekDay;
        var deltaWeekDay = 0;

        if (startDate) { // By week
            // 0 = Monday; 6 = Sunday;
            dayOfWeekFromMonday = INS_TIME.getDayOfWeekFromMonday(startDate);
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    if (deltaWeekDay < 0) {
                        break;
                    }

                    packageDayRules.push(cloneObject(getRuleByDay(dataRules, weekDay)));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;
                }
            }
        } else {
            dayOfWeekFromMonday = weekDays[0] === 0 ? 6 : weekDays[0] - 1;
            for (i = 0; i < iLens; i++) {
                for (j = 0; j < jLens; j++) {
                    weekDay = weekDays[j] === 0 ? 6 : weekDays[j] - 1;
                    deltaWeekDay = weekDay - dayOfWeekFromMonday;

                    packageDayRules.push(cloneObject(dataRules[k]));
                    packageDayRules[j + i * jLens].Day = (i * WEEK_DAYS) + deltaWeekDay;

                    // k: package index
                    if (k < (kLens - 1)) {
                        k++;
                    } else {
                        k = 0;
                    }

                }
            }
        }

        return packageDayRules;
    }

    function getPackageDefaultDays() {
        return PACKAGE_DEFAULT_DAY;
    }

    function getPackageDays(dataRules, days) {
        // Get days in package
        var dataDays = [];
        // Looping package rules, get days. 1,3,5,7...
        var k = 1;
        var kLens = PACKAGE_DAYS.length;
        for (k = 0; k < kLens; k++) {
            if (days) {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === days
                });
            } else {
                dataDays.push({
                    "days": PACKAGE_DAYS[k],
                    "active": PACKAGE_DAYS[k] === getPackageDefaultDays()
                });
            }
        }
        return dataDays;
    }

    function getPackageGroup(dataPackage, packageId, groupId) {
        // Get package by packageId & groupId
        var dataGroups = [];
        var dataGroup;
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                dataGroups = dataPackage[i].Groups;
                if (dataGroup) {
                    break;
                }
                for (var j = 0, jLens = dataGroups.length; j < jLens; j++) {
                    if (dataGroups[j].Id.toString() === groupId.toString()) {
                        dataGroup = dataGroups[j];
                        break;
                    }
                }
            }
        }
        return dataGroup;
    }

    function getPackageTitle(dataPackage, packageId) {
        // Get package name by packageId
        var title = "";
        var description = "";
        // Looping package, get the right rule
        for (var i = 0, iLens = dataPackage.length; i < iLens; i++) {
            if (dataPackage[i].Id.toString() === packageId.toString()) {
                title = dataPackage[i].Title;
                description = dataPackage[i].Description;
                break;
            }
        }
        return {
            "title": title || "",
            "description": description || ""
        };
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;

        me.startDateDeferred = when.defer();
    }, {
        "getPackage": function(packageId) {
            var me = this;
            var $me = me.$element;

            var deferred = when.defer();

            if (me.dataPackage) {
                deferred.resolve(me.dataPackage);
            } else {
                // Get Package
                ajaxQuery({
                    url: packageId ? URI_PACKAGEBY_ID : URI_PACKAGE,
                    data: (packageId ? {
                        packageId: packageId,
                        language: context.language
                    } : {
                        language: context.language
                    }),
                    type: "GET",
                    dataType: "json"
                }).then(function(dataPackage) {
                    me.dataPackage = dataPackage.Packages;
                    deferred.resolve(me.dataPackage);
                    return;
                });
            }
            return deferred.promise;
        },
        "render": function(startDate) {
            var me = this;

            var packageDayRules;

            if (!me.packageRules) {
                return;
            }

            if (me.weekDays && me.duration) {
                // Subscription
                packageDayRules = getSubscriptionPackageDayRules(me.packageRules, me.weekDays, me.duration, startDate);
            } else if (me.packageDays) {
                // Standard
                packageDayRules = getPackageDayRules(me.packageRules, me.packageDays, startDate);
            }
            if (!packageDayRules || packageDayRules.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1146
                }).open();

                return;
            }
            // Publish package package
            me.publish("order/package", packageDayRules);
        },
        "readyForRender": function() {
            var me = this;

            if (me.packageGroup.ByWeek) {
                me.startDateDeferred.promise.then(function(startDate) {
                    me.render(startDate);
                });
            } else {
                // Rendering
                me.render();
            }
        },
        "hub/deliveryStartDate/change/next": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            var status = me.startDateDeferred.promise.inspect();

            if (status.state === "fulfilled" && me.packageGroup && me.packageGroup.ByWeek) {
                me.startDateDeferred = when.defer();
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
                me.readyForRender();
            } else {
                me.startDateDeferred.resolver.resolve(deliveryStartDate);
            }
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            /*
            packageid
            groupid
            days
            addon
            -----------
            event
            -----------
            weekDays
            duration
            */

            var packageId = parseInt(hashMap.packageid, 10);
            var groupId = parseInt(hashMap.groupid, 10);

            // General cleanse order
            var packageDays = parseInt(hashMap.days, 10) || getPackageDefaultDays();

            // Subscription
            var weekDays = hashMap.weekDays ? parseIntArray(hashMap.weekDays.split(",")) : [];
            var duration = parseInt(hashMap.duration, 10) || 0;

            // Addon
            var arrayAddon = hashMap.addon ? hashMap.addon.split(",") : [];

            // General cleanse order
            var eventCode = hashMap.event || "";

            // packageId & groupId is required
            // or will direct to root page
            if (isNaN(packageId) || isNaN(groupId)) {
                pageUrl.goto("/");
                return;
            }

            // Trigger event while following param changes
            if (me.packageId === packageId &&
                me.groupId === groupId &&
                me.packageDays === packageDays &&
                _.difference(weekDays, me.weekDays).length === 0 &&
                me.duration === duration &&
                _.difference(arrayAddon, me.arrayAddon).length === 0 &&
                me.eventCode === eventCode
            ) {
                return;
            }
            me.packageDays = packageDays || 0;
            me.weekDays = weekDays;
            me.duration = duration || 0;
            me.arrayAddon = arrayAddon;
            me.eventCode = eventCode;

            // Update Addon
            me.publish("order/addon", arrayAddon);

            // Publish source code
            me.publish("order/source", eventCode);

            // Package OR Customize
            if (packageId !== 0 && groupId !== 0) {
                // Clear customize order data from cookie
                cookie.rm("customize", {
                    path: "/"
                });
                // Clear customize order data from cookie
                cookie.rm("subscription_cust", {
                    path: "/"
                });

                // Compare with last hash, return while without change
                if (me.packageId && me.groupId && me.packageId === packageId && me.groupId === groupId) {
                    // Ready for render
                    me.readyForRender();
                    return;
                }
                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;

                // Get Package
                me.getPackage(packageId).then(function(dataPackage) {
                    // Looping package, get the right rule
                    // Save to widget at same time
                    var packageGroup = me.packageGroup = getPackageGroup(dataPackage, packageId, groupId);
                    me.packageTitle = getPackageTitle(dataPackage, packageId);

                    if (!packageGroup) {
                        return;
                    }

                    me.packageRules = packageGroup.Rules;

                    // Ready for render
                    me.readyForRender();
                });
            } else if (weekDays && duration) {
                // Get customize content from cookie
                cookie.get("subscription_cust").then(function(customizeRule) {
                    me.packageRules = [{
                        "Id": 0,
                        "Day": 1,
                        "Name": "Subscription customize",
                        "Products": JSON.parse(customizeRule)
                    }];
                    // Not by week
                    me.render();
                }, function() {});

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            } else { // Customize
                // Publish package package
                me.publish("order/package");

                // Save to widget
                me.packageId = packageId;
                me.groupId = groupId;
            }
        }
    });
});

define('troopjs-requirejs/template!widget/delivery/receiver/index.html',[],function() { return function template(data) { var o = "";
    var receiverId = data.receiverId || 0;
o += "\r\n<p class=\"orderedit-delivery-title\" data-weave=\"widget/blurb/main(1087)\">Deliver to this address</p>\r\n<div data-action=\"receivercontrol\" class=\"orderedit-delivery-content orderedit-delivery-address\">\r\n    <div data-action=\"changedeliveraddress\" data-weave=\"widget/select/receiver/main(receiverId)\" data-receiver-id=\"" +receiverId+ "\" data-value=\"" +receiverId+ "\"></div>\r\n    <br>\r\n    <p class=\"orderedit-delivery-address-edit\">\r\n        <span>&rsaquo;</span> <a class=\"link-fn\" data-action=\"editAddress\" data-weave=\"widget/blurb/main(1088)\">Edit this address</a>\r\n    </p>\r\n</div>\r\n<p>\r\n    <span>&rsaquo;</span> <a class=\"link-fn\" data-action=\"addAddress\" data-weave=\"widget/blurb/main(1074)\">Add a new delivery address</a>\r\n</p>\r\n "; return o; }; });
define('widget/delivery/receiver/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html",
    "json2"
], function(Widget, weave, when, $, context, ajaxQuery, api, template) {
    "use strict";

    return Widget.extend(function($element, widgetName, receiverId) {
        var me = this;

        me.receiverId = receiverId || 0;
    }, {
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template, {
                receiverId: me.receiverId
            }).then(function() {
            });
        },
        "hub/receiver/default": function(defaultReceiver) {
            // TODO:
            var me = this;
            var $me = me.$element;
            var $receiverControl = $me.find("[data-action=receivercontrol]");

            if (defaultReceiver) {
                // Set default value.
                me.publish("receiver/changeDeliverAddress", defaultReceiver);
                me.publish("deliverTime/reload", defaultReceiver.AreaId);
                me.publish("expressCompany/reload", defaultReceiver.ExpressCompanyId);

                $receiverControl.show();
            } else {
                $receiverControl.hide();
            }
        },
        // Change to another address
        "dom:[data-action=receivercontrol]/change": function(e, receiver) {
            var me = this;
            var $el = $(e.currentTarget);

            if (!receiver) {
                return;
            }

            me.receiverId = receiver.Id;
            me.publish("receiver/changeDeliverAddress", receiver);
            me.publish("deliverTime/reload", receiver.AreaId);
            me.publish("expressCompany/reload", receiver.ExpressCompanyId);
        },
        // Manage delivery address
        "dom:[data-action=addAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress");
        },
        // Edit selected address
        "dom:[data-action=editAddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            me.publish("receiver/editAddress", {
                isEdit: true,
                receiverId: me.receiverId
            });
        }
    });
});

define('troopjs-requirejs/template!widget/delivery/selfpickup/entry/index.html',[],function() { return function template(data) { var o = "";
	var isLogin = data.isLogin;
o += "\r\n<a "; if (isLogin) { o += "href=\"#pickuptyp-selfpickup\" data-toggle=\"tab\" data-action=\"switchselfpack\" "; } else { o += "href=\"#\" data-action=\"signin\""; } o += " data-weave=\"widget/blurb/main(1027)\">Pick up in store</a>"; return o; }; });
define('widget/delivery/selfpickup/entry/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/cookie/main",
    "template!./index.html",
    "json2"
], function(Widget, $, cookie, template) {
    "use strict";

    var VISIBLE_DATEPICKER = true;
    var VISIBLE_DELIVERYTIME = true;

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template, {
                "isLogin": false
            });
        },
        "hub/receiver/hasLogin": function() {
            var me = this;

            me.html(template, {
                "isLogin": true
            });
        },
        // Render no login
        "hub/receiver/nonLogin": function() {
            var me = this;

            me.html(template, {
                "isLogin": false
            });
        }
    });
});


define('troopjs-requirejs/template!widget/delivery/selfpickup/index.html',[],function() { return function template(data) { var o = "";
    var account = data.account;
    
    var stores = data.stores;
    var j = 0;
    var jLens = stores.length;

    var cities = data.cities;
    var i = 0;
    var iLens = cities.length;
o += "\r\n<div class=\"selfpicupstore\">\r\n    <ul class=\"nav nav-tabs\">\r\n        ";
            for (i = 0; i < iLens; i++) {
        o += "\r\n            <li class=\""; if (i===0) { o += "active"; } o += "\">\r\n                <a href=\"#selfpicupstore-" +cities[i].Id+ "\" data-toggle=\"tab\">" +cities[i].Name+ "</a>\r\n            </li>\r\n        "; } o += "\r\n    </ul>\r\n    <div class=\"tab-content\">\r\n        ";
            for (i = 0; i < iLens; i++) {
        o += "\r\n            <div class=\"tab-pane "; if (i===0) { o += "active"; } o += "\" id=\"selfpicupstore-" +cities[i].Id+ "\">\r\n                "; renderStore(cities[i].Id); o += "\r\n            </div>\r\n        "; } o += "\r\n   </div>\r\n</div>\r\n<form class=\"standardform form-selfpickup-info form-horizontal\">\r\n    ";
        renderAddress();
    o += "\r\n</form>\r\n\r\n";
    function renderStore(cityId) {
        var store;
o += "\r\n    ";
        for (j = 0; j < jLens; j++) {
            store = stores[j];
            if (store.StoreCityId === cityId && store.IsActive) { 
    o += "\r\n                <label class=\"radio selfpicupstore-single\">\r\n                    <input type=\"radio\" class=\"icheck\" value=\"" +store.StoreId+ "\" data-action=\"changeStore\" name=\"selfpickup-store\">\r\n                    <strong class=\"selfpickupstore-name\">" +store.StoreName + "</strong>\r\n                </label>\r\n                <p class=\"selfpickupstore-gap\">\r\n                    <span data-weave=\"widget/blurb/main(1128)\">Store address:</span>\r\n                    " +store.StoreAddress + "\r\n                    "; if (store.StoreMapUrl) { o += "\r\n                        [<a class=\"link-fn\" data-url=\"" +store.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n                    "; } o += "\r\n                </p>\r\n                "; if (store.StoreBusinessHours) { o += "\r\n                    <p class=\"selfpickupstore-gap\">\r\n                        <span data-weave=\"widget/blurb/main(1382)\">Business hours:</span>\r\n                        " +store.StoreBusinessHours + "\r\n                    </p>\r\n                "; } o += "\r\n                "; if (store.StoreTel) { o += "\r\n                    <p class=\"selfpickupstore-gap\">\r\n                        <span data-weave=\"widget/blurb/main(1387)\">Contact tel:</span>\r\n                        " +store.StoreTel + "\r\n                    </p>\r\n                "; } o += "\r\n    ";
            }
        }
    o += "\r\n"; } o += "\r\n\r\n"; function renderAddress() { o += "\r\n    <p class=\"orderedit-delivery-title\" data-weave=\"widget/blurb/main(1383)\">Leave your contact information</p>\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"selfpickup-firstname\" data-weave=\"widget/blurb/main(1397)\">Name:</label>\r\n        <div class=\"col-sm-8\">\r\n            <input class=\"form-control\" type=\"text\" id=\"selfpickup-firstname\" name=\"selfpickup-firstname\" placeholder=\"Pickup contactor's name is required\" required value=\"" +(account.FirstName || "")+ "\" data-weave=\"widget/blurb/main(1398, 'placeholder')\">\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"selfpickup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n        <div class=\"col-sm-8\">\r\n            <input class=\"form-control\" type=\"text\" id=\"selfpickup-phonenumber\" name=\"selfpickup-phonenumber\" placeholder=\"Your Contact Number\" required value=\"" +(account.Mobile || "")+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/delivery/selfpickup/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./index.html",
    "json2",
    "underscore.string"
], function(Widget, weave, when, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_STORES = api.get("stores");

    function getCities(stores) {
        var i = 0;
        var iLens = stores.length;
        var cities = [];
        var storeCityId = 0;

        function searchCities() {
            return _.find(cities, function(e, j) {
                return e.Id === storeCityId;
            });
        }

        for (i = 0; i < iLens; i++) {
            storeCityId = stores[i].StoreCityId;
            // City exist
            if (!stores[i].IsActive || searchCities()) {
                continue;
            }
            // City not exist
            cities.push({
                "Id": storeCityId,
                "Name": stores[i].StoreCityName
            });
        }

        return cities;
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "render": function(stores, dataAccount) {
            var me = this;
            var $me = me.$element;

            dataAccount = dataAccount || {};


            // Rendering
            me.html(template, {
                "account": dataAccount,
                "stores": stores,
                "cities": getCities(stores)
            }).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    checkedClass: 'checked',
                    increaseArea: '20%'
                });
                // Check default
                $('input[name=selfpickup-store]:eq(0)').iCheck('check');
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // GET deliver time
            ajaxQuery({
                url: URI_STORES,
                type: "GET",
                data: {
                    "language": context.language
                },
                dataType: "json"
            }).then(function(data) {
                var stores = data.Stores;

                // Signin? Write default account info.
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.render(stores, data);
                }, function() {
                    //
                    me.render(stores);
                });

            });
        },
        "getIsSelfPickup": function() {
            var me = this;

            // Publish submit order event
            return me.publish("order/pickuptype").spread(function(dataPickupType) {
                return when.resolve(dataPickupType);
            });
        },
        "hub/order/selfpickup": function() {
            var me = this;
            var $me = me.$element;

            return me.getIsSelfPickup().then(function(isSelfPickup) {
                var storeId;
                var firstName;
                var mobile;
                var selfpickupInfo = {};
                var promiseValue = [];

                if (isSelfPickup) {
                    storeId = $me.find(".iradio_square.checked > input").val();
                    firstName = _.trim($me.find("#selfpickup-firstname").val());
                    mobile = _.trim($me.find("#selfpickup-phonenumber").val());

                    // Validate mobile
                    if (!regex.mobile(mobile)) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1408
                        }).open();

                        return when.reject();
                    }

                    promiseValue.push({
                        "isSelfPickup": true,
                        "selfpickupInfo": {
                            "storeId": storeId,
                            "firstName": firstName,
                            "mobile": mobile
                        }
                    });
                } else {
                    promiseValue.push({
                        "isSelfPickup": false,
                        "selfpickupInfo": {}
                    });
                }
                return promiseValue;
            });
        },
        "dom:[data-toggle=tab]/click": function(e) {
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var href = $el.attr("href");

            // Check default
            $me.find(href + ' input[name=selfpickup-store]:eq(0)').iCheck('check');
        }
    });
});

define('troopjs-requirejs/template!widget/deliveryaddress/index.html',[],function() { return function template(data) { var o = "";
    var i = 0;
    var iLens = data.length;
    for(i = 0; i < iLens; i++){
        renderAddress(data[i], i);
    }
o += "\r\n<form class=\"form-horizontal\" role=\"form\" data-action=\"addnewaddress\">\r\n    "; if (iLens > 0) { o += "\r\n        <div class=\"form-account-addaddress cf text-right\" data-action=\"addaddressvisiblecontrol\">\r\n            <button class=\"btn btn-default btn-lg\" data-action=\"addaddress\">\r\n                <i class=\"glyphicon glyphicon-plus\"></i>\r\n                <span data-weave=\"widget/blurb/main(1067)\">Add Another Address</span>\r\n            </button>\r\n        </div>\r\n    "; } o += "\r\n    <div class=\"form-submit hid cf text-right\" data-action=\"submitvisiblecontrol\">\r\n        <button type=\"submit\" class=\"btn btn-default btn-lg btn-action\">\r\n            <span data-weave=\"widget/blurb/main(1079)\">SAVE</span>\r\n        </button>\r\n    </div>\r\n</form>\r\n"; function renderAddress(address, index) { o += "\r\n<form class=\"form-horizontal\" role=\"form\" data-action=\"editaddress\">\r\n    <div class=\"form-section address-section form-deliveryaddress\" data-id=\"" +address.Id+ "\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname-" +index+ "\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-firstname-" +index+ "\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required value=\"" +address.FirstName+ "\" "; if (index === 0) {o += "autofocus"; } o += " data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber-" +index+ "\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-phonenumber-" +index+ "\" name=\"signup-phonenumber\" placeholder=\"Your Contact Number\" required value=\"" +address.Mobile+ "\" data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n            <div class=\"col-sm-8\">\r\n                <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"" +(address.ProvinceId||0)+ "\" data-city-id=\"" +(address.CityId||0)+ "\" data-area-id=\"" +(address.AreaId||0)+ "\"></div>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"\"></label>\r\n            <div class=\"col-sm-8\">\r\n                <input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Shipping address\" required value=\"" +address.Line1+ "\" data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-zipcode-" +index+ "\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n            <div class=\"col-sm-8\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-zipcode-" +index+ "\" name=\"signup-zipcode\" placeholder=\"Zip code\" value=\"" +(address.ZipCode||'')+ "\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"\"></label>\r\n            <div class=\"col-sm-8\">\r\n                <button type=\"submit\" class=\"btn btn-default form-deliveryaddress-updateaddress\" data-action=\"updateaddress\">\r\n                    <span data-weave=\"widget/blurb/main(1069)\">Update</span>\r\n                </button>\r\n                <a class=\"btn btn-default form-deliveryaddress-deleteaddress\" data-action=\"deleteaddress\" data-weave=\"widget/blurb/main(1070)\">Delete</a>\r\n                "; if (!address.IsDefault) { o += "\r\n                    <a class=\"btn btn-default form-deliveryaddress-setaddressdefault\" data-action=\"setaddressdefault\" data-weave=\"widget/blurb/main(1071)\">Set as default</a>\r\n                "; } else { o += "\r\n                    <a class=\"btn btn-default form-deliveryaddress-setaddressdefault disabled\" disabled data-weave=\"widget/blurb/main(1072)\">Default address</a>\r\n                "; } o += "\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n"; }  return o; }; });

define('troopjs-requirejs/template!widget/deliveryaddress/address.html',[],function() { return function template(data) { var o = "<div class=\"form-section address-section form-deliveryaddress form-adddeliveryaddress\">\r\n\t<div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1395)\">Receiver name:</label>\r\n\t    <div class=\"col-sm-8\">\r\n\t        <input type=\"text\" class=\"form-control\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Type receiver name here\" required autofocus data-weave=\"widget/blurb/main(1396, 'placeholder')\">\r\n\t    </div>\r\n\t</div>\r\n\t\r\n\t<div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n\t    <div class=\"col-sm-8\">\r\n\t        <input type=\"text\" class=\"form-control\" id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Contact Number\" required data-weave=\"widget/blurb/main(1134, 'placeholder')\">\r\n\t    </div>\r\n\t</div>\r\n\t<div class=\"form-group\">\r\n\t    <label class=\"col-sm-3 control-label\" data-weave=\"widget/blurb/main(1057)\">Delivery address:</label>\r\n        <div class=\"col-sm-8\">\r\n\t        <div data-action=\"updateregion\" class=\"region\" data-weave=\"widget/select/region/main(provinceId, cityId, areaId)\" data-province-id=\"0\" data-city-id=\"0\" data-area-id=\"0\"></div>\r\n        </div>\r\n\t</div>\r\n\t<div class=\"form-group\">\r\n        <label class=\"col-sm-3 control-label\" for=\"\"></label>\r\n\t    <div class=\"col-sm-8\">\r\n\t\t\t<input class=\"form-control\" type=\"text\" name=\"signup-line1\" placeholder=\"Shipping address\" required data-weave=\"widget/blurb/main(1136, 'placeholder')\">\r\n\t\t</div>\r\n\t</div>\r\n    <div class=\"form-group\">\r\n\t\t<label class=\"col-sm-3 control-label\" for=\"signup-zipcode\" data-weave=\"widget/blurb/main(1207)\">Zip code:</label>\r\n        <div class=\"col-sm-8\">\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-zipcode\" name=\"signup-zipcode\" placeholder=\"Zip code\" data-weave=\"widget/blurb/main(1208, 'placeholder')\">\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/deliveryaddress/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/blurb/main",
    "widget/basic/regex",
    "template!./index.html",
    "template!./address.html",
    "jquery.validation",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, Blurb, regex, template, templateAddress) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_RECEIVER = api.get("receiver");
    var URI_RECEIVER_ADD = api.get("receiver_create");
    var URI_RECEIVER_UPDATE = api.get("receiver_update");
    var URI_RECEIVER_DELETE = api.get("receiver_delete");
    var URI_RECEIVER_DEFAULT = api.get("receiver_setdefault");

    function getStatus(code) {
        var status = [];
        status["11"] = "1278";

        return status[code] || "";
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.render();
        },
        "render": function() {
            var me = this;
            var $me = me.$element;

            ajaxQuery({
                url: URI_RECEIVER,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                me.html(template, data.Receivers);
                // Add delivery address if no address
                if (data.Receivers.length <= 0) {
                    var $address = $(templateAddress());
                    // Initial inner data-weave
                    weave.apply($address.find("[data-weave]"));
                    // Append elements
                    $me.find("[data-action=addnewaddress]").prepend($address);
                    $me.find("[data-action=submitvisiblecontrol]").show();
                }
            });
        },
        "dom:[data-action=addaddress]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $address = $(templateAddress());
            // Initial inner data-weave
            weave.apply($address.find("[data-weave]"));
            // Append elements
            $me.find("[data-action=addaddressvisiblecontrol]").before($address);
            $me.find("[data-action=addaddressvisiblecontrol]").hide();
            $me.find("[data-action=submitvisiblecontrol]").show();
        },
        "collectData": function($form) {
            if (!$form || $form.length <= 0) {
                return;
            }

            var region = $form.find("[data-action=updateregion]").data("value");
            var provinceId = 0;
            var cityId = 0;
            var areaId = 0;

            if (region && region.Province && region.City && region.District) {
                provinceId = region.Province.Id;
                cityId = region.City.Id;
                areaId = region.District.Id;
            }

            var data = {
                firstName: _.trim($form.find("[name=signup-firstname]").val()),
                lastName: _.trim($form.find("[name=signup-lastname]").val()),
                mobile: _.trim($form.find("[name=signup-phonenumber]").val()),
                zipcode: _.trim($form.find("[name=signup-zipcode]").val()),
                line1: _.trim($form.find("[name=signup-line1]").val()),
                line2: _.trim($form.find("[name=signup-line2]").val()),
                cityId: cityId,
                areaId: areaId,
                provinceId: provinceId
            };
            return data;
        },
        "dom:[data-action=editaddress]/submit": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).find(".form-deliveryaddress");

            var addressData = me.collectData($form);
            _.extend(addressData, {
                id: $form.data("id")
            });

            // Validate mobile
            if (!regex.mobile(addressData.mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            ajaxQuery({
                url: URI_RECEIVER_UPDATE,
                data: JSON.stringify(addressData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1151
                }).open();
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });

        },
        "dom:[data-action=deleteaddress]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).parents(".form-deliveryaddress");
            var addressData = {
                id: $form.data("id")
            };

            if (confirm(blurb.get(1249) || "Sure to delete this address?")) {
                ajaxQuery({
                    url: URI_RECEIVER_DELETE,
                    data: addressData,
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    me.render();
                    // Show response msg as a lightbox
                    new InfoBox({
                        title: 1148,
                        content: 1150
                    }).open();
                });
            }

        },
        "dom:[data-action=setaddressdefault]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $form = $(e.currentTarget).parents(".form-deliveryaddress");
            var addressData = {
                id: $form.data("id")
            };

            ajaxQuery({
                url: URI_RECEIVER_DEFAULT,
                data: addressData,
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                me.render();
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1149
                }).open();
            });
        },
        "dom:[data-action=addnewaddress]/submit": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $form = $me.find(".form-adddeliveryaddress");

            var addressData = me.collectData($form);

            // Validate mobile
            if (!regex.mobile(addressData.mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            ajaxQuery({
                url: URI_RECEIVER_ADD,
                data: JSON.stringify(addressData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                me.render();
                // Show response msg as a lightbox
                new InfoBox({
                    title: 1148,
                    content: 1152
                }).open();
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});
define('widget/download/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/api/main",
    "widget/ajaxquery/main",
    "widget/cookie/main",
    "widget/infobox/main"
], function(Widget, $, context, api, ajaxQuery, cookie, InfoBox) {
    "use strict";

    var URI_ACCOUNT = api.get("account");

    return Widget.extend({
        "dom:[data-action=download]/click": function(e) {
            var me = this;

            /*function fail() {
                e.preventDefault();
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1232
                }).open();
            }

            // Has cookie
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(undefined, fail);
            }, fail);*/
        }
    });
});

define('troopjs-requirejs/template!widget/download/brochure/index.html',[],function() { return function template(data) { var o = "";
  var CACHE_SERVER = data.cacheServer;
  var LANGUAGE = data.language;
  var PATH_BROCHURE = "/download/" + LANGUAGE + "/VCLEANSE-brochure.pdf";
  var PATH_BROCHURE_INSERT = "/download/" + LANGUAGE + "/VCLEANSE-brochure-insert.pdf";
o += "\r\n<div class=\"download-content download-brochure cf float-abs\">\r\n  <div class=\"download-thumbnail col-l\"></div>\r\n  <div class=\"download-main col-l\">\r\n    <h3 data-weave=\"widget/blurb/main(1226)\">VCLEANSE Brochure</h3>\r\n    <p data-weave=\"widget/blurb/main(1231)\">Click to download:</p>\r\n    <p>\r\n        <span>&rsaquo;</span>\r\n        <a href=\"" +CACHE_SERVER + PATH_BROCHURE+ "\" target=\"_blank\" data-action=\"download\">\r\n          <span data-weave=\"widget/blurb/main(1228)\">VCLEANSE brochure</span>\r\n        </a>\r\n    </p>\r\n  </div>\r\n</div>"; return o; }; });
define('widget/download/brochure/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/download/main",
    "template!./index.html"
], function(Widget, $, context, Download, template) {
    "use strict";

    return Download.extend({
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template, context);
        }
    });
});

define('troopjs-requirejs/template!widget/download/checklist/index.html',[],function() { return function template(data) { var o = "";
  var CACHE_SERVER = data.cacheServer;
  var PATH_CHECKLIST = "/download/VCLEANSE-checklist.pdf";
o += "\r\n<div class=\"download-content download-checklist cf float-abs\">\r\n  <div class=\"download-thumbnail col-l\"></div>\r\n  <div class=\"download-main col-l\">\r\n    <h3 data-weave=\"widget/blurb/main(1227)\">VCLEANSE Checklist</h3>\r\n    <p data-weave=\"widget/blurb/main(1231)\">Click to download:</p>\r\n    <p>\r\n        <span>&rsaquo;</span>\r\n        <a href=\"" +CACHE_SERVER + PATH_CHECKLIST+ "\" target=\"_blank\" data-action=\"download\">\r\n          <span data-weave=\"widget/blurb/main(1230)\">VCLEANSE checklist</span>\r\n        </a>\r\n    </p>\r\n  </div>\r\n</div>"; return o; }; });
define('widget/download/checklist/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/download/main",
    "template!./index.html"
], function(Widget, $, context, Download, template) {
    "use strict";

    return Download.extend({
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template, context);
        }
    });
});

define('troopjs-requirejs/template!widget/emailsubscribe/index.html',[],function() { return function template(data) { var o = "<div class=\"subscribe-form\">\r\n    <form class=\"form-inline cf\" role=\"form\" data-action=\"anonymous-subscribe-email\">\r\n        <div class=\"form-group\">\r\n            <input name=\"subscribe-email\" id=\"subscribe-email\" data-action=\"subscribe-email\" type=\"email\" class=\"form-control input-email\" placeholder=\"Subscribe to VCLEANSE\" required value=\"\" data-weave=\"widget/blurb/main(1411, 'placeholder')\">\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1403)\">Subscribe</button>\r\n    </form>\r\n    <div class=\"subscribe-success slide hid\" data-action=\"emailsubscribe-success\">\r\n        <p class=\"text-success\">\r\n            <span class=\"glyphicon glyphicon-ok-sign\"></span>\r\n            <span data-weave=\"widget/blurb/main(1412)\"></span>\r\n        </p>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/emailsubscribe/main',["troopjs-browser/component/widget",
	"jquery",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, $, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_SUBSCRIBE = api.get("subscribe");

	function getStatus(code) {
		var status = [];
		status["-1"] = "1181";

		return status[code] || "";
	}

	return Widget.extend(function($element, widgetName, isInternal) {
		var me = this;

		me.isInternal = isInternal || false;
	}, {
		"sig/start": function() {
			var me = this;

			me.html(template);
		},
		"dom:form[data-action=anonymous-subscribe-email]/submit": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			var $form = $(e.currentTarget);
			var $successLink = $me.find("[data-action=emailsubscribe-success]");
			var email = _.trim($form.find("[data-action=subscribe-email]").val());

			if (!regex.email(email)) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: 1407
				}).open();

				return;
			}
			ajaxQuery({
				url: URI_SUBSCRIBE,
				data: JSON.stringify({
					"email": email
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}).then(function(data) {
				$form.hide();
				$successLink.show().removeClass("out").addClass("in");
			}, function(e) {
				// Show error msg as a lightbox
				new InfoBox({
					title: 1144,
					content: getStatus(e)
				}).open();
			});
		}
	});
});
define('widget/enrollment/signout/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "context",
    "widget/cookie/main"
], function(Widget, weave, when, $, context,cookie) {
    "use strict";

    return Widget.extend(function() {
        var me = this;
        cookie.rm("memberid", {
            path: "/"
        });
        window.location.href = '/';
    });
});

define('troopjs-requirejs/template!widget/formaccount/index.html',[],function() { return function template(data) { var o = "<form class=\"form-horizontal\" role=\"form\">\r\n\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-firstname\" data-weave=\"widget/blurb/main(1054)\">First name:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-firstname\" name=\"signup-firstname\" placeholder=\"Your First Name Here\" autofocus required value=\"" +data.FirstName+ "\" data-weave=\"widget/blurb/main(1402, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-lastname\" data-weave=\"widget/blurb/main(1055)\">Last name:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-lastname\" name=\"signup-lastname\" placeholder=\"Your Last Name Here\" value=\"" +data.LastName+ "\" required data-weave=\"widget/blurb/main(1133, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-phonenumber\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-phonenumber\" name=\"signup-phonenumber\" placeholder=\"Your Contact Number\" value=\"" +data.Mobile+ "\" data-weave=\"widget/blurb/main(1137, 'placeholder')\" required disabled>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-email\" data-weave=\"widget/blurb/main(1076)\">Email:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"email\" class=\"form-control\" id=\"signup-email\" name=\"signup-email\" placeholder=\"Your Email Address Here\" required value=\"" +data.Email+ "\" data-weave=\"widget/blurb/main(1138, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-email\" data-weave=\"widget/blurb/main(1120)\">Gender:</label>\r\n            <div class=\"col-sm-7\">\r\n                <label class=\"radio-inline\">\r\n                    <input type=\"radio\" id=\"signup-gender-female\" name=\"signup-gender\" value=\"0\" ";if(data.GenderId === 0){o += "checked";}o += "> <span data-weave=\"widget/blurb/main(1121)\">Female</span>\r\n                </label>\r\n                <label class=\"radio-inline\">\r\n                    <input type=\"radio\" id=\"signup-gender-male\" name=\"signup-gender\" value=\"1\" ";if(data.GenderId === 1){o += "checked";}o += "> <span data-weave=\"widget/blurb/main(1122)\">Male</span>\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-birthday\" data-weave=\"widget/blurb/main(1123)\">Birthday:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-birthday\" data-action=\"datepicker\" value=\"" +(data.Birthday || "")+ "\">\r\n            </div>\r\n        </div>\r\n        <hr>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"\"></label>\r\n            <div class=\"col-sm-7 text-center\">\r\n                <button type=\"submit\" class=\"btn btn-lg btn-default btn-action\" data-weave=\"widget/blurb/main(1079)\">SAVE</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <input type=\"hidden\" id=\"blurb-1215\" data-weave=\"widget/blurb/main(1215, 'value')\" value=\"Phonenumber is required.\">\r\n    <input type=\"hidden\" id=\"blurb-1216\" data-weave=\"widget/blurb/main(1216, 'value')\" value=\"Verify that you've entered the correct phone number.\">\r\n    <input type=\"hidden\" id=\"blurb-1275\" data-weave=\"widget/blurb/main(1275, 'value')\" value=\"First name is required.\">\r\n    <input type=\"hidden\" id=\"blurb-1276\" data-weave=\"widget/blurb/main(1276, 'value')\" value=\"Last name is required.\">\r\n    <input type=\"hidden\" id=\"blurb-1277\" data-weave=\"widget/blurb/main(1277, 'value')\" value=\"Email is required.\">\r\n</form>"; return o; }; });
define('widget/formaccount/main',["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"template!./index.html",
	"jquery.validation",
	"jquery.ui.datepicker",
	"jquery.ui.datepicker-zh-CN",
	"json2",
	"underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, template) {
	"use strict";

	var URI_ACCOUNT = api.get("account");
	var URI_EMAILCHECK = api.get("emailcheck");
	var URI_PHONECHECK = api.get("phonecheck");
	var URI_ACCOUNT_UPDATE = api.get("account_update");

	function geti18n() {
		var lngMap = {
			"en": "",
			"cn": "zh-CN"
		};
		var lng = context.language || "en";
		return lngMap[lng];
	}

	function getStatus(code) {
		var status = [];
		status["-1"] = "1185";

		return status[code] || "";
	}

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_ACCOUNT,
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				//Render
				me.html(template, data).then(function() {

					// Generate datepicker by jQuery-UI
					var $datepicker = $me.find("[data-action=datepicker]");
					$(document.body).addClass("jquery-ui-lightness");

					// Localize
					$.datepicker.setDefaults($.datepicker.regional[geti18n()]);
					// Datepicker
					$datepicker.datepicker({
						"firstDay": 1,
						"showAnim": "slideDown",
						"changeMonth": true,
						"changeYear": true,
						"dateFormat": "yy-mm-dd",
						"yearRange": "1940:2014",
						"defaultDate": "1980-06-15"
					});

					// Validate & submit
					me.validation();

				});
			});
		},
		"validation": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var m1215 = $form.find("#blurb-1215").val();
			var m1216 = $form.find("#blurb-1216").val();
			var m1275 = $form.find("#blurb-1275").val();
			var m1276 = $form.find("#blurb-1276").val();
			var m1277 = $form.find("#blurb-1277").val();

			$form.validate({
				rules: {
					"signup-firstname": "required",
					"signup-lastname": "required",
					"signup-email": {
						required: true
					},
					"signup-phonenumber": {
						required: true,
						number: true
					},
					"signup-birthday": {
						dateISO: true
					}
				},
				messages: {
					"signup-firstname": m1275,
					"signup-lastname": m1276,
					"signup-email": {
						required: m1277
					},
					"signup-phonenumber": {
						required: m1215,
						number: m1216
					},
					"signup-birthday": {
						dateISO: "e.g. 2000-01-01"
					}
				},
				submitHandler: function(form) {
					var birthday = $form.find("#signup-birthday").val();
					var registerData = {
						firstName: _.trim($form.find("#signup-firstname").val()),
						lastName: _.trim($form.find("#signup-lastname").val()),
						validationcode:"",
						mobile: _.trim($form.find("#signup-phonenumber").val()),
						EMail: _.trim($form.find("#signup-email").val()),
						Gender: $form.find("[name=signup-gender]:checked").val(),
						Birthday: birthday ? new Date(birthday) : ""
					};

					ajaxQuery({
						url: URI_ACCOUNT_UPDATE,
						data: JSON.stringify(registerData),
						type: "POST",
						contentType: "application/json; charset=UTF-8",
						dataType: "json"
					}).then(function(data) {
						// Show response msg as a lightbox
						new InfoBox({
							title: 1148,
							content: 1153
						}).open();
					}, function(e) {
						// Show error msg as a lightbox
						new InfoBox({
							title: 1144,
							content: getStatus(e)
						}).open();
					});
				}
			});
		}
	});
});

define('troopjs-requirejs/template!widget/formfindpassword/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var serviceDomain = data.serviceDomain;
o += "\r\n<form role=\"form\">\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-mobile\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-mobile\" name=\"signup-mobile\" autofocus required placeholder=\"Your Contact Number\" data-weave=\"widget/blurb/main(1137, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group imageverificationcode cf\">\r\n            <label for=\"sendsms-image-verificationcode\" data-weave=\"widget/blurb/main(1614)\">Verification code:</label>\r\n            <img src=\"" +serviceDomain+ "/common/image\" width=\"80\" height=\"31\" alt=\"Verification code\" data-action=\"refreshimagesecurity\">\r\n            <input type=\"text\" class=\"form-control\" id=\"sendsms-image-verificationcode\" data-action=\"checkverificationcode\" name=\"sendsms-image-verificationcode\" placeholder=\"Please type the verification code.\" required data-weave=\"widget/blurb/main(1344, 'placeholder')\">\r\n        </div>\r\n        <button id=\"verification-code\" class=\"btn btn-default\" data-action=\"sendverificationcode\" data-weave=\"widget/blurb/main(1084)\">SEND VERIFICATION CODE</button>\r\n        <span class=\"sending hid\">\r\n            <img src=\"" +cacheServer+ "/images/waiting.gif\" width=\"32\" height=\"32\" alt=\"\">\r\n            <span data-weave=\"widget/blurb/main(1341)\">Sending...</span>\r\n            <span data-action=\"countdown\"></span>\r\n        </span>\r\n    </div>\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-verificationcode\" data-weave=\"widget/blurb/main(1085)\">Security code:</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-verificationcode\" name=\"signup-verificationcode\" placeholder=\"Input verification code sent to your mobile phone\" required data-weave=\"widget/blurb/main(1139, 'placeholder')\">\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1177)\">RESET PASSWORD</button>\r\n    </div>\r\n\r\n    <input type=\"hidden\" id=\"blurb-1211\" data-weave=\"widget/blurb/main(1211, 'value')\" value=\"Please type the verification code we send to your mobile phone.\">\r\n    <input type=\"hidden\" id=\"blurb-1215\" data-weave=\"widget/blurb/main(1215, 'value')\" value=\"Phonenumber is required.\">\r\n    <input type=\"hidden\" id=\"blurb-1216\" data-weave=\"widget/blurb/main(1216, 'value')\" value=\"Verify that you've entered the correct phone number.\">\r\n    <input type=\"hidden\" id=\"blurb-1345\" data-weave=\"widget/blurb/main(1345, 'value')\" value=\"Please type the verification code.\">\r\n</form>"; return o; }; });
define('widget/formfindpassword/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/blurb/main",
    "template!./index.html",
    "widget/basic/jqueryext",
    "jquery.validation",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, ajaxQuery, api, InfoBox, pageUrl, cookie, Blurb, template) {
    "use strict";

    var URI_PASSWORD_CODE = api.get("forgotpasswordverificationcode");
    var URI_RESET_PASSWORD = api.get("resetpassword");

    var blurb = new Blurb($(document.body));

    function getVerificationStatus(code) {
        var status = [];
        status["-4"] = "1273";
        status["-6"] = "1182";
        status["11"] = "1191";
        status["-5"] = "1190";

        return status[code] || "";
    }

    function getStatus(code) {
        var status = [];
        status["-5"] = "1274";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            }).then(function() {
                var $form = $me.find("form");
                var $account = $form.find('[name=signup-mobile]');
                // Get account from cookie
                cookie.get("account").then(function(account) {
                    // Auto filled account
                    $account.val(account);
                });
                // Validation            
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1211 = $form.find("#blurb-1211").val();
            var m1215 = $form.find("#blurb-1215").val();
            var m1216 = $form.find("#blurb-1216").val();
            var m1345 = $form.find("#blurb-1345").val();
            $form.validate({
                rules: {
                    "signup-verificationcode": "required",
                    "sendsms-image-verificationcode": "required",
                    "signup-mobile": {
                        required: true,
                        number: true,
                        mobile: true
                    }
                },
                messages: {
                    "signup-verificationcode": m1211,
                    "sendsms-image-verificationcode": m1345,
                    "signup-mobile": {
                        required: m1215,
                        number: m1216
                    }
                },
                submitHandler: function(form) {
                    var resetPasswordData = {
                        "mobile": _.trim($form.find("#signup-mobile").val()),
                        "ValidationCode": _.trim($form.find("#signup-verificationcode").val()),
                        "language": context.language
                    };

                    ajaxQuery({
                        url: URI_RESET_PASSWORD,
                        data: resetPasswordData,
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Show msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1160,
                            closeCallback: function() {
                                pageUrl.goto("/enrollment/signin");
                            }
                        }).open();
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();
                    });
                }
            });
        },
        "dom:button[data-action=sendverificationcode]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var $sending = $me.find(".sending");
            var $imageVerification = $(".imageverificationcode");

            var $mobile = $me.find("#signup-mobile");
            var mobile = _.trim($mobile.val());

            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            var verificationCode = _.trim($verificationCode.val());

            if ($mobile.valid()&&$verificationCode.valid()) {
                ajaxQuery({
                    url: URI_PASSWORD_CODE,
                    data: {
                        "mobile": mobile,
                        "language": context.language,
                        "verifyCode": verificationCode
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("#signup-verificationcode").focus();

                        //
                        $el.hide();
                        $imageVerification.hide();
                        $sending.show();
                        var $countdown = $("[data-action=countdown]");
                        var countdownMax = 60;

                        (function countdownSending() {
                            if (countdownMax <= 0) {
                                clearTimeout(insTimeout);

                                $countdown.text("");
                                $sending.hide();
                                $imageVerification.show();
                                $el.show().text(blurb.get("1342"));

                                return;
                            }
                            var insTimeout = setTimeout(function() {
                                $countdown.text(countdownMax);
                                countdownSending();
                            }, 1000);
                            countdownMax--;
                        })();
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getVerificationStatus(e)
                    }).open();
                });
            }
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});

define('troopjs-requirejs/template!widget/formlogin/index.html',[],function() { return function template(data) { var o = "<h3><span data-weave=\"widget/blurb/main(1000)\">Sign in</span></h3>\r\n<form role=\"form\">\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-passwordrepeat\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <input type=\"text\" class=\"form-control\" name=\"login-account\" placeholder=\"Mobile\" autofocus required data-weave=\"widget/blurb/main(1140, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-passwordrepeat\" data-weave=\"widget/blurb/main(1279)\">Password:</label>\r\n            <input type=\"password\" class=\"form-control\" name=\"login-password\" placeholder=\"Password\" required data-weave=\"widget/blurb/main(1130, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <a href=\"/enrollment/forgotpassword\" data-weave=\"widget/href/main\" target=\"_blank\">\r\n                <span data-weave=\"widget/blurb/main(1083)\">I forgot my password</span>\r\n            </a>\r\n            <span>&nbsp;|&nbsp;</span>\r\n            <a href=\"/enrollment/register\" data-weave=\"widget/href/main\" target=\"_blank\">\r\n                <span data-weave=\"widget/blurb/main(1355)\">New Customers?</span>\r\n            </a>\r\n        </div>\r\n        <button type=\"submit\" id=\"su\" class=\"btn btn-lg btn-default btn-block\" data-weave=\"widget/blurb/main(1080)\">SIGN IN</button>\r\n        <input type=\"hidden\" id=\"blurb-1271\" data-weave=\"widget/blurb/main(1271, 'value')\" value=\"Mobile is required\">\r\n        <input type=\"hidden\" id=\"blurb-1272\" data-weave=\"widget/blurb/main(1272, 'value')\" value=\"Password is required\">\r\n    </div>\r\n</form>"; return o; }; });
define('widget/formlogin/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/querystring/main",
    "template!./index.html",
    "jquery.validation",
    "jquery.formatter",
    "json2",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, InfoBox, pageUrl, cookie, queryString, template) {
    "use strict";

    var URI_LOGIN = api.get("login");

    function getStatus(code) {
        var status = [];
        status["-2"] = "1178";
        status["0"] = "1179";
        status["-10"] = "1180";

        return status[code] || "";
    }

    return Widget.extend(function($element, widgetName, redirect) {
        var me = this;

        me.redirect = redirect;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var req = queryString.getRequest();
            if(req.referrer){
                me.redirect = req.referrer;
            }

            // Rendering
            me.html(template).then(function() {
                var $form = $me.find("form");
                var $account = $form.find('[name=login-account]');
                // Get account from cookie
                cookie.get("account").then(function(account) {
                    // Auto filled account
                    $account.val(account);
                });
                // Validation
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1271 = $form.find("#blurb-1271").val();
            var m1272 = $form.find("#blurb-1272").val();

            $form.validate({
                rules: {
                    "login-account": {
                        required: true
                    },
                    "login-password": "required"
                },
                messages: {
                    "login-account": {
                        required: m1271
                    },
                    "login-password": m1272
                },
                submitHandler: function(form) {
                    var account = _.trim($form.find("[name=login-account]").val());
                    var password = _.trim($form.find("[name=login-password]").val());
                    var loginData = {
                        email: account,
                        password: password
                    };
                    // Post data
                    ajaxQuery({
                        url: URI_LOGIN,
                        data: loginData,
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Set account to cookie
                        cookie.set("account", account);
                        
                        // 用于前端测试用的memberid写入cooike
                        if(data.IsFrontendTest !== undefined && data.IsFrontendTest === true){
                        	cookie.set("memberid", data.MemberId,{
                                path: "/"
                            });
                        }
                        
                        if (me.redirect) {
                            // Direct to Homepage after login
                            pageUrl.goto(me.redirect);
                        } else {
                            // Reload current page
                            window.location.reload(true);
                        }
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();
                    });
                }
            });
        }
    });
});

define('troopjs-requirejs/template!widget/formregister/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var serviceDomain = data.serviceDomain;
o += "\r\n<h3 data-weave=\"widget/blurb/main(1020)\">Create an account</h3>\r\n<form role=\"form\">\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <a href=\"/enrollment/signin\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1081)\">I have a VCLEANSE account</span>\r\n            </a>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-mobile\" data-weave=\"widget/blurb/main(1056)\">Mobile:</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-mobile\" name=\"signup-mobile\" autofocus required placeholder=\"Your Contact Number\" data-weave=\"widget/blurb/main(1137, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group imageverificationcode cf\">\r\n            <label for=\"sendsms-image-verificationcode\" data-weave=\"widget/blurb/main(1614)\">Verification code:</label>\r\n            <img src=\"" +serviceDomain+ "/common/image\" width=\"80\" height=\"31\" alt=\"Verification code\" data-action=\"refreshimagesecurity\">\r\n            <input type=\"text\" class=\"form-control\" id=\"sendsms-image-verificationcode\" data-action=\"checkverificationcode\" name=\"sendsms-image-verificationcode\" placeholder=\"Please type the verification code.\" required data-weave=\"widget/blurb/main(1344, 'placeholder')\">\r\n        </div>\r\n        <button id=\"verification-code\" class=\"btn btn-default\" data-action=\"sendverificationcode\" data-weave=\"widget/blurb/main(1084)\">SEND VERIFICATION CODE</button>\r\n        <span class=\"sending hid\">\r\n            <img src=\"" +cacheServer+ "/images/waiting.gif\" width=\"32\" height=\"32\" alt=\"\">\r\n            <span data-weave=\"widget/blurb/main(1341)\">Sending...</span>\r\n            <span data-action=\"countdown\"></span>\r\n        </span>\r\n    </div>\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group mobilesecuritycode\">\r\n            <label for=\"signup-verificationcode\" data-weave=\"widget/blurb/main(1617)\">Security code:</label>\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-verificationcode\" name=\"signup-verificationcode\" placeholder=\"Input verification code sent to your mobile phone\" required data-weave=\"widget/blurb/main(1139, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group imagesecuritycode hid cf\">\r\n            <label for=\"signup-image-verificationcode\" data-weave=\"widget/blurb/main(1085)\">Security code:</label>\r\n            <img src=\"" +serviceDomain+ "/common/image\" width=\"80\" height=\"31\" alt=\"Security code\" data-action=\"refreshimagesecurity\">\r\n            <input type=\"text\" class=\"form-control\" id=\"signup-image-verificationcode\" name=\"signup-image-verificationcode\" placeholder=\"Please type the verification code.\" required data-weave=\"widget/blurb/main(1344, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-password\" data-weave=\"widget/blurb/main(1401)\">Choose a password:</label>\r\n            <input type=\"password\" class=\"form-control\" id=\"signup-password\" name=\"signup-password\" placeholder=\"Password\" required data-weave=\"widget/blurb/main(1130, 'placeholder')\">\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"signup-passwordrepeat\" data-weave=\"widget/blurb/main(1078)\">Confirm password:</label>\r\n            <input type=\"password\" class=\"form-control\" id=\"signup-passwordrepeat\" name=\"signup-passwordrepeat\" placeholder=\"Confirm Password\" required data-weave=\"widget/blurb/main(1131, 'placeholder')\">\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-lg btn-default btn-block\" data-weave=\"widget/blurb/main(1068)\">CREATE ACCOUNT</button>\r\n    </div>\r\n\r\n    <input type=\"hidden\" id=\"blurb-1211\" data-weave=\"widget/blurb/main(1211, 'value')\" value=\"Please type the verification code we send to your mobile phone.\">\r\n    <input type=\"hidden\" id=\"blurb-1212\" data-weave=\"widget/blurb/main(1212, 'value')\" value=\"Please type a password, and then retype it to confirm.\">\r\n    <input type=\"hidden\" id=\"blurb-1213\" data-weave=\"widget/blurb/main(1213, 'value')\" value=\"Please retype password to confirm.\">\r\n    <input type=\"hidden\" id=\"blurb-1214\" data-weave=\"widget/blurb/main(1214, 'value')\" value=\"These passwords don't match.\">\r\n    <input type=\"hidden\" id=\"blurb-1215\" data-weave=\"widget/blurb/main(1215, 'value')\" value=\"Phonenumber is required.\">\r\n    <input type=\"hidden\" id=\"blurb-1216\" data-weave=\"widget/blurb/main(1216, 'value')\" value=\"Verify that you've entered the correct phone number.\">\r\n    <input type=\"hidden\" id=\"blurb-1217\" data-weave=\"widget/blurb/main(1217, 'value')\" value=\"Your phone number is already registered.\">\r\n    <input type=\"hidden\" id=\"blurb-1345\" data-weave=\"widget/blurb/main(1345, 'value')\" value=\"Please type the verification code.\">\r\n</form>"; return o; }; });
define('widget/formregister/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/basic/regex",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "template!./index.html",
    "widget/basic/jqueryext",
    "jquery.validation",
    "jquery.formatter",
    "json2",
    "underscore.string"
], function(Widget, weave, $, context, regex, ajaxQuery, api, InfoBox, pageUrl, Blurb, template) {
    "use strict";

    var URI_PHONECHECK = api.get("phonecheck");
    var URI_ACCOUNT_CREATE = api.get("account_create");
    var URI_SENDVERIFICATIONCODE = api.get("sendverificationcode");

    var SENDTIME = 2;

    var blurb = new Blurb($(document.body));

    function getStatus(code) {
        var status = [];
        status["1"] = "1187";
        status["10"] = "1181";
        status["11"] = "1182";
        status["12"] = "1183";
        status["13"] = "1184";
        status["-1"] = "1185";
        status["-2"] = "1186";
        status["-3"] = "1188";
        status["-4"] = "1189";
        status["-5"] = "1190";
        status["-6"] = "1191";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            }).then(function() {
                var $form = $me.find("form");
                // Validation
                me.validation();
            });
        },
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1211 = $form.find("#blurb-1211").val();
            var m1212 = $form.find("#blurb-1212").val();
            var m1213 = $form.find("#blurb-1213").val();
            var m1214 = $form.find("#blurb-1214").val();
            var m1215 = $form.find("#blurb-1215").val();
            var m1216 = $form.find("#blurb-1216").val();
            var m1217 = $form.find("#blurb-1217").val();
            var m1345 = $form.find("#blurb-1345").val();

            $form.validate({
                rules: {
                    "signup-verificationcode": "required",
                    "sendsms-image-verificationcode": "required",
                    "signup-image-verificationcode": "required",
                    "signup-password": "required",
                    "signup-passwordrepeat": {
                        required: true,
                        equalTo: "#signup-password"
                    },
                    "signup-mobile": {
                        required: true,
                        number: true,
                        mobile: true,
                        remote: {
                            url: URI_PHONECHECK,
                            type: "GET",
                            data: {
                                phone: function() {
                                    return $me.find("#signup-mobile").val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    "signup-verificationcode": m1211,
                    "sendsms-image-verificationcode": m1345,
                    "signup-image-verificationcode": m1345,
                    "signup-password": m1212,
                    "signup-passwordrepeat": {
                        "required": m1213,
                        "equalTo": m1214
                    },
                    "signup-mobile": {
                        "required": m1215,
                        "number": m1216,
                        "remote": m1217
                    }
                },
                submitHandler: function(form) {
                    var registerData = {
                        "password": _.trim($form.find("#signup-password").val()),
                        "mobile": _.trim($form.find("#signup-mobile").val()),
                        "ValidationCode": _.trim($form.find("#signup-verificationcode").val()),
                        "ValidateType": 1 // Mobile
                    };

                    if (SENDTIME <= 0) {
                        _.extend(registerData, {
                            "ValidationCode": _.trim($form.find("#signup-image-verificationcode").val()),
                            "ValidateType": 0 // Image
                        });
                    }

                    ajaxQuery({
                        url: URI_ACCOUNT_CREATE,
                        data: JSON.stringify(registerData),
                        contentType: "application/json; charset=UTF-8",
                        type: "POST",
                        dataType: "json"
                    }).then(function(data) {
                        // Direct to enrollment success page
                        pageUrl.goto("/enrollment/success");

                        refreshSecurity($("[data-action=refreshimagesecurity]"));
                    }, function(e) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: getStatus(e)
                        }).open();

                        refreshSecurity($("[data-action=refreshimagesecurity]:eq(1)"));
                    });
                }
            });
        },
        "dom:button[data-action=sendverificationcode]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $el = $(e.currentTarget);
            var $sending = $me.find(".sending");
            var $imageVerification = $(".imageverificationcode");

            var $mobile = $me.find("#signup-mobile");
            var mobile = _.trim($mobile.val());

            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            var verificationCode = _.trim($verificationCode.val());

            if ($mobile.valid()) {
                if (SENDTIME > 0) {
                    if ($verificationCode.valid()) {
                        ajaxQuery({
                            url: URI_SENDVERIFICATIONCODE,
                            data: {
                                "mobile": mobile,
                                language: context.language,
                                verifyCode: verificationCode
                            },
                            type: "POST",
                            dataType: "json"
                        }).then(function(data) {
                            $me.find("#signup-verificationcode").focus();

                            //
                            $el.hide();
                            $imageVerification.hide();
                            $verificationCode.val("");
                            $sending.show();
                            var $countdown = $("[data-action=countdown]");
                            var countdownMax = 60;

                            (function countdownSending() {
                                if (countdownMax <= 0) {
                                    clearTimeout(insTimeout);

                                    $countdown.text("");
                                    $sending.hide();
                                    $el.show();
                                    if (SENDTIME > 0) {
                                        $el.text(blurb.get("1342"));
                                        refreshSecurity($("[data-action=refreshimagesecurity]:eq(0)"));
                                        $imageVerification.show();
                                    } else {
                                        $el.text(blurb.get("1343"));
                                    }

                                    return;
                                }
                                var insTimeout = setTimeout(function() {
                                    $countdown.text(countdownMax);
                                    countdownSending();
                                }, 1000);
                                countdownMax--;
                            })();

                            // Send time count, while API success
                            SENDTIME--;
                        }, function(e) {
                            // Show error msg as a lightbox
                            new InfoBox({
                                title: 1144,
                                content: getStatus(e)
                            }).open();
                        });
                    }
                } else {
                    $(".mobilesecuritycode").hide();
                    refreshSecurity($("[data-action=refreshimagesecurity]:eq(1)"));
                    $(".imagesecuritycode").show();
                }
            }
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});

define('troopjs-requirejs/template!widget/formsafety/index.html',[],function() { return function template(data) { var o = "";
   var domain = data.serviceDomain;
o += "\r\n<form class=\"form-horizontal\" role=\"form\">\r\n    <div class=\"form-section\">\r\n        <div class=\"form-group\">\r\n            <div class=\"col-sm-12 text-center\">\r\n                <h3 class=\"form-account-info\" data-weave=\"widget/blurb/main(1154)\">Change a new password!</h3>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-password\" data-weave=\"widget/blurb/main(1077)\">Choose a password:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"password\" class=\"form-control\" id=\"signup-password\" name=\"signup-password\" placeholder=\"Password\" required data-weave=\"widget/blurb/main(1130, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-passwordrepeat\" data-weave=\"widget/blurb/main(1078)\">Confirm password:</label>\r\n            <div class=\"col-sm-7\">\r\n                <input type=\"password\" class=\"form-control\" id=\"signup-passwordrepeat\" name=\"signup-passwordrepeat\" placeholder=\"Confirm Password\" required data-weave=\"widget/blurb/main(1131, 'placeholder')\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"signup-image-verificationcode\" data-weave=\"widget/blurb/main(1085)\">Security code:</label>\r\n            <div class=\"col-sm-3\">\r\n                <input type=\"text\" class=\"form-control\" id=\"signup-image-verificationcode\" name=\"signup-image-verificationcode\" placeholder=\"\" required data-weave=\"widget/blurb/main(1464, 'placeholder')\">\r\n            </div>\r\n            <div class=\"col-sm-2 text-right\">\r\n                <img src=\"" +domain+ "/common/image\" width=\"80\" height=\"31\" alt=\"Security code\" data-action=\"refreshimagesecurity\">\r\n            </div>\r\n            <div class=\"col-sm-2 text-left\"> <a class=\"btn btn-link\" data-action=\"refreshimagesecurity\" data-weave=\"widget/blurb/main(1463)\">Change</a>\r\n            </div>\r\n        </div>\r\n        <hr>\r\n        <div class=\"form-group\">\r\n            <label class=\"col-sm-3 control-label\" for=\"\"></label>\r\n            <div class=\"col-sm-7 text-center\">\r\n                <button type=\"submit\" class=\"btn btn-lg btn-default btn-action\" data-weave=\"widget/blurb/main(1079)\">SAVE</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <input type=\"hidden\" id=\"blurb-1213\" data-weave=\"widget/blurb/main(1213, 'value')\" value=\"Please retype password to confirm.\">\r\n    <input type=\"hidden\" id=\"blurb-1214\" data-weave=\"widget/blurb/main(1214, 'value')\" value=\"These passwords don't match.\">\r\n</form>"; return o; }; });
define('widget/formsafety/main',["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"widget/cookie/main",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/infobox/main",
	"template!./index.html",
	"jquery.validation",
	"jquery.ui.datepicker",
	"json2",
	"underscore.string"
], function(Widget, weave, $, context, cookie, ajaxQuery, api, InfoBox, template) {
	"use strict";

	var URI_ACCOUNT = api.get("account");
	var URI_ACCOUNT_CHGPWD = api.get("account_chgpwd");

	function getStatus(code) {
		var status = [];
		status["-1"] = "1462";
		status["1000"] = "1462";
		status["-5"] = "1190";

		return status[code] || "";
	}

	function refreshSecurity() {
		var $image = $("[data-action=refreshimagesecurity]");

		var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
		$image.attr("src", src);
	}

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;


			cookie.get("memberid").then(function() {
				me.html(template, {
					serviceDomain: context.serviceDomain
				}).then(function() {
					me.validation();
				});
			});
		},
		"validation": function() {
			var me = this;
			var $me = me.$element;

			var $form = $me.find("form");

			var m1213 = $form.find("#blurb-1213").val();
			var m1214 = $form.find("#blurb-1214").val();


			var $image = $("[data-action=refreshimagesecurity]");
			var $vcode = $form.find("#signup-image-verificationcode");
			var $pwd = $form.find("#signup-password");
			var $pwdre = $form.find("#signup-passwordrepeat");

			$form.validate({
				rules: {
					"signup-passwordrepeat": {
						equalTo: "#signup-password"
					}
				},
				messages: {
					"signup-passwordrepeat": {
						required: m1213,
						equalTo: m1214
					}
				},
				submitHandler: function(form) {
					var registerData = {
						password: _.trim($pwd.val()),
						validationcode: _.trim($vcode.val())
					};

					cookie.get("memberid").then(function() {
						ajaxQuery({
							url: URI_ACCOUNT_CHGPWD,
							data: JSON.stringify(registerData),
							type: "POST",
							contentType: "application/json; charset=UTF-8",
							dataType: "json"
						}).then(function(data) {
							// Show response msg as a lightbox
							new InfoBox({
								title: 1148,
								content: 1465
							}).open().then(function() {
								refreshSecurity($image);
								$vcode.val("");
								$pwd.val("");
								$pwdre.val("");
							});
						}, function(e) {
							// Show error msg as a lightbox
							new InfoBox({
								title: 1144,
								content: getStatus(e)
							}).open().then(function() {
								refreshSecurity($image);
								$vcode.val("");
								if (e === "-5") {
									$pwd.val("");
									$pwdre.val("");
								}
							});
						});
					});
				}
			});
		},
		"dom:[data-action=refreshimagesecurity]/click": function(e) {
			e.preventDefault();

			var me = this;
			refreshSecurity();
		}
	});
});

define('troopjs-requirejs/template!widget/giftcard/mobile/index.html',[],function() { return function template(data) { var o = "<div class=\"giftcard-main\">\r\n    <div id=\"carousel-example-generic\" class=\"carousel slide\" data-interval=\"false\">\r\n        <ol class=\"carousel-indicators\">\r\n            <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" data-weave=\"widget/blurb/main(1122)\">Male</li>\r\n            <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\" class=\"active\" data-weave=\"widget/blurb/main(1121)\">Female</li>\r\n        </ol>\r\n        <p class=\"giftcard-content-description\" data-weave=\"widget/blurb/main(p1005)\"></p>\r\n        <div class=\"carousel-inner\" role=\"listbox\">\r\n            <div class=\"item swipe giftcard-carousel\">\r\n                <div class=\"row swipe-wrap\">\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1002)\">1 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;425</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/19/days/1\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1010)\"></p>\r\n                    </div>\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1003)\">3 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;1275</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/19/days/3\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1011)\"></p>\r\n                    </div>\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1004)\">5 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;2125</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/19/days/5\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1012)\"></p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item swipe giftcard-carousel active\">\r\n                <div class=\"row swipe-wrap\">\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1002)\">1 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;325</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/20/days/1\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1010)\"></p>\r\n                    </div>\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1003)\">3 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;975</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/20/days/3\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1011)\"></p>\r\n                    </div>\r\n                    <div class=\"giftcard-item\">\r\n                        <h5><b data-weave=\"widget/blurb/main(p1004)\">5 DAY</b>\r\n                        </h5>\r\n                        <p>\r\n                            <img data-weave=\"widget/basic/imageswitch\" src=\"/images/blank.png\" data-src=\"images/giftcard/giftcard.jpg\" height=\"150\" alt=\"\">\r\n                            <span class=\"giftcard-price\">&yen;1625</span>\r\n                        </p>\r\n                        <p class=\"buyButton\">\r\n                            <a href=\"/order/receiver\" data-weave=\"widget/href/main widget/blurb/main(1050)\" data-hash=\"packageid/10/groupid/20/days/5\" class=\"btn btn-default btn-lg btn-block\">BUY</a>\r\n                        </p>\r\n                        <p data-weave=\"widget/blurb/main(p1012)\"></p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/giftcard/mobile/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html",
    "swipe"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                var $carousel = $me.find('.giftcard-carousel');

                //                    var carouselSwipe = new Swipe($carousel.get(1));
                $carousel.Swipe();
            });
        },
        "dom:.carousel-indicators li/click": function(e) {
            var me = this;
            var $me = me.$element;
            if ($(e.currentTarget).index() === 0) {
                var $carousel = $me.find('.giftcard-carousel');
                //                    setTimeout(function(){
                //                        new Swipe($carousel.get(0));
                //                    },10);
                new Swipe($carousel.get(0));
                //                $carousel.Swipe();
            }
        }
    });
});

define('troopjs-requirejs/template!widget/homeslider/index.html',[],function() { return function template(data) { var o = "";
  var carouselHeight = data.carouselHeight || 500;
  var zoomRate = carouselHeight/500;

  var events = data.events;
  var domain = data.domain;
  var cacheServer = data.cacheServer;
  var language = data.language;

  var i = 0;
  var iLens = events.length;

  var j = 0;
  var jLens = events.length;

  var event;
o += "\r\n<div id=\"home-carousel\" class=\"carousel slide\" data-ride=\"carousel\">\r\n  "; if (iLens > 1) { o += "\r\n  <ol class=\"carousel-indicators\">\r\n    "; for(i=0; i<iLens; i++) { o += "\r\n        <li data-target=\"#home-carousel\" data-slide-to=\"" +i+ "\" class=\""; if (i===0) { o += "active"; } o += "\"></li>\r\n    "; } o += "\r\n  </ol>\r\n  "; } o += "\r\n  <div class=\"carousel-inner\">\r\n    "; for(j=0; j<jLens; j++) { 
          event = events[j];
    o += "\r\n      <div class=\"item "; if (j===0) { o += "active"; } o += "\" style=\"\r\n        background-color: " +(event.color || '#fff')+ ";\r\n        "; if (event.bg) { o += "background-image: url(" +event.bg+ ");"; } o += "\r\n        background-position: center center; \r\n        \">\r\n        "; 
          switch (event.type.toLowerCase()) { 
            case "picture": renderPicture();
              break;
            case "map": renderMap();
              break;
            case "edm": renderEdm();
              break;
            case "spring": renderSpring();
              break;
            default: break;
          }
        o += "\r\n      </div>\r\n    "; } o += "\r\n  </div>\r\n</div>\r\n\r\n"; function renderPicture() { o += "\r\n  <a "; if (event.url) { o += "href=\"" +event.url+ "\""; } o += " data-weave=\"widget/href/main\" ";if(event.blank){o += "target=\"_blank\"";}o += ">\r\n    <img class=\"align-c\" src=\"" +cacheServer+ "/" +event.src+ "\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"" +event.title+ "\" title=\"" +event.title+ "\">\r\n  </a>\r\n"; } o += "\r\n\r\n"; function renderMap() { 
    var k = 0;
    var kLens = event.url.length;
  o += "\r\n  <img class=\"align-c\" src=\"" +cacheServer+ "/" +event.src+ "\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"" +event.title+ "\" title=\"" +event.title+ "\" usemap=\"#" +event.id+ "\">\r\n  <map name=\"" +event.id+ "\" id=\"" +event.id+ "\">\r\n    "; for(k=0; k<kLens; k++) { 
      var mapUrl = event.url[k];
    o += "\r\n      <area shape=\"" +mapUrl.shape+ "\" coords=\"" +parseCoords(mapUrl.coords)+ "\" "; if (mapUrl.href) { o += "href=\"" +mapUrl.href+ "\""; } o += " data-weave=\"widget/href/main\" alt=\"" +mapUrl.alt+ "\" />\r\n    "; } o += "\r\n  </map>\r\n"; } o += "\r\n\r\n"; function renderEdm() { o += "\r\n  <a href=\"" +domain+ "/mail/" +event.date+ "/mail" +(event.sep_lng ? '_' + language : '')+ ".html\" ";if(event.blank){o += "target=\"_blank\"";}o += ">\r\n    <img class=\"align-c\" src=\"" +cacheServer+ "/mail/" +event.date+ "/images/banner_h_250_w.jpg\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"" +event.title+ "\" title=\"" +event.title+ "\">\r\n  </a>\r\n"; } o += "\r\n\r\n"; function renderSpring() { o += "\r\n  <div class=\"align-c text-center homeslider-spring\" style=\"height: " +carouselHeight+ "px; width: " +zoomRate*878+ "px;\">\r\n    <a "; if (event.url) { o += "href=\"" +event.url+ "\""; } o += " data-weave=\"widget/href/main\" ";if(event.blank){o += "target=\"_blank\"";}o += ">\r\n      <img src=\"" +cacheServer+ "/" +event.src+ "\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"" +event.title+ "\" title=\"" +event.title+ "\">\r\n    </a>\r\n    <a "; if (event.url) { o += "href=\"" +event.url+ "\""; } o += " data-weave=\"widget/href/main\" ";if(event.blank){o += "target=\"_blank\"";}o += " class=\"homeslider-spring-absbox\" style=\"left: 0; height: " +carouselHeight+ "px; width: " +zoomRate*435+ "px;\">\r\n      <img class=\"slide out homeslider-spring-absbox\" src=\"" +cacheServer+ "/images/home/spring/spring_slide_l.jpg\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px; right: 0;\" alt=\"\">\r\n    </a>\r\n    <a "; if (event.url) { o += "href=\"" +event.url+ "\""; } o += " data-weave=\"widget/href/main\" ";if(event.blank){o += "target=\"_blank\"";}o += " class=\"homeslider-spring-absbox\" style=\"right: 0; height: " +carouselHeight+ "px; width: " +zoomRate*443+ "px;\">\r\n      <img class=\"slide out reverse homeslider-spring-absbox\" src=\"" +cacheServer+ "/images/home/spring/spring_slide_r.jpg\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px; left: 0;\" alt=\"\">\r\n    </a>\r\n    <div class=\"homeslider-spring-absbox\" style=\"height: " +carouselHeight+ "px; width: " +zoomRate*161+ "px; left: -" +zoomRate*161+ "px;\">\r\n      <img src=\"" +cacheServer+ "/images/home/spring/spring_fixed_l.jpg\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"\">\r\n    </div>\r\n    <div class=\"homeslider-spring-absbox\" style=\"height: " +carouselHeight+ "px; width: " +zoomRate*161+ "px; right: -" +zoomRate*161+ "px;\">\r\n      <img src=\"" +cacheServer+ "/images/home/spring/spring_fixed_r.jpg\" height=\"" +carouselHeight+ "\" style=\"height: " +carouselHeight+ "px;\" alt=\"\">\r\n    </div>\r\n  </div>\r\n"; } o += "\r\n\r\n"; function parseCoords(coords){
  var arrCoords = coords.split(",");
  var i = 0;
  var iLens = arrCoords.length;
  var arrZoomCoords = [];
  for(i=0; i<iLens; i++){
    arrZoomCoords.push(parseInt(arrCoords[i], 10) * zoomRate);
  }
  return arrZoomCoords.join(",");
} o += "\r\n\r\n<style type=\"text/css\">\r\n  .homeslider-spring {\r\n    position: relative;\r\n  }\r\n\r\n  .homeslider-spring .homeslider-spring-absbox {\r\n    display: block;\r\n    overflow: hidden; \r\n    position: absolute; \r\n    top: 0; \r\n  }\r\n\r\n  .homeslider-spring .slide.out {\r\n    transform: translateX(0);\r\n    -moz-transform: translateX(0);\r\n    -webkit-transform: translateX(0);\r\n    -o-transform: translateX(0);\r\n  }\r\n  .homeslider-spring .slide.out.reverse {\r\n    transform: translateX(0);\r\n    -moz-transform: translateX(0);\r\n    -webkit-transform: translateX(0);\r\n    -o-transform: translateX(0);\r\n  }\r\n  .homeslider-spring .out {\r\n    animation-timing-function: ease-in-out; \r\n    -moz-animation-timing-function: ease-in-out; \r\n    -webkit-animation-timing-function: ease-in-out; \r\n    -o-animation-timing-function: ease-in-out; \r\n\r\n    animation-duration: 1000ms; \r\n    -moz-animation-duration: 1000ms; \r\n    -webkit-animation-duration: 1000ms; \r\n    -o-animation-duration: 1000ms; \r\n\r\n    animation-delay: 2000ms;\r\n    -moz-animation-delay: 2000ms;\r\n    -webkit-animation-delay: 2000ms;\r\n    -o-animation-delay: 2000ms;\r\n\r\n    animation-fill-mode: forwards;\r\n    -moz-animation-fill-mode: forwards;\r\n    -webkit-animation-fill-mode: forwards;\r\n    -o-animation-fill-mode: forwards;\r\n  }\r\n</style>"; return o; }; });
define('widget/homeslider/main',["troopjs-browser/component/widget",
  "jquery",
  "context",
  "template!./index.html",
  "underscore"
], function(Widget, $, context, template) {
  "use strict";

  var CAROUSEL_INTERVAL = 10000;
  var CAROUSEL_HEIGHT = 500;
  var CAROUSEL_HEIGHT_MIN = 360;

  /*{
    "type": "spring",
    "blank": false,
    "url": "/cleanse",
    "src": "images/home/spring/spring_content.jpg",
    "title": "维果清祝您新年快乐！购买5天清体疗程，便获得一天礼品卡！",
    "color": "#fff",
    "endDate": new Date("2015-02-19 00:00:00")
  }, {
    "type": "map",
    "id": "workshopmap",
    "blank": true,
    "url": [{
      "shape": "rect",
      "coords": "640,395,845,465",
      "href": "/workshop",
      "alt": "报名入口"
    }],
    "src": "images/home/homeslider_workshop_03.jpg",
    "title": "报名参加一场免费健康Lifestyle工作坊吧！",
    "color": "#fff"
  }*/
  // type: edm/picture/customize
  var EVENTS = [{
    "type": "picture",
    "id": "brand",
    "blank": false,
    "url": "/cleanse",
    "src": "images/home/homeslider_brand.jpg",
    "title": "真正的百分百冷压清体蔬果汁",
    "color": "#fff"
  }, {
    "type": "picture",
    "id": "juicemonday",
    "blank": false,
    "url": "/juicemonday",
    "src": "images/home/homeslider_juicemonday.jpg",
    "title": "周一果汁日：请提一下，清凉一夏！",
    "color": "#fff"
  }, {
    "type": "picture",
    "id": "workshopmap",
    "blank": false,
    "url": "/workshop",
    "src": "images/home/homeslider_workshop_03.jpg",
    "title": "报名参加一场免费健康Lifestyle工作坊吧！",
    "color": "#fff"
  }];

  // Homepage, at least visible area
  function getCarouselHeight() {
    var $homepage = $(".homepage");
    if (!$homepage || $homepage.length <= 0) {
      return CAROUSEL_HEIGHT;
    }

    var viewHeight = $(window).height() || 0;
    var headerHeight = $(".body").position().top || 0;
    var eventHeight = $(".home-event").height() || 0;
    // Slider height
    var carouselHeight = viewHeight - headerHeight - eventHeight;

    if (carouselHeight < CAROUSEL_HEIGHT_MIN) {
      carouselHeight = CAROUSEL_HEIGHT_MIN;
    } else if (carouselHeight > CAROUSEL_HEIGHT) {
      carouselHeight = CAROUSEL_HEIGHT;
    }

    return carouselHeight;
  }

  return Widget.extend({
    "sig/start": function() {
      var me = this;
      var $me = me.$element;

      var domain = context.domain;
      var cacheServer = context.cacheServer;
      var language = context.language;

      var now = new Date();
      var activeEvents = [];
      _.each(EVENTS, function(e, i) {
        // Update cache
        e.src += "?v=" + context.cacheKey;
        // Goto next loop while expired
        if (!e.endDate || now < e.endDate) {
          activeEvents.push(e);
        }
      });

      me.html(template, {
        "events": activeEvents,
        "carouselHeight": getCarouselHeight(),
        "domain": domain,
        "cacheServer": cacheServer,
        "language": language
      }).then(function() {
        $me.find('#home-carousel').carousel({
          interval: CAROUSEL_INTERVAL
        });
      });
    }
  });
});
define('widget/href/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/sitemap/main"
], function(Widget, $, context, sitemap) {
    "use strict";

    return Widget.extend(function($element, widgetName, byLanguage) {
        var me = this;
        me.byLanguage = byLanguage || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var href = $me.attr("href");
            var query = $me.data("query");
            var hash = $me.data("hash");

            if (!href ||
                href.indexOf("http://") >= 0 ||
                href.indexOf("https://") >= 0
            ) {
                return;
            }

            // Attach Language
            if (me.byLanguage) {
                href = href + "_" + context.language;
            }

            href = sitemap.get(href);
            if (query) {
                href += "?" + query;
            }
            if (hash) {
                href += "#" + hash;
            }

            // Reset href
            $me.attr("href", href);
        }
    });
});

define('troopjs-requirejs/template!widget/juice/juices/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer || "";
    var language = data.language;

    var COLUMN = 2;
    var PIC_URL = cacheServer + "/images/juice/33x130/";

    var products = data.products;
    // Products
    var i = 0;
    var iLens = products.length;
    // Row
    var j = 0;
    var jLens = Math.ceil(iLens/COLUMN);
    // Products in Row
    var k = 0;
    var kLens = 0;
o += "\r\n<div class=\"juicelist\">\r\n    "; for (j = 0; j < jLens; j++) { o += "\r\n        <ul class=\"hor float-abs\">\r\n            ";
                k = COLUMN * j;
                kLens = COLUMN + k;
                kLens = kLens > iLens ? iLens : kLens;
                for(i = k; i < kLens; i++) {
                    renderProduct(products[i]);
                }
            o += "\r\n        </ul>\r\n    "; } o += "\r\n</div>\r\n\r\n"; function renderProduct(product) { o += "\r\n    <li>\r\n        <div class=\"float-abs juicelist-juice\">\r\n            <div class=\"juicebottle juicebottle-47\">\r\n                <a href=\"/juice/" +product.Url+ "\" data-weave=\"widget/href/main\">\r\n                    <img src=\"" +PIC_URL + product.Image+ "\" width=\"33\" height=\"130\" alt=\"" +product.Name+ "\" title=\"" +product.Name+ "\">\r\n                </a>\r\n            </div>\r\n            <div class=\"orderedit-products-text\">\r\n                <p class=\"orderedit-products-name\">\r\n                    <a href=\"/juice/" +product.Url+ "\" data-weave=\"widget/href/main\" class=\"link-text\"><strong>" +product.Name+ "</strong></a>\r\n                    <!--strong class=\"orderedit-products-price number-price\">&yen; " +product.Price+ "</strong-->\r\n                </p>\r\n                <p class=\"orderedit-products-des\">\r\n                    <a href=\"/juice/" +product.Url+ "\" data-weave=\"widget/href/main\" class=\"link-text\">" +(product.Description || "")+ "</a>\r\n                    <br>\r\n                    <a href=\"/juice/" +product.Url+ "\" data-weave=\"widget/href/main widget/blurb/main(1097)\" class=\"link-fn\">+ Learn more</a>\r\n                </p>\r\n                <p class=\"orderedit-action\">\r\n                    <a class=\"btn btn-default\" href=\"/cleanse\" data-weave=\"widget/href/main widget/blurb/main(1220)\" data-hash=\"category/0\">ORDER NOW</a>\r\n                </p>\r\n            </div>\r\n        </div>\r\n    </li>\r\n"; }  return o; }; });
define('widget/juice/juices/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "template!./index.html"
], function(Widget, $, when, context, ajaxQuery, api, template) {
    "use strict";

    var URI_PRODUCT = api.get("product_loadbycategory");

    return Widget.extend(function($element, widgetName, categoryId) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            ajaxQuery({
                url: URI_PRODUCT,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var Products = data.Products;

                me.html(template, {
                    "products": Products,
                    "cacheServer": context.cacheServer,
                    "language": context.language
                }).then(function () {

                });
            });

        }
    });
});

define('troopjs-requirejs/template!widget/juice/index.html',[],function() { return function template(data) { var o = "";
    var categories = data.categories;

    var i = 0;
    var iLens = categories.length;

    var j = 0;
    var jLens = categories.length;
o += "\r\n<ul class=\"nav nav-tabs\" data-weave=\"widget/basic/bootstrap-tab\">\r\n    ";
        for(i = 0; i < iLens; i++){
            appendCategory(categories[i], i+1, iLens);
        }
    o += "\r\n</ul>\r\n<div class=\"tab-content\">\r\n    ";
        for(j = 0; j < jLens; j++){
            appendJuices(categories[j]);
        }
    o += "\r\n</div>\r\n\r\n"; function appendCategory(category, sequence, lens) { o += "\r\n    <li class=\""; if (category.IsDefault) { o += "active"; } o += " "; if (sequence === lens) { o += "last"; } o += "\">\r\n        "; if (category.Id === 0) { o += "\r\n            <a href=\"#category_" + category.Id + "\" data-id=\"" + category.Id + "\" data-toggle=\"tab\" data-action=\"customize\">" + category.Name + "</a>\r\n        "; } else { o += "\r\n            <a href=\"#category_" + category.Id + "\" data-id=\"" + category.Id + "\" data-toggle=\"tab\">" + category.Name + "</a>\r\n        "; } o += "\r\n    </li>\r\n"; } o += "\r\n\r\n"; function appendJuices(category) { o += "\r\n    <div class=\"tab-pane "; if (category.IsDefault) { o += "active"; } o += "\" id=\"category_" + category.Id + "\">\r\n        <div class=\"cleanse-about\">\r\n            " +(category.Description || "&nbsp;")+ "\r\n        </div>\r\n        <div class=\"cleanse-cols cf\" data-weave=\"widget/juice/juices/main(categoryId)\" data-category-id=\"" + category.Id + "\"></div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/juice/main',["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/pageurl/main",
	"widget/cookie/main",
	"widget/troophash/main",
	"template!./index.html"
], function(Widget, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, template) {
	"use strict";

	var URI_PRODUCT_CATEGORY = api.get("product_category");

	return Widget.extend(function($element, widgetName) {
		var me = this;

		me.preventRoute = false;
	}, {
		"hub:memory/route": function(uriPath, uriEvent) {
			var me = this;
			var $me = me.$element;

			var hashPath = uriPath.path || "category/1".split("/");
			var hashMap = troopHash.parse(hashPath);
			var categoryId = parseInt(hashMap.category, 10);

			// prvrt: Prevent hash route event
			if (me.preventRoute) {
				return;
			}

			ajaxQuery({
				url: URI_PRODUCT_CATEGORY,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}, $me).then(function(data) {
				var categories = data.Categories;

				// Set default
				var i = 0;
				var iLens = categories.length;
				for (i = 0; i < iLens; i++) {
					if (categories[i].Id === categoryId) {
						categories[i].IsDefault = true;
					}
				}

				// Render
				me.html(template, {
					"categories": categories
				}).then(function() {});
			});
		},
		"dom:a[data-toggle=tab]/click": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;
			var $el = $(e.currentTarget);

			var categoryId = $el.data("id");

			//return;
			me.preventRoute = true;
			troopHash.extend({
				"category": categoryId
			});
		}
	});
});

define('troopjs-requirejs/template!widget/juicemonday/index.html',[],function() { return function template(data) { var o = "";
    var startDate = data.startDate || "";
    var mondays = data.mondays || "";
o += "\r\n<div id=\"carousel-juicemonday\" class=\"carousel carousel-clear slide\" data-interval=\"false\">\r\n    <ol class=\"carousel-indicators\">\r\n        <li data-target=\"#carousel-juicemonday\" data-slide-to=\"0\" class=\"active juicemonday-pkg-1\" data-weave=\"widget/blurb/main(p1001)\">\r\n            女士全断食套餐\r\n        </li>\r\n        <li data-target=\"#carousel-juicemonday\" data-slide-to=\"1\" class=\"juicemonday-pkg-2\" data-weave=\"widget/blurb/main(p1002)\">\r\n            女士半断食套餐\r\n        </li>\r\n        <li data-target=\"#carousel-juicemonday\" data-slide-to=\"2\" class=\"juicemonday-pkg-3\" data-weave=\"widget/blurb/main(p1003)\">\r\n            男士全断食套餐\r\n        </li>\r\n        <li data-target=\"#carousel-juicemonday\" data-slide-to=\"3\" class=\"juicemonday-pkg-4\" data-weave=\"widget/blurb/main(p1004)\">\r\n            男士半断食套餐\r\n        </li>\r\n    </ol>\r\n    <div class=\"carousel-inner\" role=\"listbox\">\r\n        <div class=\"item juicemonday-pkg active\">\r\n            <div>\r\n                <img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch\" data-src=\"images/event/juicemonday/pkg_female_full.jpg\" alt=\"女士全断食套餐\" width=\"350\" height=\"200\">\r\n            </div>\r\n            <p class=\"juicemonday-text\">\r\n                经典冷压口味：以6瓶蔬果汁代替一天的日常餐食，是简单易行的清体入门基础套餐。<br>在短期内将大量的、高质量的蔬果、坚果营养注入身体，给自己的器官“放个假”。\r\n            </p>\r\n            <p class=\"juicemonday-text\">\r\n                总金额：1625元 （含5次运费）<br>\r\n                默认外送时间为：<span data-action=\"juicemondaydate\">" + mondays+ "</span>\r\n            </p>\r\n            <div class=\"juicemonday-action\">\r\n                <a data-weave=\"widget/href/main\" href=\"/order/receiver\" data-hash=\"packageid/3/groupid/6/weekDays/1/deliveryStartDate/" +startDate+ "/duration/5/event/jmon\" class=\"btn btn-default btn-lg btn-action\">确认订餐</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"item juicemonday-pkg\">\r\n            <div>\r\n                <img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch\" data-src=\"images/event/juicemonday/pkg_female_half.jpg\" alt=\"女士半断食套餐\" width=\"350\" height=\"200\">\r\n            </div>\r\n            <p class=\"juicemonday-text\">\r\n                经典冷压口味：用4瓶蔬果汁替代一日中的任意两餐，而第三餐则清淡进食，<br>这种做法可以帮助您觉得健康饮食“简单易行”。\r\n            </p>\r\n            <p class=\"juicemonday-text\">\r\n                总金额：1125元 （含5次运费）<br>\r\n                默认配送时间为：<span data-action=\"juicemondaydate\">" + mondays+ "</span>\r\n            </p>\r\n            <div class=\"juicemonday-action\">\r\n                <a data-weave=\"widget/href/main\" href=\"/order/receiver\" data-hash=\"packageid/3/groupid/6/weekDays/1/deliveryStartDate/" +startDate+ "/duration/5/event/jmon\" class=\"btn btn-default btn-lg btn-action\">确认订餐</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"item juicemonday-pkg\">\r\n            <div>\r\n                <img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch\" data-src=\"images/event/juicemonday/pkg_male_full.jpg\" alt=\"男士全断食套餐\" width=\"350\" height=\"200\">\r\n            </div>\r\n            <p class=\"juicemonday-text\">\r\n                经典冷压口味：以8瓶蔬果汁代替一天的日常餐食简单易行的清体入门基础套餐，<br>在短期内将大量的、高质量的蔬果、坚果营养注入身体，给自己的器官“放个假”。\r\n            </p>\r\n            <p class=\"juicemonday-text\">\r\n                总金额：2125元 （含5次运费）<br>\r\n                默认配送时间为：<span data-action=\"juicemondaydate\">" + mondays+ "</span>\r\n            </p>\r\n            <div class=\"juicemonday-action\">\r\n                <a data-weave=\"widget/href/main\" href=\"/order/receiver\" data-hash=\"packageid/3/groupid/3/weekDays/1/deliveryStartDate/" +startDate+ "/duration/5/event/jmon\" class=\"btn btn-default btn-lg btn-action\">确认订餐</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"item juicemonday-pkg\">\r\n            <div>\r\n                <img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch\" data-src=\"images/event/juicemonday/pkg_male_half.jpg\" alt=\"男士半断食套餐\" width=\"350\" height=\"200\">\r\n            </div>\r\n            <p class=\"juicemonday-text\">\r\n                经典冷压口味：用5瓶蔬果汁替代一日中的任意两餐，而第三餐则清淡进食，<br>这种做法可以帮助您觉得健康饮食“简单易行”。\r\n            </p>\r\n            <p class=\"juicemonday-text\">\r\n                总金额：1375元 （含5次运费）<br>\r\n                默认配送时间为：<span data-action=\"juicemondaydate\">" + mondays+ "</span>\r\n            </p>\r\n            <div class=\"juicemonday-action\">\r\n                <a data-weave=\"widget/href/main\" href=\"/order/receiver\" data-hash=\"packageid/3/groupid/3/weekDays/1/deliveryStartDate/" +startDate+ "/duration/5/event/jmon\" class=\"btn btn-default btn-lg\">确认订餐</a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/juicemonday/main',["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/time/main",
	"template!./index.html"
], function(Widget, $, context, Time, template) {
	"use strict";

	// Time function
	var INS_TIME = new Time();

	return Widget.extend({
		"sig/start": function() {
			var me = this;

			var now = new Date();
			var startMonday;
			if (INS_TIME.getDayOfWeekFromMonday(now) < 6) {
				startMonday = INS_TIME.getWeekStartDate(INS_TIME.addDay(now, 7));
			} else {
				startMonday = INS_TIME.addDay(now, 8);
			}

			var juiceMondays = [];
			for (var i = 0; i < 5; i++) {
				juiceMondays.push(INS_TIME.getStrDate(INS_TIME.addDay(startMonday, 7 * i)));
			}

			// Render
			me.html(template, {
				"startDate": INS_TIME.getStrDate(startMonday, "-"),
				"mondays": juiceMondays.join("、")
			});
		}
	});
});
define('widget/language/init',["troopjs-core/component/base",
    "jquery",
    "widget/cookie/main"
], function(Component, $, cookie) {
    "use strict";

    var LANGUAGE_EN_US = "en-us";
    var LANGUAGE_ZH_CN = "zh-cn";

    var COOKIE_LNG_EXPIRES = 365;

    function saveLngToCookie(toLng) {
        cookie.set('lng', toLng, {
            expires: COOKIE_LNG_EXPIRES,
            path: '/'
        });
    }

    return Component.extend({
        "init": function() {
            // Get browser language
            var language = navigator.language;
            // For IE
            if (!language) {
                language = navigator.browserLanguage;
            }
            // CN
            if (language.toLowerCase() === LANGUAGE_EN_US) {
                saveLngToCookie("en");
                return "en";
            } else {
                saveLngToCookie("cn");
                return "cn";
            }
        }
    });

});

define('troopjs-requirejs/template!widget/language/index.html',[],function() { return function template(data) { var o = "";
	var language = data.language || "en";
o += "\r\n<ul class=\"hor seplist\">\r\n    <li class=\"lng-selector-en\">\r\n        <a "; if(language !== "en") { o += "class=\"link-fn\" data-lng=\"en\" data-action=\"changelanguage\""; } o += ">English</a>\r\n    </li>\r\n    <li class=\"lng-selector-cn\">\r\n        <a "; if(language !== "cn") { o += "class=\"link-fn\" data-lng=\"cn\" data-action=\"changelanguage\""; } o += ">中文</a>\r\n    </li>\r\n</ul>"; return o; }; });
define('widget/language/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/cookie/main",
    "template!./index.html"
], function(Widget, $, context, cookie, template) {
    "use strict";

    var LANGUAGE_CSSCLASS_PREFIX = "lng-";
    var COOKIE_LNG_EXPIRES = 365;

    function saveLngToCookie(toLng) {
        cookie.set("lng", toLng, {
            expires: COOKIE_LNG_EXPIRES,
            path: "/"
        });
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var language = context.language;

            // Styling by language
            $(document.documentElement).addClass(LANGUAGE_CSSCLASS_PREFIX + language);

            // Rendering
            me.html(template, {
                "language": language
            });
        },
        "dom:[data-action=changelanguage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            var toLng = $el.data("lng");
            // Store in cookie
            saveLngToCookie(toLng);
            // Refresh page
            location.reload();
        }
    });
});

define('troopjs-requirejs/template!widget/map/index.html',[],function() { return function template(data) { var o = "";
	var url = data.url;
o += "\r\n<iframe src=\"" +url+ "\" frameborder=\"0\" align=\"middle\" width=\"602\" height=\"402\" class=\"map-container\"></iframe>"; return o; }; });
define('widget/map/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, $, PopupBox, template) {
    "use strict";

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "dom/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            var url = $el.data("url");

            if (!url) {
                return;
            }

            var $map = $(template({
                "url": url
            }));

            // Lightbox configuration
            var mapPopup = new PopupBox({
                msg: $map,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("orderEdit/updateOrder");
                    $map.remove();
                }
            });

            mapPopup.open();
        }
    });
});

define('troopjs-requirejs/template!widget/moonbear/index.html',[],function() { return function template(data) { var o = "";
	var isBlock = data.isBlock;
o += "\r\n<button type=\"submit\" class=\"btn btn-lg btn-default "; if (isBlock) { o += "btn-block"; } o += "\">购买 &nbsp;&nbsp; | &nbsp;&nbsp; BUY</button>"; return o; }; });
define('widget/moonbear/main',["troopjs-browser/component/widget",
	"jquery",
	"widget/cookie/main",
	"widget/pageurl/main",
	"template!./index.html"
], function(Widget, $, cookie, pageUrl, template) {
	"use strict";

	return Widget.extend(function($element, widgetName, isBlock){
		var me = this;

		me.isBlock = isBlock;
	},{
		"sig/start": function() {
			var me = this;

			me.html(template, {
				"isBlock": me.isBlock
			});
		},
		//data-weave="widget/href/main" href="/order/receiver" data-hash="packageid/17/groupid/27/days/30/addon/0,0" 
		"dom:button/click": function(e) {
			e.preventDefault();

			var me = this;
			var deliveryList = [];

			// Publish submit order event
			me.publish("numbercontroller/get").spread(function(dataNumber) {
				deliveryList.push({
					"IsDeliver": true,
					"PackageProducts": [],
					"Products": [dataNumber]
				});

				// Set customize order data to cookie
				cookie.set("customize", JSON.stringify(deliveryList), {
					path: "/"
				});

				// Next step
				pageUrl.goto("/order/receiver",
					"packageid/" + 0 +
					"/groupid/" + 0 +
					"/event/bear");
			});
		}
	});
});

define('troopjs-requirejs/template!widget/navigation/index.html',[],function() { return function template(data) { var o = "";
    var firstName = data.firstName || "";
    var path = data.path;
    var menu = path[1];
    var subMenu = path[2] || "";

    switch(menu) {
        case "intl":
            renderPublic();
            break;
        case "enrollment":
            renderEnrollment();
            break;
        case "account":
            renderAccount();
            break; 
        case "order":
            renderOrder();
            break;
        case "payment":
            renderPayment();
            break;
        default: break;
    }
o += "\r\n\r\n"; function renderPublic(){ o += "\r\n"; } o += "\r\n\r\n"; function renderEnrollment(){ o += "\r\n"; } o += "\r\n\r\n"; function renderAccount(){ o += "\r\n    <ul class=\"nav nav-tabs\">\r\n        <li "; if (subMenu === "basic") { o += "class=\"active\""; } o += ">\r\n            <a href=\"/account/basic\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1013)\">Basic information</span>\r\n            </a>\r\n        </li>\r\n         <li "; if (subMenu === "safety") { o += "class=\"active\""; } o += ">\r\n            <a href=\"/account/safety\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1461)\">Account Safety</span>\r\n            </a>\r\n        </li>\r\n        <li "; if (subMenu === "receiver") { o += "class=\"active\""; } o += ">\r\n            <a href=\"/account/receiver\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1014)\">Delivery addresses</span>\r\n            </a>\r\n        </li>\r\n        <li "; if (subMenu === "orders" || subMenu === "orderdetail") { o += "class=\"active\""; } o += ">\r\n            <a href=\"/account/orders\" data-weave=\"widget/href/main\">\r\n                <span data-weave=\"widget/blurb/main(1015)\">My orders</span>\r\n            </a>\r\n        </li>\r\n    </ul>\r\n"; } o += "\r\n\r\n"; function renderOrder(){ o += "\r\n<!-- 进度触点开始 -->\r\n<link rel=\"stylesheet\" href=\"/static/css/orderBar.css\">\r\n<div class=\"w w1 stepflex-header clearfix\">\r\n    <div class=\"stepflex\" id=\"#sflex03\">\r\n        <dl class=\"first "; if (subMenu === "receiver") { o += " doing "; } o += "\">\r\n            <dt class=\"s-num\">1</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(1281)\">填写核对订单信息<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal "; if (subMenu === "confirm") { o += " doing "; } o += "\">\r\n            <dt class=\"s-num\">2</dt>\r\n            <dd class=\"s-text\" data-weave=\"widget/blurb/main(1358)\">支付及确认订单<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal "; if (subMenu === "success") { o += " doing "; } o += " \">\r\n            <dt class=\"s-num\">3</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(1280)\">成功提交订单<s></s><b></b></dd>\r\n        </dl>\r\n    </div>\r\n</div> \r\n<!-- 进度触点结束 -->\r\n"; } o += "\r\n\r\n"; function rrenderOrder(){ o += "\r\n    <ul class=\"nav nav-tabs\">\r\n        <li "; if (subMenu === "receiver") { o += "class=\"active\""; } o += ">\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1281)\">Delivery Address &amp; Start Date </span>\r\n            </a>\r\n        </li>\r\n        <li "; if (subMenu === "confirm") { o += "class=\"active\""; } o += ">\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1358)\">Payment and Invoice</span>\r\n            </a>\r\n        </li>\r\n        <li "; if (subMenu === "success") { o += "class=\"active\""; } o += ">\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1280)\">Successfully Ordered</span>\r\n            </a>\r\n        </li>\r\n    </ul>\r\n"; } o += "\r\n\r\n\r\n"; function renderPayment(){ o += "\r\n<!-- 进度触点开始 -->\r\n<link rel=\"stylesheet\" href=\"/static/css/orderBar.css\">\r\n<div class=\"w w1 stepflex-header clearfix\">\r\n    <div class=\"stepflex\" id=\"#sflex03\">\r\n        <dl class=\"first "; if (subMenu === "receiver") { o += " doing "; } o += "\">\r\n            <dt class=\"s-num\">1</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(1281)\">填写核对订单信息<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal "; if (subMenu === "topay") { o += " doing "; } o += "\">\r\n            <dt class=\"s-num\">2</dt>\r\n            <dd class=\"s-text\" data-weave=\"widget/blurb/main(1010)\">支付及确认订单<s></s><b></b></dd>\r\n        </dl>\r\n        <dl class=\"normal "; if (subMenu === "success") { o += " doing "; } o += " \">\r\n            <dt class=\"s-num\">3</dt>\r\n            <dd class=\"s-text\"  data-weave=\"widget/blurb/main(1280)\">成功提交订单<s></s><b></b></dd>\r\n        </dl>\r\n    </div>\r\n</div> \r\n<!-- 进度触点结束 -->\r\n"; } o += "\r\n\r\n"; function rrenderPayment(){ o += "\r\n    <ul class=\"nav nav-tabs\">\r\n        <li>\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1281)\">Select start date</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1009)\">Confirm</span>\r\n            </a>\r\n        </li>\r\n        <li "; if (subMenu === "topay") { o += "class=\"active\""; } o += ">\r\n            <a>\r\n                <span data-weave=\"widget/blurb/main(1010)\">Pay</span>\r\n            </a>\r\n        </li>\r\n        "; if (subMenu === "success" || subMenu === "paysuccess") { o += "\r\n            <li class=\"active\">\r\n                <a>\r\n                    <span data-weave=\"widget/blurb/main(1280)\">Success pay</span>\r\n                </a>\r\n            </li>\r\n        "; } else if (subMenu === "failed" || subMenu === "payfailed") { o += "\r\n            <li class=\"active\">\r\n                <a>\r\n                    <span data-weave=\"widget/blurb/main(1378)\">Pay failed</span>\r\n                </a>\r\n            </li>\r\n        "; } o += "\r\n    </ul>\r\n"; }  return o; }; });
define('widget/navigation/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/api/main",
    "widget/sitemap/main",
    "widget/ajaxquery/main",
    "widget/cookie/main",
    "template!./index.html"
], function(Widget, $, api, sitemap, ajaxQuery, cookie, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var pathName = location.pathname;
            var pathKey = sitemap.reversematch(pathName);

            if (!pathKey) {
                return;
            }

            // Array[0]: ""
            // Array[1]: main menu
            // Array[2]: sub menu
            var arrPath = pathKey.split("/");

            // Has cookie
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Rendering
                    me.html(template, {
                        "firstName": data.FirstName,
                        "path": arrPath
                    });
                }, function() {
                    // Rendering
                    me.html(template, {
                        "path": arrPath
                    });
                });
            }, function() {
                // Rendering
                me.html(template, {
                    "path": arrPath
                });
            });

        }
    });
});

define('troopjs-requirejs/template!widget/numbercontroller/index.html',[],function() { return function template(data) { var o = "";
    var count = data.count;
    var countMin = data.countMin;
    var countMax = data.countMax;
    var price = data.price;
    var disable = data.disable;
o += "\r\n<div class=\"numbercontroller\">\r\n\t<div class=\"numbercontroller-title\" data-weave=\"widget/blurb/main(1460)\">Choose a number</div>\r\n\t<div class=\"numbercontroller-number\">\r\n\t\t"; if (!disable) { o += "\r\n\t\t\t<a class=\"numbercontroller-minus\">\r\n\t\t\t\t<span class=\"glyphicon glyphicon-minus\"></span>\r\n\t\t\t</a>\r\n\t\t"; } o += "\r\n\t\t<input class=\"numbercontroller-input\" type=\"number\" name=\"bottlecount\" min=\"" +countMin+ "\" max=\"" +countMax+ "\" value=\"" +count+ "\" "; if(disable) { o += "disabled"; } o += " required/>\r\n\t\t"; if (!disable) { o += "\r\n\t\t\t<a class=\"numbercontroller-plus\">\r\n\t\t\t\t<span class=\"glyphicon glyphicon-plus\"></span>\r\n\t\t\t</a>\r\n\t\t"; } o += "\r\n\t</div>\r\n\t<div class=\"numbercontroller-total\">\r\n\t\t合计：&yen;<strong class=\"numbercontroller-amount\">" +count * price+ "</strong>\r\n\t</div>\r\n</div>"; return o; }; });
define('widget/numbercontroller/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/infobox/main",
    "widget/blurb/main",
    "template!./index.html",
    "jquery.textchange"
], function(Widget, $, InfoBox, Blurb, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    return Widget.extend(function($element, widgetName, id, count, countMin, countMax, price, disable) {
        var me = this;
        // get parameters
        me.id = id;
        me.countDefault = count || 0;
        me.count = count || 0;
        me.countMin = countMin || 0;
        me.countMax = countMax || 0;
        me.price = price || 0;
        me.disable = disable || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template, {
                "count": me.count,
                "countMin": me.countMin,
                "countMax": me.countMax,
                "price": me.price,
                "disable": me.disable
            }).then(function() {
                var $number = me.$number = $me.find("input.numbercontroller-input");
                me.$amount = $me.find(".numbercontroller-amount");

                $number.on("textchange", function(e) {
                    var $el = $(e.currentTarget);
                    var eVal = $el.val();

                    if (!eVal) {
                        me.count = me.countDefault;
                        return;
                    }
                    if (isNaN(eVal)) {
                        $el.val(me.count);
                        return;
                    }
                    if (eVal < me.countMin || eVal > me.countMax) {
                        $el.val(me.count);
                        return;
                    }
                    me.count = parseInt(eVal, 10);
                    // Maintain order
                    me.changeProduct();
                });
            });
        },
        "changeProduct": function() {
            var me = this;

            // Amount change
            me.$amount.text(me.count * me.price);
        },
        "hub/numbercontroller/get": function() {
            var me = this;
            var $me = me.$element;

            var postData = {
                "id": me.id,
                "price": me.price,
                "count": me.count
            };

            var promiseValue = [];
            promiseValue.push(postData);
            return promiseValue;
        },
        "dom:.numbercontroller-plus/click": function(e) {
            e.preventDefault();
            var me = this;
            if (me.disable) {
                return;
            }

            if (me.countMax > 0 && me.count > me.countMax - 1) {
                // Show error msg as a lightbox
                new InfoBox({
                    "title": 1144,
                    "content": blurb.get(1204, me.countMax),
                    "overLap": "overlap"
                }).open();
                return;
            }
            me.count++;
            me.$number.val(me.count);
            // Maintain order
            me.changeProduct();
        },
        "dom:.numbercontroller-minus/click": function(e) {
            e.preventDefault();
            var me = this;
            if (me.disable) {
                return;
            }

            if (me.count < me.countMin + 1) {
                return;
            }
            me.count--;
            me.$number.val(me.count);
            // Maintain order
            me.changeProduct();
        }
    });
});

define('troopjs-requirejs/template!widget/order/calendar/index.html',[],function() { return function template(data) { var o = "<div class=\"ordercalendar-control cf\">\r\n    "; if (!data.isStartWeek) { o += "\r\n        <a class=\"link-fn\" data-action=\"prevweek\">&lt; <span data-weave=\"widget/blurb/main(1025)\">Past two weeks</span></a>\r\n    "; } o += "\r\n    <a class=\"link-fn\" data-action=\"nextweek\"><span data-weave=\"widget/blurb/main(1026)\">Next two weeks</span> &gt;</a>\r\n</div>\r\n<div class=\"ordercalendar\">\r\n    <table>\r\n        ";
            var i = 0;
            var calendar = data.calendar;
            var iLens = calendar.length;
            var w = 0;
            var weeks = Math.ceil(iLens / 7);
            for(w; w < weeks; w++){
                o += "\r\n                    <tr>\r\n                        ";
                            for(i = w * 7; i < (w + 1) * 7; i++){
                                if(calendar[i]){
                                    appendDay(calendar[i]);
                                }
                            }
                        o += "\r\n                    </tr>\r\n                ";
            }
        o += "\r\n    </table>\r\n</div>\r\n\r\n"; function appendDay(day) {
    if(day.isAvailable) {
        appendDayAvailable(day);
    }
    else {
        appendDayDisable(day);
    }
} o += "\r\n"; function appendDayAvailable(day) { o += "\r\n    <td>\r\n        <div class=\"ordercalendar-day "; if(day.isDeliveryStartDate) { o += "ordercalendar-day-start"; } o += " "; if(day.products){ o += "ordercalendar-day-active"; } o += "\">\r\n            <div class=\"ordercalendar-day-date\">\r\n                <div><strong>" +day.weekName+ "</strong></div>\r\n                <div>" +day.shortDate+ "</div>\r\n            </div>\r\n            <div class=\"ordercalendar-day-main\" data-action=\"editorder\" data-date=\"" +day.date+ "\" "; if(day.products){ o += "data-toggle=\"popover\" data-content=\""; for(var i=0;i<day.products.length;i++) {o += "\r\n            " +day.products[i].Name+ " &times; " +day.products[i].Count+ "<br>\r\n            "; } o += "\""; } o += " >\r\n                "; if(day.products){ o += "\r\n                    <div class=\"ordercalendar-day-order\">\r\n                        <div class=\"ordercalendar-day-gap ordercalendar-day-order-products\">\r\n                            <span class=\"juicebottle-generic\"></span> <span>&times; " +countProductsQuantity(day.products)+ "</span>\r\n\r\n                        </div>\r\n                        "; if (day.isDeliver) { o += "\r\n                            <p class=\"ordercalendar-day-gap ordercalendar-day-delivery-address\" title=\"" +day.receiverAddress+ "\">" +day.receiverAddress+ "</p>\r\n                        "; } else { o += "\r\n                            <p class=\"ordercalendar-day-gap ordercalendar-day-delivery-address ordercalendar-day-no-delivery-address\" data-weave=\"widget/blurb/main(1250)\">No address</p>\r\n                            <!--p class=\"ordercalendar-day-gap ordercalendar-day-delivery-address\" data-weave=\"widget/blurb/main(1027)\">Pick up in store</p-->\r\n                        "; } o += "\r\n                        "; if (day.deliverTime) { o += "\r\n                            <div class=\"ordercalendar-day-gap ordercalendar-day-sep\"></div>\r\n                            <p class=\"ordercalendar-day-gap ordercalendar-day-delivery-time\" title=\"" +day.deliverTime+ "\">" +day.deliverTime+ "</p>\r\n                        "; } o += "\r\n                    </div>\r\n                    <div class=\"ordercalendar-day-linkadd\">\r\n                        <a class=\"link-fn\">+ <span data-weave=\"widget/blurb/main(1028)\">Change</span></a>\r\n                    </div>\r\n                "; } else { o += "\r\n                    <div class=\"ordercalendar-day-shoppingcart\">\r\n                        <a class=\"link-fn\" title=\"Add\" data-weave=\"widget/blurb/main(1029, 'title')\"></a>\r\n                    </div>\r\n                    <div class=\"ordercalendar-day-linkadd\">\r\n                        <a class=\"link-fn\">+ <span data-weave=\"widget/blurb/main(1029)\">Add</span></a>\r\n                    </div>\r\n                "; } o += "\r\n            </div>\r\n        </div>\r\n    </td>\r\n"; } o += "\r\n"; function appendDayDisable(day) { o += "\r\n    <td>\r\n        <div class=\"ordercalendar-day ordercalendar-day-disable\">\r\n            <div class=\"ordercalendar-day-date\">\r\n                <div><strong>" +day.weekName+ "</strong></div>\r\n                <div>" +day.shortDate+ "</div>\r\n            </div>\r\n            <div class=\"ordercalendar-day-main\">\r\n                <div class=\"ordercalendar-day-late\">\r\n                    <a></a>\r\n                </div>\r\n                <div class=\"ordercalendar-day-gap\" data-weave=\"widget/blurb/main(1030)\">\r\n                    Too late to order\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </td>\r\n"; } o += "\r\n";
    function countProductsQuantity(products){
        var i = 0;
        var iLens = products.length;
        var quantity = 0;
        for(i; i < iLens; i++){
            quantity += products[i].Count;
        }
        return quantity;
    }
 return o; }; });
define('widget/order/calendar/main',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "widget/blurb/main",
    "widget/time/main",
    "widget/customize/main",
    "template!./index.html",
    "json2"
], function(Widget, $, _, when, Blurb, Time, Customize, template) {
    "use strict";

    // Manually initialize "Customize", set $el = $body
    (new Customize($(document.body))).start();

    var blurb = new Blurb($(document.body));
    // Time function
    var INS_TIME = new Time();

    var WEEKS_ONE_PAGE = 2;
    /* create an array of days which need to be disabled */
    var DISABLEDDAYS = [
        "2-2-2016",
        "2-3-2016",
        "2-4-2016",
        "2-5-2016",
    ];

    // 不配送日期
    function offDays(date) {
        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();
        var weekday = date.getDay();

        if (DISABLEDDAYS) {
            if ($.inArray((m + 1) + '-' + d + '-' + y, DISABLEDDAYS) != -1 || new Date() > date) {
                return false;
            }
        }

        return true;
    }

    function getOrderCalendar(availableStartDate, baseDate) {
        var weekStartDate = INS_TIME.getWeekStartDate(baseDate);
        var baseDateDay = INS_TIME.getDayOfWeekFromMonday(baseDate);

        return {
            // true: can't prev anymore
            zero: false,
            // 0: base date contained in current two weeks
            page: 0,
            getCurrentWeeks: function() {
                var me = this;
                // Current week start date count from baseDate week
                var i = 0;
                var iLens = WEEKS_ONE_PAGE * 7;
                // Pages
                var delta = me.page * WEEKS_ONE_PAGE * 7;
                var j = 0;
                var twoWeeks = [];
                var date;

                for (i; i < iLens; i++) {
                    date = INS_TIME.addDay(weekStartDate, i + delta);

                    twoWeeks.push({
                        date: date,
                        weekName: INS_TIME.getWeekName(date.getDay(), true),
                        shortDate: INS_TIME.getShortDate(date),
                        isAvailable: INS_TIME.compareDate(availableStartDate, date) >= 0 ? offDays(date) : false,
                        isDeliveryStartDate: INS_TIME.compareDate(baseDate, date) === 0 ? true : false
                    });
                    if (i === 0) {
                        me.zero = INS_TIME.compareDate(availableStartDate, date) <= 0 ? true : false;
                    }
                }
                return twoWeeks;
            },
            currentWeeks: function() {
                var me = this;

                return me.getCurrentWeeks();
            },
            nextWeeks: function() {
                var me = this;

                me.page++;
                return me.getCurrentWeeks();
            },
            prevWeeks: function() {
                var me = this;

                if (me.zero) {
                    return;
                }
                me.page--;
                return me.getCurrentWeeks();
            }
        };
    }

    function getAddressString(address) {
        // Get address string from object
        if (!address) {
            return "";
        }
        var strAddress = "";

        if (address.FirstName) {
            strAddress += address.FirstName + " ";
        }
        if (address.LastName) {
            strAddress += address.LastName;
        }
        if (address.Line1) {
            strAddress += ", " + address.Line1;
        }
        if (address.Line2) {
            strAddress += ", " + address.Line2;
        }

        return strAddress;
    }

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];

        // Deferred for startData
        me.startDateDeferred = when.defer();
    }, {
        // Render calendar with order/receiver info
        "render": function() {
            var me = this;
            var $me = me.$element;

            if (!me.twoWeeks) {
                return;
            }

            var calendarOrder = me.twoWeeks.currentWeeks();
            var deliveryList = me.deliveryList;
            var i = 0;
            var iLens = calendarOrder.length;
            var j = 0;
            var jLens = deliveryList.length;

            var iCalendarOrder;
            var jDeliveryList;
            // Has products in order
            if (jLens > 0) {
                // Build data for template
                for (i = 0; i < iLens; i++) {
                    // Loop package data
                    for (j = 0; j < jLens; j++) {
                        iCalendarOrder = calendarOrder[i];
                        jDeliveryList = deliveryList[j];
                        // Append package data to corresponding date
                        if (INS_TIME.compareDate(jDeliveryList.Date, iCalendarOrder.date) === 0) {
                            // Prevent undefined
                            jDeliveryList.Products = jDeliveryList.Products || [];
                            jDeliveryList.PackageProducts = jDeliveryList.PackageProducts || [];
                            // Copy
                            iCalendarOrder.products = jDeliveryList.Products.concat(jDeliveryList.PackageProducts);
                            iCalendarOrder.receiverAddress = getAddressString(jDeliveryList.Receiver);
                            iCalendarOrder.isDeliver = jDeliveryList.IsDeliver;
                            iCalendarOrder.deliverTime = jDeliveryList.DeliverTimeName;
                        }
                    }
                }
            }
            // Render Two Weeks Calendar
            me.html(template, {
                "calendar": calendarOrder,
                "isStartWeek": me.twoWeeks.zero
            }).then(function() {
                $me.find("[data-toggle=popover]").popover({
                    "trigger": "hover",
                    "html": true,
                    "title": blurb.get(1609),
                    "placement": "right",
                    "delay": {
                        "show": 0,
                        "hide": 50
                    }
                });
            });
        },
        "hub:memory/order/deliveryList": function(deliveryList, isDailyUpdate) {
            var me = this;

            me.deliveryList = deliveryList;

            // Render calendar
            if (!isDailyUpdate) {
                // Render after week data updated
                me.startDateDeferred.promise.then(function() {
                    me.render();
                });
            } else {
                me.render();
            }
        },
        "hub:memory/deliveryStartDate/change": function(args) {
            var me = this;

            // Date info
            var availableStartDate = args.availableStartDate;
            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;

            // Get two week deliver data, and render calendar
            me.twoWeeks = getOrderCalendar(availableStartDate, deliveryStartDate);

            // Week data updated
            me.startDateDeferred.resolver.resolve();
        },
        "dom:[data-action=prevweek]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Move calendar
            me.twoWeeks.prevWeeks();
            // Mixin calendar/order, rendering
            me.render();
        },
        "dom:[data-action=nextweek]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Move calendar
            me.twoWeeks.nextWeeks();
            // Mixin calendar/order, rendering
            me.render();
        },
        // Edit daily order
        "dom:[data-action=editorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $e = $(e.currentTarget);
            // Get date from DOM, return while failure
            var strDate = $e.data("date");
            if (!strDate) {
                return;
            }
            var clickDate = new Date(strDate);
            var i = 0;
            var iLens = me.deliveryList.length;
            var dailyOrder;

            for (i; i < iLens; i++) {
                if (INS_TIME.compareDate(me.deliveryList[i].Date, clickDate) === 0) {
                    dailyOrder = me.deliveryList[i];
                    break;
                }
            }
            // Set a temp daily Order, if no order in current day
            if (!dailyOrder) {
                dailyOrder = {
                    Date: clickDate,
                    IsDeliver: true
                };
            }

            // Popup order-edit lightbox.
            me.publish("customize/popup", dailyOrder);
        }
    });
});

define('troopjs-requirejs/template!widget/order/confirm/index.html',[],function() { return function template(data) { var o = "";
    var orderId = data.orderId || 0;
    var sourceCode = data.sourceCode || "";
o += "\r\n<div class=\"payment-box\">\r\n    <div class=\"payment-row row-methed orderconfirm-payment\" data-weave=\"widget/order/paymenttype/main(sourceCode)\" data-source-code=\"" +sourceCode+ "\"></div>\r\n    <div class=\"payment-row row-invoice invoice\" data-weave=\"widget/order/invoice/main\"></div>\r\n    <!-- TODO: HARD -->\r\n    "; if (sourceCode.toUpperCase() === "WARM") { o += "\r\n        <div class=\"payment-row row-gift gift\" data-weave=\"widget/order/gift/main\"></div>\r\n    "; } o += "\r\n    <div class=\"payment-row orderconfirm-promotion\" data-weave=\"widget/order/promotion/main(orderId)\" data-order-id=\"" +orderId+ "\"></div>\r\n    <div class=\"payment-row ordersourcecode hidden\">\r\n        <p>\r\n            <small data-weave=\"widget/blurb/main(1318)\">Type group purchase code if you're group purchase customer!</small>\r\n        </p>\r\n        <div class=\"the-form\">\r\n            <form class=\"standardform\">\r\n                <div>\r\n                    <input type=\"text\" id=\"order-sourcecode\" placeholder=\"\" "; if (sourceCode) { o += "value=\"" +sourceCode+ "\" disabled"; } o += ">\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n    <div class=\"payment-row row-confirm\">\r\n        <h4>\r\n            <strong data-weave=\"widget/blurb/main(1031)\">Confirm</strong>\r\n            <span data-weave=\"widget/blurb/main(1032)\">Your Order</span>\r\n        </h4>\r\n        <div class=\"order-summary\" data-weave=\"widget/order/summary/main(orderId)\" data-order-id=\"" +orderId+ "\"></div>\r\n    </div>\r\n    <div class=\"cf text-center orderaction-container\">\r\n        <a class=\"btn btn-lg btn-default btn-action\" data-action=\"confirmorder\" data-weave=\"widget/blurb/main(1115)\">CONFIRM</a>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/order/confirm/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, ajaxQuery, api, pageUrl, troopHash, template) {
    "use strict";

    var PRICE_DELIVERY = 25;
    var URI_ORDER_CONFIRM = api.get("order_confirm");
    var EXPIRED_CODE = 5;
    var URI_ORDER_PAYMENT = api.get("orderpayment");
    // Transfer to blurb
    var invoiceDict = {
        "1": 1158,
        "2": 1159
    };

    return Widget.extend(function($element, widgetName, step) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.step = step;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            // Load order detail, so ignore order step
            if (me.step === "order") {
                return;
            }
            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }

            var hashMap = troopHash.parse(hashPath);
            var orderId = me.orderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Special logic for GiftCard
            var sourceCode = me.sourceCode = hashMap.event || "";

            me.html(template, {
                "orderId": orderId,
                "sourceCode": sourceCode
            });
        },
        "getPayment": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/paymenttype/submit", me.orderId).spread(function(dataPayment) {
                return when.resolve(dataPayment);
            });
        },
        "getInvoice": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/invoice/submit", me.orderId).spread(function(dataInvoice) {
                return when.resolve(dataInvoice);
            });
        },
        "getPromotion": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/promotion/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "getGiftInfo": function() {
            var me = this;
            // Publish submit order event
            return me.publish("order/gift/submit", me.orderId).spread(function(dataPromotion) {
                return when.resolve(dataPromotion);
            });
        },
        "dom:[data-action=confirmorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;
            var $el = $(e.currentTarget);

            var postData = {
                "OrderId": me.orderId
            };

            when.join(me.getPayment(), me.getInvoice(), me.getPromotion(), me.getGiftInfo()).then(function(data) {
                postData.Payment = data[0];
                postData.Invoice = data[1];
                postData.Promotion = data[2];
                postData.GiftInfo = data[3];

                _.extend(postData, {
                    "SourceCode": $me.find("#order-sourcecode").val() || ""
                });

                ajaxQuery({
                    url: URI_ORDER_CONFIRM,
                    data: JSON.stringify(postData),
                    contentType: "application/json; charset=UTF-8",
                    type: "POST",
                    dataType: "json"
                }, $el.parent()).then(function(data) {
                    // Success Confirm Order (save Payment type & Invoice info)
                    //
                    // Redirect by payment type
                    var paymentType = postData.Payment.payment;
                    // URL redirect
                    if (paymentType.toString() === "1") {
                        pageUrl.goto("/payment/topay", "orderid/" + me.orderId);
                    } else {
                        // Show success info
                        pageUrl.goto("/order/success", "orderid/" + me.orderId, {
                            "orderid": me.orderId
                        });
                    }
                });
            });
        }
    });
});

define('troopjs-requirejs/template!widget/order/gift/index.html',[],function() { return function template(data) { var o = "";
    var isGift = data.isGift || false;
    var blessing = data.blessing || "";
o += "\r\n<h4><strong data-weave=\"widget/blurb/main(1606)\">This is a Gift</strong></h4>\r\n<div class=\"the-form\">\r\n    <div class=\"form-group\">\r\n        <input type=\"checkbox\" class=\"icheck\" id=\"gift-needblessing\" data-action=\"needgift\" value=\"\" "; if (isGift) { o += "checked"; } o += ">\r\n        <label for=\"gift-needblessing\">\r\n            <span data-weave=\"widget/blurb/main(1606)\">This is a Gift</span>\r\n        </label>\r\n    </div>\r\n    <div class=\"form-group blessing-sub "; if (!isGift) { o += "hid"; } o += " row\">\r\n        <div class=\"col-sm-12 col-md-12\">\r\n            <div class=\"gift-inputs\">\r\n                <textarea style=\"resize: vertical;\" class=\"form-control\" rows=\"3\" cols=\"10\" data-for=\"gift-blessing\" name=\"gift-blessing\" id=\"gift-blessing\" placeholder=\"礼品赠言\">"; if (isGift && blessing) { o += "" + blessing; } o += "</textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/order/gift/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/cookie/main",
    "template!./index.html",
    "json2"
], function(Widget, $, cookie, template) {
    "use strict";

    return Widget.extend({
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            me.html(template, data).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            cookie.get("gift_info").then(function(giftInfo) {
                var objGiftInfo = JSON.parse(giftInfo);

                me.render({
                    "isGift": objGiftInfo.isGift,
                    "blessing": objGiftInfo.blessing
                });
            }, function() {
                me.render({
                    "isGift": false,
                    "blessing": ""
                });
            });
        },
        "dom:[data-action=needgift]/ifChanged": function(e) {
            var me = this;
            var $me = me.$element;

            if (e.currentTarget.checked) {
                $me.find(".blessing-sub").show();
            } else {
                $me.find(".blessing-sub").hide();
            }
        },
        "hub/order/gift/update": function(giftInfo) {
            cookie.set("gift_info", JSON.stringify(giftInfo), {
                expires: 7,
                path: "/"
            });
        },
        "hub/order/gift/remove": function() {
            cookie.rm("gift_info", {
                path: "/"
            });
        },
        "hub/order/gift/submit": function(orderId) {
            var me = this;

            var $me = me.$element;
            var isGift;
            var postData;

            isGift = $me.find(".icheckbox_square").hasClass("checked");

            postData = {
                "orderId": orderId,
                "IsGift": isGift,
                "Blessing": isGift === true ? $me.find("#gift-blessing").val() : ""
            };

            var promiseValue = [];
            promiseValue.push(postData);
            return promiseValue;
        }
    });
});

define('troopjs-requirejs/template!widget/order/invoice/index.html',[],function() { return function template(data) { var o = "<h4><strong data-weave=\"widget/blurb/main(1041)\">Your tax invoice (fapiao)</strong></h4>\r\n<p>\r\n    <small data-weave=\"widget/blurb/main(1043)\">VCLEANSE will issue an invoice for products, for delivery fees, please contact Yamato Express.</small>\r\n</p>\r\n<div class=\"the-form\">\r\n    <div class=\"form-group\">\r\n        <input type=\"checkbox\" class=\"icheck\" id=\"invoice-needinvoice\" data-action=\"needinvoice\" value=\"\">\r\n        <label for=\"invoice-needinvoice\" data-weave=\"widget/blurb/main(1044)\">Yes, I would like invoice</label>\r\n    </div>\r\n    <div class=\"form-group invoice-sub hid row\">\r\n        <div class=\"col-sm-12 col-md-4\">\r\n            <input type=\"radio\" class=\"icheck\" id=\"invoice-personal\" data-action=\"personalinvoice\" name=\"invoice-type\" value=\"2\" checked>\r\n            <label for=\"invoice-personal\" data-weave=\"widget/blurb/main(1045)\">Personal invoice</label>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group invoice-sub hid row\">\r\n        <div class=\"col-sm-12 col-md-4\">\r\n            <input type=\"radio\" class=\"icheck\" id=\"invoice-company\" data-action=\"companyinvoice\" name=\"invoice-type\" value=\"1\" data-weave=\"widget/checkmyradio-listener/main\">\r\n            <label for=\"invoice-company\" data-weave=\"widget/blurb/main(1046)\">Company invoice</label>\r\n        </div>\r\n        <div class=\"col-sm-12 col-md-7\">\r\n            <div class=\"invoice-inputs\">\r\n                <input type=\"text\" class=\"form-control\" data-for=\"invoice-company\" id=\"invoice-companyname\" placeholder=\"Company title\" data-weave=\"widget/checkmyradio/main\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"invoice-sub hid\">\r\n        <p class=\"help-block\" data-weave=\"widget/blurb/main(1201)\">Your invoice will be issued on the juice package together, please note that check!</p>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/order/invoice/main',["troopjs-browser/component/widget",
	"jquery",
	"template!./index.html",
	"json2"
], function(Widget, $, template) {
	"use strict";

	return Widget.extend({
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			me.html(template).then(function() {
				$me.find('.icheck').iCheck({
					checkboxClass: 'icheckbox_square',
					radioClass: 'iradio_square',
					increaseArea: '20%'
				});
				// Check default
				$('input[name=invoice-type]:eq(0)').iCheck('check');
			});
		},
		"dom:[data-action=companyinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (!e.currentTarget.checked) {
				return;
			}
			$me.find("#invoice-companyname").focus();
		},
		"dom:[data-action=needinvoice]/ifChanged": function(e) {
			var me = this;
			var $me = me.$element;

			if (e.currentTarget.checked) {
				$me.find(".invoice-sub").show();
			} else {
				$me.find(".invoice-sub").hide();
			}

			// Trigger "height change" event on "auto follower"
			//me.publish("autoFollower/resize");
		},
		"hub/order/invoice/submit": function(orderId) {
			var me = this;

			var $me = me.$element;
			var invoiceType;
			var postData;

			if ($me.find(".icheckbox_square").hasClass("checked")) {
				invoiceType = $me.find(".iradio_square.checked > input").val();
				postData = {
					orderId: orderId,
					invoiceType: invoiceType,
					title: invoiceType.toString() === "1" ? $me.find("#invoice-companyname").val() : ""
				};
			}

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;

		}
	});
});

define('troopjs-requirejs/template!widget/order/index.html',[],function() { return function template(data) { var o = "<div class=\"calendar cf hidden\" data-weave=\"widget/order/startdate/main\"></div>\r\n<div class=\"ordercalendar-container\" data-weave=\"widget/order/calendar/main\"></div>"; return o; }; });
define('widget/order/main',["troopjs-browser/component/widget",
    "jquery",
    "underscore",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/time/main",
    "widget/cookie/main",
    "template!./index.html",
    "json2"
], function(Widget, $, _, when, context, cloneObject, ajaxQuery, api, pageUrl, InfoBox, Time, cookie, template) {
    "use strict";

    var URI_ORDER_CREATE = api.get("order_create");

    // Time function
    var INS_TIME = new Time();

    return Widget.extend(function() {
        var me = this;

        // Init an empty deliverlist
        me.deliveryList = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Rendering
            me.html(template);
        },
        "publishDeliveryList": function(isDailyUpdate) {
            var me = this;
            // Publish to order confirm, list price list
            // Publish to calendar, render order on calendar            
            me.publish("order/deliveryList", me.deliveryList, isDailyUpdate);
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryStartDate = args.selectedDate;
            // Save delivery start date
            me.deliveryStartDate = deliveryStartDate;

            // Mixin calendar/order, rendering
            // Without orders & package, should be customize
            // Publish to order confirm, list price list
            me.publishDeliveryList();
        },
        // Daily order updated
        "hub/order/dailyUpdate": function(dailyOrder) {
            var me = this;

            var i = 0;
            var iLens = me.deliveryList.length;
            var isNew = true;
            var isDelete = false;

            if (!dailyOrder) {
                return;
            }
            // Delete day which without products
            if (!dailyOrder.PackageRuleId &&
                (!dailyOrder.Products || dailyOrder.Products.length <= 0)) {
                isDelete = true;
            }

            for (i = 0; i < iLens; i++) {
                if (INS_TIME.compareDate(me.deliveryList[i].Date, dailyOrder.Date) === 0) {
                    isNew = false;
                    // Delete day which without products
                    if (isDelete) {
                        me.deliveryList.splice(i, 1);
                        break;
                    } else {
                        me.deliveryList[i] = dailyOrder;
                    }
                }
            }
            // Order on new date
            // & with products
            if (isNew && dailyOrder &&
                (dailyOrder.PackageRuleId || dailyOrder.Products && dailyOrder.Products.length > 0)) {
                me.deliveryList.push(dailyOrder);
            }

            // Publish to order confirm, list price list
            me.publishDeliveryList(true);
        },
        // Submit order
        "hub/order/submit": function() {
            var me = this;

            if (!me.deliveryList || me.deliveryList.length <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1147
                }).open();
                // Do not submit order
                return;
            }
            // Format Date to string
            var i = 0;
            var iLens = me.deliveryList.length;

            for (i = 0; i < iLens; i++) {
                // Use strDate for cookie
                me.deliveryList[i].Date = INS_TIME.getStrDate(me.deliveryList[i].Date);
            }

            // Set customize order data to cookie
            cookie.set("customize", JSON.stringify(me.deliveryList), {
                path: "/"
            });

            // Next step
            pageUrl.goto("/order/receiver",
                "packageid/" + 0 +
                "/groupid/" + 0 +
                "/deliveryStartDate/" + INS_TIME.getStrDate(me.deliveryList[0].Date, "-"));
        }
    });
});

define('troopjs-requirejs/template!widget/order/next/index.html',[],function() { return function template(data) { var o = "<h3>\r\n    <strong data-weave=\"widget/blurb/main(1031)\">Confirm</strong>\r\n    <span data-weave=\"widget/blurb/main(1032)\">Your Order</span>\r\n</h3>\r\n<div class=\"billlist\">\r\n    <table>\r\n        <tr>\r\n            <td data-weave=\"widget/blurb/main(1033)\">Product:</td>\r\n            <td class=\"number-price\">&yen;" +data.product+ "</td>\r\n        </tr>\r\n    </table>\r\n</div>\r\n<div class=\"line-hor\"></div>\r\n<div class=\"billlist billlist-amount\">\r\n    <table>\r\n        <tr>\r\n            <td><strong data-weave=\"widget/blurb/main(1040)\">Total:</strong></td>\r\n            <td class=\"number-price\"><strong>&yen;" +data.total+ "</strong></td>\r\n        </tr>\r\n    </table>\r\n</div>\r\n<div class=\"line-hor\"></div>\r\n<div class=\"orderconfirm-action\">\r\n    <a class=\"btn btn-lg btn-default\" data-action=\"submitorder\" data-weave=\"widget/blurb/main(1114)\">NEXT</a>\r\n</div>"; return o; }; });
define('widget/order/next/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, number, ajaxQuery, api, pageUrl, template) {
    "use strict";

    return Widget.extend(function($element, widgetName, step) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.step = step;
    }, {
        "render": function(data) {
            var me = this;

            me.html(template, data);
        },
        "dom:[data-action=submitorder]/click": function(e) {
            e.preventDefault();
            var me = this;

            // Publish submit order event
            me.publish("order/submit");
        },
        "hub:memory/order/deliveryList": function(deliveryList) {
            var me = this;

            // Loop delivery list
            var i = 0;
            var iLens = deliveryList.length;
            var j = 0;
            var jLens;
            var priceProducts = 0;

            var iDeliveryList;
            var iProducts;

            for (i = 0; i < iLens; i++) {
                iDeliveryList = deliveryList[i];

                // Prevent undefined
                iDeliveryList.Products = iDeliveryList.Products || [];
                iDeliveryList.PackageProducts = iDeliveryList.PackageProducts || [];

                iProducts = iDeliveryList.Products.concat(iDeliveryList.PackageProducts);
                jLens = iProducts.length;
                for (j = 0; j < jLens; j++) {
                    priceProducts += iProducts[j].Count * iProducts[j].Price;
                }
            }
            me.render({
                "step": me.step,
                "product": number.getDecimal(priceProducts),
                "total": number.getDecimal(priceProducts)
            });
        }
    });
});

define('troopjs-requirejs/template!widget/order/paymenttype/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var paymentType = data.paymentType;
    var sourceCode = data.sourceCode || "";
    var isMobile = data.isMobile;

    var paymentId = 0;
    /* Default: pay online */
    var defaultPaymentId = 1;
o += "\r\n<h4>\r\n    <strong data-weave=\"widget/blurb/main(1111)\">Payment method</strong>\r\n</h4>\r\n<p class=\"text-warning\">\r\n    <small>\r\n        <b>支付宝快捷支付（含信用卡、储蓄卡）</b>已开通大额支付。<a href=\"https://cshall.alipay.com/lab/help_detail.htm?help_id=245345\" target=\"_blank\">支付限额详见&gt;&gt;</a>\r\n    </small>\r\n</p>\r\n<div class=\"the-form\">\r\n    "; 
        var i = 0;
        var iLens = paymentType.length;
        for (i = 0; i < iLens; i++) {
            paymentId = paymentType[i].Id;
    o += "\r\n        <div class=\"form-group\" data-action=\"orderconfirm-payment-" +paymentId+ "\">\r\n            <label for=\"orderconfirm-payment-" +paymentId+ "\">\r\n                <input type=\"radio\" class=\"icheck\" id=\"orderconfirm-payment-" +paymentId+ "\" name=\"orderconfirm-pay-type\" value=\"" +paymentId+ "\" "; if (paymentId === defaultPaymentId) { o += "checked"; } o += ">\r\n                " +paymentType[i].Name+ "\r\n                <!-- TODO: HARD -->\r\n                "; if (paymentId === 1) { o += "\r\n                    "; if (!isMobile) { o += "\r\n                        [<a class=\"link-fn\" data-action=\"showallbanks\" data-weave=\"widget/blurb/main(1451)\">See all online payment types</a>]\r\n                    "; } 
                } o += "\r\n            </label>\r\n        </div>\r\n    "; } o += "\r\n</div>"; return o; }; });

define('troopjs-requirejs/template!widget/order/paymenttype/banklist.html',[],function() { return function template(data) { var o = "";
  var cacheServer = data.cacheServer;
o += "\r\n\r\n<div class=\"v-popup-container\">\r\n  <h6>支付平台：</h6>\r\n  <ul class=\"hor banklist\">\r\n    <li data-action=\"selectbank\" data-id=\"ALIPAY\">\r\n        <img src=\"" +cacheServer+ "/images/payment/alipay.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"ALIPAY_QR\">\r\n        <img src=\"" +cacheServer+ "/images/payment/alipay_saomazhifu.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"UCFPAY\">\r\n        <img src=\"" +cacheServer+ "/images/payment/ucfpay.jpg\" width=\"126\" height=\"35\">\r\n    </li>\r\n  </ul>\r\n\r\n  <h6>支付网银：</h6>\r\n  <ul class=\"hor banklist\">\r\n    <li data-action=\"selectbank\" data-id=\"CMB\">\r\n        <label class=\"bank-cmbchina\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"BOCB2C\">\r\n        <label class=\"bank-boc\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"ICBCB2C\">\r\n        <label class=\"bank-icbc\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"COMM\">\r\n        <label class=\"bank-bankcomm\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"CCB\">\r\n        <label class=\"bank-ccb\"></label>\r\n    </li>\r\n    <li data-action=\"selectbank\" data-id=\"CIB\">\r\n        <label class=\"bank-cib\"></label>\r\n    </li>\r\n  </ul>\r\n  <ul class=\"hor banklist banklist-more\">\r\n      <li data-action=\"selectbank\" data-id=\"CMBC\">\r\n          <label class=\"bank-cmbc\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"GDB\">\r\n          <label class=\"bank-cgbchina\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SPDB\">\r\n          <label class=\"bank-spdb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"ABC\">\r\n          <label class=\"bank-abchina\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SPABANK\">\r\n          <label class=\"bank-pingan\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"POSTGC\">\r\n          <label class=\"bank-psbc\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"CITIC\">\r\n          <label class=\"bank-ecitic\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SHRCB\">\r\n          <label class=\"bank-srcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"SHBANK\">\r\n          <label class=\"bank-bankofshanghai\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"NBBANK\">\r\n          <label class=\"bank-nbcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"HZCBB2C\">\r\n          <label class=\"bank-hccb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"BJBANK\">\r\n          <label class=\"bank-bankofbeijing\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"BJRCB\">\r\n          <label class=\"bank-bjrcb\"></label>\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"WZCBB2C-DEBIT\">\r\n          <label class=\"bank-wzcb\"></label>\r\n      </li>\r\n  </ul>\r\n  <hr>\r\n  <p class=\"text-center\">\r\n    <button type=\"button\" class=\"btn btn-default\" data-action=\"close\" data-weave=\"widget/blurb/main(1452)\">I got it</button>\r\n  </p>\r\n</div>"; return o; }; });
define('widget/order/paymenttype/main',["troopjs-browser/component/widget",
	"jquery",
	"when",
	"context",
	"widget/ajaxquery/main",
	"widget/api/main",
	"widget/popupbox/main",
	"template!./index.html",
	"template!./banklist.html",
	"json2",
	"underscore"
], function(Widget, $, when, context, ajaxQuery, api, PopupBox, template, templateBanklist) {
	"use strict";

	var URI_PAYMENT = api.get("payment");

	return Widget.extend(function($element, widgetName, sourceCode) {
		var me = this;

		me.sourceCode = sourceCode || "";
		me.deferred = when.defer();
		// order
		// confirm
		// orderdetail
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_PAYMENT,
				data: {
					language: context.language
				},
				type: "GET",
				dataType: "json"
			}).then(function(data) {
				var paymentTypes = data.Values;

				// Rendering
				me.html(template, {
					"paymentType": paymentTypes,
					"sourceCode": me.sourceCode,
					"cacheServer": context.cacheServer,
					"isMobile": context.isMobile
				}).then(function() {
					$me.find('.icheck').iCheck({
						checkboxClass: 'icheckbox_square',
						radioClass: 'iradio_square',
						checkedClass: 'checked',
						increaseArea: '20%'
					});

					// Rendered
					me.deferred.resolver.resolve();
				});
			});
		},
		"hidePayonDelivery": function() {
			var me = this;
			var $me = me.$element;

			// Hide "Cash on Delivery"
			$me.find("[data-action=orderconfirm-payment-2]").remove();
		},
		"hub/order/orderdetail": function(order) {
			var me = this;

			if (!order || !order.DeliverList) {
				return;
			}

			// Waiting for payment rendered
			me.deferred.promise.then(function() {
				_.each(order.DeliverList, function(e) {
					// Without "payment on delivery"
					// -----------------------------
					// 维果清上海商城店 - 3
					// Z&B Fitness Studio - 4
					// 泰生厨房 - 6
					// -----------------------------
					// 上海顺丰标准B - 8
					// 空运 - 9
					// 空运次晨 - 10
					// 空运B - 12
					if ((e.IsSelfPickUp &&
							e.StoreInfo &&
							_.indexOf([3, 4, 6], e.StoreInfo.StoreId) >= 0) ||
						_.indexOf([8, 9, 10, 12], e.ExpressCompanyId) >= 0) {
						me.hidePayonDelivery();
						return;
					}
				});
			});
		},
		"hub/order/paymenttype/submit": function(orderId) {
			var me = this;
			var $me = me.$element;

			var paymentType = $me.find(".iradio_square.checked > input").val();

			var postData = {
				orderId: orderId,
				payment: paymentType,
				language: context.language
			};

			var promiseValue = [];
			promiseValue.push(postData);
			return promiseValue;
		},
		"dom:[data-action=showallbanks]/click": function() {
			var me = this;

			var $templateBanklist = $(templateBanklist({
				"cacheServer": context.cacheServer
			}));
			// Lightbox configuration
			var popupboxBanklist = new PopupBox({
				msg: $templateBanklist,
				closeble: true,
				closeInner: true,
				zIndex: 1050,
				closeButtonList: ['[data-action=close]'],
				closeCallback: function() {}
			});

			popupboxBanklist.open();
		}
	});
});

define('troopjs-requirejs/template!widget/order/promotion/index.html',[],function() { return function template(data) { var o = "<h4>\r\n    <strong data-weave=\"widget/blurb/main(1239)\">Apply promotion code</strong>\r\n</h4>\r\n<p>\r\n    <small data-weave=\"widget/blurb/main(1240)\">Have any gift cards or promotional claim codes?</small>\r\n</p>\r\n<div class=\"the-form order-applypromotion\">\r\n    <form id=\"promotioncode\" class=\"standardform form-promotioncode\">\r\n        <div class=\"cf\">\r\n            <input type=\"text\" id=\"order-promotioncode\" placeholder=\"\" required>\r\n            <button class=\"\" type=\"submit\" data-action=\"applypromotioncode\" data-weave=\"widget/blurb/main(1241)\">APPLY</button>\r\n        </div>\r\n        <div>\r\n            <p data-action=\"invalidpromotioncode\" class=\"order-promotioncode-msg\"></p>\r\n        </div>\r\n    </form>\r\n</div>"; return o; }; });
define('widget/order/promotion/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/pageurl/main",
    "template!./index.html",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, Blurb, pageUrl, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_PROMOTION_CODE = api.get("order_promotion_code");

    function getStatus(code) {
        var status = [];
        status["1"] = "1242";
        status["2"] = "1270";
        status["3"] = "1242";
        status["4"] = "1367";
        status["5"] = "1368";
        status["6"] = "1369";
        status["7"] = "1388";
        status["8"] = "1389";
        status["9"] = "1390";
        status["10"] = "1391";
        status["11"] = "1468";
        status["12"] = "1625";

        return blurb.get(status[code]) || "";
    }

    function getSuccessStatus(code) {
        // Here code means promotion rule id
        var status = [];
        status["1"] = "1370";
        status["2"] = "1370";
        status["3"] = "1371";
        status["4"] = "1372";
        status["5"] = "1373";
        status["6"] = "1384";
        status["7"] = "1385";
        status["8"] = "1602";
        status["9"] = "1603";
        status["10"] = "1604";

        return blurb.get(status[code]) || "";
    }

    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;

        me.orderId = parseInt(orderId, 10);
    }, {
        "sig/start": function() {
            var me = this;

            me.html(template);
        },
        "dom:form/submit": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            var $code = $me.find("#order-promotioncode");
            var $submitBtn = $me.find("[data-action=applypromotioncode]");

            var code = _.trim($code.val());

            var postData = {
                OrderId: me.orderId,
                Code: code
            };

            ajaxQuery({
                url: URI_PROMOTION_CODE,
                data: JSON.stringify(postData),
                contentType: "application/json; charset=UTF-8",
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                $errMsg.text(getSuccessStatus(data.RuleId)).addClass("order-promotioncode-valid");

                $code.attr("baga", code);
                // Change order price summary
                me.publish("order/promotion", data.Order);
            }, function(e) {
                $errMsg.text(getStatus(e)).removeClass("order-promotioncode-valid");

                $code.removeAttr("baga");
                // Change order price summary reflesh
                me.publish("order/promotion");
            });
        },
        "dom:#order-promotioncode/focus": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $errMsg = $me.find("[data-action=invalidpromotioncode]");
            $errMsg.empty().removeClass("order-promotioncode-valid");
        },
        "hub/order/promotion/submit": function(orderId) {
            var me = this;

            var $me = me.$element;

            var $code = $me.find("#order-promotioncode");

            var code = _.trim($code.attr("baga"));
            var postData;

            if (code) {
                postData = {
                    OrderId: me.orderId,
                    Code: code
                };
            }

            var promiseValue = [];
            promiseValue.push(postData);

            return promiseValue;
        }
    });
});
define('widget/order/startdate/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/pageurl/main",
    "widget/time/main",
    "jquery.ui.datepicker",
    "jquery.ui.datepicker-zh-CN"
], function(Widget, $, context, pageUrl, Time) {
    "use strict";

    // To order from tomorrow, should order before 8:00 am
    var HOUR_AHEAD = 5;
    // Official holiday
    var HOLIDAY = [
		new Date(2016, 3, 1),
		new Date(2016, 3, 2),
		new Date(2016, 3, 3),
		new Date(2016, 3, 4),
    ];

    // Time function
    var INS_TIME = new Time();

    function getAvailableStartDate() {
        var now = new Date();
        var date = INS_TIME.getZeroDate(now);
        var deltaDay = 0;

        var hour = now.getHours();
        var startDate = hour >= HOUR_AHEAD ? INS_TIME.addDay(date, 2) : INS_TIME.addDay(date, 1);

        if (HOLIDAY && HOLIDAY.length > 0) {
            // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
            // 节假日后一天的订单，会放到节假日当天一起配送
            // 所以节假日前一天给厨房生产之后，不能订餐到节假日后一天的订单
            (function() {
                var i = 0;
                var iLens = HOLIDAY.length;
                var holiday;

                for (i = 0; i < iLens; i++) {
                    holiday = INS_TIME.addDay(INS_TIME.getZeroDate(HOLIDAY[i]), 1);
                    if (INS_TIME.compareDate(holiday, startDate) === 0) {
                        startDate = INS_TIME.addDay(startDate, 1);
                        continue;
                    }
                }
            })();
        }

        return startDate;
    }

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Available first date for order
            var dateAvailable = getAvailableStartDate();
            // Initialize start date from today
            me.publish("deliveryStartDate/change", {
                // Date info
                "availableStartDate": dateAvailable,
                "selectedDate": dateAvailable
            });
        }
    });
});

define('troopjs-requirejs/template!widget/order/summary/index.html',[],function() { return function template(data) { var o = "<div class=\"subtotal\">\r\n    <table>\r\n        "; if (data.preferentialProduct) { o += "\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1033)\">Product:</strong></td>\r\n                <td class=\"number-list-price value\">&yen;" +data.product+ "</td>\r\n            </tr>\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1237)\">Product(Preferential):</strong></td>\r\n                <td class=\"number-price value\">&yen;" +data.preferentialProduct+ "</td>\r\n            </tr>\r\n        "; } else { o += "\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1033)\">Product:</strong></td>\r\n                <td class=\"number-price value\">&yen;" +data.product+ "</td>\r\n            </tr>\r\n        "; } o += "\r\n        "; if (data.preferentialDelivery) { o += "\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1034)\">Delivery:</strong></td>\r\n                <td class=\"number-list-price value\">&yen;" +data.delivery+ "</td>\r\n            </tr>\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1238)\">Delivery(Preferential):</strong></td>\r\n                <td class=\"number-price value\">&yen;" +data.preferentialDelivery+ "</td>\r\n            </tr>\r\n        "; } else { o += "\r\n            <tr>\r\n                <td class=\"name\"><strong data-weave=\"widget/blurb/main(1034)\">Delivery:</strong></td>\r\n                <td class=\"number-price value\">&yen;" +data.delivery+ "</td>\r\n            </tr>\r\n        "; } o += "\r\n    </table>\r\n</div>\r\n<div class=\"total\">\r\n    <table>\r\n        <tr>\r\n            <td class=\"name\"><strong data-weave=\"widget/blurb/main(1040)\">Total:</strong></td>\r\n            <td class=\"number-price value\">&yen;" +data.total+ "</td>\r\n        </tr>\r\n    </table>\r\n</div>"; return o; }; });
define('widget/order/summary/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/number",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "template!./index.html",
    "json2",
    "underscore"
], function(Widget, $, when, context, number, ajaxQuery, api, pageUrl, template) {
    "use strict";

    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend(function($element, widgetName, orderId) {
        var me = this;

        // order
        // confirm
        // orderdetail
        me.orderId = orderId;
    }, {
        "render": function(dataOrder) {
            var me = this;
            var $me = me.$element;

            var renderData = {
                "orderId": dataOrder.OrderId,
                "product": number.getDecimal(dataOrder.TotalPrice),
                "delivery": number.getDecimal(dataOrder.DeliverFee),
                "total": number.getDecimal(dataOrder.PreferentialPrice + dataOrder.PreferentialLogisticsFee)
            };

            // Append preferential logic
            if (dataOrder.TotalPrice !== dataOrder.PreferentialPrice) {
                _.extend(renderData, {
                    "preferentialProduct": number.getDecimal(dataOrder.PreferentialPrice)
                });
            }
            if (dataOrder.DeliverFee !== dataOrder.PreferentialLogisticsFee) {
                _.extend(renderData, {
                    "preferentialDelivery": number.getDecimal(dataOrder.PreferentialLogisticsFee)
                });
            }

            me.html(template, renderData);
        },
        "queryOrder": function() {
            var me = this;
            var $me = me.$element;

            var orderId = me.orderId;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var order = data.Order;

                if (order.OrderStatusCode === -10){
                     pageUrl.goto("/payment/topay", "orderid/" + order.OrderID);
                     return;
                }
                else if (order.OrderStatusCode > 0){
                    pageUrl.goto("/order/success", "orderid/" + order.OrderID, {
                        "orderid": order.OrderID
                    });
                    return;
                }

                // Rendering
                me.render(order);
                // Publish order detail
                me.publish("order/orderdetail", order);
            });
        },
        "sig/start": function() {
            var me = this;
            me.queryOrder();
        },
        "hub/order/promotion": function(data) {
            var me = this;
            if (data) {
                me.render(data);
            } else {
                me.queryOrder();
            }
        }
    });
});

define('troopjs-requirejs/template!widget/ordernow/index.html',[],function() { return function template(data) { var o = "<a class=\"btn btn-lg btn-default btn-shopping\" href=\"/cleanse\" data-weave=\"widget/href/main widget/blurb/main(1220)\">\r\n\tORDER NOW\r\n</a>"; return o; }; });
define('widget/ordernow/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            me.html(template);
        }
    });
});

define('troopjs-requirejs/template!widget/orders/delivery/index.html',[],function() { return function template(data) { var o = "";
	var deliveryTimeId = data.deliveryTimeId || 0;
o += "\r\n<div class=\"linebox popup-changedeliveryaddress v-popup-container\">\r\n    <h4 data-weave=\"widget/blurb/main(1346)\">Delivery Address</h4>\r\n\t<div data-action=\"changedeliveryaddress-container\"></div>\r\n    <h4 data-action=\"tweakdeliverytime\" data-weave=\"widget/blurb/main(1347)\">Delivery Date</h4>\r\n\t<div data-action=\"tweakdeliverytime\" data-weave=\"widget/delivery/deliverytime/main(deliveryTimeId)\" data-delivery-time-id=\"" +deliveryTimeId+ "\"></div>\r\n\t<div class=\"changedeliveryaddress-action\" data-action=\"tweakdeliverytime\">\r\n\t    <a class=\"btn btn-md btn-default\" data-action=\"changedeliveryaddress-save\" data-weave=\"widget/blurb/main(1348)\">SAVE CHANGES</a>\r\n\t    <a class=\"btn btn-md btn-default\" data-action=\"close\" data-weave=\"widget/blurb/main(1075)\">CANCEL</a>\r\n\t</div>\r\n</div>"; return o; }; });
define('widget/orders/delivery/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "when",
    "jquery",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/popupbox/main",
    "widget/infobox/main",
    "template!./index.html"
], function(Widget, weave, when, $, ajaxQuery, api, PopupBox, InfoBox, template) {
    "use strict";

    var DELIVERY_ADDRESS;
    var DELIVERY_TIME;

    var URI_ORDER_UPDATERECEIVER = api.get("order_updatereceiver");

    function getStatus(code) {
        var status = [];
        status["-100"] = "1470"; //参数错误
        status["-1"] = "1470"; //参数不正确
        status["-300"] = "1471"; //运费增加
        status["-400"] = "1469"; //地区不配送
        return status[code] || "";
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;
    }, {
        "hub/orders/changeDeliveryAddress/popup": function(orderId, dailyOrderId, deliveryTimeId) {
            var me = this;

            me.dailyOrderId = dailyOrderId;
            me.orderId = orderId;

            // Create template
            var $me = $(template({
                "deliveryTimeId": deliveryTimeId
            }));

            // Lightbox configuration
            me.popupOrderEdit = new PopupBox({
                msg: $me,
                closeble: true,
                closeInner: true,
                zIndex: 20,
                overLap: "replace",
                closeButtonList: ['[data-action=close]'],
                // TODO:
                closeCallback: function() {
                    //me.unsubscribe("changeDeliveryAddress/updateOrder");
                    $me.remove();
                }
            });

            // Manually weave template
            // TODO: Show lightbox first
            me.popupOrderEdit.open().then(function() {
                me.publish("receiver/refreshAddress");
            });
        },
        "hub/receiver/changeDeliverAddress": function(receiver) {
            var me = this;

            if (!receiver) {
                return;
            }

            DELIVERY_ADDRESS = receiver;
        },
        "hub/receiver/changeDeliverTime": function(deliverTime) {
            var me = this;

            if (!deliverTime) {
                return;
            }

            DELIVERY_TIME = deliverTime;
        },
        // Render edit
        "hub/receiver/editAddress": function(args) {
            var me = this;
            var $me = me.$element;

            $me.find("[data-action=tweakdeliverytime]").hide();
            var $delivery = $me.find("[data-action=changedeliveryaddress-container]");
            var editReceiverId = (args && args.isEdit) ? args.receiverId : "";

            $delivery.html("<div data-weave=\"widget/delivery/editaddress/main(receiverid)\" data-receiverid=\"" + editReceiverId + "\"></div>");
            // Manually weave template
            weave.apply($delivery.find("[data-weave]")).then(function() {
                me.popupOrderEdit.centralize();
            });
        },
        // Address updated, refresh deliver type
        "hub/receiver/refreshAddress": function(receiverId) {
            var me = this;
            var $me = me.$element;
            receiverId = receiverId || "";

            $me.find("[data-action=tweakdeliverytime]").show();
            var $delivery = $me.find("[data-action=changedeliveryaddress-container]");
            $delivery.html("<div data-weave=\"widget/delivery/receiver/main(receiverid)\" data-receiverid=\"" + receiverId + "\"></div>");
            // Manually weave template
            // By weave following widget will - Submit order information on current day
            weave.apply($delivery.find("[data-weave]")).then(function() {
                me.popupOrderEdit.centralize();
            });
        },
        "dom:[data-action=changedeliveryaddress-save]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var postData = {
                "OrderId": me.orderId,
                "SubOrderId": me.dailyOrderId,
                "ReceiverId": DELIVERY_ADDRESS.Id,
                "DeliveryTimeId": DELIVERY_TIME.Id
            };

            ajaxQuery({
                url: URI_ORDER_UPDATERECEIVER,
                data: JSON.stringify(postData),
                contentType: "application/json; charset=UTF-8",
                type: "POST",
                dataType: "json"
            }).then(function(data) {
                // Close popup
                me.popupOrderEdit.close().then(function() {
                    me.publish("order/detail/update");
                });
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});

define('troopjs-requirejs/template!widget/orders/detail/index.html',[],function() { return function template(data) { var o = "";
    var order = data.order;
    var editble = data.editble;
    var canPay = checkOrderCanPay(order.OrderStatusCode);
o += "\r\n<p class=\"reminder\" data-weave=\"widget/blurb/main(1119)\">\r\n    You can modify your order any time up to 24 hours before your scheduled delivery. If you seleced online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.\r\n</p>\r\n<div class=\"orderdetail-topic\">\r\n    <h5><strong data-weave=\"widget/blurb/main(1060)\">Order details</strong></h5>\r\n    <div>\r\n        <ul class=\"hor float-abs seplist\">\r\n            <li>\r\n                <span data-weave=\"widget/blurb/main(1061)\">Order number:</span> <strong class=\"text-striking\">" +order.OrderID+ "</strong>\r\n            </li>\r\n            <li>\r\n                <span class=\"pull-left\" data-weave=\"widget/blurb/main(1125)\">Payment status:</span>\r\n                <ul class=\"hor pull-left\">\r\n                    "; 
                        renderOrderPayments(order.OrderID, order.OrderPayments, canPay); 
                    o += "\r\n                </ul>\r\n            </li>\r\n            <li>\r\n                <span data-weave=\"widget/blurb/main(1092)\">Order status:</span> \r\n                <span class=\"text-status-" +getOrderStatusByCode(order.OrderStatusCode)+ "\">" +order.OrderStatusCodeString+ "</span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<div class=\"orderdetail-list\">\r\n    ";
        (function(){
            var i = 0;
            var iLens = order.DeliverList.length;
            for(i = 0; i < iLens; i++){
                if(i%2 === 0 && i !== 0){
                    o += "\r\n                    </ul>\r\n                    ";
                }
                if(i%2 === 0){
                    o += "\r\n                    <ul class=\"hor\">\r\n                    ";
                }
                renderDailyOrder(order.DeliverList[i], canPay);
            }
        })();
    o += "\r\n</div>\r\n\r\n"; function renderDailyOrder(dailyOrder, canPay) { o += "\r\n    <li>\r\n        <div class=\"orderdetail-order orderdetail-delivered\">\r\n            <p class=\"orderdetail-date\">\r\n                <strong>" +dailyOrder.DeliverDate+ ",</strong>\r\n                "; if(dailyOrder.IsSelfPickUp) { o += "\r\n                    <span>-</span>\r\n                "; } else { o += "\r\n                    <span>" +dailyOrder.DeliverTime+ "</span>\r\n                "; } o += "\r\n                <span class=\"orderdetail-status\">" +(dailyOrder.SubStatusCodeString || "")+ "</span>\r\n            </p>\r\n            ";
                (function(){
                    var i = 0;
                    var iLens = dailyOrder.ProductList.length;
                    for(i = 0; i < iLens; i++){
                        renderDailyProduct(dailyOrder.ProductList[i]);
                    }
                })();
            o += "\r\n            "; if(dailyOrder.IsSelfPickUp) { o += "\r\n                <p>\r\n                    <strong data-weave=\"widget/blurb/main(1058)\">Pick up in store:</strong>\r\n                    <span>" +dailyOrder.StoreInfo.StoreName + "</span>\r\n                </p>\r\n                <p>\r\n                    <strong data-weave=\"widget/blurb/main(1382)\">Business hours:</strong>\r\n                    <span><small>" +dailyOrder.StoreInfo.StoreBusinessHours+ "</small></span>\r\n                </p>\r\n                <p>\r\n                    <strong data-weave=\"widget/blurb/main(1128)\">Store address:</strong>\r\n                    <span>" +dailyOrder.StoreInfo.StoreAddress + "</span>\r\n                    "; if (dailyOrder.StoreInfo.StoreMapUrl) { o += "\r\n                        [<a class=\"link-fn\" data-url=\"" +dailyOrder.StoreInfo.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n                    "; } o += "\r\n                </p>\r\n            "; } else { o += "\r\n                <p>\r\n                    <strong data-weave=\"widget/blurb/main(1066)\">Deliver to:</strong>\r\n                    <span>" +dailyOrder.Name+ ", " +dailyOrder.Address+ "</span>\r\n                    "; if (editble && dailyOrder.SubStatusCode === 10 && canPay) { o += "\r\n                        <a class=\"link-fn\" data-weave=\"widget/blurb/main(1088)\" data-order-id=\"" +order.OrderID+ "\" data-ordersm-id=\"" +dailyOrder.Id+ "\" data-delivery-time-id=\"" +dailyOrder.DeliverTimeId+ "\" data-action=\"changeDeliveryAddress\">Edit this address</a>\r\n                    "; } o += "\r\n                </p>\r\n            "; } o += "\r\n            "; if(dailyOrder.Mobile) { o += "\r\n                <p><strong data-weave=\"widget/blurb/main(1116)\">Tel:</strong> " +dailyOrder.Mobile+ "</p>\r\n            "; } o += "\r\n            "; if(!dailyOrder.IsSelfPickUp && dailyOrder.DeliveryNo) { o += "\r\n                ";
                    switch (dailyOrder.TemplateType) { 
                        case 1:
                o += "\r\n                            <p>\r\n                                <strong data-weave=\"widget/blurb/main(1206)\">Package via:</strong>\r\n                                <a href=\"http://sh.cn.ta-q-bin.com/\" target=\"_blank\">" +dailyOrder.ExpressCompanyName+ "</a>\r\n                            </p>\r\n                ";
                        break;
                        case 2:
                        default:
                o += "\r\n                            <p>\r\n                                <strong data-weave=\"widget/blurb/main(1206)\">Package via:</strong>\r\n                                <a href=\"http://www.sf-express.com/cn/sc/dynamic_functions/waybill/#search/bill-number%>\" target=\"_blank\">" +dailyOrder.ExpressCompanyName+ "</a>\r\n                            </p>\r\n                "; 
                        break;
                    }
                o += "\r\n                <p><strong data-weave=\"widget/blurb/main(1093)\">Tracking number:</strong> " +dailyOrder.DeliveryNo+ "</p>\r\n            "; } o += "\r\n            <!--div class=\"orderdetail-change\">\r\n                <a data-weave=\"widget/blurb/main(1094)\">+ Change</a>\r\n            </div-->\r\n        </div>\r\n    </li>\r\n"; } o += "\r\n";  function renderDailyProduct(dailyProduct) { o += "\r\n    <p><strong>" +dailyProduct.Count+ "</strong> * " +dailyProduct.ProductName+ "</p>\r\n"; } o += "\r\n";  function getOrderStatusByCode(code){
        var status = "";
        switch(code){
            case 30: status = "completed";
                break;
            default: status = "inprocess";
                break;
        }
        return status;
    }
    function checkOrderCanPay(code){
        return code > -20;
    }
o += "\r\n";  function renderOrderPayments(orderId, payments, canPay){
        for(var i = 0 ; i< payments.length ; i++){ 
            var obj = payments[i];
            var type = obj.PaymentType;
            var typeId = obj.PaymentTypeId;
            var payment = obj.Payment;
            var isPaid = obj.IsPaid;
            var isOnline = obj.IsOnlinePay;
        o += "\r\n                <li style=\"margin-left: 10px;\">\r\n                    <span>";if(i>0){o += "/ ";}o += "" +type+ "</span>&nbsp;\r\n                    <span class=\"number-price\">&yen;" +payment+ "</span>&nbsp;\r\n                    <span>\r\n                        "; if (isOnline && typeId === 1 && isPaid === false && canPay) { o += "\r\n                            <a class=\"btn btn-success btn-xs\" href=\"/payment/topay\" data-hash=\"orderid/" +orderId+ "\" data-weave=\"widget/blurb/main(1458) widget/href/main\">Pay</a>\r\n                        ";}else if(isPaid===true){o += "\r\n                            <span class=\"text-status-completed\" data-weave=\"widget/blurb/main(1619)\">已支付</span>\r\n                        ";}else if(isPaid===false){o += "\r\n                            <span class=\"text-status-unpaid\" data-weave=\"widget/blurb/main(1618)\">未支付</span>\r\n                        ";}o += "\r\n                    </span>\r\n                </li>\r\n           "; }
    }
 return o; }; });
define('widget/orders/detail/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/popupbox/main",
    "widget/pageurl/main",
    "widget/orders/delivery/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!../../map/index.html",
    "jquery.cookie",
    "json2"
], function(Widget, $, context, ajaxQuery, api, PopupBox, pageUrl, Delivery, troopHash, template, templateMap) {
    "use strict";

    (new Delivery($(document.body))).start();
    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend(function($element, widgetName, editble) {
        var me = this;

        me.editble = editble || false;
    }, {
        "render": function(orderId) {
            var me = this;
            var $me = me.$element;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            // Ajax
            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                me.html(template, {
                    "order": data.Order,
                    "editble": me.editble
                });
            });
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            var orderId = me.orderId = hashMap.orderid;
            // orderId is required
            // or will direct to: account/orders.html
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Render
            me.render(orderId);
        },
        "hub/order/detail/update": function() {
            var me = this;
            var orderId = me.orderId;

            // Render
            me.render(orderId);
        },
        "dom:[data-action=changeDeliveryAddress]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);
            var orderId = parseInt($el.data("orderId"), 10);
            var dailyOrderId = parseInt($el.data("ordersmId"), 10);
            var deliveryTimeId = parseInt($el.data("deliveryTimeId"), 10);

            // Popup order-edit lightbox.
            me.publish("orders/changeDeliveryAddress/popup",orderId, dailyOrderId, deliveryTimeId);
        }
    });
});

define('troopjs-requirejs/template!widget/orders/list/index.html',[],function() { return function template(data) { var o = "";
    var EXPIRED_CODE = 5;

    var orders = data.orders || [];
    var disabled = data.disabled || false;
    var urlDetail = data.urlDetail;
    var pages = data.pages || 1;
    var index = data.index || 1;
o += "\r\n<table class=\"table table-bordered table-hover\">\r\n    <thead>\r\n        <tr>\r\n            <td width=\"90\"><span data-weave=\"widget/blurb/main(1098)\">Order number</span></td>\r\n            <td width=\"100\"><span data-weave=\"widget/blurb/main(1099)\">Order date</span></td>\r\n            <td width=\"100\"><span data-weave=\"widget/blurb/main(1100)\">Date of first delivery</span></td>\r\n            <td width=\"130\"><span data-weave=\"widget/blurb/main(1101)\">Total Amount (RMB)</span></td>\r\n            <td><span data-weave=\"widget/blurb/main(1124)\">Payment Status</span></td>\r\n            <td><span data-weave=\"widget/blurb/main(1102)\">Order Status</span></td>\r\n            <td width=\"140\" data-weave=\"widget/blurb/main(1620)\">Action</td>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        ";
            var i = 0;
            var iLens = orders.length;
            for(i = 0; i < iLens; i++){
                renderOrder(orders[i]);
            }
        o += "\r\n    </tbody>\r\n</table>\r\n"; if(pages > 1){ o += "\r\n    <ul class=\"pagination\">\r\n        <li "; if(index <= 1) { o += "class=\"disabled\""; } o += ">\r\n            <a class=\"link-fn\" "; if(index > 1) { o += "data-action=\"prevpage\""; } o += ">&laquo;</a>\r\n        </li>\r\n        ";
            var j = 1;
            var jLens = pages;
            for(j = 1; j <= jLens; j++) {
                renderPages(j, index);
            }
        o += "\r\n        <li "; if(index >= pages) { o += "class=\"disabled\""; } o += ">\r\n            <a class=\"link-fn\" "; if(index < pages) { o += "data-action=\"nextpage\""; } o += ">&raquo;</a>\r\n        </li>\r\n    </ul>\r\n"; } o += "\r\n\r\n";
function renderPages(page, index) { 
    if(page !== index){
o += "\r\n        <li>\r\n            <a class=\"link-fn\" data-action=\"switchpage\" data-page=\"" +page+ "\">" +page+ "</a>\r\n        </li>\r\n"; } else { o += "\r\n        <li class=\"active\">\r\n            <a class=\"link-fn\">" +page+ " <span class=\"sr-only\">(current)</span></a>\r\n        </li>    \r\n"; }
} o += "\r\n\r\n"; function renderOrder(order) {
        var orderPrice= (order.PreferentialPrice + order.PreferentialLogisticsFee); o += "\r\n    <tr data-id=\"" +order.Id+ "\">\r\n        <td>\r\n            <a href=\"" +urlDetail+ "\" data-hash=\"orderid/" +order.Id+ "\" data-weave=\"widget/href/main\">\r\n                " +order.Id+ "\r\n            </a>\r\n        </td>\r\n        <td><span>" +order.OrderDate+ "</span></td>\r\n        <td><span>" +order.FirstDeliveryDate+ "</span></td>\r\n        <td class=\"number-price\"><span>&yen;" +orderPrice+ "</span></td>\r\n        <td>\r\n            <ul>\r\n               "; 
                var canPay = checkOrderCanPay(order.OrderStatusCode);
               renderOrderPayments(canPay,order.Id, order.OrderPayments); o += "\r\n            </ul>\r\n        </td> \r\n        <td><span class=\"text-status-" +getOrderStatusByCode(order.OrderStatusCode)+ "\">" +order.OrderStatusCodeString+ "</span></td>\r\n        <td>\r\n            <a class=\"btn btn-default btn-sm\" href=\"" +urlDetail+ "\" data-hash=\"orderid/" +order.Id+ "\" data-weave=\"widget/href/main widget/blurb/main(1621)\">Details</a>\r\n            "; if (checkOrderCanCancel(order.OrderStatusCode, order.OrderPayments) && !disabled) { o += "\r\n                <a class=\"btn btn-default btn-sm\" data-id=\"" +order.Id+ "\" data-action=\"deleteorder\" data-weave=\"widget/blurb/main(1219)\">Cancel</a>\r\n            "; } o += " \r\n        </td>\r\n    </tr>\r\n"; } o += "\r\n\r\n";
    function getOrderStatusByCode(code) {
        var status = "";
        switch(code) {
            case 30: status = "completed";
                break;
            case 10: status = "inprocess";
                break;
            case 20: status = "inprocess";
                break;
            default: status = "inprocess";
                break;
        }
        return status;
    }
    function checkOrderCanCancel(code, payments) {
        var canCancel = false;
        var hasPaid = false;

        for(var i = 0, iLens = payments.length; i < iLens; i++) { 
            if(payments[i].IsPaid) {
                hasPaid = true;
            }
        }
        if(!hasPaid && (code === -20 || code === -10 || code === 10)){
            canCancel = true;
        }
        return canCancel;
    }
    function checkOrderCanPay(code){
        return code > -20;
    }
o += "\r\n";   function renderOrderPayments(canPay, orderId, payments) {
        for(var i = 0, iLens = payments.length; i < iLens; i++) { 
            var obj = payments[i];
            var type = obj.PaymentType;
            var typeId = obj.PaymentTypeId;          
            var payment = obj.Payment;
            var isPaid = obj.IsPaid;
            var isOnline = obj.IsOnlinePay;
        o += "\r\n                <li>\r\n                    <p>\r\n                        <span>" +type+ "</span>\r\n                        <span class=\"number-price\">&yen;" +payment+ "</span>\r\n                        <span class=\"pull-right\">\r\n                            "; if (isOnline && typeId === 1 && isPaid === false && canPay) { o += "\r\n                                <a class=\"btn btn-success btn-sm\" href=\"/payment/topay\" data-hash=\"orderid/" +orderId+ "\" data-weave=\"widget/blurb/main(1458) widget/href/main\">Pay</a>\r\n                            ";}else if(isPaid===true){o += "\r\n                                <span class=\"text-status-completed\" data-weave=\"widget/blurb/main(1619)\">已支付</span>\r\n                            ";}else if(isPaid===false){o += "\r\n                                <span class=\"text-status-unpaid\" data-weave=\"widget/blurb/main(1618)\">未支付</span>\r\n                            ";}o += "\r\n                        </span>\r\n                    </p>\r\n                </li>\r\n           "; }
    }
 return o; }; });
define('widget/orders/list/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/infobox/main",
    "template!./index.html"
], function(Widget, $, context, ajaxQuery, api, Blurb, InfoBox, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    var URI_ORDER_DELETE = api.get("order_delete");

    function getStatus(code) {
        var status = [];
        status["1"] = "1624"; //没有权限
        status["2"] = "1624"; //无法删除

        return status[code] || "";
    }

    return Widget.extend(function(){
        var me = this;

        me.index = 0;
    },{
        "hub/orders/list": function(data) {
            var me = this;

            me.index = data.index || 1;
            // Rendering
            return me.html(template, data);
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "page/" + index;
        },
        "dom:[data-action=deleteorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            if (confirm(blurb.get(1248) || "Sure to delete this order?")) {
                // Delete order by orderId
                // ...
                ajaxQuery({
                    url: URI_ORDER_DELETE,
                    data: {
                        orderId: orderId,
                        language: context.language
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("tr[data-id=" + orderId + "]").hide("slow", function() {
                        $(this).remove();
                    });
                }, function(e){
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            }
        }
    });
});

define('troopjs-requirejs/template!widget/orders/index.html',[],function() { return function template(data) { var o = "<p class=\"reminder\" data-weave=\"widget/blurb/main(1119)\">\r\n    You can cancel your order any time up to 24 hours before your scheduled delivery. If you seleced online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.\r\n</p>\r\n<div data-weave=\"widget/orders/list/main\"></div>"; return o; }; });
define('widget/orders/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders");

    var PAGE_COUNT = 10;

    return Widget.extend(function($element, widgetName, pageCount) {
        var me = this;

        me.index = 1;
        me.pageCount = pageCount || PAGE_COUNT;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath) || {
                "page": 1
            };
            var index = hashMap.page || 1;

            // Apply to me
            me.index = parseInt(index, 10);

            // Ajax
            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    "startIndex": (me.index - 1) * me.pageCount + 1,
                    "count": me.pageCount,
                    "language": context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                // Rendering
                me.html(template).then(function(){
                    // Publish
                    me.publish("orders/list", {
                        "orders": data.Orders,
                        "disabled": false,
                        "urlDetail": "/account/orderdetail",
                        "pages": Math.ceil(data.Total / me.pageCount),
                        "index": me.index
                    });
                });
            });
        }
    });
});

define('troopjs-requirejs/template!widget/orders/mobile/detail/index.html',[],function() { return function template(data) { var o = "";
    var order = data.order;
    var editble = data.editble;
o += "\r\n<p class=\"reminder\" data-weave=\"widget/blurb/main(1119)\">\r\n    You can modify your order any time up to 24 hours before your scheduled delivery. If you seleced online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.\r\n</p>\r\n<div class=\"orderdetail-topic\">\r\n    <h5><strong data-weave=\"widget/blurb/main(1060)\">Order details</strong></h5>\r\n    <table class=\"table table-bordered table-hover\">\r\n        <tbody>\r\n        <tr>\r\n            <td colspan=\"2\"><span data-weave=\"widget/blurb/main(1061)\">Order number:</span> <span class=\"text-striking\">" +order.OrderID+ "</span></td>\r\n        </tr>\r\n        <tr>\r\n            <td><span data-weave=\"widget/blurb/main(1125)\">Payment status:</span> <span class=\"text-status-" +getPayStatusById(order.IsPaid)+ "\">" +order.PayStatus+ "</span></td>\r\n            <td><span data-weave=\"widget/blurb/main(1092)\">Order status:</span> <span class=\"text-status-" +getOrderStatusById(order.OrderStatusID)+ "\">" +order.OrderStatus+ "</span></td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<div class=\"orderdetail-list\">\r\n    ";
        (function(){
            var i = 0;
            var iLens = order.DeliverList.length;
            for(i = 0; i < iLens; i++){
                if(i%2 === 0 && i !== 0){
                    o += "\r\n                    </ul>\r\n                    ";
                }
                if(i%2 === 0){
                    o += "\r\n                    <ul class=\"hor\">\r\n                    ";
                }
                renderDailyOrder(order.DeliverList[i],i+1);
            }
        })();
    o += "\r\n</div>\r\n\r\n"; function renderDailyOrder(dailyOrder,index) {
o += "\r\n    <li>\r\n        <table class=\"table table-bordered table-hover table-condensed\">\r\n            <tbody>\r\n            <tr>\r\n                <td colspan=\"2\"><strong><span data-weave=\"widget/blurb/main(1479)\">子订单</span> " +index+ "</strong></td>\r\n            </tr>\r\n            <tr>\r\n                <td width=\"90\"><span data-weave=\"widget/blurb/main(1475)\">外送日期：</span></td>\r\n                <td><strong>" +dailyOrder.DeliverDate+ "</strong><br><span>" +dailyOrder.DeliverTime+ "</span></td>\r\n            </tr>\r\n            <tr>\r\n                <td><span data-weave=\"widget/blurb/main(1478)\">状态：</span></td>\r\n                <td><span class=\"orderdetail-status\">" +(dailyOrder.Status || "")+ "</span></td>\r\n            </tr>\r\n            <tr>\r\n                <td><span data-weave=\"widget/blurb/main(1476)\">Delivery address:</span></td>\r\n                <td>\r\n                    "; if(dailyOrder.IsSelfPickUp) { o += "\r\n                    <p>\r\n                        <strong data-weave=\"widget/blurb/main(1058)\">Pick up in store</strong>\r\n                        <span>" +dailyOrder.StoreInfo.StoreName + "</span>\r\n                    </p>\r\n                    <p>\r\n                        <strong data-weave=\"widget/blurb/main(1128)\">Store address:</strong>\r\n                        <span>" +dailyOrder.StoreInfo.StoreAddress + "</span>\r\n                        "; if (dailyOrder.StoreInfo.StoreMapUrl) { o += "\r\n                        [<a class=\"link-fn\" data-url=\"" +dailyOrder.StoreInfo.StoreMapUrl+ "\" data-weave=\"widget/map/main widget/blurb/main(1381)\">View map</a>]\r\n                        "; } o += "\r\n                    </p>\r\n                    "; } else { o += "\r\n                    <p>\r\n                        <span>" +dailyOrder.FirstName+ " " +dailyOrder.LastName+ ", " +dailyOrder.Address+ "</span>\r\n                        "; if (editble && dailyOrder.StatusId < 1) { o += "\r\n                        <a class=\"link-fn\" data-weave=\"widget/blurb/main(1088)\" data-ordersm-id=\"" +dailyOrder.Id+ "\" data-action=\"changeDeliveryAddress\">Edit this address</a>\r\n                        "; } o += "\r\n                    </p>\r\n                    "; } o += "\r\n                </td>\r\n            </tr>\r\n            <tr>\r\n                <td><span data-weave=\"widget/blurb/main(1116)\">Tel:</span></td>\r\n                <td>"; if(dailyOrder.Mobile) { o += "\r\n                    " +dailyOrder.Mobile+ "\r\n                    "; } o += "</td>\r\n            </tr>\r\n            <tr>\r\n                <td><span data-weave=\"widget/blurb/main(1477)\">清单：</span></td>\r\n                <td>";
                    (function(){
                    var i = 0;
                    var iLens = dailyOrder.ProductList.length;
                    for(i = 0; i < iLens; i++){
                    renderDailyProduct(dailyOrder.ProductList[i]);
                    }
                    })();
                    o += "</td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n\r\n        <div class=\"orderdetail-order orderdetail-delivered\">\r\n            "; if(!dailyOrder.IsSelfPickUp && dailyOrder.DeliveryNo) { o += "\r\n                ";
                    switch (dailyOrder.TemplateType) { 
                        case 1:
                o += "\r\n                            <p>\r\n                                <strong data-weave=\"widget/blurb/main(1206)\">Package via:</strong>\r\n                                <a href=\"http://sh.cn.ta-q-bin.com/\" target=\"_blank\">" +dailyOrder.ExpressCompanyName+ "</a>\r\n                            </p>\r\n                ";
                        break;
                        case 2:
                        default:
                o += "\r\n                            <p>\r\n                                <strong data-weave=\"widget/blurb/main(1206)\">Package via:</strong>\r\n                                <a href=\"http://www.sf-express.com/cn/sc/dynamic_functions/waybill/#search/bill-number%>\" target=\"_blank\">" +dailyOrder.ExpressCompanyName+ "</a>\r\n                            </p>\r\n                "; 
                        break;
                    }
                o += "\r\n                <p><strong data-weave=\"widget/blurb/main(1093)\">Tracking number:</strong> " +dailyOrder.DeliveryNo+ "</p>\r\n            "; } o += "\r\n            <!--div class=\"orderdetail-change\">\r\n                <a data-weave=\"widget/blurb/main(1094)\">+ Change</a>\r\n            </div-->\r\n        </div>\r\n    </li>\r\n"; } o += "\r\n"; function renderDailyProduct(dailyProduct) { o += "\r\n    <p><strong>" +dailyProduct.Count+ "</strong> * " +dailyProduct.ProductName+ "</p>\r\n"; } o += "\r\n"; function getOrderStatusById(id){
        var status = "";

        switch(id){
            case 2: status = "completed";
                break;
            case 1: status = "inprocess";
                break;
            default:break;
        }

        return status;
    }
    function getPayStatusById(isPaid){
        var status = "";

        if(isPaid) {
            status = "completed";
        }
        else {
            status = "unpaid";
        }

        return status;
    }
 return o; }; });
define('widget/orders/mobile/detail/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/popupbox/main",
    "widget/pageurl/main",
    "widget/orders/delivery/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!../../../map/index.html",
    "jquery.cookie",
    "json2"
], function(Widget, $, context, ajaxQuery, api, PopupBox, pageUrl, Delivery, troopHash, template, templateMap) {
    "use strict";

    (new Delivery($(document.body))).start();
    var URI_ORDERDETAIL = api.get("orderdetail");

    return Widget.extend(function($element, widgetName, editble) {
        var me = this;

        me.editble = editble || false;
    }, {
        "render": function(orderId) {
            var me = this;
            var $me = me.$element;

            if (!orderId) {
                return;
            }

            var postData = {
                language: context.language,
                orderid: orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                me.html(template, {
                    "order": data.Order,
                    "editble": me.editble
                });
            });
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);

            var orderId = me.orderId = hashMap.orderid;
            // orderId is required
            // or will direct to: account/orders.html
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            // Render
            me.render(orderId);
        },
        "hub/order/detail/update": function() {
            var me = this;
            var orderId = me.orderId;

            // Render
            me.render(orderId);
        },
        "dom:[data-action=changeDeliveryAddress]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);
            var dailyOrderId = parseInt($el.data("ordersmId"), 10);

            // Popup order-edit lightbox.
            me.publish("orders/changeDeliveryAddress/popup", dailyOrderId);
        }
    });
});

define('troopjs-requirejs/template!widget/orders/mobile/index.html',[],function() { return function template(data) { var o = "<div data-action=\"myorders\"></div>"; return o; }; });
define('widget/orders/mobile/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/cookie/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
], function(Widget, weave, $, ajaxQuery, api, cookie, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_ACCOUNT = api.get("account");

    return Widget.extend({
        "render": function(data) {
            var me = this;
            var $me = me.$element;

            // Rendering
            me.html(template, data).then(function() {

                // Signin?
                cookie.get("memberid").then(function() {
                    // Get User Account
                    ajaxQuery({
                        url: URI_ACCOUNT,
                        type: "GET",
                        dataType: "json"
                    }).then(function(data) {
                        // Signin
                        me.publish("mine/hasLogin");
                    }, function() {
                        //noLogin
                        me.publish("mine/nonLogin");
                    });
                }, function() {
                    //
                    me.publish("mine/nonLogin");
                });
            });
        },
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.render();
        },
        // Already login, is a member
        "hub/mine/hasLogin": function() {
            var me = this;
            var $me = me.$element;

            var $orders = $me.find("[data-action=myorders]");

            $orders.html("<div data-weave=\"widget/orders/mobile/orders/main()\"></div>");
            // Manually weave template
            weave.apply($orders.find("[data-weave]")).then(function() {});
        },
        // Render no login
        "hub/mine/nonLogin": function() {
            var me = this;
            var $me = me.$element;

            var $orders = $me.find("[data-action=myorders]");

            $orders.html('<div data-weave="widget/orders/mobile/nonlogin/main()"></div>');
            // Manually weave template
            weave.apply($orders.find("[data-weave]")).then(function() {});
        }
    });
});

define('troopjs-requirejs/template!widget/orders/mobile/nonlogin/index.html',[],function() { return function template(data) { var o = "<p data-weave=\"widget/blurb/main(1313)\">Sign in to your account</p>"; return o; }; });
define('widget/orders/mobile/nonlogin/main',["troopjs-browser/component/widget",
    "jquery",
    "widget/ajaxquery/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
], function(Widget, $, ajaxQuery, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                me.publish("enrollment/signin/popup");
            });
        },
        "dom:[data-action=signin]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            // Popup order-edit lightbox.
            me.publish("enrollment/signin/popup");
        }
    });
});

define('troopjs-requirejs/template!widget/orders/mobile/orders/index.html',[],function() { return function template(data) { var o = "";
    var EXPIRED_CODE = 5;

    var orders = data.orders;
    var pages = data.pages;
    var index = data.index;
o += "\r\n<p class=\"reminder\" data-weave=\"widget/blurb/main(1119)\">\r\n    You can cancel your order any time up to 24 hours before your scheduled delivery. If you seleced online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.\r\n</p>\r\n\r\n";
    var i = 0;
    var iLens = orders.length;
    for(i = 0; i < iLens; i++){
        renderOrder(orders[i]);
    }
o += "\r\n"; if(pages > 1){ o += "\r\n    <ul class=\"pagination\">\r\n        <li "; if(index <= 1) { o += "class=\"disabled\""; } o += ">\r\n            <a class=\"link-fn\" data-action=\"prevpage\">&laquo;</a>\r\n        </li>\r\n        ";
            var j = 1;
            var jLens = pages;
            for(j = 1; j <= jLens; j++) {
                renderPages(j, index);
            }
        o += "\r\n        <li "; if(index >= pages) { o += "class=\"disabled\""; } o += ">\r\n            <a class=\"link-fn\" data-action=\"nextpage\">&raquo;</a>\r\n        </li>\r\n    </ul>\r\n"; } o += "\r\n";
function renderPages(page, index) { 
    if(page !== index){
o += "\r\n        <li>\r\n            <a class=\"link-fn\" data-action=\"switchpage\" data-page=\"" +page+ "\">" +page+ "</a>\r\n        </li>\r\n"; } else { o += "\r\n        <li class=\"active\">\r\n            <a class=\"link-fn\">" +page+ " <span class=\"sr-only\">(current)</span></a>\r\n        </li>    \r\n"; }
} o += "\r\n\r\n"; function renderOrder(order) { o += "\r\n<table class=\"table table-bordered table-hover\">\r\n    <thead>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1098)\">Order number</span></td>\r\n        <td><a href=\"/account/orderdetail\" data-hash=\"orderid/" +order.Id+ "\" data-weave=\"widget/href/main\">" +order.Id+ "</a></td>\r\n    </tr>\r\n    </thead>\r\n    <tbody>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1099)\">Order date</span></td>\r\n        <td><span>" +order.OrderDate+ "</span></td>\r\n    </tr>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1100)\">Date of first delivery</span></td>\r\n        <td><span>" +order.FirstDeliveryDate+ "</span></td>\r\n    </tr>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1101)\">Total Amount (RMB)</span></td>\r\n        <td class=\"number-price\"><span>&yen;" +(order.PreferentialPrice + order.PreferentialLogisticsFee)+ "</span></td>\r\n    </tr>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1124)\">Payment Status</span></td>\r\n        <td><span class=\"text-status-" +getPayStatusById(order.IsPaid)+ "\" data-status=\"" +order.IsPaid+ "\">" +order.PayStatus+ "</span></td>\r\n    </tr>\r\n    <tr>\r\n        <td><span data-weave=\"widget/blurb/main(1102)\">Order Status</span></td>\r\n        <td><span class=\"text-status-" +getOrderStatusById(order.OrderStatusID)+ "\">" +order.OrderStatus+ "</span></td>\r\n    </tr>\r\n    <tr>\r\n        <td><a class=\"btn btn-default\" href=\"/account/orderdetail\" data-hash=\"orderid/" +order.Id+ "\" data-weave=\"widget/href/main widget/blurb/main(1472)\">查看订单</a></td>\r\n        <td>\r\n            "; if (!order.IsPaid && !(order.OrderStatusID === EXPIRED_CODE)) { o += "\r\n            <a class=\"btn btn-default\" data-id=\"" +order.Id+ "\" data-action=\"topay\" data-weave=\"widget/blurb/main(1221)\">Pay</a>\r\n            "; } o += "\r\n            "; if (order.CanCancel) { o += "\r\n            <a class=\"btn btn-default\" data-id=\"" +order.Id+ "\" data-action=\"deleteorder\" data-weave=\"widget/blurb/main(1219)\">Cancel</a>\r\n            "; } o += "\r\n        </td>\r\n    </tr>\r\n    </tbody>\r\n</table>\r\n"; } o += "\r\n\r\n";
    function getOrderStatusById(id){
        var status = "";

        switch(id){
            case 2: status = "completed";
                break;
            case 1: status = "inprocess";
                break;
            default:break;
        }

        return status;
    }
    function getPayStatusById(isPaid){
        var status = "";

        if(isPaid) {
            status = "completed";
        }
        else {
            status = "unpaid";
        }

        return status;
    }
 return o; }; });
define('widget/orders/mobile/orders/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders");
    var URI_ORDER_DELETE = api.get("order_delete");
    var PAGE_COUNT = 10;

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.index = 1;
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath) || {
                "page": 1
            };
            var index = hashMap.page || 1;

            // Apply to me
            me.index = parseInt(index, 10);

            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    startIndex: (me.index - 1) * PAGE_COUNT + 1,
                    count: PAGE_COUNT,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {

                me.html(template, {
                    orders: data.Orders,
                    pages: Math.ceil(data.Total / PAGE_COUNT),
                    index: me.index
                });

            });
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "page/" + index;
        },
        "dom:[data-action=topay]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            // URL redirect to payment page
            pageUrl.goto("/payment/topay", "orderid/" + orderId);
        },
        "dom:[data-action=deleteorder]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            if (confirm(blurb.get(1248) || "Sure to delete this order?")) {
                // Delete order by orderId
                // ...
                ajaxQuery({
                    url: URI_ORDER_DELETE,
                    data: {
                        orderId: orderId,
                        language: context.language
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $me.find("tr[data-id=" + orderId + "]").hide("slow", function() {
                        $(this).remove();
                    });
                });
            }
        }
    });
});
define('widget/pageverticalline/main',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            setTimeout(function() {
                var heightLeft = $me.prev().height();
                var heightRight = $me.next().height();
                var heightMe = ((heightLeft > heightRight) ? heightLeft : heightRight) + 20;
                $me.height(heightMe);
            }, 250);

        }
    });
});

define('troopjs-requirejs/template!widget/payment/payresult/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
    var signin = data.signin;
    var paySuccess = data.paySuccess;
    var orderId = data.orderId;
    var result = data.result || "";
    var hasEmail = data.hasEmail;
o += "\r\n<div class=\"success payresult-box\" style=\"overflow: hidden;\">\r\n    "; if (paySuccess) { o += "\r\n        <h3>\r\n            <span class=\"text-success glyphicon glyphicon-ok\"></span>\r\n            <strong data-weave=\"widget/blurb/main(1173)\">Congrats!</strong>\r\n            <span data-weave=\"widget/blurb/main(1224)\">Your order has been successfully placed.</span>\r\n        </h3>\r\n        <p>\r\n            <span data-weave=\"widget/blurb/main(1409)\">Order confirmation has been sent to your mobile via SMS, please check!</span>\r\n        </p>\r\n    "; } else { o += "\r\n        <h3>\r\n            <span class=\"text-danger glyphicon glyphicon-remove\"></span>\r\n            <strong data-weave=\"widget/blurb/main(1377)\">Sorry!</strong>\r\n            <span data-weave=\"widget/blurb/main(1376)\">Your payment failed.</span>\r\n            <span>" +result+ "</span>\r\n        </h3>\r\n    "; } o += "\r\n    <hr>\r\n    "; if (signin) { o += "\r\n        <div>\r\n        "; if (!hasEmail) { o += "\r\n            <form class=\"form-inline\" role=\"form\" data-action=\"signin-subscribe-email\">\r\n                <div class=\"form-group\">\r\n                    <input type=\"email\" class=\"form-control\" id=\"signup-email\" name=\"signup-email\" placeholder=\"Subscribe to VCLEANSE\" required value=\"\" data-weave=\"widget/blurb/main(1410, 'placeholder')\">\r\n                </div>\r\n                <button type=\"submit\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1403)\">Subscribe</button>\r\n            </form>\r\n            <div class=\"slide hid\" data-action=\"enrollmentsuccess-link\">\r\n                <p class=\"text-success\">\r\n                    <span class=\"glyphicon glyphicon-ok-sign\"></span>\r\n                    <span data-weave=\"widget/blurb/main(1405)\"></span>\r\n                </p>\r\n                "; renderSigninLink(); o += "\r\n            </div>\r\n        "; } else { renderSigninLink(); } o += "\r\n        </div>\r\n    "; } else { o += "\r\n        <div data-weave=\"widget/emailsubscribe/main()\"></div>\r\n        <br>\r\n        <div>\r\n            <p>\r\n                <i class=\"glyphicon glyphicon-chevron-right\"></i>\r\n                <a href=\"/order/search\" data-weave=\"widget/href/main\">\r\n                    <strong data-weave=\"widget/blurb/main(1175)\">Click here</strong>\r\n                </a>\r\n                <span data-weave=\"widget/blurb/main(1359)\">to search my orders</span>\r\n            </p>\r\n            <p>\r\n                <i class=\"glyphicon glyphicon-chevron-right\"></i>\r\n                <a href=\"/order\" data-weave=\"widget/href/main\">\r\n                    <strong data-weave=\"widget/blurb/main(1175)\">Click here</strong>\r\n                </a>\r\n                <span data-weave=\"widget/blurb/main(1223)\">to place a new order.</span>\r\n            </p>\r\n        </div>\r\n    "; } o += "\r\n    <div class=\"survey-container hid\" data-action=\"rendersurvey\" data-weave=\"widget/survey/main(surveyId)\" data-survey-id=\"1000\"></div>\r\n    <div class=\"payresult-share\" data-weave=\"widget/payment/share/main\"></div>\r\n    <div class=\"payresult-checklist\" data-weave=\"widget/checklist/main\"></div>\r\n</div>\r\n\r\n"; function renderSigninLink() { o += "\r\n    <p>\r\n        <i class=\"glyphicon glyphicon-chevron-right\"></i>\r\n        <a href=\"/account/orders\" data-weave=\"widget/href/main\">\r\n            <strong data-weave=\"widget/blurb/main(1175)\">Click here</strong>\r\n        </a>\r\n        <span data-weave=\"widget/blurb/main(1176)\">to see all of your orders</span>\r\n    </p>\r\n    <p>\r\n        <i class=\"glyphicon glyphicon-chevron-right\"></i>\r\n        <a href=\"/order\" data-weave=\"widget/href/main\">\r\n            <strong data-weave=\"widget/blurb/main(1175)\">Click here</strong>\r\n        </a>\r\n        <span data-weave=\"widget/blurb/main(1223)\">to place a new order.</span>\r\n    </p>\r\n"; }  return o; }; });
define('widget/payment/payresult/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/troophash/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./index.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, troopHash, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_EMAILCHECK = api.get("emailcheck");
    var URI_ACCOUNT_UPDATE = api.get("account_update");

    function getStatus(code) {
        var status = [];
        status["-1"] = "1406";

        return status[code] || "";
    }

    function getMessage(id) {
        var lan = context.language;
        var blurbDict = {
            "0": {
                "en": "",
                "cn": ""
            },
            "1": {
                "en": "Sign in",
                "cn": "Order price is error!"
            },
            "2": {
                "en": "Order has expired",
                "cn": "支付订单已经过期"
            },
            "3": {
                "en": "Order is not exists!",
                "cn": "支付订单不存在"
            },
            "4": {
                "en": "trade error",
                "cn": "trade失败"
            },
            "5": {
                "en": "Verify error",
                "cn": "验证失败"
            },
            "6": {
                "en": "No Return params!",
                "cn": "无返回参数"
            }
        };
        return (blurbDict[id] || "0")[lan];
    }

    return Widget.extend(function($element, widgetName, paySuccess) {
        var me = this;

        me.paySuccess = paySuccess;
        me.registerData = {};
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = parseInt(hashMap.orderid, 10);

            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }
            var result = hashMap.result;
            if (result) {
                result = getMessage(result);
            }

            // Signin?
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {

                    if (!data.FirstName) {
                        data.FirstName = "";
                    }
                    if (!data.LastName) {
                        data.LastName = "";
                    }
                    me.registerData = data;

                    // Signin
                    me.html(template, {
                        "cacheServer": context.cacheServer,
                        "signin": true,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId,
                        "result": result,
                        "hasEmail": data.Email ? true : false
                    });
                }, function() {
                    //
                    me.html(template, {
                        "cacheServer": context.cacheServer,
                        "signin": false,
                        "paySuccess": me.paySuccess,
                        "orderId": orderId,
                        "result": result
                    });
                });
            }, function() {
                //
                me.html(template, {
                    "cacheServer": context.cacheServer,
                    "signin": false,
                    "paySuccess": me.paySuccess,
                    "orderId": orderId,
                    "result": result
                });
            });
        },
        "hub:memory/order/checklist": function(order) {
            var me = this;
            var $me = me.$element;

            var $survey = $me.find("[data-action=rendersurvey]");
            if ($survey.length <= 0) {
                return;
            }

            var orderSourceCode = order.OrderSourceCode || "";
            if(!orderSourceCode){
                return;
            }

            if (orderSourceCode.toUpperCase() === "WARM") {
                $survey.show();
            }
        },
        "dom:form[data-action=signin-subscribe-email]/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var $form = $(e.currentTarget);
            var $successLink = $me.find("[data-action=enrollmentsuccess-link]");

            var email = _.trim($form.find("#signup-email").val());

            if (!regex.email(email)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1407
                }).open();

                return;
            }

            _.extend(me.registerData, {
                Email: email
            });

            ajaxQuery({
                url: URI_ACCOUNT_UPDATE,
                data: JSON.stringify(me.registerData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                $form.hide();
                $successLink.show().removeClass("out").addClass("in");
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});

define('troopjs-requirejs/template!widget/payment/share/index.html',[],function() { return function template(data) { var o = "";
    var domain = data.domain;
    var cacheServer = data.cacheServer;
	var orderSourceCode = data.orderSourceCode || "";
o += "\r\n"; if(orderSourceCode.toUpperCase() === "WARM") { o += "\r\n\t<div class=\"row\">\r\n\t\t<p class=\"col-md-12\" data-weave=\"widget/blurb/main(1466)\"></p>\r\n\t\t<div class=\"col-md-12 text-center\">\r\n\t    \t<img class=\"BSHARE_IMAGE\" data-tag=\"bshare_img\" src=\"" +cacheServer+ "/images/event/warmingday/card_01.jpg\" alt=\"今冬最fashion取暖方法！和我一起加入维果清12月16日暖体日吧~用美味健康的暖汤驱走冰冷\" width=\"100%\">\r\n\t    \t<img class=\"BSHARE_IMAGE hid\" data-tag=\"bshare_img\" src=\"" +cacheServer+ "/images/event/warmingday/card_02.jpg\" alt=\"今冬最fashion取暖方法！和我一起加入维果清12月16日暖体日吧~用美味健康的暖汤驱走冰冷\" width=\"100%\">\r\n\t    </div>\r\n\t</div>\r\n\t<script>\r\n\twindow._bd_share_config = {\r\n\t\t\"common\": {\r\n\t\t\t\"bdSnsKey\": {},\r\n\t\t\t\"bdText\": \"今冬最fashion取暖方法！和我一起加入维果清12月16日暖体日吧~用美味健康的暖汤驱走冰冷~购买链接：http://www.vcleanse.com/warmingday?referer=share&share=yes\",\r\n\t\t\t\"bdMini\": \"2\",\r\n\t\t\t\"bdMiniList\": false,\r\n\t\t\t\"bdUrl\": \"http://www.vcleanse.com/warmingday?referer=share&share=yes\",\r\n\t\t\t\"bdPic\": \"http://www.vcleanse.com/images/event/warmingday/card_01.jpg||http://www.vcleanse.com/images/event/warmingday/card_02.jpg\",\r\n\t\t\t\"bdStyle\": \"0\",\r\n\t\t\t\"bdSize\": \"16\"\r\n\t\t},\r\n\t\t\"image\": {\r\n\t\t\t\"tag\" : \"bshare_img\",\r\n\t\t\t\"viewList\": [\"tsina\", \"weixin\"],\r\n\t\t\t\"viewText\": \"分享到：\",\r\n\t\t\t\"viewSize\": \"16\"\r\n\t\t}\r\n\t};\r\n\t</script>\r\n"; } o += "\r\n\r\n<script>\r\nwith(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];\r\n</script>"; return o; }; });
define('widget/payment/share/main',["troopjs-browser/component/widget",
	"jquery",
    "context",
	"template!./index.html"
], function(Widget, $, context, template) {
	"use strict";

	return Widget.extend({
		"hub:memory/order/checklist": function(order) {
			var me = this;
			var $me = me.$element;

			var orderSourceCode = order.OrderSourceCode || "";

			if(!orderSourceCode){
				return;
			}

			// Rendering
			me.html(template, {
	            "domain": context.domain,
	            "cacheServer": context.cacheServer,
				"orderSourceCode": orderSourceCode
			});
		}
	});
});

define('troopjs-requirejs/template!widget/payment/topay/index.html',[],function() { return function template(data) { var o = "";
  var cacheServer = data.cacheServer;
  var orderId = data.orderId;
  var hasOnlinePay = data.hasOnlinePay;
  var onlinePayment = data.onlinePayment;

  var url_qr = cacheServer+"/payment/pay?orderid="+ orderId + "&paymentplatform=1&bank=&qr=0";
o += "\r\n"; if (hasOnlinePay) { o += "\r\n<div class=\"topay confirm-box\">\r\n    <h3>\r\n        <strong data-weave=\"widget/blurb/main(1108)\">Pay online</strong>\r\n    </h3>\r\n    <p>\r\n        <span class=\"payment-highlight\" data-weave=\"widget/blurb/main(1141)\">Order successful submit, payment as soon as possible please!</span>\r\n        <br>\r\n        <span data-weave=\"widget/blurb/main(1061)\" data-action=\"orderid\" data-id=\"" +orderId+ "\">Order number:</span>\r\n        <strong>\r\n            " +orderId+ "\r\n        </strong>\r\n        <br>\r\n        <span data-weave=\"widget/blurb/main(1142)\">Amount Due (RMB):</span>\r\n        <strong>&yen;\r\n            " +onlinePayment+ "\r\n        </strong>\r\n        <br>\r\n        <span data-weave=\"widget/blurb/main(1143)\" data-format=\"" +onlinePayment+ "\">Please complete payment within 24 hours. Orders will be cancelled after 24 hours with no payment.</span>\r\n    </p>\r\n    <hr>\r\n    <h6>\r\n        <strong data-weave=\"widget/blurb/main(1109)\">Finish your payment by click following payment platform!</strong>\r\n    </h6>\r\n    <p class=\"text-warning\">\r\n        <small>\r\n          <b>支付宝快捷支付（含信用卡、储蓄卡）</b>已开通大额支付。<a href=\"https://cshall.alipay.com/lab/help_detail.htm?help_id=245345\" target=\"_blank\">支付限额详见&gt;&gt;</a>\r\n        </small>\r\n    </p>\r\n    <ul class=\"hor banklist\">\r\n      <li data-action=\"selectbank\" data-id=\"ALIPAY\">\r\n          <img src=\"" +cacheServer+ "/images/payment/alipay.jpg\" width=\"126\" height=\"35\" alt=\"ALIPAY\">\r\n      </li>\r\n      <li data-action=\"selectbank\" data-id=\"UCFPAY\">\r\n          <img src=\"" +cacheServer+ "/images/payment/ucfpay.jpg\" width=\"126\" height=\"35\" alt=\"UCFPAY\">\r\n      </li>\r\n    </ul>\r\n    <h6><strong data-weave=\"widget/blurb/main(p1001)\">扫码支付：</strong></h6>\r\n    <p>\r\n      <small data-weave=\"widget/blurb/main(p1003)\">使用支付宝钱包扫一扫即可付款</small>\r\n    </p>\r\n    <iframe class=\"payment-qrmap\" src=\"" +url_qr+ "\" frameborder=\"0\" align=\"middle\" width=\"600\" height=\"272\" class=\"map-container\"></iframe>\r\n    <h6><strong data-weave=\"widget/blurb/main(p1002)\">网银支付：</strong></h6>\r\n    <p>\r\n      <small data-weave=\"widget/blurb/main(p1005)\">Attention: you cannot choose a new payment method once you have committed to one. If you still want to change, please cancel you order and place a new one!</small>\r\n    </p>\r\n    <ul class=\"hor banklist\">\r\n        <li data-action=\"selectbank\" data-id=\"CMB\">\r\n            <label class=\"bank-cmbchina\"></label>\r\n        </li>\r\n        <li data-action=\"selectbank\" data-id=\"BOCB2C\">\r\n            <label class=\"bank-boc\"></label>\r\n        </li>\r\n        <li data-action=\"selectbank\" data-id=\"ICBCB2C\">\r\n            <label class=\"bank-icbc\"></label>\r\n        </li>\r\n        <li data-action=\"selectbank\" data-id=\"COMM\">\r\n            <label class=\"bank-bankcomm\"></label>\r\n        </li>\r\n        <li data-action=\"selectbank\" data-id=\"CCB\">\r\n            <label class=\"bank-ccb\"></label>\r\n        </li>\r\n        <li data-action=\"selectbank\" data-id=\"CIB\">\r\n            <label class=\"bank-cib\"></label>\r\n        </li>\r\n    </ul>\r\n    <div id=\"banklist-more\" class=\"banklist collapse\">\r\n      <ul class=\"hor banklist banklist-more\">\r\n          <li data-action=\"selectbank\" data-id=\"CMBC\">\r\n              <label class=\"bank-cmbc\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"GDB\">\r\n              <label class=\"bank-cgbchina\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"SPDB\">\r\n              <label class=\"bank-spdb\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"ABC\">\r\n              <label class=\"bank-abchina\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"SPABANK\">\r\n              <label class=\"bank-pingan\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"POSTGC\">\r\n              <label class=\"bank-psbc\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"CITIC\">\r\n              <label class=\"bank-ecitic\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"SHRCB\">\r\n              <label class=\"bank-srcb\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"SHBANK\">\r\n              <label class=\"bank-bankofshanghai\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"NBBANK\">\r\n              <label class=\"bank-nbcb\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"HZCBB2C\">\r\n              <label class=\"bank-hccb\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"BJBANK\">\r\n              <label class=\"bank-bankofbeijing\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"BJRCB\">\r\n              <label class=\"bank-bjrcb\"></label>\r\n          </li>\r\n          <li data-action=\"selectbank\" data-id=\"WZCBB2C-DEBIT\">\r\n              <label class=\"bank-wzcb\"></label>\r\n          </li>\r\n      </ul>\r\n    </div>\r\n    <div class=\"text-right\">\r\n      <a class=\"link-fn payment-showmorebank\" data-toggle=\"collapse\" data-action=\"collapsebanks\" data-target=\"#banklist-more\">\r\n        <span data-weave=\"widget/blurb/main(1096)\">See more</span>\r\n        <span class=\"glyphicon glyphicon-chevron-down\"></span>\r\n      </a>\r\n    </div>\r\n    <div class=\"text-center paymentaction-container\">\r\n        <span data-weave=\"widget/blurb/main(p1004)\" data-action=\"confirmpayment-tip\" class=\"in\">Select payment method</span>\r\n        <button class=\"btn btn-lg btn-default btn-action\" data-action=\"confirmpayment\" data-weave=\"widget/blurb/main(1458)\">Pay</button>\r\n    </div>\r\n</div>\r\n"; } else { o += "\r\n<div class=\"success payresult-box\" style=\"overflow: hidden;\">\r\n    <h3>\r\n        <span class=\"text-danger glyphicon glyphicon-remove\"></span>\r\n        <strong data-weave=\"widget/blurb/main(1377)\">Sorry!</strong>\r\n        <span data-weave=\"widget/blurb/main(1481)\">Invalid payment request!</span>\r\n    </h3>\r\n    <hr>\r\n    <div class=\"payresult-checklist\" data-weave=\"widget/checklist/main\"></div>\r\n</div>\r\n"; }  return o; }; });

define('troopjs-requirejs/template!widget/payment/topay/result.html',[],function() { return function template(data) { var o = "";
    var signin = data.signin||false;
o += "\r\n<div class=\"modal-dialog payment-result\">\r\n    <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <h4 class=\"modal-title\" data-weave=\"widget/blurb/main(1456)\">Comfirm</h4>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            "; if (signin) { o += "\r\n                <a href=\"/account/orders\" class=\"btn btn-link btn-lg\" data-weave=\"widget/href/main\">\r\n                    <span data-weave=\"widget/blurb/main(1454)\">Finish Payment</span>\r\n                </a>\r\n            "; } else { o += "\r\n                <a href=\"/\" class=\"btn btn-link btn-lg\" data-weave=\"widget/href/main\">\r\n                    <span data-weave=\"widget/blurb/main(1454)\">Finish Payment</span>\r\n                </a>\r\n            "; } o += "\r\n            <button class=\"btn btn-link btn-lg\" data-weave=\"widget/blurb/main(1455)\" data-action=\"close\" data-dismiss=\"modal\">Issue with payment</button>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-default\" data-action=\"close\" data-dismiss=\"modal\" data-weave=\"widget/blurb/main(1457)\">Close</button>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/payment/topay/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/cookie/main",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/popupbox/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!./result.html",
    "json2"
], function(Widget, $, when, context, cookie, ajaxQuery, api, pageUrl, PopupBox, troopHash, template, templateResult) {
    "use strict";
    var URI_ACCOUNT = api.get("account");
    var URI_ORDERDETAIL = api.get("orderdetail");

    var PLATFORM_ALI = 1;
    var PLATFORM_UCF = 2;
    var PLATFORM_SELFPICK = 3;
    var PLATFORM_ALIWAP = 4;
    var PLATFORM_UNION = 5;

    //
    var QRTYPE_NA = -1;
    var QRTYPE_SIMPREPOSE = 0; //
    var QRTYPE_PREPOSE = 1; //iframe url
    var QRTYPE_SKIP = 2;
    var QRTYPE_MINI = 3;

    function getPaymentOnline(payments) {
        if (!payments) {
            return -1;
        }

        for (var i = 0, iLens = payments.length; i < iLens; i++) {
            var payment = payments[i];

            if (payment.IsOnlinePay && !payment.IsPaid) {
                return payment.Payment;
            }
        }

        return -1;
    }

    return Widget.extend(function() {
        var me = this;
        me.bank = "";
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path;
            if (!hashPath) {
                pageUrl.goto("/");
                return;
            }
            var hashMap = troopHash.parse(hashPath);
            var orderId = me.OrderId = parseInt(hashMap.orderid, 10);
            // orderId is required
            // or will direct to root page
            if (!orderId) {
                pageUrl.goto("/");
                return;
            }

            var postData = {
                "language": context.language,
                "orderid": orderId
            };

            ajaxQuery({
                url: URI_ORDERDETAIL,
                data: postData,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var dataOrder = data.Order;

                var onlinePayment = getPaymentOnline(dataOrder.OrderPayments);

                me.html(template, {
                    "cacheServer": context.cacheServer,
                    "orderId": dataOrder.OrderID,
                    "hasOnlinePay": onlinePayment >= 0 ? true : false,
                    "onlinePayment": onlinePayment
                });
            });
        },
        "renderModel": function(isSignin) {
            var me = this;

            var $templateResult = $(templateResult({
                "signin": isSignin
            }));
            // Lightbox configuration
            var popupboxResult = new PopupBox({
                msg: $templateResult,
                closeble: true,
                closeInner: true,
                zIndex: 1050,
                closeButtonList: ['[data-action=close]'],
                closeCallback: function() {}
            });

            popupboxResult.open();
        },
        "showModel": function() {
            var me = this;

            // Signin?
            cookie.get("memberid").then(function() {
                // Get User Account
                ajaxQuery({
                    url: URI_ACCOUNT,
                    type: "GET",
                    dataType: "json"
                }).then(function(data) {
                    // Signin
                    me.renderModel(true);
                }, function() {
                    //
                    me.renderModel(false);
                });
            }, function() {
                //
                me.renderModel(false);
            });
        },
        "dom:[data-action=confirmpayment]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            //var OrderId=$me.find("[data-action=orderid]").data("id");

            var paymentplatform = "";
            var bankId = "";
            var qr = QRTYPE_NA;

            if (!me.bank) {
                // Show payment bank select hint
                $tip.show().addClass("pop");
                setTimeout(function() {
                    $tip.removeClass("pop");
                }, 1000);
                return;
            }

            if (me.bank === "ALIPAY") {
                paymentplatform = PLATFORM_ALI;
            } else if (me.bank === "UCFPAY") {
                paymentplatform = PLATFORM_UCF;
            } else if (me.bank === "ALIPAY_QR") {
                paymentplatform = PLATFORM_ALI;
                qr = QRTYPE_SKIP;
            } else {
                paymentplatform = PLATFORM_ALI;
                bankId = me.bank;
            }

            // Go to bank window
            pageUrl.winopen("/payment/pay", null, {
                "orderid": me.OrderId,
                "paymentplatform": paymentplatform,
                "bank": bankId,
                "qr": qr
            });
            // Show payment result choice popup
            me.showModel();
        },
        "dom:[data-action=selectbank]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $el = $(e.currentTarget);
            var $li = $me.find("ul.banklist > li");
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            // Select bank - UI
            $li.removeClass("active");
            $el.addClass("active");
            // Save selected bank
            me.bank = $el.data("id");
            // Remove payment bank select hint
            $tip.hide();
        },
        "dom:[data-action=collapsebanks]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var cssClass_down = "glyphicon-chevron-down";
            var cssClass_up = "glyphicon-chevron-up";

            var $el = $(e.currentTarget);
            var $elGlyphIcon = $el.find(".glyphicon");
            var $liMore = $("ul.banklist-more > li");
            var $tip = $me.find("[data-action=confirmpayment-tip]");

            if ($elGlyphIcon.hasClass(cssClass_down)) {
                // Extend
                $elGlyphIcon.removeClass(cssClass_down);
                $elGlyphIcon.addClass(cssClass_up);
            } else {
                // Shrink
                $elGlyphIcon.removeClass(cssClass_up);
                $elGlyphIcon.addClass(cssClass_down);
                // Update bank selection
                if ($liMore.filter(".active").length > 0) {
                    me.bank = "";
                    $liMore.removeClass("active");
                    // Show payment bank select hint
                    $tip.show();
                }
            }
        }
    });
});
define('widget/popupmydescription/listener/main',["troopjs-browser/component/widget",
  "jquery",
  "widget/popupbox/main"
], function(Widget, $, PopupBox) {
  "use strict";

  return Widget.extend({
    "hub:memory/popupMyDescription/popup": function(for_el_id) {
      var me = this;

      var $me = me.$element;

      if ($me[0].id !== for_el_id) {
        return;
      }

      var block = new PopupBox({
        msg: $me.clone(),
        closeble: true,
        closeInner: true,
        zIndex: 10
      });
      block.open();
    }
  });
});
define('widget/popupmydescription/main',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    return Widget.extend({
        "dom/click": function() {
            var me = this;

            var obj_ID = me.$element.attr("data-for");
            me.publish("popupMyDescription/popup", obj_ID);
        }
    });
});

define('troopjs-requirejs/template!widget/search/index.html',[],function() { return function template(data) { var o = "<form>\r\n\t<h2 class=\"txtalign-c\" data-weave=\"widget/blurb/main(1350)\">Find My Orders</h2>\r\n\t<div class=\"cf\">\r\n\t    <div class=\"the-form\">\r\n\t        <div>\r\n\t        \t<input type=\"text\" class=\"input-search\" data-action=\"searchinput\" required>\r\n\t        \t<button type=\"submit\" data-action=\"search\" data-weave=\"widget/blurb/main(1353)\">SEARCH</button>\r\n\t        </div>\r\n\t        <div class=\"hint clear\" data-weave=\"widget/blurb/main(1354)\">Receiver mobile</div>\r\n\t    </div>\r\n\t</div>\r\n</form>"; return o; }; });
define('widget/search/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/basic/regex",
    "widget/infobox/main",
    "widget/pageurl/main",
    "template!./index.html"
], function(Widget, $, context, regex, InfoBox, pageUrl, template) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            me.html(template, {
                cacheServer: context.cacheServer,
                serviceDomain: context.serviceDomain
            });
        },
        "dom:[data-action=search]/click": function(e) {
            e.preventDefault();

            var $input = $("input[data-action=searchinput]");
            var mobile = $input.val();

            // Validate mobile
            if (!regex.mobile(mobile)) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1408
                }).open();

                return;
            }

            // Redirect to search page
            pageUrl.goto("/order/orders",
                "mobile/" + mobile);
        }
    });
});

define('troopjs-requirejs/template!widget/search/orders/index.html',[],function() { return function template(data) { var o = "";
    var EXPIRED_CODE = 5;

    var cacheServer = data.cacheServer;
    var serviceDomain = data.serviceDomain;
    var verified = data.verified;
    var mobile = data.mobile;
o += "\r\n\r\n"; if (!verified) { o += "\r\n    <form role=\"form\" class=\"search-verification\">\r\n        <div class=\"form-section\">\r\n            <div class=\"form-group\">\r\n                <label for=\"search-verificationcode\" data-weave=\"widget/blurb/main(1375)\">To protect your privacy, we need to verify the phone number.</label>\r\n                <div class=\"form-group imageverificationcode cf\">\r\n                    <label for=\"sendsms-image-verificationcode\" data-weave=\"widget/blurb/main(1614)\">Verification code:</label>\r\n                    <img src=\"" +serviceDomain+ "/common/image\" width=\"80\" height=\"31\" alt=\"Verification code\" data-action=\"refreshimagesecurity\">\r\n                    <input type=\"text\" class=\"form-control\" id=\"sendsms-image-verificationcode\" data-action=\"checkverificationcode\" name=\"sendsms-image-verificationcode\" placeholder=\"Please type the verification code.\" required data-weave=\"widget/blurb/main(1344, 'placeholder')\">\r\n                </div>\r\n                <p>\r\n                    <button data-action=\"send-verificationcode\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1084)\">SEND VERIFICATION CODE</button> <span data-weave=\"widget/blurb/main(1616)\">to：</span>" +mobile+ "\r\n                </p>\r\n                <div class=\"input-group\">\r\n                    <input type=\"text\" class=\"form-control\" id=\"search-verificationcode\" name=\"search-verificationcode\" placeholder=\"Input verification code sent to your mobile phone\" required focus data-weave=\"widget/blurb/main(1139, 'placeholder')\">\r\n                    <span class=\"input-group-btn sendVerificationAgain\">\r\n                        <button type=\"submit\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1615)\">VERIFY AND SEARCH</button>\r\n                        <span class=\"sending hid input-group-btn\">\r\n                            <img src=\"" +cacheServer+ "/images/waiting.gif\" width=\"32\" height=\"32\" alt=\"\">\r\n                            <span data-weave=\"widget/blurb/main(1341)\">Sending...</span>\r\n                            <span data-action=\"countdown\"></span>\r\n                        </span>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <input type=\"hidden\" id=\"blurb-1345\" data-weave=\"widget/blurb/main(1345, 'value')\" value=\"Please type the verification code.\">\r\n        <input type=\"hidden\" id=\"blurb-1139\" data-weave=\"widget/blurb/main(1139, 'value')\" value=\"Input verification code sent to your mobile phone.\">\r\n    </form>\r\n"; } else { o += "\r\n    <p class=\"reminder\" data-weave=\"widget/blurb/main(1119)\">\r\n        You can cancel your order any time up to 24 hours before your scheduled delivery. If you seleced online payment, please note that you will need to complete payment 24 hours before your start date to confirm your order.\r\n    </p>\r\n    <div data-weave=\"widget/orders/list/main\"></div>\r\n"; }  return o; }; });
define('widget/search/orders/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/blurb/main",
    "widget/infobox/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "template!./index.html",
    "jquery.validation",
    "underscore.string"
], function(Widget, $, context, ajaxQuery, api, pageUrl, Blurb, InfoBox, cookie, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // OrderStatusID
    var URI_ORDERS = api.get("orders_bymobile");
    var URI_SENDVERIFICATIONCODE = api.get("sendverificationcode");

    var PAGE_COUNT = 30;

    function getStatus(code) {
        var status = [];
        status["1003"] = "1362"; //手机号错误 or 没有订单
        status["1"] = "1187";
        status["10"] = "1181";
        status["11"] = "1182";
        status["12"] = "1183";
        status["13"] = "1184";
        status["-1"] = "1185";
        status["-2"] = "1186";
        status["-3"] = "1188";
        status["-4"] = "1189";
        status["-5"] = "1190";
        status["-6"] = "1191";
        status["-7"] = "1274";

        return status[code] || "";
    }

    function refreshSecurity($el) {
        var src = context.serviceDomain + "/common/image?t=" + (new Date()).getTime();
        $el.attr("src", src);
    }

    return Widget.extend(function($element, widgetName, pageCount) {
        var me = this;

        me.index = 1;
        me.pageCount = pageCount || PAGE_COUNT;
    }, {
        "validation": function() {
            var me = this;
            var $me = me.$element;

            var $form = $me.find("form");

            var m1345 = $form.find("#blurb-1345").val();
            var m1139 = $form.find("#blurb-1139").val();

            $form.validate({
                rules: {
                    "sendsms-image-verificationcode": "required",
                    "search-verificationcode": "required"
                },
                messages: {
                    "sendsms-image-verificationcode": m1345,
                    "search-verificationcode": m1139
                },
                submitHandler: function(form) {
                    var code = _.trim($form.find("#search-verificationcode").val());
                    if (!code) {
                        return;
                    }

                    me.renderResult(code);
                }
            });
        },
        "renderResult": function(code) {
            var me = this;
            var $me = me.$element;

            // Ajax Query
            ajaxQuery({
                url: URI_ORDERS,
                data: {
                    "mobile": me.mobile,
                    "startIndex": (me.index - 1) * PAGE_COUNT + 1,
                    "count": PAGE_COUNT,
                    "ValidationCode": code,
                    "language": context.language
                },
                type: "GET",
                dataType: "json"
            }).then(function(data) {
                if (!code) {
                    // Publish
                    me.publish("orders/list", {
                        "orders": data.Orders,
                        "disabled": true,
                        "urlDetail": "/order/orderdetail",
                        "pages": Math.ceil(data.Total / me.pageCount),
                        "index": me.index
                    }).then(function() {
                        // Set search mobile to cookie
                        cookie.set("search", me.mobile);
                    });
                } else {
                    me.html(template, {
                        cacheServer: context.cacheServer,
                        serviceDomain: context.serviceDomain,
                        mobile: me.mobile,
                        verified: true
                    }).then(function() {
                        me.publish("orders/list", {
                            "orders": data.Orders,
                            "disabled": true,
                            "urlDetail": "/order/orderdetail",
                            "pages": Math.ceil(data.Total / me.pageCount),
                            "index": me.index
                        }).then(function() {
                            // Set search mobile to cookie
                            cookie.set("search", me.mobile);
                        });
                    });
                }

            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        },
        "sendVerificationCode": function() {
            var me = this;
            var $me = me.$element;

            var $btnSend = $me.find("[data-action=send-verificationcode]");
            var $sendInfo = $btnSend.parent("p");
            var $sending = $me.find(".sending");
            var $countdown = $me.find("[data-action=countdown]");

            var $imageVerification = $(".imageverificationcode");
            var $verificationCode = $me.find("#sendsms-image-verificationcode");
            if ($verificationCode.valid()) {
                var verificationCode = _.trim($verificationCode.val());
                ajaxQuery({
                    url: URI_SENDVERIFICATIONCODE,
                    data: {
                        "mobile": me.mobile,
                        language: context.language,
                        verifyCode: verificationCode
                    },
                    type: "POST",
                    dataType: "json"
                }).then(function(data) {
                    $("#search-verificationcode").focus();
                    $sendInfo.hide();
                    $imageVerification.hide();
                    $sending.show();
                    var countdownMax = 60;

                    (function countdownSending() {
                        if (countdownMax <= 0) {
                            clearTimeout(insTimeout);

                            $countdown.text("");
                            $sending.hide();
                            refreshSecurity($("[data-action=refreshimagesecurity]"));
                            $imageVerification.show();
                            $sendInfo.show();
                            $btnSend.text(blurb.get("1342"));

                            return;
                        }
                        var insTimeout = setTimeout(function() {
                            $countdown.text(countdownMax);
                            countdownSending();
                        }, 1000);
                        countdownMax--;
                    })();
                }, function(e) {
                    // Show error msg as a lightbox
                    new InfoBox({
                        title: 1144,
                        content: getStatus(e)
                    }).open();
                });
            }
        },
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;
            var hashPath = uriPath.path;
            var mobile;
            var index;
            if (!hashPath || hashPath.length < 2) {
                return;
            }

            var hashMap = troopHash.parse(hashPath);

            mobile = hashMap.mobile;
            index = hashMap.page || 1;

            if (!mobile) {
                return;
            }
            // Store in memory
            me.mobile = mobile;
            // Apply to me
            me.index = parseInt(index, 10);

            function renderVerification() {
                me.html(template, {
                    cacheServer: context.cacheServer,
                    serviceDomain: context.serviceDomain,
                    mobile: mobile,
                    verified: false
                }).then(function() {
                    me.validation();
                });
            }

            // Get account from cookie
            cookie.get("search").then(function(mobile) {
                if (me.mobile === mobile) {
                    // Session Cookie, don't verify
                    me.html(template, {
                        cacheServer: context.cacheServer,
                        serviceDomain: context.serviceDomain,
                        mobile: mobile,
                        verified: true
                    }).then(function() {
                        me.renderResult();
                    });
                } else {
                    renderVerification();
                }
            }, function() {
                renderVerification();
            });
        },
        "dom:[data-action=send-verificationcode]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            // Return while hidden
            if (!$(e.currentTarget).is(":visible")) {
                return;
            }

            me.sendVerificationCode();
        },
        "dom:form/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;
            var $form = $(e.currentTarget);

            var code = _.trim($form.find("#search-verificationcode").val());

            if (!code) {
                return;
            }

            me.renderResult(code);
        },
        "dom:[data-action=prevpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index - 1;
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=nextpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var index = me.index + 1;
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=switchpage]/click": function(e) {
            e.preventDefault();
            var me = this;

            var page = $(e.currentTarget).data("page");
            var index = parseInt(page, 10);
            location.hash = "mobile/" + me.mobile + "/page/" + index;
        },
        "dom:[data-action=topay]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);
            var orderId = $e.data("id");

            // URL redirect to payment page
            pageUrl.goto("/payment/topay", "orderid/" + orderId);
        },
        "dom:[data-action=refreshimagesecurity]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $el = $(e.currentTarget);

            refreshSecurity($el);
        }
    });
});

define('widget/select/main',["troopjs-browser/component/widget",
  "jquery",
  "underscore"
], function(Widget, $) {
  "use strict";

  return Widget.extend({
    "copyName": function(srcKey) {
      var me = this;

      if (!me.options || me.options.length <= 0) {
        return;
      }

      // Copy
      _.each(me.options, function(el, i) {
        el.Name = el[srcKey];
      });
    },
    "reset": function(selectedId) {
      var me = this;
      var $me = me.$element;

      if(!selectedId){
        return;
      }

      // Get selected option(object)
      var selectedOption = null;
      if (me.options && me.options.length > 0) {
        _.each(me.options, function(el, i) {
          if (el.Id === selectedId) {
            selectedOption = el;
            return;
          }
        });
      }

      // Set value to parent element
      // Trigger parent element customize "change" event
      $me
        .data("value", selectedId)
        .trigger("change", selectedOption);
    },
    "dom:[data-action=changeoption]/change": function(e) {
      e.preventDefault();

      var me = this;
      var $me = me.$element;

      var val = e.currentTarget.value;

      // Get selected option(object)
      var selectedOption = null;
      if (me.options && me.options.length > 0) {
        _.each(me.options, function(el, i) {
          if (el.Id.toString() === val) {
            selectedOption = el;
            return;
          }
        });
      }

      // Set value to parent element
      // Trigger parent element customize "change" event
      $me
        .data("value", val)
        .trigger("change", selectedOption);
    }
  });

});

define('troopjs-requirejs/template!widget/select/index.html',[],function() { return function template(data) { var o = "";
    var arrData = data.options || [];
    var selectedId = data.selectedId;
    var disabled = data.disabled;
o += "\r\n\r\n<select class=\"form-control\" data-action=\"changeoption\" " +(disabled ? "disabled" : "")+ ">\r\n    "; if ((!selectedId && arrData.length > 0 && arrData[0].Id !== 0) || arrData.length <= 0) { o += "\r\n        <option value=\"0\">请选择</option>\r\n    "; } o += "\r\n    ";
        var i = 0;
        var iLens = arrData.length;
        for (i=0; i < iLens; i++) {
    o += "\r\n            <option value=\"" +arrData[i].Id+ "\" "; if (selectedId === arrData[i].Id) { o += "selected"; } o += ">" +arrData[i].Name+ "</option>  \r\n    ";
        }
    o += "\r\n</select>"; return o; }; });
define('widget/select/receiver/main',["widget/select/main",
  "jquery",
  "context",
  "widget/api/main",
  "widget/blurb/main",
  "widget/ajaxquery/main",
  "widget/infobox/main",
  "template!../index.html",
  "json2",
  "underscore"
], function(Select, $, context, api, Blurb, ajaxQuery, InfoBox, template) {
  "use strict";

  var URI_RECEIVER = api.get("receiver");
  var blurb = new Blurb($(document.body));

  function getAddressString(address) {
    if (!address) {
      return "";
    }
    var strAddress = "";

    if (context.language === "en") {
      if (address.FirstName) {
        strAddress += address.FirstName;
      }
      if (address.LastName) {
        strAddress += " " + address.LastName;
      }
      strAddress += ",";
    } else {
      if (address.LastName) {
        strAddress += address.LastName;
      }
      if (address.FirstName) {
        strAddress += " " + address.FirstName;
      }
      strAddress += "，";
    }

    strAddress += " " + address.CityName;
    strAddress += " " + address.AreaName;
    if (address.Line1) {
      strAddress += " " + address.Line1;
    }
    if (address.Line2) {
      strAddress += " " + address.Line2;
    }
    // Deliverble
    if(!address.IsActive){
      strAddress += " (" + blurb.get(1469) + ")";
    }

    return strAddress;
  }

  return Select.extend(function($element, widgetName, receiverId, disabled) {
    var me = this;

    me.receiverId = parseInt(receiverId, 10);
    me.disabled = disabled;
  }, {
    "sig/start": function() {
      var me = this;

      ajaxQuery({
        url: URI_RECEIVER,
        data: {
          "t": (new Date()).getTime()
        },
        type: "GET",
        dataType: "json"
      }).then(function(data) {

        var defaultReceiver;

        // With/Without receiver
        if (data.Receivers && data.Receivers.length > 0) {
          _.each(data.Receivers, function(receiver) {
            // Set "Name"
            receiver.Name = getAddressString(receiver);
            // Find default receiver
            if (me.receiverId) {
              if (receiver.Id === me.receiverId) {
                defaultReceiver = receiver;
              }
            } else if (receiver.IsDefault) {
              // Set receiverId
              me.receiverId = receiver.Id;
              defaultReceiver = receiver;
            }
          });

          // Store options for trigger event
          me.options = data.Receivers;

          // Rendering
          me.html(template, {
            "options": me.options,
            "selectedId": me.receiverId,
            "disabled": me.disabled
          }).then(function() {
            // Init default selected option
            me.reset(me.receiverId);
          });
        }

        me.publish("receiver/default", defaultReceiver);

      });
    }
  });
});
define('widget/select/region/helper',["jquery"], function($) {
    "use strict";

    return {
        "getCityByCityId": function(provinces, cityId) {
            var i = 0;
            var iLens = provinces.length;
            var j = 0;
            var jLens = 0;
            var cities = {};
            var province = {};

            for (i; i < iLens; i++) {
                province = provinces[i];
                cities = province.Cities;
                j = 0;
                jLens = cities.length;
                for (j; j < jLens; j++) {
                    if (cities[j].Id === cityId) {
                        return cities;
                    }
                }
            }
            return {};
        },
        "getDistrictsByCityId": function(city, cityId) {
            var i = 0;
            var iLens = city.length;
            for (i; i < iLens; i++) {
                if (city[i].Id === cityId) {
                    return city[i].District;
                }
            }
        },
        "getCitiesByProvinceId": function(provinces, provinceId) {
            var i = 0;
            var iLens = provinces.length;
            for (i; i < iLens; i++) {
                if (provinces[i].Id === provinceId) {
                    return provinces[i].Cities;
                }
            }
        },
         "getProvinceIdByCityId": function(provinces, cityId) {
            var i = 0;
            var iLens = provinces.length;
            var j = 0;
            var jLens = 0;
            var cities = {};
            var province = {};

            for (i; i < iLens; i++) {
                province = provinces[i];
                cities = province.Cities;
                j = 0;
                jLens = cities.length;
                for (j; j < jLens; j++) {
                    if (cities[j].Id === cityId) {
                        return province.Id;
                    }
                }
            }
            return 0;
        },
    };
});
define('widget/select/region/citypicker/main',["widget/select/main",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/select/region/helper",
    "template!../../index.html"
], function(Select, $, context, ajaxQuery, api, helper, template) {
    "use strict";


    var INSTANCE_ID = 0;

    return Select.extend(function($element, widgetName, cityId, disabled) {
        var me = this;

        INSTANCE_ID++;
        me.instance_id = INSTANCE_ID;
        me.cityId = parseInt(cityId, 10);
        me.disabled = disabled;
    }, {
        "hub:memory/region/city/reload": function(msg) {
            var me = this;
            var $me = me.$element;

            if (msg.id === me.instance_id) {

                if (msg.data && msg.data.length > 0) {
                    // Option group changed
                    if (!(_.find(msg.data, function(e, i) {
                            return e.Id === me.cityId;
                        }))) {
                        // Do not select a default area, force to select one
                        me.cityId = 0; //msg.data[0].Id;
                    }
                }
                $me.data("value", me.cityId);

                // Store options for trigger event
                me.options = msg.data;

                // Rendering
                me.html(template, {
                    "options": me.options,
                    "selectedId": me.cityId,
                    "disabled": me.disabled
                }).then(function() {
                    // Trigger event, apply value to parent
                    $me.find("[data-action=changeoption]").trigger("change");
                });
            }
        },
        "dom:select/change": function(e, fnCallback) {
            e.preventDefault();
            e.stopPropagation();

            var me = this;

            var cityId = parseInt(e.currentTarget.value,10);

            me.publish("region/district/reload", {
                id: me.instance_id,
                data: helper.getDistrictsByCityId(me.options, cityId)
            }).then(fnCallback);
        }
    });
});
define('widget/select/region/districtpicker/main',["widget/select/main",
	"jquery",
	"widget/select/region/helper",
	"template!../../index.html"
], function(Select, $, helper,template) {
	"use strict";

	var INSTANCE_ID = 0;

	return Select.extend(function($element, widgetName, districtId, disabled) {
		var me = this;

		INSTANCE_ID++;
		me.instance_id = INSTANCE_ID;
		me.districtId = parseInt(districtId, 0);
		me.disabled = disabled;
	}, {
		"hub:memory/region/district/reload": function(msg) {
			var me = this;
			var $me = me.$element;

			if (msg.id === me.instance_id) {

				if (msg.data && msg.data.length > 0) {
                    // Option group changed
					if (!(_.find(msg.data, function(e, i) {
						return e.Id === me.districtId;
					}))) {
						// Do not select a default area, force to select one
						me.districtId = 0;//msg.data[0].Id;
					}
				}
				$me.data("value", me.districtId);

				// Store options for trigger event
				me.options = msg.data;

				// Rendering
				me.html(template, {
					"options": me.options,
					"selectedId": me.districtId,
					"disabled": me.disabled
				}).then(function(){
                    // Trigger event, apply value to parent
                    $me.find("[data-action=changeoption]").trigger("change");
				});
			}
		}
	});
});

define('troopjs-requirejs/template!widget/select/region/index.html',[],function() { return function template(data) { var o = "";
	var cityId = data.cityId;
	var provinceId = data.provinceId;
    var areaId = data.areaId;
    var disabled = data.disabled;
o += "\r\n<div class=\"row\">\r\n    <div class=\"col-sm-4\" data-weave=\"widget/select/region/provincepicker/main(provinceId, disabled)\" data-province-id=\"" +provinceId+ "\" data-value=\"" +provinceId+ "\" data-disabled=\"" +disabled+ "\"></div>\r\n    <div class=\"col-sm-4\" data-weave=\"widget/select/region/citypicker/main(cityId, disabled)\" data-city-id=\"" +cityId+ "\" data-value=\"" +cityId+ "\" data-disabled=\"" +disabled+ "\"></div>\r\n    <div class=\"col-sm-4\" data-action=\"updatecityarea\" data-weave=\"widget/select/region/districtpicker/main(areaId, disabled)\" data-area-id=\"" +areaId+ "\" data-value=\"" +areaId+ "\" data-disabled=\"" +disabled+ "\"></div>\r\n</div>"; return o; }; });
define('widget/select/region/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "when",
    "context",
    "widget/api/main",
    "widget/ajaxquery/main",
    "widget/select/region/helper",
    "template!./index.html",
    "json2"
], function(Widget, weave, $, when, context, api, ajaxQuery, helper, template) {
    "use strict";

    var URI_REGION_PROVINCE = api.get("region_with_province");

    return Widget.extend(function($element, widgetName, provinceId, cityId, areaId, disabled) {
        var me = this;

        me.provinceId = parseInt(provinceId, 10);
        me.cityId = parseInt(cityId, 10);
        me.areaId = parseInt(areaId, 10);
        me.disabled = disabled || false;
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Ajax
            ajaxQuery({
                "url": URI_REGION_PROVINCE,
                "data": {
                    "language": context.language,
                    "version": context.cacheKey
                },
                "type": "GET",
                "dataType": "json"
            }).then(function(data) {
                me.provinces = data.Provinces;
                me.currCities = helper.getCityByCityId(me.provinces, me.cityId);

                if (me.provinceId === 0) {
                    me.provinceId = helper.getProvinceIdByCityId(me.provinces, me.cityId);
                }

                me.html(template, {
                    "provinceId": me.provinceId,
                    "cityId": me.cityId,
                    "areaId": me.areaId,
                    "disabled": me.disabled
                }).then(function() {
                    me.publish("region/province/reload", {
                        data: me.provinces
                    });
                });
            });
        },
        "dom:[data-action=updatecityarea]/change": function(e, distict) {
            e.preventDefault();
            e.stopPropagation();

            var me = this;
            var $me = me.$element;

            if (!distict) {
                return;
            }

            var districtId = distict.Id;

            var provinces = me.provinces;
            var i = 0;
            var iLens = provinces.length;

            var cities;
            var j = 0;
            var jLens = 0;

            var districts;
            var k = 0;
            var kLens = 0;

            // Default Region
            var region = {
                Province: {
                    Id: 0,
                    Name: ""
                },
                City: {
                    Id: 0,
                    Name: ""
                },
                District: {
                    Id: 0,
                    Name: ""
                }
            };

            for (i = 0; i < iLens; i++) {
                cities = provinces[i].Cities;
                jLens = cities.length;
                for (j = 0; j < jLens; j++) {
                    districts = cities[j].District;
                    kLens = districts.length;
                    for (k = 0; k < kLens; k++) {
                        if (districts[k].Id === districtId) {
                            region.Province = {
                                "Id": provinces[i].Id,
                                "Name": provinces[i].Name
                            };
                            region.City = {
                                "Id": cities[j].Id,
                                "Name": cities[j].Name
                            };
                            region.District = {
                                "Id": districts[k].Id,
                                "Name": districts[k].Name,
                                "ExpressCompanyId": districts[k].ExpressCompanyId
                            };
                        }
                    }
                }
            }

            //
            $me
                .data("value", region)
                .trigger("reset", region);
        }
    });
});
define('widget/select/region/provincepicker/main',["widget/select/main",
    "jquery",
    "widget/select/region/helper",
    "template!../../index.html"
], function(Select, $, helper, template) {
    "use strict";

    var INSTANCE_ID = 0;

    return Select.extend(function($element, widgetName, provinceId, disabled) {
        var me = this;

        INSTANCE_ID++;
        me.instance_id = INSTANCE_ID;
        me.provinceId = parseInt(provinceId, 0);
        me.disabled = disabled;
    }, {
        "hub:memory/region/province/reload": function(msg) {
            var me = this;
            var $me = me.$element;

            if (msg.data && msg.data.length > 0) {
                // Option group changed
                if (!(_.find(msg.data, function(e, i) {
                    return e.Id === me.provinceId;
                }))) {
                    // Do not select a default area, force to select one
                    me.provinceId = 0; //msg.data[0].Id;
                }
            }
            $me.data("value", me.provinceId);

            // Store options for trigger event
            me.options = msg.data;

            // Rendering
            me.html(template, {
                "options": me.options,
                "selectedId": me.provinceId,
                "disabled": me.disabled
            }).then(function() {
                // Trigger event, apply value to parent
                $me.find("[data-action=changeoption]").trigger("change");
            });
        },
        "dom:select/change": function(e, fnCallback) {
            e.preventDefault();
            e.stopPropagation();

            var me = this;

            var provinceId = parseInt(e.currentTarget.value, 10);

            me.publish("region/city/reload", {
                id: me.instance_id,
                data: helper.getCitiesByProvinceId(me.options, provinceId)
            }).then(fnCallback);
        }
    });
});
define('widget/showmycontent/main',["troopjs-browser/component/widget",
    "jquery"
], function(Widget, $) {
    "use strict";

    var MYCONTENT_CLASS = ".showmycontent-content";
    var INFO_ACCOUNT = ".info-account";
    var MYCONTENT_DISAPPEAR_SEC = 0.2 * 1000;

    return Widget.extend(function($element, widgetName, dir) {
        var me = this;

        me.dir = dir || "b";
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            // Prevent events bubble to $me
            $myContent.click(function(e) {
                e.stopPropagation();
            });
        },
        "dom/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            if ($myContent.length <= 0) {
                return;
            }

            me.hideOtherLoginForm();

            if ($myContent.hasClass('Actived')) {
                $myContent.removeClass('Actived');
            } else {
                $myContent.addClass('Actived');
            }

            var contentWidth = $myContent.outerWidth();
            var contentHeight = $myContent.outerHeight();
            var eventWidth = $me.outerWidth();
            var eventHeight = $me.outerHeight();
            // Position reset
            switch (me.dir) {
                default:
                case "b":
                    $myContent
                        .css("left", -1 * (contentWidth - eventWidth) / 2)
                        .css("top", eventHeight);
                    break;
                case "t":
                    $myContent
                        .css("left", -1 * (contentWidth - eventWidth) / 2)
                        .css("top", -1 * (contentHeight));
                    break;
            }
            // Show or Hide Content
            $myContent.toggle();
        },
        "dom/mouseleave": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $myContent = $me.find(MYCONTENT_CLASS);
            if ($myContent.length <= 0) {
                return;
            }
            if (me.loginFormIsActive()) {
                return;
            }
            setTimeout(function() {
                $myContent.hide();
            }, MYCONTENT_DISAPPEAR_SEC);
        },
        "loginFormIsActive": function() {
            var me = this;
            var $me = me.$element;
            var $myContent = $me.find(MYCONTENT_CLASS);
            var isActive = false;
            $myContent.find("input").each(function(i) {
                if ($(this).is(":focus")) {
                    isActive = true;
                }
            });
            return isActive;
        },
        "hideOtherLoginForm": function() {
            var me = this;
            var $me = me.$element;
            $(INFO_ACCOUNT).find(MYCONTENT_CLASS).each(function(i) {
                if ($(this).hasClass('Actived')) {
                    $(this).removeClass('Actived');
                    $(this).hide();
                }
            });
        }
    });
});

define('troopjs-requirejs/template!widget/startdate/index.html',[],function() { return function template(data) { var o = "";
	var dateFrom = data.dateFrom;
	var disabled = data.disabled;
	var range = data.range;
	var dateTo = data.dateTo;
o += "\r\n"; if(range) { o += "\r\n\t<div class=\"row startdate-range\">\r\n\t\t<div class=\"col-md-6\">\r\n\t\t\t<span data-weave=\"widget/blurb/main(1399)\">From</span>\r\n\t\t\t<input type=\"text\" class=\"form-control datepicker-from\" data-action=\"datepickerfrom\" value=\"" +dateFrom+ "\" "; if(disabled) { o += "disabled"; } o += " onfocus=\"this.blur();\">\r\n\t\t</div>\r\n\t\t<div class=\"col-md-6\">\r\n\t\t\t<span data-weave=\"widget/blurb/main(1400)\">to</span>\r\n\t\t\t<input type=\"text\" class=\"form-control datepicker-to\" data-action=\"datepickerto\" value=\"" +dateTo+ "\" "; if(disabled) { o += "disabled"; } o += ">\r\n\t\t</div>\r\n\t</div>\r\n"; } else { o += "\r\n\t<input type=\"text\" class=\"form-control datepicker-from\" style=\"position: relative; z-index: 2;\" data-action=\"datepickerfrom\" onfocus=\"this.blur();\" value=\"" +dateFrom+ "\" "; if(disabled) { o += "disabled"; } o += ">\r\n"; }  return o; }; });
define('widget/startdate/main',["troopjs-browser/component/widget",
    "jquery",
    "when",
    "context",
    "widget/basic/cloneobject",
    "widget/pageurl/main",
    "widget/time/main",
    "widget/infobox/main",
    "widget/troophash/main",
    "template!./index.html",
    "jquery.ui.datepicker",
    "jquery.ui.datepicker-zh-CN",
    "underscore"
], function(Widget, $, when, context, cloneObject, pageUrl, Time, InfoBox, troopHash, template) {
    "use strict";

    // Time function
    var INS_TIME = new Time();

    // To order from tomorrow
    // Default: should order before 10:00 am
    var HOUR_AHEAD = 10;

    // Month duration
    // Default: 1 month
    var MONTH_RANGE = 1;

    // Official holiday
    var HOLIDAY = [
        // Note: Month start from 0
        new Date(2016, 3, 1),
        new Date(2016, 3, 2),
		new Date(2016, 3, 3),
		new Date(2016, 3, 4),
    ];

    function geti18n() {
        var lngMap = {
            "en": "",
            "cn": "zh-CN"
        };
        var lng = context.language || "en";
        return lngMap[lng];
    }

    function transformDateArray(DateArray) {
        if (!DateArray && DateArray.length <= 0) {
            return;
        }

        var disableDays = [];
        _.each(DateArray, function(e) {
            // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
            // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
            var d = INS_TIME.addDay(e, 1);
            disableDays.push(INS_TIME.getStrDate(d, "-"));
        });
        return disableDays;
    }

    return Widget.extend(function($element, widgetName, range, rangeMonth) {
        var me = this;

        me.range = range || false;
        me.rangeMonth = 0;
        if (range) {
            // Default: 3 months
            me.rangeMonth = parseInt(rangeMonth, 10) || MONTH_RANGE;
        }

        me._holiday = HOLIDAY;
    }, {
        "sig/start": function() {
            var me = this;
        },
        // 饮用周期大于1天时，需要考虑假期是否在饮用周期内
        "preForbidDate": function(days) {
            var me = this;

            days = days || 1;
            if (!me._holiday && me._holiday.length <= 0) {
                return;
            }
            var holidayStartDate = me._holiday[0];
            var holidayMax = cloneObject(me._holiday);
            var i = 1;
            for (i = 1; i < days; i++) {
                holidayMax.unshift(INS_TIME.addDay(holidayStartDate, i * -1));
            }
            /* create an array of days which need to be disabled */
            me._disableDays = transformDateArray(holidayMax);
        },
        // wdays[array]
        "preForbidWeekDays": function(wdays) {
            var me = this;

            if (!wdays) {
                return;
            }

            var arrWeekDays = [0, 1, 2, 3, 4, 5, 6];

            if (wdays.length > 0) {
                me._disableWeekDays = _.difference(arrWeekDays, wdays);
            } else {
                me._disableWeekDays = [];
            }
        },
        "getAvailableStartDate": function(hourAhead, days, wdays) {
            var me = this;

            hourAhead = hourAhead || HOUR_AHEAD;
            days = days || 1;
            var now = new Date();
            var dateNow = INS_TIME.getZeroDate(now);

            var hour = now.getHours();
            var startDate = hour >= hourAhead ? INS_TIME.addDay(dateNow, 2) : INS_TIME.addDay(dateNow, 1);
            var endDate = INS_TIME.addDay(startDate, days - 1);

            if (me._holiday && me._holiday.length > 0) {
                // 遇到节假日，节假日当天放假，不生产，导致节假日后一天不能配送
                // 节假日后一天的订单，会放到节假日前一天生产，节假日当天一起配送
                (function() {
                    var i = 0;
                    var iLens = me._holiday.length;
                    var holiday;

                    for (i = 0; i < iLens; i++) {
                        holiday = INS_TIME.addDay(INS_TIME.getZeroDate(me._holiday[i]), 1);

                        // startdate <= holiday <= enddate
                        // move startdate to the day after holiday
                        if (INS_TIME.compareDate(holiday, startDate) <= 0 && INS_TIME.compareDate(holiday, endDate) >= 0) {
                            // Reset dateRange
                            startDate = INS_TIME.addDay(holiday, 1);
                            endDate = INS_TIME.addDay(startDate, days - 1);
                            continue;
                        }
                    }
                })();
            }

            // Has weekdays, select recent date(the nearest date after startdate) match weekdays
            return INS_TIME.getNearestWeekdayDate(startDate, wdays);
        },
        // 不配送日期
        "offDays": function(date, disableWeekdays) {
            var me = this;

            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();
            var weekday = date.getDay();

            // Disable weedays, union preForbid & argument
            disableWeekdays = disableWeekdays || [];
            disableWeekdays = _.union(disableWeekdays, me._disableWeekDays);

            if (me._disableDays && me._disableDays.length > 0) {
                if ($.inArray(y + '-' + (m + 1) + '-' + d, me._disableDays) != -1 || new Date() > date) {
                    return [false];
                }
            }
            if (disableWeekdays && disableWeekdays.length > 0) {
                if ($.inArray(weekday, disableWeekdays) != -1 || new Date() > date) {
                    return [false];
                }
            }

            return [true];
        },
        "render": function(dateAvailable, extOption, disabled) {
            var me = this;
            var $me = me.$element;

            // Extend
            if (extOption && extOption.minDate) {
                dateAvailable = extOption.minDate;
            }

            // Initialize start date from today
            me.publish("deliveryStartDate/change", {
                // Date info
                "availableStartDate": dateAvailable,
                "selectedDate": dateAvailable
            });

            // Prepare for rendering
            var dataOption = {
                "dateFrom": INS_TIME.getStrDate(dateAvailable, "-"),
                "disabled": disabled,
                "language": context.language,
                "range": me.range
            };

            var dateTo;
            var weekDaysLens = me._arrDays.length || 0;
            var arrLastWeekDay = [];
            if (me.range) {
                if (weekDaysLens > 0) {
                    arrLastWeekDay.push(me._arrDays[weekDaysLens - 1]);
                }
                dateTo = INS_TIME.getNearestWeekdayDate(INS_TIME.addMonth(dateAvailable, me.rangeMonth),
                    arrLastWeekDay);
                _.extend(dataOption, {
                    "dateTo": INS_TIME.getStrDate(dateTo, "-")
                });

                // Initialize end date from today
                me.publish("deliveryEndDate/change", {
                    // Date info
                    "selectedDate": dateTo
                });
            }

            // Rendering
            me.html(template, dataOption).then(function() {
                // Generate datepicker by jQuery-UI
                $(document.body).addClass("jquery-ui-lightness");
                // Localize
                $.datepicker.setDefaults($.datepicker.regional[geti18n()]);
                // datepicker options
                var datepickerOptionFrom = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+3m",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryStartDate/change", {
                            // Date info
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "availableStartDate": dateAvailable,
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                var datepickerOptionTo = {
                    "firstDay": 1,
                    "showOtherMonths": false,
                    "selectOtherMonths": true,
                    "dateFormat": "yy-mm-dd",
                    "minDate": dateAvailable,
                    "maxDate": "+1y",
                    "beforeShowDay": function(date) {
                        return me.offDays(date);
                    },
                    "numberOfMonths": context.isMobile ? 1 : 2,
                    "onSelect": function(strDate, inst) {
                        me.publish("deliveryEndDate/change", {
                            // All date format to "xxxx-xx-xx 00:00:00"
                            "selectedDate": INS_TIME.getDateByString(strDate)
                        });
                    }
                };
                if (extOption) {
                    datepickerOptionFrom = _.extend(datepickerOptionFrom, extOption);
                    datepickerOptionTo = _.extend(datepickerOptionTo, extOption);
                }

                var $datepickerFrom = $me.find("[data-action=datepickerfrom]");
                var $datepickerTo;

                if (me.range) {
                    $datepickerTo = $me.find("[data-action=datepickerto]");

                    // Extend datepicker options
                    $.extend(datepickerOptionFrom, {
                        "beforeShowDay": function(date) {
                            // Only first active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.rest(me._arrDays)) : me.offDays(date);
                        },
                        "onSelect": function(strDate, inst) {
                            me.publish("deliveryStartDate/change", {
                                // Date info
                                // All date format to "xxxx-xx-xx 00:00:00"
                                "availableStartDate": dateAvailable,
                                "selectedDate": INS_TIME.getDateByString(strDate)
                            }).then(function() {
                                // Focus datepicker to element
                                $datepickerTo.focus();
                            });
                        },
                        "onClose": function(selectedDate) {
                            $datepickerTo.datepicker("option", "minDate", selectedDate);
                        }
                    });
                    $.extend(datepickerOptionTo, {
                        "beforeShowDay": function(date) {
                            // Only last active day is clickble
                            return weekDaysLens > 1 ? me.offDays(date, _.initial(me._arrDays)) : me.offDays(date);
                        },
                        "onClose": function(selectedDate) {
                            $datepickerFrom.datepicker("option", "maxDate", selectedDate);
                        }
                    });

                    // Datepicker To
                    $datepickerTo.datepicker(datepickerOptionTo);
                }

                // Datepicker From
                $datepickerFrom.datepicker(datepickerOptionFrom);
            });
        },
        // eventCode[string]
        // days[int]
        // wdays[array]
        "process": function(hourAhead, eventCode, days, wdays) {
            var me = this;

            var dateAvailable;
            var dateEvent;
            var dateEventEnd;
            var dateNow = INS_TIME.getZeroDate(new Date());

            days = days || 1; // 1 day cleanse

            // Forbit date before Holiday while multi days
            me.preForbidDate(days);

            // Forbit days in week
            if (wdays && wdays.length > 0) {
                me.preForbidWeekDays(wdays);
            }

            // Available first date for order
            // First weekdays is a special requirement from subscription
            var firstWeekday = [];
            if (wdays && wdays.length > 0) {
                firstWeekday.push(wdays[0]);
            }
            dateAvailable = me.getAvailableStartDate(hourAhead, days, firstWeekday);

            // Switch package
            switch (eventCode.toUpperCase()) {
                case "CFAC": // Clease for a Cause
                    dateEvent = new Date(2014, 7, 30);
                    dateEventEnd = new Date(2014, 7, 30);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, true);
                    }
                    break;
                case "WARM": // Warming Day
                    dateEvent = new Date(2014, 11, 16);
                    dateEventEnd = new Date(2015, 0, 4);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        }, false);
                    }
                    break;
                case "VALE": // Valentine's Day
                    dateEvent = new Date(2015, 1, 11);
                    dateEventEnd = new Date(2015, 1, 18);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "WOMN": // Women's Day
                    dateEvent = new Date(2015, 2, 3);
                    dateEventEnd = new Date(2015, 2, 9);
                    if (dateNow > dateEventEnd) {
                        // Show error msg as a lightbox
                        new InfoBox({
                            title: 1144,
                            content: 1393,
                            closeCallback: function() {
                                pageUrl.goto("/order");
                            }
                        }).open();

                        return;
                    } else if (dateAvailable > dateEvent) {
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent,
                            "maxDate": dateEventEnd
                        }, false);
                    }
                    break;
                case "BEAR": // Moonbear
                    dateEvent = new Date(2014, 9, 10);
                    if (dateAvailable > dateEvent) {
                        // CFC special logic, buy after 8.30
                        me.render(dateAvailable);

                        return;
                    } else {
                        // Rendering
                        me.render(dateAvailable, {
                            "minDate": dateEvent
                        });
                    }
                    break;
                case "BRDL": // Bridal
                    if (dateAvailable.getDay() !== 1) {
                        dateEvent = INS_TIME.addDay(dateAvailable, 8 - dateAvailable.getDay());
                    } else {
                        dateEvent = dateAvailable;
                    }
                    // Rendering
                    me.render(dateAvailable, {
                        "minDate": dateEvent,
                        "beforeShowDay": function(date) {
                            return offDays(date, [0, 2, 3, 4, 5, 6]);
                        }
                    });
                    break;
                default:
                    // Rendering
                    me.render(dateAvailable);
                    return;
            }
        },
        // Special/Event order listner
        /*"hub:memory/route": function(uriPath, uriEvent) {
            var me = this;

            var hashPath = uriPath.path;
            var hashMap = troopHash.parse(hashPath);

            var eventCode = hashMap.event || "";
            var days = parseInt(hashMap.days, 10) || 1;
            var wdays = hashMap.wdays || "";
            var hourAhead = hashMap.hour || HOUR_AHEAD;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.wdays === wdays &&
                me.hourAhead === hourAhead
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.wdays = wdays;
            me.hourAhead = hourAhead;

            var arrPreDays;
            var arrDays = [];

            if (wdays) {
                arrPreDays = wdays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(hourAhead, eventCode, days, arrDays);
        },
        */
        "hub:memory/startdate/set": function(dateOption) {
            var me = this;

            if (!dateOption) {
                return;
            }

            var eventCode = dateOption.eventCode || "";
            var days = parseInt(dateOption.days, 10) || 1;
            var weekDays = (dateOption.weekDays || "").toString();
            var hourAhead = dateOption.hourAhead || HOUR_AHEAD;
            var holiday = dateOption.holiday || HOLIDAY;

            // Trigger event while following param changes
            if (me.eventCode === eventCode &&
                me.days === days &&
                me.weekDays === weekDays &&
                me.hourAhead === hourAhead &&
                me._holiday.length == holiday.length &&
                _.difference(holiday, me._holiday).length <= 0
            ) {
                return;
            }
            me.eventCode = eventCode;
            me.days = days;
            me.weekDays = weekDays;
            me.hourAhead = hourAhead;
            me._holiday = holiday;

            var arrPreDays;
            var arrDays = [];

            if (weekDays) {
                arrPreDays = weekDays.split(",");
                _.each(arrPreDays, function(e, i) {
                    e = parseInt(e, 10);
                    if (isNaN(e) || e < 0 || e > 6) {
                        return;
                    }
                    arrDays.push(e);
                });
            }

            // For datepicker range
            me._arrDays = arrDays;

            // Process
            me.process(me.hourAhead, me.eventCode, me.days, me._arrDays);

            return when.resolve();
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/addon/index.html',[],function() { return function template(data) { var o = "<div class=\"option-addon\">\r\n    <label>\r\n        <input type=\"checkbox\" data-id=\"22\" data-price=\"50\" class=\"icheck\" data-action=\"changeaddon\">\r\n        <span data-weave=\"widget/blurb/main(1292)\">Purchase Coconut Water</span>\r\n    </label>\r\n</div>\r\n<div class=\"option-addon\">\r\n    <label>\r\n        <input type=\"checkbox\" data-id=\"20\" data-price=\"25\" class=\"icheck\" data-action=\"changeaddon\">\r\n        <span data-weave=\"widget/blurb/main(1291)\">Purchase Spicy Lemonade</span>\r\n    </label>\r\n</div>"; return o; }; });
define('widget/subscription/addon/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend(function() {
        var me = this;

        me.addon = [];
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            me.html(template).then(function() {
                $me.find('.icheck').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '20%'
                });
            });
        },
        "dom:[data-action=changeaddon]/ifChanged": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);

            me.update({
                "id": parseInt($e.data("id"), 10),
                "price": parseInt($e.data("price"), 10)
            }, e.currentTarget.checked);
        },
        "hub/subscription/addon/reset": function(gender) {
            var me = this;
            var $me = me.$element;

            $me.find('.icheck').iCheck('uncheck');
        },
        "update": function(data, isAppend) {
            var me = this;
            var $me = me.$element;

            var i = 0;
            var iLens = me.addon.length;

            if (isAppend) {
                me.addon.push(data);
            } else {
                for (i = 0; i < iLens; i++) {
                    if (me.addon[i].id === data.id) {
                        me.addon.splice(i, 1);
                        break;
                    }
                }
            }

            me.publish("subscription/addon/update", me.addon);
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/category/index.html',[],function() { return function template(data) { var o = "";
    var categories = data.categories;
    var col = data.col;

    var i = 0;
    var iLens = categories.length;
o += "\r\n<ul class=\"nav nav-tabs\" role=\"tablist\" data-weave=\"widget/basic/bootstrap-tab\">\r\n    ";
        for(i = 0; i < iLens; i++){
            appendTab(categories[i], i+1, iLens);
        }
    o += "\r\n</ul>\r\n<div class=\"tab-content\">\r\n    ";
        for(i = 0; i < iLens; i++){
            appendTabContent(categories[i]);
        }
    o += "\r\n</div>\r\n\r\n"; function appendTab(category, sequence, lens) { o += "\r\n    <li class=\""; if (category.IsDefault) { o += "active"; } o += " "; if (sequence === lens) { o += "last"; } o += "\" data-action=\"changepackagetab\" data-category-id=\"" + category.Id + "\">\r\n        <a href=\"#category_" + category.Id + "\" role=\"tab\" data-toggle=\"tab\">" + category.Name + "</a>\r\n    </li>\r\n    "; if(sequence < lens) { o += "\r\n        <li class=\"nav-tabs-sep\"><a>/</a></li>\r\n    "; } o += "\r\n"; } o += "\r\n\r\n"; function appendTabContent(category) { o += "\r\n    <div class=\"tab-pane fade "; if (category.IsDefault) { o += "in active"; } o += "\" id=\"category_" + category.Id + "\">\r\n        "; if (category.Id === 0) { o += "\r\n            <div data-weave=\"widget/subscription/customize/main\"></div>\r\n        "; } else { o += "\r\n            <div data-weave=\"widget/subscription/category/package/main(categoryId, col)\" data-category-id=\"" + category.Id + "\" data-col=\"" +col+ "\" class=\"row subscription-packagecategory\"></div>\r\n            <div class=\"subscription-subsidiary row\">\r\n                <div class=\"col-md-4\">\r\n                    <h4>\r\n                        <span class=\"glyphicon glyphicon-play\"></span>\r\n                        <span data-weave=\"widget/blurb/main(p1005)\">Gender?</span>\r\n                    </h4>\r\n                    <div data-weave=\"widget/subscription/gender/main\"></div>\r\n                </div>\r\n                <div class=\"col-md-8\">\r\n                    <h4>\r\n                        <span class=\"glyphicon glyphicon-play\"></span>\r\n                        <span data-weave=\"widget/blurb/main(p1022)\">Want more juice? Add to your package?</span>\r\n                    </h4>\r\n                    <div data-weave=\"widget/subscription/addon/main\"></div>\r\n                </div>\r\n            </div>\r\n        "; } o += "\r\n    </div>\r\n"; }  return o; }; });
define('widget/subscription/category/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/blurb/main",
    "widget/troophash/main",
    "template!./index.html",
    "underscore"
], function(Widget, weave, $, context, ajaxQuery, api, Blurb, troopHash, template) {
    "use strict";

    var blurb = new Blurb($(document.body));

    var URI_PACKAGE_CATEGORY = api.get("package_category");
    // Own program, packageId = 0, groupId = 0
    var PACKAGE_CUSTOMIZE = {
        "Id": 0,
        "Name": blurb.get(1155),
        "Description": blurb.get(1352)
    };

    return Widget.extend(function() {
        var me = this;
    }, {
        "hub:memory/route": function(uriPath, uriEvent) {
            var me = this;
            var $me = me.$element;

            var hashPath = uriPath.path || "category/1".split("/");
            var hashMap = troopHash.parse(hashPath);

            // Set defatul = 2, warming package first
            var categoryId = parseInt(hashMap.category, 10) || 2;
            var col = parseInt(hashMap.col, 10) || 1;

            // Prevent reload while other hash changes
            if (me.categoryId && me.col && categoryId === me.categoryId && me.col === col) {
                return;
            }
            me.categoryId = categoryId;
            me.col = col;

            ajaxQuery({
                url: URI_PACKAGE_CATEGORY,
                data: {
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var categories = data.Categories;
                // Append customize package
                categories.push(PACKAGE_CUSTOMIZE);

                // Set default
                var i = 0;
                var iLens = categories.length;
                for (i = 0; i < iLens; i++) {
                    if (categories[i].Id === categoryId) {
                        categories[i].IsDefault = true;
                    }
                }

                // Render
                me.html(template, {
                    "categories": categories,
                    "col": col
                }).then(function() {
                    me.publish("subscription/packagecategory/change", categoryId);
                });
            });
        },
        // Package
        "dom:[data-action=changepackagetab]/click": function(e) {
            e.preventDefault();
            var me = this;

            var $e = $(e.currentTarget);

            var packageCategoryId = parseInt($e.data("categoryId"), 10);

            me.publish("subscription/packagecategory/change", packageCategoryId);
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/category/package/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer || "";
    var language = data.language;
    var col = data.col;

    var PIC_PACKAGE_URL = cacheServer + "/images/cleanse/";
    var PIC_TAG_URL = cacheServer + "/images/tags/";
    var PIC_PACKAGE_DEFAULT_URL = cacheServer + "/images/cleanse/default.jpg";
    var cols = 3;
    var colSpan = Math.floor(12 / cols);

    var packages = data.packages;
    var i = 0;
    var iLens = packages.length;
    var row = 0;
    var rows = Math.ceil(iLens / cols);
    for (row = 0; row < rows; row++) {
        for(i = row * cols; i < (row + 1) * cols; i++){
            // Kick out Gift Card
            if(packages[i] && packages[i].Id !== 10){
                appendPackage(packages[i], i+1, (row + 1) * cols);
            }
        }
    }
o += "\r\n<!-- package is a keyword? -->\r\n"; function appendPackage(pkg, sequence, lens) { o += "\r\n    <div class=\"col-md-" +colSpan+ " packagecontainer "; if (sequence === lens) { o += "last"; } o += " "; if (sequence === col) { o += "active"; } o += "\" data-action=\"packagecontainer\" data-package-id=\"" +pkg.Id+ "\">\r\n        <div class=\"packagemain\">\r\n            <h2>\r\n            " +(pkg.Title || "&nbsp;")+ "\r\n                "; if (pkg.Tags && pkg.Tags.length > 0) {
                    var j = 0;
                    var jLens = pkg.Tags.length;
                    for (j = 0; j < jLens; j++) {
                o += "\r\n                    <span class=\"tag\">\r\n                        <img src=\"" +PIC_TAG_URL + pkg.Tags[j].ImagePath+ "\" alt=\"" +pkg.Tags[j].Name+ "\" title=\"" +pkg.Tags[j].Name+ "\">\r\n                    </span>\r\n                ";
                        }
                    }
                o += "\r\n            </h2>\r\n            <div class=\"cleanse-img\" data-action=\"showcleansedetail\">\r\n                "; if (pkg.ImageName) { o += "\r\n                    <img src=\"" + PIC_PACKAGE_URL + pkg.ImageName+ "\" alt=\"" +pkg.Title+ "\" title=\"" +pkg.Title+ "\">\r\n                "; } else { o += "\r\n                    <img src=\"" + PIC_PACKAGE_DEFAULT_URL+ "\" alt=\"" +pkg.Title+ "\" title=\"" +pkg.Title+ "\">\r\n                "; } o += "\r\n                <a href=\"/cleanse/detail\" target=\"blank\" data-hash=\"packageid/" +pkg.Id+ "\" data-weave=\"widget/href/main\" class=\"cleanse-img-cover\">\r\n                    <span data-weave=\"widget/blurb/main(1340)\">Details</span>\r\n                </a>\r\n            </div>\r\n            <div class=\"cleanse-intro\">\r\n                <div class=\"cleanse-intro-des\">" +(pkg.Description || "&nbsp;")+ "</div>\r\n            </div>\r\n            <br>\r\n            <a class=\"btn btn-lg btn-default btn-block\" data-action=\"selectpackage\" data-weave=\"widget/blurb/main(1601)\">SELECT</a>\r\n        </div>\r\n    </div>\r\n"; }  return o; }; });
define('widget/subscription/category/package/main',["troopjs-browser/component/widget",
    "when",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/pageurl/main",
    "widget/cookie/main",
    "widget/troophash/main",
    "widget/enrollment/signin/main",
    "template!./index.html",
    "json2"
], function(Widget, when, $, context, ajaxQuery, api, pageUrl, cookie, troopHash, Signin, template) {
    "use strict";

    (new Signin($(document.body))).start();

    var URI_PACKAGE_BYCATEGORY = api.get("package_loadbycategory");

    return Widget.extend(function($element, widgetName, categoryId, col) {
        var me = this;

        me.categoryId = parseInt(categoryId, 10) || 0;
        me.col = col || 1;

        me.deferred = when.defer();
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            if (!me.categoryId) {
                return;
            }

            ajaxQuery({
                url: URI_PACKAGE_BYCATEGORY,
                data: {
                    categoryId: me.categoryId,
                    language: context.language
                },
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {
                var packages = me.packages = data.Packages;

                // Render
                me.html(template, {
                    "packages": packages,
                    "cacheServer": context.cacheServer,
                    "language": context.language,
                    "col": me.col
                }).then(function(){
                    me.deferred.resolver.resolve();
                });
            });
        },
        "hub/subscription/packagecategory/change": function(packageCategoryId) {
            var me = this;

            if (me.categoryId !== packageCategoryId) {
                return;
            }

            me.deferred.promise.then(function() {
                // Choose first package as default
                me.publish("subscription/package/update", me.packages[me.col - 1]);
            });
        },
        "dom:[data-action=selectpackage]/click": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $packageContainer = $e.parents("[data-action=packagecontainer]");

            $packageContainer.siblings().removeClass("active");
            $packageContainer.addClass("active");

            // Package data
            var packageId = parseInt($packageContainer.data("packageId"), 10);

            var packages = me.packages;
            var i = 0;
            var iLens = packages.length;

            var iPackage;
            for (i = 0; i < iLens; i++) {
                if (packages[i].Id === packageId) {
                    iPackage = packages[i];
                    break;
                }
            }

            // publish
            me.publish("subscription/package/update", iPackage);
        },
        "dom:[data-action=showcleansedetail]/mouseenter": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeIn(200);
        },
        "dom:[data-action=showcleansedetail]/mouseleave": function(e) {
            e.preventDefault();

            var me = this;
            var $e = $(e.currentTarget);
            var $cover = $e.find(".cleanse-img-cover");

            $cover.fadeOut(200);
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/customize/index.html',[],function() { return function template(data) { var o = "<div class=\"row customize orderedit-products\" data-weave=\"widget/customize/productcategory/main\"></div>"; return o; }; });
define('widget/subscription/customize/main',["troopjs-browser/component/widget",
  "jquery",
  "template!./index.html"
], function(Widget, $, template) {
  "use strict";

  return Widget.extend(function() {
    var me = this;

    me.dailyProducts = [];
  }, {
    "hub/subscription/customize/init": function() {
      var me = this;

      return me.html(template).then(function() {
        // Get products
        me.publish("orderProduct/get");
      });
    },
    // Products change
    "hub/customize/changeProduct": function(args) {
      var me = this;

      // {
      //     id: xxx,
      //     price: xxx,
      //     count: xxx
      // }
      if (!args) {
        return;
      }
      var id = args.id;
      var price = args.price;
      var count = args.count;

      var j = 0;
      var jLens = me.dailyProducts.length;

      var isNewProduct = true;

      // Already has products
      if (jLens) {
        for (j = 0; j < jLens; j++) {
          if (me.dailyProducts[j].Id === id) {
            isNewProduct = false;

            // Update only when products count > 0
            if (count > 0) {
              me.dailyProducts[j].Count = count;
            } else {
              // Remove current product
              me.dailyProducts.splice(j, 1);
            }
            break;
          }
        }
        if (isNewProduct && count > 0) {
          // count should always > 0
          me.dailyProducts.push({
            "Id": id,
            "Price": price,
            "Count": count
          });
        }
      } else if (count > 0) {
        // count should always > 0
        me.dailyProducts.push({
          "Id": id,
          "Price": price,
          "Count": count
        });
      }

      // publish
      me.publish("subscription/customize/update", me.dailyProducts);
    }
  });
});

define('troopjs-requirejs/template!widget/subscription/fivetwo/index.html',[],function() { return function template(data) { var o = "<div class=\"row fivetwo\">\r\n    <div class=\"col-md-4\">\r\n        <h4 data-weave=\"widget/blurb/main(p1023)\">A more challengeing cleanse for</h4>\r\n        <ul>\r\n            <li data-weave=\"widget/blurb/main(p1024)\">5:2 dieter</li>\r\n            <li data-weave=\"widget/blurb/main(p1025)\">Experienced cleansers</li>\r\n            <li data-weave=\"widget/blurb/main(p1026)\">Light eaters</li>\r\n            <li data-weave=\"widget/blurb/main(p1027)\">Serious dieters (weight loss)</li>\r\n        </ul>\r\n        <br>\r\n        <div>\r\n            <h4><span class=\"glyphicon glyphicon-play\"></span><span data-weave=\"widget/blurb/main(p1005)\">Gender?</span></h4>\r\n            <div data-weave=\"widget/subscription/gender/main\"></div>\r\n        </div>\r\n        <br>\r\n        <div>\r\n            <h4><span class=\"glyphicon glyphicon-play\"></span><span data-weave=\"widget/blurb/main(p1022)\">Want more juice? Add to your package?</span></h4>\r\n            <div data-weave=\"widget/subscription/addon/main\"></div>\r\n        </div>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n        <img src=\"/images/blank.png\" data-weave=\"widget/basic/imageswitch\" data-src=\"images/subscription/52.jpg\" alt=\"5:2\" width=\"100%\">\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n        <p data-weave=\"widget/blurb/main(p1028)\">\r\n            VCLEANSE new 5:2 juice package contains 7 bottles of juices for the female package. The package contains 3 green juices, 2 lemonades, 1 cranberry nut milk and 1 coconut water. The male version contains a total of 9 bottles – with one extra cranberry nut milk and one extra coconut water.\r\n        </p>\r\n        <p data-weave=\"widget/blurb/main(p1029)\">\r\n            Created especially for 5:2 dieters, the new 5:2 package is strictly calculated in nutrition and calories. It contains even more colorful choices, yet lower calories than slim package (520 versus 823 for female package, and 730 versus 1250 for male package), making your weekly cleanse easier and flexible.\r\n        </p>\r\n    </div>\r\n</div>"; return o; }; });
define('widget/subscription/fivetwo/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            me.html(template);
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/gender/index.html',[],function() { return function template(data) { var o = "";
	var gender = data || 2;
o += "\r\n<div class=\"btn-group option-gender\" data-toggle=\"buttons\" data-action=\"\">\r\n    <label class=\"btn btn-option "; if(gender === 1){ o += "active"; } o += "\">\r\n        <input type=\"radio\" name=\"radio-gender\" data-action=\"changegender\" value=\"1\" "; if(gender === 1){ o += "checked"; } o += "><span data-weave=\"widget/blurb/main(1122)\">Male</span>\r\n    </label>\r\n    <label class=\"btn btn-option "; if(gender === 2){ o += "active"; } o += "\">\r\n        <input type=\"radio\" name=\"radio-gender\" data-action=\"changegender\" value=\"2\" "; if(gender === 2){ o += "checked"; } o += "><span data-weave=\"widget/blurb/main(1121)\">Female</span>\r\n    </label>\r\n</div>\r\n<a class=\"gender-question link-fn\" data-toggle=\"popover\" data-container=\"body\" data-placement=\"top\" data-content=\"\" title=\"\" data-original-title=\"\" data-weave=\"widget/blurb/main(p1016, 'data-content')\">\r\n    <span class=\"glyphicon glyphicon-question-sign\"></span>\r\n</a>"; return o; }; });
define('widget/subscription/gender/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html"
], function(Widget, $, template) {
    "use strict";

    // 1: male
    // 2: female
    var GENDER_DEFAULT = 2;

    return Widget.extend(function() {
        var me = this;
    }, {
        "render": function(gender){
            var me = this;
            var $me = me.$element;

            // Default: female
            gender = gender || GENDER_DEFAULT;

            // Render
            me.html(template, gender).then(function(){
                $me.find("[data-toggle=popover]").popover();
            });
        },
        "sig/start": function() {
            var me = this;
            
            me.render();
        },
        "hub/subscription/gender/render": function(gender) {
            var me = this;

            me.render(gender);
        },
        "dom:[data-action=changegender]/change": function(e) {
            e.preventDefault();
            var me = this;

            var gender = parseInt(e.currentTarget.value, 10);

            me.publish("subscription/gender/update", gender);
        }
    });
});

define('troopjs-requirejs/template!widget/subscription/index.html',[],function() { return function template(data) { var o = "";
    var monthDuration = data.monthDuration;
o += "\r\n<div class=\"subscription-plan\">\r\n    <div class=\"subscription-heading\">\r\n        <h2 data-weave=\"widget/blurb/main(p1001)\" class=\"text-center\">SELECT SCHEDULE</h2>\r\n    </div>\r\n    <div class=\"subscription-body\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-7\">\r\n                <h3>\r\n                    <span class=\"badge\">1</span>\r\n                    <span class=\"subscription-plan-step-title\" data-weave=\"widget/blurb/main(p1006)\">Days cleansing</span>\r\n                </h3>\r\n                <div class=\"btn-group option-days\" data-toggle=\"buttons\" data-action=\"\">\r\n                    <label class=\"btn btn-option active\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"1\" checked><span data-weave=\"widget/blurb/main(p1009)\">Mon</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"2\"><span data-weave=\"widget/blurb/main(p1010)\">Tue</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"3\"><span data-weave=\"widget/blurb/main(p1011)\">Wed</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"4\"><span data-weave=\"widget/blurb/main(p1012)\">Thu</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"5\"><span data-weave=\"widget/blurb/main(p1013)\">Fri</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"6\"><span data-weave=\"widget/blurb/main(p1014)\">Sat</span>\r\n                    </label>\r\n                    <label class=\"btn btn-option\">\r\n                        <input type=\"checkbox\" name=\"checkbox-days\" data-action=\"changeweekdays\" value=\"0\"><span data-weave=\"widget/blurb/main(p1015)\">Sun</span>\r\n                    </label>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-5\">\r\n                <h3>\r\n                    <span class=\"badge\">2</span>\r\n                    <span class=\"subscription-plan-step-title\" data-weave=\"widget/blurb/main(p1007)\">Start duration</span>\r\n                </h3>\r\n                <div class=\"subscription-plan-single\" data-weave=\"widget/startdate/main(range, rangeMonth)\" data-range=\"true\" data-range-month=\"" +monthDuration+ "\"></div>\r\n            </div>\r\n        </div>\r\n        <div class=\"subscription-how\">\r\n            <br>\r\n            <p>\r\n                <a href=\"http://news.vcleanse.com/?p=1035\" target=\"blank\" data-weave=\"widget/blurb/main(p1008)\">Need help making your juice plan? See our top recommendations!</a>\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"subscription-package\">\r\n    <div class=\"subscription-heading\">\r\n        <h3>\r\n            <span class=\"badge\">3</span>\r\n            <span class=\"subscription-plan-step-title\" data-weave=\"widget/blurb/main(p1017)\">SELECT A CLEANSE OR SELECT INDIVIDUAL JUICES</span>\r\n        </h3>\r\n    </div>\r\n    <div class=\"subscription-body\" data-weave=\"widget/subscription/category/main\"></div>\r\n</div>\r\n<div class=\"subscription-result\">\r\n    <div class=\"subscription-heading\">\r\n        <h2 data-weave=\"widget/blurb/main(p1018)\">ORDER INFORMATION</h2>\r\n    </div>\r\n    <div class=\"subscription-body\">\r\n        <p class=\"subscription-summary\" data-action=\"summarycontainer\"></p>\r\n        <hr>\r\n        <div class=\"text-right\">\r\n            <p class=\"subscription-total\">\r\n                <span data-weave=\"widget/blurb/main(1040)\">total:</span>\r\n                <span class=\"subscription-total-money\" data-action=\"showtotalmoney\">&yen; 7200</span>\r\n            </p>\r\n            <button type=\"button\" class=\"btn-action btn btn-default btn-lg\" data-action=\"submitsubsciption\" data-weave=\"widget/blurb/main(1031)\">CONFIRM</button>\r\n        </div>\r\n    </div>\r\n</div>"; return o; }; });

define('troopjs-requirejs/template!widget/subscription/summary.html',[],function() { return function template(data) { var o = "";
	var gender = data.gender;
	var weekDays = data.weekDays;
	var deliveryStartDate = data.deliveryStartDate;
	var deliveryEndDate = data.deliveryEndDate;
	var duration = data.duration;
	var packageId = data.packageId;
	var packageName = data.packageName;
	var language = data.language;

	var strGender = "";
	if(packageId && gender){
		if(language === "cn"){
			strGender = gender === 1 ? "男" : "女";
		} else {
			strGender = gender === 1 ? "male" : "female";
		}
	}

	var format = packageName + "," + weekDays.length + "," + duration + "," + deliveryStartDate + "," + deliveryEndDate;
o += "\r\n"; 
	if(strGender){
		format = format + "," + strGender;
o += "\r\n\t\t<span data-weave=\"widget/blurb/main(p1002)\" data-format=\"" +format+ "\">\r\n\t\t\tYou've committed to <span class=\"subscription-summary-fill\">{0} ({4}) {1}</span> days a week for the next <span class=\"subscription-summary-fill\">{2}</span> weeks, starting from <span class=\"subscription-summary-fill\">{3}</span>.\r\n\t\t</span>\r\n";
	} else {
o += "\r\n\t\t<span data-weave=\"widget/blurb/main(p1003)\" data-format=\"" +format+ "\">\r\n\t\t\tYou've committed to <span class=\"subscription-summary-fill\">{0} {1}</span> days a week for the next <span class=\"subscription-summary-fill\">{2}</span> weeks, starting from <span class=\"subscription-summary-fill\">{3}</span>.\r\n\t\t</span>\r\n";
	}
 return o; }; });
define('widget/subscription/main',["troopjs-browser/component/widget",
    "troopjs-browser/loom/weave",
    "jquery",
    "context",
    "widget/cookie/main",
    "widget/blurb/main",
    "widget/time/main",
    "widget/pageurl/main",
    "widget/infobox/main",
    "widget/troophash/main",
    "template!./index.html",
    "template!./summary.html",
    "underscore"
], function(Widget, weave, $, context, cookie, Blurb, Time, pageUrl, InfoBox, troopHash, template, templateSummary) {
    "use strict";

    var blurb = new Blurb($(document.body));
    // Time function
    var INS_TIME = new Time();

    var MONTH_WEEKS = 4;
    var MONTH_DURATION = 1;

    function parseStr(arr, sep) {
        if (!arr || arr.length <= 0) {
            return;
        }
        sep = sep || ",";
        var strArr = "";
        var iLens = arr.length;

        _.each(arr, function(e, i) {
            strArr += e;
            if (i < iLens - 1) {
                strArr += sep;
            }
        });

        return strArr;
    }

    return Widget.extend(function($element, widgetName) {
        var me = this;

        // Default plan
        me.gender = 2;
        me.weekDays = [1];
        me.deliveryStartDate = null;
        me.deliveryEndDate = null;
        me.duration = MONTH_DURATION * MONTH_WEEKS;
        // Initial package
        me.packageInfo = {};
        me.packageId = 0;
        me.packageName = "-";
        me.packageGroupId = 0;
        // Daily price, not contain addon
        me.dailyPrice = 0;
        me.addon = [];
    }, {
        "sig/start": function() {
            var me = this;

            // Render
            me.html(template, {
                "monthDuration": MONTH_DURATION
            }).then(function() {
                // Pre settings
                /*troopHash.extend({
                    "wdays": 1
                });*/
                me.publish("startdate/set", {
                    "weekDays": "1"
                });

                // Reset products
                me.publish("subscription/gender/render", me.gender);
            });
        },
        "hub/subscription/summary/update": function() {
            var me = this;
            var $me = me.$element;

            // Summary
            var $summary = $me.find("[data-action=summarycontainer]");
            $summary.html(templateSummary({
                "gender": me.gender,
                "weekDays": me.weekDays,
                "deliveryStartDate": INS_TIME.getStrDate(me.deliveryStartDate),
                "deliveryEndDate": INS_TIME.getStrDate(me.deliveryEndDate),
                "duration": me.duration,
                "packageId": me.packageId,
                "packageName": me.packageName,
                "language": context.language
            }));
            weave.apply($summary.find("[data-weave]"));

            // Amount
            var $amount = $me.find("[data-action=showtotalmoney]");
            var days = me.weekDays.length * me.duration;
            var dailyPrice = me.dailyPrice;
            if (me.addon.length > 0) {
                _.each(me.addon, function(e, i) {
                    dailyPrice += e.price;
                });
            }
            var amount = days * dailyPrice;
            $amount.html("&yen; " + amount);
        },
        // Plan
        "dom:[data-action=changeweekdays]/change": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            var $checkedWeekDays = $me.find("input[data-action=changeweekdays]:checked");
            me.weekDays = [];

            $checkedWeekDays.each(function(i, e) {
                var weekDay = parseInt(e.value, 10);
                me.weekDays.push(weekDay);
            });

            // Only first week day is available
            var startWeekDay = -1;

            if (me.weekDays.length > 0) {
                startWeekDay = me.weekDays.join(",");
            }
            // Reset startdate
            /*troopHash.extend({
                "wdays": startWeekDay
            });*/
            me.publish("startdate/set", {
                "weekDays": startWeekDay
            });

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/deliveryStartDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            me.deliveryStartDate = args.selectedDate;

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/deliveryEndDate/change": function(args) {
            var me = this;

            // Save delivery start date to widget
            var deliveryEndDate = me.deliveryEndDate = args.selectedDate;

            // Head + Tail = +1
            me.duration = INS_TIME.weekDiff(me.deliveryStartDate, deliveryEndDate) + 1;

            // Update
            me.publish("subscription/summary/update");
        },
        /*
        "dom:[data-action=changeduration]/change": function(e) {
            e.preventDefault();
            var me = this;

            // 4 weeks per month
            me.duration = e.currentTarget.value * MONTH_WEEKS;

            // Update
            me.publish("subscription/summary/update");
        },*/
        "hub/subscription/gender/update": function(gender) {
            var me = this;

            me.gender = gender;

            // Update dailyprice which related to gender
            if (me.packageId !== 0) {
                me.publish("subscription/package/update", me.packageInfo).then(function() {
                    // Update
                    me.publish("subscription/summary/update");
                });
            } else {
                // Update
                me.publish("subscription/summary/update");
            }
        },
        // Package
        "hub/subscription/packagecategory/change": function(packageCategoryId) {
            var me = this;
            var $me = me.$element;

            switch (packageCategoryId) {
                case 0:
                    me.packageInfo = {};
                    me.packageId = 0;
                    me.packageName = blurb.get("p1004");
                    me.packageGroupId = 0;
                    me.dailyPrice = 0; // TODO:
                    me.addon = [];

                    // Reset products
                    me.publish("subscription/customize/init").then(function() {
                        // Update
                        me.publish("subscription/summary/update");
                    });

                    break;
                default:
                    me.packageInfo = {};
                    me.packageId = 0;
                    me.packageName = "-";
                    me.packageGroupId = 0;
                    me.dailyPrice = 0;
                    me.addon = [];

                    // Reset Gender
                    me.publish("subscription/gender/render", me.gender);
                    // Reset Addon
                    me.publish("subscription/addon/reset");

                    break;
            }
        },
        "hub/subscription/addon/update": function(addon) {
            var me = this;

            me.addon = addon || [];

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/subscription/package/update": function(packageInfo) {
            var me = this;

            if (!packageInfo) {
                return;
            }

            me.packageInfo = packageInfo;
            me.packageId = packageInfo.Id;
            me.packageName = packageInfo.Title;
            me.packageGroupId = packageInfo.Groups[me.gender - 1].Id;
            // First day price as daily price
            me.dailyPrice = packageInfo.Groups[me.gender - 1].Rules[0].PackagePrice;

            // Update
            me.publish("subscription/summary/update");
        },
        "hub/subscription/customize/update": function(dailyProducts) {
            var me = this;
            var dailyPrice = 0;
            _.each(dailyProducts, function(e, i) {
                dailyPrice += e.Price * e.Count;
            });

            // Reset daily price
            me.dailyPrice = dailyPrice;
            // Set customize order data to cookie
            cookie.set("subscription_cust", JSON.stringify(dailyProducts), {
                path: "/"
            });

            // Update
            me.publish("subscription/summary/update");
        },
        // Submit
        "dom:[data-action=submitsubsciption]/click": function(e) {
            e.preventDefault();
            var me = this;
            var $me = me.$element;

            // Without day & duration
            if (!me.weekDays || me.weekDays.length <= 0 || me.duration <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: "1144",
                    content: "p1030"
                }).open();

                return;
            }
            // Without package/product
            if (me.dailyPrice <= 0) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: "1144",
                    content: "p1031"
                }).open();

                return;
            }

            var hash = "packageid/" + me.packageId +
                "/groupid/" + me.packageGroupId +
                "/weekDays/" + parseStr(me.weekDays) +
                "/deliveryStartDate/" + INS_TIME.getStrDate(me.deliveryStartDate, "-") +
                "/duration/" + me.duration;

            // Handle addon
            var i = 0;
            var iLens = 0;
            var addonIdArr = [];
            if (me.addon && me.addon.length > 0) {
                iLens = me.addon.length;
                for (i = 0; i < iLens; i++) {
                    addonIdArr.push(me.addon[i].id);
                }
                hash = hash + "/addon/" + parseStr(addonIdArr);
            }

            // Source Code
            hash += "/event/subs";

            pageUrl.goto("/order/receiver", hash);
        }
    });
});

define('troopjs-requirejs/template!widget/successenrollment/index.html',[],function() { return function template(data) { var o = "";
    var cacheServer = data.cacheServer;
o += "\r\n<div class=\"checklist\" style=\"overflow: hidden;\">\r\n    <h2><strong data-weave=\"widget/blurb/main(1165)\">Dear new V-Cleanser</strong></h2>\r\n    <p class=\"checklist-subtitle\" data-weave=\"widget/blurb/main(1166)\">Congrats! You are about to embark on a fun journey with us!</p>\r\n    <div class=\"line-hor\"></div>\r\n    <h6><strong data-weave=\"widget/blurb/main(1167)\">Who says living healthily is hard? In our eyes, it can be stylish, hip, and full of sparks.</strong></h6>\r\n    <hr>\r\n    <form class=\"form-inline\" role=\"form\">\r\n        <div class=\"form-group\">\r\n            <input type=\"email\" class=\"form-control\" id=\"signup-email\" name=\"signup-email\" placeholder=\"Subscribe to VCLEANSE\" required value=\"\" data-weave=\"widget/blurb/main(1404, 'placeholder')\">\r\n        </div>\r\n        <button type=\"submit\" class=\"btn btn-default\" data-weave=\"widget/blurb/main(1403)\">Subscribe</button>\r\n    </form>\r\n    <div class=\"checklist-link slide hid\" data-action=\"enrollmentsuccess-link\">\r\n        <p class=\"text-success\">\r\n            <span class=\"glyphicon glyphicon-ok-sign\"></span>\r\n            <span data-weave=\"widget/blurb/main(1405)\"></span>\r\n\t\t</p>\r\n        <p class=\"checklist-link-recommend m-hidden\">\r\n            <span>&rsaquo;</span>\r\n            <strong data-weave=\"widget/blurb/main(1168)\">Finish your VCleanse profile.</strong>\r\n        </p>\r\n        <p class=\"m-hidden\">\r\n            <span>&rsaquo;</span>\r\n            <span data-weave=\"widget/blurb/main(1374)\">Learn about our juices here.</span>\r\n        </p>\r\n        <p>\r\n            <span>&rsaquo;</span>\r\n            <span data-weave=\"widget/blurb/main(1169)\">Place your first order.</span>\r\n        </p>\r\n    </div>\r\n    <hr>\r\n    <div class=\"checklist-gap\">\r\n        <h6><strong data-weave=\"widget/blurb/main(1170)\">Questions?</strong></h6>\r\n        <p data-weave=\"widget/blurb/main(1171)\">We are just one WeChat away!</p>\r\n        <p>\r\n            <img src=\"" +cacheServer+ "/images/qrcode_service.png\" width=\"128\" height=\"128\" alt=\"\">\r\n        </p>\r\n    </div>\r\n    <div class=\"line-hor\"></div>\r\n    <p data-weave=\"widget/blurb/main(1172)\">Green is the new black. Health is the priceless luxury. Staying fit is the ultimate sexy.</p>\r\n</div>"; return o; }; });
define('widget/successenrollment/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/ajaxquery/main",
    "widget/api/main",
    "widget/infobox/main",
    "widget/basic/regex",
    "template!./index.html",
    "underscore"
], function(Widget, $, context, ajaxQuery, api, InfoBox, regex, template) {
    "use strict";

    var URI_ACCOUNT = api.get("account");
    var URI_EMAILCHECK = api.get("emailcheck");
    var URI_ACCOUNT_UPDATE = api.get("account_update");

    function getStatus(code) {
        var status = [];
        status["-1"] = "1406";

        return status[code] || "";
    }

    return Widget.extend(function() {
        var me = this;

        me.registerData = {};
    }, {
        "sig/start": function() {
            var me = this;
            var $me = me.$element;

            // Get user account info
            ajaxQuery({
                url: URI_ACCOUNT,
                type: "GET",
                dataType: "json"
            }, $me).then(function(data) {

                if(!data.FirstName){
                    data.FirstName = "";
                }
                if(!data.LastName){
                    data.LastName = "";
                }
                me.registerData = data;

                // Rendering
                me.html(template, {
                    "cacheServer": context.cacheServer
                }).then(function() {});
            });
        },
        "dom:form/submit": function(e) {
            e.preventDefault();

            var me = this;
            var $me = me.$element;

            var $form = $(e.currentTarget);
            var $successLink = $me.find("[data-action=enrollmentsuccess-link]");

            var email = _.trim($form.find("#signup-email").val());

            if(!regex.email(email)){
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: 1407
                }).open();
                
                return;
            }

            _.extend(me.registerData, {
                Email: email
            });

            ajaxQuery({
                url: URI_ACCOUNT_UPDATE,
                data: JSON.stringify(me.registerData),
                type: "POST",
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            }).then(function(data) {
                $form.hide();
                $successLink.show().removeClass("out").addClass("in");
            }, function(e) {
                // Show error msg as a lightbox
                new InfoBox({
                    title: 1144,
                    content: getStatus(e)
                }).open();
            });
        }
    });
});

define('troopjs-requirejs/template!widget/survey/index.html',[],function() { return function template(data) { var o = "";
  var survey = data;
  var surveyId = survey.SurveyId;
  var items = survey.ItemList;
  var itemLens = items.length;
o += "\r\n<div class=\"survey panel panel-default\">\r\n  <div class=\"panel-heading text-center\">\r\n    <h3 class=\"panel-title\">\r\n      " +survey.Title+ "<br>\r\n      "; if (survey.Remark) { o += "\r\n        <small>" +survey.Remark+ "</small>\r\n      "; } o += "\r\n    </h3>\r\n  </div>\r\n  <div class=\"panel-body\">\r\n    <form data-action=\"survey-form\" role=\"form\">\r\n      "; for(var i = 0 ; i<itemLens ; i++){
            renderItem(i,items[i],itemLens); 
      } o += "\r\n      <div class=\"form-group\">\r\n        <div class=\"text-center\">\r\n          <button type=\"submit\" class=\"btn btn-default btn-action\" data-weave=\"widget/blurb/main(1610)\">Submit</button>\r\n        </div>\r\n      </div>\r\n    </form>\r\n    <div class=\"form-group\" data-action=\"survey-report\"></div>\r\n  </div>\r\n</div>\r\n\r\n"; function renderItem(index, item, itemLens) { 
    var itemType = item.ItemTypeId;
    var itemId = item.ItemId;

    var detail = {};
    var j = 0;
    var jLens = 0;
o += "\r\n    <div class=\"form-group\" data-action=\"surveyitem\" data-id=\"" +itemId+ "\" data-type=\"" +itemType+ "\">\r\n      <h5>\r\n        ";if(itemLens>1){o += "<strong>" +(index+1)+ "</strong>";}o += "\r\n        " +item.ItemTitle + "\r\n        ";if(itemType === 1){ rendertitle();}o += "\r\n      </h5>\r\n        "; 
          if(itemType === 0) { 
            detail = item.ItemDetail;
            jLens = detail.length || 0;
            for(j = 0; j < jLens; j++) {
              renderSingleItem(itemId, j, detail[j]); 
            }
          } else if (itemType === 1){
            detail = item.ItemDetail;
            jLens = detail.length || 0;
            for(j = 0 ; j < jLens; j++) {
              renderMultiItem(itemId, j, detail[j]); 
            }  
          } else if (itemType === 2){
            renderTextItem();
          }
        o += "\r\n    </div>\r\n"; } o += "\r\n\r\n\r\n"; function renderSingleItem(itemId,index,detail) { o += "\r\n    <label class=\"radio-inline\">\r\n        <input type=\"radio\" name=\"inlineRadioOptions-" +itemId+ "\" value=\"" +detail.SelectId+ "\"> " +detail.Name+ "\r\n    </label>\r\n"; } o += "\r\n"; function renderMultiItem(itemId,index,detail) { o += "\r\n    <label class=\"checkbox-inline\">\r\n        <input type=\"checkbox\" name=\"inlineCheckboxOptions-" +itemId+ "\" value=\"" +detail.SelectId+ "\"> " +detail.Name+ "\r\n    </label>\r\n"; } o += "\r\n"; function renderTextItem() {o += "\r\n    <div class=\"inputtext\">\r\n          <textarea class=\"form-control\" rows=\"3\" name=\"inputtext\" placeholder=\"\" data-weave=\"widget/blurb/main(1612, 'placeholder')\"></textarea>\r\n     </div>\r\n"; } o += "\r\n"; function rendertitle() {o += "\r\n    <small data-weave=\"widget/blurb/main(1613)\">(Multiple Choice)</small>\r\n"; }  return o; }; });
define('widget/survey/main',["troopjs-browser/component/widget",
	"troopjs-browser/loom/weave",
	"jquery",
	"context",
	"when",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, weave, $, context, when, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_LOAD_SURVEY = api.get("load_survey");
	var URI_SAVE_SURVEY = api.get("save_survey");

	var ITEM_TYPE = {
		"SigleSelect": 0,
		"MultiSelect": 1,
		"Text": 2
	};

	return Widget.extend(function($element, widgetName, surveyId) {
		var me = this;

		me.surveyId = surveyId || 0;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_LOAD_SURVEY,
				data: JSON.stringify({
					"id": me.surveyId,
					"language": context.language
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}, $me).then(function(data) {
				if (data.Survey) {
					me.html(template, data.Survey);
				} else {
					$me.empty();
				}
			});
		},
		"collectPostDataItem": function($obj) {
			var me = this;

			if (!$obj) {
				return;
			}
			var itemid = $obj.data("id");
			var itemtype = $obj.data("type");
			var text = "";

			if (!itemid) {
				return;
			}

			if (itemtype === ITEM_TYPE.SigleSelect) {
				var $radio = $obj.find(".radio-inline > input");
				$radio.each(function(i) {
					if ($radio[i].checked) {
						text += $radio[i].value;
					}
				});
			} else if (itemtype === ITEM_TYPE.MultiSelect) {
				var $checkbox = $obj.find(".checkbox-inline > input");
				$checkbox.each(function(i) {
					if ($checkbox[i].checked) {
						text += $checkbox[i].value + ",";
					}
				});
				text = text.replace(/(.*)[,，]$/, '$1');
			} else if (itemtype === ITEM_TYPE.Text) {
				text = $obj.find(".inputtext > textarea").val();
			} else {
				return;
			}

			return text ? {
				"ItemId": itemid,
				"ItemTypeId": itemtype,
				"ResultText": text
			} : null;
		},
		"collectPostData": function() {
			var me = this;
			var $me = me.$element;
			var deferred = when.defer();

			var postData = [];

			var $obj = $me.find("[data-action=surveyitem]");

			$obj.each(function(i, e) {
				var $el = $(e);
				var dataItem = me.collectPostDataItem($el);
				if (dataItem) {
					postData.push(dataItem);
				}
			});

			if (postData.length > 0) {
				deferred.resolve(postData);
			} else {
				deferred.reject();
			}

			return deferred.promise;
		},
		"dom:form/submit": function(e) {
			e.preventDefault();

			var me = this;
			var $me = me.$element;

			me.collectPostData().then(function(postData) {
				var data = {
					"SurveyId": me.surveyId,
					"Details": postData
				};

				// Ajax Query
				ajaxQuery({
					url: URI_SAVE_SURVEY,
					data: JSON.stringify(data),
					type: "POST",
					contentType: "application/json; charset=UTF-8",
					dataType: "json"
				}).then(function(data) {
					var $report = $me.find("[data-action=survey-report]");
					var $form = $me.find("[data-action=survey-form]");

					$report.html("<div data-weave=\"widget/survey/report/main(surveyId)\" data-survey-id=\"" + me.surveyId + "\"></div>");
					weave.apply($report.find("[data-weave]")).then(function() {
						$form.hide();
					});
				});
			}, function() {

			});

		}
	});
});

define('troopjs-requirejs/template!widget/survey/report/index.html',[],function() { return function template(data) { var o = "";
	if(!data){
		renderThanks();
	} else {
		var reports= data;
		var i = 0;
		var iLens= reports.length;
		 for(i=0;i< iLens;i++){
				renderItem(i,iLens,reports[i]);
			} 
	}
o += "\r\n\r\n"; function renderItem(index,iLens,item){
		var j = 0;
		var lists= item.ItemList;
		var jLens = lists.length;
		var total = item.ItemTotal;
		var badge="";
		if(iLens>1){
			badge=(index+1);
		}
	o += "\r\n\t<div class=\"col-md-6\">\r\n\t\t<div class=\"panel panel-default\">\r\n\t\t\t<div class=\"panel-heading text-center\">\r\n\t\t\t\t<h3 class=\"panel-title\"><span class=\"badge\">" +badge+ "</span>&nbsp;" +item.ItemTitle+ "</h3>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"panel-body\">\r\n\t\t\t\t<form role=\"form\">\r\n\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t"; for(j = 0 ;j< jLens ; j++){
							renderProcess(lists[j],total);
						}o += "\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n"; } o += "\r\n\r\n"; function renderProcess(process,total){
		var result = (process.Count/total)*100;
	o += "\r\n\t<span>" +process.Name+ "</span>\r\n\t<div class=\"progress\">\r\n\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" +result+ "\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " +result+ "%;\r\n\t\t\t\">\r\n\t\t\t" +process.Persent+ "\r\n\t\t</div>\r\n\t</div>\r\n"; } o += "\r\n"; function renderThanks(){o += "\r\n\t <span data-weave=\"widget/blurb/main(1611)\"></span>\r\n";} return o; }; });
define('widget/survey/report/main',["troopjs-browser/component/widget",
	"jquery",
	"context",
	"when",
	"widget/api/main",
	"widget/ajaxquery/main",
	"widget/basic/regex",
	"widget/infobox/main",
	"template!./index.html"
], function(Widget, $, context, when, api, ajaxQuery, regex, InfoBox, template) {
	"use strict";

	var URI_SURVEY_REPORT = api.get("report_survey");

	return Widget.extend(function($element, widgetName, surveyId) {
		var me = this;

		me.surveyId = surveyId || 0;
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			ajaxQuery({
				url: URI_SURVEY_REPORT,
				data: JSON.stringify({
					"id": me.surveyId,
					"language": context.language
				}),
				type: "POST",
				contentType: "application/json; charset=UTF-8",
				dataType: "json"
			}, $me).then(function(data) {
				me.html(template, data.Report);
			}, function(e) {
				me.html(template);
			});
		}
	});
});

define('troopjs-requirejs/template!widget/swipe/index.html',[],function() { return function template(data) { var o = "";
    var swipeLens = data.swipeLens;
    var startSlide = data.startSlide;
    var bulletsPos = data.bulletsPos;
    var i = 0;
o += "\r\n<ul class=\"bullets hor\">\r\n    "; for(i=0;i<swipeLens;i++){o += "\r\n        <li class=\"bullets-item "; if (i === startSlide) { o += "on"; } o += "\">\r\n            <a>&bull;</a>\r\n        </li>\r\n    "; } o += "\r\n</ul>\r\n<style>\r\n    .bullets {\r\n        display: inline-block;\r\n        padding: 0;\r\n        margin: 0;\r\n        "; if (bulletsPos === "right") { o += "\r\n            position: absolute;\r\n            bottom: 5px;\r\n            right: 10px;\r\n        "; } else if (bulletsPos === "left") { o += "\r\n            position: absolute;\r\n            bottom: 5px;\r\n            left: 10px;\r\n        "; } o += "\r\n    }\r\n    .bullets > li {\r\n        list-style: none;\r\n        margin-left: 4px;\r\n    }\r\n    .bullets > li > a {\r\n        margin-top: 1px;\r\n        display: block;\r\n        width: 6px;\r\n        height: 6px;\r\n        overflow: hidden;\r\n        background-color: #fff;\r\n        border: 1px solid #666;\r\n        border-radius: 10px;\r\n        text-indent: -5000px;\r\n    }\r\n    .bullets > li.on > a {\r\n        margin-top: 0;\r\n        width: 8px;\r\n        height: 8px;\r\n        background-color: #333;\r\n        border-color: transparent;\r\n    }\r\n\r\n</style>"; return o; }; });
define('widget/swipe/main',["troopjs-browser/component/widget",
    "jquery",
    "template!./index.html",
    "swipe"
], function (Widget, $, template) {
    "use strict";

    return Widget.extend(function ($element, widgetName) {
        var me = this;
    }, {
        "sig/start": function () {
            var me = this;
            var $me = me.$element;

            var startSlide = parseInt($me.data("startSlide"), 10) || 0;
            var speed = parseInt($me.data("speed"), 10) || 300;
            var auto = parseInt($me.data("auto"), 10) || 0;
            var continuous = $me.data("continuous") || true;
            var disableScroll = $me.data("disableScroll") || false;
            var stopPropagation = $me.data("stopPropagation") || false;
            // center, left, right
            var bulletsPos = $me.data("bulletsPos") || "center";

            var swipeLens = $me.find(".swipe-wrap").children().length || 0;

            var $swipeBullets = $(template({
                "swipeLens": swipeLens,
                "startSlide": startSlide,
                "bulletsPos": bulletsPos
            }));
            $me.append($swipeBullets);
            var $bullets = $swipeBullets.find("> li.bullets-item");

            $me.Swipe({
                startSlide: startSlide,
                speed: speed,
                auto: auto,
                continuous: continuous,
                disableScroll: disableScroll,
                stopPropagation: stopPropagation,
                callback: function (i, e) {
                    $bullets.removeClass("on");
                    $bullets.eq(i).addClass("on");
                },
                transitionEnd: function (index, elem) {
                }
            });

        }
    });
});

define('troopjs-requirejs/template!widget/tips/holidaypromotion/index.html',[],function() { return function template(data) { var o = "";
    var isMobile = data.isMobile;
    var cacheServer = data.cacheServer;
    var language = data.language;
    var txt = language === "en" ? "GIFT<br>Detail&gt;&gt;" : "限时优惠<br>详细&gt;&gt;";
o += "\r\n<a class=\"link-fn package-days-promotion tips\" data-action=\"showpromotionrule\">\r\n    <!--img src=\"" + cacheServer + '/images/event/holidaypromotion/icon_gift.png'+ "\" width=\"20\" height=\"20\" alt=\"维果清祝您节日快乐！购买5天清体疗程，便获得一天礼品卡！\" title=\"维果清祝您节日快乐！购买5天清体疗程，便获得一天礼品卡！\"-->\r\n    <span class=\"tips-text\" title=\"维果清祝您节日快乐！购买5天清体疗程，便获得一天礼品卡！\">" +txt+ "</span>\r\n</a>"; return o; }; });
define('widget/tips/holidaypromotion/main',["troopjs-browser/component/widget",
    "jquery",
    "context",
    "widget/popupbox/main",
    "template!./index.html"
], function(Widget, $, context, PopupBox, template) {
    "use strict";

    var END_DATE = new Date("2015-02-19 00:00:00");

    return Widget.extend({
        "sig/start": function() {
            var me = this;

            var now = new Date();
            if(now >= END_DATE){
                return;
            }
            // Render
            me.html(template, {
                "isMobile": context.isMobile,
                "cacheServer": context.cacheServer,
                "language": context.language
            });
        },
        "dom:[data-action=showpromotionrule]/click": function(e) {
            e.preventDefault();

            var me = this;
            var path = context.isMobile ? "/images/event/holidaypromotion/mobile/rule.jpg" : "/images/event/holidaypromotion/rule.jpg";
            var $el = "<div><img src='/images/blank.png' data-src=" + path + " " +
                "data-weave='widget/basic/imageswitch(byLanguage)' data-by-language='true'" +
                (context.isMobile ? "width='100%'" : "width='800' height='306'") +
                "></div>";

            // Lightbox configuration
            var popupRule = new PopupBox({
                msg: $el,
                closeble: true,
                closeInner: true,
                zIndex: 10,
                // TODO:
                closeCallback: function() {}
            });

            // Manually weave template
            // TODO: Show lightbox first
            popupRule.open().then(function() {});

        }
    });
});
define('widget/warmingday/main',["troopjs-browser/component/widget",
	"jquery",
	"context",
	"widget/cookie/main",
	"widget/pageurl/main",
	"jquery.textchange"
], function(Widget, $, context, cookie, pageUrl) {
	"use strict";

	return Widget.extend(function() {
		var me = this;

		// gift / self
		me.typeCode = "gift";
	}, {
		"sig/start": function() {
			var me = this;
			var $me = me.$element;

			var $sense = $me.find(".warmingday-sense");
			if (context.language === "cn") {
				$sense.slideDown();
			} else {
				$sense.hide();
			}
		},
		"dom:a[data-action=buywarmingday]/click": function(e) {
			// Do not preventDefault
			var me = this;
			var $me = me.$element;

			var $blessing;
			var blessing = "";

			if (me.typeCode === "gift") {
				$blessing = $me.find("[data-action=inputblessing]");
				blessing = $blessing.val() || "";

				cookie.set("gift_info", JSON.stringify({
					"isGift": true,
					"blessing": blessing
				}), {
					expires: 7,
					path: "/"
				});
			} else {
				cookie.rm("gift_info", {
					path: "/"
				});
			}
		},
		"dom:#carousel-example-generic/slide.bs.carousel": function() {
			var me = this;
			var $me = me.$element;

			var index = $me.find(".carousel-indicators li.active").index();
			var $sense = $me.find(".warmingday-sense");
			var $control = $me.find("#carousel-example-generic .carousel-control");
			var lang = context.language;

			// Self
			if (index === 0) {
				$control.eq(0).fadeIn();
				$control.eq(1).hide();
				$sense.slideUp();

				// IsGift?
				me.typeCode = "self";
			}
			// Gift
			else {
				$control.eq(0).hide();
				$control.eq(1).fadeIn();
				if (lang === "cn") {
					$sense.slideDown();
				} else {
					$sense.slideUp();
				}

				// IsGift?
				me.typeCode = "gift";
			}
		}
	});
});
