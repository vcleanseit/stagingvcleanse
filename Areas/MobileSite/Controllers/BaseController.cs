﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VCleanse.Shop.Common;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;

namespace VCleanse.Shop.Host.Areas.MobileSite.Controllers
{
    public class BaseController : Controller
    {
        // GET api/<controller>
        protected VcleanseContext CurrentContext
        {
            get
            {
                return VcleanseContext.Current;
            }
        }


        protected int CurrentMember_id
        {
            get
            {
                return CurrentContext == null ? 0 : CurrentContext.Member_id;
            }
        }


        #region Json result
        protected JsonResult CreateSuccessfulGetResult()
        {
            return Json(new { IsSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        protected JsonResult CreateFailedGetResult()
        {
            return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
        }

        protected JsonResult CreateFailedGetResult(int statusCode, string message)
        {
            return Json(new { IsSuccess = false, StatusCode = statusCode, Description = message }, JsonRequestBehavior.AllowGet);
        }

        protected JsonResult CreateFailedGetResult(int statusCode, string format, params object[] args)
        {
            return CreateFailedGetResult(statusCode, string.Format(format, args));
        }

        protected JsonResult CreateSucessedPostJsonResult()
        {
            return CreateSucessedPostJsonResult(string.Empty);
        }

        protected JsonResult CreateSucessedPostJsonResult(string message)
        {
            return Json(new { IsSuccess = true, StatusCode = Constants.GeneralStatus.Success, Description = message });
        }

        protected JsonResult CreateFailedPostJsonResult(int statusCode, string message)
        {
            return Json(new { IsSuccess = false, StatusCode = statusCode, Description = message });
        }

        protected JsonResult CreateFailedPostJsonResult(int statusCode, string format, params object[] args)
        {
            return CreateFailedPostJsonResult(statusCode, string.Format(format, args));
        }

        protected JsonResult CreateFailedPostJsonResult()
        {
            return Json(new { IsSuccess = false });
        }
        #endregion

        protected bool IsChinese(string language)
        {
            return LanguageHelper.IsChinese(language);
        }

        #region Token
        protected string CreateToken(int member_id)
        {
            DateTime now = DateTime.UtcNow;
            string tokenString = string.Format("{0};{1}", member_id, DateTimeUtility.FormatDate(now, DateTimeUtility.DEFAULT_LONG_DATE_PATTERN, false));
            return Security.Encrypt(tokenString);
        }

        protected bool IsTokenValid(string token, int member_id)
        {
            try
            {
                string decString = Security.Decrypt(token);
                string[] parsedStrings = decString.Split(';');
                if (parsedStrings.Length != 2 || !string.Equals(member_id, parsedStrings[0]))
                {
                    return false;
                }
                DateTime? time = DateTimeUtility.Parse(parsedStrings[1], DateTimeUtility.DEFAULT_LONG_DATE_PATTERN);
                if (time == null)
                {
                    return false;
                }
                else
                {
                    return time.Value.AddMinutes(Constants.Config.TokenValidMinutes) > DateTime.UtcNow;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}