﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.SessionTracking;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;

namespace VCleanse.Shop.Host.Areas.MobileSite.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Index()
        {
            return View("Signin");
        }
        public ActionResult Signin()
        {
            return View("Signin");
        }
        public ActionResult Register() {
            return View("Register");
        }

        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Id);
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Session);
            return RedirectToAction("home", "intl");
        }

        [HttpPost]
        public ActionResult Logon(string email, string password)
        {
            NLogHelper.LogInfo("Signin", "Email:" + email + ", Password:" + password );

            if (ModelState.IsValid)
            {
                VUsersBLL userBLL = new VUsersBLL();
                //登陆者的ID loginID
                int loginID = userBLL.VUSERLOGIN(email, password, HttpHelper.GetIp(HttpContext), WebConstants.LogMessage.LogonViaWebSite);

                if (loginID == 0)
                {
                    return CreateFailedPostJsonResult(Constants.LogonStatus.InvalidUserNameOrPassword, "Invalid username or password");
                }
                else
                {
                    var memberInfo = InitMemberInfo(loginID);
                    //写入session
                    SessionTrackingService.SaveSessionUser(VcleanseContext.Current.Session_id, memberInfo.Id);
                    NLogHelper.LogInfo("SigninSuccess", "Email:" + email + ", Password:" + password );

                    return Json(new { IsSuccess = true, Id = memberInfo.Id, Name = memberInfo.FirstName });
                }
            }
            return null;
        }


        public VCleanse.Shop.Model.Members.MemberInfo InitMemberInfo(int memberId)
        {
            var memberInfo = VUsersBLL.LoadMemberInfo(memberId, false);
            HttpHelper.WriteCookie(HttpContext, WebConstants.CookieKey.Id, memberInfo.Id.ToString(), true);
            HttpHelper.WriteCookie(HttpContext, WebConstants.CookieKey.Name, memberInfo.FirstName, false);
            return memberInfo;
        }
    }
}