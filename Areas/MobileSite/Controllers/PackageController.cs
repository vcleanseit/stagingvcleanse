﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Packages;
using VCleanse.Shop.Common;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Model.Packages;
using VCleanse.Shop.Model.Shared;

namespace VCleanse.Shop.Host.Areas.MobileSite.Controllers
{
    public class PackageController : BaseController
    {
        /// <summary>
        /// 获取所有套餐信息
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Package")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllPackages(string language)
        {
            object package = LoadPackageInner(language, null, null);
            return Json(new { IsSuccess = true, Packages = package }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取所有套餐信息
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("PackageByPackageID")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadPackagesByPackageID(string language, int packageId)
        {
            object package = LoadPackageInner(language, null, packageId);
            return Json(new { IsSuccess = true, Packages = package }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// 加载套餐内容
        /// </summary>
        /// <param name="language"></param>
        /// <param name="categoryId"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public static object LoadPackageInner(string language, int? categoryId, int? packageId)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            var packageList = PackageBLL.LoadAllPackages();
            IEnumerable<PackageInfo> packages = packageList;

            if (packageId != null)
            {
                packages = packageList.Where(item => item.ID == packageId);
            }
            else
            {
                packages = packageList.Where(item => item.IsActive.Equals(true));
            }

            if (categoryId != null)
            {
                packages = packages.Where(item => item.CategoryId == categoryId);
            }

            var packageTypesDict = PackageBLL.LoadPackageGroupTypes();
            var products = PackageBLL.LoadAllProducts();
            object package = packages.Select(item => new
            {
                Id = item.ID,
                Title = isChinese ? item.Name : item.ENName,
                SubTitle = isChinese ? item.Title : item.ENTitle,
                Description = isChinese ? item.Description : item.ENDescription,
                Url = isChinese ? item.CNUrl : item.ENUrl,
                Tips = isChinese ? item.Tips : item.ENTips,
                CategoryId = item.CategoryId,
                Introduction = isChinese ? item.Introduction : item.ENIntroduction,
                ImageName = isChinese ? item.ImageName : item.ENImageName,
                Tags = item.Tags.Select(tagItem => new
                {
                    TagId = tagItem.Id,
                    Name = isChinese ? tagItem.Name : tagItem.EnName,
                    ImagePath = tagItem.ImagePath,
                }),
                Groups = item.PackageGroups.Select(groupItem => new
                {
                    Id = groupItem.ID,
                    Name = GetGroupTypeString(packageTypesDict, isChinese, groupItem.GroupTypeID),
                    GroupTypeId = groupItem.GroupTypeID,
                    ByWeek = groupItem.IsByWeek,
                    Rules = groupItem.DayRules.OrderBy(rule => rule.Days)
                        .SelectMany(dayRuleItem => dayRuleItem.GroupRules.OrderBy(dayItem => dayItem.RuleValue)
                            .Select(ruleItem => new
                            {
                                Id = ruleItem.ID,
                                Day = ruleItem.RuleValue,
                                Name = (isChinese ? item.Name : item.ENName) + " " + ruleItem.RuleValue.ToString(),
                                PackagePrice = ruleItem.PackagePrice,
                                Products = ruleItem.RuleDetail.Join(products, detailItem => detailItem.ProductID
                                    , productItem => productItem.ProductsID
                                    , (detailItem, productItem) => new
                                    {
                                        Count = detailItem.Count,
                                        Id = productItem.ProductsID,
                                        Name = isChinese ? productItem.ProductsName : productItem.ENProductsName,
                                        Image = productItem.ProductsImage,
                                        Price = productItem.ProductsPrice,
                                        Url = isChinese ? productItem.CNUrl : productItem.ENUrl,
                                        MaxCount = productItem.ProductsMaxN,
                                        Description = isChinese ? productItem.Description : productItem.ENDescription,
                                    }
                                    ),
                            })
                                )
                }),
            });
            return package;
        }

        /// <summary>
        /// 套餐类别名称
        /// </summary>
        /// 
        private static string GetGroupTypeString(Dictionary<byte, KeyValueType> dict, bool isChinese, byte key)
        {
            if (dict.ContainsKey(key))
            {
                var item = dict[key];
                return isChinese ? item.Name : item.ENName;
            }
            return string.Empty;
        }
    }
}