﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.Promotion;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Data;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.PaymentService;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model._Legacy.Orders;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Packages;
using VCleanse.Shop.Model.Promotions;

namespace VCleanse.Shop.Host.Areas.MobileSite.Controllers
{
    public class OrderController : BaseController
    {
        public ActionResult Index()
        {
            return View("OrderBuy");
        }
        public ActionResult OrderBuy()
        {
            if (CurrentMember_id == Config.DefaultCustomerId)
            {
                ViewBag.IsSuccess = false;
            }
            else
            {
                MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
                ViewBag.IsSuccess = true;
                ViewBag.Receivers = memberInfo.Receivers;
            }

            return View("OrderBuy");
        }


        public ActionResult OrderConfirm()
        {
            string orderID = Request.QueryString["orderID"];
            string result = "OK";
            int resultId = 0;
            decimal fee = 0;
            var orderInfo = PaymentHelper.getOrderInfo(int.Parse(orderID));
            if (orderInfo != null)
            {
                if (PaymentHelper.CheckOrderCanPayStatus(orderInfo))
                {
                    fee = PaymentHelper.getPaymentPrice(orderInfo);
                    if (fee < 10)
                    {
                        result = "支付价格不对";
                        resultId = PaymentHelper.PayErrorMsg.PRICE;
                    }
                }
                else
                {
                    result = "支付订单已经过期";
                    resultId = PaymentHelper.PayErrorMsg.ESPIRED;
                }
            }
            else
            {
                result = "支付订单不存在";
                resultId = PaymentHelper.PayErrorMsg.NOTEXISTS;
            }
            ViewBag.Price = fee;
            ViewBag.Result = result;
            ViewBag.ResultId = resultId;
            return View("orderConfirm");
        }
        public ActionResult OrderPayForWX()
        {
            string openid = Request.QueryString["openid"];
            string orderID = Request.QueryString["orderID"];
            var orderInfo = PaymentHelper.getOrderInfo(int.Parse(orderID));
            string result = "OK";
            int resultId = 0;
            if (orderInfo != null)
            {
                if (PaymentHelper.CheckOrderCanPayStatus(orderInfo))
                {
                    decimal fee = PaymentHelper.getPaymentPrice(orderInfo);
                    if (fee < 10)
                    {
                        result = "支付价格不对";
                        resultId = PaymentHelper.PayErrorMsg.PRICE;
                    }
                }
                else
                {
                    result = "支付订单已经过期";
                    resultId = PaymentHelper.PayErrorMsg.ESPIRED;
                }
            }
            else
            {
                result = "支付订单不存在";
                resultId = PaymentHelper.PayErrorMsg.NOTEXISTS;
            }
            ViewBag.openid = openid;
            ViewBag.orderID = orderID;
            ViewBag.Result = result;
            ViewBag.ResultId = resultId;
            ViewBag.Price = orderInfo.PreferentialPrice+orderInfo.PreferentialLogisticsFee;
            return View("orderPayForWX");
        }

        public ActionResult OrderFinished()
        {
            string orderID = Request.QueryString["orderID"];
            if (orderID != "" && orderID != null && orderID != "undefined") {
                int i = OrdersBLL.Mobile_FinishedOrder(int.Parse(orderID));
            };
            return View("orderFinished");
        }

        #region <<创建订单>>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult CreateOrder(Mobile_CreateOrder_Client Order_C)
        {
            #region <<验证客户端订单信息正确性>>

            //写入日志
            NLogHelper.LogInfo("OrderParameters", new JavaScriptSerializer().Serialize(Order_C));
            if (Order_C == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            if (Order_C.PackageID == 0 || Order_C.PackageRuleID == 0 || Order_C.PackageRuleCount == 0 || Order_C.OrderPrice == 0 || Order_C.DeliverList == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }

            #endregion


            #region <<整理成数据库要的数据>>
            Mobile_CreateOrder_Server Order_S = new Mobile_CreateOrder_Server();
            Order_S.RuldID = Order_C.PackageRuleID;
            Order_S.OrderPrice = Order_C.OrderPrice;
            Order_S.OrdersVUserID = CurrentContext.IsUserLogined ? CurrentMember_id : Config.DefaultCustomerId;
            Order_S.SessionID = CurrentContext.IsUserLogined ? CurrentContext.Session_id : null;

            Order_S.SubOrderXML = GetSubOrderXML(Order_C.DeliverList);
            Order_S.SubOrderDetailXML = OrdersBLL.GetSubOrderDetailXML(Order_C.PackageRuleID);

            int OrderID = OrdersBLL.Mobile_CreateOrderMain(Order_S);

            return Json(new { IsSuccess = true, Order_id = OrderID });

            #endregion
        }

        /// <summary>
        /// 获取订单信息
        /// </summary>
        [HttpGet]
        public ActionResult OrderInfo(int OrderID)
        {
            Mobile_OrderBuyInfo orderInfo = new Mobile_OrderBuyInfo();

            orderInfo = OrdersBLL.Mobile_GetOrderInfo(OrderID);
            if (orderInfo != null)
            {
                return Json(new { IsSuccess = true, OrderInfo = orderInfo }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
            }

        }


        /// <summary>
        /// 计算价格
        /// </summary>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult Summary(Mobile_SummaryInfo summary)
        {

            NLogHelper.LogInfo("Summary", new JavaScriptSerializer().Serialize(summary));
            if (summary == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            else if (summary.OrderID == 0)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }

            int i = ValidatePromotion(summary.OrderID, summary.PromotionCode);
            summary.PromotionCode = i == 0 ? summary.PromotionCode : null;

            Mobile_OrderPriceInfo oPrice = new Mobile_OrderPriceInfo();
            oPrice = OrdersBLL.Mobile_Summary(summary);
            return Json(new { IsSuccess = true, SummaryInfo = oPrice, status = i });
        }

        /// <summary>
        /// 验证优惠码能否使用
        /// </summary>
        public int ValidatePromotion(int OrderID, string Code)
        {
            PromotionRuleInfo promotionRule = null;
            if (Code != null && Code != "undefined" && Code != "")
            {
                promotionRule = PromotionBLL.LoadAllPromotionRulesByCode(Code);
            }

            string message = "恭喜您成功使用优惠码";
            int i = 0;
            if (promotionRule == null)
            {
                i = Constants.Promotion.NotExists;
                message = "无此优惠码";
                return i;
            }
            if (promotionRule.ActiveFrom > DateTime.Now)
            {
                i = Constants.Promotion.NotStart;
                message = "此优惠码活动还未开始";
                return i;
            }
            if (promotionRule.ActiveTo < DateTime.Now)
            {
                i = Constants.Promotion.ExpiredCode;
                message = "此优惠码已过期";
                return i;
            }
            if (!promotionRule.IsActive)
            {
                i = Constants.Promotion.HasUsed;
                message = "此优惠码已被使用";
                return i;
            }

            var orderInfo = OrdersBLL.LoadOrderById(OrderID);
            if (orderInfo.TotalPrice < promotionRule.StartAmount)
            {
                i = Constants.Promotion.InvalidStartAmount;
                message = "订单价格未满足优惠条件";
                return i;
            }
            return i;
        }


        public ActionResult OrderMessage(int orderId, string Comments, string Weixin)
        {
            int memberID = CurrentContext.IsUserLogined ? CurrentMember_id : Config.DefaultCustomerId;
            int resultId = 0;
            if (orderId > 0)
            {
                resultId = OrdersBLL.Mobile_OrderMessage(orderId, Comments, Weixin, memberID);
            }

            return Json(new { IsSuccess = resultId > 0 ? true : false }, JsonRequestBehavior.AllowGet);

        }


        /// <summary>
        /// 确认订单信息
        /// </summary>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult OrderConfirm(Mobile_ConfirmOrder ConfirmInfo)
        {
            if (ConfirmInfo == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            else if (ConfirmInfo.OrderID == 0 || ConfirmInfo.PersonName == null || ConfirmInfo.Telephone == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            var orderInfo = OrdersBLL.LoadOrderById(ConfirmInfo.OrderID);
            if (orderInfo.OrdersVUserID != Config.DefaultCustomerId && orderInfo.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "请先登录您自己的账号");
            }
            //验证送货地址是否有效
            if (OrdersBLL.CheckAreaIsActive(ConfirmInfo.AreaID).Equals(false))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.AreaIsNotActive, "The area is not available");
            }
            ConfirmInfo.OrdersVUserID = CurrentContext.IsUserLogined ? CurrentMember_id : Config.DefaultCustomerId;
            int i = ValidatePromotion(ConfirmInfo.OrderID, ConfirmInfo.PromotionCode);
            ConfirmInfo.PromotionCode = i == 0 ? ConfirmInfo.PromotionCode : null;

            if (OrdersBLL.Mobile_ConfirmOrder(ConfirmInfo) > 0)
            {
                if (ConfirmInfo.InvoiceType != 0)
                {
                    var PreferentialPrice = OrdersBLL.LoadOrderById(ConfirmInfo.OrderID).PreferentialPrice;
                    InvoiceInfo invoiceInfo = new InvoiceInfo();
                    invoiceInfo.InvoiceType = ConfirmInfo.InvoiceType;
                    invoiceInfo.OrderId = ConfirmInfo.OrderID;
                    invoiceInfo.Title = ConfirmInfo.Title;
                    invoiceInfo.Amount = PreferentialPrice;
                    int invoiceId = OrdersBLL.SaveInvoice(invoiceInfo);
                }
                return Json(new { IsSuccess = true, OrderID = ConfirmInfo.OrderID });
            }
            else
            {
                return Json(new { IsSuccess = false, message = "失败，请重试" });
            }



        }


        /// <summary>
        /// 将DeliverList转成数据库所需要的数据类型
        /// </summary>
        /// <param name="DeliverList"></param>
        /// <returns></returns>
        public string GetSubOrderXML(string DeliverList)
        {
            DeliverList = DeliverList.Substring(0, DeliverList.Length - 1);
            string[] date = DeliverList.Split(',');
            string SubOrderXML = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            ds.Tables.Add(dt);
            dt.Columns.Add("DeliverTime");
            dt.Columns.Add("OrderIndex");
            for (int i = 0; i < date.Length; i++)
            {
                DataRow drow = dt.NewRow();
                drow["DeliverTime"] = date[i];
                drow["OrderIndex"] = (i + 1);
                ds.Tables[0].Rows.Add(drow);
            }

            SubOrderXML = ds.GetXml();
            return SubOrderXML;

        }



        #endregion
    }
}