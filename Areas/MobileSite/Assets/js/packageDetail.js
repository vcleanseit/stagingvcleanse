/**
 * index.html base js
 */
// Call this from the developer console and you can control both instances
var calendars = {};
var packagedetail = null;
var sex = 1;
var PackageRuleID = null;
var Dateslength = 3;
$(document).ready(function () {
    var ID = getQueryString("packageId");
    var postValue = {
        //TODO:
        //语言要获取，这里只是测试
        "language": "cn",
        "packageId": ID
    }
    $.ajax({
        url: "../Package/PackageByPackageID",
        data: postValue,
        dataType: 'json',
        type: 'get',
        async: false,
        success: function (data) {
            //数据加载

            packagedetail = data;
            LoadData();

        }, failure: function () {

        }
    })
    /**
	 * Slider
	 */

   $('.banner').unslider({
		autoplay:true,
        speed: 500,               //  The speed to animate each slide (in milliseconds)
        delay: 5000,              //  The delay between slide animations (in milliseconds)
        complete: function () { },  //  A function that gets called after every slide animation
        keys: true,               //  Enable keyboard (left, right) arrow shortcuts
        dots: true,               //  Display dot navigation
        fluid: false,              //  Support responsive design. May break non-responsive designs
        nav:true,
        arrows:false
    });

    /**
	 * 底价格选择器
	 */

    $(document).scroll(function (e) {
        //console.log($(this).height() + ":" + $(window).height() + ":" + $(this).scrollTop());
        //console.log($(this).height() - $(this).scrollTop() - $(window).height());
        //if($(this).scrollTop()/$(this).height()>0.76){
        if ($(this).height() - $(this).scrollTop() - $(window).height() < 120) {
            $(".lay-order-frm").removeClass("fixed");
        } else {
            $(".lay-order-frm").addClass("fixed");
        }
    })
    /**
	 * 男女选择
	 */
    $('.sex-selector span').click(function (e) {
        $('.sex-selector span').removeClass("active");
        $(e.target).addClass('active');
        if ($('.sex-selector span.active').html() == "男士") {
            sex = 0;
        } else {
            sex = 1;
        };
        LoadData();
    })

    /**
	 * tab切换
	 */
    $(".detail-tab li").click(function (e) {
        $(".detail-tab li").removeClass("active");
        $(this).addClass("active");
        $(".product-list").hide();
        $(".recommend").hide();
        $("." + $(this).attr("data-tab")).show();
    })

    /**
	 * 日期相关
	 */
    // 设置Combo控件日期
    function setDates(arrDates) {
        $("#cleanse-dates").html("");
        for (var i = 0; i < arrDates.length; i++) {
            var currDate = arrDates[i].date;
            $("#cleanse-dates").append("<option >" + currDate + "</option>");
        }
        $("#drink-days").html(arrDates.length);
        //DeliverList = arrDates;
        var p = $("#date_PackagePrice").html();
        Dateslength = arrDates.length;
        var price = p * Dateslength;
        $("#price").html(price);
        $(".drink").show();
    }
    function getDates() {
        var opts = $("#cleanse-dates").children();
        var arrDates = [];
        for (var i = 0; i < opts.length; i++) {
            arrDates.push({ date: $(opts[i]).html() });
        }
        return arrDates;
    }
    $(".cal-close").click(function (e) {
        $(".lay-cal").hide();
    });

    $("#btn-dates").click(function (e) {
        var arrDates = getDates();
        if (arrDates.length == 0) {
            $("#btn-calconfirm").attr("disabled", "disabled");
        } else {
            $("#btn-calconfirm").removeAttr("disabled");
        }
        calendars.clndr.setEvents(arrDates);
        //$(".lay-cal").css("top",$(".content").scrollTop());
        $(".content").scrollTop(0);
        $(".content").css("overflow", "hidden");
        $(".lay-cal").show();
    })
    // Clndr日历控件

    $("#btn-calconfirm").click(function (e) {
        var selectedDates = calendars.clndr.options.events;
        // 放到Combolist里
        setDates(selectedDates);

        $(".lay-cal").hide();
    });

    // 日期模板
    var clndrTemplate =
        "<div class='clndr-controls'>" +
            "<div class='clndr-control-button clndr-previous-button'>" +
                "<span class=''><</span>" +
            "</div>" +
            "<div class='month'><%= year %>年<%= month %>月</div>" +
            "<div class='clndr-control-button rightalign clndr-next-button'>" +
                "<span class=''>></span>" +
            "</div>" +
        "</div>" +
        "<table class='clndr-table' border='0' cellspacing='0' cellpadding='0'>" +
            "<thead>" +
                "<tr class='header-days'>" +
                "<% for(var i = 0; i < daysOfTheWeek.length; i++) { %>" +
                    "<td class='header-day'><%= daysOfTheWeek[i] %></td>" +
                "<% } %>" +
                "</tr>" +
            "</thead>" +
            "<tbody>" +
            "<% for(var i = 0; i < numberOfRows; i++){ %>" +
                "<tr>" +
                "<% for(var j = 0; j < 7; j++){ %>" +
                "<% var d = j + i * 7; %>" +
                    "<td class='<%= days[d].classes %>'>" +
                        "<div class='day-contents'><%= days[d].day %></div>" +
                    "</td>" +
                "<% } %>" +
                "</tr>" +
            "<% } %>" +
            "</tbody>" +
            "</table>";
    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.locale('ru');

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    // Events to load into calendar
    // 默认日期
    var eventArray = [];

    

    // The order of the click handlers is predictable. Direct click action
    // callbacks come first: click, nextMonth, previousMonth, nextYear,
    // previousYear, nextInterval, previousInterval, or today. Then
    // onMonthChange (if the month changed), inIntervalChange if the interval
    // has changed, and finally onYearChange (if the year changed).
    var startDate;//当前时间在12点之前日期从明天开始，12点之后，从后天开始
    if(moment().hours() < 12){ 
    	startDate = moment().add(1, 'days').format('YYYY-MM-DD');
    	eventArray = [{
            date: moment().add(1, 'days').format('YYYY-MM-DD')
        }, {
            date: moment().add(2, 'days').format('YYYY-MM-DD')
        }, {
            date: moment().add(3, 'days').format('YYYY-MM-DD')
        }];
    }else{
    	startDate = moment().add(2, 'days').format('YYYY-MM-DD');
    	eventArray = [{
            date: moment().add(2, 'days').format('YYYY-MM-DD')
        }, {
            date: moment().add(3, 'days').format('YYYY-MM-DD')
        }, {
            date: moment().add(4, 'days').format('YYYY-MM-DD')
        }];
    }
    setDates(eventArray);
    calendars.clndr = $('.cal-selection').clndr({
        daysOfTheWeek: ['日', '一', '二', '三', '四', '五', '六'],
        template: clndrTemplate,
        events: eventArray,
        constraints: {
            startDate: startDate
        },
        clickEvents: {
            click: function (target) {
            	if($(target.element).hasClass('inactive') == false){
	            	var currEvent = target.date.format("YYYY-MM-DD");
	                
	                if (target.events.length > 0) {
	                    this.removeEvents(function (event) {
	                        return event.date == currEvent;
	                    });
	                } else {
	                    // ：对限制日期的判断
	                    this.addEvents([{ date: currEvent }]);
	                }
	                if (this.options.events.length > 0) {
	                    $("#btn-calconfirm").removeAttr("disabled");
	                } else {
	                    $("#btn-calconfirm").attr("disabled", "disabled");
	                }
            	}

            },
            today: function () {
                console.log('Cal-1 today');
            },
            nextMonth: function () {
                console.log('Cal-1 next month');
            },
            previousMonth: function () {
                console.log('Cal-1 previous month');
            },
            onMonthChange: function () {
                console.log('Cal-1 month changed');
            },
            nextYear: function () {
                console.log('Cal-1 next year');
            },
            previousYear: function () {
                console.log('Cal-1 previous year');
            },
            onYearChange: function () {
                console.log('Cal-1 year changed');
            },
            nextInterval: function () {
                console.log('Cal-1 next interval');
            },
            previousInterval: function () {
                console.log('Cal-1 previous interval');
            },
            onIntervalChange: function () {
                console.log('Cal-1 interval changed');
            }
        },
        multiDayEvents: {
            singleDay: 'date',
            endDate: 'endDate',
            startDate: 'startDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false
    });
})
///加载PackageDetail数据
function LoadData() {
    var banner = "";
    banner += '<ul>';
    banner += '<li><img src="/Areas/MobileSite/Assets/images/packagedetailbanner1/' + packagedetail.Packages[0].ImageName + '"></li>';
    banner += '<li><img src="/Areas/MobileSite/Assets/images/packagedetailbanner2/' + packagedetail.Packages[0].ImageName + '"></li>';
    banner += '</ul>';
    $("#banner").html(banner);

    if (packagedetail.Packages[0].Groups.length == 1) {
        sex = 0;
    }
    $("#date_Title").html(packagedetail.Packages[0].Title);
    $("#date_PackagePrice").html(packagedetail.Packages[0].Groups[sex].Rules[0].PackagePrice);
    $("#date_Description").html(packagedetail.Packages[0].Description);

    var html = "";
    for (var i = 0; i < packagedetail.Packages[0].Groups[sex].Rules[0].Products.length; i++) {
        html += "<div class='product'>";
        html += "<img src='/Areas/MobileSite/Assets/images/products/" + packagedetail.Packages[0].Groups[sex].Rules[0].Products[i].Image + "' alt=''>";
        html += "<div class='no'><div>" + (i + 1) + "</div></div>";
        html += "<h2 class='name'><span>" + packagedetail.Packages[0].Groups[sex].Rules[0].Products[i].Name + "</span></h2>";
        html += "<div class='desc'>";
        html += "<label>介绍：</label>";
        html += "<p>" + packagedetail.Packages[0].Groups[sex].Rules[0].Products[i].Description + "</p>";
        html += "</div>";
        html += "</div>";
    }
    $("#products").html(html);
    var p = $("#date_PackagePrice").html();
    var price = p * Dateslength;
    $("#price").html(price);

    PackageRuleID = packagedetail.Packages[0].Groups[sex].Rules[0].Id;

}

$("#Buy").click(
    function () {
        DeliverList = "";
        var dates = $("#cleanse-dates").children();
        for (var i = 0; i < dates.length; i++) {
            DeliverList = DeliverList + $(dates[i]).html() + ",";
        }

        var postValue = {
            //TODO:
            //语言要获取，这里只是测试
            "OrderPrice": $("#price").html(),
            "PackageID": getQueryString("packageId"),
            "PackageRuleID": PackageRuleID,
            "PackageRuleCount": 1,
            "DeliverList": DeliverList
        }
        $.ajax({
            url: "../Order/CreateOrder",
            data: postValue,
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {
                window.location.href = "../order/orderbuy?OrderID=" + data.Order_id + "&DeliverData=" + DeliverList;
            }, failure: function () {

            }
        })

    })



