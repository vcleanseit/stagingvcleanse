
$(document).ready(function () {
    $(".switch-button").click(function (e) {
        if (!$(this).hasClass("disabled")) {
            if ($(this).hasClass("on")) {
                $(this).removeClass("on");
                $(this).addClass("off");
            } else {
                $(this).removeClass("off");
                $(this).addClass("on");
            }
        }
    })

    $("#swc-coupon").click(function (e) {
        if ($(this).hasClass("off")) {
            $("#coupon-box").hide();
        } else {
            $("#coupon-box").show();
        }
    })

    $("#swc-delivery").click(function (e) {
        if ($(this).hasClass("off")) {
            $("#delivery-express").hide();
        } else {
            $("#delivery-express").show();
        }
    })

    $("#swh-invoice").click(function (e) {
        if ($(this).hasClass("off")) {
            $("#swh-invoice-type").hide();
            $("#invoice-title").hide();
        } else {
            $("#swh-invoice-type").show();
            $("#sle-invoice-type").val("personal");
            $("#invoice-title").hide();
        }
    })
    $("#sle-invoice-type").change(function (e) {
        if ($(this).val() == 'personal') {
            $("#invoice-title").hide();
        } else {
            $("#invoice-title").show();
        }
    })






    $(".add-close").click(function (e) {
        $(".address-form").hide();
    })

    $("#btn-addconfirm").click(function (e) {

    })

    $(".pay-list li").click(function (e) {
        $(".pay-list li").removeClass("active");
        $(this).addClass("active");
    })

    $(".switch-otherpay").click(function (e) {
        $(".pay-list").animate({
            height: 350,
        }, 1000, function () {
            $(".switch-otherpay").hide();
        })
    })

    $(".address-expend").click(function (e) {
        if ($(".address-list").hasClass("closed")) {
            $(".address-list").removeClass("closed");
            $(".address-list").addClass("expended");
        } else {
            $(".address-list").addClass("closed");
            $(".address-list").removeClass("expended");
        }
    })
    //$("#btn-order-step2").click(function(e){
    //	//window.location = "orderFinished"
    //})



})

///物流可选的送达时间段
function LoadDeliverTimeByArea(areaId) {
    //alert(areaId);
    if (areaId == "" || areaId == null || areaId == undefined) {
        areaId = 1;
    }
    $.ajax({
        url: "/dataprovider/LoadDeliverTimeByArea",
        data: "areaId=" + areaId,
        dataType: 'json',
        type: 'get',
        async: false,
        success: function (data) {
            if (data.IsSuccess) {
                $("#DeliverTimeByArea").empty();
                for (var i = 0; i < data.Values.length; i++) {

                    $("#DeliverTimeByArea").append("<option value='" + data.Values[i].Id + "'>" + data.Values[i].Name + "</option>");

                }

            }
        }, failure: function () {

        }
    })
}
// //地址框事件重刷
function reflushEvent() {
    $(".address-box").click(function (e) {
        var currAddress = $(this).attr("data-value");
        // TODO:判断是否为空页
        if (currAddress == "new") {
            $("#address-form #FirstName").val("");
            $("#address-form #Mobile").val("");
            $("#address-form #Line1").val("");
            $("#address-form #ZipCode").val("");
            $("#address-form #province").val("0");
            $("#address-form #city").val("0");
            $("#address-form #district").val("0");

            $(".address-form").attr("data-value", "");
            $(".address-form").show();
        } else if (currAddress == "nologin") {
            $(".address-form").show();
        } else {
            // 读取我的地址（已登录状态）
            $.ajax({
                url: "/personal/myreceivers",
                dataType: 'json',
                type: 'get',
                async: false,
                success: function (data) {
                    if (data.IsSuccess) {
                        for (var i = 0; i < data.Receivers.length; i++) {
                            var address = data.Receivers[i];
                            if (address.Id == currAddress) {
                                $("#address-form #FirstName").val(address.FirstName);
                                $("#address-form #Mobile").val(address.Mobile);
                                $("#address-form #Line1").val(address.Line1 + address.Line2);
                                $("#address-form #ZipCode").val(address.ZipCode);
                                $("#address-form #province").val(address.ProvinceId);
                                $("#province").change();
                                $("#address-form #city").val(address.CityId);
                                $("#city").change();
                                $("#address-form #district").val(address.AreaId);
                                $(".address-form").attr("data-value", address.Id);
                                $(".address-form").show();
                            }
                        }

                    }
                }, failure: function () {

                }
            })
        }
    })
}

function addCheck(){
	var isOk = true;
	
	if($("#FirstName").val() == ""){
		alert("请填写姓名");
		isOk = false;
	}else if($("#Mobile").val().length != 11 || $("#Mobile").val().substr(0,1) != 1){
		alert("手机格式错误");
		isOk = false;
	}else if($("#province").val() == "" || $("#city").val() == "" || $("#district").val() == "" ||
			$("#province").val() == null || $("#city").val() == null || $("#district").val() == null ){
		alert("请选择省市区");
		isOk = false;
	}else if($("#Line1").val() == "" ){
		alert("请填写详细地址");
		isOk = false;
	}
	
	
	return isOk;
}
$(document).ready(function () {
    reflushEvent();//地址框事件重刷
    $("#btn-addconfirm").click(function(e){
    //$("#frmAddress").submit(function (e) {
    	if(addCheck() == true){
	        var isLogin = true;
	        isLogin = getCookie("Name") == null ? false : true;
	        // 提交
	        var receiverId = $("#address-form").attr("data-value");
	
	        var address = {
	            id: $("#address-form").attr("data-value"),
	            areaId: $("#district").val(),
	            cityId: $("#city").val(),
	            firstName: $("#FirstName").val(),
	            line1: $("#Line1").val(),
	            mobile: $("#Mobile").val(),
	            zipcode: $("#ZipCode").val()
	        }
	        if (isLogin) {
	            if (receiverId != "") {
	                $.ajax({
	                    url: "/personal/updatereceiver",
	                    dataType: 'json',
	                    type: 'post',
	                    data: address,
	                    async: false,
	                    success: function (data) {
	                        //保存成功后更新列表里的数据
	                        var addEle = $(".address-box[data-value='" + data.ReceiverInfo.Id + "']");
	                        addEle.find("#delivery-name").html(data.ReceiverInfo.FirstName);
	                        addEle.find("#delivery-phone").html(data.ReceiverInfo.Mobile);
	                        addEle.find("#delivery-address").html(data.ReceiverInfo.Line1);
	                        addEle.find("#areaID").val(data.ReceiverInfo.AreaId);
	                        // 到第一条去
	                        $(".address-list").prepend(addEle);
	                        // 然后关闭
	                        if ($(".address-list").hasClass('expended')) {
	                            $(".address-expend").click();
	                        }
	                        $(".address-form").hide();
	
	                    }
	                })
	            } else {
	
	                $.ajax({
	                    url: "/personal/addreceiver",
	                    dataType: 'json',
	                    type: 'post',
	                    data: address,
	                    async: false,
	                    success: function (data) {
	                        //保存成功后新增一个记录放在第一条
	                        $(".address-list").prepend(
	                            "<div class='address-box' data-value='" + data.ReceiverID + "'>" +
	                                "<div class='line1'>" +
	                                "    <label>收货人：</label>" +
	                                "    <label id='delivery-name'>" + data.ReceiverInfo.FirstName + "</label>" +
	                                "    <label id='delivery-phone'>" + data.ReceiverInfo.Mobile + "</label>" +
	                                "    <input type='hidden' id='cityID' value=" + data.ReceiverInfo.CityId + " />" +
	                                "    <input type='hidden' id='areaID' value=" + data.ReceiverInfo.AreaId + " />" +
	                                "    <input type='hidden' id='delivery-zipCode' value=" + data.ReceiverInfo.ZipCode + " />" +
	                                "</div>" +
	                                "<div class='line2'>" +
	                                "    <label>收货地址：</label>" +
	                                "    <span id='delivery-address'>" + data.ReceiverInfo.Line1 + "</span>" +
	                                "</div>" +
	                            "</div>"
	                        );
	                        reflushEvent();//地址框事件重刷
	                        //合起来
	                        if ($(".address-list").hasClass('expended')) {
	                            $(".address-expend").click();
	                        }
	                        $(".address-form").hide();
	                    }
	                })
	            }
	        } else {
	
	            $(".address-list").prepend(
	                "<div class='address-box' data-value='nologin'>" +
	                    "<div class='line1'>" +
	                    "    <label>收货人：</label>" +
	                    "    <label id='delivery-name'>" + $("#FirstName").val() + "</label>" +
	                    "    <label id='delivery-phone'>" + $("#Mobile").val() + "</label>" +
	                    "    <input type='hidden' id='cityID' value=" + $("#city").val() + " />" +
	                    "    <input type='hidden' id='areaID' value=" + $("#district").val() + " />" +
	                    "    <input type='hidden' id='delivery-zipCode' value=" + $("#ZipCode").val() + " />" +
	                    "</div>" +
	                    "<div class='line2'>" +
	                    "    <label>收货地址：</label>" +
	                    "    <span id='delivery-address'>" + $("#Line1").val() + "</span>" +
	                    "</div>" +
	                "</div>"
	            );
	            reflushEvent();//地址框事件重刷
	            //合起来
	            if ($(".address-list").hasClass('expended')) {
	                $(".address-expend").click();
	            }
	            $(".address-form").hide();
	
	        }
	
	        LoadDeliverTimeByArea($("#district").val());
	
	        summary(getQueryString("OrderID"), $("#coupon").val(), $("#district").val());
	
    		}
	
	    })
    
})

//// 省市区开始
var Regins;

$(document).ready(function () {
    iniRegin("cn");


    $("#province").change(function (e) {
        var province = $(this).val();
        if (province != "") {

            var provinceIndex = $(this.selectedOptions).attr("data-arr-index");
            if (provinceIndex != undefined) {
                $(this).attr('data-arr-index', provinceIndex);
                var arrCitys = Regins[provinceIndex].Cities;
                $("#city").html("<option value=''>请选择</option>");
                for (var i = 0; i < arrCitys.length; i++) {
                    $("#city").append("<option value=" + arrCitys[i].Id + " data-arr-index=" + i + ">" + arrCitys[i].Name + "</option>");
                }
            } else {
            }
        } else {
            $("#city").html("<option value=''>请选择</option>");
            $("#district").html("<option value=''>请选择</option>");
        }
    })

    $("#city").change(function (e) {
        var city = $(this).val();
        if (city != "") {
            var provinceIndex = $("#province").attr("data-arr-index");
            if (provinceIndex != undefined) {
                var cityIndex = $(this.selectedOptions).attr("data-arr-index");

                $(this).attr('data-arr-index', cityIndex);

                var arrDistrict = Regins[provinceIndex].Cities[cityIndex].District;
                $("#district").html("<option value=''>请选择</option>");
                for (var i = 0; i < arrDistrict.length; i++) {
                    $("#district").append("<option value=" + arrDistrict[i].Id + " data-arr-index=" + i + ">" + arrDistrict[i].Name + "</option>");
                }
            }
        } else {
            $("#district").html("<option value=''>请选择</option>");
        }
    })

})


// 省市区初始化
function iniRegin(language) {
    $.ajax({
        url: "/dataprovider/RegionWithProvince",
        dataType: 'json',
        type: 'get',
        data: { language: language },
        async: false,
        success: function (data) {
            if (data.IsSuccess) {
                Regins = data.Provinces;
                for (var i = 0; i < Regins.length; i++) {
                    $("#province").append("<option value=" + Regins[i].Id + " data-arr-index=" + i + ">" + Regins[i].Name + "</option>");
                }
            }
        }, failure: function () {

        }
    })
}



///summary 计算价格
function summary(OrderID, PromotionCode, AreaID) {
    var summaryInfo = {
        OrderID: OrderID,
        PromotionCode: PromotionCode,
        AreaID: AreaID,
        IsSelfPickUp: 0  ///目前只有配送，无自提
    }
    $.ajax({
        url: "../order/Summary",
        dataType: 'json',
        type: 'post',
        data: summaryInfo,
        async: false,
        success: function (data) {
            $("#delivery-price").html(data.SummaryInfo.DeliverFee);
            $("#total_price").html(data.SummaryInfo.TotalPrice);
            $("#tips").html("-￥" + data.SummaryInfo.PromotionValue);
            $("#set-total").html(data.SummaryInfo.TotalPrice);
        }
    })
}

$("#btn-coupon").click(
    function () {
        summary(getQueryString("OrderID"), $("#coupon").val(), $("#district").val());
    }
    )

