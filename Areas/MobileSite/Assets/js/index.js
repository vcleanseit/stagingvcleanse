/**
 * index.html base js
 */

$(document).ready(function () {
    /**
	 * Welcome layout 进入后慢慢消失
	 */

    //showWelcome();
    //$("#welcome-layout").on('click', hideWelcome);
    //setTimeout(hideWelcome, 2000);

    /**
	 * Slider
	 */

	$('.banner').unslider({
		autoplay:true,
        speed: 500,               //  The speed to animate each slide (in milliseconds)
        delay: 5000,              //  The delay between slide animations (in milliseconds)
        complete: function () { },  //  A function that gets called after every slide animation
        keys: true,               //  Enable keyboard (left, right) arrow shortcuts
        dots: true,               //  Display dot navigation
        fluid: false,              //  Support responsive design. May break non-responsive designs
        nav:true,
        arrows:false
    });
    
    var postValue = {
        //TODO:
        //语言要获取，这里只是测试
        "language": "cn"
    }
    $.ajax({
        url: "../Package/Package",
        data: postValue,
        dataType: 'json',
        type: 'get',
        async: false,
        success: function (data) {
            //TODO:
            //数据显示

            loadData(data);
        }, failure: function () {

        }
    })
})

function loadData(data) {

    if (data.IsSuccess) {
        var packageInfo = "";
        var sex = 1;
        var Classic = "<div class='series'><h1>冷压套餐</h1><h2>Intro Cleanses</h2></div>";//冷压套餐
        var Warming = "<div class='series'><h1>暖体套餐</h1><h2>Intro Warming</h2></div>";//暖体套餐
        var Specials = "<div class='series'><h1>特制清体</h1><h2>Specials</h2></div>";//特制清体
        var Master = "<div class='series'><h1>大师套餐</h1><h2>Master</h2></div>";//大师套餐
        var IsClassic, IsWarming, IsSpecials, IsMaster = false;
        for (var i = 0; i < data.Packages.length; i++) {
            if (data.Packages[i].Groups.length == 1) {
                sex = 0;
            }
            packageInfo = ""
            packageInfo += "<div class='package' onclick='PackageDetail(" + data.Packages[i].Id + ")'>";
            packageInfo += "<div class='package-img'>";
            packageInfo += "<img src='/images/cleanse/" + data.Packages[i].ImageName + "' alt=''>";
            packageInfo += "<div class='price'>￥" + data.Packages[i].Groups[sex].Rules[0].PackagePrice + "</div>";
            packageInfo += "</div>";
            packageInfo += "<h1><span>" + data.Packages[i].Title + "</span></h1>";
            packageInfo += "<div class='desc'>" + data.Packages[i].Tips + "</div>";
            packageInfo += "</div>";
            switch (data.Packages[i].CategoryId) {
                case 1:
                    Classic = Classic + packageInfo;
                    IsClassic = true;
                    break;
                case 2:
                    Warming += packageInfo;
                    IsWarming = true;
                    break;
                case 3:
                    Specials += packageInfo;
                    IsSpecials = true;
                    break;
                case 4:
                    Master += packageInfo;
                    IsMaster = true;
                    break;

                default:
                    break;
            }
        }
        var html = "";
        if (IsClassic) {
            html += Classic;
        }
        if (IsWarming) {
            html += Warming;
        }
        if (IsSpecials) {
            html += Specials;
        }
        if (IsMaster) {
            html += Master;
        }
        $("#content").html(html);
    } else {
        //数据错误
    }
}

function PackageDetail(Id) {
    window.location.href = "../mobilesite/cleanse/packagedetail?packageId=" + Id;
}