$(document).ready(function () {
    showWelcome();

    $(".logo").animate({
        top: "8%",
    }, 1000, function () {
        $("#frm-signin").animate({
            opacity: 1,
        }, 500);
        $(".underlink").animate({
            opacity: 1,
        }, 500);
    });
})

var timerCD;
$("#btn-smscode").click(
    function () {
        var postValue = {
            "Mobile": $("#username").val(),
            "verifyCode": $("#verificationcode").val(),
            "language": "cn"
        }

        $.ajax({
            url: "/Account/SendValidationCodeByWeb",
            data: postValue,
            dataType: 'json',
            type: 'post',
            async: false,
            success: function (data) {

                if (data.IsSuccess) {
                    timerCD = setInterval(smsCD, 1000);
                    // window.location.href = "/mobilesite";
                } else {
                    $(".tips").html(data.Description);
                    $(".tips").animate({
                        opacity: 0,
                    }, 5000, function () {
                        $(".tips").html("");
                        $(".tips").css("opacity", 1);
                    });
                }

            }, failure: function () {

            }
        })
    })

var time = 60;
var btnCaption = $("#btn-smscode").html();
function smsCD(t) {
    $("#btn-smscode").attr("disabled", "disabled");
    $("#btn-smscode").html(time);
    time--;
    if (time == 0) {
        $("#btn-smscode").removeAttr("disabled");
        $("#btn-smscode").html(btnCaption);
        clearInterval(timerCD);
    }
}

function verify() {
    if ($("#username").val().length != 11 || $("#username").val().substr(0, 1) != 1) {
        $(".tips").html("手机格式有误！");
        $(".tips").animate({
            opacity: 0,
        }, 5000, function () {
            $(".tips").html("");
            $(".tips").css("opacity", 1);
        });
        return false;
    } else if ($("#password").val() != $("#passwordrepeat").val()) {
        $(".tips").html("密码与确认密码必须保持一致！");
        $(".tips").animate({
            opacity: 0,
        }, 5000, function () {
            $(".tips").html("");
            $(".tips").css("opacity", 1);
        });
        return false;
    }
    return true;
}
