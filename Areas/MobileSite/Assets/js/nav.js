var NAV_OPEND = false;		// 目前菜单状态是否已打开
var MENU_HEIGHT = 40;	// 菜单的高度
var MENU_OPENSPEED = 300;	// 菜单展开的速度

$(document).ready(function (){
	
	$(".nav-subsonmenu-item").on('click', function (e) {
         window.location.href="../mobilesite/cleanse/packagedetail?packageId=35"
    });
	/**
	 * 菜单打开关闭
	 */
	$("#btn-nav").on('click',function(e){
		if(NAV_OPEND){
			closeNav();
		}else{
			openNav();
		}
	});
	
	$(".nav-space").on('click',function(e){
		closeNav();
	});
	
	
	function openNav(){
		$("nav").animate({
			left: 0,
		}, MENU_OPENSPEED, function() {
			NAV_OPEND = true;
		});
	}
	
	function closeNav(){
		$("nav").animate({
			left: "-100%",
		}, MENU_OPENSPEED, function() {
			NAV_OPEND = false;
		});
	}
	
	/**
	 * 菜单折叠打开
	 */
	
	$("nav .nav-menu .has-sub .sub-main").on('click',function(e){
		var el = $(e.target).parent();
		if(el.hasClass('expanded')){
			el.animate({
				height: MENU_HEIGHT,
			}, MENU_OPENSPEED, function() {
				el.removeClass('expanded');
			});
		}else{
			var curHeight = el.height();
			var autoHeight = el.css('height', 'auto').height();
			el.height(curHeight).animate({
				height: autoHeight,
			}, MENU_OPENSPEED, function() {
				el.css("height","auto");
				el.addClass('expanded');
			});
			
		}
	})
})