﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Areas.MobileSite
{
    public class MobileAreaRegistration : AreaRegistration
    {

        public override string AreaName
        {
            get
            {
                return "MobileSite";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Mobile_default",
                "mobileSite/{controller}/{action}/{id}",
                new { controller = "cleanse", action = "Index", id = UrlParameter.Optional },
                new string[] { "VCleanse.Shop.Host.Areas.MobileSite.Controllers" }
            );
        }
    }
}