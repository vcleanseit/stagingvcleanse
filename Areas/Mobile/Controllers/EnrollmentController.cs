﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class EnrollmentController : BaseController
    {
        public ActionResult Forgotpassword()
        {
            return View("Forgotpassword");
        }

        public ActionResult Register()
        {
            return View("Register");
        }

        public ActionResult Signin()
        {
            return View("Signin");
        }

        public ActionResult Success()
        {
            return View("Success");
        }

    }
}
