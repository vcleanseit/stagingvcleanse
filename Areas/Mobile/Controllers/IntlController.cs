﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class IntlController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Home");
        }

        // For old website users(add to favorite icon)
        public ActionResult Home()
        {
            return Redirect("/m");
        }

    }
}