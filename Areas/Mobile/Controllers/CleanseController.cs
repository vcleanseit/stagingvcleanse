﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class CleanseController : BaseController
    {
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Detail()
        {
            return View("Detail");
        }

        public ActionResult Choose()
        {
            return View("Choose");
        }

    }
}
