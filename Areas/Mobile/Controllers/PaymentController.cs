﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class PaymentController : BaseController
    {
        public ActionResult ToPay()
        {
            return View("ToPay");
        }

        public ActionResult PaySuccess()
        {
            NLogHelper.LogInfo("Action", "PaySuccess");

            return View("PaySuccess");
        }
        public ActionResult PayFailed()
        {
            NLogHelper.LogInfo("Action", "PayFailed");

            return View("PayFailed");
        }

    }
}
