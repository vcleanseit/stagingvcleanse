﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz._Legacy.Feedback;
using VCleanse.Shop.Biz.Promotion;
using VCleanse.Shop.Host.Areas.Mobile.Models;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Promotions;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class FeedbackController : BaseController
    {
        // GET api/<controller>
        public ActionResult Index()
        {
            return Redirect("/m/feedback/feedback");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Feedback()
        {
            return View("feedback");
        }


        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult SendSMSView()
        {
            return View("SendSMSView");
        }


        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult PromotionCode()
        {
            return View("PromotionCode");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult Submit(OrderVote ordervote)
        {
            //写入日志
            NLogHelper.LogInfo("VoteSubmit  提交投票");
            int taste = ordervote.taste;
            int effect = ordervote.effect;
            int delivery = ordervote.delivery;
            int again = ordervote.again;
            int orderID = ordervote.orderID;
            string commit = ordervote.commit;
            if (taste == 0 || effect == 0 || delivery == 0 || orderID == 0)
            {
                return Json(new { IsSuccess = false,ErrorMessage = "数据为空" });
               
            }
            OrderItemInfo OrderInfos = new OrderItemInfo();
            OrderInfos = OrdersBLL.LoadOrderById(orderID);
            if (OrderInfos == null) {
                return Json(new { IsSuccess = false, ErrorMessage = "数据为空" });
            }


            FeedBack fb = new FeedBack();
            fb = FeedbackBLL.GetFeedbackInfo(orderID);
            PromotionRuleInfo promotionRule = new PromotionRuleInfo();
            if (fb != null && fb.PromotionCode != "" && fb.PromotionCode!=null)
            {
                 promotionRule = PromotionBLL.LoadAllPromotionRulesByCode(fb.PromotionCode);            
            }
            else
            {                
                promotionRule.ActiveFrom = DateTime.Now;
                promotionRule.ActiveTo = DateTime.Parse("2016-12-31");
                Random rd = new Random();
                string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                string sCode = "";
                for (int i = 0; i < 6; i++)
                {
                    sCode += str[rd.Next(str.Length)];
                }
                promotionRule.Code = sCode;
                promotionRule.IsActive = true;
                promotionRule.PromotionTypeId = 2;//一次性
                promotionRule.RuleId = 1;//满减
                promotionRule.RuleValue = 10;//优惠10元
                promotionRule.StartAmount = 10;
                try
                {
                    PromotionBLL.InsertPromotionCode(promotionRule);

                    FeedBack fback = new FeedBack();
                    fback.DelievryScore = delivery;
                    fback.EffectScore = effect;
                    fback.CreateTime = DateTime.Now;
                    fback.IsBuyAgain = again == 1 ? true : false;
                    fback.FeelScore = taste;
                    fback.OrderID = orderID;
                    fback.PromotionCode = sCode;
                    fback.Commit = commit;
                    fback.UpdateTime = DateTime.Now;
                    FeedbackBLL.InsertFeedBack(fback);                   

                }
                catch (Exception)
                {
                    return Json(new{IsSuccess = false});
                }  

            }
         
            return Json
               (new
               {
                   IsSuccess = true,
                   PromotionCode = promotionRule.Code,
                   PromotionValue = promotionRule.RuleValue,
                   PromotionActiveTo = promotionRule.ActiveTo
               });
        }

     
    }
}