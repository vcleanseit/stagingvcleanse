﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Areas.Mobile.Models
{
    public class OrderVote
    {
        public int taste { set; get; }
        public int effect { set; get; }
        public int delivery { set; get; }
        public int again { set; get; }
        public int orderID { set; get; }

        public string commit { set; get; }
    }
}