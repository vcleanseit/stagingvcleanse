﻿using VCleanse.Shop.Biz.Shared;
using System;
using System.Web;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Biz;

namespace VCleanse.Shop.Host.Context
{
    public class VcleanseWebModule : IHttpModule
    {
        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;

        }

        void context_BeginRequest(object sender, System.EventArgs e)
        {
            if (ShouldRunModule())
            {
                var context = HttpContext.Current;
                var currentUser = BuildUserContext(context);

                if (string.IsNullOrEmpty(currentUser.Session_id))
                {
                    var sessionId = CreateAndSaveNewSession(context);
                    currentUser.Session_id = sessionId;

                    SessionService.AnalyzeAndSaveSession(sessionId, context);
                }

                HttpContext.Current.Items[WebConstants.Context.CurrentUserKey] = currentUser;
            }
        }



        //private object BuildUserContextTest(HttpContext httpContext)
        //{
        //    VcleanseContext userContext = new VcleanseContext()
        //    {
        //        IsUserLogined = true,
        //        Member_id = 2533,
        //        DisplayName = "abc",
        //    };
        //    userContext.Member = VUsersBLL.LoadMemberInfo(userContext.Member_id, true);
        //    return userContext;
        //}


        private VcleanseContext BuildUserContext(HttpContext httpContext)
        {
            VcleanseContext userContext = new VcleanseContext()
            {
                IsUserLogined = false,
                Member_id = Config.DefaultCustomerId,
                DisplayName = string.Empty,
                IsDebug = IsDebug(httpContext),
                Session_id = GetSessionId(httpContext)
            };
            string memberString = HttpHelper.GetCookieValue(WebConstants.CookieKey.Id, httpContext.Request);
            int member_id = 0;
            if (string.IsNullOrWhiteSpace(memberString))
            {
                member_id = Config.DefaultCustomerId;
            }
            else
            {
                string decedMemberString = Security.Decrypt(memberString);
                int.TryParse(decedMemberString, out member_id);
            }
            if (member_id > 0 && member_id != Config.DefaultCustomerId)
            {
                userContext.IsUserLogined = true;
                userContext.Impersonating = false;
            }
            else
            {
                userContext.IsUserLogined = false;
                userContext.Impersonating = true;
            }
            userContext.Member = VUsersBLL.LoadMemberInfo(member_id, true);
            userContext.Member_id = member_id;
            userContext.DisplayName = HttpHelper.GetCookieValue(WebConstants.CookieKey.Name, httpContext.Request);

            return userContext;
        }

        private string GetSessionId(HttpContext httpContext)
        {
            return HttpHelper.GetCookieValue(WebConstants.CookieKey.Session, httpContext.Request);
        }

        private string CreateAndSaveNewSession(HttpContext httpContext)
        {
            var newSessionId = Guid.NewGuid().ToString().ToLower().Replace("-",string.Empty);
            HttpHelper.WriteCookie(httpContext, WebConstants.CookieKey.Session, newSessionId, false);
            return newSessionId;
        }

        public bool ShouldRunModule()
        {
            return true;
        }


        private bool IsDebug(HttpContext httpContext)
        {
            string debugQueryString = httpContext.Request.QueryString[WebConstants.CookieKey.Debug];
            if (string.IsNullOrWhiteSpace(debugQueryString))
            {
                var cookie = httpContext.Request.Cookies[WebConstants.CookieKey.Debug];
                if (cookie != null)
                {
                    debugQueryString = cookie.Value;
                }
            }
            bool isDebug = string.Equals(debugQueryString, "true", System.StringComparison.OrdinalIgnoreCase);
            HttpHelper.WriteCookie(httpContext, WebConstants.CookieKey.Debug, isDebug.ToString(), false);
            return isDebug;
        }
    }
}