﻿using System;

namespace VCleanse.Shop.Host.PaymentService.Ucfpay.com

{
    public static class UcfPayStatusCode
    {
        public static readonly string SUCCESS = "20";
        public static readonly string FAIL = "30";
    }
}
