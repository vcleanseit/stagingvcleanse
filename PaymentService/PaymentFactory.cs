﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Model;

namespace VCleanse.Shop.Host.PaymentService
{
    public class PaymentFactory
    {
        public IPayment GetPayment(int paymentPlatformId, int orderId, string totalPrice, string strBank, int qrType, string ip)
        {
            switch (paymentPlatformId)
            {
                case Constants.PaymentPlatform.ALIPAY:
                    return new Alipay.AlipayNormalService(orderId,totalPrice,strBank,qrType,ip);

                case Constants.PaymentPlatform.UCFPAY:
                    return new Ucfpay.UcfpayService(orderId,totalPrice);

                case Constants.PaymentPlatform.ALIWAPPAY:
                    return new AlipayWap.AlipayWapService(orderId, totalPrice);

                case Constants.PaymentPlatform.UNIONPAY:
                    return new PayError();
               default:
                    return new PayError();
            }
        
        }
    }
}