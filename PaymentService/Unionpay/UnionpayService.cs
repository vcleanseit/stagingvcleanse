﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using VCleanse.Shop.Host.PaymentService.Unionpay.com;


namespace VCleanse.Shop.Host.PaymentService.Unionpay
{
    public class UnionpayService
    {

        public static string UnionPayHtml(int orderId, string totalPrice)
        {
           
            string out_trade_no = orderId.ToString();
            string total_fee = totalPrice;


            //把请求参数打包成数组
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();
            sParaTemp.Add("MerId", UnionPayConfig.MerId);
            sParaTemp.Add("OrdId", UnionpayCore.TransUnionPayOrderNo(out_trade_no));
            sParaTemp.Add("TransAmt", UnionpayCore.TransUnionPayAmt(total_fee));
            sParaTemp.Add("CuryId", UnionPayConfig.CuryId);
            sParaTemp.Add("TransDate", DateTime.Now.ToString("yyyyMMdd"));
            sParaTemp.Add("TransType", UnionPayConfig.TransType);
            sParaTemp.Add("Version", UnionPayConfig.Version);
            sParaTemp.Add("BgRetUrl", UnionPayConfig.BgRetUrl);
            sParaTemp.Add("PageRetUrl", UnionPayConfig.PageRetUrl);
            sParaTemp.Add("GateId", UnionPayConfig.GateId);
            sParaTemp.Add("Priv1", UnionPayConfig.Priv1);

            string sHtmlText = "";

            try
            {
                sHtmlText = Create_direct_unionpay_by_user(sParaTemp);
            }
            catch (Exception ex)
            {
               
            }
           return sHtmlText;
        }
        protected static string Create_direct_unionpay_by_user(SortedDictionary<string, string> sParaTemp)
        {

            string pwd = sParaTemp["MerId"] + sParaTemp["OrdId"] + sParaTemp["TransAmt"] +
                       sParaTemp["CuryId"] + sParaTemp["TransDate"] + sParaTemp["TransType"] + sParaTemp["Priv1"];

            sParaTemp.Add("ChkValue", UniSignData.sign(UnionPayConfig.MerId, pwd));

            string strButtonValue = "确认";
            //表单提交HTML数据
            string strHtml = "";

            //构造表单提交HTML数据
            strHtml = BuildFormHtmlUnionPay(sParaTemp, UnionPayConfig.postAction, "post", strButtonValue);

            return strHtml;
        }

        protected static string BuildFormHtmlUnionPay(SortedDictionary<string, string> sParaTemp, string gateway, string strMethod, string strButtonValue)
        {

            StringBuilder sbHtml = new StringBuilder();

            sbHtml.Append("<form id='Unionpaysubmit' name='Unionpaysubmit' action='" + gateway + "' method='" + strMethod.ToLower().Trim() + "'>");

            foreach (KeyValuePair<string, string> temp in sParaTemp)
            {
                sbHtml.Append("<input type='hidden' name='" + temp.Key + "' value='" + temp.Value + "'/>");
            }

            //submit按钮控件请不要含有name属性
            sbHtml.Append("<input type='submit' value='" + strButtonValue + "' style='display:none;'></form>");

            sbHtml.Append("<script>document.forms['Unionpaysubmit'].submit();</script>");

            return sbHtml.ToString();
        }
    }
}