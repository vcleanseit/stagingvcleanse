﻿using VCleanse.Shop.Biz.Log;
using Maticsoft.DBUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Utility
{
    public static class Security
    {
        public static string Encrypt(string original)
        {
            return DESEncrypt.Encrypt(original);
        }

        public static string Decrypt(string encrpytText)
        {
            try
            {
                return DESEncrypt.Decrypt(encrpytText);
            }
            catch (Exception ex)
            {
                ExceptionPublishHelper.PublishFormat(ex, encrpytText);
                return string.Empty;
            }
        }

    }

}