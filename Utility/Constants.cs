﻿
namespace VCleanse.Shop.Host.Utility
{
    public static class WebConstants
    {
        public static class Context
        {
            public const string CurrentUserKey = "Logon_User";
            public const string ImageValidationCode = "ImageValidationCode";
        }

        public static class CookieKey
        {
            public const string Id = "MemberId";
            public const string Name = "Name";
            public const string Session = "SessionId";
            public const string Debug = "Debug";
            public const string OrderSource = "source";
        }

        public static class LogMessage
        {
            public const string LogonViaWebSite = "会员通过订单页面登录网站";
        }

        public static class OrderDateCheckKey
        {
            /// <summary>
            /// HH
            /// </summary>
            public const string BaseKey = "HH";
            /// <summary>
            /// 12
            /// </summary>
            public const int BaseKeyValue = 12;
            /// <summary>
            /// 1
            /// </summary>
            public const int DayIntervaltoBeforeBaseKey = 1;
            /// <summary>
            /// 2
            /// </summary>
            public const int DayIntervaltoAfterBaseKey = 2;
        }
    }
}