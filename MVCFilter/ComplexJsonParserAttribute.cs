﻿using VCleanse.Shop.Biz.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace VCleanse.Shop.Host.MVCFilter
{
    public class ComplexJsonParserAttribute : ActionFilterAttribute
    {
        private readonly Type _type;
        private readonly string _key;

        public ComplexJsonParserAttribute(Type targetType, string key)
        {
            _type = targetType;
            _key = key;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string stringifiedModel = string.Empty;
                using (StreamReader reader = new StreamReader(filterContext.HttpContext.Request.InputStream))
                {
                    stringifiedModel = reader.ReadToEnd();
                    reader.Close();
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                object deserialized = js.Deserialize(stringifiedModel, _type);
                filterContext.ActionParameters[_key] = deserialized;
            }
            catch (Exception e)
            {
                ExceptionPublishHelper.PublishFormat(e);
                base.OnActionExecuting(filterContext);
            }
        }
    }
}