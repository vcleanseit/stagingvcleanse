{
    "IsSuccess": true,
    "Packages": [{
        "Id": 42,
        "Title": "能量满格特饮",
        "SubTitle": "-",
        "Description": "专为喜爱运动的您贴心设计。4瓶蔬果汁，为您迅速补充运动流失的水分、葡萄糖、电解质和蛋白质，让身体随时活力满满！",
        "Url": "",
        "Tips": "",
        "Introduction": "专为喜爱运动的您贴心设计。4瓶蔬果汁，为您迅速补充运动流失的水分、葡萄糖、电解质和蛋白质，让身体随时活力满满！",
        "ImageName": "",
        "Tags": [],
        "Groups": [{
            "Id": 63,
            "Name": "-",
            "GroupTypeId": 0,
            "ByWeek": false,
            "Rules": [{
                "Id": 647,
                "Day": 1,
                "Name": "能量满格特饮",
                "PackagePrice": 150,
                "Gallery": [{
                    "img": "recommend/workout/set/workout.jpg"
                }, {
                    "img": "recommend/workout/ingredients/fragrantclear.jpg"
                }, {
                    "img": "recommend/workout/ingredients/fragrantclear.jpg"
                }, {
                    "img": "recommend/workout/ingredients/spicylemonade.jpg"
                }, {
                    "img": "recommend/workout/ingredients/spicylemonade.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 48,
                    "Name": "芳香椰子水",
                    "Image": "recommend/workout/individual/fragrantclear.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "萨瓦迪卡！没什么比这清新幽香的滋味儿更能安抚您运动后饥渴的身体与心灵了！随泰国海风而来的百分百纯天然低糖，却又饱含了让您身心滋养的电解质与矿物元素钾，甜蜜又贴心，怎能不爱上？我们走遍全球，只为将这份孕育自泰国的芳香带给您。",
                    "Ingredient": "100%纯净椰子汁",
                    "Nutritionn": "热量57k、蛋白质0g、脂肪0g、总碳水化合物13.3g",
					"Nutrition": "热量57k"
                }, {
                    "Count": 1,
                    "Id": 48,
                    "Name": "芳香椰子水",
                    "Image": "recommend/workout/individual/fragrantclear.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "萨瓦迪卡！没什么比这清新幽香的滋味儿更能安抚您运动后饥渴的身体与心灵了！随泰国海风而来的百分百纯天然低糖，却又饱含了让您身心滋养的电解质与矿物元素钾，甜蜜又贴心，怎能不爱上？我们走遍全球，只为将这份孕育自泰国的芳香带给您。",
                    "Ingredient": "100%纯净椰子汁",
                    "Nutritionn": "热量57k、蛋白质0g、脂肪0g、总碳水化合物13.3g",
					"Nutrition": "热量57k"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "大师柠檬水",
                    "Image": "recommend/workout/individual/spicylemonade.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "最初的大师清体配方由维果清继承并忠实为您呈现。纯净柠檬汁配以南美胡椒和适量糖浆为清体达人量身打造，这瞬间迸发的热情，如电流般唤醒您的味蕾及觉察力。 我们荣幸地为您呈献这近三十年来俘获无数爱意的配方，一起感受南美辣椒所带来的心跳加速手心出汗的刺激吧！",
                    "Ingredient": "柠檬、龙舌兰糖浆、南美胡椒。",
                    "Nutritionn": "热量10k、蛋白质0g、脂肪0g、总碳水化合物2.9g",
					"Nutrition": "热量10k"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "大师柠檬水",
                    "Image": "recommend/workout/individual/spicylemonade.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "最初的大师清体配方由维果清继承并忠实为您呈现。纯净柠檬汁配以南美胡椒和适量糖浆为清体达人量身打造，这瞬间迸发的热情，如电流般唤醒您的味蕾及觉察力。 我们荣幸地为您呈献这近三十年来俘获无数爱意的配方，一起感受南美辣椒所带来的心跳加速手心出汗的刺激吧！",
                    "Ingredient": "柠檬、龙舌兰糖浆、南美胡椒。",
                    "Nutritionn": "热量10k、蛋白质0g、脂肪0g、总碳水化合物2.9g",
					"Nutrition": "热量10k"
                }]
            }]
        }]
    }]
}