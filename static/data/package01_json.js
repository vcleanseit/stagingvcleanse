{
	"IsSuccess": true,
	"Packages": [{
		"Id": 19,
		"Title": "暖体系列入门套餐2015",
		"SubTitle": "暖体系列入门套餐2015",
		"Description": "维果清为初尝清体的您倾情奉献美味好喝的暖体套餐2015款，精心甄选了坚果、草本等滋养成分，缤纷的色彩，美味的口感，维果清温润呵护您的每一个秋冬日。暖体经典入门套餐2015",
		"Url": "cleanse_warmingdetoxcn",
		"Tips": "女士套餐6瓶，男士套餐8瓶。温馨小贴士：部分饮品加热后口感更美味哦，套餐每日配送的菜单口味相同。",
		"Introduction": "维果清为初尝清体的您倾情奉献美味好喝的暖体套餐2015款，精心甄选了坚果、草本等滋养成分，缤纷的色彩，美味的口感，维果清温润呵护您的每一个秋冬日。",
		"ImageName": "warming_detox.jpg",
		"Tags": [],
		"Groups": [{
			"Id": 30,
			"Name": "男士",
			"GroupTypeId": 1,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 573,
				"Day": 1,
				"Name": "暖体系列入门套餐2015男士",
				"PackagePrice": 400.00,
				"Gallery":[{"img":"warming/intro/set/m.jpg"},
				           {"img":"warming/intro/ingredients/chiagreen.jpg"},
				           {"img":"warming/intro/ingredients/classicblack.jpg"},
				           {"img":"warming/intro/ingredients/darkwhite.jpg"},
				           {"img":"warming/intro/ingredients/easyred.jpg"},
				           {"img":"warming/intro/ingredients/hotpear.jpg"},
				           {"img":"warming/intro/ingredients/luohanguo.jpg"},
				           {"img":"warming/intro/ingredients/pumpkinsoup.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "热梨生姜特饮",
					"Image": "warming/intro/individual/hotpear.png",
					"Price": 50.00,
					"Url": "pearandginger",
					"MaxCount": 50,
					"Description": "这是一款香甜却又带有一丝辛辣的梨制特饮。山药含有丰富的维生素B6，钾和镁，生梨带来甘甜和生津止渴，生姜、肉桂为您驱走寒冷，温润全身，带走多余的热量，在冬日的午后暖上一杯，温暖流入心间。",
					"Ingredient":"生梨，山药， 桂花， 生姜，肉桂和其它调料。",
					"Nutrition":"热量79k、蛋白质0g、脂肪0g、总碳水化合物19.3g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "甘薯南瓜汤",
					"Image": "warming/intro/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这是一款谁都不忍拒绝的冬日暖汤。浓厚香醇的甘薯、南瓜佐以莳萝和肉桂的辛香，富含膳食纤维，帮助促进胃动力，保持皮肤年轻柔软，给身心最温暖的呵护。",
					"Ingredient":"南瓜、甘薯、芹菜、小茴香、大蒜及数种暖体天然香料。",
					"Nutrition":"热量109k、蛋白质2.8g、脂肪0.4g、总碳水化合物25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "轻松红",
					"Image": "warming/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这款蔬果汁益肺解渴，轻松红由莲藕葡萄、甜菜根等主要滋养成分组成，也许是维果清系列产品里口味最棒的一款，喝下一口为您焕然一新，轻松一天，活力满满。",
					"Ingredient":"莲藕、红葡萄、脐橙、柠檬、苹果、甜菜根、柠檬草。",
					"Nutrition":"热量236k、蛋白质5.4g、脂肪0g、总碳水化合物58.6g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "维老吉",
					"Image": "warming/intro/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "灵感来源于凉茶但却可加热饮用，这是我们向传统致敬的一款特饮。这款润心肺的特饮是秋冬季节缓解秋乏、秋燥必不可少的健康饮品。",
					"Ingredient":"罗汉果、龙眼、甘草、红枣、梨、其它草本。",
					"Nutrition":"热量65k、蛋白质0g、脂肪0g、总碳水化合物15.5g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "经典黑",
					"Image": "warming/intro/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "matchamilk",
					"MaxCount": 50,
					"Description": "黑芝麻与杏仁的完美融合，爽滑入口。带给您有利心脏健康的脂肪、维生素E、Ω-3脂肪酸、纤维以及植物固醇。零添加的健康“芝麻糊”，只属于秋冬的幸福味道。",
					"Ingredient":"杏仁，黑芝麻",
					"Nutrition":"热量208k、蛋白质7.1g、脂肪17.4g、总碳水化合物8.9g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "醇厚白",
					"Image": "warming/intro/individual/darkwhite.png",
					"Price": 50.00,
					"Url": "darkwhite",
					"MaxCount": 50,
					"Description": "浓醇的巧克力也可以喝起来毫无负疚感！生机可可粉和生腰果为你带来各种有益身体的丰富营养，让您心花怒放的同时也能促进健康。今天的下午茶就用浓郁的醇厚白犒劳一下自己吧！",
					"Ingredient":"生腰果、龙舌兰、生可可粉、水。",
					"Nutrition":"热量349k、蛋白质12g、脂肪27g、总碳水化合物22.1g"
				}]
			}]
		}, {
			"Id": 31,
			"Name": "女士",
			"GroupTypeId": 2,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 580,
				"Day": 1,
				"Name": "暖体系列入门套餐2015女士",
				"PackagePrice": 300.00,
				"Gallery":[{"img":"warming/intro/set/f.jpg"},
				           {"img":"warming/intro/ingredients/chiagreen.jpg"},
				           {"img":"warming/intro/ingredients/classicblack.jpg"},
				           {"img":"warming/intro/ingredients/easyred.jpg"},
				           {"img":"warming/intro/ingredients/hotpear.jpg"},
				           {"img":"warming/intro/ingredients/luohanguo.jpg"},
				           {"img":"warming/intro/ingredients/pumpkinsoup.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "热梨生姜特饮",
					"Image": "warming/intro/individual/hotpear.png",
					"Price": 50.00,
					"Url": "pearandginger",
					"MaxCount": 50,
					"Description": "这是一款香甜却又带有一丝辛辣的梨制特饮。山药含有丰富的维生素B6，钾和镁，生梨带来甘甜和生津止渴，生姜、肉桂为您驱走寒冷，温润全身，带走多余的热量，在冬日的午后暖上一杯，温暖流入心间。",
					"Ingredient":"生梨，山药， 桂花， 生姜，肉桂和其它调料。",
					"Nutrition":"热量79k、蛋白质0g、脂肪0g、总碳水化合物19.3g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "甘薯南瓜汤",
					"Image": "warming/intro/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这是一款谁都不忍拒绝的冬日暖汤。浓厚香醇的甘薯、南瓜佐以莳萝和肉桂的辛香，富含膳食纤维，帮助促进胃动力，保持皮肤年轻柔软，给身心最温暖的呵护。",
					"Ingredient":"南瓜、甘薯、芹菜、小茴香、大蒜及数种暖体天然香料。",
					"Nutrition":"热量109k、蛋白质2.8g、脂肪0.4g、总碳水化合物25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "轻松红",
					"Image": "warming/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这款蔬果汁益肺解渴，轻松红由莲藕葡萄、甜菜根等主要滋养成分组成，也许是维果清系列产品里口味最棒的一款，喝下一口为您焕然一新，轻松一天，活力满满。",
					"Ingredient":"莲藕、红葡萄、脐橙、柠檬、苹果、甜菜根、柠檬草。",
					"Nutrition":"热量236k、蛋白质5.4g、脂肪0g、总碳水化合物58.6g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "维老吉",
					"Image": "warming/intro/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "灵感来源于凉茶但却可加热饮用，这是我们向传统致敬的一款特饮。这款润心肺的特饮是秋冬季节缓解秋乏、秋燥必不可少的健康饮品。",
					"Ingredient":"罗汉果、龙眼、甘草、红枣、梨、其它草本。",
					"Nutrition":"热量65k、蛋白质0g、脂肪0g、总碳水化合物15.5g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "经典黑",
					"Image": "warming/intro/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "matchamilk",
					"MaxCount": 50,
					"Description": "黑芝麻与杏仁的完美融合，爽滑入口。带给您有利心脏健康的脂肪、维生素E、Ω-3脂肪酸、纤维以及植物固醇。零添加的健康“芝麻糊”，只属于秋冬的幸福味道。",
					"Ingredient":"杏仁，黑芝麻",
					"Nutrition":"热量208k、蛋白质7.1g、脂肪17.4g、总碳水化合物8.9g"
				}]
			}]
		}]
	}]
}