{
	"IsSuccess": true,
	"Packages": [{
		"Id": 19,
		"Title": "冷压经典入门套餐2015",
		"SubTitle": "冷压经典入门套餐2015",
		"Description": "包含适合清体新手的美味冷压蔬果汁，配以浓醇香甜的醇厚白，为您的身体提供丰富的维生素、矿物质及其它优质的天然营养物质。以多种蔬菜、水果及坚果来源的植物营养替代您的日常饮食，帮助您实现清体目标，养成良好健康的饮食习惯。",
		"Url": "cleanse_warmingdetoxcn",
		"Tips": "女士套餐内每日含6瓶蔬果汁，男士套餐内每日8瓶蔬果汁。",
		"Introduction": "包含适合清体新手的美味冷压蔬果汁，配以浓醇香甜的醇厚白，为您的身体提供丰富的维生素、矿物质及其它优质的天然营养物质。以多种蔬菜、水果及坚果来源的植物营养替代您的日常饮食，帮助您实现清体目标，养成良好健康的饮食习惯。",
		"ImageName": "warming_detox.jpg",
		"Tags": [],
		"Groups": [{
			"Id": 30,
			"Name": "男士",
			"GroupTypeId": 1,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 573,
				"Day": 1,
				"Name": "冷压经典入门套餐2015男士",
				"PackagePrice": 400.00,
				"Gallery":[{"img":"cold/intro/set/m.jpg"},
				           {"img":"cold/intro/ingredients/chiagreen.jpg"},
				           {"img":"cold/intro/ingredients/classicwhite.jpg"},
				           {"img":"cold/intro/ingredients/darkwhite.jpg"},
				           {"img":"cold/intro/ingredients/easygreen.jpg"},
				           {"img":"cold/intro/ingredients/easyred.jpg"},
				           {"img":"cold/intro/ingredients/passionateyellow.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "挚爱黄",
					"Image": "cold/intro/individual/passionateyellow.png",
					"Price": 50.00,
					"Url": "passionateyellow",
					"MaxCount": 50,
					"Description": "清洌宜人、甘甜爽口构成了这款美丽特饮的独特标签。融合了根茎类植物的醇厚口感和热带水果的酸甜，让您身体的每个细胞都苏醒过来！脆爽的凉薯、醇甜的胡萝卜，搭配酸甜的百香果，让您品尝到来自热带的神秘芳香，为您的美丽加分。",
					"Ingredient":"胡萝卜、凉薯、百香果、柑橘、柠檬。",
					"Nutrition":"热量125k、蛋白质2.58g、脂肪0g、总碳水化合物28.9g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "芳香绿",
					"Image": "cold/intro/individual/easygreen.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "温柔而清新，是绿色蔬果汁中最甜美宜人的一款。黄瓜、菠萝和薄荷在提供充足人体必需的水分的同时，也让您从舌尖到胃都倍感轻松。",
					"Ingredient":"黄瓜、薄荷、菠萝、菠菜。",
					"Nutrition":"热量82k、蛋白质2.4g、脂肪0g、总碳水化合物18g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "醇厚白",
					"Image": "cold/intro/individual/darkwhite.png",
					"Price": 50.00,
					"Url": "darkwhite",
					"MaxCount": 50,
					"Description": "浓醇的巧克力也可以喝起来毫无负疚感！生机可可粉和生腰果为你带来各种有益身体的丰富营养，让您心花怒放的同时也能促进健康。今天的下午茶就用浓郁的醇厚白犒劳一下自己吧！",
					"Ingredient":"生腰果、龙舌兰、生可可粉、水。",
					"Nutrition":"热量349k、蛋白质12g、脂肪27g、总碳水化合物22.1g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "轻松红",
					"Image": "cold/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这款蔬果汁益肺解渴，轻松红由莲藕葡萄、甜菜根等主要滋养成分组成，也许是维果清系列产品里口味最棒的一款，喝下一口为您焕然一新，轻松一天，活力满满。",
					"Ingredient":"莲藕、红葡萄、脐橙、柠檬、苹果、甜菜根、柠檬草。",
					"Nutrition":"热量236k、蛋白质5.4g、脂肪0g、总碳水化合物58.6g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "cold/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "经典白",
					"Image": "cold/intro/individual/classicwhite.png",
					"Price": 50.00,
					"Url": "classicwhite",
					"MaxCount": 50,
					"Description": "黑色的礼裙永远不会不合时宜，而白色的腰果奶也永远不会让您失望。丰富的植物蛋白和多种矿物质，提供每日必需的基础营养。直接饮用的畅快，倒入咖啡的芳香，温热后的醇厚，她有您想要的一切！",
					"Ingredient":"生腰果、肉桂粉。",
					"Nutrition":"热量292k、蛋白质9.5g、脂肪22.8g、总碳水化合物16.8g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "cold/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "经典白",
					"Image": "cold/intro/individual/classicwhite.png",
					"Price": 50.00,
					"Url": "classicwhite",
					"MaxCount": 50,
					"Description": "黑色的礼裙永远不会不合时宜，而白色的腰果奶也永远不会让您失望。丰富的植物蛋白和多种矿物质，提供每日必需的基础营养。直接饮用的畅快，倒入咖啡的芳香，温热后的醇厚，她有您想要的一切！",
					"Ingredient":"生腰果、肉桂粉。",
					"Nutrition":"热量292k、蛋白质9.5g、脂肪22.8g、总碳水化合物16.8g"
				}]
			}]
		}, {
			"Id": 31,
			"Name": "女士",
			"GroupTypeId": 2,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 580,
				"Day": 1,
				"Name": "暖体系列入门套餐2015女士",
				"PackagePrice": 300.00,
				"Gallery":[{"img":"cold/intro/set/f.jpg"},
				           {"img":"cold/intro/ingredients/chiagreen.jpg"},
				           {"img":"cold/intro/ingredients/classicwhite.jpg"},
				           {"img":"cold/intro/ingredients/darkwhite.jpg"},
				           {"img":"cold/intro/ingredients/easygreen.jpg"},
				           {"img":"cold/intro/ingredients/easyred.jpg"},
				           {"img":"cold/intro/ingredients/passionateyellow.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "挚爱黄",
					"Image": "cold/intro/individual/passionateyellow.png",
					"Price": 50.00,
					"Url": "passionateyellow",
					"MaxCount": 50,
					"Description": "清洌宜人、甘甜爽口构成了这款美丽特饮的独特标签。融合了根茎类植物的醇厚口感和热带水果的酸甜，让您身体的每个细胞都苏醒过来！脆爽的凉薯、醇甜的胡萝卜，搭配酸甜的百香果，让您品尝到来自热带的神秘芳香，为您的美丽加分。",
					"Ingredient":"胡萝卜、凉薯、百香果、柑橘、柠檬。",
					"Nutrition":"热量125k、蛋白质2.58g、脂肪0g、总碳水化合物28.9g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "芳香绿",
					"Image": "cold/intro/individual/easygreen.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "温柔而清新，是绿色蔬果汁中最甜美宜人的一款。黄瓜、菠萝和薄荷在提供充足人体必需的水分的同时，也让您从舌尖到胃都倍感轻松。",
					"Ingredient":"黄瓜、薄荷、菠萝、菠菜。",
					"Nutrition":"热量82k、蛋白质2.4g、脂肪0g、总碳水化合物18g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "醇厚白",
					"Image": "cold/intro/individual/darkwhite.png",
					"Price": 50.00,
					"Url": "darkwhite",
					"MaxCount": 50,
					"Description": "浓醇的巧克力也可以喝起来毫无负疚感！生机可可粉和生腰果为你带来各种有益身体的丰富营养，让您心花怒放的同时也能促进健康。今天的下午茶就用浓郁的醇厚白犒劳一下自己吧！",
					"Ingredient":"生腰果、龙舌兰、生可可粉、水。",
					"Nutrition":"热量349k、蛋白质12g、脂肪27g、总碳水化合物22.1g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "轻松红",
					"Image": "cold/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这款蔬果汁益肺解渴，轻松红由莲藕葡萄、甜菜根等主要滋养成分组成，也许是维果清系列产品里口味最棒的一款，喝下一口为您焕然一新，轻松一天，活力满满。",
					"Ingredient":"莲藕、红葡萄、脐橙、柠檬、苹果、甜菜根、柠檬草。",
					"Nutrition":"热量236k、蛋白质5.4g、脂肪0g、总碳水化合物58.6g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "cold/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "经典白",
					"Image": "cold/intro/individual/classicwhite.png",
					"Price": 50.00,
					"Url": "classicwhite",
					"MaxCount": 50,
					"Description": "黑色的礼裙永远不会不合时宜，而白色的腰果奶也永远不会让您失望。丰富的植物蛋白和多种矿物质，提供每日必需的基础营养。直接饮用的畅快，倒入咖啡的芳香，温热后的醇厚，她有您想要的一切！",
					"Ingredient":"生腰果、肉桂粉。",
					"Nutrition":"热量292k、蛋白质9.5g、脂肪22.8g、总碳水化合物16.8g"
				}]
			}]
		}]
	}]
}