{
	"IsSuccess": true,
	"Packages": [{
		"Id": 19,
		"Title": "暖体系列大师套餐2015",
		"SubTitle": "暖体系列大师套餐2015",
		"Description": "维果清大师暖体套装特别融入更多绿色，为您带来更彻底的清体体验，精心甄选了坚果、草本等滋养成分，经过数百次口味及配方对比调制，融入更多绿色，N种蔬果营养一口搞定！全程温润呵护您的每一个秋冬日。",
		"Url": "cleanse_warmingdetoxcn",
		"Tips": "女士套餐6瓶，男士套餐8瓶。温馨小贴士：部分饮品加热后口感更美味哦，套餐每日配送的菜单口味相同。",
		"Introduction": "维果清大师暖体套装特别融入更多绿色，为您带来更彻底的清体体验，精心甄选了坚果、草本等滋养成分，经过数百次口味及配方对比调制，融入更多绿色，N种蔬果营养一口搞定！全程温润呵护您的每一个秋冬日。",
		"ImageName": "warming_detox.jpg",
		"Tags": [],
		"Groups": [{
			"Id": 30,
			"Name": "男士",
			"GroupTypeId": 1,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 573,
				"Day": 1,
				"Name": "暖体系列入门套餐2015男士",
				"PackagePrice": 400.00,
				"Gallery":[{"img":"warming/advanced/set/m.jpg"},
				           {"img":"warming/advanced/ingredients/chiagreen.jpg"},
				           {"img":"warming/advanced/ingredients/classicblack.jpg"},
				           {"img":"warming/advanced/ingredients/darkgreen.jpg"},
				           {"img":"warming/advanced/ingredients/darkwhite.jpg"},
				           {"img":"warming/advanced/ingredients/purplebroth.jpg"},
				           {"img":"warming/advanced/ingredients/luohanguo.jpg"},
				           {"img":"warming/advanced/ingredients/pumpkinsoup.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/advanced/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "chiagreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "维老吉",
					"Image": "warming/advanced/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "灵感来源于凉茶但却可加热饮用，这是我们向传统致敬的一款特饮。这款润心肺的特饮是秋冬季节缓解秋乏、秋燥必不可少的健康饮品。",
					"Ingredient":"罗汉果、龙眼、甘草、红枣、梨、其它草本。",
					"Nutrition":"热量65k、蛋白质0g、脂肪0g、总碳水化合物15.5g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "甘薯南瓜汤",
					"Image": "warming/advanced/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这是一款谁都不忍拒绝的冬日暖汤。浓厚香醇的甘薯、南瓜佐以莳萝和肉桂的辛香，富含膳食纤维，帮助促进胃动力，保持皮肤年轻柔软，给身心最温暖的呵护。",
					"Ingredient":"南瓜、甘薯、芹菜、小茴香、大蒜及数种暖体天然香料。",
					"Nutrition":"热量109k、蛋白质2.8g、脂肪0.4g、总碳水化合物25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "醇厚绿",
					"Image": "warming/advanced/individual/darkgreen.png",
					"Price": 50.00,
					"Url": "darkgreen",
					"MaxCount": 50,
					"Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
					"Ingredient":"菠菜等深色绿叶菜家族、苹果、柑橘。",
					"Nutrition":"热量112k、蛋白质8.4g、脂肪0g、总碳水化合物21.2g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "紫甘蓝",
					"Image": "warming/advanced/individual/purplebroth.png",
					"Price": 50.00,
					"Url": "purplebroth",
					"MaxCount": 50,
					"Description": "Nourishing and detoxifying purple coloured broth made from 10 different types of veggies. Low in calories, high in antioxidants and minerals. Purple cabbage is the superstar in this soup: a natural source of electrolytes and minerals like calcium, potassium and manganese, purple cabbage can help control heart-rate and blood pressure. Plus, the pigment anthocyanins, that give purple its beautiful colour may also be capable of reducing growth of cancer cells. Get ready to feel bold and beautiful!",
					"Ingredient":"Purple cabbage, carrot, tomato, broccoli, celery, onion, sea salt.",
					"Nutrition":"热量34k、蛋白质1.3g、脂肪0g、总碳水化合物7g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "经典黑",
					"Image": "warming/advanced/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "blacksesame",
					"MaxCount": 50,
					"Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
					"Ingredient":"Almonds and black sesame.",
					"Nutrition":"热量208k、蛋白质7.1g、脂肪17.4g、总碳水化合物8.9g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/advanced/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "醇厚白",
					"Image": "warming/advanced/individual/darkwhite.png",
					"Price": 50.00,
					"Url": "darkwhite",
					"MaxCount": 50,
					"Description": "浓醇的巧克力也可以喝起来毫无负疚感！生机可可粉和生腰果为你带来各种有益身体的丰富营养，让您心花怒放的同时也能促进健康。今天的下午茶就用浓郁的醇厚白犒劳一下自己吧！",
					"Ingredient":"生腰果、龙舌兰、生可可粉、水。",
					"Nutrition":"热量349k、蛋白质12g、脂肪27g、总碳水化合物22.1g"
				}]
			}]
		}, {
			"Id": 31,
			"Name": "女士",
			"GroupTypeId": 2,
			"ByWeek": true,
			
			"Rules": [{
				"Id": 580,
				"Day": 1,
				"Name": "暖体系列入门套餐2015女士",
				"PackagePrice": 300.00,
				"Gallery":[{"img":"warming/advanced/set/f.jpg"},
				           {"img":"warming/advanced/ingredients/chiagreen.jpg"},
				           {"img":"warming/advanced/ingredients/classicblack.jpg"},
				           {"img":"warming/advanced/ingredients/darkgreen.jpg"},
				           {"img":"warming/advanced/ingredients/easyred.jpg"},
				           {"img":"warming/advanced/ingredients/purplebroth.jpg"},
				           {"img":"warming/advanced/ingredients/luohanguo.jpg"},
				           {"img":"warming/advanced/ingredients/pumpkinsoup.jpg"}
						],
				"Products": [{
					"Count": 1,
					"Id": 14,
					"Name": "奇亚绿",
					"Image": "warming/advanced/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "chiagreen",
					"MaxCount": 50,
					"Description": "她也许是您最需要的一款清体健康蔬果饮品。这款310ml的小绿给你带来百分百的纯蔬果汁清爽口感，喝下一口倍感能量满满，仿佛沉浸在田园清香的氛围中，你焕然一新的一天由奇亚绿为你打开。",
					"Ingredient":"菠菜、黄瓜、苹果、芡欧鼠尾草。",
					"Nutrition":"热量129k、蛋白质4.6g、脂肪2.9g、总碳水化合物25.7g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "维老吉",
					"Image": "warming/advanced/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "灵感来源于凉茶但却可加热饮用，这是我们向传统致敬的一款特饮。这款润心肺的特饮是秋冬季节缓解秋乏、秋燥必不可少的健康饮品。",
					"Ingredient":"罗汉果、龙眼、甘草、红枣、梨、其它草本。",
					"Nutrition":"热量65k、蛋白质0g、脂肪0g、总碳水化合物15.5g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "甘薯南瓜汤",
					"Image": "warming/advanced/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "这是一款谁都不忍拒绝的冬日暖汤。浓厚香醇的甘薯、南瓜佐以莳萝和肉桂的辛香，富含膳食纤维，帮助促进胃动力，保持皮肤年轻柔软，给身心最温暖的呵护。",
					"Ingredient":"南瓜、甘薯、芹菜、小茴香、大蒜及数种暖体天然香料。",
					"Nutrition":"热量109k、蛋白质2.8g、脂肪0.4g、总碳水化合物25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "醇厚绿",
					"Image": "warming/advanced/individual/darkgreen.png",
					"Price": 50.00,
					"Url": "darkgreen",
					"MaxCount": 50,
					"Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
					"Ingredient":"菠菜等深色绿叶菜家族、苹果、柑橘。",
					"Nutrition":"热量112k、蛋白质8.4g、脂肪0g、总碳水化合物21.2g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "紫甘蓝",
					"Image": "warming/advanced/individual/purplebroth.png",
					"Price": 50.00,
					"Url": "purplebroth",
					"MaxCount": 50,
					"Description": "Nourishing and detoxifying purple coloured broth made from 10 different types of veggies. Low in calories, high in antioxidants and minerals. Purple cabbage is the superstar in this soup: a natural source of electrolytes and minerals like calcium, potassium and manganese, purple cabbage can help control heart-rate and blood pressure. Plus, the pigment anthocyanins, that give purple its beautiful colour may also be capable of reducing growth of cancer cells. Get ready to feel bold and beautiful!",
					"Ingredient":"Purple cabbage, carrot, tomato, broccoli, celery, onion, sea salt.",
					"Nutrition":"热量34k、蛋白质1.3g、脂肪0g、总碳水化合物7g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "经典黑",
					"Image": "warming/advanced/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "blacksesame",
					"MaxCount": 50,
					"Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
					"Ingredient":"Almonds and black sesame.",
					"Nutrition":"热量208k、蛋白质7.1g、脂肪17.4g、总碳水化合物8.9g"
				}]
			}]
		}]
	}]
}