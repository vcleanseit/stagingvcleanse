{
    "IsSuccess": true,
    "Packages": [
        {
            "Id": 35,
            "Title": "Intro Classic Cleanse",
            "SubTitle": "-",
            "Description": "Our Classic Cleanse is just as its name states: a classic. Delicious cold-pressed juices and decadent nutmilks make this your secret weapon to looking and feeling amazing. Our Intro Classic Cleanse is created for those who are relatively new to cleansing and want to experience the best that raw foods and juicing has to offer.",
            "Url": "",
            "Tips": "Females receive 6 bottles per day, and males receive 8 bottles per day.",
            "Introduction": "Our Classic Cleanse is just as its name states: a classic. Delicious cold-pressed juices and decadent nutmilks make this your secret weapon to looking and feeling amazing. Our Intro Classic Cleanse is created for those who are relatively new to cleansing and want to experience the best that raw foods and juicing has to offer.",
            "ImageName": "",
            "Tags": [],
            "Groups": [
                {
                    "Id": 54,
                    "Name": "Male",
                    "GroupTypeId": 1,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 638,
                            "Day": 1,
                            "Name": "Intro Classic Cleanse",
                            "PackagePrice": 400,
                "Gallery": [{
                    "img": "cold/intro/set/m.jpg"
                }, {
                    "img": "cold/intro/ingredients/chiagreen.jpg"
                }, {
                    "img": "cold/intro/ingredients/classicwhite.jpg"
                }, {
                    "img": "cold/intro/ingredients/darkwhite.jpg"
                }, {
                    "img": "cold/intro/ingredients/easygreen.jpg"
                }, {
                    "img": "cold/intro/ingredients/easyred.jpg"
                }, {
                    "img": "cold/intro/ingredients/passionateyellow.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 46,
                    "Name": "Passionate Yellow",
                    "Image": "cold/intro/individual/passionateyellow.png",
                    "Price": 50.00,
                    "Url": "passionateyellow",
                    "MaxCount": 50,
                    "Description": "This juice bursts with bright and warm tones that can awaken every cell of your body!  Jicama, rich in potassium, copper and iron, helps manage blood pressure and is good for circulation. Carrots add falcarinol with its cancer-preventing properties. Passion fruit adds Vitamin C, and anti-oxidants.",
                    "Ingredient": "Carrot, Jicama, Orange, Passion fruit, Lemon.",
                    "Nutrition": "Calories 125k、Protein 2.58g、Total Fat 0g、Total Carb 28.9g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "cold/intro/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Dark White",
                    "Image": "cold/intro/individual/darkwhite.png",
                    "Price": 50.00,
                    "Url": "darkwhite",
                    "MaxCount": 50,
                    "Description": "To be honest, we were craving chocolate. An intensely rich, intensely decadent cup of fantasy hot chocolate that would nonetheless be healthy! This one hits the mark pretty well, with nearly 10 grams of plant protein and heart healthy fats per bottle.",
                    "Ingredient": "Raw cacao, agave and cashews.",
                    "Nutrition": "Calories 349k、Protein 12g、Total Fat 27g、Total Carb 22.1g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Easy Red",
                    "Image": "cold/intro/individual/easyred.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
                    "Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
                    "Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/intro/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Classic White",
                    "Image": "cold/intro/individual/classicwhite.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Nutmilks provide protein and heart healthy fats on a cleanse. Just like the little black dress, one can always do with a really good and luscious cashew nutmilk. Drink it straight, pour it in coffee or tea, warm it up for a thicker texture. It's delicious regardless of the context.",
                    "Ingredient": "Raw cashews and cinnamon.",
                    "Nutrition": "Calories 292k、Protein 9.5g、Total Fat 22.8g、Total Carb 16.8g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/intro/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Classic White",
                    "Image": "cold/intro/individual/classicwhite.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Nutmilks provide protein and heart healthy fats on a cleanse. Just like the little black dress, one can always do with a really good and luscious cashew nutmilk. Drink it straight, pour it in coffee or tea, warm it up for a thicker texture. It's delicious regardless of the context.",
                    "Ingredient": "Raw cashews and cinnamon.",
                    "Nutrition": "Calories 292k、Protein 9.5g、Total Fat 22.8g、Total Carb 16.8g"
                }]
                        }
                    ]
                },
                {
                    "Id": 55,
                    "Name": "Female",
                    "GroupTypeId": 2,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 639,
                            "Day": 1,
                            "Name": "Intro Classic Cleanse 1",
                            "PackagePrice": 300,
                "Gallery": [{
                    "img": "cold/intro/set/f.jpg"
                }, {
                    "img": "cold/intro/ingredients/chiagreen.jpg"
                }, {
                    "img": "cold/intro/ingredients/classicwhite.jpg"
                }, {
                    "img": "cold/intro/ingredients/darkwhite.jpg"
                }, {
                    "img": "cold/intro/ingredients/easygreen.jpg"
                }, {
                    "img": "cold/intro/ingredients/easyred.jpg"
                }, {
                    "img": "cold/intro/ingredients/passionateyellow.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 46,
                    "Name": "Passionate Yellow",
                    "Image": "cold/intro/individual/passionateyellow.png",
                    "Price": 50.00,
                    "Url": "passionateyellow",
                    "MaxCount": 50,
                    "Description": "This juice bursts with bright and warm tones that can awaken every cell of your body!  Jicama, rich in potassium, copper and iron, helps manage blood pressure and is good for circulation. Carrots add falcarinol with its cancer-preventing properties. Passion fruit adds Vitamin C, and anti-oxidants.",
                    "Ingredient": "Carrot, Jicama, Orange, Passion fruit, Lemon.",
                    "Nutrition": "Calories 125k、Protein 2.58g、Total Fat 0g、Total Carb 28.9g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "cold/intro/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Dark White",
                    "Image": "cold/intro/individual/darkwhite.png",
                    "Price": 50.00,
                    "Url": "darkwhite",
                    "MaxCount": 50,
                    "Description": "To be honest, we were craving chocolate. An intensely rich, intensely decadent cup of fantasy hot chocolate that would nonetheless be healthy! This one hits the mark pretty well, with nearly 10 grams of plant protein and heart healthy fats per bottle.",
                    "Ingredient": "Raw cacao, agave and cashews.",
                    "Nutrition": "Calories 349k、Protein 12g、Total Fat 27g、Total Carb 22.1g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Easy Red",
                    "Image": "cold/intro/individual/easyred.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
                    "Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
                    "Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/intro/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Classic White",
                    "Image": "cold/intro/individual/classicwhite.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Nutmilks provide protein and heart healthy fats on a cleanse. Just like the little black dress, one can always do with a really good and luscious cashew nutmilk. Drink it straight, pour it in coffee or tea, warm it up for a thicker texture. It's delicious regardless of the context.",
                    "Ingredient": "Raw cashews and cinnamon.",
                    "Nutrition": "Calories 292k、Protein 9.5g、Total Fat 22.8g、Total Carb 16.8g"
                }]
                        }
                    ]
                }
            ]
        }
    ]
}