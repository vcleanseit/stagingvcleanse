{
    "IsSuccess": true,
    "Packages": [{
        "Id": 41,
        "Title": "Healthy Meal Pack",
        "SubTitle": "-",
        "Description": "The perfect healthy meal on the go. Have a delicious soup or nutmilk and a green juice. Good for two meal replacements.",
        "Url": "",
        "Tips": "",
        "Introduction": "The perfect healthy meal on the go. Have a delicious soup or nutmilk and a green juice. Good for two meal replacements.",
        "ImageName": "",
        "Tags": [],
        "Groups": [{
            "Id": 62,
            "Name": "-",
            "GroupTypeId": 0,
            "ByWeek": false,
            "Rules": [{
                "Id": 646,
                "Day": 1,
                "Name": "Healthy Meal Pack",
                "PackagePrice": 200,
                "Gallery": [{
                    "img": "recommend/healthymeal/set/healthymeal.jpg"
                }, {
                    "img": "recommend/healthymeal/ingredients/blacksesame.jpg"
                }, {
                    "img": "recommend/healthymeal/ingredients/pumpkinsoup.jpg"
                }, {
                    "img": "recommend/healthymeal/ingredients/chiagreen.jpg"
                }, {
                    "img": "recommend/healthymeal/ingredients/chiagreen.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 48,
                    "Name": "Classic Black",
                    "Image": "recommend/healthymeal/individual/blacksesame.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
                    "Ingredient": "Almonds and black sesame.",
                    "Nutrition": "Calories 208k、Protein 7.1g、Total Fat 17.4g、Total Carb 8.9g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Sweet potato pumpkin soup",
                    "Image": "recommend/healthymeal/individual/pumpkinsoup.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Slightly spicy winter soup that will keep you warm. Creamy blend of sweet potatoes, pumpkin and warming spices like cumin and cinnamon. Sweet potatoes are rich in calcium, potassium, vitamins A and C and magnesium, a mineral that helps you relax and de-stress. Did we mention cumin and cinnamon are great digestive aids? Enjoy!",
                    "Ingredient": "Pumpkin, sweet potato, celery, cumin, garlic and natural warming spices.",
                    "Nutrition": "Calories 109k、Protein 2.8g、Total Fat 0.4g、Total Carb 25g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "recommend/healthymeal/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "recommend/healthymeal/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }]
            }]
        }]
    }]
}