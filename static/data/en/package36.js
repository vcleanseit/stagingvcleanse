{
    "IsSuccess": true,
    "Packages": [
        {
            "Id": 36,
            "Title": "Advanced Classic Cleanse",
            "SubTitle": "-",
            "Description": "Lighter and with more green juice than the Intro Classic Cleanse, this cleanse is suitable for more advanced juicers. Each bottle of green juice contains approximately ? kg of veggies and fruits, which means literally washing your insides with the best that nature has to offer.",
            "Url": "",
            "Tips": "Females receive 6 bottles per day, and males receive 8 bottles per day.",
            "Introduction": "Lighter and with more green juice than the Intro Classic Cleanse, this cleanse is suitable for more advanced juicers. Each bottle of green juice contains approximately ? kg of veggies and fruits, which means literally washing your insides with the best that nature has to offer.",
            "ImageName": "",
            "Tags": [],
            "Groups": [
                {
                    "Id": 56,
                    "Name": "Male",
                    "GroupTypeId": 1,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 640,
                            "Day": 1,
                            "Name": "Advanced Classic Cleanse",
                            "PackagePrice": 400,
                "Gallery": [{
                    "img": "cold/advanced/set/m.jpg"
                }, {
                    "img": "cold/advanced/ingredients/chiagreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/passionateyellow.jpg"
                }, {
                    "img": "cold/advanced/ingredients/easyred.jpg"
                }, {
                    "img": "cold/advanced/ingredients/darkgreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/easygreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/classicwhite.jpg"
                }, {
                    "img": "cold/advanced/ingredients/darkwhite.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "chiagreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 46,
                    "Name": "Passionate Yellow",
                    "Image": "cold/advanced/individual/passionateyellow.png",
                    "Price": 50.00,
                    "Url": "passionateyellow",
                    "MaxCount": 50,
                    "Description": "This juice bursts with bright and warm tones that can awaken every cell of your body!  Jicama, rich in potassium, copper and iron, helps manage blood pressure and is good for circulation. Carrots add falcarinol with its cancer-preventing properties. Passion fruit adds Vitamin C, and anti-oxidants.",
                    "Ingredient": "Carrot, Jicama, Orange, Passion fruit, Lemon.",
                    "Nutrition": "Calories 125k、Protein 2.58g、Total Fat 0g、Total Carb 28.9g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Easy Red",
                    "Image": "cold/advanced/individual/easyred.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
                    "Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
                    "Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Dark Green",
                    "Image": "cold/advanced/individual/darkgreen.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
                    "Ingredient": "Dark leafy greens from spinach to its more exotic cousins, with apples and a citrus twist.",
                    "Nutrition": "Calories 112k、Protein 8.4g、Total Fat 0g、Total Carb 21.2g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "cold/advanced/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }, {
                    "Count": 1,
                    "Id": 21,
                    "Name": "Classic White",
                    "Image": "cold/advanced/individual/classicwhite.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Nutmilks provide protein and heart healthy fats on a cleanse. Just like the little black dress, one can always do with a really good and luscious cashew nutmilk. Drink it straight, pour it in coffee or tea, warm it up for a thicker texture. It's delicious regardless of the context.",
                    "Ingredient": "Raw cashews and cinnamon.",
                    "Nutrition": "Calories 292k、Protein 9.5g、Total Fat 22.8g、Total Carb 16.8g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 23,
                    "Name": "Dark White",
                    "Image": "cold/advanced/individual/darkwhite.png",
                    "Price": 50.00,
                    "Url": "darkwhite",
                    "MaxCount": 50,
                    "Description": "To be honest, we were craving chocolate. An intensely rich, intensely decadent cup of fantasy hot chocolate that would nonetheless be healthy! This one hits the mark pretty well, with nearly 10 grams of plant protein and heart healthy fats per bottle.",
                    "Ingredient": "Raw cacao, agave and cashews.",
                    "Nutrition": "Calories 349k、Protein 12g、Total Fat 27g、Total Carb 22.1g"
                }]
                        }
                    ]
                },
                {
                    "Id": 57,
                    "Name": "Female",
                    "GroupTypeId": 2,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 641,
                            "Day": 1,
                            "Name": "Advanced Classic Cleanse 1",
                            "PackagePrice": 300,
                "Gallery": [{
                    "img": "cold/advanced/set/f.jpg"
                }, {
                    "img": "cold/advanced/ingredients/chiagreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/passionateyellow.jpg"
                }, {
                    "img": "cold/advanced/ingredients/easyred.jpg"
                }, {
                    "img": "cold/advanced/ingredients/darkgreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/easygreen.jpg"
                }, {
                    "img": "cold/advanced/ingredients/darkwhite.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "cold/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "chiagreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 46,
                    "Name": "Passionate Yellow",
                    "Image": "cold/advanced/individual/passionateyellow.png",
                    "Price": 50.00,
                    "Url": "passionateyellow",
                    "MaxCount": 50,
                    "Description": "This juice bursts with bright and warm tones that can awaken every cell of your body!  Jicama, rich in potassium, copper and iron, helps manage blood pressure and is good for circulation. Carrots add falcarinol with its cancer-preventing properties. Passion fruit adds Vitamin C, and anti-oxidants.",
                    "Ingredient": "Carrot, Jicama, Orange, Passion fruit, Lemon.",
                    "Nutrition": "Calories 125k、Protein 2.58g、Total Fat 0g、Total Carb 28.9g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Easy Red",
                    "Image": "cold/advanced/individual/easyred.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
                    "Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
                    "Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Dark Green",
                    "Image": "cold/advanced/individual/darkgreen.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
                    "Ingredient": "Dark leafy greens from spinach to its more exotic cousins, with apples and a citrus twist.",
                    "Nutrition": "Calories 112k、Protein 8.4g、Total Fat 0g、Total Carb 21.2g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "cold/advanced/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }, {
                    "Count": 1,
                    "Id": 21,
                    "Name": "Classic White",
                    "Image": "cold/advanced/individual/classicwhite.png",
                    "Price": 50.00,
                    "Url": "classicwhite",
                    "MaxCount": 50,
                    "Description": "Nutmilks provide protein and heart healthy fats on a cleanse. Just like the little black dress, one can always do with a really good and luscious cashew nutmilk. Drink it straight, pour it in coffee or tea, warm it up for a thicker texture. It's delicious regardless of the context.",
                    "Ingredient": "Raw cashews and cinnamon.",
                    "Nutrition": "Calories 292k、Protein 9.5g、Total Fat 22.8g、Total Carb 16.8g"
                }]
                        }
                    ]
                }
            ]
        }
    ]
}