{
    "IsSuccess": true,
    "Packages": [
        {
            "Id": 33,
            "Title": "Intro Warming Cleanse ",
            "SubTitle": "-",
            "Description": "Our Warming Cleanse consists of soups and infusions that are meant to be drunk hot, in addition to cold-pressed juices and nutmilks. This cleanse is perfect when the weather is cold, or when you need something soothing and nourishing to warm you up. Flavors are more complex and savory than in the Classic Cleanse but calories are equally controlled. Our Warming Cleanse recipes focus on boosting immunity, lung and throat health, and skin hydration.",
            "Url": "",
            "Tips": "Females receive 6 bottles per day, and males receive 8 bottles per day. Warm Tip: more delicious after heating.",
            "Introduction": "Our Warming Cleanse consists of soups and infusions that are meant to be drunk hot, in addition to cold-pressed juices and nutmilks. This cleanse is perfect when the weather is cold, or when you need something soothing and nourishing to warm you up. Flavors are more complex and savory than in the Classic Cleanse but calories are equally controlled. Our Warming Cleanse recipes focus on boosting immunity, lung and throat health, and skin hydration.",
            "ImageName": "",
            "Tags": [],
            "Groups": [
                {
                    "Id": 50,
                    "Name": "Male",
                    "GroupTypeId": 1,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 634,
                            "Day": 1,
                            "Name": "Intro Warming Cleanse",
                            "PackagePrice": 400,
				"Gallery": [{
					"img": "warming/intro/set/m.jpg"
				}, {
					"img": "warming/intro/ingredients/chiagreen.jpg"
				}, {
					"img": "warming/intro/ingredients/classicblack.jpg"
				}, {
					"img": "warming/intro/ingredients/darkwhite.jpg"
				}, {
					"img": "warming/intro/ingredients/easyred.jpg"
				}, {
					"img": "warming/intro/ingredients/hotpear.jpg"
				}, {
					"img": "warming/intro/ingredients/luohanguo.jpg"
				}, {
					"img": "warming/intro/ingredients/pumpkinsoup.jpg"
				}],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "Hot pear and ginger infusion",
					"Image": "warming/intro/individual/hotpear.png",
					"Price": 50.00,
					"Url": "pearandginger",
					"MaxCount": 50,
					"Description": "We wanted the perfect drink for when it’s cold out, or when you’re cold even on a hot summer’s day. Drink this and feel all warmed up.",
					"Ingredient": "Pear, Chinese yam, osmanthus, ginger, cinnamon and other spices.",
					"Nutrition": "Calories 79k、Protein 0g、Total Fat 0g、Total Carb 19.3g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "The Chia Green",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
					"Ingredient": "Spinach, cucumber, apple, chia.",
					"Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "Sweet potato pumpkin soup",
					"Image": "warming/intro/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "Slightly spicy winter soup that will keep you warm. Creamy blend of sweet potatoes, pumpkin and warming spices like cumin and cinnamon. Sweet potatoes are rich in calcium, potassium, vitamins A and C and magnesium, a mineral that helps you relax and de-stress. Did we mention cumin and cinnamon are great digestive aids? Enjoy!",
					"Ingredient": "Pumpkin, sweet potato, celery, cumin, garlic and natural warming spices.",
					"Nutrition": "Calories 109k、Protein 2.8g、Total Fat 0.4g、Total Carb 25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "Easy Red",
					"Image": "warming/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
					"Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
					"Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "Luo Han Guo Infusion",
					"Image": "warming/intro/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "Besides being a great sweetener for diabetics, Luo Han fruit is said to enhance longevity. Licorice root is used for various digestive complaints, including heartburn, colic and ulcers. Longan is used as a remedy for fatigue and insomnia. This herbal blend has a calming and nourishing effect on the nervous system and is a wonderful remedy for cough and cold.",
					"Ingredient": "Luo Han fruit, longan, licorice root, jujube, pear and herbs.",
					"Nutrition": "Calories 65k、Protein 0g、Total Fat 0g、Total Carb 15.5g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "Classic Black",
					"Image": "warming/intro/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "matchamilk",
					"MaxCount": 50,
					"Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
					"Ingredient": "Almonds and black sesame.",
					"Nutrition": "Calories 208k、Protein 7.1g、Total Fat 17.4g、Total Carb 8.9g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "The Chia Green",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
					"Ingredient": "Spinach, cucumber, apple, chia.",
					"Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "Dark White",
					"Image": "warming/intro/individual/darkwhite.png",
					"Price": 50.00,
					"Url": "darkwhite",
					"MaxCount": 50,
					"Description": "To be honest, we were craving chocolate. An intensely rich, intensely decadent cup of fantasy hot chocolate that would nonetheless be healthy! This one hits the mark pretty well, with nearly 10 grams of plant protein and heart healthy fats per bottle.",
					"Ingredient": "Raw cacao, agave and cashews.",
					"Nutrition": "Calories 349k、Protein 12g、Total Fat 27g、Total Carb 22.1g"
				}]
                        }
                    ]
                },
                {
                    "Id": 51,
                    "Name": "Female",
                    "GroupTypeId": 2,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 635,
                            "Day": 1,
                            "Name": "Intro Warming Cleanse",
                            "PackagePrice": 300,
				"Gallery": [{
					"img": "warming/intro/set/f.jpg"
				}, {
					"img": "warming/intro/ingredients/chiagreen.jpg"
				}, {
					"img": "warming/intro/ingredients/classicblack.jpg"
				}, {
					"img": "warming/intro/ingredients/easyred.jpg"
				}, {
					"img": "warming/intro/ingredients/hotpear.jpg"
				}, {
					"img": "warming/intro/ingredients/luohanguo.jpg"
				}, {
					"img": "warming/intro/ingredients/pumpkinsoup.jpg"
				}],
				"Products": [{
					"Count": 1,
					"Id": 46,
					"Name": "Hot pear and ginger infusion",
					"Image": "warming/intro/individual/hotpear.png",
					"Price": 50.00,
					"Url": "pearandginger",
					"MaxCount": 50,
					"Description": "We wanted the perfect drink for when it’s cold out, or when you’re cold even on a hot summer’s day. Drink this and feel all warmed up.",
					"Ingredient": "Pear, Chinese yam, osmanthus, ginger, cinnamon and other spices.",
					"Nutrition": "Calories 79k、Protein 0g、Total Fat 0g、Total Carb 19.3g"
				}, {
					"Count": 1,
					"Id": 14,
					"Name": "The Chia Green",
					"Image": "warming/intro/individual/chiagreen.png",
					"Price": 50.00,
					"Url": "easygreen",
					"MaxCount": 50,
					"Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
					"Ingredient": "Spinach, cucumber, apple, chia.",
					"Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
				}, {
					"Count": 1,
					"Id": 47,
					"Name": "Sweet potato pumpkin soup",
					"Image": "warming/intro/individual/pumpkinsoup.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "Slightly spicy winter soup that will keep you warm. Creamy blend of sweet potatoes, pumpkin and warming spices like cumin and cinnamon. Sweet potatoes are rich in calcium, potassium, vitamins A and C and magnesium, a mineral that helps you relax and de-stress. Did we mention cumin and cinnamon are great digestive aids? Enjoy!",
					"Ingredient": "Pumpkin, sweet potato, celery, cumin, garlic and natural warming spices.",
					"Nutrition": "Calories 109k、Protein 2.8g、Total Fat 0.4g、Total Carb 25g"
				}, {
					"Count": 1,
					"Id": 19,
					"Name": "Easy Red",
					"Image": "warming/intro/individual/easyred.png",
					"Price": 50.00,
					"Url": "redbeetsoup",
					"MaxCount": 50,
					"Description": "Protect your lungs and quench your thirst! Lotus root is traditionally used to treat respiratory problems and lung-related ailments, and to increase energy and neutralize toxins. This delicate juice boosts immunity, stimulates circulation and rejuvenates skin.",
					"Ingredient": "Lotus, Red Grapes, Orange, Lemon, Apple, Beetroot, Lemongrass.",
					"Nutrition": "Calories 236k、Protein 5.4g、Total Fat 0g、Total Carb 58.6g"
				}, {
					"Count": 1,
					"Id": 84,
					"Name": "Luo Han Guo Infusion",
					"Image": "warming/intro/individual/luohanguo.png",
					"Price": 50.00,
					"Url": "luohanguo",
					"MaxCount": 50,
					"Description": "Besides being a great sweetener for diabetics, Luo Han fruit is said to enhance longevity. Licorice root is used for various digestive complaints, including heartburn, colic and ulcers. Longan is used as a remedy for fatigue and insomnia. This herbal blend has a calming and nourishing effect on the nervous system and is a wonderful remedy for cough and cold.",
					"Ingredient": "Luo Han fruit, longan, licorice root, jujube, pear and herbs.",
					"Nutrition": "Calories 65k、Protein 0g、Total Fat 0g、Total Carb 15.5g"
				}, {
					"Count": 1,
					"Id": 48,
					"Name": "Classic Black",
					"Image": "warming/intro/individual/blacksesame.png",
					"Price": 50.00,
					"Url": "matchamilk",
					"MaxCount": 50,
					"Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
					"Ingredient": "Almonds and black sesame.",
					"Nutrition": "Calories 208k、Protein 7.1g、Total Fat 17.4g、Total Carb 8.9g"
				}]
                        }
                    ]
                }
            ]
        }
    ]
}