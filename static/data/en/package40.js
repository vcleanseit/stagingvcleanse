{
    "IsSuccess": true,
    "Packages": [{
        "Id": 40,
        "Title": "Green Pack",
        "SubTitle": "-",
        "Description": "Go Green! Each bottle contains ? kg of veggies and fruits, packed with glorious chlorophyll and nutrients. Great as a morning snack or afternoon pick-me-up. Drink 2 bottles a day and you are well on your way.",
        "Url": "",
        "Tips": "",
        "Introduction": "Go Green! Each bottle contains ? kg of veggies and fruits, packed with glorious chlorophyll and nutrients. Great as a morning snack or afternoon pick-me-up. Drink 2 bottles a day and you are well on your way.",
        "ImageName": "",
        "Tags": [],
        "Groups": [{
            "Id": 61,
            "Name": "-",
            "GroupTypeId": 0,
            "ByWeek": false,
            "Rules": [{
                "Id": 645,
                "Day": 1,
                "Name": "Green Pack",
                "PackagePrice": 200,
                "Gallery": [{
                    "img": "recommend/dailygreen/set/dailygreen.jpg"
                }, {
                    "img": "recommend/dailygreen/ingredients/chiagreen.jpg"
                }, {
                    "img": "recommend/dailygreen/ingredients/chiagreen.jpg"
                }, {
                    "img": "recommend/dailygreen/ingredients/easygreen.jpg"
                }, {
                    "img": "recommend/dailygreen/ingredients/easygreen.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "recommend/dailygreen/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "recommend/dailygreen/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "recommend/dailygreen/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Easy Green",
                    "Image": "recommend/dailygreen/individual/easygreen.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Springtime inspired this Green. We wanted to create an aromatic bouquet of herbs and fragrant vegetables and fruits.",
                    "Ingredient": "Cucumber, mint, pineapple, and a touch of leafy greens.",
                    "Nutrition": "Calories 82k、Protein 2.4g、Total Fat 0g、Total Carb 18g"
                }]
            }]
        }]
    }]
}