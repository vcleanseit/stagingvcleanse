{
    "IsSuccess": true,
    "Packages": [{
        "Id": 42,
        "Title": "Workout Pack",
        "SubTitle": "-",
        "Description": "Better than water or sports drinks. Spicy lemonade and coconut water provide natural hydration and energy during and after workouts.",
        "Url": "",
        "Tips": "",
        "Introduction": "Better than water or sports drinks. Spicy lemonade and coconut water provide natural hydration and energy during and after workouts.",
        "ImageName": "",
        "Tags": [],
        "Groups": [{
            "Id": 63,
            "Name": "-",
            "GroupTypeId": 0,
            "ByWeek": false,
            "Rules": [{
                "Id": 647,
                "Day": 1,
                "Name": "Workout Pack",
                "PackagePrice": 150,
                "Gallery": [{
                    "img": "recommend/workout/set/workout.jpg"
                }, {
                    "img": "recommend/workout/ingredients/fragrantclear.jpg"
                }, {
                    "img": "recommend/workout/ingredients/fragrantclear.jpg"
                }, {
                    "img": "recommend/workout/ingredients/spicylemonade.jpg"
                }, {
                    "img": "recommend/workout/ingredients/spicylemonade.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 48,
                    "Name": "Fragrant Clear",
                    "Image": "recommend/workout/individual/fragrantclear.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "There's nothing quite as refreshingly delicious as pure fresh coconut water. We looked far and wide for the best coconut to pour from, and found what we were searching for in an especially fragrant breed from Thailand.",
                    "Ingredient": "100% coconut water",
                    "Nutrition": "Fresh and pure coconut water, poured right from imported Thai coconut."
                }, {
                    "Count": 1,
                    "Id": 48,
                    "Name": "Fragrant Clear",
                    "Image": "recommend/workout/individual/fragrantclear.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "There's nothing quite as refreshingly delicious as pure fresh coconut water. We looked far and wide for the best coconut to pour from, and found what we were searching for in an especially fragrant breed from Thailand.",
                    "Ingredient": "100% coconut water",
                    "Nutrition": "Fresh and pure coconut water, poured right from imported Thai coconut."
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Spicy Lemonade",
                    "Image": "recommend/workout/individual/spicylemonade.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "In the 1940s, the \"Master Cleanse\" was developed to treat stomach ulcers, digestion issues, and other ailments. Despite intense controversy, it was embraced by cleanse aficionados, yogis, and celebrities. We made a lighter and tangier version from imported fresh lemons, perfect as a refreshing tonic.",
                    "Ingredient": "Lemon, agave, and cayenne pepper.",
                    "Nutrition": "Calories 10k、Protein 0g、Total Fat 0g、Total Carb 2.9g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Spicy Lemonade",
                    "Image": "recommend/workout/individual/spicylemonade.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "In the 1940s, the \"Master Cleanse\" was developed to treat stomach ulcers, digestion issues, and other ailments. Despite intense controversy, it was embraced by cleanse aficionados, yogis, and celebrities. We made a lighter and tangier version from imported fresh lemons, perfect as a refreshing tonic.",
                    "Ingredient": "Lemon, agave, and cayenne pepper.",
                    "Nutrition": "Calories 10k、Protein 0g、Total Fat 0g、Total Carb 2.9g"
                }]
            }]
        }]
    }]
}