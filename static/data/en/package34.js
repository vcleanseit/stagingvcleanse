{
    "IsSuccess": true,
    "Packages": [
        {
            "Id": 34,
            "Title": "Advanced Warming Cleanse ",
            "SubTitle": "-",
            "Description": "Lighter than the Intro Warming Cleanse, this cleanse is suitable for more advanced juicers who are looking for a savory and hot option for the cold season.",
            "Url": "",
            "Tips": "Females receive 6 bottles per day, and males receive 8 bottles per day. Warm Tip: more delicious after heating.",
            "Introduction": "Lighter than the Intro Warming Cleanse, this cleanse is suitable for more advanced juicers who are looking for a savory and hot option for the cold season.",
            "ImageName": "",
            "Tags": [],
            "Groups": [
                {
                    "Id": 52,
                    "Name": "Male",
                    "GroupTypeId": 1,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 636,
                            "Day": 1,
                            "Name": "Advanced Warming Cleanse",
                            "PackagePrice": 400,
                "Gallery": [{
                    "img": "warming/advanced/set/m.jpg"
                }, {
                    "img": "warming/advanced/ingredients/chiagreen.jpg"
                }, {
                    "img": "warming/advanced/ingredients/classicblack.jpg"
                }, {
                    "img": "warming/advanced/ingredients/darkgreen.jpg"
                }, {
                    "img": "warming/advanced/ingredients/darkwhite.jpg"
                }, {
                    "img": "warming/advanced/ingredients/purplebroth.jpg"
                }, {
                    "img": "warming/advanced/ingredients/luohanguo.jpg"
                }, {
                    "img": "warming/advanced/ingredients/pumpkinsoup.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "warming/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "chiagreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 84,
                    "Name": "Luo Han Guo Infusion",
                    "Image": "warming/advanced/individual/luohanguo.png",
                    "Price": 50.00,
                    "Url": "luohanguo",
                    "MaxCount": 50,
                    "Description": "Besides being a great sweetener for diabetics, Luo Han fruit is said to enhance longevity. Licorice root is used for various digestive complaints, including heartburn, colic and ulcers. Longan is used as a remedy for fatigue and insomnia. This herbal blend has a calming and nourishing effect on the nervous system and is a wonderful remedy for cough and cold.",
                    "Ingredient": "Luo Han fruit, longan, licorice root, jujube, pear and herbs.",
                    "Nutrition": "Calories 65k、Protein 0g、Total Fat 0g、Total Carb 15.5g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Sweet potato pumpkin soup",
                    "Image": "warming/advanced/individual/pumpkinsoup.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Slightly spicy winter soup that will keep you warm. Creamy blend of sweet potatoes, pumpkin and warming spices like cumin and cinnamon. Sweet potatoes are rich in calcium, potassium, vitamins A and C and magnesium, a mineral that helps you relax and de-stress. Did we mention cumin and cinnamon are great digestive aids? Enjoy!",
                    "Ingredient": "Pumpkin, sweet potato, celery, cumin, garlic and natural warming spices.",
                    "Nutrition": "Calories 109k、Protein 2.8g、Total Fat 0.4g、Total Carb 25g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Dark Green",
                    "Image": "warming/advanced/individual/darkgreen.png",
                    "Price": 50.00,
                    "Url": "darkgreen",
                    "MaxCount": 50,
                    "Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
                    "Ingredient": "Dark leafy greens from spinach to its more exotic cousins, with apples and a citrus twist.",
                    "Nutrition": "Calories 112k、Protein 8.4g、Total Fat 0g、Total Carb 21.2g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Purple Broth",
                    "Image": "warming/advanced/individual/purplebroth.png",
                    "Price": 50.00,
                    "Url": "purplebroth",
                    "MaxCount": 50,
                    "Description": "Nourishing and detoxifying purple coloured broth made from 10 different types of veggies. Low in calories, high in antioxidants and minerals. Purple cabbage is the superstar in this soup: a natural source of electrolytes and minerals like calcium, potassium and manganese, purple cabbage can help control heart-rate and blood pressure. Plus, the pigment anthocyanins, that give purple its beautiful colour may also be capable of reducing growth of cancer cells. Get ready to feel bold and beautiful!",
                    "Ingredient": "Purple cabbage, carrot, tomato, broccoli, celery, onion, sea salt.",
                    "Nutrition": "Calories 34k、Protein 1.3g、Total Fat 0g、Total Carb 7g"
                }, {
                    "Count": 1,
                    "Id": 48,
                    "Name": "Classic Black",
                    "Image": "warming/advanced/individual/blacksesame.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
                    "Ingredient": "Almonds and black sesame.",
                    "Nutrition": "Calories 208k、Protein 7.1g、Total Fat 17.4g、Total Carb 8.9g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "warming/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "easygreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 14,
                    "Name": "Dark White",
                    "Image": "warming/advanced/individual/darkwhite.png",
                    "Price": 50.00,
                    "Url": "darkwhite",
                    "MaxCount": 50,
                    "Description": "To be honest, we were craving chocolate. An intensely rich, intensely decadent cup of fantasy hot chocolate that would nonetheless be healthy! This one hits the mark pretty well, with nearly 10 grams of plant protein and heart healthy fats per bottle.",
                    "Ingredient": "Raw cacao, agave and cashews.",
                    "Nutrition": "Calories 349k、Protein 12g、Total Fat 27g、Total Carb 22.1g"
                }]
                        }
                    ]
                },
                {
                    "Id": 53,
                    "Name": "Female",
                    "GroupTypeId": 2,
                    "ByWeek": false,
                    "Rules": [
                        {
                            "Id": 637,
                            "Day": 1,
                            "Name": "Advanced Warming Cleanse 2015 1",
                            "PackagePrice": 300,
                "Gallery": [{
                    "img": "warming/advanced/set/f.jpg"
                }, {
                    "img": "warming/advanced/ingredients/chiagreen.jpg"
                }, {
                    "img": "warming/advanced/ingredients/classicblack.jpg"
                }, {
                    "img": "warming/advanced/ingredients/darkgreen.jpg"
                }, {
                    "img": "warming/advanced/ingredients/easyred.jpg"
                }, {
                    "img": "warming/advanced/ingredients/purplebroth.jpg"
                }, {
                    "img": "warming/advanced/ingredients/luohanguo.jpg"
                }, {
                    "img": "warming/advanced/ingredients/pumpkinsoup.jpg"
                }],
                "Products": [{
                    "Count": 1,
                    "Id": 14,
                    "Name": "The Chia Green",
                    "Image": "warming/advanced/individual/chiagreen.png",
                    "Price": 50.00,
                    "Url": "chiagreen",
                    "MaxCount": 50,
                    "Description": "We created this Green to provide protein and heart-healthy fats for those with nut allergies or sensitive stomachs. Chia is easy to digest, and the word itself means \"strength\". This super-seed was used in ancient cultures as an energy booster. Chia seeds are the richest plant source of Omega-3, the vital fats that protect against inflammation.",
                    "Ingredient": "Spinach, cucumber, apple, chia.",
                    "Nutrition": "Calories 129k、Protein 4.6g、Total Fat 2.9g、Total Carb 25.7g"
                }, {
                    "Count": 1,
                    "Id": 84,
                    "Name": "Luo Han Guo Infusion",
                    "Image": "warming/advanced/individual/luohanguo.png",
                    "Price": 50.00,
                    "Url": "luohanguo",
                    "MaxCount": 50,
                    "Description": "Besides being a great sweetener for diabetics, Luo Han fruit is said to enhance longevity. Licorice root is used for various digestive complaints, including heartburn, colic and ulcers. Longan is used as a remedy for fatigue and insomnia. This herbal blend has a calming and nourishing effect on the nervous system and is a wonderful remedy for cough and cold.",
                    "Ingredient": "Luo Han fruit, longan, licorice root, jujube, pear and herbs.",
                    "Nutrition": "Calories 65k、Protein 0g、Total Fat 0g、Total Carb 15.5g"
                }, {
                    "Count": 1,
                    "Id": 47,
                    "Name": "Sweet potato pumpkin soup",
                    "Image": "warming/advanced/individual/pumpkinsoup.png",
                    "Price": 50.00,
                    "Url": "redbeetsoup",
                    "MaxCount": 50,
                    "Description": "Slightly spicy winter soup that will keep you warm. Creamy blend of sweet potatoes, pumpkin and warming spices like cumin and cinnamon. Sweet potatoes are rich in calcium, potassium, vitamins A and C and magnesium, a mineral that helps you relax and de-stress. Did we mention cumin and cinnamon are great digestive aids? Enjoy!",
                    "Ingredient": "Pumpkin, sweet potato, celery, cumin, garlic and natural warming spices.",
                    "Nutrition": "Calories 109k、Protein 2.8g、Total Fat 0.4g、Total Carb 25g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Dark Green",
                    "Image": "warming/advanced/individual/darkgreen.png",
                    "Price": 50.00,
                    "Url": "darkgreen",
                    "MaxCount": 50,
                    "Description": "Not for the faint of heart. We were absolutely determined to pack as many leafy greens as we could into this deep and dense concoction.",
                    "Ingredient": "Dark leafy greens from spinach to its more exotic cousins, with apples and a citrus twist.",
                    "Nutrition": "Calories 112k、Protein 8.4g、Total Fat 0g、Total Carb 21.2g"
                }, {
                    "Count": 1,
                    "Id": 19,
                    "Name": "Purple Broth",
                    "Image": "warming/advanced/individual/purplebroth.png",
                    "Price": 50.00,
                    "Url": "purplebroth",
                    "MaxCount": 50,
                    "Description": "Nourishing and detoxifying purple coloured broth made from 10 different types of veggies. Low in calories, high in antioxidants and minerals. Purple cabbage is the superstar in this soup: a natural source of electrolytes and minerals like calcium, potassium and manganese, purple cabbage can help control heart-rate and blood pressure. Plus, the pigment anthocyanins, that give purple its beautiful colour may also be capable of reducing growth of cancer cells. Get ready to feel bold and beautiful!",
                    "Ingredient": "Purple cabbage, carrot, tomato, broccoli, celery, onion, sea salt.",
                    "Nutrition": "Calories 34k、Protein 1.3g、Total Fat 0g、Total Carb 7g"
                }, {
                    "Count": 1,
                    "Id": 48,
                    "Name": "Classic Black",
                    "Image": "warming/advanced/individual/blacksesame.png",
                    "Price": 50.00,
                    "Url": "blacksesame",
                    "MaxCount": 50,
                    "Description": "Seeds and nuts are the plant world's best energy bundles. We harvest both into this deliciously wholesome black sesame fusion.",
                    "Ingredient": "Almonds and black sesame.",
                    "Nutrition": "Calories 208k、Protein 7.1g、Total Fat 17.4g、Total Carb 8.9g"
                }]
                        }
                    ]
                }
            ]
        }
    ]
}