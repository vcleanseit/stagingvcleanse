(function ($) {
	var $nav = $("#product-type-nav"),
		$navList = $nav.find("li"),
		$productContent = $("#product-list"),
		$productList = $productContent.children(),
		currentSelectedTypeIndex = 0,

		$orderTotalPrice = $("#orderTotalPrice"),

		$itemNutrition = $("#item-nutrition");
	$nav.on("mouseenter", "li", function(e) {
		changeProductType($(this));
	})

	function changeProductType($this) {
		var	index = $navList.index($this),
			showList = $productList.eq(index);

		if (index === currentSelectedTypeIndex) {
			return false;
		}

		$navList.removeClass('active');
		$this.addClass('active');

		$productList.removeClass('active')
		showList.css('opacity', 0).addClass('active')
			.animate({opacity: 1}, 500);
		currentSelectedTypeIndex = index;
	}

	$productContent.on("click", ".number-minus, .number-plus", function(){
		//点击页面中所有的加减都重新计算一遍价格
		var totalPrice = 0;
		$productContent.find(".number").each(function() {
			var $this = $(this),
				value = $this.val();
			value = isNaN(value) ? 0 : parseInt(value);
			totalPrice += value;
		});
		$orderTotalPrice.text(totalPrice);
	});
	// 营养列表
	var timeout = null;
	$(".product-type-list").on("mouseenter", "dl", function() {
		var $this = $(this);
		//判断悬浮层是否超过屏幕
		timeout = setTimeout(showNutrition, 500);


		function showNutrition () {
			var offset = $this.offset(),
				$clone = $itemNutrition.clone(),
				docWidth = document.documentElement.clientWidth,
				totalWidth = offset.left + $itemNutrition.width() + parseInt($itemNutrition.css("marginLeft")),
				cssConfig = {
					top: offset.top,
					left: offset.left + 10,
					opacity: 0
				}

			$this.append($clone);
			if (totalWidth > docWidth) {
				$clone.addClass('right');
				cssConfig.left -= 20;
			}
			$clone.css(cssConfig)
					.show()
					.animate({opacity: 1, left: offset.left}, 300);
		}
		

	}).on("mouseleave", "dl", function() {
		clearTimeout(timeout);

		var $this = $(this),
			$clone = $this.children(".item-nutrition"),
			animateConfig = {
				opacity: 0, 
				left: "+=10"
			};
		if ($clone[0]) {
			if ($clone.hasClass('right')) {
				animateConfig.left = "-=10";
			}
			$clone.animate(animateConfig, 300, function() {
				$this.off("mouseleave");
				$clone.remove();
			})
		}
	})

})($);