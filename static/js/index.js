(function($) {
	var $window = $(window);
	var $document = $(document);

	setBottlePosition();
	initBanner();
	switchMenu();


	function switchMenu(){
		$(".header-nav-bottom li a").removeClass('active');
		$($(".header-nav-bottom li a").get(0)).addClass('active');
	}
	
	function initBanner () {
		$('.banner').unslider({
	    	speed: 500,
			delay: 5000,
			keys: true,
			dots: true,
			fluid: true  
	    });
	}
	
	function setBottlePosition() {
		var bottleRealHeight = 330;			
		var bottleStartTopPos = 150;

		var footerHeight = 320;
		var topHeight = 74;

		
		var $bottle = $("#bottle");
		var positionCase2Active = false;
		var obj = {
			getTopPos : function () {
				return $window.scrollTop();
			},
			setBottomPos : function () {
				var self = this;
				var wHeight = $window.height();
				
				if (wHeight < bottleRealHeight) 
				{
					self.bottlePosCase1();
				}
				else if (wHeight < bottleRealHeight + bottleStartTopPos)
				{
					self.bottlePosCase2();
				}
				else
				{
					positionCase2Active = false;
					self.bottlePosCase3();
				}					
			},
			bottlePosCase1 : function () {
				var self = this;						
				$bottle.css({ position: 'absolute', top: bottleStartTopPos });
			},
			bottlePosCase2 : function () {
				var self = this;					
				var tp = self.getTopPos();					
				var hh = topHeight;
				var distance = Math.round(($window.height() - bottleRealHeight) / 2);			
				
				positionCase2Active = distance;
				
				if (tp < bottleStartTopPos - distance) {						
					$bottle.css({ position: 'absolute', top: bottleStartTopPos });
				}
				else
				{
					$bottle.css({ position: 'fixed', top: distance });
				}
				self.bottlePosCase3();					
			},			
			bottlePosCase3 : function () {
				var self = this;						
				var tp = self.getTopPos();
				var bh = bottleRealHeight;					
				var fh = footerHeight + 30; //slant position	
				var dh = $document.height();
				var distance = bottleStartTopPos;
			
				if (positionCase2Active !== false) {
					distance = positionCase2Active;
				}
				
				
				if (tp > dh-(bh+fh) - distance) {						
					$bottle.css({ position: 'absolute', top: dh-(bh+fh)});
				}
				else
				{
					if (positionCase2Active === false) {							
						$bottle.css({ position: 'fixed', top: bottleStartTopPos });
					}
				}
			}
		}
		$window.on('scroll resize', function () {
			obj.setBottomPos();
		});
		obj.setBottomPos()
	} 
})($);