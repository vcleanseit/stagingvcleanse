(function($, window, vc) {
	var $deatilContent = $("#detail-content"),
		packageid = vc.formatHash().packageid || "",
		config = vc.config,
		http = vc.http,
		url = vc.url,
		selectedGender = 0, // config.defaultGender,
		selectedDayNumber = config.defaultDayNumber,
		placeOrderUrl = '';

	switchMenu();


	function switchMenu(){
		$(".header-nav-bottom li a").removeClass('active');
		$($(".header-nav-bottom li a").get(1)).addClass('active');
	}
	loadData();

	// 加载数据
	function loadData() {

		//模拟
		http.get("/static/data/" + vc.getCookie('Language') + "/package" + packageid + ".js?v=2", null, function(d) {
			setTimeout(function() {
				initPage(d)
			}, 100)
		});
	}

	function initPage(d) {
		var packages = d.Packages || [],
			packageData = packages[0] || {},
			data = formatJsonData(packageData);
		// 赋值到全局
		detailData = data;
		// 解析模板
		$deatilContent
			.css({
				top: 30,
				opacity: 0
			})
			.html(template('detailTemplate', data))
			.animate({
				top: 0,
				opacity: 1
			}, 400, "easeOutQuad")

		// 初始化页面 绑定事件
		init();

	}

	function init() {

		var $daynum = $("#daynum");

		bindEvent();
		initThumbImgList();
		initBuyTime();

		// 默认性别
		$("#gender").find("input[value=" + selectedGender + "]").trigger("click");
		// 默认天数
		if ($daynum[0]) {
			if (!selectedDayNumber) {
				selectedDayNumber = config.dayNumber[config.dayNumber.length - 1];
			}
			$daynum.find("input[value=" + selectedDayNumber + "]").trigger('click');
		}

	}

	function bindEvent() {
		// 切换性别
		$deatilContent.on('click', '.gender-radio', function() {
				var $this = $(this),
					val = $this.val() | 0;
				if (selectedGender !== val) {
					// 重置选中的性别
					selectedGender = val;
					// 重新设定rules
					setShowRules(detailData, val | 0);
					var productImgHtml = template('productImgTemplate', detailData);
					var productListHtml = template('productListTemplate', detailData);
					$("#detail-list").html(productListHtml);
					$("#gallery").html(productImgHtml);

					initThumbImgList();
					calcTotalPrice();
				}
			})
			// 切换天数
			.on("click", '.daynum-radio', function() {
				calcTotalPrice();
			})
			// 附加物选择
			.on("click", '.additions-checkbox', function() {
				calcTotalPrice();
			})
			// 小图切换
			.on('click', "#thumbImgList a", function() {
				var $this = $(this);
				$img = $this.children(),
					dataSrc = $img.attr("data-src");
				if ($this.hasClass('selected')) {
					return false;
				}
				$this.parent().siblings().find('a').removeClass('selected');
				$this.addClass('selected');

				if (dataSrc) {
					$("#boothImg").attr('src', dataSrc) //.animate({opacity: 1}, 300)
				}

				return false;
			})
			//下单
			.on('click', "#placeOrderBtn", function() {
				if (placeOrderUrl) {
					window.location.href = placeOrderUrl;
				}
			})
			// singDetail event
	}

	function calcTotalPrice() {
		var data = {
				packageid: packageid,
				groupid: 31,
				days: 5,
				addon: []
			},
			totalPrice = 0,
			additionsPrice = 0,
			//天数
			//days = $(".daynum-radio:checked").val() || config.defaultDayNumber;
			$startDate = $("#startDate").val(),
			$endDate = $("#endDate").val();
		//周数
		weeks = getWeekBetweenDates($startDate, $endDate);
		dayPerWeeks = getDayPerWeeks();
		// 所有选择附加物
		$(".additions-checkbox:checked").each(function() {
				additionsPrice += $(this).data('price') | 0;
				data.addon.push(this.value);
			})
			//(附加物 + 单价) * 天
			//totalPrice += (additionsPrice + detailData.showRules.PackagePrice) * days;
			//(附加物 + 单价) * 天数 * 周数
		totalPrice += (additionsPrice + detailData.showRules.PackagePrice) * dayPerWeeks * weeks;

		data.deliverystartdate = $startDate;
		data.weekdays = getWeekDays().join(",");
		data.groupid = detailData.showRules.groupid;
		data.weeks = weeks;
		data.addon = data.addon.join(",");

		// #packageid/{{packageid}}/groupid/{{groupid}}/weekDays/{{weekdays}}/deliveryStartDate/{{deliverystartdate}}/duration/{{weeks}}/addon/{{addon}}/event/subs
		placeOrderUrl = template.compile(config.placeSubscriptionUrl)(data);
		//修改价格
		$("#totalPrice").text(totalPrice);
	}

	function getDayPerWeeks() {
		var weeks = $("#week dd .toggle-checkbox input:checked").length;
		return weeks;
	}

	function getWeekDays() {
		var weekDays = [];
		$("#week dd .toggle-checkbox input:checked").each(function(i, e) {
			if (e.value) {
				weekDays.push(e.value);
			}
		});
		return weekDays;
	}

	function getWeekBetweenDates(startDate, endDate) {
		var start = new Date(startDate.replace("-", "/"));
		var end = new Date(endDate.replace("-", "/"));
		var diff = parseInt((end - start) / (1000 * 60 * 60 * 24));
		var weeks = Math.floor(diff / 7) + 1
		$("#weeks").val(weeks);
		return weeks;
	}

	function setShowRules(data, index) {
		data.showRules = data.Rules[index];

		return data;
	}
	// 格式化数据
	function formatJsonData(data) {
		//console.log(data);
		var formatData = {};

		formatData.Title = data.Title;
		formatData.Introduction = data.Introduction;
		formatData.Tips = data.Tips;
		formatData.Groups = [];
		formatData.Rules = [];


		for (var i = 0, len = data.Groups.length; i < len; i++) {
			var curData = data.Groups[i]
			formatData.Groups.push({
				GroupTypeId: curData.GroupTypeId,
				Id: curData.Id,
				Name: curData.Name
			});
			curData.Rules[0].groupid = curData.Id;

			// 添加rules下面第一数组元素
			formatData.Rules.push(curData.Rules[0]);

			//formatData[ detailData.Groups[i].GroupTypeId ] = detailData.Groups[i].Rules[0];
		}

		setShowRules(formatData, selectedGender);
		// 套餐天数
		formatData.daynum = config.dayNumber;
		// 附加选项
		formatData.additions = config.additions;
		return formatData;
	}

	// 绑定小图
	function initThumbImgList() {
		var $thumbImgList = $("#thumbImgList"),
			imgLength = $thumbImgList.children().length,
			diff = 54;
		// 小图多于6个
		if (imgLength > 6) {
			$thumbImgList.parent().addClass("scroll");
			scorllThumbImgList()
		} else {
			$thumbImgList.parent().removeClass("scroll");
		}
		// 小图滚动
		function scorllThumbImgList() {
			var $left = $(".left-arrow"),
				$right = $(".right-arrow"),

				wrapWidth = $(".thumb-wrap").width(),
				listWidth = imgLength * diff,
				isEnd = true;

			$thumbImgList.parent().addClass("scroll");
			$thumbImgList.css({
				width: listWidth
			});

			$left.show();
			$right.show();
			$right.on('click', function() {
				if (!isEnd) {
					return false;
				}

				var marginLeft = getMarginLeft();
				if (Math.abs(marginLeft) + wrapWidth < listWidth) {
					isEnd = false;
					marginLeft -= diff;
					$thumbImgList.animate({
						"marginLeft": marginLeft
					}, function() {
						isEnd = true;
					})
				}
			})
			$left.on('click', function() {
				if (!isEnd) {
					return false;
				}

				var marginLeft = getMarginLeft();
				if (marginLeft == 0) {
					return false;
				}
				marginLeft += diff;
				if (marginLeft > 0) {
					marginLeft = 0;
				}
				if (marginLeft <= listWidth) {
					isEnd = false;
					$thumbImgList.animate({
						"marginLeft": marginLeft
					}, function() {
						isEnd = true;
					})
				}
				return false;
			})

			function getMarginLeft() {
				return parseInt($thumbImgList.css("marginLeft"));
			}
		}
	}

	// 初始化时间
	function initBuyTime() {
		var $week = $("#week");
		if (!$week[0]) {
			return false;
		}
		var dayCheckboxList = $week.find(":checkbox"),
			$startDate = $("#startDate"),
			$endDate = $("#endDate");
		wDateConfig = (function() {
			var config = {
				startDate: startDate,
				dateFmt: 'yyyy-MM-dd',
				doubleCalendar: true,
				opposite: true,
				disabledDays: [0, 1, 2, 3, 4, 5, 6]
			}
			return {
				getConfig: function() {
					return config
				},
				setConfig: function(obj) {
					config = $.extend(config, obj);
				}
			}
		})();

		bindEvent();

		//默认选中第一个 星期一
		$(dayCheckboxList[0]).trigger('click');

		function bindEvent() {
			// 监控天选择
			$week.on("click", ":checkbox", function() {
					var minDay = getMinSelectedDayVal(),
						dateObj = null;

					if (isNaN(minDay)) {
						minDay = 1;
					}

					dateObj = getStartAndEndDay(minDay);
					//设置input value
					setDayInputVal(dateObj);
					//计算用户选择的最小天
					minDay = minDay == 7 ? 0 : minDay;
					wDateConfig.setConfig({
						minDate: dateObj.startDate,
						startDate: dateObj.startDate,
						disabledDays: getDisabledDays(minDay)
					});
					calcTotalPrice();
				})
				// 注意：周日checkboxvalue为7，日期控件为0，需替换
			$startDate.on("focus", function() {
				var config = wDateConfig.getConfig();
				config.onpicked = function() {
					$endDate.focus();
					calcTotalPrice();
				}
				WdatePicker(config);
				return false;
			})
			$endDate.on('focus', function() {
				var config = wDateConfig.getConfig();
				delete config.startDate;
				//结束时间最小不能小于开始时间
				config.minDate = $startDate.val();
				config.onpicked = function() {
					calcTotalPrice();
				}
				WdatePicker(config);
			})
		}
		// 选择订购时间段
		function setDayInputVal(date) {
			$startDate.val(date.startDate);
			$endDate.val(date.endDate);
		}

		function getStartAndEndDay(diff) {
			var startDate = new XDate(),
				endDate = new XDate();
			startDate = calcDay(startDate, diff + 7);
			endDate.addMonths(1);
			endDate = calcDay(endDate, diff + 7);
			return {
				startDate: startDate.toString("yyyy-MM-dd"),
				endDate: endDate.toString("yyyy-MM-dd")
			}
		}

		function calcDay(today, diff) {
			var weekday = today.getDay();
			var calcDay = new XDate(1000 * 60 * 60 * 24 * (diff - weekday) + today.getTime());
			return calcDay;
		}

		function getDisabledDays(minDay) {
			var days = [0, 1, 2, 3, 4, 5, 6],
				disabledDays = [];
			//需要替换周日的数字
			$.each(days, function(index, value) {
				if (value != minDay) {
					disabledDays.push(value)
				}
			})
			return disabledDays;
		}

		function getMinSelectedDayVal() {
			var arr = [];
			dayCheckboxList.filter(":checked").each(function(index, checkbox) {
				arr.push(checkbox.value);
			})
			return parseInt(arr.min());
		}
	}

	/*--获取网页传递的参数--*/
	function request(paras) {
		var url = location.href;
		var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
		var paraObj = {}
		for (i = 0; j = paraString[i]; i++) {
			paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
		}
		var returnValue = paraObj[paras.toLowerCase()];
		if (typeof(returnValue) == "undefined") {
			return "";
		} else {
			return returnValue;
		}
	}
})($, window, vc);