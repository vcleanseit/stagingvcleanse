(function($, document) {
	$(function() {
		//全局事件
		$(document).on("click", ".toggle-radio :radio", function() {
			var $this = $(this),
				name = $this[0].name;
			//$(document).find(":radio[name=" + name + "]").parent().removeClass("selected");
			$this.parent().siblings(".toggle-radio").removeClass("selected");
			$this.parent().addClass("selected");
		}).on("click", ".toggle-checkbox :checkbox", function() {
			var $this = $(this);
			$this.parent().toggleClass("selected");
		}).on("click", ".checkbox :checkbox", function() {
			var $this = $(this);
			$this.closest("label").toggleClass("checked");
		}).on("click", ".number-control .number-plus, .number-control .number-minus", function(event) {
			var $this = $(this),
				$input = $this.siblings('input'),
				inputVal = $input.val() | 0,
				maxVal = ($this.parent().attr('data-max') | 0) || 50;

			if ($this.hasClass("number-plus")) {
				if (inputVal <= maxVal) {
					inputVal += 1;
					$input.val(inputVal);
				}
				
			} else if ($this.hasClass("number-minus")) {
				if (inputVal > 0) {
					inputVal -= 1;
					$input.val(inputVal);
				}
			}
			return false;
		})

		initForm();

		//wechat-qrcode
		$("#wechat-qrcode").on("mouseenter", function() {
			$(this).children().show();
		}).on("mouseleave", function() {
			$(this).children().hide();
		})

		function initForm() {
			// 初始化toggle-checkbox
			var $toggleRadio = $(".toggle-radio"),
				$toggleCheckbox = $(".toggle-checkbox"),
				$checkbox = $(".checkbox");

			$toggleRadio.find(":radio:checked").parent().addClass('selected');
			$toggleCheckbox.find(":checkbox:checked").parent().addClass('selected');
			$checkbox.find(":checkbox:checked").closest("label").addClass('checked');
		}
		Array.prototype.min = function() {
				var r = this.sort(function(a, b) {
					return a - b;
				})
				return r[0];
			}
			//huandong
		$.extend($.easing, {
			easeOutQuad: function(x, t, b, c, d) {
				return -c * (t /= d) * (t - 2) + b;
			}
		});
	});
})($, document);

var vc = (function($) {
	var
		// 解析hash
		formatHash = function() {
			//#pageid/19/storadid/21
			var hash = window.location.hash,
				result = {};
			if (hash.charAt(0) === "#") {
				hash = hash.substr(1);
				var arr = hash.split("/");
				for (var i = 0; i < arr.length; i += 2) {
					result[arr[i]] = arr[i + 1]
				}
			}
			return result;
		},
		getCookie = function getCookie(c_name) {
			if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(c_name + "=")
				if (c_start != -1) {
					c_start = c_start + c_name.length + 1
					c_end = document.cookie.indexOf(";", c_start)
					if (c_end == -1) c_end = document.cookie.length
					return unescape(document.cookie.substring(c_start, c_end))
				}
			}
			return ""
		},
		ajaxUrl = {
			loadPackageByPackageId: "/dataprovider/LoadPackageByPackageId",
		},
		//全局ajax过滤器
		filter = {
			request: function(params) {
				return params;
			},
			responseSuccess: function(response) {
				return response;
			},
			responseError: function(response) {
				return response;
			}
		},
		//ajax方法
		http = {
			get: function(url, params, successcallback, errorcallback) {
				var params = filter.request(params);
				$.ajax({
					url: url,
					data: params,
					method: "get",
					dataType: "json",
					error: function(data) {
						data = filter.responseError(data);
						if ($.isFunction(errorcallback)) {
							errorcallback(data);
						}
					},
					success: function(data) {
						data = filter.responseSuccess(data);
						if ($.isFunction(successcallback)) {
							successcallback(data);
						}
					},
					timeout: 5000
				})
			}
		},
		// 配置
		config = {
			categoryId: {
				// cleanse page 
				// 初次订购
				'firstBuy-1': "/cleanse/detail#packageid/33",
				'firstBuy-2': "/cleanse/detail#packageid/35",
				// 非初次订购
				'againBuy-1': "/cleanse/detail#packageid/34",
				'againBuy-2': "/cleanse/detail#packageid/36",
			},
			//下单url
			//placeOrderUrl: '/order/receiver#packageid/{{packageid}}/groupid/{{groupid}}/days/{{days}}/addon/{{addon}}',
			placeOrderUrl: '/buy/orderinfo#packageid/{{packageid}}/groupid/{{groupid}}/days/{{days}}/addon/{{addon}}',
			placeSubscriptionUrl: '/order/receiver#packageid/{{packageid}}/groupid/{{groupid}}/weekDays/{{weekdays}}/deliveryStartDate/{{deliverystartdate}}/duration/{{weeks}}/addon/{{addon}}/event/subs',
			// 套餐天数
			dayNumber: [1, 3, 5],
			// 附加物
			additions: [{
				id: 22,
				text: getCookie('Language') === "en" ? "Fragrant Clear" : "每次外送加一瓶芳香椰子水",
				price: 50
			}, {
				id: 20,
				text: getCookie('Language') === "en" ? "Spicy Lemonade" : "每次外送加一瓶大师清体柠檬水",
				price: 25
			}
			/*
			, {
				id: 89,
				text: getCookie('Language') === "en" ? "Purple Broth" : "每次外送加一瓶紫甘蓝靓汤",
				price: 50
			}
			*/
			],
			// 默认选中5天
			defaultDayNumber: 1,
			// 默认女
			defaultGender: 1 // 0:男，1:女
		};
	return {
		url: ajaxUrl,
		http: http,
		config: config,
		formatHash: formatHash,
		getCookie: getCookie
	}
})($, window);

