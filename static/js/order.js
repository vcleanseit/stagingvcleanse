
var isLogin = false; //默认本次操作没有登录过


$(document).ready(function(){

	// 如果没有登录过，则为无登录购买
	if(isLogin == false){
		// 无登录购买就要打开地址窗口
		use_NewConsignee(false,'');
	}
	$(".btn-login").click(function(e){
		hide_address();
		open_login();

	})
	$(".popupbox-close").click(function(e){
		close_login();
	})
	// 关掉地址
	$(".access-nav-close").click(function(){
		hide_address();
	});
	$(".popup-signin-form #su").click(function(e){
		isLogin = true;
		$("#consignee1").removeClass("hide");
		$("#btnNewAddr").addClass("hide");
		hide_address();
		hide_ConsigneeAll();
		close_login();
	})
	$(".popup-signin-new #su").click(function(e){
		isLogin = false;
		close_login();	
	})
	
	$("#frmLogin").submit(function(){

		return false;
	})
	//地址点击
	$('.consignee-item').click(function(){
		clear_allAddrSelect();
		$(this).parent().addClass("ui-switchable-panel-selected");
		$(this).addClass("item-selected");
		
		var firstname = $(this).parent().find(".addr-detail .addr-name").html();
		$("#editaddress #signup-firstname").val(firstname);
		
		var addr = $(this).parent().find(".addr-detail .addr-info").html();
		$("#editaddress #signup-line1").val(addr);
		
		var tel = $(this).parent().find(".addr-detail .addr-tel").html();
		var fulladdr = "收货地址：" + firstname + "&nbsp;" + addr + "&nbsp;" + tel + "&nbsp;";
		$(".receive-addr").html(fulladdr);
	})
	
//	// 地址的hover效果
//	$('#consignee-list li').mouseenter(function(){
//		$("#consignee-list li").removeClass("li-hover");
//		$(this).addClass("li-hover");
//	}).mouseleave(function(){$("#consignee-list li").removeClass("li-hover");});
//	
//	
//	// 编辑地址
//	$('.edit-consignee').click(function(){
//		
//		var title = $(this).parent().parent().find(".consignee-item span").html();
//		use_NewConsignee(true,title);
//		//$(".orderedit-delivery-title strong").html(title);
//		
//		var firstname = $(this).parent().parent().find(".addr-detail .addr-name").html();
//		$("#editaddress #signup-firstname").val(firstname);
//		
//		var addr = $(this).parent().parent().find(".addr-detail .addr-info").html();
//		$("#editaddress #signup-line1").val(addr);
//		
//		var tel = $(this).parent().parent().find(".addr-detail .addr-tel").html();
//		$("#editaddress #signup-phonenumber").val(tel);
//		//signup-firstname
//	});
//	
//	$("#editaddress").submit(function(){
//		$("#consignee1").removeClass("hide");
//		$("#btnNewAddr").addClass("hide");
//		hide_address();
//		hide_ConsigneeAll();
//		return false;
//	})
	// 支付选择的hover效果
	$('.online-payment')
	.hover(function(){
		$(this).addClass('payment-item-hover');
	},function(){
		$(this).removeClass('payment-item-hover');
	}); 
	
	// 支付选择的切换效果
//	$('#payment-list .online-payment').click(function(){
//		$('#payment-list .online-payment').removeClass('item-selected');
//		$(this).addClass('item-selected');
//	})
	
	//发票的效果
	$('#invoice-list .online-payment').click(function(){
		$('#invoice-list .online-payment').removeClass('item-selected');
		$('#invoice-title').hide();
		$('#btn-invoice-confirm').hide();
		$(this).addClass('item-selected');
	})
	// 发票抬头的效果
	$("#invoice-company").click(function(){
		$('#invoice-title').show();
		$('#btn-invoice-confirm').show();
		
	})
	
	// 发票提交
	$("#btn-invoice-confirm").click(function(){
		if($(this).val() == "确定"){
			if($('#invoice-title').val() != ''){
				$('#invoice-title').addClass('rd-only');
				$('#invoice-title').attr("readonly",true);
				$(this).val("修改");
			}else{
				
			}
		}else{
			$('#invoice-title').removeClass('rd-only');
			$('#invoice-title').attr("readonly",false);
			$(this).val("确定");
			$('#invoice-title').focus();
		}
	})
	
	// 日期选择器
	$startDate = $("#startDate"),
	wDateConfig = (function() {
		var config = {
			dateFmt:'yyyy-MM-dd', 
			doubleCalendar:true,
			opposite:true
		}
		return {
			getConfig: function() {
				return config
			},
			setConfig: function(obj) {
				config = $.extend(config, obj);
			}
		}
	})();
	$startDate.on("focus", function() {
		var config = wDateConfig.getConfig();
		config.onpicked = function(){
			reShowShipment();
		}
		WdatePicker(config);
		return false;
	})
	
	// 配送方式
	$("#deliverytime").on("change",function(){
		reShowShipment();
	})
	 
	
	//var objShip = []{}
	$("#selfpicupstore").on("change",function(){
		reShowShipment();
	})
	
	// 银行选择
	$(".banklist li").click(function(){
		$(".banklist li").removeClass('active');
		$(this).addClass('active');
		$("#bank-name").html($(this).attr('data-name'));
	})

	// 到顶
	$("#backpanel-inner").click(function(){
		goto_top();
	})
})

// 配送方式的切换
function reShowShipment(){
	var startTime = $("#startDate").val();
	var deliverytime = $("#deliverytime").val();
	
	$(".promisetip").html("预计：&nbsp;" + startTime + "&nbsp;" + deliverytime + "&nbsp;送达");
	
}

/**
 * 使用新收货人地址
 	isLog = true是说明已经登录
 */
function use_NewConsignee(isLog,use_title) {
	var txtTitle = "新增收货地址";
	if(isLog != true){
		txtTitle = "收货人信息";
	}

	if(use_title != ""){
		txtTitle = use_title
	}
	if(isLogin){
		$(".order-address-box .tips").hide();
	}else{
		$(".order-address-box .tips").show();
	}
	$(".orderedit-delivery-title strong").html(txtTitle);
	$('#editaddress input').val("");
	$(".order-address-box").show().css("opacity", 0)
		.animate({"opacity": 1}, 300);
	
	$(".backdrop").css("z-index",100);
	$(".backdrop").show().css("opacity", 0)
	.animate({"opacity": 0.8}, 300);
}

function open_login(){
	$(".popupbox").show();
	
	$(".backdrop").css("z-index",100);
	$(".backdrop").show().css("opacity", 0)
		.animate({"opacity": 0.8}, 300);	
}
function close_login(){
	$(".popupbox").hide();
	$(".backdrop").css("z-index",-100);
	$(".backdrop").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
		
	});
}
function hide_address() {
	$(".order-address-box").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
		$(this).hide() 
	});
	$(".backdrop").css("z-index",-100);
	$(".backdrop").css("opacity", 1).animate({"opacity": 0}, 300, function() { 
		
	});
}

function show_ConsigneeAll(){
	$("#consignee-list li").show();
	$(".addr-switch.switch-on").hide();
	$(".addr-switch.switch-off").show();
} 

function hide_ConsigneeAll(){
	$("#consignee-list li").hide();
	$("#consignee-list li.ui-switchable-panel-selected").show();
	$(".addr-switch.switch-on").show();
	$(".addr-switch.switch-off").hide();
}

function clear_allAddrSelect(){
	$("#consignee-list li").removeClass("ui-switchable-panel-selected");
	$("#consignee-list .consignee-item").removeClass("item-selected");
}


//
//function doSwithTab(mode){
//	$("#express_shipment_item").removeClass('curr');
//	$("#pick_shipment_item").removeClass('curr');
//	
//	$("#express_shipment").hide();
//	$("#selfpick_shipment").hide();
//
//	switch(mode){
//	case "pay":
//		$("#express_shipment").show();
//		$("#express_shipment_item").addClass('curr');
//		break;
//	case "picksite":
//		$("#selfpick_shipment").show();
//		$("#pick_shipment_item").addClass('curr');
//		break;
//	}
//}


// 到顶端
var goto_top_type = -1;  
var goto_top_itv = 0;     
function goto_top_timer()  {
	var y = goto_top_type == 1 ? document.documentElement.scrollTop : document.body.scrollTop;  
	var moveby = 15;     
	y -= Math.ceil(y * moveby / 100);
	if (y < 0) {  y = 0;  }
	if (goto_top_type == 1) {
		document.documentElement.scrollTop = y;
	}  else {
		document.body.scrollTop = y;
	}
	if (y == 0) {
		clearInterval(goto_top_itv);
		goto_top_itv = 0;
	}
}

function goto_top(){
	if (goto_top_itv == 0) {
		if (document.documentElement && document.documentElement.scrollTop) {
				goto_top_type = 1;
		}else if(document.body && document.body.scrollTop) {
			goto_top_type = 2;  
		}else{
			goto_top_type = 0;
		}
		if (goto_top_type > 0) {
			goto_top_itv = setInterval('goto_top_timer()', 10);
		}
	}
}