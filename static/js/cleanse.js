(function ($) {
	var $nav = $("#cleanse-type-nav"),
		$navList = $nav.find('li'),
		$navContent = $("#cleanse-type-content"),
		$navContentList = $navContent.children(),
		$firstAccessNav = $("#first-access-nav"),
		$firstAccessNavBtns = $firstAccessNav.find(".btn-small"),

		$firstAccessBox = $(".first-access-box"),
		$backdrop = $(".backdrop"),
		currentSelectedTypeIndex = 0,
		config = vc.config || {};

	init();
	function init() {
		bindEvent();
		bindShowDetailUrl();
		isFirstAccessPage();
		switchMenu();
	}

	function switchMenu(){
		$(".header-nav-bottom li a").removeClass('active');
		$($(".header-nav-bottom li a").get(1)).addClass('active');
	}
	// 绑定 查看套餐详情 url
	function bindShowDetailUrl() {
		if (typeof config.categoryId == "object") {
			for (var key in config.categoryId) {
				$("#" + key).attr("href", config.categoryId[key])
			}
		}
	}

	function bindEvent() {
		//切换列表
		$nav.on("mouseenter", "li", function(e) {
			changeCleanseType($(this));
		})
		//首次访问
		$firstAccessNav.on('click', ".btn-small", function(e) {
			e.preventDefault();
			var $this = $(this);

			var index = $firstAccessNavBtns.index($this);

			var $navType = $($navList[index]);
			if ($navType[0]) {
				changeCleanseType($navType);
			}
			hide();
			return false;
		}).on('click', '.access-nav-close', function() {
			hide();
			return false;
		})
		function hide() {
			$firstAccessBox.css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				$(this).remove() 
			});
			$backdrop.css("opacity", 1).animate({"opacity": 0}, 300, function() { 
				$(this).remove() 
			});
		}
	}
	function changeCleanseType($this) {
		var	index = $navList.index($this),
			showUl = $navContent.children().eq(index);
		if (index === currentSelectedTypeIndex) {
			return false;
		}
		if (!showUl[0]) {
			return false;
		}
		$navList.removeClass('active');
		$this.addClass('active');

		$navContentList.removeClass('active')
		showUl.css('opacity', 0).addClass('active')
			.animate({opacity: 1}, 500);
		currentSelectedTypeIndex = index;
	}

	function isFirstAccessPage() {
		if (window.localStorage) {
			var isAccess = localStorage.getItem("access");
			if (!isAccess) {
				$firstAccessBox.show().css("opacity", 0)
					.animate({"opacity": 1}, 300);

				$backdrop.show().css("opacity", 0)
					.animate({"opacity": 0.8}, 300);
				localStorage.setItem("access", "1");
			}
		}
	}
})($);
