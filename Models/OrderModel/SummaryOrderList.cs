﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Models.OrderModel
{
    public class DeliveryList
    {
        public string DeliverDate { get; set; }

        public List<ProductList> ProductList { get; set; }

        public bool IsSelfPickup { get; set; }

        public string AreaID { get; set; }

        public int PackageRuleId { get; set; }

        public int PackageRuleCount { get; set; }
    }
}