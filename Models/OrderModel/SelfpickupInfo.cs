﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Models.OrderModel
{
    public class SelfpickupInfo
    {
        public int StoreId { get; set; }


        private string _firstName;

        public string FirstName
        {
            get { return _firstName==null?string.Empty:_firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName == null ? string.Empty : _lastName; }
            set { _lastName = value; }
        }

        private string _mobile;

        public string Mobile
        {
            get { return _mobile == null ? string.Empty : _mobile; }
            set { _mobile = value; }
        }

    }
}