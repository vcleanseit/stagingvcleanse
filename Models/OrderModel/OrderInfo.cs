﻿using System.Collections.Generic;

namespace VCleanse.Shop.Host.Models.OrderModel
{
    public class OrderInfo
    {
        public int Order_id { get; set; }

        //public int? PayMethod { get; set; }

        public List<DeliverItem> DeliverList { get; set; }

        public string Language { get; set; }


        public string SourceCode { get; set; }
    }
}
