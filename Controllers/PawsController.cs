﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz._Legacy.cfc;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Model.cfc;

namespace VCleanse.Shop.Host.Controllers
{
    public class PawsController : BaseController
    {
        //
        // GET: /Paws/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Certificate()
        {
            return View("Certificate");
        }

        /// <summary>
        /// 图片下载
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult Image(int orderId)
        {
            cfcModel cfc = cfcHelper.GetCfc(orderId);

            if (cfc == null)
            {
                return Redirect("/");
            }

            string url = "d:/Template/cfc.jpg";

            System.Drawing.Image image = System.Drawing.Image.FromFile(url);

            Graphics g = Graphics.FromImage(image);
            try
            {
                Font font = new System.Drawing.Font("Arial", 16, (System.Drawing.FontStyle.Bold));
                Font font3 = new System.Drawing.Font("Arial", 20, (System.Drawing.FontStyle.Regular));
                System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush
                    (new Rectangle(0, 0, image.Width, image.Height), Color.FromArgb(102, 102, 102), Color.FromArgb(102, 102, 102), 1.2F, true);
                g.DrawString(cfc.UserName, font, brush, 122, 424);
                g.DrawString(cfc.Number.ToString(), font, brush, 564, 458);
                g.DrawString(cfc.CertificateNumber, font3, brush, 489, 763);

                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return File(ms.ToArray(), "image/Gif");
            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }

        }

    }
}
