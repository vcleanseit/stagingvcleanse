﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Services.Imp;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;

namespace VCleanse.Shop.Host.Controllers
{
    public class SubscriptionController : BaseController
    {
        //
        // GET: /Subscriptions/

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Recommend()
        {
            return View("Recommend");
        }

        public ActionResult Detail()
        {
            return View("Detail");
        }

        [HttpPost]
        public ActionResult Subscribe(string email)
        {
           return SubscrbierInner(email, false);
        }

        [HttpPost]
        public ActionResult Unsubscribe(string email)
        {
            return SubscrbierInner(email, true);
        }

        private ActionResult SubscrbierInner(string email, bool isDisabled)
        {
            if (!ValidationUtility.IsValidEmail(email))
            {
                return CreateFailedPostJsonResult(Constants.Subscription.InvalidEmail, "Invalid email");
            }
            else
            {
                int subscriberId = 0;
                SubscriptionService service = new SubscriptionService();
                subscriberId = service.SubscriberInsertOrUpdate(email, isDisabled);
                return Json(new { IsSuccess = true, SubscrbierId = subscriberId});

            }
        }

    }
}
