﻿using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Members;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Host.Context;

namespace VCleanse.Shop.Host.Controllers
{
    public class WeChatController : AccountBaseController
    {
        /// <summary>
        /// 创建新用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateUser(WeChatUserInfo user)
        {
            JsonResult validationResult = ValidateWeChatInfo(user);
            if (validationResult != null)
            {
                return validationResult;
            }

            CreateWeiChatUserResult result = VUsersBLL.CreateWeChatUser(user, RandomHelper.FixLengthNumber(Config.PhoneValidationCodeLength));
            return Json(new { IsSuccess = true, IsExist = result.IsExists, Email = result.EMail});
        }
        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateUser(UpdateWeChatUserParam param)
        {
            bool result = VUsersBLL.UpdateWeChatUser(param);

            if (!result)
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.UUIDNotExists, "OpenID does not exist.");
            }
            
            return Json(new { IsSuccess = true });
        }
        /// <summary>
        /// 验证用户绑定状态
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CheckUserBindStatus(string openId)
        {
            if (string.IsNullOrEmpty(openId))
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.ArgumentNull, "OpenId is empty.");
            }
            
            bool result = VUsersBLL.CheckWeChatUserBindStatus(openId);

            return Json(new { IsSuccess = true, IsBinded = result });
        }
        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendValidationCode(string mobile, string language)
        {
            if (!ValidationUtility.IsCellPhone(mobile))
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.InvalidMobile, "Invalid mobile");
            }


             string key = GetCellPhoneValidationSessionKey(mobile);

             /*  ValidationCodeInfo lastValidatinInfo = Session[key] as ValidationCodeInfo;*/
             ValidationCodeInfo lastValidatinInfo = null;
             if (HttpRuntime.Cache.Get(key) == null)
             {
                  lastValidatinInfo = null;
             }
             else
             {
                 lastValidatinInfo = (ValidationCodeInfo)HttpRuntime.Cache.Get(key);
             }

             if (lastValidatinInfo != null && lastValidatinInfo.LastSend.AddSeconds(Config.CellPhoneValidationPeriodSeconds) > DateTime.UtcNow)
             {
                 return BuildPostServiceErrorResult(ServiceErrorCode.InvalidMobileValidationFrequency, "Send mobile validation code too often.");
             }

            VUsersBLL bll = new VUsersBLL();

            
            if (lastValidatinInfo == null || lastValidatinInfo.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
            {
                lastValidatinInfo = new ValidationCodeInfo()
                {
                    Phone = mobile,
                    LastSend = DateTime.UtcNow,
                    Code = RandomHelper.FixLengthNumber(Config.PhoneValidationCodeLength),
                };
            }

            lastValidatinInfo.LastSend = DateTime.UtcNow;
           // HttpRuntime.Cache.Insert(key, lastValidatinInfo);
          /*  Session[key] = lastValidatinInfo; */
            //cache[key] = lastValidatinInfo;

            var message = string.Format(LanguageHelper.IsChinese(language) ? "您的维果清验证码是{0}" :"Your VCLEANSE verification code is {0}", lastValidatinInfo.Code);
            SMSService.Instance.SendSMS(mobile, message);

            return Json(new { IsSuccess = true, ValidationCode = lastValidatinInfo.Code });
        }

        #region Non-Public Methods
        /// <summary>
        /// 验证用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private JsonResult ValidateWeChatInfo(WeChatUserInfo user)
        {
            if (user == null)
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.ArgumentNull, "Request param is null.");
            }
            //判断是否是有效手机号码
            if (!ValidationUtility.IsCellPhone(user.Mobile))
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.InvalidMobile, "Invalid mobile phone.");
            }
            /*
            string key = GetCellPhoneValidationSessionKey(user.Mobile);
             ValidationCodeInfo validationCode=null;
             if (HttpRuntime.Cache[key] == null)
             {
                 return BuildPostServiceErrorResult(ServiceErrorCode.MobileValidationIsNUll, "cache is null .");
             }
             else
             {
                 validationCode = (ValidationCodeInfo)HttpRuntime.Cache[key];
             }
            if (validationCode == null
                || !string.Equals(validationCode.Code, user.ValidationCode, StringComparison.OrdinalIgnoreCase)
                || validationCode.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
            {
                return BuildPostServiceErrorResult(ServiceErrorCode.InvalidMobileValidationCode, "Invalid validation code.");
            }
            HttpRuntime.Cache.Remove(key);
            */
            return null;
        }

        #endregion

        
    }
}
