﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.Controllers
{
    public class CommonController : BaseController
    {
        //
        // GET: /Common/

        public ActionResult Index()
        {
            return View();
        }

        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult Image()
        {
            string checkCode = GetValidation(4); // 产生5位随机验证码字符
            Session[WebConstants.Context.ImageValidationCode] = checkCode; //将字符串保存到Session中，以便需要时进行验证
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(80, 25);
            Graphics g = Graphics.FromImage(image);
            try
            {
                //生成随机生成器
                Random random = new Random();
                //清空图片背景色
                g.Clear(Color.White);
                // 画图片的背景噪音线
                int i;
                for (i = 0; i < 5; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);
                    g.DrawLine(new Pen(GetRandomColor()), x1, y1, x2, y2);
                }
                Font font = new System.Drawing.Font("Arial", 12, (System.Drawing.FontStyle.Bold));
                System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2F, true);
                g.DrawString(checkCode, font, brush, 12, 8);
                //画图片的前景噪音点
                g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return File(ms.ToArray(), "image/Gif");
            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }

        }
        private Color GetRandomColor()
        {
            int r, g, b;
            Random ra = new Random();
            r = ra.Next(0, 256);
            g = ra.Next(0, 256);
            b = ra.Next(0, 256);
            Color _color = Color.FromArgb(r, g, b);
            return _color;
        }
        public string GetValidation(int num)
        {
            string str = "ABHKMNSTWXYZ2345689"; //"或者写汉字也行"
            string validatecode = "";
            Random rd = new Random();
            for (int i = 0; i < num; i++)
            {
                validatecode += str.Substring(rd.Next(0, str.Length), 1);
            }
            return validatecode;
        }
    }
}
