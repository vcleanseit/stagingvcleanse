﻿using VCleanse.Shop.Biz.Packages;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.CityArea;
using VCleanse.Shop.Model.Packages;
using VCleanse.Shop.Model.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Model._Legacy.CityArea;

namespace VCleanse.Shop.Host.Controllers
{
    public class DataProviderController : BaseController
    {
        /// <summary>
        /// 加载所有的区域
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Region")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllRegion(string language)
        {
            CityAreaBLL areaBLL = new CityAreaBLL();
            List<CityAreaTreeInfo> treeList = areaBLL.LoadAreas(false);
            bool isChinese = LanguageHelper.IsChinese(language);
            Func<AreaInfo, string> func = null;
            if (isChinese)
            {
                func = item => item.CAName;
            }
            else
            {
                func = item => item.EnName;
            }

            //增加 ExpressCompanyID
            var simpleTree = treeList.Select(item => new
            {
                Id = item.CAID,
                Name = func(item),
                District = item.Children.Select(childItem => new
                    {
                        Id = childItem.CAID,
                        Name = func(childItem),
                        ExpressCompanyId = childItem.ExpressCompanyId,
                        ExpressCompanyName = childItem.ExpressCompanyName,
                    }).ToList(),
            });

            return Json(new { IsSuccess = true, City = simpleTree }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 根据地区自动选择物流公司
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("RegionWithProvince")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllRegionWithProvince(string language)
        {
            CityAreaBLL areaBLL = new CityAreaBLL();
            List<ProvinceTreeInfo> treeList = areaBLL.LoadAllRegionWithProvince();

            bool isChinese = LanguageHelper.IsChinese(language);
            Func<AreaInfo, string> func = null;
            if (isChinese)
            {
                func = item => item.CAName;
            }
            else
            {
                func = item => item.EnName;
            }

            var simpleTree = treeList.OrderBy(p=>p.PSort).Select(item => new
            {
                Id = item.ProvinceId,
                Name = isChinese?item.ProvinceNameCn:item.ProvinceNameEn,
                Cities = item.Cities.Where(c=>c.IsActive.Equals(true)).Select(Cities => new
                {
                    Id = Cities.CAID,
                    Name = func(Cities),
                    District = Cities.Children.Where(cc=>cc.IsActive.Equals(true)).Select(childItem => new
                    {
                        Id = childItem.CAID,
                        Name = func(childItem),
                        ExpressCompanyId = childItem.ExpressCompanyId,
                        ExpressCompanyName = childItem.ExpressCompanyName,
                        IsActive = childItem.IsActive
                    }).ToList(),
                }).ToList(),
            });



            return Json(new { IsSuccess = true, Provinces = simpleTree }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 判断邮箱是否重复
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult IsEmailDuplicated(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            VUsersBLL bll = new VUsersBLL();
            return Json(!bll.IsMailExisted(email), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 判断手机号码是否重复
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult IsPhoneDuplicated(string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            if (!ValidationUtility.IsCellPhone(phone))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            VUsersBLL bll = new VUsersBLL();
            return Json(!bll.IsPhoneExisted(phone), JsonRequestBehavior.AllowGet);
            //return Json(false, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 加载所有的套餐
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Package")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllPackages(string language)
        {
            object package = LoadPackageInner(language,null,null);
            return Json(new { IsSuccess = true, Packages = package }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 根据类别找到套餐
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadPackageByCategoryId")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadPackageByCategoryId(int categoryId, string language)
        {
            object package = LoadPackageInner(language, categoryId,null);
            return Json(new { IsSuccess = true, Packages = package }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 根据套餐ID找到套餐信息
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadPackageByPackageId")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadPackageByPackageId(int packageId, string language)
        {
            object package = LoadPackageInner(language,null, packageId);
            return Json(new { IsSuccess = true, Packages = package }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 加载套餐内容
        /// </summary>
        /// <param name="language"></param>
        /// <param name="categoryId"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public object LoadPackageInner(string language, int? categoryId ,int? packageId)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            var packageList = PackageBLL.LoadAllPackages();
            IEnumerable<PackageInfo> packages = packageList;
            
            if (packageId != null)
            {
                packages = packageList.Where(item => item.ID == packageId);
            }
            else
            {
                packages = packageList.Where(item => item.IsActive.Equals(true));
            }

            if (categoryId != null)
            {
                packages = packages.Where(item => item.CategoryId == categoryId);
            }

            var packageTypesDict = PackageBLL.LoadPackageGroupTypes();
            var products = PackageBLL.LoadAllProducts();
            object package = packages.Select(item => new
            {
                Id = item.ID,
                Title = isChinese ? item.Name : item.ENName,
                SubTitle = isChinese ? item.Title : item.ENTitle,
                Description = isChinese ? item.Description : item.ENDescription,
                Url= isChinese? item.CNUrl: item.ENUrl,
                Tips=isChinese? item.Tips:item.ENTips,
                Introduction=isChinese? item.Introduction:item.ENIntroduction,
                ImageName = isChinese ? item.ImageName : item.ENImageName,
                Tags = item.Tags.Select(tagItem => new
                {
                    TagId = tagItem.Id,
                    Name = isChinese ? tagItem.Name : tagItem.EnName,
                    ImagePath = tagItem.ImagePath,
                }),
                Groups = item.PackageGroups.Select(groupItem => new
                    {
                        Id = groupItem.ID,
                        Name = GetGroupTypeString(packageTypesDict, isChinese, groupItem.GroupTypeID),
                        GroupTypeId = groupItem.GroupTypeID,
                        ByWeek = groupItem.IsByWeek,
                        Rules = groupItem.DayRules.OrderBy(rule => rule.Days)
                            .SelectMany(dayRuleItem => dayRuleItem.GroupRules.OrderBy(dayItem => dayItem.RuleValue)
                                .Select(ruleItem => new
                                            {
                                                Id = ruleItem.ID,
                                                Day = ruleItem.RuleValue,
                                                Name = (isChinese ? item.Name : item.ENName) + " " + ruleItem.RuleValue.ToString(),
                                                PackagePrice = ruleItem.PackagePrice,
                                                Products = ruleItem.RuleDetail.Join(products, detailItem => detailItem.ProductID
                                                    , productItem => productItem.ProductsID
                                                    , (detailItem, productItem) => new
                                                       {
                                                           Count = detailItem.Count,
                                                           Id = productItem.ProductsID,
                                                           Name = isChinese ? productItem.ProductsName : productItem.ENProductsName,
                                                           Image = productItem.ProductsImage,
                                                           Price = productItem.ProductsPrice,
                                                           Url = isChinese ? productItem.CNUrl : productItem.ENUrl,
                                                           MaxCount = productItem.ProductsMaxN,
                                                           Description = isChinese ? productItem.Description : productItem.ENDescription,
                                                       }
                                                    ),
                                            })
                                    )
                    }),
            });
            return package;
        }

        /// <summary>
        /// 套餐类别
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("PackageCategory")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadPackageCategory(string language)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            var categoryList = PackageBLL.LoadPackageCategory();
            var categories = categoryList.OrderBy(item => item.DisplayOrder).Select(item => new
            {
                Id = item.id,
                Name = isChinese ? item.Name : item.ENName,
                Description = isChinese ? item.Description : item.ENDescription,
            });
            return Json(new { IsSuccess = true, Categories = categories }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 产品类别
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("ProductCategory")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadProductCategory(string language)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            var categoryList = PackageBLL.LoadProductCategory();
            var categories = categoryList.OrderBy(item => item.DisplayOrder).Select(item => new
            {
                Id = item.id,
                Name = isChinese ? item.Name : item.ENName,
                Description = isChinese ? item.Description: item.ENDescription,
                ImageUrl = item.ImageUrl,
                ColorClass= item.ColorClass,
            });
            return Json(new { IsSuccess = true, Categories = categories }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 所有产品
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Products")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllProducts(string language)
        {
            return innerProducts(language);
        }
        /// <summary>
        /// 通过类别加载产品
        /// </summary>
        /// <param name="language"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadProductsByCategoryId")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadProductsByCategoryId(string language, int categoryId)
        {
            return innerProducts(language, categoryId, 0);
        }
        /// <summary>
        /// 通过DaliyValidProduct表  查找产品
        /// </summary>
        /// <param name="language"></param>
        /// <param name="dayId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadProductsByDayId")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadProductsByDayId(string language, int dayId)
        {
            return innerProducts(language, 0, dayId);
        }
        /// <summary>
        /// 获取产品
        /// </summary>
        /// <param name="language"></param>
        /// <param name="categoryId"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        private ActionResult innerProducts(string language, int categoryId = 0, int day = 0)
        {
            bool isChinese = LanguageHelper.IsChinese(language);

            var productList = (day > 0 && day < 8) ? PackageBLL.LoadAllProductsByDay(day).Where(p => p.IsActive.Equals(true)) : PackageBLL.LoadAllActiveProducts();

            var products = productList.OrderBy(item => item.PSort).Select(item => new
            {
                Id = item.ProductsID,
                Name = isChinese ? item.ProductsName : item.ENProductsName,
                Image = item.ProductsImage,
                Price = item.ProductsPrice,
                MaxCount = item.ProductsMaxN,
                Description = isChinese ? item.Description : item.ENDescription,
                Url = isChinese ? item.CNUrl : item.ENUrl,
                ProductTypeId = item.ProductCategoryId,
                DayActive= item.DayActive
            });

            if (categoryId > 0)
            {
                products = products.Where(item => item.ProductTypeId == categoryId);
            }
            return Json(new { IsSuccess = true, Products = products }, JsonRequestBehavior.AllowGet);
        }

        private string GetGroupTypeString(Dictionary<byte, KeyValueType> dict, bool isChinese, byte key)
        {
            if (dict.ContainsKey(key))
            {
                var item = dict[key];
                return isChinese ? item.Name : item.ENName;
            }
            return string.Empty;
        }

        /// <summary>
        /// 加载所有发票类型
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("InvoiceTypes")]
        public ActionResult LoadAllInvoiceTypes(string language)
        {
            var invoiceTypes = SharedBLL.LoadAllInvoiceTypes();
            bool isChinese = LanguageHelper.IsChinese(language);
            Func<KeyValueType, string> func = null;
            if (isChinese)
            {
                func = item => item.Name;
            }
            else
            {
                func = item => item.ENName;
            }
            return Json(new
            {
                IsSuccess = true,
                InvoiceTypes = invoiceTypes.Select(item => new
                {
                    Id = item.Id,
                    Name = func(item),
                })
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 加载订单配置
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult LoadOrderConfig()
        {
            DateTime now = DateTime.Now;
            DateTime today = now.Date;
            DateTime allowedOrderDate = now.AddMinutes(Config.OrderBeforeMinutes).Date;
            return Json(new
            {
                IsSuccess = true
                ,
                Today = DateTimeUtility.CreateCustomizedDate(today)
                ,
                AllowedFirstOrderDate = DateTimeUtility.CreateCustomizedDate(allowedOrderDate)
                ,
                LogisticsFee = Config.LogisticsFee.ToString("###.#0")
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 加载送货时间
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadDeliverTime")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadDeliverTime(string language)
        {
            var deliverDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadDeliverTime);
            return KeyValueTypesReturn(deliverDict, language);
        }

        /// <summary>
        /// 通过区域选择送货时间
        /// </summary>
        /// <param name="language"></param>
        /// <param name="areaId"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadDeliverTimeByArea")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadDeliverTimeByArea(string language, int areaId)
        {
            var deliverDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadDeliverTimeByAreaId, areaId);
            return KeyValueTypesReturn(deliverDict, language);
        }

      
        /// <summary>
        /// 加载支付方式
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadPayment")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadPayment(string language)
        {
            var deliverDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);
            return KeyValueTypesReturn(deliverDict, language);
        }
        /// <summary>
        /// 参数名称和参数值
        /// </summary>
        /// <param name="deliverDict"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        private JsonResult KeyValueTypesReturn(Dictionary<byte, KeyValueType> deliverDict, string language)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            return Json(new
            {
                IsSuccess = true,
                Values = deliverDict.Values.OrderBy(item => item.Id).Select(item => new { Id = item.Id, Name = isChinese ? item.Name : item.ENName })
            }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 时间转换
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult FirstAllowedOrderDate()
        {
            var time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow.AddMinutes(Config.OrderBeforeMinutes), TimeZoneInfo.FindSystemTimeZoneById("China Standard Time"));
            return Json(time);
        }

        /// <summary>
        /// 加载所有快递公司
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("ExpressCompany")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadExpressCompany(string language)
        {
            var list = SharedBLL.LoadAllExpressCompany();
            return Json(new { ExpressCompanies = list, IsSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 加载所有门店信息
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("LoadAllStores")]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.CacheByParam)]
        public ActionResult LoadAllStores(string language)
        {
            var list = SharedBLL.LoadAllStores();
            var storeList = list.Select(item => new { 
                   StoreId=item.StoreId,
                   StoreName=IsChinese(language)?item.StoreName:item.StoreEnName,
                   StoreAddress =IsChinese(language)?item.StoreAddress:item.StoreEnAddress,
                   StoreDescription=IsChinese(language)?item.StoreDescription:item.StoreEnDescription,
                   StoreBusinessHours = IsChinese(language) ? item.StoreBusinessHours : item.StoreEnBusinessHours,
                   StoreTel = item.StoreTel,
                   StoreCityId= item.StoreCityId,
                   StoreCityName = IsChinese(language) ? item.StoreCityName : item.StoreCityEnName,
                   StoreMapUrl = item.StoreMapUrl,
                   IsActive=item.IsActive,
                }
            );

            return Json(new { Stores = storeList, IsSuccess = true }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 子订单状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult OrderStatusCode()
        {
            return Json(new { Code =  SharedBLL.LoadAllSubOrderStatusCode(), IsSuccess = true }, JsonRequestBehavior.AllowGet);
        }
    }
}
