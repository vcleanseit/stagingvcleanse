using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Packages;
using VCleanse.Shop.Biz.Promotion;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Model.Promotions;
using VCleanse.Shop.Model.Shared;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Models.OrderModel;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Biz;
using OrderMDL = VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Tracking;
using System.Web.Script.Serialization;
using VCleanse.Shop.Biz.GG;
using VCleanse.Shop.Model.GG;
using VCleanse.Shop.Biz.Log;
using VCleanse.Shop.Model.WeChatOrders;
using VCleanse.Shop.Host.Utility;
using System.Net;
using System.IO;
using System.Text;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Model.cfc;
using VCleanse.Shop.Biz._Legacy.cfc;
using VCleanse.Shop.Data.Log;
using VCleanse.Shop.Model.CityArea;
using VCleanse.Shop.Model._Legacy.Orders;
using VCleanse.Shop.Model.SMS;

namespace VCleanse.Shop.Host.Controllers
{
    public class OrderController : BaseController
    {
        #region <<View返回路径>>
        public ActionResult Index()
        {
            return Redirect("/cleanse/index");
        }

        // For old website users(add to favorite icon)
        public ActionResult Package()
        {
            return Redirect("/");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Search()
        {
            return View("Search");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Orders()
        {
            return View("Orders");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult OrderDetail()
        {
            return View("OrderDetail");
        }

        [MemberAuthorize(ToLogin = true)]
        public ActionResult Customize()
        {
            return View("Customize");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Receiver()
        {
            return View("Receiver");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Orderinfo()
        {
            return View("orderinfo");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Confirm()
        {
            return View("Confirm");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult ConfirmTest()
        {
            return View("ConfirmTest");
        }

        [HttpGet]
        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Success(int orderId)
        {
            return View("Success");
        }
        #endregion

       


        #region <<创建订单>>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult CreateOrder(OrderInfo orderItem)
        {
            #region <<验证客户端订单信息正确性>>
            //写入日志
            NLogHelper.LogInfo("OrderParameters", new JavaScriptSerializer().Serialize(orderItem));
            if (orderItem == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            if (orderItem.DeliverList == null || orderItem.DeliverList.Count == 0)
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.OrderItemEmpty, "Empty object");
            }

            #endregion

            #region <<创建订单信息>>

            ///当前登录人接收地址
            var receivers = this.CurrentContext.Member.Receivers;
            ///创建订单信息
            OrderMDL.OrderInfo orderInfo = new OrderMDL.OrderInfo()
            {
                ///收货地址list
                DeliverList = orderItem.DeliverList.Select(item => new OrderMDL.DeliverItemInfo()
                {
                    Receiver_id = item.ReceiverId,
                    Date = item.Date,
                    IsSelfpickup = item.IsSelfpickup,
                    ReceiverInfo = CreateReceiverInfo(item, receivers),
                    outOfPackageProducts = CreateProductInfo(item.Products, 0, 0),
                    Products = CreateProductInfo(item.Products, item.PackageRuleId, item.PackageRuleCount),
                    DeliverTimeId = item.DeliverTimeId,
                    PackageRuleId = item.PackageRuleId,
                    PackageRuleCount = item.PackageRuleCount
                }).ToList(),
                Member_id = CurrentMember_id,
                Order_id = orderItem.Order_id,
            };
            orderInfo.TotalPrice = AccountTotalPrice(orderInfo);
            orderInfo.LogisticsFee = OrdersBLL.TotalLogisticsFee(orderInfo);
            orderInfo.SourceCodeId = OrdersBLL.getSourceIdByCode(orderItem.SourceCode);

            #endregion

            #region <<验证创建的订单的正确性>>
            //验证订单送货地址、送的产品是否为空
            if (orderInfo.DeliverList == null || orderInfo.DeliverList.Count == 0 || orderInfo.DeliverList[0].Products.Count == 0)
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.OrderItemEmpty, "Empty object");
            }
            //验证送货地址是否有效
            if (OrdersBLL.CheckAreaIsActive(orderInfo).Equals(false))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.AreaIsNotActive, "The area is not available");
            }
            //判断送货时间是否在下单之前
            if (CheckOrdersmDate(orderItem.DeliverList))
            {
                NLogHelper.LogInfo("OrderDateError", new JavaScriptSerializer().Serialize(orderItem.DeliverList) + " | " + HttpHelper.getLogContext());
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.OrderDateError, "OrderDateError");
            }
            //判断是否来源是否是月亮熊
            OrdersBLL.CheckEventContidition(orderInfo);

            //写入日志
            NLogHelper.LogInfo("OrderAfterProcess", new JavaScriptSerializer().Serialize(orderInfo));
            #endregion

            #region <<在数据库创建数据并打包返回Json>>
            int status_id = OrdersBLL.CreateOrder(orderInfo);
            if (status_id > 0)
            {
                //add order log 140714
                NLogHelper.LogInfo("CreateOrderSuccess", string.Format("OrderId=[{0}]", status_id));
                OrdersBLL.SaveOrderSession(status_id, VcleanseContext.Current.Session_id);
                return Json(new { IsSuccess = true, Order_id = status_id });
            }
            else
            {
                NLogHelper.LogInfo("CreateOrderFail", string.Format("StatusId=[{0}]", status_id));
                if (status_id == Constants.CreateOrderStatus.InvalidOrderOwner)
                {
                    return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid order id or not own to current user");
                }
                else
                {
                    return CreateFailedPostJsonResult(Constants.GeneralStatus.UnpextedError, "Error code:{0}", status_id);
                }
            }
            #endregion
        }
        #endregion

        #region <<订单付款>>
        /// <summary>
        /// 确认付款
        /// </summary>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]        
        public JsonResult ConfirmOrder(OrderMDL.ConfirmOrderInfo confirmInfo)
        {
            #region <<验证优惠码>>
            NLogHelper.LogInfo("ConfirmParameters", new JavaScriptSerializer().Serialize(confirmInfo));
            if (confirmInfo.Promotion != null)
            {
                //使用优惠券  true  代表使用
                HandlePromotion(confirmInfo.Promotion, true);
            }
            #endregion
           

            var orderInfo = OrdersBLL.LoadOrderById(confirmInfo.OrderId);
            if (orderInfo == null || orderInfo.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid order id");
            }

            //invoice
            SaveInvoice(orderInfo, confirmInfo.Invoice);

            //payment
            if (confirmInfo.Payment != null)
            {
                SavePayment(orderInfo, confirmInfo.Payment);
                // Special promotion: Buy 5, give 1 day gift card
                OrdersBLL.ConfirmEventContidition(orderInfo, confirmInfo.Payment.Payment);
            }

            //source code 默认未知来源  1
            byte sourceId = OrdersBLL.getSourceIdByCode(confirmInfo.SourceCode);
            if (!sourceId.Equals(Constants.OrderSource.Unknow_Sources) && !sourceId.Equals(orderInfo.OrderSourceId))
            {
                OrdersBLL.SaveOrderSource(confirmInfo.OrderId, sourceId);
            }

            //giftinfo
            if (confirmInfo.GiftInfo != null)
            {
                SaveGiftInfo(orderInfo, confirmInfo.GiftInfo);
            }
            return Json(new { IsSuccess = true, });
        }
        #endregion

        #region <<删除订单>>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult DeleteOrder(int orderId, string language)
        {
            NLogHelper.LogInfo("DeleteOrder", orderId.ToString());
            //获取所有子订单信息
            var orderItem = OrdersBLL.LoadOrderById(orderId);
            if (orderItem == null || orderItem.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedPostJsonResult(Constants.CancelOrderStatus.NotAuthorized, "Non authorized action");
            }
            if (OrderStatusHelper.checkOrderCannotDelete(orderItem))
            {
                return CreateFailedPostJsonResult(Constants.CancelOrderStatus.CanNotCancel, "Can not be deleted");
            }
            else
            {
                //执行删除
                bool deleteStatus = OrdersBLL.DeleteOrder(orderId);
                if (deleteStatus)
                {
                    string message = "";
                    if (LanguageHelper.IsChinese(language))
                    {
                        message = "您的订单{0}已取消。谢谢！";
                    }
                    else
                    {
                        message = "Dear customer, your order {0} has been cancelled. Thank you for your interests.";
                    }
                    string mobile = VcleanseContext.Current.Member.Mobile;
                   
                    SMSService.Instance.SendSMS(mobile, string.Format(message, orderItem.OrdersID));

                    NLogHelper.LogInfo(orderItem.OrdersID.ToString(), "delete order");
                    return Json(new { IsSuccess = true, StatusCode = Constants.GeneralStatus.Success });
                }
                else
                {
                    return CreateFailedPostJsonResult(Constants.CancelOrderStatus.CanNotCancel, "Can not be deleted");
                }
            }
        }

        #endregion

        #region <<修改订单的信息>>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult UpdateOrdersmReceiver(UpdateOrdersmReceiverParams param)
        {
            #region <<数据验证>>
            var validResult = ValidUpdateOrdersmReceiverParams(param);
            if (validResult != null)
            {
                NLogHelper.LogInfo("UpdateReceiverError"
                    , new JavaScriptSerializer().Serialize(validResult) + new JavaScriptSerializer().Serialize(param));
                return validResult;
            }
            #endregion

            #region <<数据库操作>>
            //数据库执行操作
            int result = OrdersBLL.UpdateReceiverByOrdersmIdAndReceiverId(CurrentMember_id, param.SubOrderId, param.ReceiverId, param.DeliveryTimeId);           
            #endregion

            #region <<数据返回>>
             if (result == 1)
            {
                //add order log 140714
                OrderLogHelper.WriteOrderLog_UpdateReceiver(param.SubOrderId, param.ReceiverId, param.DeliveryTimeId, CurrentContext.Member_id, "UpdateReceiverSuccess");
                return Json(new { IsSuccess = true, StatusCode = Constants.GeneralStatus.Success });
            }
            else
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "no record");
            }
            #endregion

        }
        #endregion

        #region <<查询订单>>

        /// <summary>
        /// 分页获取个人订单
        /// </summary>
        [MemberAuthorize]
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public JsonResult LoadMyOrders(string member_id, int startIndex, int count, string language)
        {
            if (count > Constants.QueryOrderStatus.MAX_QUERY_COUNT)
            {
                return CreateFailedGetResult(Constants.GeneralStatus.InvalidParameter, "count:{0}", count);
            }
            var orders = OrdersBLL.LoadMyOrder(CurrentMember_id, startIndex, count);
            return LoadMyOrdersInner(language, orders);
        }

        /// <summary>
        /// 通过手机号码获取订单
        /// </summary>
        [HttpGet]
        [MemberAuthorize(AllowImpersonate = true)]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public JsonResult LoadOrderByMobile(string ValidationCode, string mobile, int startIndex, int count, string language)
        {
            NLogHelper.LogInfo("LoadOrderByMobile", mobile);
            JsonResult validateResult = ValidateMobileInfo(mobile, ValidationCode);

            if (validateResult != null)
            {
                return validateResult;
            }

            var orders = OrdersBLL.LoadAnonymityOrderByMobile(mobile, startIndex, count);

            if (orders.Total < 1)
            {
                return Json(new { IsSuccess = true, Total = orders.Total }, JsonRequestBehavior.AllowGet);
            }
            return LoadMyOrdersInner(language, orders);
        }
        /// <summary>
        /// 获取订单信息
        /// </summary>
        private JsonResult LoadMyOrdersInner(string language, OrderMDL.MyOrderInfo orders)
        {
            var payStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayStatus);
            var orderStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderStatus);
            var paymentDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);

            var orderStatusCode = SharedBLL.LoadAllOrderStatusCode();

            bool isChinese = IsChinese(language);

            //TODO:  loadmyorders
            var myOrder = orders.Orders.Select(item => new
            {
                Id = item.OrdersID,
                //IsPaid = item.IsPaid == Constants.QueryOrderStatus.Paid || item.Payment == Constants.Payment.PayOnDelivery,
                //PayStatus = GetPayStatus(item.IsPaid, payStatus, isChinese, item.Payment, paymentDict),
                OrderDate = item.OrdersDate.ToDisplayString(),
                FirstDeliveryDate = item.FristDeliverDate.ToDisplayString(),
                Amount = item.OrdersPrice,
                //OrderStatusID = ResetOrderStatus(item),
                LogisticsFee = item.LogisticsFee,
                OrderStatusCode = item.StatusCode,
                OrderStatusCodeString = OrderStatusHelper.GetStatusCodeString(item.StatusCode, orderStatusCode, isChinese),
                //OrderStatus = GetStatusString(item.IsPaid, ResetOrderStatus(item), payStatus, orderStatus, item.Payment, paymentDict, isChinese),
                //CanCancel = item.IsPaid != Constants.QueryOrderStatus.Paid && item.FirstOrderItemStatus == Constants.OrderItemStatus.Waiting,
                OrderPayments = OrderStatusHelper.OrderPaymentList(OrdersBLL.LoadAllOrderPayments(item.OrdersID), paymentDict, isChinese),
                PreferentialPrice = item.PreferentialPrice,
                PreferentialLogisticsFee = item.PreferentialLogisticsFee,
            });
            return Json(new { IsSuccess = true, Orders = myOrder, Total = orders.Total }, JsonRequestBehavior.AllowGet);
        }


        #region wechat view

        [HttpGet]
        [MemberAuthorize(ToLogin = true)]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult worders(string openId, int? type)
        {
            if (!type.HasValue)
                type = 0;

            List<WeChatMyOrderItemInfo> Orderlist = new List<WeChatMyOrderItemInfo>();

            if (string.IsNullOrEmpty(openId))
            {
                return View(Orderlist);
            }


            Session[VCleanse.Shop.Model.Constants.WechatStaticString.OPENID] = openId;

            int startIndex = 1;
            int count = 50;
            string language = "cn";

            int member_id = 0;
            member_id = VUsersBLL.getUserIdByOpenId(openId);

            if (member_id < 1)
            {
                return View(Orderlist);
            }

            var orders = OrdersBLL.LoadMyOrder(member_id, startIndex, count);
            var payStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayStatus);
            var orderStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderStatus);
            var paymentDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);
            bool isChinese = IsChinese(language);
            var orderStatusCode = SharedBLL.LoadAllOrderStatusCode();

            List<VCleanse.Shop.Model.Orders.MyOrderItemInfo> orderFilter = new List<VCleanse.Shop.Model.Orders.MyOrderItemInfo>();

            if (type == Constants.WechatSearchType.WillPay)
            {

                //TODO 微信待付款订单
                //orderFilter = orders.Orders.Where(item => item.Payment.Equals((byte)Constants.Payment.PayOnLine) && item.IsPaid.Equals((byte)Constants.IsPaid.UnPaid) && !item.OrderStatus.Equals((byte)Constants.OrderStatus.Expired)).ToList();
                orderFilter = orders.Orders.Where(item => item.StatusCode.Equals(Constants.StatusCode.NotPaid) && OrderStatusHelper.checkOrderIsExpired(item.OrdersDate)).ToList();
                ViewData[VCleanse.Shop.Model.Constants.WechatStaticString.PAGENAME] = "我的待付款订单";
                ViewData[VCleanse.Shop.Model.Constants.WechatStaticString.Type] = type.Value;
            }
            else
            {
                ViewData[VCleanse.Shop.Model.Constants.WechatStaticString.PAGENAME] = "我的所有订单";
                orderFilter = orders.Orders;
                ViewData[VCleanse.Shop.Model.Constants.WechatStaticString.Type] = type.Value;
            }



            if (orders == null)
            {
                return View(Orderlist);
            }

            //TODO: 微信状态显示
            foreach (var item in orderFilter)
            {
                Orderlist.Add(new WeChatMyOrderItemInfo
                {
                    Id = item.OrdersID,
                    //IsPaid = item.IsPaid == Constants.QueryOrderStatus.Paid || item.Payment == Constants.Payment.PayOnDelivery,
                    //PayStatus = GetPayStatus(item.IsPaid, payStatus, isChinese, item.Payment, paymentDict),
                    OrderDate = item.OrdersDate.ToDisplayString(),
                    FirstDeliveryDate = item.FristDeliverDate.ToDisplayString(),
                    Amount = item.OrdersPrice,
                    //OrderStatusID = ResetOrderStatus(item),
                    LogisticsFee = item.LogisticsFee,
                    //OrderStatus = GetStatusString(item.IsPaid, ResetOrderStatus(item), payStatus, orderStatus, item.Payment, paymentDict, isChinese),
                    //CanCancel = item.IsPaid != Constants.QueryOrderStatus.Paid && item.FirstOrderItemStatus == Constants.OrderItemStatus.Waiting,

                    OrderStatusCode = item.StatusCode,
                    OrderStatusCodeString = OrderStatusHelper.GetStatusCodeString(item.StatusCode, orderStatusCode, isChinese),
                    OrderPayments = OrderStatusHelper.OrderPaymentListForWeChat(OrdersBLL.LoadAllOrderPayments(item.OrdersID), paymentDict, isChinese),

                    PreferentialPrice = item.PreferentialPrice,
                    PreferentialLogisticsFee = item.PreferentialLogisticsFee,
                    WeChatOrderItemList = null
                });
            }

            return View(Orderlist);
        }

        [HttpGet]
        [MemberAuthorize(ToLogin = true)]
        public ActionResult wdetails(int orderId)
        {
            WeChatOrderItemInfo orderItem = new WeChatOrderItemInfo();
            orderItem = getOrderInfoView(orderId);
            return View(orderItem);
        }

        [HttpGet]
        [MemberAuthorize(ToLogin = true)]
        public ActionResult wdelivering(string openId)
        {
            if (string.IsNullOrEmpty(openId))
            {
                return View();
            }

            int member_id = 0;
            member_id = VUsersBLL.getUserIdByOpenId(openId);

            if (member_id < 1)
            {
                return View();
            }
            Session[VCleanse.Shop.Model.Constants.WechatStaticString.OPENID] = openId;

            List<VCleanse.Shop.Model.Orders.OrderDeliverItemInfo> orderDeliveringItem = OrdersBLL.LoadWeChatOrderDeliveringByUserId(member_id);
            if (orderDeliveringItem == null || orderDeliveringItem.Count < 1)
            {
                return View();
            }
            if (orderDeliveringItem.Count == 1)
            {
                return Redirect(string.Format("wshipper?shipid={0}&storeId={1}&expresscompany={2}&isRedirect=1", orderDeliveringItem[0].DeliveryNo, orderDeliveringItem[0].StoreId, orderDeliveringItem[0].ExpressCompany));
            }

            Dictionary<byte, KeyValueType> payStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayStatus);
            Dictionary<byte, KeyValueType> orderStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderStatus);
            Dictionary<byte, KeyValueType> orderItemStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderItemStatus);
            Dictionary<byte, KeyValueType> deliverTimeDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadDeliverTime);
            Dictionary<byte, KeyValueType> paymentDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);
            Dictionary<int, IntegeKeyValueType> cityDict = SharedBLL.LoadAllCityArea();
            List<ExpressCompanyInfo> expressCompanyList = SharedBLL.LoadAllExpressCompany();
            bool isChinese = true;

            List<WeChatOrderDeliverItemInfo> item = getWeChatOrderDeliverItem(orderDeliveringItem, isChinese, payStatus, orderStatus,
                                                         orderItemStatus, deliverTimeDict, paymentDict, cityDict, expressCompanyList);

            ViewData[VCleanse.Shop.Model.Constants.WechatStaticString.PAGENAME] = "我的待收货订单";
            return View(item);
        }

        [HttpGet]
        [MemberAuthorize(ToLogin = true)]
        public ActionResult wshipper(string shipid, string storeId, string expresscompany)
        {

            VCleanse.Shop.Host.Service.WeChatShipment.IWeChatShipmentService shipmentInfo = VCleanse.Shop.Host.Service.WeChatShipment.WeChatShipmentFactory.GetShipment(expresscompany);

            return View(shipmentInfo.GetShipmentInfo(shipid, storeId));

        }
        #endregion
        #region wechat view private method


        private WeChatOrderItemInfo getOrderInfoView(int orderId, string language = "cn")
        {

            VCleanse.Shop.Model.Orders.OrderItemInfo orderInfo = OrdersBLL.LoadOrderById(orderId);
            Dictionary<byte, KeyValueType> payStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayStatus);
            Dictionary<byte, KeyValueType> orderStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderStatus);
            Dictionary<byte, KeyValueType> orderItemStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderItemStatus);
            Dictionary<byte, KeyValueType> deliverTimeDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadDeliverTime);
            Dictionary<byte, KeyValueType> paymentDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);
            Dictionary<int, IntegeKeyValueType> cityDict = SharedBLL.LoadAllCityArea();
            List<ExpressCompanyInfo> expressCompanyList = SharedBLL.LoadAllExpressCompany();

            var orderStatusCode = SharedBLL.LoadAllOrderStatusCode();
            var subOrderStatusCode = SharedBLL.LoadAllSubOrderStatusCode();

            bool isChinese = IsChinese(language);

            if (orderInfo == null)
            {
                return null;
            }

            //TODO: Wechat loadorderid
            WeChatOrderItemInfo orderInfoItem = new WeChatOrderItemInfo
            {
                OrderID = orderInfo.OrdersID,
                TotalPrice = orderInfo.TotalPrice,
                DeliverFee = orderInfo.DeliverFee,
                //PaymentId = orderInfo.Payment,
                // Payment = GetNameForByte(orderInfo.Payment, paymentDict, isChinese),
                // IsPaid = orderInfo.IsPaid == Constants.QueryOrderStatus.Paid || orderInfo.Payment == Constants.Payment.PayOnDelivery,
                //PayStatus = GetPayStatus(orderInfo.IsPaid, payStatus, isChinese, orderInfo.Payment, paymentDict),
                OrderDate = orderInfo.OrdersDate.ToDisplayString(),
                //OrderPayDate = orderInfo.OrderPayDate.ToDisplayString(),
                //OrderStatusID = ResetOrderStatus(orderInfo),
                //OrderStatus = GetStatusString(orderInfo.IsPaid, ResetOrderStatus(orderInfo), payStatus, orderStatus, orderInfo.Payment, paymentDict, isChinese),
                OrderStatusCode = orderInfo.StatusCode,
                OrderStatusCodeString = OrderStatusHelper.GetStatusCodeString(orderInfo.StatusCode, orderStatusCode, isChinese),
                OrderPayments = OrderStatusHelper.OrderPaymentListForWeChat(OrdersBLL.LoadAllOrderPayments(orderInfo.OrdersID), paymentDict, isChinese),
                PreferentialPrice = orderInfo.PreferentialPrice,
                PreferentialLogisticsFee = orderInfo.PreferentialLogisticsFee,
                DeliverList = getWeChatOrderDeliverItem(orderInfo.DeliverList, isChinese, payStatus, orderStatus,
                                                        orderItemStatus, deliverTimeDict, paymentDict, cityDict, expressCompanyList)
            };

            //ViewData["orderInfoItem"] = orderInfoItem;
            return orderInfoItem;
        }

        private List<WeChatOrderDeliverItemInfo> getWeChatOrderDeliverItem(
            List<VCleanse.Shop.Model.Orders.OrderDeliverItemInfo> itemList,
            bool isChinese,
            Dictionary<byte, KeyValueType> payStatus,
            Dictionary<byte, KeyValueType> orderStatus,
            Dictionary<byte, KeyValueType> orderItemStatus,
            Dictionary<byte, KeyValueType> deliverTimeDict,
            Dictionary<byte, KeyValueType> paymentDict,
            Dictionary<int, IntegeKeyValueType> cityDict,
            List<ExpressCompanyInfo> expressCompanyList)
        {
            List<WeChatOrderDeliverItemInfo> Orderlist = new List<WeChatOrderDeliverItemInfo>();
            foreach (var item in itemList)
            {
                Orderlist.Add(new WeChatOrderDeliverItemInfo
                {
                    OrdersMID = item.OrdersMID,
                    OrderId = item.OrderId,
                    DeliverDate = item.DeliverDate.ToDisplayString(),
                    Mobile = item.Mobile,
                    OrdersMUser = item.OrdersMUser,
                    Address = item.IsSelfPickUp ? string.Format("门店自提：{0}", BuildStoreAddress(item.StoreId, isChinese)) : BuildAddress(item, cityDict, isChinese),
                    OrdersCity = item.OrdersCity,
                    OrdersArea = item.OrdersArea,
                    //OrderStatusID = item.OrderStatusID,
                    //OrderStatus = GetNameForByte(item.OrderStatusID, orderItemStatus, isChinese),
                    IsSelfPickUp = item.IsSelfPickUp,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    DeliverTimeId = item.DeliverTimeId,
                    DeliverTime = item.DeliverTimeId == 0 ? string.Empty : GetNameForByte((byte)item.DeliverTimeId, deliverTimeDict, isChinese),
                    SubPrice = item.ProductList == null ? 0 : item.ProductList.Sum(productItem => productItem.TotalPrice),
                    ExpressCompanyId = item.ExpressCompany,
                    ExpressCompanyName = GetExpressCompanyName(expressCompanyList, item.ExpressCompany),
                    DeliveryNo = item.DeliveryNo,
                    DeliveryRemark = item.Remark,
                    StoreId = item.StoreId,
                    ProductList = getWeChatOrderProductItem(item.ProductList, isChinese),
                    DeliverNo = OrdersBLL.GetDelveryNoByOrdersMID(item.OrdersMID),

                });
            }
            return Orderlist;
        }
        private List<WeChatOrderProductItemInfo> getWeChatOrderProductItem(List<VCleanse.Shop.Model.Orders.OrderProductItemInfo> itemList, bool isChinese)
        {

            List<WeChatOrderProductItemInfo> OrderProductItemlist = new List<WeChatOrderProductItemInfo>();
            if (itemList == null)
            {
                return OrderProductItemlist;
            }
            foreach (var productItem in itemList)
            {
                OrderProductItemlist.Add(new WeChatOrderProductItemInfo
                {
                    OrdersMXID = productItem.OrdersMXID,
                    ProductId = productItem.ProductId,
                    ProductName = isChinese ? productItem.ProductName : productItem.ENProductName,
                    Count = productItem.Count,
                    TotalPrice = productItem.TotalPrice,
                    ImageUrl = productItem.ImageUrl
                });
            }
            return OrderProductItemlist;
        }

        #endregion

        //private byte ResetOrderStatus(OrderMDL.MyOrderItemInfo item)
        //{
        //    //TODO: ordermain status loadmyorders
        //    //if (item.IsPaid == Constants.QueryOrderStatus.Paid || item.Payment == Constants.Payment.PayOnDelivery)
        //    //{
        //    //    return item.OrderStatus;
        //    //}
        //    //else if (item.OrdersDate.AddMinutes(Config.OrderBeforeMinutes) < DateTime.Now)
        //    //{
        //    //    return Constants.OrderStatus.Expired;
        //    //}
        //    //else
        //    //{
        //    //    return item.OrderStatus;
        //    //}
        //    return item.OrderStatus;
        //}

        //private byte ResetOrderStatus(OrderMDL.OrderItemInfo orderInfo)
        //{
        //    //TODO OrderMain Status
        //    //if (orderInfo.IsPaid == Constants.QueryOrderStatus.Paid || orderInfo.Payment == Constants.Payment.PayOnDelivery)
        //    //{
        //    //    return orderInfo.OrderStatus;
        //    //}
        //    //else if (orderInfo.OrdersDate.AddMinutes(Config.OrderBeforeMinutes) < DateTime.Now)
        //    //{
        //    //    return Constants.OrderStatus.Expired;
        //    //}
        //    //else
        //    //{
        //    //    return orderInfo.OrderStatus;
        //    //}

        //    if (OrderStatusHelper.checkOrderIsPaid(orderInfo))
        //    {
        //        return orderInfo.OrderStatus;
        //    }
        //    else
        //    {
        //        return OrderStatusHelper.checkOrderIsExpired(orderInfo.OrdersDate) ? (byte)Constants.OrderStatus.Expired : orderInfo.OrderStatus;
        //    }
        //}

        private string GetPayStatus(byte ispaid, Dictionary<byte, KeyValueType> payStatus, bool isChinese, byte payment, Dictionary<byte, KeyValueType> paymentStatus)
        {
            if (payment == Constants.Payment.PayOnDelivery)
            {
                return GetNameForByte(payment, paymentStatus, isChinese);
            }
            else
            {
                return GetNameForByte(ispaid, payStatus, isChinese);
            }
        }

        private string GetStatusString(byte payStatus, byte orderStatus,
                Dictionary<byte, KeyValueType> payStatusDict, Dictionary<byte, KeyValueType> orderStatusDict,
                byte payment, Dictionary<byte, KeyValueType> paymentDict, bool isChinese)
        {
            if (payStatus == Constants.QueryOrderStatus.Paid || payment == Constants.Payment.PayOnDelivery || orderStatus == Constants.OrderStatus.Expired)
            {
                if (orderStatusDict.ContainsKey(orderStatus))
                {
                    var value = orderStatusDict[orderStatus];
                    return (isChinese ? value.Name : value.ENName);
                }
            }
            else
            {

                if (payStatusDict.ContainsKey(payStatus))
                {
                    var value = payStatusDict[payStatus];
                    return (isChinese ? value.Name : value.ENName);
                }
            }
            return string.Empty;
        }


        [MemberAuthorize(AllowImpersonate = true)]
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public JsonResult LoadOrderById(int orderId, string language)
        {
            var orderInfo = OrdersBLL.LoadOrderById(orderId);
            if (orderInfo == null || orderInfo.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedGetResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid order id");
            }
            var payStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayStatus);
            var orderStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderStatus);
            var orderItemStatus = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadOrderItemStatus);
            var deliverTimeDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadDeliverTime);
            var paymentDict = SharedBLL.LoadKeyValueTypes(Constants.CacheKey.LoadPayment);
            var cityDict = SharedBLL.LoadAllCityArea();
            var expressCompanyList = SharedBLL.LoadAllExpressCompany();
            var orderStatusCode = SharedBLL.LoadAllOrderStatusCode();
            var subOrderStatusCode = SharedBLL.LoadAllSubOrderStatusCode();

            bool isChinese = IsChinese(language);

            //TODO: LoadOrderById
            var orderItem = new
            {
                OrderID = orderInfo.OrdersID,
                TotalPrice = orderInfo.TotalPrice,
                DeliverFee = orderInfo.DeliverFee,
                //PaymentId = orderInfo.Payment,
                //Payment = GetNameForByte(orderInfo.Payment, paymentDict, isChinese),
                //IsPaid = orderInfo.IsPaid == Constants.QueryOrderStatus.Paid || orderInfo.Payment == Constants.Payment.PayOnDelivery,
                //PayStatus = GetPayStatus(orderInfo.IsPaid, payStatus, isChinese, orderInfo.Payment, paymentDict),
                OrderDate = orderInfo.OrdersDate.ToDisplayString(),
                //OrderPayDate = orderInfo.OrderPayDate.ToDisplayString(),
                //OrderStatusID = ResetOrderStatus(orderInfo),
                OrderStatusCode = orderInfo.StatusCode,
                OrderStatusCodeString = OrderStatusHelper.GetStatusCodeString(orderInfo.StatusCode, orderStatusCode, isChinese),
                //OrderStatus = GetStatusString(orderInfo.IsPaid, ResetOrderStatus(orderInfo), payStatus, orderStatus, orderInfo.Payment, paymentDict, isChinese),
                PreferentialPrice = orderInfo.PreferentialPrice,
                PreferentialLogisticsFee = orderInfo.PreferentialLogisticsFee,
                OrderSourceId = orderInfo.OrderSourceId,
                OrderSourceCode = SharedBLL.GetOrderSourceCode(orderInfo.OrderSourceId),
                DeliverList = orderInfo.DeliverList.OrderBy(item => item.DeliverDate).Select(item => new
                {
                    Id = item.OrdersMID,
                    OrderId = item.OrderId,
                    DeliverDate = item.DeliverDate.ToDisplayString(),
                    Mobile = item.Mobile,
                    Name = item.OrdersMUser,
                    Address = BuildAddress(item, cityDict, isChinese),
                    CityId = item.OrdersCity,
                    AreaId = item.OrdersArea,
                    //StatusId = item.OrderStatusID,
                    //Status = GetNameForByte(item.OrderStatusID, orderItemStatus, isChinese),
                    SubStatusCode = item.SubStatusCode,
                    SubStatusCodeString = OrderStatusHelper.GetStatusCodeString(item.SubStatusCode, subOrderStatusCode, isChinese),
                    IsSelfPickUp = item.IsSelfPickUp,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    DeliverTimeId = item.DeliverTimeId,
                    DeliverTime = GetNameForByte((byte)item.DeliverTimeId, deliverTimeDict, isChinese),
                    SubPrice = item.ProductList.Sum(productItem => productItem.TotalPrice),
                    ExpressCompanyId = item.ExpressCompany,
                    ExpressCompanyName = GetExpressCompanyName(expressCompanyList, item.ExpressCompany),
                    TemplateType = item.TemplateType,
                    DeliveryNo = item.DeliveryNo,
                    DeliveryRemark = item.Remark,
                    StoreInfo = BuildStoreInfo(item.StoreId, isChinese),
                    ProductList = item.ProductList.Select(productItem => new
                    {
                        Id = productItem.OrdersMXID,
                        ProductId = productItem.ProductId,
                        ProductName = isChinese ? productItem.ProductName : productItem.ENProductName,
                        Count = productItem.Count,
                        TotalPrice = productItem.TotalPrice,
                        ImageUrl = productItem.ImageUrl,
                    }),
                }),
                InvoiceInfo = new
                {
                    Title = orderInfo.InvoiceInfo.Title,
                    Amount = orderInfo.InvoiceInfo.Amount,
                    InvoiceType = orderInfo.InvoiceInfo.InvoiceType,
                    TypeName = GetInvoiceTypeName(orderInfo.InvoiceInfo.InvoiceType, isChinese),
                },
                OrderPayments = OrderStatusHelper.OrderPaymentList(orderInfo.OrderPayments, paymentDict, isChinese),
            };
            return Json(new { IsSuccess = true, Order = orderItem }, JsonRequestBehavior.AllowGet);
        }


        private string GetExpressCompanyName(List<ExpressCompanyInfo> expressCompanyList, int id)
        {
            var company = expressCompanyList.FirstOrDefault(item => item.Id == id);
            return (company == null ? string.Empty : company.Name);
        }

        private string GetInvoiceTypeName(byte typeId, bool isChinese)
        {
            var invoiceItem = SharedBLL.LoadAllInvoiceTypes().FirstOrDefault(item => item.Id == typeId);
            if (invoiceItem == null)
            {
                return string.Empty;
            }
            else
            {
                return isChinese ? invoiceItem.Name : invoiceItem.ENName;
            }
        }

        private string BuildAddress(OrderMDL.OrderDeliverItemInfo item, Dictionary<int, IntegeKeyValueType> cityDict, bool isChinese)
        {
            string cityName = GetNameForInteger(cityDict, item.OrdersCity, isChinese);
            string areaName = GetNameForInteger(cityDict, item.OrdersArea, isChinese);
            string address = item.OrdersMAddress;
            return string.Format("{0} {1} {2}", cityName, areaName, address);
        }

        private string GetNameForByte(byte id, Dictionary<byte, KeyValueType> deliverTimeDict, bool isChinese)
        {
            if (deliverTimeDict.ContainsKey(id))
            {
                return isChinese ? deliverTimeDict[id].Name : deliverTimeDict[id].ENName;
            }
            return string.Empty;
        }


        private string GetNameForInteger(Dictionary<int, IntegeKeyValueType> dict, int id, bool isChinese)
        {
            if (dict.ContainsKey(id))
            {
                return isChinese ? dict[id].Name : dict[id].ENName;
            }
            return string.Empty;
        }

        private string BuildStoreAddress(int storeId, bool isChiness)
        {
            if (storeId == 0)
            {
                return string.Empty;
            }
            var storeList = SharedBLL.LoadAllStores();
            var storeItem = storeList.FirstOrDefault(item => int.Equals(item.StoreId, storeId));
            return isChiness ? storeItem.StoreAddress : storeItem.StoreEnAddress;
        }

        private object BuildStoreInfo(int storeId, bool isChinese)
        {
            if (storeId == 0)
            {
                return null;
            }
            var storeList = SharedBLL.LoadAllStores();
            var storeItem = storeList.FirstOrDefault(item => int.Equals(item.StoreId, storeId));
            return new
            {
                StoreId = storeItem.StoreId,
                StoreName = isChinese ? storeItem.StoreName : storeItem.StoreEnName,
                StoreCityName = isChinese ? storeItem.StoreCityName : storeItem.StoreCityEnName,
                StoreAddress = isChinese ? storeItem.StoreAddress : storeItem.StoreEnAddress,
                StoreBusinessHours = isChinese ? storeItem.StoreBusinessHours : storeItem.StoreEnBusinessHours,
                StoreTel = storeItem.StoreTel,
                StoreMapUrl = storeItem.StoreMapUrl,
            }; ;
        }
        #endregion



        #region <<修改订单时  验证数据>>
        /// <summary>
        /// 修改订单时的验证数据
        /// </summary>
        private ActionResult ValidUpdateOrdersmReceiverParams(UpdateOrdersmReceiverParams param)
        {
            if (param.OrderId.Equals(0) || param.SubOrderId.Equals(0) || param.ReceiverId.Equals(0) || param.DeliveryTimeId.Equals(0))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.OrderItemEmpty, "Empty object");
            }

            var orderInfo = OrdersBLL.LoadOrderById(param.OrderId);
            if (orderInfo == null || orderInfo.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid order id");
            }

            var subOrderInfo = orderInfo.DeliverList.Find(p => p.OrdersMID.Equals(param.SubOrderId));
            if (subOrderInfo == null || subOrderInfo.IsSelfPickUp.Equals(true) || subOrderInfo.OrdersArea.Equals(0))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid suborder id");
            }

            var receiverInfo = OrdersBLL.getReceiverInfoById(CurrentMember_id, param.ReceiverId);
            if (receiverInfo == null || receiverInfo.AreaId.Equals(0))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.InvalidOrderOwner, "Invalid receiverId");
            }

            bool checkArea = OrdersBLL.CheckAreaIsActive(receiverInfo.AreaId);
            if (checkArea.Equals(false))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.AreaIsNotActive, "The area is not available");
            }
            bool checkFee = OrdersBLL.CheckFreightIncreased(subOrderInfo.OrdersArea, receiverInfo.AreaId);
            if (checkFee.Equals(true))
            {
                return CreateFailedPostJsonResult(Constants.CreateOrderStatus.UpdateReceiverError, "Increased freight");
            }

            return null;
        }
        #endregion

        #region <<编辑产品信息>>
        /// <summary>
        /// 创建产品信息
        /// </summary>
        private List<OrderMDL.OrderProductInfo> CreateProductInfo(List<OrderProduct> orderedProducts, int packageRuleId, int packageRuleCount)
        {
            List<OrderProduct> packageProducts = new List<OrderProduct>();

            if (orderedProducts == null)
            {
                orderedProducts = new List<OrderProduct>();
            }
            if (packageRuleId.Equals(0))
            {
                packageProducts = new List<OrderProduct>();
            }
            else
            {
                var rules = PackageBLL.LoadAllPackages()
                    .SelectMany(p => p.PackageGroups)
                    .SelectMany(p => p.DayRules)
                    .SelectMany(p => p.GroupRules);
                var rule = rules.Where(p => packageRuleId.Equals(p.ID)).FirstOrDefault();
                if (rule != null && packageRuleCount > 0)
                {
                    var ruleDetail = rule.RuleDetail;

                    if (ruleDetail != null)
                    {
                        ruleDetail.ForEach(
                            p => packageProducts.Add(
                                    new OrderProduct()
                                    {
                                        Id = p.ProductID,
                                        Count = p.Count * packageRuleCount
                                    }
                                                      )
                                          );
                    }
                }
            }

            var concatRange = orderedProducts.Concat(packageProducts);

            var productList = PackageBLL.LoadAllProducts();
            var products = concatRange.Join(
                    productList,
                    orderProduct => orderProduct.Id,
                    productInfo => productInfo.ProductsID,
                    (orderProduct, productInfo) => CreateOrderedProductInfo(orderProduct, productInfo)
                );
            return products.Where(item => item != null).ToList();
        }

        /// <summary>
        /// 创建订单产品信息
        /// </summary>
        private OrderMDL.OrderProductInfo CreateOrderedProductInfo(OrderProduct orderedProduct, VCleanse.Shop.Model.Packages.ProductInfo productInfo)
        {
            if (orderedProduct.Count <= productInfo.ProductsMaxN)
            {
                return new OrderMDL.OrderProductInfo
                {
                    Id = productInfo.ProductsID,
                    Count = orderedProduct.Count,
                    UnitPrice = productInfo.ProductsPrice,
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region <<判断订单日期是否符合要求>>
        /// <summary>
        /// 判断主订单时间是否符合要求
        /// </summary>
        /// <param name="deliverList"></param>
        /// <returns></returns>
        private bool CheckOrdersmDate(List<DeliverItem> deliverList)
        {
            foreach (var item in deliverList)
            {
                if (!OrderDateIsLegal(DateTime.Now, item.Date))
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// 判断子订单日期是否符合要求
        /// </summary>

        private bool OrderDateIsLegal(DateTime orderDate, DateTime deliverDate)
        {
            try
            {
                if (int.Parse(orderDate.ToString(WebConstants.OrderDateCheckKey.BaseKey)) >= WebConstants.OrderDateCheckKey.BaseKeyValue)
                {
                    return deliverDate.Date >= orderDate.Date.AddDays(WebConstants.OrderDateCheckKey.DayIntervaltoAfterBaseKey);
                }
                else
                {
                    return deliverDate.Date >= orderDate.Date.AddDays(WebConstants.OrderDateCheckKey.DayIntervaltoBeforeBaseKey);
                }
            }
            catch (Exception ex)
            {
                ExceptionPublishHelper.PublishFormat(ex, "OrderDateIsLegal");
                return true;
            }

        }
        #endregion

        #region <<创建订单的收获地址信息>>
        /// <summary>
        /// 创建订单的收获地址信息
        /// </summary>
        private ReceiverInfo CreateReceiverInfo(DeliverItem deliverItem, List<ReceiverInfo> receivers)
        {
            if (deliverItem.IsSelfpickup)
            {
                if (deliverItem.SelfpickupInfo != null)
                {
                    ReceiverInfo receiver = new ReceiverInfo()
                    {
                        StoreId = deliverItem.SelfpickupInfo.StoreId,
                        FirstName = deliverItem.SelfpickupInfo.FirstName,
                        LastName = string.Empty,
                        Mobile = deliverItem.SelfpickupInfo.Mobile
                    };
                    return receiver;
                }
                else
                {
                    return new ReceiverInfo();
                }
            }
            else
            {
                if (deliverItem.ReceiverId > 0)
                {
                    return receivers.FirstOrDefault(item => item.Id == deliverItem.ReceiverId);
                }
                else if (deliverItem.Contact != null)
                {
                    ReceiverInfo receiver = new ReceiverInfo()
                    {
                        FirstName = deliverItem.Contact.FirstName,
                        LastName = string.Empty,
                        Mobile = deliverItem.Contact.Mobile,
                        ZipCode = deliverItem.Contact.ZipCode,
                        CityId = deliverItem.Contact.CityId,
                        AreaId = deliverItem.Contact.AreaId,
                        Line1 = deliverItem.Contact.Line1,
                        Line2 = string.Empty,
                    };
                    return receiver;
                }
                else
                {
                    return new ReceiverInfo();
                }
            }
        }

        #endregion

        #region <<计算订单总价格>>
        /// <summary>
        /// 计算订单总价格
        /// </summary>

        private decimal AccountTotalPrice(OrderMDL.OrderInfo orderInfo)
        {
            if (orderInfo.DeliverList.Count == 0)
                return 0;
            else
            {
                var rules = PackageBLL.LoadAllPackages().SelectMany(p => p.PackageGroups).SelectMany(p => p.DayRules).SelectMany(p => p.GroupRules);
                var outOfPackageProducts = orderInfo.DeliverList.SelectMany(p => p.outOfPackageProducts);
                return (from deliver in orderInfo.DeliverList
                        from rule in rules
                        where deliver.PackageRuleId == rule.ID
                        select rule.PackagePrice * deliver.PackageRuleCount).Sum() + outOfPackageProducts.Sum(p => p.TotalPrice);
            }
        }
        #endregion

        #region <<验证手机、Session>>
        private JsonResult ValidateMobileInfo(string mobile, string clientCode)
        {
            if (string.IsNullOrWhiteSpace(mobile) || !ValidationUtility.IsCellPhone(mobile))
            {
                return CreateFailedGetResult(Constants.GeneralStatus.InvalidParameter, "mobile:{0}", mobile);
            }

            if (string.IsNullOrEmpty(clientCode))
            {
                return null;
            }
            ValidationCodeInfo validationCode = GetValidationSession(mobile);
            if (validationCode == null
                || !string.Equals(validationCode.Code, clientCode, StringComparison.OrdinalIgnoreCase)
                || validationCode.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
            {
                return CreateFailedGetResult(Constants.RegistStatus.InvalidMobileCodeNum, "Validation code error");
            }
            else
            {
                return null;
            }
        }
        protected ValidationCodeInfo GetValidationSession(string phone)
        {
            ValidationCodeInfo cellphone = Session[GetCellPhoneValidationSessionKey(phone)] as ValidationCodeInfo;
            return cellphone;
        }
        protected string GetCellPhoneValidationSessionKey(string phone)
        {
            return string.Format("Cell_{0}", phone);
        }
        #endregion


        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public JsonResult SaveInvoice(OrderMDL.OrderItemInfo orderInfo, OrderMDL.InvoiceInfo invoiceInfo)
        {
            if (invoiceInfo == null)
            {
                invoiceInfo = new OrderMDL.InvoiceInfo()
                {
                    InvoiceType = 0,
                    OrderId = orderInfo.OrdersID,
                };
            }
            invoiceInfo.Amount = orderInfo.PreferentialPrice;
            int invoiceId = OrdersBLL.SaveInvoice(invoiceInfo);
            return Json(new { IsSuccess = true, InvoiceId = invoiceId });
        }

        /// <summary>
        /// 保存支付信息 并发送短信通知
        /// </summary>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public JsonResult SavePayment(OrderMDL.OrderItemInfo orderInfo, OrderMDL.PaymentInfo paymentInfo)
        {
            paymentInfo.FactPayment = orderInfo.PreferentialLogisticsFee + orderInfo.PreferentialPrice;
            OrdersBLL.SavePayment(paymentInfo);
            string smsMessage = string.Empty;
            var SMSList = SMSService.LoadAllSMSInfo();
            SMSInfo smsInfo = new SMSInfo();
            
          
            if (LanguageHelper.IsChinese(paymentInfo.Language))
            {
                if (paymentInfo.Payment == Constants.Payment.PayOnDelivery)
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 1);
                    if (smsInfo != null && smsInfo.CN_Content != null && smsInfo.CN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.CN_Content;
                    }
                    else {
                        smsMessage = "您在维果清的订单{0}已经提交成功，总金额{1}元（货到付款）。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！";
                    }
                  
                }
                else
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 2);
                    if (smsInfo != null && smsInfo.CN_Content != null && smsInfo.CN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.CN_Content;
                    }
                    else
                    {
                        smsMessage = "您在维果清的订单{0}已经提交成功，总金额{1}元，请尽快完成在线支付。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！";
                    }
                }
                //smsMessage = paymentInfo.Payment == Constants.Payment.PayOnDelivery ?
                //"您在维果清的订单{0}已经提交成功，总金额{1}元（货到付款）。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！点击http://t.cn/RqaOfd2 识别二维码或微信公众服务号回复：维粉，加入本周小维粉丝群，获得更多营养知识，一起更加健康。" :
                //"您在维果清的订单{0}已经提交成功，总金额{1}元，请尽快完成在线支付。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！点击http://t.cn/RqaOfd2 识别二维码或微信公众服务号回复：维粉，加入本周小维粉丝群，获得更多营养知识，一起更加健康。";
            }
            else
            {
                if (paymentInfo.Payment == Constants.Payment.PayOnDelivery)
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 1);
                    if (smsInfo != null && smsInfo.EN_Content != null && smsInfo.EN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.EN_Content;
                    }
                    else
                    {
                        smsMessage = "Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1} (Payment on Delivery). Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!";
                    }
                }
                else
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 2);
                    if (smsInfo != null && smsInfo.EN_Content != null && smsInfo.EN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.EN_Content;
                    }
                    else
                    {
                        smsMessage = "Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1}, please complete online payment ASAP. Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!";
                    }
                }
                //smsMessage = paymentInfo.Payment == Constants.Payment.PayOnDelivery ?
                //"Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1} (Payment on Delivery). Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!http://t.cn/RqaOfd2" :
                //"Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1}, please complete online payment ASAP. Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!http://t.cn/RqaOfd2";
            }

            string mobile = string.Empty;
            if (CurrentContext.Member.Id == Config.DefaultCustomerId)
            {
                try
                {
                    mobile = orderInfo.DeliverList.FirstOrDefault().Mobile.ToString();
                }
                catch
                {
                    mobile = string.Empty;
                }
            }
            else
            {
                mobile = VcleanseContext.Current.Member.Mobile;

            }

            SMSService.Instance.SendSMS(mobile
                , string.Format(smsMessage, orderInfo.OrdersID, orderInfo.PreferentialLogisticsFee + orderInfo.PreferentialPrice));
            return Json(new { IsSuccess = true, });
        }

        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public JsonResult SaveGiftInfo(OrderMDL.OrderItemInfo orderInfo, OrderMDL.GiftInfo giftInfo)
        {
            giftInfo.OrderId = orderInfo.OrdersID;
            OrdersBLL.SaveGiftInfo(giftInfo);
            return Json(new { IsSuccess = true });
        }

        

        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public JsonResult ApplyPromotion(PromotionCode promotionCode)
        {
            return HandlePromotion(promotionCode, false);
        }


        private JsonResult HandlePromotion(PromotionCode promotionCode, bool confirmFlag)
        {
            // validate step 1
            JsonResult _validatePromotionCode = ValidatePromotionCode(promotionCode);
            if (_validatePromotionCode != null)
            {
                NLogHelper.LogPromotionInfo(confirmFlag, 1, promotionCode, _validatePromotionCode.Data);
                return _validatePromotionCode;
            }

            //获取这条优惠券的信息
            PromotionRuleInfo promotionRule = PromotionBLL.LoadAllPromotionRulesByCode(promotionCode.Code);


            // validate step 2
            JsonResult _validatePromotionRule = ValidatePromotionRule(promotionRule);
            if (_validatePromotionRule != null)
            {
                NLogHelper.LogPromotionInfo(confirmFlag, 2, promotionCode, _validatePromotionRule.Data);
                return _validatePromotionRule;
            }

            var orderInfo = OrdersBLL.LoadOrderById(promotionCode.OrderId);

            // validate step 3
            JsonResult _validateOrderInfo = ValidateOrderInfo(orderInfo, promotionRule);
            if (_validateOrderInfo != null)
            {
                NLogHelper.LogPromotionInfo(confirmFlag, 3, promotionCode, _validateOrderInfo.Data);
                return _validateOrderInfo;
            }

            // validate step 3.5
            JsonResult _validateSourceToPromotionCode = ValidateSourceToPromotionCode(orderInfo, promotionRule);
            if (_validateSourceToPromotionCode != null)
            {
                NLogHelper.LogPromotionInfo(confirmFlag, 35, promotionCode, _validateSourceToPromotionCode.Data);
                return _validateSourceToPromotionCode;
            }

            // validate step 4   check order has used rule then reflesh
            if (confirmFlag)
            {
                JsonResult _validateOldPromotionRule = ValidateOldPromotionRule(orderInfo);
                if (_validateOldPromotionRule != null)
                {
                    NLogHelper.LogPromotionInfo(confirmFlag, 4, promotionCode, _validateOldPromotionRule.Data);
                    return _validateOldPromotionRule;
                }
            }

            // validate step 5
            JsonResult _validatePromotionType = ValidatePromotionType(confirmFlag, promotionRule, orderInfo);
            if (_validatePromotionType != null)
            {
                NLogHelper.LogPromotionInfo(confirmFlag, 5, promotionCode, _validatePromotionType.Data);
                return _validatePromotionType;
            }

            OrderPreferentialInfo orderPreferentialInfo = new OrderPreferentialInfo();
            //  start step 6
            StartPromotionRule(confirmFlag, ref orderPreferentialInfo, promotionRule, orderInfo);

            NLogHelper.LogPromotionInfo(confirmFlag, promotionCode);

            return Json(new
            {
                RuleId = promotionRule.RuleId,
                IsSuccess = true,
                Order = new
                {
                    OrderId = orderPreferentialInfo.OrderId,
                    TotalPrice = orderPreferentialInfo.OriginalPrice,
                    DeliverFee = orderPreferentialInfo.OriginalDeliveryFee,
                    PreferentialPrice = orderPreferentialInfo.PreferentialPrice,
                    PreferentialLogisticsFee = orderPreferentialInfo.PreferentialDeliveryFee,
                }
            }
                        );
        }


        // validate step 1
        private JsonResult ValidatePromotionCode(PromotionCode promotionCode)
        {
            if (string.IsNullOrEmpty(promotionCode.Code))
            {
                return CreateFailedPostJsonResult(Constants.Promotion.InvalidPromotionCode, "Invalid promotion code");
            }
            if (promotionCode.OrderId == 0)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.InvalidOrderId, "Invalid order id");
            }
            return null;
        }
        // 验证这个优惠券是否在有效期内
        private JsonResult ValidatePromotionRule(PromotionRuleInfo promotionRule)
        {
            DateTime now = DateTime.Now;
            if (promotionRule == null)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.NotExists, "Not Exists promotion code");
            }
            if (promotionRule.ActiveFrom > now)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.NotStart, "Not Start promotion code");
            }
            if (promotionRule.ActiveTo < now)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.ExpiredCode, "Expired promotion code");
            }
            if (!promotionRule.IsActive)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.HasUsed, "Promotion code has used.");
            }

            return null;
        }
        // validate step 3
        private JsonResult ValidateOrderInfo(OrderMDL.OrderItemInfo orderInfo, PromotionRuleInfo promotionRule)
        {
            if (orderInfo == null || orderInfo.OrdersVUserID != CurrentMember_id)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.InvalidOrderId, "Invalid order id");
            }
            if (orderInfo.TotalPrice < promotionRule.StartAmount)
            {
                return CreateFailedPostJsonResult(Constants.Promotion.InvalidStartAmount, "Invalid start amount");
            }
            return null;
        }
        // validate step 3.5
        private JsonResult ValidateSourceToPromotionCode(OrderMDL.OrderItemInfo orderInfo, PromotionRuleInfo promotionRule)
        {
            if (promotionRule.Code.Equals(Constants.Uber.CODE, StringComparison.OrdinalIgnoreCase))
            {
                if (!orderInfo.OrderSourceId.Equals(Constants.OrderSource.WarmingDay))
                {
                    return CreateFailedPostJsonResult(Constants.Promotion.UberOnly, "Uber only");
                }
            }

            return null;
        }
        // validate step 4
        private JsonResult ValidateOldPromotionRule(OrderMDL.OrderItemInfo orderInfo)
        {
            if (orderInfo.PromotionRuleId > 0)
            {
                var OldpromotionRule = PromotionBLL.LoadAllPromotionRulesById(orderInfo.PromotionRuleId);

                if (OldpromotionRule != null)
                {
                    // 0730 prevent client fight
                    if (OldpromotionRule.PromotionTypeId != Constants.PromotionType.Recyclable)
                    {
                        return CreateFailedPostJsonResult(Constants.Promotion.HasDisposableRuleIsIllegal, "Has DisposableRule this is illegal");
                    }

                    //clear old promotion
                    switch (OldpromotionRule.RuleId)
                    {
                        case Constants.PromotionRuleType.Deduction:
                            break;
                        case Constants.PromotionRuleType.Percentage:
                            break;
                        case Constants.PromotionRuleType.DeliveryFree:
                            break;
                        case Constants.PromotionRuleType.AddOneCoconut:
                        case Constants.PromotionRuleType.DeliveryFreeAddOneCoconut:
                        case Constants.PromotionRuleType.FirstDayAddOneCocunut:
                            PromotionBLL.DeliverListClearOneCoconut(orderInfo.DeliverList);
                            break;
                        case Constants.PromotionRuleType.AddOneLemonade:
                        case Constants.PromotionRuleType.DeliveryFreeAddOneLemonade:
                        case Constants.PromotionRuleType.FirstDayAddOneLemonade:
                            PromotionBLL.DeliverListClearOneLemonade(orderInfo.DeliverList);
                            break;
                        case Constants.PromotionRuleType.FirstDayAddOneMoonbear:
                            PromotionBLL.DeliverListClearOneMoonbear(orderInfo.DeliverList);
                            break;
                        default:
                            break;
                    }

                    NLogHelper.LogPromotionInfo(true, orderInfo.OrdersID, orderInfo.PromotionRuleId, OldpromotionRule.RuleId);
                }
            }
            return null;
        }
        // validate step 5
        private JsonResult ValidatePromotionType(bool confirmFlag, PromotionRuleInfo promotionRule, OrderMDL.OrderItemInfo orderInfo)
        {
            if (promotionRule.PromotionTypeId == Constants.PromotionType.Recyclable)
            {
            }
            else if (promotionRule.PromotionTypeId == Constants.PromotionType.Disposable)
            {
                if (confirmFlag)
                {
                    PromotionBLL.UpdateDisposablePromotionRule(promotionRule.id, orderInfo.OrdersVUserID);
                }
            }
            else if (promotionRule.PromotionTypeId == Constants.PromotionType.UniquenessPerCustomer)
            {
                if (orderInfo.OrdersVUserID < 1 || orderInfo.OrdersVUserID == Config.DefaultCustomerId)
                {
                    return CreateFailedPostJsonResult(Constants.Promotion.AnonymousCannotUseThisRule, "Anonymous Cannot Use this Rule");
                }
                if (PromotionBLL.CheckPromotionCustomerHasUsedDisposableRule(orderInfo.OrdersVUserID, Constants.PromotionType.UniquenessPerCustomer))
                {
                    return CreateFailedPostJsonResult(Constants.Promotion.UserUseOnce, "You have used this type of promotion");
                }
                if (confirmFlag)
                {
                    PromotionBLL.UpdateDisposablePromotionRule(promotionRule.id, orderInfo.OrdersVUserID);
                }
            }
            else if (promotionRule.PromotionTypeId == Constants.PromotionType.IsFirstOrder)
            {
                Boolean isFirstOrder = OrdersBLL.IsFirstOrder(CurrentContext.Member_id);
                if (!isFirstOrder)
                {
                    return CreateFailedPostJsonResult(Constants.Promotion.NotFirstOrder, "You have ordered before.");
                }
            }
            return null;
        }
        // step 6
        private void StartPromotionRule(bool confirmFlag, ref OrderPreferentialInfo orderPreferential, PromotionRuleInfo promotionRule, OrderMDL.OrderItemInfo orderInfo)
        {
            decimal preferentialPrice = orderInfo.TotalPrice;
            decimal deliverPrice = orderInfo.DeliverFee;

            switch (promotionRule.RuleId)
            {
                case Constants.PromotionRuleType.Deduction:
                    preferentialPrice -= promotionRule.RuleValue;
                    break;
                case Constants.PromotionRuleType.Percentage:
                    preferentialPrice = preferentialPrice * promotionRule.RuleValue / 100;
                    break;
                case Constants.PromotionRuleType.DeliveryFree:
                    deliverPrice = 0;
                    break;
                case Constants.PromotionRuleType.AddOneCoconut:
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneCoconut(orderInfo.DeliverList, true);
                    }
                    break;
                case Constants.PromotionRuleType.DeliveryFreeAddOneCoconut:
                    deliverPrice = 0;
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneCoconut(orderInfo.DeliverList, true);
                    }
                    break;
                case Constants.PromotionRuleType.AddOneLemonade:
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneLemonade(orderInfo.DeliverList, true);
                    }
                    break;
                case Constants.PromotionRuleType.DeliveryFreeAddOneLemonade:
                    deliverPrice = 0;
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneLemonade(orderInfo.DeliverList, true);
                    }
                    break;
                case Constants.PromotionRuleType.FirstDayAddOneCocunut:
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneCoconut(orderInfo.DeliverList, false);
                    }
                    break;
                case Constants.PromotionRuleType.FirstDayAddOneLemonade:
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneLemonade(orderInfo.DeliverList, false);
                    }
                    break;
                case Constants.PromotionRuleType.FirstDayAddOneMoonbear:
                    if (confirmFlag)
                    {
                        PromotionBLL.DeliverListAddOneMoonbear(orderInfo.DeliverList, false);
                    }
                    break;
                default: break;
            }

            if (preferentialPrice <= 0)
            {
                preferentialPrice = 0;
            }


            orderPreferential = new OrderPreferentialInfo()
               {
                   OrderId = orderInfo.OrdersID,
                   OriginalPrice = orderInfo.TotalPrice,
                   OriginalDeliveryFee = orderInfo.DeliverFee,
                   PreferentialDeliveryFee = deliverPrice,
                   PreferentialPrice = preferentialPrice,
                   PromotionRuleId = promotionRule.id,
               };
            if (confirmFlag)
            {
                PromotionBLL.UpdatePreferentialPrice(orderPreferential);
            }

        }




        [HttpGet]
        public ActionResult Ex()
        {
            throw new Exception("abc");
         
        }

        /// <summary>
        /// 地球日订单
        /// </summary>
        [HttpPost]
        public ActionResult CreateEarthDayOrder(VCleanse.Shop.Model.Orders.CreateEventOrderParam param)
        {
            try
            {
                var products = Config.EarthDayProducts.Split(',').ToList().ConvertAll<int>(idString => int.Parse(idString));
                var deliveryDate = new DateTime(2014, 4, 22);
                var deliverTimeId = 1;
                var orderInfo = new VCleanse.Shop.Model.Orders.EventOrderInfo()
                {
                    DeliveryDate = deliveryDate,
                    DeliverTimeId = deliverTimeId,
                    OrderPrice = 300,
                    LogisticsFee = 0,
                    FirstName = param.FirstName,
                    LastName = param.LastName,
                    Mobile = param.Mobile,
                    CityId = param.CityId,
                    AreaId = param.AreaId,
                    Address = param.Address1 + param.Address2,
                    ZipCode = param.ZipCode,
                    PayMethod = param.PayMethod,
                    ProductIds = products
                };

                var orderId = OrdersBLL.CreateEventOrder(orderInfo);

                bool isChinese = LanguageHelper.IsChinese(param.Language);

                if (orderId == -1)
                {
                    return CreateFailedPostJsonResult(-1, "Already reached max order number.");
                }

                SendEventOrderSMS(orderInfo, orderId, isChinese);
                return Json(new { IsSuccess = true, OrderId = orderId });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = true, ErrorMessage = ex.Message });
            }
        }

        /// <summary>
        /// 活动订单
        /// </summary>
        [HttpPost]
        public ActionResult CreateEventOrder(VCleanse.Shop.Model.Orders.CreateEventOrderParam param)
        {
            if (!VCleanse.Shop.Model.Orders.EventOrderConfig.AllEvents.ContainsKey(param.EventCode))
            {
                return CreateFailedPostJsonResult(-1, "No Matching Event Id");
            }

            var eventConfig = VCleanse.Shop.Model.Orders.EventOrderConfig.AllEvents[param.EventCode];
            if (DateTime.Now < eventConfig.StartDate)
            {
                return CreateFailedPostJsonResult(-2, "Event has not started");
            }
            if (DateTime.Now > eventConfig.EndDate)
            {
                return CreateFailedPostJsonResult(-3, "Event has already ended.");
            }

            try
            {
                var products = eventConfig.Products.Select(p => p.ProductId);
                var price = eventConfig.Products.Sum(p => p.Count * p.Price);
                var nextDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(2);
                var orderInfo = new VCleanse.Shop.Model.Orders.EventOrderInfo()
                {
                    DeliveryDate = eventConfig.DeliverDate == DateTime.MinValue ? nextDay : eventConfig.DeliverDate,
                    DeliverTimeId = eventConfig.DeliverTimeId,
                    OrderPrice = price,
                    LogisticsFee = eventConfig.LogisticsFee,
                    FirstName = param.FirstName,
                    LastName = param.LastName,
                    Mobile = param.Mobile,
                    CityId = param.CityId,
                    AreaId = param.AreaId,
                    Address = param.Address1 + param.Address2,
                    ZipCode = param.ZipCode,
                    PayMethod = param.PayMethod,
                    ProductIds = products,
                    EventId = eventConfig.EventId,
                    MaxOrderPerKitchen = eventConfig.MaxOrderPerKitchen
                };

                var orderId = OrdersBLL.CreateEventOrder(orderInfo);

                bool isChinese = LanguageHelper.IsChinese(param.Language);

                if (orderId == -9)
                {
                    return CreateFailedPostJsonResult(-9, "Already reached max order number.");
                }

                SendEventOrderSMS(orderInfo, orderId, isChinese);
                return Json(new { IsSuccess = true, OrderId = orderId });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = true, ErrorMessage = ex.Message });
            }
        }

        /// <summary>
        /// 发送活动短信
        /// </summary>
        private void SendEventOrderSMS(VCleanse.Shop.Model.Orders.EventOrderInfo orderInfo, int orderId, bool isChinese)
        {
            string smsMessage;
            var SMSList = SMSService.LoadAllSMSInfo();
            SMSInfo smsInfo = new SMSInfo();
            if (isChinese)
            {
                if (orderInfo.PayMethod == Constants.Payment.PayOnDelivery)
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 3);
                    if (smsInfo != null && smsInfo.CN_Content != null && smsInfo.CN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.CN_Content;
                    }
                    else
                    {
                        smsMessage = "您在维果清的订单{0}已经提交成功，总金额{1}元（货到付款）。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！";
                    }

                }
                else
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 4);
                    if (smsInfo != null && smsInfo.CN_Content != null && smsInfo.CN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.CN_Content;
                    }
                    else
                    {
                        smsMessage = "您在维果清的订单{0}已经提交成功，总金额{1}元，请尽快完成在线支付。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！";
                    }
                }

                //smsMessage = orderInfo.PayMethod == Constants.Payment.PayOnDelivery ?
                //"您在维果清的订单{0}已经提交成功，总金额{1}元（货到付款）。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！点击http://t.cn/RqaOfd2 识别二维码或微信公众服务号回复：维粉，加入本周小维粉丝群，获得更多营养知识，一起更加健康。" :
                //"您在维果清的订单{0}已经提交成功，总金额{1}元，请尽快完成在线支付。现在关注【维果清微信官方服务号】并且绑定您的手机，即可用微信轻松查询订单详情、配送信息，回复“人工客服”，随时随地语音咨询！点击http://t.cn/RqaOfd2 识别二维码或微信公众服务号回复：维粉，加入本周小维粉丝群，获得更多营养知识，一起更加健康。";
            }
            else
            {
                if (orderInfo.PayMethod == Constants.Payment.PayOnDelivery)
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 3);
                    if (smsInfo != null && smsInfo.EN_Content != null && smsInfo.EN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.EN_Content;
                    }
                    else
                    {
                        smsMessage = "Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1} (Payment on Delivery). Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!";
                    }
                }
                else
                {
                    smsInfo = SMSList.FirstOrDefault(p => p.Type == 4);
                    if (smsInfo != null && smsInfo.EN_Content != null && smsInfo.EN_Content != "" && smsInfo.IsValid)
                    {
                        smsMessage = smsInfo.EN_Content;
                    }
                    else
                    {
                        smsMessage = "Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1}, please complete online payment ASAP. Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!";
                    }
                }
                //smsMessage = orderInfo.PayMethod == Constants.Payment.PayOnDelivery ?
                //"Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1} (Payment on Delivery). Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!http://t.cn/RqaOfd2" :
                //"Congrats! Your VCLEANSE order {0} has been placed. The total amount is RMB{1}, please complete online payment ASAP. Please register with our Wechat service account (VCLEANSE) to track your order through the delivery process. Happy juicing!http://t.cn/RqaOfd2";
            }
           

            SMSService.Instance.SendSMS(orderInfo.Mobile
              , string.Format(smsMessage, orderId, orderInfo.OrderPrice + orderInfo.LogisticsFee));
        }




    }
}
