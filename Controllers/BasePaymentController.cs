﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;


using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.Payment;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Payment;

using VCleanse.Shop.Host.PaymentService;
using VCleanse.Shop.Host.PaymentService.Ucfpay;
using VCleanse.Shop.Host.PaymentService.Alipay;
using VCleanse.Shop.Host.PaymentService.AlipayWap;
using VCleanse.Shop.Host.PaymentService.Unionpay;

using ALIPAY = VCleanse.Shop.Host.PaymentService.Alipay.com;
using ALIWAPPAY = VCleanse.Shop.Host.PaymentService.AlipayWap.com;
using UCFPAY = VCleanse.Shop.Host.PaymentService.Ucfpay.com;
using UNIONPAY = VCleanse.Shop.Host.PaymentService.Unionpay.com;


namespace VCleanse.Shop.Host.Controllers
{
    public class BasePaymentController : BaseController
    {



        #region "RECEIVE"
        /// <summary>
        /// 1.A 支付宝普通异步回调
        /// </summary>
        /// <returns></returns>
        protected string AliPayNotify()
        {
            string content = string.Empty;//回调用
            string result = string.Empty;
            int orderId = 0;
            SortedDictionary<string, string> sPara = GetRequestPostSort();

            string originValue = PaymentHelper.DictToString(sPara);
            PaymentHelper.GetReturnLogStart("1.A 支付宝普通异步回调", originValue);


            if (sPara.Count > 0)//判断是否有带返回参数
            {
                ALIPAY.AliNotify aliNotify = new ALIPAY.AliNotify();
                bool verifyResult = aliNotify.Verify(sPara, Request.Form["notify_id"], Request.Form["sign"]);

                if (verifyResult)//验证成功
                {
                    //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
                    //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
                    string trade_no = Request.Form["trade_no"];         //支付宝交易号
                    string order_no = Request.Form["out_trade_no"];     //获取订单号
                    string total_fee = Request.Form["total_fee"];       //获取总金额
                    string subject = Request.Form["subject"];           //商品名称、订单名称
                    string body = Request.Form["body"];                 //商品描述、订单备注、描述
                    string buyer_email = Request.Form["buyer_email"];   //买家支付宝账号
                    string trade_status = Request.Form["trade_status"]; //交易状态
                    decimal fee = 0;
                    decimal.TryParse(total_fee, out fee);
                    orderId = PaymentHelper.ParseOrderNumber(order_no);
                    var orderInfo = PaymentHelper.getOrderInfo(orderId);

                    if (orderInfo != null)
                    {
                        if (PaymentHelper.getPaymentPrice(orderInfo).Equals(fee))
                        {
                            AliPayNotifyLogInfo log = new AliPayNotifyLogInfo()
                            {
                                Body = body,
                                BuyerEMail = buyer_email,
                                OrderId = orderId,
                                Subject = subject,
                                TotalFee = fee,
                                TradeNo = trade_no,
                                TradeStatus = trade_status,
                                PaymentPlatform = Constants.PaymentPlatform.ALIPAY,
                                OriginalValue = originValue
                            };

                            PaymentHelper.setPaidLog(log);

                            if (log.OrderId > 0 && (Request.Form["trade_status"] == "TRADE_FINISHED" || Request.Form["trade_status"] == "TRADE_SUCCESS"))
                            {
                                PaymentHelper.setPaidConfirm(log.OrderId, fee);
                                content = PaymentHelper.AliPayReturnTpye.SUCCESS;  //请不要修改或删除
                                result = "成功";
                            }
                            else
                            {
                                content = PaymentHelper.AliPayReturnTpye.FAIL;
                                result = "trade失败 R000158" + Request.Form["trade_status"];
                            }
                        }
                        else
                        {
                            content = PaymentHelper.AliPayReturnTpye.FAIL;
                            result = "与订单价格不对 R000131 DB=(" + PaymentHelper.getPaymentPrice(orderInfo).ToString() + " from=(" + fee.ToString() + ")";
                        }
                    }
                    else
                    {
                        content = PaymentHelper.AliPayReturnTpye.FAIL;
                        result = "没有订单信息 R000163" + orderId.ToString();
                    }
                }
                else//验证失败
                {
                    content = PaymentHelper.AliPayReturnTpye.FAIL;
                    result = "验证失败 R000169";
                }
            }
            else
            {
                content = "无通知参数";
                result = "无通知参数";
            }
            PaymentHelper.GetReturnLogEnd("1.A 支付宝普通异步回调", orderId, content, result);
            return content;
        }
        /// <summary>
        /// 1.B(1) 支付宝普通同步回调   /   1.B(2) 支付宝普通二维码同步回调
        /// </summary>
        /// <param name="orderId">页面跳转用</param>
        /// <param name="qrFlag"></param>
        /// <returns></returns>
        protected string AliPayReturn(out int orderId, out int errorId, bool qrFlag = false)
        {
            string content = string.Empty;//回调用
            string result = string.Empty;//log 用 
            orderId = 0;
            errorId = 0;
            SortedDictionary<string, string> sPara = GetRequestGetSort();
            string originValue = PaymentHelper.DictToString(sPara);

            if (qrFlag)
            {
                PaymentHelper.GetReturnLogStart("1.B(2) 支付宝普通二维码同步回调", originValue);
            }
            else
            {
                PaymentHelper.GetReturnLogStart("1.B(1) 支付宝普通同步回调", originValue);
            }

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                ALIPAY.AliNotify aliNotify = new ALIPAY.AliNotify();
                bool verifyResult = aliNotify.Verify(sPara, Request.QueryString["notify_id"], Request.QueryString["sign"]);

                if (verifyResult)//验证成功
                {
                    string out_trade_no = Request.QueryString["out_trade_no"];//订单号
                    string trade_no = Request.QueryString["trade_no"];//支付宝交易号
                    string trade_status = Request.QueryString["trade_status"]; //交易状态
                    string total_fee = Request.QueryString["total_fee"]; //
                    decimal fee = 0;
                    decimal.TryParse(total_fee, out fee);
                    orderId = PaymentHelper.ParseOrderNumber(out_trade_no);
                    var orderInfo = PaymentHelper.getOrderInfo(orderId);
                    if (orderInfo != null)
                    {
                        if (PaymentHelper.getPaymentPrice(orderInfo).Equals(fee))
                        {
                            if (Request.QueryString["trade_status"] == "TRADE_FINISHED" || Request.QueryString["trade_status"] == "TRADE_SUCCESS")
                            {
                                content = PaymentHelper.AliPayReturnTpye.SUCCESS;
                                result = "成功";
                            }
                            else
                            {
                                content = PaymentHelper.AliPayReturnTpye.FAIL;
                                result = "trade失败 R000236" + Request.Form["trade_status"];
                                errorId = PaymentHelper.PayErrorMsg.TRADE;
                            }
                        }
                        else
                        {
                            content = PaymentHelper.AliPayReturnTpye.FAIL;
                            result = "与订单价格不对 R000211 DB=(" + PaymentHelper.getPaymentPrice(orderInfo).ToString() + " from=(" + fee.ToString() + ")";
                            errorId = PaymentHelper.PayErrorMsg.PRICE;
                        }
                    }
                    else
                    {
                        content = PaymentHelper.AliPayReturnTpye.FAIL;
                        result = "没有订单信息 R000241 " + orderId.ToString();
                        errorId = PaymentHelper.PayErrorMsg.NOTEXISTS;
                    }
                }
                else//验证失败
                {
                    content = PaymentHelper.AliPayReturnTpye.FAIL;
                    result = "验证失败 R000243";
                    errorId = PaymentHelper.PayErrorMsg.VERIFYERROR;
                }
            }
            else
            {
                content = "无返回参数";
                result = "无返回参数";
                errorId = PaymentHelper.PayErrorMsg.NOPARAMS;
            }
            if (qrFlag)
            {
                PaymentHelper.GetReturnLogEnd("1.B(2) 支付宝普通二维码同步回调", orderId, content, result);
            }
            else
            {
                PaymentHelper.GetReturnLogEnd("1.B(1) 支付宝普通同步回调", orderId, content, result);
            }
            return content;
        }

        protected string AliWapPayNotify()
        {
            string content = string.Empty;//回调用
            string result = string.Empty;//log 用
            int orderId = 0;
            Dictionary<string, string> sPara = GetRequestPost();

            string originValue = PaymentHelper.DictToStringNotSort(sPara);
            PaymentHelper.GetReturnLogStart("2.A 支付宝网页异步回调", originValue);

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                ALIWAPPAY.Notify aliNotify = new ALIWAPPAY.Notify();
                bool verifyResult = aliNotify.VerifyNotify(sPara, Request.Form["sign"]);

                if (verifyResult)//验证成功
                {
                    //解密（如果是RSA签名需要解密，如果是MD5签名则下面一行清注释掉）
                    sPara = aliNotify.Decrypt(sPara);
                    PaymentHelper.GetReturnLogStart("2.A 支付宝网页异步回调 XML解析结果", PaymentHelper.DictToStringNotSort(sPara));
                    //XML解析notify_data数据
                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(sPara["notify_data"]);
                        string out_trade_no = xmlDoc.SelectSingleNode("/notify/out_trade_no").InnerText;//商户订单号
                        string trade_no = xmlDoc.SelectSingleNode("/notify/trade_no").InnerText; //支付宝交易号
                        string trade_status = xmlDoc.SelectSingleNode("/notify/trade_status").InnerText;  //交易状态
                        string subject = xmlDoc.SelectSingleNode("/notify/subject").InnerText;  // 
                        string buyer_email = xmlDoc.SelectSingleNode("/notify/buyer_email").InnerText;  //buyer_email
                        string total_fee = xmlDoc.SelectSingleNode("/notify/total_fee").InnerText;  //buyer_email

                        decimal fee = 0;
                        decimal.TryParse(total_fee, out fee);

                        orderId = PaymentHelper.ParseOrderNumber(out_trade_no);
                        var orderInfo = PaymentHelper.getOrderInfo(orderId);
                        if (orderInfo != null)
                        {
                            if (PaymentHelper.getPaymentPrice(orderInfo).Equals(fee))
                            {
                                AliPayNotifyLogInfo log = new AliPayNotifyLogInfo()
                                {
                                    Body = "WAP",
                                    BuyerEMail = buyer_email,
                                    OrderId = orderId,
                                    Subject = subject,
                                    TotalFee = fee,
                                    TradeNo = trade_no,
                                    TradeStatus = trade_status,
                                    PaymentPlatform = Constants.PaymentPlatform.ALIWAPPAY,
                                    OriginalValue = originValue
                                };
                                PaymentHelper.setPaidLog(log);
                                if (log.OrderId > 0 && (trade_status == "TRADE_FINISHED" || trade_status == "TRADE_SUCCESS"))
                                {
                                    PaymentHelper.setPaidConfirm(log.OrderId, fee);
                                    content = PaymentHelper.AliPayReturnTpye.SUCCESS;
                                    result = "成功";
                                }
                                else
                                {
                                    content = trade_status;
                                    result = "trade失败 R000306" + trade_status;
                                }
                            }
                            else
                            {
                                content = PaymentHelper.AliPayReturnTpye.FAIL;
                                result = "与订单价格不对 R000311 DB=(" + PaymentHelper.getPaymentPrice(orderInfo).ToString() + " from=(" + fee.ToString() + ")";
                            }
                        }
                        else
                        {
                            content = PaymentHelper.AliPayReturnTpye.FAIL;
                            result = "没有订单信息 R000401" + orderId.ToString();
                        }

                    }
                    catch (Exception exc)
                    {
                        content = exc.ToString();
                        result = "参数异常 R000408" + exc.ToString();
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                else//验证失败
                {
                    content = PaymentHelper.AliPayReturnTpye.FAIL;
                    result = "验证失败 R000416";
                }
            }
            else
            {
                content = "无通知参数";
                result = "无通知参数";
            }

            PaymentHelper.GetReturnLogEnd("2.A 支付宝网页异步回调", orderId, content, result);
            return content;
        }

        protected string AliWapPayReturn(out int orderId, out int errorId)
        {
            string content = string.Empty;//回调用
            string result = string.Empty;//log 用
            orderId = 0;
            errorId = 0;
            Dictionary<string, string> sPara = GetRequestGet();

            string originValue = PaymentHelper.DictToStringNotSort(sPara);
            PaymentHelper.GetReturnLogStart("2.B 支付宝网页同步回调", originValue);

            if (sPara.Count > 0)//判断是否有带返回参数
            {
                ALIWAPPAY.Notify aliNotify = new ALIWAPPAY.Notify();
                bool verifyResult = aliNotify.VerifyReturn(sPara, Request.QueryString["sign"]);

                if (verifyResult)//验证成功
                {
                    string out_trade_no = Request.QueryString["out_trade_no"];//商户订单号
                    string trade_no = Request.QueryString["trade_no"]; //支付宝交易号
                    string trade_result = Request.QueryString["result"];  //交易状态 、、(result:success)

                    orderId = PaymentHelper.ParseOrderNumber(out_trade_no);

                    content = PaymentHelper.AliPayReturnTpye.SUCCESS;
                    result = "成功";
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                else//验证失败
                {
                    content = PaymentHelper.AliPayReturnTpye.FAIL;
                    result = "验证失败 R000361";
                    errorId = PaymentHelper.PayErrorMsg.VERIFYERROR;
                }
            }
            else
            {
                content = "无通知参数";
                result = "无通知参数";
                errorId = PaymentHelper.PayErrorMsg.NOPARAMS;
            }

            PaymentHelper.GetReturnLogEnd("2.B 支付宝网页同步回调", orderId, content, result);
            return content;
        }





        protected UCFPAY.UcfPayItem getUcfPayResquestItem()
        {
            SortedDictionary<string, string> sPara = new SortedDictionary<string, string>();
            sPara = string.Equals("POST", this.HttpContext.Request.HttpMethod.ToUpper()) ? GetRequestPostSort() : GetRequestGetSort();

            return UcfpayReturn.getUcfPayItem(sPara);
        }
        protected string UcfPayCallBack(ref string returnOrderId, UCFPAY.UcfPayItem ucfPayItem, bool IsSync)
        {
            return UcfpayReturn.UcfPayCallBack(ref returnOrderId, ucfPayItem, IsSync);
        }






        protected UNIONPAY.UnionPayItem getUnionPayResquestItem()
        {
            SortedDictionary<string, string> sPara = new SortedDictionary<string, string>();
            sPara = string.Equals("POST", this.HttpContext.Request.HttpMethod.ToUpper()) ? GetRequestPostSort() : GetRequestGetSort();

            return UnionpayReturn.getUnionPayItem(sPara);
        }
        protected string UnionPayCallBack(ref string returnOrderId, UNIONPAY.UnionPayItem item, bool IsSync)
        {
            return UnionpayReturn.UnionPayCallBack(ref returnOrderId, item, IsSync);
        }


        #endregion





        #region "collect param"

        /// 获取支付宝POST过来通知消息，并以“参数名=参数值”的形式组成数组
        /// </summary>
        /// <returns>request回来的信息组成的数组</returns>
        protected Dictionary<string, string> GetRequestPost()
        {
            int i = 0;
            Dictionary<string, string> sArray = new Dictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.Form;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.Form[requestItem[i]]);
            }

            return sArray;
        }

        protected SortedDictionary<string, string> GetRequestPostSort()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.Form;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.Form[requestItem[i]]);
            }

            return sArray;
        }

        protected SortedDictionary<string, string> GetRequestGetSort()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.QueryString;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.QueryString[requestItem[i]]);
            }

            return sArray;

        }
        protected Dictionary<string, string> GetRequestGet()
        {
            int i = 0;
            Dictionary<string, string> sArray = new Dictionary<string, string>();
            NameValueCollection coll;
            //Load Form variables into NameValueCollection variable.
            coll = Request.QueryString;

            // Get names of all forms into a string array.
            String[] requestItem = coll.AllKeys;

            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], Request.QueryString[requestItem[i]]);
            }

            return sArray;

        }
        #endregion

    }
}
