﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Services;
using VCleanse.Shop.Biz.Services.Imp;
using VCleanse.Shop.Common;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Model.Survey;


namespace VCleanse.Shop.Host.Controllers
{
    public class SurveyController : BaseController
    {
        //
        // GET: /Survey/

        public ActionResult Index()
        {
            return View("Index");
        }
        /// <summary>
        /// 通过ID获取问卷调查结果
        /// </summary>
        /// <param name="id"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadSurveyById(int id, string language)
        {
            bool isChinese = LanguageHelper.IsChinese(language);

            ISurveyService isurveyService = new SurveyService();
            var survey = isurveyService.LoadSurveyById(id);
            var surveyItem = isurveyService.LoadSurveyItemById(id);

            var result = survey.Select(master => new
            {
                SurveyId = master.SurveyId,
                Title = isChinese ? master.Title : master.EnTitle,
                Remark = isChinese ? master.Remark : master.EnRemark,
                IsActive = isurveyService.checkIsActive(master),
                ItemList = surveyItem.Where(p => p.SurveyId.Equals(master.SurveyId) && p.IsActive.Equals(true)).OrderBy(s => s.Sort).Select(item => new
                {
                    ItemId = item.ItemId,
                    IsActive = item.IsActive,
                    ItemTitle = isChinese ? item.ItemTitle : item.EnItemTitle,
                    ItemTypeId = item.ItemTypeId,
                    ItemDetail = item.ItemTypeId.Equals(VCleanse.Shop.Model.Constants.Survey.Text) ? null :
                    isurveyService.LoadSurveyItemDetailById(item.ItemDetailId)
                    .Where(z => z.ItemDetailId.Equals(item.ItemDetailId)).OrderBy(h => h.Sort).Select(detail => new
                    {
                        SelectId = detail.SelectId,
                        Name = isChinese ? detail.Name : detail.EnName,
                        Sort = detail.Sort
                    })
                })
            }).FirstOrDefault();

            return Json(new { IsSuccess = true, Survey = result.IsActive.Equals(true) ? result : null });

        }

        [HttpPost]
        public ActionResult Save(SurveyResult result)
        {
            var memberid = VcleanseContext.Current.Member_id;
            var sessionid = VcleanseContext.Current.Session_id;
            var url = Request.UrlReferrer == null ? String.Empty : Request.UrlReferrer.ToString();
            var ip = VCleanse.Shop.Host.Utility.HttpHelper.GetIp(HttpContext);

            ISurveyService isurveyService = new SurveyService();
            result.InsertBy = memberid;
            result.SessionId = sessionid;
            result.ReferUrl = url;
            result.ClientIp = ip;
            int resultId = isurveyService.save(result);
            if (resultId > 0)
            {
                foreach (SurveyResultDetail item in result.Details)
                {
                    if (item.ResultText == null || item.ResultText.Equals(string.Empty) || item.ItemId.Equals(0))
                    {
                        continue;
                    }
                    if (item.ItemTypeId.Equals(VCleanse.Shop.Model.Constants.Survey.MultiSelect))
                    {
                        SurveyResultDetail list = new SurveyResultDetail();
                        string[] arrayList = item.ResultText.Split(',');
                        foreach (var array in arrayList)
                        {
                            list.ResultText = array.Trim();
                            list.ItemId = item.ItemId;
                            isurveyService.saveDetail(resultId, list);
                        }
                    }
                    else
                    {
                        isurveyService.saveDetail(resultId, item);
                    }
                }
            }
            return Json(new { IsSuccess = true });
        }

        [HttpPost]
        public ActionResult Report(int id, string language)
        {
            bool isChinese = LanguageHelper.IsChinese(language);
            ISurveyService isurveyService = new SurveyService();

            var report = isurveyService.LoadSurveyReportById(id);

            if (!report.Any())
            {
                return CreateFailedPostJsonResult(-1, "no recode or no authority");
            }

            var result = report.Select(item => new
            {
                ItemTitle = isChinese ? item.ItemTitle : item.EnItemTitle,
                ItemTotal = item.ItemTotal,
                ItemList = item.Details.OrderByDescending(z => z.Count).Select(detail => new
                {
                    Name = isChinese ? detail.Name : detail.EnName,
                    Count = detail.Count,
                    Persent = detail.Persent
                })

            });
            return Json(new { IsSuccess = true, Report = result });
        }

    }
}
