﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class AccountBaseController : BaseController
    {
        protected string GetCellPhoneValidationSessionKey(string phone)
        {
            return string.Format("Cell_{0}", phone);
        }

        protected ValidationCodeInfo GetValidationSession(string phone)
        {
            ValidationCodeInfo cellphone = Session[GetCellPhoneValidationSessionKey(phone)] as ValidationCodeInfo;
            return cellphone;
        }

        protected JsonResult BuildPostServiceErrorResult(int statusCode, string message)
        {
            return Json(new { IsSuccess = false, ErrorCode = statusCode, ErrorMessage = message });
        }

    }

   
    [Serializable]
    public class ValidationCodeInfo
    {
        public string Phone { get; set; }

        public string Code { get; set; }

        public DateTime LastSend { get; set; }
    }
}
