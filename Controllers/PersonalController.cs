﻿using System.Linq;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.Controllers
{


    public class PersonalController : BaseController
    {
        /// <summary>
        /// 注册信息
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        [HttpGet]
        [MemberAuthorize]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult MyRegisterInfo(string memberId)
        {
            //if (member_id != CurrentMember_id)
            //{
            //    return CreateFailedGetResult();
            //}
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            var info = new
            {
                Email = memberInfo.EMail,
                FirstName = memberInfo.FirstName,
                LastName = memberInfo.LastName,
                Mobile = memberInfo.Mobile,
                GenderId = memberInfo.GenderId,
                Birthday = DateTimeUtility.FormatDate(memberInfo.BirthDay),
                IsSuccess = true,
            };
            return Json(info, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 我的收货地址信息
        /// </summary>
        /// <param name="member_id"></param>
        /// <returns></returns>
        [HttpGet]
        [MemberAuthorize(AllowImpersonate = true)]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult MyReceivers(string member_id)
        {
            //if (member_id != CurrentMember_id)
            //{
            //    return CreateFailedGetResult();
            //}
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            var result = new
            {
                IsSuccess = true,
                Receivers = memberInfo.Receivers.Select(item => new
                {
                    Id = item.Id,
                    CityId = item.CityId,
                    AreaId = item.AreaId,
                    Mobile = item.Mobile,
                    Phone = item.Phone,
                    Line1 = item.Line1,
                    Line2 = item.Line2,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    IsDefault = item.IsDefault,
                    ZipCode = item.ZipCode,
                    CityName = item.CityName,
                    AreaName = item.AreaName,
                    StoreId = item.StoreId,
                    ProvinceId = item.ProvinceId,
                    ProvinceName = item.ProvinceName,
                    IsActive = item.IsActive,
                    ExpressCompanyId = item.ExpressCompanyId,
                    ExpressCompanyName = item.ExpressCompanyName
                }),
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 新增收货地址
        /// </summary>
        /// <param name="receiver"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        //[ComplexJsonParser(typeof(Receiver), "receiver")]
        public ActionResult AddReceiver(ReceiverInfo receiver)
        {
            //if (member_id != CurrentMember_id)
            //{
            //    return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            //}
            var validateResult = ValidateReceiver(receiver);
            if (validateResult != null)
            {
                return validateResult;
            }
            return Json(new { IsSuccess = true, ReceiverID = VUsersBLL.AddReceiver(receiver, CurrentMember_id) });
        }
        /// <summary>
        /// 更新收货地址
        /// </summary>
        /// <param name="receiver"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        // [ComplexJsonParser(typeof(Receiver), "receiver")]
        public ActionResult UpdateReceiver(ReceiverInfo receiver)
        {
            var validateResult = ValidateReceiver(receiver);
            if (validateResult != null)
            {
                return validateResult;
            }
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            var existedReceiver = memberInfo.Receivers.FirstOrDefault(item => item.Id == receiver.Id);
            if (existedReceiver == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            }
            receiver.IsDefault = existedReceiver.IsDefault;
            return Json(new { IsSuccess = true, ReceiverID = VUsersBLL.UpdateReceiver(receiver, CurrentMember_id) });
        }
        /// <summary>
        /// 删除收货地址
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult DeleteReceiver(int id)
        {
            //if (member_id != CurrentMember_id)
            //{
            //    return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            //}
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            if (!memberInfo.Receivers.Exists(item => item.Id == id))
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            }
            VUsersBLL.DeleteReceiver(id, CurrentMember_id);
            return CreateSucessedPostJsonResult();
        }
        
        /// <summary>
        /// 验证收获地址
        /// </summary>
        /// <param name="receiver"></param>
        /// <returns></returns>
        private JsonResult ValidateReceiver(ReceiverInfo receiver)
        {
            if (receiver == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            }
            //else if (!ValidationUtility.IsCellPhone(receiver.Mobile))
            //{
            //    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCellPhone, "Invalid cell phone");
            //}
            return null;
        }
        /// <summary>
        /// 更新个人信息
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult UpdateMyInfo(RegisterInfo register)
        {
            if (register == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            }
            //if (register.Member_id != CurrentMember_id)
            //{
            //    return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidToken, "Invalid token");
            //}
            if (string.IsNullOrWhiteSpace(register.EMail) || !ValidationUtility.IsValidEmail(register.EMail))
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidEMail, "Invalid email");
            }
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            memberInfo.LastName = register.LastName;
            memberInfo.Mobile = register.Mobile;
            memberInfo.Password = null;// string.IsNullOrWhiteSpace(register.Password) ? memberInfo.Password : register.Password;
            memberInfo.FirstName = register.FirstName;
            memberInfo.EMail = register.EMail;
            memberInfo.GenderId = register.Gender;
            memberInfo.BirthDay = register.Birthday;
            int resultStatus = VUsersBLL.UpdateMemberInfo(memberInfo);
            if (resultStatus >= Constants.GeneralStatus.Success)
            {
                return CreateSucessedPostJsonResult();
            }
            else
            {
                return CreateFailedPostJsonResult(resultStatus, "Failed");
            }
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult ChangePassword(RegisterInfo register)
        {
            if (register == null || CurrentMember_id.Equals(Config.DefaultCustomerId) || CurrentMember_id.Equals(0)
                || string.IsNullOrWhiteSpace(register.Password) || string.IsNullOrWhiteSpace(register.ValidationCode))
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid paramers");
            }

            if (Session[WebConstants.Context.ImageValidationCode] == null)
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "img Validation code none");
            }

            string validationImageCode = Session[WebConstants.Context.ImageValidationCode].ToString();
            if (!validationImageCode.Equals(register.ValidationCode, System.StringComparison.OrdinalIgnoreCase))
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "img Validation code error");
            }


            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            if (memberInfo == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid member info");
            }

            memberInfo.Password = register.Password.Trim();

            int resultStatus = VUsersBLL.UpdateMemberInfo(memberInfo);
            if (resultStatus >= Constants.GeneralStatus.Success)
            {
                return CreateSucessedPostJsonResult();
            }
            else
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.Fail, " update failed");
            }
        }
        /// <summary>
        /// 设置默认地址
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpPost]
        public ActionResult SetAsDefaultReceiver(int id)
        {
            MemberInfo memberInfo = VUsersBLL.LoadMemberInfo(CurrentMember_id, true);
            var receiver = memberInfo.Receivers.FirstOrDefault(item => item.Id == id);
            if (receiver == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Invalid object");
            }
            if (!receiver.IsDefault)
            {
                receiver.IsDefault = true;
                VUsersBLL.UpdateReceiver(receiver, CurrentMember_id);
            }
            return Json(new { IsSuccess = true, ReceiverID = id });
        }
        /// <summary>
        /// 加载Context
        /// </summary>
        /// <returns></returns>
        [MemberAuthorize]
        [HttpGet]
        [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
        public ActionResult LoadContext()
        {
            VcleanseContext context = CurrentContext;
            if (context != null)
            {
                var clientContext = new
                {
                    Member_id = context.Member_id,
                    DisplayName = context.DisplayName,
                    Token = CreateToken(context.Member_id),
                    CacheKey = Config.CacheKey,
                };
                return Json(new { IsSuccess = true, Context = clientContext }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var clientContext = new
                {
                    Member_id = 0,
                    CacheKey = Config.CacheKey,
                };
                return Json(new { IsSuccess = true, Context = clientContext }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
