﻿using VCleanse.Shop.Biz.GG;
using VCleanse.Shop.Biz.SessionTracking;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Model.Tracking;
using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Biz;

namespace VCleanse.Shop.Host.Controllers
{
    public class AccountController : AccountBaseController
    {
        [MemberAuthorize(ToLogin = true)]
        public ActionResult Orders()
        {
            return View("Orders");
        }


        [MemberAuthorize(ToLogin = true)]
        public ActionResult OrderDetail()
        {
            return View("OrderDetail");
        }

        [MemberAuthorize(ToLogin = true)]
        public ActionResult Receiver()
        {
            return View("Receiver");
        }

        [MemberAuthorize(ToLogin = true)]
        public ActionResult Basic()
        {
            return View("Basic");
        }

        [MemberAuthorize(ToLogin = true)]
        public ActionResult Safety()
        {
            return View("Safety");
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="email">登录名</param>
        /// <param name="password">密码</param>
        /// <param name="returnUrl">null</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Logon(string email, string password, string returnUrl)
        {
            NLogHelper.LogInfo("Signin", "Email:" + email + ", Password:" + password + ", Return URL:" + returnUrl);

            if (ModelState.IsValid)
            {
                VUsersBLL userBLL = new VUsersBLL();
                //登陆者的ID loginID
                int loginID = userBLL.VUSERLOGIN(email, password, HttpHelper.GetIp(HttpContext), WebConstants.LogMessage.LogonViaWebSite);

                if (loginID == 0)
                {
                    return CreateFailedPostJsonResult(Constants.LogonStatus.InvalidUserNameOrPassword, "Invalid username or password");
                }
                else
                {
                    var memberInfo = InitMemberInfo(loginID);
                    //写入session
                    SessionTrackingService.SaveSessionUser(VcleanseContext.Current.Session_id, memberInfo.Id);
                    NLogHelper.LogInfo("SigninSuccess", "Email:" + email + ", Password:" + password + ", Return URL:" + returnUrl);

                    return Json(new { IsSuccess = true, Id = memberInfo.Id, Name = memberInfo.FirstName });
                }
            }
            return null;
        }

        private VCleanse.Shop.Model.Members.MemberInfo InitMemberInfo(int memberId)
        {
            var memberInfo = VUsersBLL.LoadMemberInfo(memberId, false);
            HttpHelper.WriteCookie(HttpContext, WebConstants.CookieKey.Id, memberInfo.Id.ToString(), true);
            HttpHelper.WriteCookie(HttpContext, WebConstants.CookieKey.Name, memberInfo.FirstName, false);
            return memberInfo;
        }

        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Id);
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Session);
            return RedirectToAction("home", "intl");
        }

        #region Register
        [HttpPost]
        public ActionResult Regist(RegisterInfo register)
        {
            NLogHelper.LogInfo("Signin", new JavaScriptSerializer().Serialize(register));

            if (ModelState.IsValid)
            {
                JsonResult result = ValidateRegistInfo(register);
                if (result != null)
                {
                    return result;
                }
                int registResult = VUsersBLL.RegistMe(register.Mobile, register.Password, HttpHelper.GetIp(this.HttpContext), register.ValidateType);
                if (registResult == Constants.RegistStatus.DuplicatedMobileLegacy)
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.DuplicatedMobileLegacy, "Duplicated mobile");
                }
                else
                {
                    InitMemberInfo(registResult);
                    return CreateSucessedPostJsonResult();
                }
            }
            return CreateFailedPostJsonResult(Constants.GeneralStatus.UnpextedError, "Unexpected Error");
        }

        private bool CheckVerifyCode(string code)
        {
            if (Session[WebConstants.Context.ImageValidationCode] == null)
            {
                NLogHelper.LogInfo("ValidationCode", code);
                return false;
            }
            string validationImageCode = Session[WebConstants.Context.ImageValidationCode].ToString();
            if (validationImageCode.Equals(code, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            else
            {
                NLogHelper.LogInfo("ValidationCode", validationImageCode + " / " + code);
                return false;
            }
        }

        private JsonResult ValidateRegistInfo(RegisterInfo register)
        {
            if (register == null)
            {
                return CreateFailedPostJsonResult(Constants.GeneralStatus.InvalidObject, "Empty object");
            }
            if (!ValidationUtility.IsCellPhone(register.Mobile))
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCellPhone, "Invalid phone");
            }
            if (register.ValidateType.Equals(0))
            {
                if (Session[WebConstants.Context.ImageValidationCode] == null)
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "img Validation code none");
                }
                string validationImageCode = Session[WebConstants.Context.ImageValidationCode].ToString();
                if (validationImageCode.Equals(register.ValidationCode, StringComparison.OrdinalIgnoreCase))
                {
                    return null;
                }
                else
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "img Validation code error");
                }

            }
            else
            {
                ValidationCodeInfo validationCode = GetValidationSession(register.Mobile);
                if (validationCode == null
                    || !string.Equals(validationCode.Code, register.ValidationCode, StringComparison.OrdinalIgnoreCase)
                    || validationCode.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "Validation code error");
                }
                else
                {
                    return null;
                }
            }

            //if (!ValidationUtility.IsValidEmail(register.EMail))
            //{
            //    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidEMail, "Invalid email");
            //}
            //if (string.IsNullOrWhiteSpace(register.FirstName))
            //{
            //    return CreateFailedPostJsonResult(Constants.RegistStatus.EmptyFirstName, "Fristname can't be empty");
            //}
            //if (string.IsNullOrWhiteSpace(register.LastName))
            //{
            //    return CreateFailedPostJsonResult(Constants.RegistStatus.EmptyLastName, "LastNaem can't be empty");
            //}
           
        }

        private VCleanse.Shop.Model.VUsers CreateUser(RegisterInfo register)
        {
            VCleanse.Shop.Model.VUsers registerUser = new VCleanse.Shop.Model.VUsers()
            {
                EndRemark = string.Empty,
                IP = HttpHelper.GetIp(this.HttpContext),
                isChecked = 1,
                LastName = register.LastName,
                VBirthday = new DateTime(1900, 1, 1),
                VEMail = register.EMail,
                VMobile = register.Mobile,
                VPass = register.Password,
                VTel = register.Mobile,
                VProfession = 1,
                VUser = register.FirstName,
            };
            //AssignAddress(registerUser, register);
            return registerUser;
        }

        //private void AssignAddress(VCleanse.Shop.Model.VUsers registerUser, RegisterInfo register)
        //{
        //    if (register.Address == null || register.Address.Count == 0)
        //    {
        //        return;
        //    }
        //    Address address = register.Address[0];
        //    registerUser.VCityID = address.CityId;
        //    registerUser.VAreaID = address.districtId;
        //    registerUser.VAddress = string.Concat(address.Company, address.Street);
        //}
        #endregion

        [HttpPost]
        public JsonResult ForgetPassword(string email)
        {
            VUsersBLL userBLL = new VUsersBLL();
            int status = userBLL.ForgotPassword(email, HttpHelper.GetIp(HttpContext));
            if (status == Constants.ForgetPasswordStatus.InvalidEmail)
            {
            }
            else if (status == Constants.ForgetPasswordStatus.SystemBusy)
            {
            }
            else
            {

            }
            return Json(new { IsSuccess = true });
        }


        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
        [HttpPost]
        public ActionResult SendValidationCodeByWeb(string mobile, string language, string verifyCode)
        {
            if (!CheckVerifyCode(verifyCode))
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCodeNum, "Validation code error");
            return SendValidationCode(mobile, language);
        }

        [HttpPost]
        public ActionResult SendValidationCode(string mobile, string language)
        {
            if (string.IsNullOrWhiteSpace(mobile) || !ValidationUtility.IsCellPhone(mobile))
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCellPhone, "Invalid cellphone");
            }
            else
            {
                //if (!string.Equals(Request.UrlReferrer.Host, Config.Host, StringComparison.OrdinalIgnoreCase))
                //{
                //    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidHost, "Invalid host");
                //}
                string key = GetCellPhoneValidationSessionKey(mobile);
                ValidationCodeInfo cellPhone = Session[key] as ValidationCodeInfo;
                if (cellPhone != null && cellPhone.LastSend.AddSeconds(Config.CellPhoneValidationPeriodSeconds) > DateTime.UtcNow)
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.ToFrequent, "To frequent");
                }
                VUsersBLL bll = new VUsersBLL();
                //if (bll.IsPhoneExisted(mobile))
                //{
                //    return CreateFailedPostJsonResult(Constants.RegistStatus.DuplicatedMobile, "Duplicated cellphone");
                //}
                if (cellPhone == null || cellPhone.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
                {
                    cellPhone = new ValidationCodeInfo()
                    {
                        Phone = mobile,
                        Code = RandomHelper.FixLengthNumber(Config.PhoneValidationCodeLength),
                    };
                }
                cellPhone.LastSend = DateTime.UtcNow;
                Session[key] = cellPhone;
                SMSService.Instance.SendSMS(mobile, string.Format(LanguageHelper.IsChinese(language) ?
                    "您的维果清验证码是{0}" :
                    "Your VCLEANSE verification code is {0}", cellPhone.Code));
            }
            return CreateSucessedPostJsonResult();
        }






    }
}
