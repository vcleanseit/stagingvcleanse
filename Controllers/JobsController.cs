﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class JobsController : BaseController
    {
        //
        // GET: /Job/

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Digitalmarketing_en()
        {
            return View("digitalmarketing_en");
        }

        public ActionResult Digitalmarketing_cn()
        {
            return View("digitalmarketing_cn");
        }

        public ActionResult Salesandmarketingassistant_en()
        {
            return View("salesandmarketingassistant_en");
        }

        public ActionResult Salesandmarketingassistant_cn()
        {
            return View("salesandmarketingassistant_cn");
        }

        public ActionResult Salesrepresentative_en()
        {
            return View("salesrepresentative_en");
        }

        public ActionResult Salesrepresentative_cn()
        {
            return View("salesrepresentative_cn");
        }

        public ActionResult Workshopleaderparttime_en()
        {
            return View("workshopleaderparttime_en");
        }

        public ActionResult Workshopleaderparttime_cn()
        {
            return View("workshopleaderparttime_cn");
        }

        public ActionResult StoreAssistant_en()
        {
            return View("storeassistant_en");
        }

        public ActionResult StoreAssistant_cn()
        {
            return View("storeassistant_cn");
        }

        public ActionResult Cashier_en()
        {
            return View("cashier_en");
        }

        public ActionResult Cashier_cn()
        {
            return View("cashier_cn");
        }

        public ActionResult Csrepresentative_en()
        {
            return View("csrepresentative_en");
        }

        public ActionResult Csrepresentative_cn()
        {
            return View("csrepresentative_cn");
        }

        public ActionResult Prmanager_en()
        {
            return View("prmanager_en");
        }

        public ActionResult Prmanager_cn()
        {
            return View("prmanager_cn");
        }

        public ActionResult Senioruidesigner_en()
        {
            return View("senioruidesigner_en");
        }

        public ActionResult Senioruidesigner_cn()
        {
            return View("senioruidesigner_cn");
        }

        public ActionResult Onlineoperation_en()
        {
            return View("onlineoperation_en");
        }

        public ActionResult Onlineoperation_cn()
        {
            return View("onlineoperation_cn");
        }

        public ActionResult Socialmediaoperation_en()
        {
            return View("socialmediaoperation_en");
        }

        public ActionResult Socialmediaoperation_cn()
        {
            return View("socialmediaoperation_cn");
        }
        public ActionResult Productmanager_en()
        {
            return View("productmanager_en");
        }
        public ActionResult Productmanager_cn()
        {
            return View("productmanager_cn");
        }
        public ActionResult Supply_en()
        {
            return View("supply_en");
        }
        public ActionResult Supply_cn()
        {
            return View("supply_cn");
        }

    }
}
