﻿using VCleanse.Shop.Biz.Log;
using VCleanse.Shop.Biz.SessionTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Host.Models;

namespace VCleanse.Shop.Host
{
    public class SessionService
    {
        private static readonly string _ggIdParam = "gguserid";
        private static readonly string _initSourceReferer = "referer";
        /// <summary>
        /// 分析保存会话
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="context"></param>
        public static void AnalyzeAndSaveSession(string sessionId, HttpContext context)
        {
            try
            {
                var session = BuildSessionInfo(sessionId, context);
                SaveSession(session);
            }
            catch (Exception ex)
            {
                ExceptionPublishHelper.PublishFormat(ex);
            }
        }
        /// <summary>
        /// 创建Session信息
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private static SessionInfo BuildSessionInfo(string sessionId, HttpContext context)
        {
            var userAgent = context.Request.UserAgent;
            var referer = context.Request.UrlReferrer == null ? string.Empty : context.Request.UrlReferrer.ToString();
            var session = new SessionInfo()
            {
                SessionId = sessionId,
                UserAgent = userAgent
            };

            if (!TryAnalyzeAsGGOrder(context, ref session))
            {
                session.VisitSourceTypeId = TryGetVisitSourceTypeFromReferrer(referer);
            }
            session.VisitSourceRawValue = referer;

            var initSourceReferer = context.Request.QueryString[_initSourceReferer];
            // Special logic for "+"
            //initSourceReferer = initSourceReferer.Replace(" ", "+");
            if (!string.IsNullOrEmpty(initSourceReferer))
            {
                session.InitVisitSourceTypeId = TryGetVisitSourceTypeFromReferrer(initSourceReferer);
                session.InitVisitSourceRawValue = initSourceReferer;
            }

            return session;
        }
        /// <summary>
        /// 保存session
        /// </summary>
        /// <param name="session"></param>
        private static void SaveSession(SessionInfo session)
        {
            SessionTrackingService.SaveSession(session);
        }

        private static bool TryAnalyzeAsGGOrder(HttpContext context, ref SessionInfo session)
        {
            int ggUserId = 0;

            var orderSourceType = context.Request.QueryString[_ggIdParam];
            if (!string.IsNullOrEmpty(orderSourceType)
                && int.TryParse(orderSourceType, out ggUserId))
            {
                session.VisitSourceTypeId = VisitSourceService.GetGGVisitSourceType().VisitSourceTypeId;
                session.VisitSourceTypeValue = ggUserId;

                return true;
            }
            
            return false;
        }
        /// <summary>
        /// 尝试从引导获取来源类型
        /// </summary>
        /// <param name="userAgent"></param>
        /// <returns></returns>
        private static int TryGetVisitSourceTypeFromReferrer(string userAgent)
        {
            var allVisitSources = VisitSourceService.GetAll().FindAll(s => !string.IsNullOrEmpty(s.RefererPattern));

            var visitSource = allVisitSources.Find(s => s.RefererPattern.Split(';').ToList().Exists(v => !string.IsNullOrEmpty(v) && userAgent.ToLower() == v.ToLower()));

            if (visitSource != null)
            {
                return visitSource.VisitSourceTypeId;
            }

            return 0;
        }
    }
}