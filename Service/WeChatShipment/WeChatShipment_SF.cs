﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using VCleanse.Shop.Model.WeChatOrders;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public class WeChatShipment_SF : IWeChatShipmentService
    {
        public const string EXPRESS_COMPANY_NAME = "顺丰快递";

        public WeChatExpressShowInfo GetShipmentInfo(string DeliveryNo, string storeId)
        {
            if (string.IsNullOrEmpty(DeliveryNo) || DeliveryNo.Length > 15)
            {
                return new WeChatShipment_Null(EXPRESS_COMPANY_NAME, "快递单编码不正确").GetShipmentInfo(DeliveryNo, storeId);
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://syt.sf-express.com/css/newmobile/queryBillInfo.action?delivery_id=" + DeliveryNo + "");
            request.Method = "GET";
            WebHeaderCollection headers = new WebHeaderCollection();
            headers.Add("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
            request.Headers = headers;
            request.ContentType = "text/html; charset=utf-8";
            WebResponse response = request.GetResponse();
            var reStream = response.GetResponseStream(); //这里就已经得到响应的字符流了
            string content;
            using (StreamReader read = new StreamReader(reStream, Encoding.UTF8))
            {
                content = read.ReadToEnd();//从这里面过滤到你想要的title值就行了 
            }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            WeChatShipmentInfo list = ser.Deserialize<WeChatShipmentInfo>(content);

            WeChatExpressShowInfo result = new WeChatExpressShowInfo()
            {
                Title_Lbl = "物流公司",
                Title = EXPRESS_COMPANY_NAME,
                DeliverNo = DeliveryNo,
                DeliverOrAddress_Lbl = "配送状态",
                DeliverOrAddress = list.Message,
                DeliveryReceiver = list.Result.Delivery_Message
            };

            if (list.Result.Router != null)
            {
                result.ShipmentMsg = new List<WeChatShipmentDetailInfo>();
                foreach (var item in list.Result.Router)
                {
                    result.ShipmentMsg.Add(new WeChatShipmentDetailInfo()
                    {
                        Message = item.Statue_Message,
                        Time = item.Time
                    });
                }
            }
            return result;
        }
    }
}