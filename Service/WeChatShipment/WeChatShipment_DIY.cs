﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Model._Legacy.Shared;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public class WeChatShipment_DIY : IWeChatShipmentService
    {
        public WeChatExpressShowInfo GetShipmentInfo(string DeliveryNo, string storeId)
        {
            var store = BuildStoreInfo(storeId);
            return new WeChatExpressShowInfo()
            {
                Title = store.StoreName,
                Title_Lbl = "自提门店",
                DeliverNo = DeliveryNo,
                DeliverOrAddress = store.StoreAddress,
                DeliverOrAddress_Lbl = "门店地址",
                DeliveryReceiver = ""
            };
        }

        private StoreInfo BuildStoreInfo(string storeId)
        {
            if (storeId == null || storeId.Length == 0)
            {
                return new StoreInfo();
            }
            var storeList = SharedBLL.LoadAllStores();
            return storeList.FirstOrDefault(item => item.StoreId.ToString() == storeId);
        }
    }
}