﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public class WeChatExpressShowInfo
    {
        /// <summary>
        /// 物流公司名称/门店名称
        /// </summary>
        public string Title { set; get; }
        /// <summary>
        /// 物流公司名称/门店名称 标签
        /// </summary>
        public string Title_Lbl { set; get; }

        /// <summary>
        /// 物流单据号
        /// </summary>
        public string DeliverNo { set; get; }

        /// <summary>
        /// 物流状态或者外部地址
        /// </summary>
        public string DeliverOrAddress { set; get; }

        /// <summary>
        /// 物流状态或者外部地址 标签
        /// </summary>
        public string DeliverOrAddress_Lbl { set; get; }

        /// <summary>
        /// 签收人
        /// </summary>
        public string DeliveryReceiver { set; get; }
        /// <summary>
        /// 物流单流转明细
        /// </summary>
        public List<WeChatShipmentDetailInfo> ShipmentMsg { set; get; }
    }

    public class WeChatShipmentDetailInfo
    {
        /// <summary>
        /// 流转信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public string Time { get; set; }
    }
}