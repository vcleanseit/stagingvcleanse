﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service
{
    public class SystemSwitchEntity
    {
        public string switchTo { set; get; }
        public List<string> excludeKeys { set; get; }
        public List<string> mobileKeys { set; get; }
        public List<string> switchUrls { set; get; }
    }
}