﻿using System.Web;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Host.Utility;
using System.Web.Caching;

namespace VCleanse.Shop.Host.Context
{
    public class VcleanseContext
    {
        public string Session_id { get; set; }

        public bool IsUserLogined { get; set; }

        public int Member_id { get; set; }

        public string DisplayName { get; set; }

        public MemberInfo Member { get; set; }

        public bool IsDebug { get; set; }

        public bool Impersonating { get; set; }

        public string OpenId { get; set; }

        public string CacheServer
        {
            get
            {
                //return "http://192.168.1.9:3000";
                return Config.CacheServer;
            }
        }

        public string Domain
        {
            get
            {
                //return "http://192.168.1.9:3000";
                return Config.Domain;
            }
        }

        public string ClientCacheKey
        {
            get
            {
                //return "1.0.0";
                //return "snapshot";
                return Config.ClientCacheKey;
            }
        }

        public OrderSource OrderSourceType { get; set; }

        public string OrderSourceValue { get; set; }

        public static VcleanseContext Current
        {
            get
            {
                VcleanseContext current = HttpContext.Current.Items[WebConstants.Context.CurrentUserKey] as VcleanseContext;
                if (current == null)
                {
                    current = new VcleanseContext();
                }
                return current;
            }
        }
    }
}