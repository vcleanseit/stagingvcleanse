﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Shared;
using VCleanse.Shop.Model.WeChatOrders;
using OrderMDL = VCleanse.Shop.Model.Orders;
namespace VCleanse.Shop.Host.Utility
{
    public class OrderStatusHelper
    {
        public static bool checkOrderIsPaid(OrderMDL.OrderItemInfo orderInfo)
        {
            var orderPayment = orderInfo.OrderPayments.Find(p => p.IsOnlinePay.Equals(true));
            return orderPayment == null ? false : orderPayment.IsPaid;
        }
        /// <summary>
        /// 判断订单是否能够删除
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public static bool checkOrderCannotDelete(OrderItemInfo orderItem)
        {
            foreach (var item in  orderItem.OrderPayments)
            {
                if (item.IsPaid.Equals(true))
                {
                    return true;
                }
            }
            return orderItem.StatusCode.Equals(Constants.StatusCode.Expired)
                || orderItem.StatusCode.Equals(Constants.StatusCode.InProcess)
            || orderItem.StatusCode.Equals(Constants.StatusCode.Complete);
        }

        /// <summary>
        /// 判断订单是否可以付款
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public static bool checkOrderCanPaid(OrderItemInfo orderItem)
        {
            return orderItem.StatusCode > Constants.StatusCode.Cancelled;
        }
        /// <summary>
        /// 订单是否过期
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool checkOrderIsExpired(DateTime date)
        {
            return date.AddMinutes(Config.OrderBeforeMinutes) < DateTime.Now;
        }
        /// <summary>
        /// 订单支付列表
        /// </summary>
        /// <param name="payments"></param>
        /// <param name="paymentDict"></param>
        /// <param name="isChinese"></param>
        /// <returns></returns>
        public static object OrderPaymentList(List<OrderPayment> payments, Dictionary<byte, KeyValueType> paymentDict, bool isChinese)
        {
            return payments.Select(item => new
            {
                PaymentType = GetNameForByte((byte)item.PaymentType, paymentDict, isChinese),
                PaymentTypeId = item.PaymentType,
                PayDate = item.PayDate == null ? string.Empty : item.PayDate.Value.ToString("yyyy-MM-dd"),
                IsOnlinePay = item.IsOnlinePay,
                Payment = item.Payment,
                IsPaid = item.IsPaid,
            });

        }
        /// <summary>
        /// 微信支付订单
        /// </summary>
        /// <param name="payments"></param>
        /// <param name="paymentDict"></param>
        /// <param name="isChinese"></param>
        /// <returns></returns>
        public static List<WeChatOrderPayment> OrderPaymentListForWeChat(List<OrderPayment> payments, Dictionary<byte, KeyValueType> paymentDict, bool isChinese)
        {
            List<WeChatOrderPayment> list = new List<WeChatOrderPayment>();

            payments.ForEach(item =>
            {
                list.Add(new WeChatOrderPayment()
                {
                    PaymentTypeName = GetNameForByte((byte)item.PaymentType, paymentDict, isChinese),
                    PaymentType = item.PaymentType,
                    IsOnlinePay = item.IsOnlinePay,
                    Payment = item.Payment,
                    IsPaid = item.IsPaid
                });
            });
            return list;
        }
        /// <summary>
        /// 发货时间
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deliverTimeDict"></param>
        /// <param name="isChinese"></param>
        /// <returns></returns>
        private static string GetNameForByte(byte id, Dictionary<byte, KeyValueType> deliverTimeDict, bool isChinese)
        {
            if (deliverTimeDict.ContainsKey(id))
            {
                return isChinese ? deliverTimeDict[id].Name : deliverTimeDict[id].ENName;
            }
            return string.Empty;
        }

        /// <summary>
        /// 订单状态
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="orderStatusCodeList"></param>
        /// <param name="isChinese"></param>
        /// <returns></returns>
        public static string GetStatusCodeString(int statusCode, List<IntegeKeyValueType> orderStatusCodeList, bool isChinese)
        {
            var status = orderStatusCodeList.Find(p => p.Id.Equals(statusCode));

            return status == null ? string.Empty : (isChinese ? status.Name : status.ENName);
        }


    }
}