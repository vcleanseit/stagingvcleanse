﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using VCleanse.Shop.Data.Log;

namespace VCleanse.Shop.Host.Utility
{
    public static class HttpHelper
    {

        public static string getLogContext()
        {
            Logcontext context = Logcontext.Create();
            return new JavaScriptSerializer().Serialize(context);
        }

        public static string GetCookieValue(string key, HttpRequest request)
        {
            HttpCookie cookie = request.Cookies[key];
            string value = cookie == null ? string.Empty : cookie.Value;
            if (string.IsNullOrWhiteSpace(value))
            {
                value = string.Empty;
            }
            return value;
        }

        public static void WriteCookie(HttpContextBase contextBase,  string key, string value, bool encrpyt)
        {
            HttpCookie cookie = new HttpCookie(key);
            cookie.Value = (encrpyt ? Security.Encrypt(value) : value);
            //cookie.Domain = "/";
            contextBase.Response.Cookies.Add(cookie);
        }

        public static void WriteCookie(HttpContext context, string key, string value, bool encrpyt)
        {
            HttpCookie cookie = new HttpCookie(key);
            cookie.Value = (encrpyt ? Security.Encrypt(value) : value);
            context.Response.Cookies.Add(cookie);
        }

        public static void ClearCookie(HttpContextBase contextBase, string key)
        {
            HttpCookie cookie = new HttpCookie(key, string.Empty);
            cookie.Expires = DateTime.Now.AddYears(-1);
            contextBase.Response.Cookies.Add(cookie);
        }

        internal static string GetIp(HttpContextBase context)
        {
            string ip = "";
            try
            {
                HttpRequestBase request = context.Request;
                if (request.ServerVariables["http_VIA"] != null)
                {
                    ip = request.ServerVariables["http_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
                }
                else
                {
                    ip = request.UserHostAddress;
                }
            }
            catch (Exception e)
            {
                ip = string.Empty;
            }
            return ip;
        }
    }
}