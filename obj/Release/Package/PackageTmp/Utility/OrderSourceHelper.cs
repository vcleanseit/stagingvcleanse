﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Host.Context;

namespace VCleanse.Shop.Host.Utility
{
    public class OrderSourceHelper
    {
        public static Tuple<OrderSource, string> LoadOrderSourceFromCookie()
        {
            var cookieValue = HttpHelper.GetCookieValue(WebConstants.CookieKey.OrderSource, HttpContext.Current.Request);
            if (!string.IsNullOrWhiteSpace(cookieValue))
            {
                var items = cookieValue.Split('$');
                if (items.Length == 2)
                { 
                    var sourceTypeId = int.Parse(items[0]);
                    return new Tuple<OrderSource, string>((OrderSource)sourceTypeId, items[1]);
                }
            }

            return new Tuple<OrderSource, string>(OrderSource.UnKnown, string.Empty);

        }
        
        public static void SaveOrderSourceToCookie(OrderSource orderSource, string orderSourceValue)
        {
            var cookieValue = BuildOrderSourceValue(orderSource, orderSourceValue);
            HttpHelper.WriteCookie(HttpContext.Current, WebConstants.CookieKey.OrderSource, cookieValue, false);
        }

        private static string BuildOrderSourceValue(OrderSource orderSource, string orderSourceValue)
        {
            return string.Format("{0}${1}", ((int)orderSource).ToString(), orderSourceValue);
        }
    }
}