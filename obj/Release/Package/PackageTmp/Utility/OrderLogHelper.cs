﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using VCleanse.Shop.Biz;
using System.Web.Script.Serialization;
using VCleanse.Shop.Model.Orders;

namespace VCleanse.Shop.Host.Utility
{
    public class OrderLogHelper
    {
        public static void WriteOrderLog_AllItem(int orderId, int changeUserId, string changeType)
        {
            OrderItemInfo orderInfo = OrdersBLL.LoadOrderById(orderId);
            if (orderInfo != null)
            {
                NLogHelper.LogSplit(2);
                NLogHelper.LogInfo("-----(changeType:" + changeType + ")----(changeUserId:" + changeUserId + ")---[" + new JavaScriptSerializer().Serialize(orderInfo) + "]");
                NLogHelper.LogSplit(2);
            }
        }
        public static void WriteOrderLog_UpdateReceiver(int ordermId, int receiverId, int deliverTimeId, int changeUserId, string changeType)
        {
            NLogHelper.LogSplit(2);
            NLogHelper.LogInfo("-----(changeType:" + changeType + ")----(changeUserId:" + changeUserId + ")[ordersmid=" + ordermId + "][receiverid=" + receiverId + "][delivertimeid=" + deliverTimeId + "]");
            NLogHelper.LogSplit(2);
        }
    }
}