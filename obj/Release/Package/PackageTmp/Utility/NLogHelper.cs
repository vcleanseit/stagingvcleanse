﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog;
using VCleanse.Shop.Model.Orders;
using System.Web.Script.Serialization;
using VCleanse.Shop.Model.Promotions;
using System.Text.RegularExpressions;


namespace VCleanse.Shop.Host.Utility
{
    public static class NLogHelper
    {
        private static Logger _logger = LogManager.GetLogger("frontend-change-log");

        public static void LogSplit(int index = 1)
        {
            for (int i = 1; i <= index; i++)
            {
                NLogHelper.LogInfo("--------------i am Split--------------------");
            }
        }

        public static void LogInfo(OrderItemInfo obj)
        {
            _logger.Info(obj);
        }
        public static void LogInfo(string message)
        {
            _logger.Info(message);
        }
        public static void LogInfo(string orderid, string message)
        {
            message = Regex.Replace(message, @"\\/Date\((\d+)\)\\/", match =>
            {
                DateTime dt = new DateTime(1970, 1, 1);
                dt = dt.AddMilliseconds(long.Parse(match.Groups[1].Value));
                dt = dt.ToLocalTime();
                return dt.ToString("yyyy-MM-dd HH:mm:ss");
            });
            _logger.Info("OrderId=[" + orderid + "],Msg=[" + message + "]");
        }
      
        public static void LogInfo(NLogParamers paramers)
        {
            _logger.Info(paramers);
        }
        public static void LogDebug(object obj)
        {
            _logger.Debug(obj);
        }


        public static void LogPromotionInfo(bool flag,int step,PromotionCode code, object data)
        {
            string flagstr = flag ? "confirm" : "check";
            string dataStr = new JavaScriptSerializer().Serialize(data);
            _logger.Info("Promotion[" + flagstr + "] Error-Step=[" + step + "]OrderId=[" + code.OrderId + "],PromotionCode=[" + code.Code + "]Msg=[" + dataStr + "]");
        }
        public static void LogPromotionInfo(bool flag, PromotionCode code)
        {
            string flagstr = flag ? "confirm" : "check";
            _logger.Info("Promotion[" + flagstr + "]  Success-OrderId=[" + code.OrderId + "],PromotionCode=[" + code.Code + "]");
        }
        public static void LogPromotionInfo(bool flag, int orderId, int oldRuleid, int oldRuleType)
        {
            string flagstr = flag ? "confirm" : "check";
            _logger.Info("Promotion[" + flagstr + "]  Old-OrderId=[" + orderId.ToString() + "],OldPromotionId=[" + oldRuleid + "] OldRuleType=[" + oldRuleType + "]");
        }
    }

    public class NLogParamers
    {
        public string LogType { get; set; }
        public string ControlerName { get; set; }
        public string ActionName { get; set; }
        public string Message { get; set; }

    }
}