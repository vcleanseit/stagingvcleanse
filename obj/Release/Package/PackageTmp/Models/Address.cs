﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Models
{
    [Serializable]
    public class Address
    {
        public string Company { get; set; }
        public string Street { get; set; }
        public int CityId { get; set; }
        public int districtId { get; set; }
        public int AddressId { get; set; }
    }
}