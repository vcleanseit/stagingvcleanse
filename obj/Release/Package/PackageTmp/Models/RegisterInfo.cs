﻿using System;

namespace VCleanse.Shop.Host.Models
{
    [Serializable]
    public class RegisterInfo
    {
        //public List<Address> Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public int Member_id { get; set; }
        public string ValidationCode { get; set; }
        public byte Gender { get; set; }
        public DateTime Birthday { get; set; }

        public int ValidateType { get; set; }

    }
}