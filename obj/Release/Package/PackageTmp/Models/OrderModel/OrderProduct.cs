﻿
namespace VCleanse.Shop.Host.Models.OrderModel
{
    public class OrderProduct
    {
        public int Id { get; set; }
        public int Count { get; set; }

        public override bool Equals(object obj)
        {
            OrderProduct toCompare = obj as OrderProduct;
            if (toCompare == null)
            {
                return false;
            }
            return Id.Equals(toCompare.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}