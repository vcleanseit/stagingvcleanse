﻿using System;
using System.Collections.Generic;
using VCleanse.Shop.Model.Members;

namespace VCleanse.Shop.Host.Models.OrderModel
{
    public class DeliverItem
    {
        public DateTime Date { get; set; }
        public string StrDate { get; set; }

        public int ReceiverId { get; set; }

        public bool IsSelfpickup { get; set; }

        public List<OrderProduct> Products { get; set; }

        public ReceiverInfo Contact { get; set; }

        public int DeliverTimeId { get; set; }

        //public List<OrderProduct> PackageProducts { get; set; }

        public int PackageRuleId { get; set; }

        public int PackageRuleCount { get; set; }

        public SelfpickupInfo SelfpickupInfo { get; set; }

    }
}
