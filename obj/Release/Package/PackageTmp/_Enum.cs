﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host
{
    public enum OrderSource : int
    {
        UnKnown = 0,
        GG = 9
    }
	
	
	public enum VisitSource : int
    {
        DirectVisit = 0,
        GG = 1,
        WeiBo = 2,
        WeChat = 3,
        Google = 4,
        Baidu = 5
    }
	
}