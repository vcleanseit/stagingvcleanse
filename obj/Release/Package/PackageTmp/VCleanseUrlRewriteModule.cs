﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Host.Service;

namespace VCleanse.Shop.Host
{
    public class VCleanseUrlRewriteModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;

            var switchTo = SystemSwitch.SwitchSystem(context.Request.UserAgent);
            if (switchTo.Length > 0)
            {
                Uri uri = new Uri(switchTo);
                if(!transMobileDomain(uri, context))
                    context.RewritePath(switchTo);
            }
            else
                transMobileDomain(context.Request.Url, context);   
        }

        private bool transMobileDomain(Uri uri, HttpContext context)
        {
            if (IsMobileDomain(uri.AbsoluteUri)
                && !uri.PathAndQuery.StartsWith("/m/"))
            {
                var mobilePath = string.Concat("/m", uri.PathAndQuery);
                context.RewritePath(mobilePath);
                return true;
            }
            return false;
        }
       
        private bool IsMobileDomain(string visitDomain)
        {
            var mobileDomain = new Uri(Config.MobileDomain);
            return visitDomain.ToLower().Contains(mobileDomain.Authority.ToLower());
        }
    }
}