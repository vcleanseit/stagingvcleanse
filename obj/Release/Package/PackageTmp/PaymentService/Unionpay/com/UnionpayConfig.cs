﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VCleanse.Shop.Biz.Shared;

namespace VCleanse.Shop.Host.PaymentService.Unionpay.com
{
    public static class UnionPayConfig
    {
        public static readonly string  postAction = "https://unionpaysecure.com/api/Pay.action";//（这里action的内容为提交交易数据的URL地址）

        public static readonly string MerId = "808080201306372";//（MerId为ChinaPay统一分配给商户的商户号，15位长度，必填）

        public static readonly int LengthMerSplit = 5;   // 中间长度 放merid 最后五位
        public static readonly int LengthOrderSplit = 7;  // 右边长度 放 orderid 后7位
        public static readonly int LengthTotalOrder = 16;  // orderid 总长度
        public static readonly char Zero = '0';  // orderid和价格  补零.
        public static readonly int LengthTransAmt = 12;  // 价格长度

        public static readonly string CuryId = "156"; //（订单交易币种，3位长度，固定为人民币156，必填）
        public static readonly string TransType = "0001";//（交易类型，4位长度，必填）
        public static readonly string Version = "20070129";//（支付接入版本号，必填）
        public static readonly string BgRetUrl = string.Format("{0}/Payment/UnionPayNotifyAsync", Config.Domain);//（后台交易接收URL，长度不要超过80个字节，必填） 异步
        public static readonly string PageRetUrl = string.Format("{0}/Payment/UnionPayNotify", Config.Domain);//页面交易接收URL，长度不要超过80个字节，必填） 同步
        public static readonly string GateId = "";//（支付网关号，可选）
        public static readonly string Priv1 = "VCLEANSE";//（商户私有域，长度不要超过60个字节）



    }

    public class UnionPayItem
    {
        public string MerId { get; set; }
        public string OrderNO { get; set; }
        public string TransDate { get; set; }
        public string Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string TransType { get; set; }
        public string Status { get; set; }
        public string CheckValue { get; set; }
        public string GateId { get; set; }
        public string Priv1 { get; set; }

    }
}
