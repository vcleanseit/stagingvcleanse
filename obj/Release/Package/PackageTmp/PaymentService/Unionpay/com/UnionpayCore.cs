﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.PaymentService.Unionpay.com
{
    public class UnionpayCore
    {
        public static string TransUnionPayAmt(string total_fee)
        {
            decimal intFee = 0;
            if (!string.IsNullOrWhiteSpace(total_fee))
            {
                decimal.TryParse(total_fee, out intFee);
            }

            return Convert.ToInt32((intFee) * 100).ToString().PadLeft(UnionPayConfig.LengthTransAmt, UnionPayConfig.Zero);
        }

        public static decimal OutTransUnionPayAmt(string total_fee)
        {
            decimal intFee = 0;
            if (!string.IsNullOrWhiteSpace(total_fee))
            {
                decimal.TryParse(total_fee, out intFee);
            }
            return intFee / 100;
        }

        public static string TransUnionPayOrderNo(string orderId)
        {
            int lengthMerSplit = UnionPayConfig.LengthMerSplit;
            int lengthOrderSplit = UnionPayConfig.LengthOrderSplit;
            int lengthTotalOrder = UnionPayConfig.LengthTotalOrder;
            int leftLength = lengthTotalOrder - lengthMerSplit - lengthOrderSplit;
            char Zero = UnionPayConfig.Zero;

            string merId = UnionPayConfig.MerId;

            string transMerid = merId.Substring(merId.Length - lengthMerSplit, lengthMerSplit);

            string orderIdLeft = string.Empty;
            string orderIdRight = string.Empty;

            if (orderId.Length <= lengthOrderSplit)
            {
                orderIdLeft = string.Empty.PadLeft(leftLength, Zero);
                orderIdRight = orderId.PadLeft(lengthOrderSplit, Zero);
            }
            else
            {
                orderIdLeft = orderId.Substring(0, orderId.Length - lengthOrderSplit).PadLeft(leftLength, Zero);
                orderIdRight = orderId.Substring(orderId.Length - lengthOrderSplit, lengthOrderSplit);

            }
            return orderIdLeft + transMerid + orderIdRight;
        }

        public static int OutTransUnionPayOrderNo(string sourceOrderNo)
        {
            
            int lengthMerSplit = UnionPayConfig.LengthMerSplit;//5
            int lengthOrderSplit = UnionPayConfig.LengthOrderSplit;//7
            int lengthTotalOrder = UnionPayConfig.LengthTotalOrder;//16

                if (sourceOrderNo.Length!=lengthTotalOrder)
                {
                    return 0;
                }

            int leftOrderLength = lengthTotalOrder - lengthMerSplit - lengthOrderSplit; //4
            int rightOrderLength = lengthTotalOrder - lengthOrderSplit; //9

            sourceOrderNo = sourceOrderNo.Substring(0, leftOrderLength) + sourceOrderNo.Substring(rightOrderLength);

            int numberOrderNumber = 0;
            if (!string.IsNullOrWhiteSpace(sourceOrderNo))
            {
                int.TryParse(sourceOrderNo, out numberOrderNumber);
            }

            return numberOrderNumber;

        }

    }
}