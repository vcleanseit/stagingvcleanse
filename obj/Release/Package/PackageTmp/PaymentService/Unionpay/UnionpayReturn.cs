﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

using VCleanse.Shop.Host.PaymentService.Unionpay.com;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Payment;

namespace VCleanse.Shop.Host.PaymentService.Unionpay
{
    public class UnionpayReturn
    {
        public static UnionPayItem getUnionPayItem(SortedDictionary<string, string> sPara)
        {
            UnionPayItem unionPayItem = new UnionPayItem();

            if (sPara.Count > 0
                && !string.IsNullOrEmpty(sPara["merid"])
                && !string.IsNullOrEmpty(sPara["orderno"])
                && !string.IsNullOrEmpty(sPara["transdate"])
                && !string.IsNullOrEmpty(sPara["amount"])
                && !string.IsNullOrEmpty(sPara["currencycode"])
                && !string.IsNullOrEmpty(sPara["transtype"])
                && !string.IsNullOrEmpty(sPara["status"])
                && !string.IsNullOrEmpty(sPara["checkvalue"])
                && !string.IsNullOrEmpty(sPara["GateId"])
                && !string.IsNullOrEmpty(sPara["Priv1"]))
            {
                unionPayItem.MerId = sPara["merid"].ToString();
                unionPayItem.OrderNO = sPara["orderno"].ToString();
                unionPayItem.TransDate = sPara["transdate"].ToString();
                unionPayItem.Amount = sPara["amount"].ToString();
                unionPayItem.CurrencyCode = sPara["currencycode"].ToString();
                unionPayItem.TransType = sPara["transtype"].ToString();
                unionPayItem.Status = sPara["status"].ToString();
                unionPayItem.CheckValue = sPara["checkvalue"].ToString();
                unionPayItem.GateId = sPara["GateId"].ToString();
                unionPayItem.Priv1 = sPara["Priv1"].ToString();

            }
            else
            {
                unionPayItem = null;
            }
            return unionPayItem;

        }

        public static string UnionPayCallBack(ref string returnOrderId, UnionPayItem unionPayItem, bool IsSync)
        {
            NLogHelper.LogSplit();

            returnOrderId = "0";
            string syncTypeString = IsSync ? "同步" : "异步";

            string originValue = new JavaScriptSerializer().Serialize(unionPayItem);
            NLogHelper.LogInfo(syncTypeString + "UnionPayCallBack", originValue);

            if (unionPayItem != null && !string.IsNullOrEmpty(unionPayItem.OrderNO))//验证成功
            {
                string sMerId = unionPayItem.MerId;
                string sOrderNo = unionPayItem.OrderNO;
                string sTransDate = unionPayItem.TransDate;
                string sAmount = unionPayItem.Amount;
                string sCurrencyCode = unionPayItem.CurrencyCode;
                string sTransType = unionPayItem.TransType;
                string sStatus = unionPayItem.Status;
                string sCheckValue = unionPayItem.CheckValue;
                string sGateId = unionPayItem.GateId;
                string sPriv1 = unionPayItem.Priv1;

                int iOrderNo = UnionpayCore.OutTransUnionPayOrderNo(sOrderNo);
                decimal dfee = UnionpayCore.OutTransUnionPayAmt(sAmount);

                if (iOrderNo == 0)
                {
                    NLogHelper.LogInfo(iOrderNo.ToString(), "订单号错误！");
                    return "fail";
                }

                if (!sStatus.Equals(UnionPayStatusCode.SUCCESS, StringComparison.OrdinalIgnoreCase))
                {
                    NLogHelper.LogInfo(iOrderNo.ToString(), "支付失败 errorCode=[" + sStatus + "]");
                    return "fail";
                }

                if (!UniSignData.check(sMerId, sOrderNo, sAmount, sCurrencyCode, sTransDate, sTransType, sStatus, sCheckValue))
                {
                    NLogHelper.LogInfo(iOrderNo.ToString(), "校验错误！非法请求");
                    return "fail";
                }

                var orderInfo = PaymentHelper.getOrderInfo(iOrderNo);
                if (orderInfo == null)
                {
                    NLogHelper.LogInfo(iOrderNo.ToString(), "订单不存在");
                    return "fail";
                }
                returnOrderId = orderInfo.OrdersID.ToString();



                decimal orderInfoFee = orderInfo.PreferentialPrice + orderInfo.PreferentialLogisticsFee;
                if (!orderInfoFee.Equals(dfee))
                {
                    NLogHelper.LogInfo(returnOrderId, "价格不正确 订单=(" + orderInfoFee.ToString() + ") 实付=(" + dfee.ToString() + ")");
                    return "fail";
                }

                AliPayNotifyLogInfo log = new AliPayNotifyLogInfo()
                {
                    Body = "-",
                    BuyerEMail = "-",
                    OrderId = orderInfo.OrdersID,
                    Subject = "UNION",
                    TotalFee = dfee,
                    TradeNo = sOrderNo,
                    TradeStatus = sStatus,
                    PaymentPlatform = Constants.PaymentPlatform.UNIONPAY,
                    OriginalValue = originValue
                };


               //TODO: 已经支付判断 UCF
                //if (!(orderInfo.IsPaid == Constants.QueryOrderStatus.Paid))
                if (!PaymentHelper.getPaymentPaidStatus(orderInfo))
                {
                    PaymentHelper.setPaidLog(log);
                    PaymentHelper.setPaidConfirm(orderInfo.OrdersID, dfee);
                    NLogHelper.LogInfo(returnOrderId, syncTypeString + "支付更新成功" + orderInfo.OrdersID.ToString());
                }
                else
                {
                    NLogHelper.LogInfo(returnOrderId, syncTypeString + "已支付成功" + orderInfo.OrdersID.ToString());
                }

                return "success";
            }
            else
            {
                NLogHelper.LogInfo(returnOrderId, syncTypeString + "支付失败,无参数");
                return "fail";
            }


        }

    }
}