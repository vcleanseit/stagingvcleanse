﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using VCleanse.Shop.Host.PaymentService.Ucfpay.com;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.PaymentService.Ucfpay
{
    public class UcfpayService:IPayment
    {
        public UcfpayService(int orderId, string totalPrice)
        {
            this.OrderId = orderId;
            this.TotalPrice = totalPrice;
        }
        public override string PayHtml()
        {
            return UcfPayHtml(this.OrderId, this.TotalPrice);
        }


        private  string UcfPayHtml(int orderId, string totalPrice)
        {
            

            string out_trade_no = Security.Encrypt(orderId.ToString());//DateTime.Now.ToString("yyyyMMddHHmmss");
            string total_fee = totalPrice;


            //把请求参数打包成数组
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();
            sParaTemp.Add("mchid", UcfPayConfig.mchid);
            sParaTemp.Add("orderid", out_trade_no);
            sParaTemp.Add("vsn", UcfPayConfig.vsn);
            sParaTemp.Add("total_fee", total_fee);
            sParaTemp.Add("page_url", UcfPayConfig.page_url);
            sParaTemp.Add("server_url", UcfPayConfig.server_url);
            //sParaTemp.Add("passwdid", UcfPayConfig.passwdid);


            string pwd = sParaTemp["mchid"] + sParaTemp["orderid"] + sParaTemp["vsn"] + UcfPayConfig.passwdid + sParaTemp["total_fee"] + sParaTemp["mchid"];
            //sParaTemp.Add("md51", pwd);
            //sParaTemp.Add("md52", "7e5ddba77df6f6a81e3802ba5f7902c6");

            sParaTemp.Add("md5str", PaymentHelper.getMd5(pwd));


            string sHtmlText = Create_direct_ucfpay_by_user(sParaTemp);
            NLogHelper.LogInfo("---------支先锋表单" + orderId.ToString(), sHtmlText);
            return sHtmlText;
        }

        public static string Create_direct_ucfpay_by_user(SortedDictionary<string, string> sParaTemp)
        {
            string strButtonValue = "确认";
            //表单提交HTML数据
            string strHtml = "";

            //构造表单提交HTML数据
            strHtml = BuildFormHtmlUcfPay(sParaTemp, UcfPayConfig.postAction, "post", strButtonValue);

            return strHtml;
        }

        private static string BuildFormHtmlUcfPay(SortedDictionary<string, string> sParaTemp, string gateway, string strMethod, string strButtonValue)
        {

            StringBuilder sbHtml = new StringBuilder();

            sbHtml.Append("<form id='ucfpaysubmit' name='ucfpaysubmit' action='" + gateway + "' method='" + strMethod.ToLower().Trim() + "'>");

            foreach (KeyValuePair<string, string> temp in sParaTemp)
            {
                sbHtml.Append("<input type='hidden' name='" + temp.Key + "' value='" + temp.Value + "'/>");
            }

            //submit按钮控件请不要含有name属性
            sbHtml.Append("<input type='submit' value='" + strButtonValue + "' style='display:none;'></form>");

            sbHtml.Append("<script>document.forms['ucfpaysubmit'].submit();</script>");

            return sbHtml.ToString();
        }

    }
}