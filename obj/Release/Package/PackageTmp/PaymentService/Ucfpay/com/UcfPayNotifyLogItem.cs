﻿using System;

namespace VCleanse.Shop.Host.PaymentService.Ucfpay.com

{
    public class UcfPayNotifyLogItem
    {
        public string TradeNo { get; set; }
        public int OrderId { get; set; }
        public decimal TotalFee { get; set; }
        public string Md5Str { get; set; }
        public string TradeStatus { get; set; }
        public string OriginalValue { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
