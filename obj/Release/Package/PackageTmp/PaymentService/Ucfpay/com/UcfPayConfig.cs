﻿using System;

namespace VCleanse.Shop.Host.PaymentService.Ucfpay.com
{
    public static class UcfPayConfig
    {
        //测试
        //public static string postAction = "http://lepay.ucfpay.com:8078/fapay/paygate.action";
        //public static string mchid = "901011100003849";//商户号
        //public static string vsn = "0.0";// 版本号  0.0测试  1.0 正式
        //public static string page_url = "http://staging.vcleanse.com/Payment/UcfPayNotify"; //返回页面  同步
        //public static string server_url = "http://staging.vcleanse.com/Payment/UcfPayNotifyAsync";  //异步
        //public static string passwdid = "123456";//（和商户约定的签名码  post）
        //public static string backpaypwd = "654321";//（和商户约定的签名码 callback）

        //正式
        public static string postAction = "https://cp.ucfpay.com:58101/fapay/paygate.action";
        public static string mchid = "000000000000002";//商户号
        public static string vsn = "1.0";// 版本号  0.0测试  1.0 正式
        public static string page_url = "http://www.vcleanse.com/Payment/UcfPayNotify"; //返回页面  同步
        public static string server_url = "http://www.vcleanse.com/Payment/UcfPayNotifyAsync";  //异步
        public static string passwdid = "WEHRTVBN23GFGGH";//（和商户约定的签名码  post）
        public static string backpaypwd = "WEHRTVBN23GFGGH";//（和商户约定的签名码 callback）



    }

    public class UcfPayItem
    {
        public string OrderId { get; set; }
        public string Status { get; set; }
        public string Total_Fee { get; set; }
        public string Md5str { get; set; }


    }
}
