﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.Payment;
using VCleanse.Shop.Host.PaymentService.Ucfpay.com;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Payment;

namespace VCleanse.Shop.Host.PaymentService.Ucfpay
{
    public class UcfpayReturn
    {

        public static UcfPayItem getUcfPayItem(SortedDictionary<string, string> sPara)
        {
            UcfPayItem ucfPayItem = new UcfPayItem();
            try
            {
                if (sPara.Count > 0 && !string.IsNullOrEmpty(sPara["orderid"]) && !string.IsNullOrEmpty(sPara["status"]) &&
                       !string.IsNullOrEmpty(sPara["total_fee"]) && !string.IsNullOrEmpty(sPara["md5str"]))
                {
                    ucfPayItem.OrderId = sPara["orderid"].ToString();
                    ucfPayItem.Status = sPara["status"].ToString();
                    ucfPayItem.Total_Fee = sPara["total_fee"].ToString();
                    ucfPayItem.Md5str = sPara["md5str"].ToString();
                }
            }
            catch (Exception)
            {
                return null;
            }

            return ucfPayItem;

        }

        public static string UcfPayCallBack(ref string returnOrderId, UcfPayItem ucfPayItem, bool IsSync)
        {
            NLogHelper.LogSplit();

            string content = string.Empty;
            returnOrderId = "";
            string syncTypeString = IsSync ? "同步" : "异步";

            string originValue = new JavaScriptSerializer().Serialize(ucfPayItem);
            NLogHelper.LogInfo(syncTypeString + "UcfPayCallBack", originValue);

            if (ucfPayItem != null && !string.IsNullOrEmpty(ucfPayItem.OrderId))//验证成功
            {
                string orderid = ucfPayItem.OrderId;
                string status = ucfPayItem.Status;
                string total_fee = ucfPayItem.Total_Fee;
                string md5str = ucfPayItem.Md5str;

                decimal fee = 0;
                decimal.TryParse(total_fee, out fee);

                string pwd = orderid + status + UcfPayConfig.backpaypwd + total_fee + UcfPayConfig.mchid;


                var orderInfo = OrdersBLL.LoadOrderById(PaymentHelper.ParseOrderNumber(orderid));
                if (orderInfo == null)
                {
                    NLogHelper.LogInfo(orderid, "订单不存在");
                    content = "fail";
                    return content;
                }
                returnOrderId = orderInfo.OrdersID.ToString();
                if (!PaymentHelper.getMd5(pwd).Equals(md5str, StringComparison.OrdinalIgnoreCase))
                {
                    NLogHelper.LogInfo(returnOrderId, "MD5验证错误");
                    content = "fail";
                    return content;
                }

                decimal orderInfoFee = PaymentHelper.getPaymentPrice(orderInfo);
                if (!orderInfoFee.Equals(fee))
                {
                    NLogHelper.LogInfo(returnOrderId, "价格不正确");
                    content = "fail";
                    return content;
                }

                if (!status.Equals(UcfPayStatusCode.SUCCESS, StringComparison.OrdinalIgnoreCase))
                {
                    NLogHelper.LogInfo(returnOrderId, "支付失败！");
                    content = "fail";
                    return content;
                }
                AliPayNotifyLogInfo log = new AliPayNotifyLogInfo()
                {
                    Body = "-",
                    BuyerEMail = "-",
                    OrderId = PaymentHelper.ParseOrderNumber(orderid),
                    Subject = "-",
                    TotalFee = fee,
                    TradeNo = orderid,
                    TradeStatus = status,
                    PaymentPlatform = Constants.PaymentPlatform.UCFPAY,
                    OriginalValue = originValue
                };


                //TODO: 已经支付判断 银联
                //if (!(orderInfo.IsPaid == Constants.QueryOrderStatus.Paid))
                //{}
                if (!PaymentHelper.getPaymentPaidStatus(orderInfo))
                {
                    PaymentBLL.AlipayLog(log);
                    PaymentBLL.SetPaid(orderInfo.OrdersID, orderInfoFee);
                    NLogHelper.LogInfo(returnOrderId, syncTypeString + "支付更新成功" + orderInfo.OrdersID.ToString());
                }
                else
                {
                    NLogHelper.LogInfo(returnOrderId, syncTypeString + "支付成功" + orderInfo.OrdersID.ToString());
                }

                content = "success";
            }
            else
            {
                NLogHelper.LogInfo(returnOrderId, syncTypeString + "支付失败,无参数");
                content = "fail";
            }


            return content;
        }

    }
}