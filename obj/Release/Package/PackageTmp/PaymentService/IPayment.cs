﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.PaymentService
{
    public abstract class IPayment
    {
         public int OrderId { get; set; }
         public string TotalPrice { get; set; }
         public string StrBank { get; set; }
         public int QrType { get; set; }
         public string Ip { get; set; }

         public virtual string PayHtml()
         {
             return string.Empty;
         }
    }
}