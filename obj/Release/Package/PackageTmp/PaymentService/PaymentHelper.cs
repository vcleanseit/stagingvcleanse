﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using VCleanse.Shop.Biz;
using VCleanse.Shop.Biz.Payment;
using VCleanse.Shop.Data.Log;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Payment;

namespace VCleanse.Shop.Host.PaymentService
{
    public class PaymentHelper
    {
        public class PayErrorMsg
        {
            public static readonly int NA = 0;
            public static readonly int PRICE = 1;//支付价格不对
            public static readonly int ESPIRED = 2;//支付订单已经过期
            public static readonly int NOTEXISTS = 3;  //支付订单不存在
            public static readonly int TRADE = 4;//trade失败
            public static readonly int VERIFYERROR = 5;//验证失败
            public static readonly int NOPARAMS = 6;//无返回参数

        }
        public class AliQrType
        {
            /// <summary>
            /// 无二维码
            /// </summary>
            public static readonly int NA = -1;
            /// <summary>
            /// 简单前置模式
            /// </summary>
            public static readonly int SIMPREPOSE = 0;
            /// <summary>
            /// 前置模式
            /// </summary>
            public static readonly int PREPOSE = 1;
            /// <summary>
            /// 跳转模式
            /// </summary>
            public static readonly int SKIP = 2;
            /// <summary>
            /// mini模式
            /// </summary>
            public static readonly int MINI = 3;
        }
        public class AliPayReturnTpye
        {
            public static readonly string SUCCESS = "success";
            public static readonly string FAIL = "fail";

        }
        public static string PaySuccessUrl(string orderId)
        {
            return "/payment/paysuccess#orderid/" + orderId.ToString();
        }
        public static string PayFailUrl(string orderId, string result = "")
        {
            return string.IsNullOrEmpty(result) ? "/payment/payfailed#orderid/" + orderId : "/payment/payfailed#orderid/" + orderId + "/result/" + result;
        }

        public static int ParseOrderNumber(string original)
        {
            if (string.IsNullOrEmpty(original))
            {
                return 0;
            }
            string orderNumberString = VCleanse.Shop.Host.Utility.Security.Decrypt(original);
            int numberOrderNumber = 0;
            if (!string.IsNullOrWhiteSpace(orderNumberString))
            {
                int.TryParse(orderNumberString, out numberOrderNumber);
            }
            return numberOrderNumber;
        }

        public static string DictToString(SortedDictionary<string, string> sParaTemp)
        {
            string result = "";
            foreach (var item in sParaTemp)
            {
                result += "(" + item.Key + ":" + item.Value + ")\n";
            }
            return result;
        }
        public static string DictToStringNotSort(Dictionary<string, string> sParaTemp)
        {
            string result = "";
            foreach (var item in sParaTemp)
            {
                result += "(" + item.Key + ":" + item.Value + ")\n";
            }
            return result;
        }
        public static string getMd5(string value)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(value, "MD5").ToLower();
        }
        public static bool CheckOrderCanPayStatus(VCleanse.Shop.Model.Orders.OrderItemInfo orderInfo)
        {
            //if (orderInfo.OrderStatus == Constants.OrderStatus.Cancelled || orderInfo.OrderStatus == Constants.OrderStatus.Expired)
            //{
            //    return false;
            //}
            return OrderStatusHelper.checkOrderCanPaid(orderInfo);
        }
        public static VCleanse.Shop.Model.Orders.OrderItemInfo getOrderInfo(int orderId)
        {
            return VCleanse.Shop.Biz.OrdersBLL.LoadOrderById(orderId);
        }
        public static decimal getPaymentPrice(VCleanse.Shop.Model.Orders.OrderItemInfo orderInfo)
        {
            OrderPayment payment = getPaymentOnline(orderInfo);
            if (payment == null)
                return 0;
            else
                return payment.Payment;
        }
        public static bool getPaymentPaidStatus(VCleanse.Shop.Model.Orders.OrderItemInfo orderInfo)
        {
            OrderPayment payment = getPaymentOnline(orderInfo);
            if (payment == null)
                return false;
            else
                return payment.IsPaid;
        }
        public static OrderPayment getPaymentOnline(VCleanse.Shop.Model.Orders.OrderItemInfo orderInfo)
        {
            OrderPayment payment = OrdersBLL.LoadOrderPaymentIsOnlinePay(orderInfo.OrdersID);
            return payment;
        }

        public static void setPaidLog(AliPayNotifyLogInfo log)
        {
            PaymentBLL.AlipayLog(log);
        }
        public static void setPaidConfirm(int orderId, decimal payment)
        {
            PaymentBLL.SetPaid(orderId, payment);
        }
        public static void GetReturnLogStart(string title, string message)
        {
            NLogHelper.LogInfo(title + "-----start-------", message);
        }
        public static void GetReturnLogEnd(string title, int orderId, string content, string result)
        {
            NLogHelper.LogInfo(title + " - -----end-------订单号=(" + orderId.ToString() + ")", "回调:(" + content + ")结果:(" + result + ")");
        }

        public static string getLogContext()
        {
            return HttpHelper.getLogContext();
        }

        public static string getUrl(string url)
        {
            return "<script type='text/javascript'>window.parent.location.href='" + url + "';</script>";
        }
    }
}