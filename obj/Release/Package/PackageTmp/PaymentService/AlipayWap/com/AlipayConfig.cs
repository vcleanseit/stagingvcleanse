﻿using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;

namespace VCleanse.Shop.Host.PaymentService.AlipayWap.com
{
    /// <summary>
    /// 类名：Config
    /// 功能：基础配置类
    /// 详细：设置帐户有关信息及返回路径
    /// 版本：3.3
    /// 日期：2012-07-05
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// 
    /// 如何获取安全校验码和合作身份者ID
    /// 1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
    /// 2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
    /// 3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”
    /// </summary>
    public class Config
    {
        #region 字段
        private static string partner = "";
        private static string key = "";
        private static string private_key = "";
        private static string public_key = "";
        private static string input_charset = "";
        private static string sign_type = "";

        private static string seller_email = "";

       
        private static string return_url = "";

       
        private static string notify_url = "";

        private static string gateway_new = "";

     
        


        #endregion

        static Config()
        {
            //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            /*
            //合作身份者ID，以2088开头由16位纯数字组成的字符串
            partner = "2088801859214778";

            //交易安全检验码，由数字和字母组成的32位字符串
            key = "qc02kwpngaxil6ergosqvgdp92cetq8o";

            //签约支付宝账号或卖家支付宝帐户
            seller_email = "payment@vcleanse.com";
            */
            //合作身份者ID，以2088开头由16位纯数字组成的字符串
            partner = "2088801859214778";

            //交易安全检验码，由数字和字母组成的32位字符串
            key = "qc02kwpngaxil6ergosqvgdp92cetq8o";

            //签约支付宝账号或卖家支付宝帐户
            seller_email = "payment@vcleanse.com";
            
            return_url = string.Format("{0}/payment/PayWapReturn", VCleanse.Shop.Biz.Shared.Config.Domain);

            notify_url = string.Format("{0}/payment/PayWapNotify", VCleanse.Shop.Biz.Shared.Config.Domain);

            gateway_new = "https://mapi.alipay.com/gateway.do?";
            /*
            //商户的私钥
            //如果签名方式设置为“0001”时，请设置该参数
            private_key = @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALSqBMSbeB09kEtpEGhaOvRCAALxFO6u8WSSUvflavhQk8HTJxiHV9nDpDMvXU+AfVK1Anw37yoPATAlC+lDtm4OdkxmUd1NwGOmiphkIBcz4Axgs9IfVIrVOew0AbDvowCXDXlKgE8lwQbOLb3hFBWJkK/LCNu5s58ecLvpa4G9AgMBAAECgYBxYGEtoQN4FjJ/oKeTVUy6DNFFB1ZDk7YZj1pSB79hpe+UK5bVZdpT5tsc5BSC1/fN95pfqhqFInV0aUtYUA4jY6Tq11Q1QP0KVITptVunFUhw61UB79Zvt2sTdeEQ63q581UjZNgzx8UPdoUTAgpBRd/F2dQtnOllARyLKbzwAQJBAO9rb7+XK8EXE0zQxGOa8WncXXE256cSZFIPcJL5670yb5hgg9+7vyLImaNzxFUyfAqmQ+uKGeaKYRxB/ygiRT0CQQDBLO6AZHf0BenPsl6rSUP1o3B313Yqn95r5Nm8sElgYOWPY0TUNn3JWfspgMy8WXy2h/O0tTy2yKBjrEut9vaBAkEA0bkK6olBiqLsBR3a6mWPk2u1brbptTAnMLrVoenK1rCXkKL64cFUbOPLsaJ3GSpf/fQnCuyuQ4R0DIzDNiTPAQJAH1v6787fhja7iSMMqUPAi4zXwiyxGyROyDcjfbUkhEfWnFtiHg0miFxaIJevJNhmHX+eeXR+gbG6D9Q1iO0xgQJAJj/045VWxpqDcnpstXrifjNYEjn8WorGisXb2LVHRrYC5aUbfZ6RMLAw7mfVhnO4r/S0nk7mVK3zDqE3+vlbjg==";

            //支付宝的公钥
            //如果签名方式设置为“0001”时，请设置该参数
            public_key = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCXNAQlgx4y3DAK6en8tI5CmrxcfiZh8sNIXq6Eak6ENRGCwtvzD2eVuOIwo2/S6XYx9xvxmocR+e9n0xEKUZfaHcI7/gGDn7GCv7CKU2hocSovRJ51JbR7w4u+TxOU/LdfzeiPSyigiIWLklESpnLFRe+3CEB68tcPueE9fZRP6QIDAQAB";
*/
            
            //商户的私钥
            //如果签名方式设置为“0001”时，请设置该参数
            private_key = @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJn8hwg5NBEz+qQJjTFgixYQ5lihpE3Po1+8As4erlAEqGhCRzAZjvZS/doU3/dshyJtbi/CbXwrpEWeKXEREQjhefJ/Od2VI+l0viO/4Qfnfwx211I6Hs956G5gMuTt25ZVa7zEgz4BNSJ6s9Nv46Unl7/NYNJq/gzqN42VjVEfAgMBAAECgYB3W52TSzvHpeG+qsStpGbK+NiDlO+VfL/nIgWOnYuB7MpY3h7t8rG0z0mDdDDSSWZJ0kbZcDyLRgy0Bc+bm532b21IHNpe9d29GoXztFXvlOJqLFhNcD5iEp2iDlia9zQFhFduv0CWwHeHvt1+6ry8pbXFdzBinrPKeFvM//3xUQJBAMpRoEpnRw6lkvBPxgA12AUe9DmTbDlgAhMdcjA8j4Y2zyURJHUfpyQjfJ7R7u8f+BNecDvK/XqksI2oYItHiacCQQDC1/Q4NOn85G7Gp42Og/mw08uqcWQNalq35QK1YkcIg3+zwiAYXIJDDR8SPzmDOvvMXS0DZHIqMe+nh7SkqXvJAkEAxGg1mcVlH/zxBsYKy815po8EPJi3aYOgv/nIDrfcBUlUEwHvSJyv389rfasDr7RzWipF/mXzaYpTs1GQV+Xc5QJAA/H5ePXhUwSRFaao4Bzd/m2bbx0SqI7hvV1+u4t84rg+6rXgjn25CYCHg4gSMw0Lsiykz6/F/AK81oiR6M+huQJAdrqFHx1WykBeSgSuWNx89FsVLspW1P7BKyBuxwxTU7ooFoAy8cakPBnMNrb4czkvA90yVeziAcR80lOm5q2/YA==";

            //支付宝的公钥
            //如果签名方式设置为“0001”时，请设置该参数
            public_key = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiVV2vI/8JFjY398yutBJWJR9SArC0wu1kexSfTB2X+atVFo5kDfadIP4Cd22bqPGHaXMel9KZmrkIFcwrEU3jy/tsdOXejg2sQgul69uUNysVirqETjjhCrU5IK5mZsFLaWFd/1fKwKHru/YzW0ImcXZSSGnzdWU7xVkrl3eZxwIDAQAB";
            
            //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑



            //字符编码格式 目前支持 utf-8
            input_charset = "utf-8";

            //签名方式，选择项：0001(RSA)、MD5
            sign_type = "0001";// "0001";
            //无线的产品中，签名方式为rsa时，sign_type需赋值为0001而不是RSA
        }

        #region 属性
        /// <summary>
        /// 获取或设置合作者身份ID
        /// </summary>
        public static string Partner
        {
            get { return partner; }
            set { partner = value; }
        }

        /// <summary>
        /// 获取或设交易安全校验码
        /// </summary>
        public static string Key
        {
            get { return key; }
            set { key = value; }
        }

        /// <summary>
        /// 获取或设置商户的私钥
        /// </summary>
        public static string Private_key
        {
            get { return private_key; }
            set { private_key = value; }
        }

        /// <summary>
        /// 获取或设置支付宝的公钥
        /// </summary>
        public static string Public_key
        {
            get { return public_key; }
            set { public_key = value; }
        }

        /// <summary>
        /// 获取字符编码格式
        /// </summary>
        public static string Input_charset
        {
            get { return input_charset; }
        }

        /// <summary>
        /// 获取签名方式
        /// </summary>
        public static string Sign_type
        {
            get { return sign_type; }
        }


        public static string Seller_email
        {
            get { return Config.seller_email; }
            set { Config.seller_email = value; }
        }
        public static string Return_url
        {
            get { return Config.return_url; }
            set { Config.return_url = value; }
        }
        public static string Notify_url
        {
            get { return Config.notify_url; }
            set { Config.notify_url = value; }
        }

           public static string Gateway_new
        {
            get { return Config.gateway_new; }
            set { Config.gateway_new = value; }
        }
        #endregion
    }
}