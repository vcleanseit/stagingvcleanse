﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.MVCFilter
{
    public enum ViewCacheControlType : byte
    {
        Unknown = 0,
        NoCache = 1,
        CacheByParam = 2,
    }

    public class OutputCacheFilterAttribute : ActionFilterAttribute
    {
        protected TimeSpan _ReleaseCycle;


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            HttpResponseBase response = filterContext.HttpContext.Response;
            _ReleaseCycle = new TimeSpan(0, 0, CacheSeconds);

            switch (ViewCacheControl)
            {
                case ViewCacheControlType.NoCache:
                    SetNoCache(response);
                    break;
                case ViewCacheControlType.CacheByParam:
                    SetCacheByParam(response);
                    break;
            }
        }

        protected virtual void SetCacheParams(HttpResponseBase response)
        {
            response.Cache.VaryByParams["*"] = true;
        }

        private void SetNoCache(HttpResponseBase response)
        {
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.Cache.SetExpires(DateTime.Now.AddDays(-365));
            response.Cache.SetMaxAge(TimeSpan.Zero);
        }

        protected virtual void SetCacheByParam(HttpResponseBase response)
        {
            response.Cache.SetCacheability(HttpCacheability.Public);
            response.Cache.SetExpires(DateTime.Now.Add(_ReleaseCycle));
            response.Cache.SetLastModified(DateTime.Now);
            response.Cache.SetMaxAge(_ReleaseCycle);
            response.Cache.SetOmitVaryStar(true);
            response.Cache.SetValidUntilExpires(true);

            SetCacheParams(response);
        }


        [DefaultValue(ViewCacheControlType.Unknown)]
        public ViewCacheControlType ViewCacheControl
        {
            get;
            set;
        }


        private int _cacheSeconds = 0;
        [DefaultValue(21 * 24 * 60 * 60)]
        public int CacheSeconds
        {
            get
            {
                return _cacheSeconds <= 0 ? 21 * 24 * 60 * 60 : _cacheSeconds;
            }
            set
            {
                _cacheSeconds = value;
            }
        }
    }
}