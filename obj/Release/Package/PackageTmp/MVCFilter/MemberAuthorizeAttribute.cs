﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Controllers;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.MVCFilter
{
    public class MemberAuthorizeAttribute : AuthorizeAttribute
    {
        public bool ToLogin
        {
            get;
            set;
        }

        public bool AllowImpersonate
        {
            get;
            set;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            VcleanseContext context = VcleanseContext.Current;

            return (context != null && (context.IsUserLogined || (AllowImpersonate && context.Impersonating)));
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (ToLogin)
            {
                string stringReferrer = HttpContext.Current.Request.RawUrl;
                filterContext.Result = new RedirectResult("~/Enrollment/Signin?referrer=" + stringReferrer);
            }
            else
            {
                filterContext.Result = new MemberUnauthorizedResult();
            }
        }
    }


    public class MemberUnauthorizedResult : ActionResult
    {

        public MemberUnauthorizedResult()
        {
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            var routeData = new RouteData();
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = "401";
            IController controller = new ErrorController();
            controller.Execute(new RequestContext(context.HttpContext, routeData));
        }
    }
}