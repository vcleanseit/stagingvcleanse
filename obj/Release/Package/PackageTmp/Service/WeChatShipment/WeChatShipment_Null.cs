﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public class WeChatShipment_Null : IWeChatShipmentService
    {
        protected string title;
        protected string msg;

        public WeChatShipment_Null(string title, string msg)
        {
            this.title = title;
            this.msg = msg;
        }

        public WeChatExpressShowInfo GetShipmentInfo(string DeliveryNo, string storeId)
        {
            return new WeChatExpressShowInfo()
            {
                Title = title,
                Title_Lbl = "物流公司",
                DeliverNo = DeliveryNo,
                DeliverOrAddress = msg,
                DeliverOrAddress_Lbl = "提示信息",
                DeliveryReceiver = ""
            };
        }
    }
}