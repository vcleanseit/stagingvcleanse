﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public interface IWeChatShipmentService
    {
        /// <summary>
        /// 创建微信快递信息页面显示的快递单跟踪
        /// </summary>
        /// <param name="DeliveryNo">快递单编号</param>
        /// <returns></returns>
        WeChatExpressShowInfo GetShipmentInfo(string DeliveryNo, string storeId);
    }
}