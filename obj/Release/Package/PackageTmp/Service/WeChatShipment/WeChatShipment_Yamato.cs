﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{
    public class WeChatShipment_Yamato : IWeChatShipmentService
    {
        public const string EXPRESS_COMPANY_NAME = "黑猫快递";

        public WeChatExpressShowInfo GetShipmentInfo(string DeliveryNo, string storeId)
        {
            WeChatExpressShowInfo result = new WeChatExpressShowInfo()
            {
                Title_Lbl = "物流公司",
                Title = EXPRESS_COMPANY_NAME,
                DeliverNo = DeliveryNo,
                DeliverOrAddress = string.Format("请访问<a href=\"{0}\">黑猫官网</a>进行单据查询。", "http://sh.cn.ta-q-bin.com/"),
                DeliverOrAddress_Lbl = "官方网站",
            };
            return result;
        }
    }
}