﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VCleanse.Shop.Host.Service.WeChatShipment
{    
    public class WeChatShipmentFactory
    {
        /// <summary>
        /// 需要改经，如果有IOC，用IOC方式更好。没有的话也尽量应该用枚举方式判断，不知道有无快递公司枚举
        /// Alan 2014-8-5 10：00AM
        /// </summary>
        public static IWeChatShipmentService GetShipment(string CompanyId)
        {
            switch (CompanyId)
            {
                case "1":
                    return new WeChatShipment_Yamato();
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                    return new WeChatShipment_SF();
                default :
                    return new WeChatShipment_DIY();
            }
        }
    }
}