﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Xml;
using System.Reflection;

namespace VCleanse.Shop.Host.Service
{
    public class SystemSwitch
    {
        private static readonly object lockObj = new object();
        
        protected const string SYS_SWITCH_XML = @"configFiles\systemSwitch.xml";
        protected const string SYS_SWITCH = "systemSwitch";
        private SystemSwitch() 
        {
            
        }

        public static string SwitchSystem(string userAgent)
        {
            SystemSwitchEntity entity = (SystemSwitchEntity)HttpContext.Current.Cache[SYS_SWITCH];
            if (entity == null)
                entity = GetSwitchEntity();
            if (entity.switchUrls.Contains(HttpContext.Current.Request.Url.AbsolutePath))
            {
                var result = entity.excludeKeys.FirstOrDefault(p => userAgent.ToLower().Contains(p.ToLower()));
                if (result != null)
                    return string.Empty;
                else
                {
                    result = entity.mobileKeys.FirstOrDefault(p => userAgent.ToLower().Contains(p.ToLower()));
                    if(result != null)
                        return entity.switchTo + HttpContext.Current.Request.Url.PathAndQuery;
                    else
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        private static SystemSwitchEntity GetSwitchEntity()
        {
            if (HttpContext.Current.Cache[SYS_SWITCH] == null)
            {
                lock (lockObj)
                {
                    if (HttpContext.Current.Cache[SYS_SWITCH] == null)
                    {
                        createCache();
                    }
                }
            }
            return (SystemSwitchEntity)HttpContext.Current.Cache[SYS_SWITCH];
        }

        private static void createCache()
        {
            string xmlPath = HttpRuntime.AppDomainAppPath + SYS_SWITCH_XML;
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(xmlPath))
            {
                xmlDoc.Load(xmlPath);               
                string assemblyName = xmlDoc.SelectSingleNode("/mobileswitch/switchTo/assembly").InnerText;
                string nameSpace = xmlDoc.SelectSingleNode("/mobileswitch/switchTo/namespace").InnerText;
                string className = xmlDoc.SelectSingleNode("/mobileswitch/switchTo/class").InnerText;
                string fieldName = xmlDoc.SelectSingleNode("/mobileswitch/switchTo/field").InnerText;

                Assembly assembly = Assembly.Load(assemblyName);
                Type fieldType = assembly.GetType(string.Format("{0}.{1}",nameSpace,className));
                string switchTo = fieldType.GetField(fieldName).GetValue(null).ToString();
                List<string> excludeKeys = new List<string>();
                List<string> switchUrls = new List<string>();
                List<string> mobileKeys = new List<string>();
                XmlNode excludeKeysNode = xmlDoc.SelectSingleNode("/mobileswitch/excludeKeys");
                XmlNode mobileKeysNode = xmlDoc.SelectSingleNode("/mobileswitch/mobileKeys");
                XmlNode switchUrlNode = xmlDoc.SelectSingleNode("/mobileswitch/switchUrl");

                foreach (XmlNode keyNode in excludeKeysNode)
                    excludeKeys.Add(keyNode.InnerText);
                foreach (XmlNode keyNode in switchUrlNode)
                    switchUrls.Add(keyNode.InnerText);
                foreach (XmlNode keyNode in mobileKeysNode)
                    mobileKeys.Add(keyNode.InnerText);

                SystemSwitchEntity entity = new SystemSwitchEntity(){
                    switchTo = switchTo,
                    excludeKeys = excludeKeys,
                    switchUrls = switchUrls,
                    mobileKeys = mobileKeys,
                };
                CacheDependency fileDependency = new CacheDependency(xmlPath);
                HttpContext.Current.Cache.Insert(SYS_SWITCH, entity, fileDependency, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
            else
                throw new Exception("can't find systemswitch.xml config!");
        }
    }
}