﻿using System.Web.Mvc;

namespace VCleanse.Shop.Host.Areas.Mobile
{
    public class MobileAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Mobile";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Mobile_default",
                "m/{controller}/{action}/{id}",
                new { controller = "cleanse", action = "Index", id = UrlParameter.Optional },
                new string[] { "VCleanse.Shop.Host.Areas.Mobile.Controllers" }
            );
        }
    }
}
