﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Model.cfc;
using VCleanse.Shop.Biz._Legacy.cfc;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class OrderController : BaseController
    {
        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Receiver()
        {
            return View("Receiver");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Confirm()
        {
            return View("Confirm");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Success(int orderId)
        {
            /*  cfc close 2014-12-03
            cfcModel cfc = cfcHelper.CheckCfc(orderId);

            if (cfc != null)
            {
                return Redirect("/m/paws/Certificate?orderid=" + cfc.OrderId);
            }
            */
            return View("Success");
        }

    }
}
