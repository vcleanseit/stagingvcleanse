﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Mobile/Account/

        public ActionResult Index()
        {
            return View();
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Orders()
        {
            return View("Orders");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Orderdetail()
        {
            return View("Orderdetail");
        }

        
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Id);
            HttpHelper.ClearCookie(HttpContext, WebConstants.CookieKey.Session);
            return RedirectToAction("home", "intl");
        }
    }
}
