﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Model.Tracking;

namespace VCleanse.Shop.Host.Areas.Mobile.Controllers
{
    public class BaseController : Controller
    {
        protected VcleanseContext CurrentContext
        {
            get
            {
                return VcleanseContext.Current;
            }
        }
    }
}
