$(document).ready(function (){
	
	// 星星选择器
	$(".star-selection img").click(function(e){
		$sele = $(this).parent();
		var currValue = $(this).attr("data-value");		// 当前点击星的值
		
		var arrStars = $sele.find("img");
		for(var i=0;i<arrStars.length;i++){	//循环每颗星
			if(i < currValue){	//如果比当前值小则显示绿色
			    $(arrStars[i]).attr("src", "/Areas/Mobile/Images/Feedback/star-green.svg");
			}else{					//如果比当前值大则显示灰色
			    $(arrStars[i]).attr("src", "/Areas/Mobile/Images/Feedback/star-gray.svg");
			}
		}
		
		$sele.attr("data-value",currValue);	// 设置本参数的值
	})
	
	// 会不会选择器
	$(".order-selection button").click(function(e){
		$sele = $(this).parent();
		var currValue = $(this).attr("data-value");
		
		var arrButtons = $sele.find("button");
		for(var i=0;i<arrButtons.length;i++){	
			$(arrButtons[i]).removeClass("active");
		}
		
		$(this).addClass("active");
		
		$sele.attr("data-value",currValue);	// 设置本参数的值
	})
	
	
	// 提交
	$("#submit-button").click(function(e){
		var isVaild = true;
		
		var taste = $("#taste").attr("data-value");
		var effect = $("#effect").attr("data-value");
		var delivery = $("#delivery").attr("data-value");
		var again = $("#again").attr("data-value");
		var orderID = $("#orderID").attr("value");
		if(taste == 0){
			showMessage("给[口感]打个分吧");
			hideMessage(1000);
			isVaild = false;
		}else if(effect == 0){
			showMessage("给[效果]打个分吧");
			hideMessage(1000);
			isVaild = false;
		}else if(delivery == 0){
			showMessage("给[配送]打个分吧");
			hideMessage(1000);
			isVaild = false;
		} else if (orderID == "") {
		    showMessage("请重新打开连接");
		    hideMessage(2000);
		    isVaild = false;
		}
		var postValue = {
				"taste":taste,
				"effect":effect,
				"delivery":delivery,
				"again": again,
				"orderID": orderID
		}
		if(isVaild){
			//提交喽
			showMessage("提交中...");
			$.ajax({
			    url: "http://www.vcleanse.com/m/feedback/Submit",
			    data: postValue,
			    dataType: 'json',			    
			    type: 'post',
			    async: false,
			    success: function (data) {
			        window.location.href = "http://www.vcleanse.com/m/feedback/PromotionCode?Code=" + data.PromotionCode;
			    }, failure: function () {
			        showMessage("提交失败");
			    }			    
			})
           
		}
	})
})

function showMessage(msg){
	$("#message-text").html(msg);
	$(".lay-message").show();
}

function hideMessage(cd){
	if(cd > 0){
		setTimeout('$(".lay-message").hide();',cd);
	}else{
		$(".lay-message").hide();		
	}
	
}