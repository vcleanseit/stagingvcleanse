﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class CleanseController : BaseController
    {
        //
        // GET: /Cleanse/

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Detail()
        {
            return View("Detail");
        }
        public ActionResult DetailTest()
        {
            return View("DetailTest");
        }

        public ActionResult Customize()
        {
            return View("customize");
        }

    }
}
