﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class IntlController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Home");
        }

        // For old website users(add to favorite icon)
        public ActionResult Home()
        {
            return Redirect("/");
        }

        public ActionResult About()
        {
            return View("About");
        }

        public ActionResult Codepressed()
        {
            return View("Codepressed");
        }

        public ActionResult Questions()
        {
            return View("Questions");
        }

        public ActionResult Theory()
        {
            return View("Theory");
        }

        public ActionResult Announce()
        {
            return View("Announce");
        }

        public ActionResult Map()
        {
            return View("map");
        }
    }
}