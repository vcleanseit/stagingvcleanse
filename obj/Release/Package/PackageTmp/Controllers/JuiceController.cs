﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class JuiceController : BaseController
    {
        //
        // GET: /Juice/

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Detail()
        {
            return View("Detail");
        }

        public ActionResult chailatte()
        {
            return View("chailatte");
        }
        public ActionResult chiagreen()
        {
            return View("chiagreen");
        }
        public ActionResult classicblack()
        {
            return View("classicblack");
        }
        public ActionResult classicgreen()
        {
            return View("classicgreen");
        }
        public ActionResult classicwhite()
        {
            return View("classicwhite");
        }
        public ActionResult cranberrymilk()
        {
            return View("cranberrymilk");
        }
        public ActionResult darkgreen()
        {
            return View("darkgreen");
        }
        public ActionResult darkwhite()
        {
            return View("darkwhite");
        }
        public ActionResult darkyellow()
        {
            return View("darkyellow");
        }
        public ActionResult easygreen()
        {
            return View("easygreen");
        }
        public ActionResult fragrantclear()
        {
            return View("fragrantclear");
        }
        public ActionResult fragrantred()
        {
            return View("fragrantred");
        }
        public ActionResult gingercarrotsoup()
        {
            return View("gingercarrotsoup");
        }
        public ActionResult Greengoddesssoup()
        {
            return View("Greengoddesssoup");
        }
        public ActionResult hibiscusinfusion()
        {
            return View("hibiscusinfusion");
        }
        public ActionResult intensegreen()
        {
            return View("intensegreen");
        }
        public ActionResult luohanguoinfusion()
        {
            return View("luohanguoinfusion");
        }
        public ActionResult matchamilk()
        {
            return View("matchamilk");
        }
        public ActionResult mushroomsoup()
        {
            return View("mushroomsoup");
        }
        public ActionResult passionateyellow()
        {
            return View("passionateyellow");
        }
        public ActionResult passionfruit()
        {
            return View("passionfruit");
        }
        public ActionResult pearandginger()
        {
            return View("pearandginger");
        }
        public ActionResult persimmoninfusion()
        {
            return View("persimmoninfusion");
        }
        public ActionResult purplebroth()
        {
            return View("purplebroth");
        }
        public ActionResult redbeetsoup()
        {
            return View("redbeetsoup");
        }
        public ActionResult spicylemonade()
        {
            return View("spicylemonade");
        }
        public ActionResult splitpeasoup()
        {
            return View("splitpeasoup");
        }
        public ActionResult strawberryvanilla()
        {
            return View("strawberryvanilla");
        }
        public ActionResult sweetpotatopumpkinsoup()
        {
            return View("sweetpotatopumpkinsoup");
        }
        public ActionResult vlatte()
        {
            return View("vlatte");
        }
        public ActionResult easyred()
        {
            return View("easyred");
        }
        public ActionResult bittergreen()
        {
            return View("bittergreen");
        }
        public ActionResult blacklemonade()
        {
            return View("blacklemonade");
        }
        public ActionResult fragrantwhite()
        {
            return View("fragrantwhite");
        }
    }
}
