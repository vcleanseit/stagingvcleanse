﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Host.Service;
namespace VCleanse.Shop.Host.Controllers
{
    public class DefaultController : BaseController
    {
        public ActionResult Index()
        {
            return View("index");
        }
    }
}
