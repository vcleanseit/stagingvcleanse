﻿using System.Web.Mvc;

namespace VCleanse.Shop.Host.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            int id = ParseOrderNumber("138810DDC301CDE5");
            return Json(new { ID = id }, JsonRequestBehavior.AllowGet);
            //return RedirectToActionPermanent("Package", "Order");

        }

        /// <summary>
        /// 解析序列号
        /// </summary>
        /// <param name="original"></param>
        /// <returns></returns>
        private int ParseOrderNumber(string original)
        {
            string orderNumberString = VCleanse.Shop.Host.Utility.Security.Decrypt(original);
            int numberOrderNumber = 0;
            if (!string.IsNullOrWhiteSpace(orderNumberString))
            {
                int.TryParse(orderNumberString, out numberOrderNumber);
            }
            return numberOrderNumber;
        }
    }
}
