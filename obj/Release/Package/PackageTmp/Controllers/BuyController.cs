﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Packages;
using VCleanse.Shop.Biz.Promotion;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Members;
using VCleanse.Shop.Model.Promotions;
using VCleanse.Shop.Model.Shared;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Host.Context;
using VCleanse.Shop.Host.Models.OrderModel;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Biz;
using OrderMDL = VCleanse.Shop.Model.Orders;
using VCleanse.Shop.Model.Tracking;
using System.Web.Script.Serialization;
using VCleanse.Shop.Biz.GG;
using VCleanse.Shop.Model.GG;
using VCleanse.Shop.Biz.Log;
using VCleanse.Shop.Model.WeChatOrders;
using VCleanse.Shop.Host.Utility;
using System.Net;
using System.IO;
using System.Text;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Model.cfc;
using VCleanse.Shop.Biz._Legacy.cfc;
using VCleanse.Shop.Data.Log;
using VCleanse.Shop.Model.CityArea;
using VCleanse.Shop.Model._Legacy.Orders;
using VCleanse.Shop.Model.SMS;
using VCleanse.Shop.Model.Packages;

namespace VCleanse.Shop.Host.Controllers
{
    public class BuyController : BaseController
    {
        // GET api/<controller>
        public ActionResult Index()
        {
            return Redirect("/buy/index");
        }

        [MemberAuthorize(AllowImpersonate = true)]
        public ActionResult Orderinfo()
        {
            return View("orderinfo");
        }
        #region <<计算价格>>
        [MemberAuthorize(AllowImpersonate = true)]
        [HttpPost]
        public ActionResult Summary(SummaryInfo summaryItem)
        {         

            #region <<验证客户端订单信息正确性>>
            //写入日志
            NLogHelper.LogInfo("OrderParameters", new JavaScriptSerializer().Serialize(summaryItem));
            if (summaryItem == null)
            {
                return Json
                   (new
                   {
                       IsSuccess = true,
                       TotalPrice = 0,
                       PreferentialPrice = 0,
                       DeliverFee = 0,
                       PreferentialLogisticsFee = 0,
                       Total = 0,
                       StatusCodes = 13
                   });
            }
            if (summaryItem.DeliveryList == null || summaryItem.DeliveryList.Count == 0)
            {
                return Json
                  (new
                  {
                      IsSuccess = true,
                      TotalPrice = 0,
                      PreferentialPrice = 0,
                      DeliverFee = 0,
                      PreferentialLogisticsFee = 0,
                      Total = 0,
                      StatusCodes = 13
                  });
            }


            #endregion

            #region <<计算运费>>


            decimal DeliverFees = 0;
            if (summaryItem.DeliveryList[0].IsSelfPickup == true)
            {
                DeliverFees = 0;
            }
            else
            {
                decimal AreaFreight = OrdersBLL.getCityAreaFreight(int.Parse(summaryItem.DeliveryList[0].AreaID));
                int Days = summaryItem.DeliveryList.Count;
                DeliverFees = AreaFreight * Days;
            }
            #endregion

            #region <<计算果汁费用>>
            var productList = PackageBLL.LoadAllProducts();

            decimal TotalPrices = 0;
            decimal PreferentialPrices = 0;
            decimal PreferentialLogisticsFees = 0;
            int iRuleId = 0;
            ///套餐内价格
            if (summaryItem.DeliveryList[0].PackageRuleId != 0 && summaryItem.DeliveryList[0].PackageRuleCount != 0)
            {
                var rules = PackageBLL.LoadAllPackages()
                   .SelectMany(p => p.PackageGroups)
                   .SelectMany(p => p.DayRules)
                   .SelectMany(p => p.GroupRules);
                var rule = rules.Where(p => summaryItem.DeliveryList[0].PackageRuleId.Equals(p.ID)).FirstOrDefault();

                decimal PackagePrice = rule.PackagePrice;
                foreach (var item in summaryItem.DeliveryList)
                {
                    TotalPrices += PackagePrice * item.PackageRuleCount;
                }

            }
            ///套餐外价格
            foreach (var item in summaryItem.DeliveryList)
            {
                if (item.ProductList != null && item.ProductList.Count != 0)
                {
                    foreach (var o in item.ProductList)
                    {
                        TotalPrices += int.Parse(o.Count) * productList.FirstOrDefault(p => p.ProductsID == int.Parse(o.Id)).ProductsPrice;
                    }
                }
            }
            #endregion

            #region <<优惠券使用>>
            int StatusCode = 0;
            PreferentialPrices = TotalPrices;
            PreferentialLogisticsFees = DeliverFees;
            if (summaryItem.PromotionCode != null && summaryItem.PromotionCode != "" && summaryItem.PromotionCode != "undefind")
            {

                var sPromotionCode = summaryItem.PromotionCode;

                PromotionRuleInfo promotionRule = PromotionBLL.LoadAllPromotionRulesByCode(sPromotionCode);
                StatusCode = ValidatePromotionRule(promotionRule);
                if (StatusCode == 0)
                {
                    //总价要满足满减要求
                    if (TotalPrices < promotionRule.StartAmount)
                    {
                        StatusCode = Constants.Promotion.InvalidStartAmount;
                    }
                    if (StatusCode == 0)
                    {
                        switch (promotionRule.RuleId)
                        {
                            case Constants.PromotionRuleType.Deduction:
                                PreferentialPrices -= promotionRule.RuleValue;
                                iRuleId = promotionRule.RuleId;
                                break;
                            case Constants.PromotionRuleType.DeliveryFree:
                                PreferentialLogisticsFees = 0;
                                iRuleId = promotionRule.RuleId;
                                break;
                            case Constants.PromotionRuleType.Percentage:
                            case Constants.PromotionRuleType.AddOneCoconut:
                            case Constants.PromotionRuleType.DeliveryFreeAddOneCoconut:
                            case Constants.PromotionRuleType.FirstDayAddOneCocunut:
                            case Constants.PromotionRuleType.AddOneLemonade:
                            case Constants.PromotionRuleType.DeliveryFreeAddOneLemonade:
                            case Constants.PromotionRuleType.FirstDayAddOneLemonade:
                            case Constants.PromotionRuleType.FirstDayAddOneMoonbear:
                                PreferentialPrices = TotalPrices;
                                PreferentialLogisticsFees = DeliverFees;
                                iRuleId = promotionRule.RuleId;
                                break;
                            default:
                                break;
                        }

                    }
                }
            }


            #endregion

            #region <<返回Json>>
            return Json
            (new
            {
                IsSuccess = true,
                TotalPrice = TotalPrices,
                PreferentialPrice = PreferentialPrices,
                DeliverFee = DeliverFees,
                PreferentialLogisticsFee = PreferentialLogisticsFees,
                Total = PreferentialPrices + PreferentialLogisticsFees,
                StatusCodes = StatusCode,
                RuleId = iRuleId
            });
            #endregion
        }
        #endregion

        /// <summary>
        ///  验证这个优惠券是否在有效期内
        /// </summary>
        private int ValidatePromotionRule(PromotionRuleInfo promotionRule)
        {
            DateTime now = DateTime.Now;
            if (promotionRule == null)
            {
                return Constants.Promotion.NotExists;
            }
            if (promotionRule.ActiveFrom > now)
            {
                return Constants.Promotion.NotStart;
            }
            if (promotionRule.ActiveTo < now)
            {
                return Constants.Promotion.ExpiredCode;
            }
            if (!promotionRule.IsActive)
            {
                return Constants.Promotion.HasUsed;
            }
            return 0;
        }
    }
}