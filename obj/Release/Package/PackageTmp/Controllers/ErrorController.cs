﻿using System;
using System.Web.Mvc;
using VCleanse.Shop.Host.MVCFilter;

namespace VCleanse.Shop.Host.Controllers
{
    [OutputCacheFilter(ViewCacheControl = ViewCacheControlType.NoCache)]
    public class ErrorController : Controller
    {
        [ActionName("general")]
        public ActionResult General(Exception exception)
        {
            return Content("General failure", "text/plain");
        }

        [ActionName("404")]
        public ActionResult Http404()
        {
            return Content("Page Not found", "text/plain");
        }

        [ActionName("403")]
        public ActionResult Http403()
        {
            return Content("Forbidden", "text/plain");
        }

        [ActionName("401")]
        public ActionResult Http401()
        {
            return Content("Unauthorized to this page", "text/plain");
        }

        [ActionName("500")]
        public ActionResult Http500()
        {
            return Content("Server error occured", "text/plain");
        }
    }
}
