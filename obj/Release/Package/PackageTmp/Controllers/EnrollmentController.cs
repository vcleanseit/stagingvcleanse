﻿using System;
using System.Web.Mvc;
using VCleanse.Shop.Biz.Shared;
using VCleanse.Shop.Common;
using VCleanse.Shop.Model;
using VCleanse.Shop.Host.MVCFilter;
using VCleanse.Shop.Biz;
using Vcleanse.AdminService.SMS;
using VCleanse.Shop.Host.Utility;

namespace VCleanse.Shop.Host.Controllers
{
    public class EnrollmentController : BaseController
    {

        public ActionResult Register()
        {
            return View("Register");
        }

        public ActionResult Signin()
        {
            return View("Signin");
        }

        public ActionResult ForgotPassword()
        {
            return View("Forgotpassword");
        }

        [MemberAuthorize]
        public ActionResult Success()
        {
            return View("Success");
        }
        /// <summary>
        /// 验证码是否正确
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private bool CheckVerifyCode(string code)
        {
            if (Session[WebConstants.Context.ImageValidationCode] == null)
            {
                return false;
            }
            string validationImageCode = Session[WebConstants.Context.ImageValidationCode].ToString();
            if (validationImageCode.Equals(code, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="language"></param>
        /// <param name="verifyCode"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendValidationCode(string mobile, string language, string verifyCode)
        {
            if (!CheckVerifyCode(verifyCode))
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCodeNum, "Validation code error");
            if (string.IsNullOrWhiteSpace(mobile) || !ValidationUtility.IsCellPhone(mobile))
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.CellPhoneNotExist, "Invalid cellphone");
            }
            else
            {
                VUsersBLL bll = new VUsersBLL();
                if (!bll.IsPhoneExisted(mobile))
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidCellPhone, "Cell phone not exist");
                }
                string key = GetCellPhoneValidationSessionKey(mobile);
                ValidationCodeInfo cellPhone = Session[key] as ValidationCodeInfo;
                if (cellPhone != null && cellPhone.LastSend.AddSeconds(Config.CellPhoneValidationPeriodSeconds) > DateTime.UtcNow)
                {
                    return CreateFailedPostJsonResult(Constants.RegistStatus.ToFrequent, "To frequent");
                }
                if (cellPhone == null || cellPhone.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
                {
                    cellPhone = new ValidationCodeInfo()
                    {
                        Phone = mobile,
                        Code = RandomHelper.FixLengthNumber(Config.PhoneValidationCodeLength),
                    };
                }
                cellPhone.LastSend = DateTime.UtcNow;
                Session[key] = cellPhone;
                SMSService.Instance.SendSMS(mobile, string.Format(LanguageHelper.IsChinese(language) ? 
                    "您的维果清验证码是{0}" : "Your VCLEANSE verification code is {0}", cellPhone.Code));
            }
            return CreateSucessedPostJsonResult();
        }
        /// <summary>
        ///  获取Session名称
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        private string GetCellPhoneValidationSessionKey(string phone)
        {
            return string.Format("CellValidation_{0}", phone);
        }
        /// <summary>
        /// 获取session值
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        private ValidationCodeInfo GetValidationSession(string phone)
        {
            ValidationCodeInfo cellphone = Session[GetCellPhoneValidationSessionKey(phone)] as ValidationCodeInfo;
            return cellphone;
        }

        [Serializable]
        private class ValidationCodeInfo
        {
            public string Phone { get; set; }

            public string Code { get; set; }

            public DateTime LastSend { get; set; }
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="validationCode"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetPassword(string mobile, string validationCode, string language)
        {
            NLogHelper.LogInfo("ResetPassword", mobile);
            
            ValidationCodeInfo validateCodeInfo = GetValidationSession(mobile);
            if (validateCodeInfo == null
                || !string.Equals(validateCodeInfo.Code, validationCode, StringComparison.OrdinalIgnoreCase)
                || validateCodeInfo.LastSend.AddMinutes(Config.PhoneValidationExpireMinutes) < DateTime.UtcNow)
            {
                return CreateFailedPostJsonResult(Constants.RegistStatus.InvalidValidationCode, "Validation code error");
            }
            string newPassword = RandomHelper.FixLengthString(6);
            VUsersBLL.UpdatePassword(mobile, newPassword);
            SMSService.Instance.SendSMS(mobile, string.Format(LanguageHelper.IsChinese(language) ?  
                "您的维果清密码是:{0}" : "Your VCLEANSE pwd has been reset to:{0}", newPassword));
            return Json(new { IsSuccess = true });
        }


    }
}
