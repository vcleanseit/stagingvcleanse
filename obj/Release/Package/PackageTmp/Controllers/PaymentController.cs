﻿using VCleanse.Shop.Biz.Payment;
using VCleanse.Shop.Model;
using VCleanse.Shop.Model.Payment;
using VCleanse.Shop.Model.Orders;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Mvc;
using VCleanse.Shop.Host.Utility;
using VCleanse.Shop.Biz;
using System.Linq;
using System.Web.Script.Serialization;
using VCleanse.Shop.Host.Models;
using VCleanse.Shop.Model.cfc;
using VCleanse.Shop.Biz._Legacy.cfc;

using VCleanse.Shop.Host.PaymentService.AlipayWap;
using VCleanse.Shop.Host.PaymentService;
using VCleanse.Shop.Host.PaymentService.Ucfpay;

using UCFPAY = VCleanse.Shop.Host.PaymentService.Ucfpay.com;
using ALIPAY = VCleanse.Shop.Host.PaymentService.Alipay.com;
using ALIWAPPAY = VCleanse.Shop.Host.PaymentService.AlipayWap.com;
using UNIONPAY = VCleanse.Shop.Host.PaymentService.Unionpay.com;

using System.Xml;

namespace VCleanse.Shop.Host.Controllers
{
    public class PaymentController : BasePaymentController
    {

        public ActionResult ToPay()
        {
            return View("ToPay");
        }

        /// <summary>
        /// 支付请求service
        /// </summary>
        /// <param name="orderId">订单号</param>
        /// <param name="paymentPlatform">支付平台 int</param>
        /// <param name="bank">支付宝银行参数string</param>
        /// <param name="qr">支付宝二维码类型ID  -1无 0简单前置 1前置模式 2跳转模式 3迷你</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Pay(int orderId, int paymentPlatform = 1, string bank = "", int qr = -1)
        {
            NLogHelper.LogInfo(orderId.ToString(), "支付<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<- 开始>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            NLogHelper.LogInfo(orderId.ToString(), string.Format("支付------------参数orderId=({0})plat=({1})bank=({2})qr=({3}) ------------------------------context=({4})--------------------------------------------------"
                , orderId, paymentPlatform, bank, qr, PaymentHelper.getLogContext()));
            string sHtmlText = string.Empty;
            var orderInfo = PaymentHelper.getOrderInfo(orderId);
            string result = "OK";
            int resultId = 0;
            if (orderInfo != null)
            {
                if (PaymentHelper.CheckOrderCanPayStatus(orderInfo))
                {
                    decimal fee = PaymentHelper.getPaymentPrice(orderInfo);

                    if (fee > 0)
                    {
                        string total_fee = fee.ToString();
                        IPayment payment = new PaymentFactory().GetPayment(paymentPlatform, orderId, total_fee, bank, qr, HttpHelper.GetIp(HttpContext));
                        sHtmlText = payment.PayHtml();
                    }
                    else
                    {
                        result = "支付价格不对";
                        resultId = PaymentHelper.PayErrorMsg.PRICE;
                    }
                }
                else
                {
                    result = "支付订单已经过期";
                    resultId = PaymentHelper.PayErrorMsg.ESPIRED;
                }
            }
            else
            {
                result = "支付订单不存在";
                resultId = PaymentHelper.PayErrorMsg.NOTEXISTS;
            }

            NLogHelper.LogInfo(orderId.ToString(), string.Format("支付<<<<<<<<<<<<<<<<<<<<<<<<<<- 结束-----订单检查结果=({0})>>>>>>>>>>>>>>>-", result));
            if (string.IsNullOrEmpty(sHtmlText))
            {
                if (qr.Equals(PaymentHelper.AliQrType.SIMPREPOSE))
                {
                    string url = Context.VcleanseContext.Current.Domain + PaymentHelper.PayFailUrl(orderId.ToString(), resultId.ToString());
                    return Content(PaymentHelper.getUrl(url));
                }
                return Redirect(PaymentHelper.PayFailUrl(orderId.ToString(), resultId.ToString()));
            }
            ViewBag.PayHtml = sHtmlText;
            return View("Pay");
        }

        /// <summary>
        /// 1.A 支付宝 普通 异步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult PayNotify()
        {
            string content = this.AliPayNotify();
            return Content(content);
        }

        /// <summary>
        /// 1.B(1) 支付宝 普通 同步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult Success()
        {
            int orderId = 0;
            int errorId = 0;
            string content = this.AliPayReturn(out orderId, out errorId);

            if (content.Equals(PaymentHelper.AliPayReturnTpye.SUCCESS, StringComparison.OrdinalIgnoreCase))
            {
                return Redirect(Context.VcleanseContext.Current.Domain + PaymentHelper.PaySuccessUrl(orderId.ToString()));
            }
            else
            {
                return Redirect(Context.VcleanseContext.Current.Domain + PaymentHelper.PayFailUrl(orderId.ToString(), errorId.ToString()));
            }

        }

        /// <summary>
        /// 1.B(2) 支付宝 普通 二维码同步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult QrReturn()
        {
            int orderId = 0;
            int errorId = 0;
            string content = this.AliPayReturn(out orderId, out errorId, true);
            string url;
            if (content.Equals(PaymentHelper.AliPayReturnTpye.SUCCESS, StringComparison.OrdinalIgnoreCase))
            {
                url = Context.VcleanseContext.Current.Domain + PaymentHelper.PaySuccessUrl(orderId.ToString());
            }
            else
            {
                url = Context.VcleanseContext.Current.Domain + PaymentHelper.PayFailUrl(orderId.ToString(), errorId.ToString());
            }
            //parent form 
            return Content(PaymentHelper.getUrl(url));
        }

        /// <summary>
        /// 2.A 支付宝 网页 异步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult PayWapNotify()
        {
            string content = this.AliWapPayNotify();
            return Content(content);
        }


        /// <summary>
        /// 2.B 支付宝 网页 同步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult PayWapReturn()
        {
            int orderId = 0;
            int errorId = 0;
            string content = this.AliWapPayReturn(out orderId, out errorId);
            if (content.Equals(PaymentHelper.AliPayReturnTpye.SUCCESS, StringComparison.OrdinalIgnoreCase))
            {
                return Redirect(Context.VcleanseContext.Current.Domain + "/m" + PaymentHelper.PaySuccessUrl(orderId.ToString()));
            }
            else
            {
                return Redirect(Context.VcleanseContext.Current.Domain + "/m" + PaymentHelper.PayFailUrl(orderId.ToString(), errorId.ToString()));
            }
        }

        #region "先锋支付 service"
        /// <summary>
        /// 5.A 先锋支付 同步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult UcfPayNotify()
        {
            string returnOrderId = string.Empty;
            UCFPAY.UcfPayItem ucfPayItem = getUcfPayResquestItem();

            if (ucfPayItem == null)
            {
                return Redirect(PaymentHelper.PayFailUrl(returnOrderId));
            }
            string result = UcfPayCallBack(ref returnOrderId, ucfPayItem, true);

            if (result.Equals(PaymentHelper.AliPayReturnTpye.SUCCESS, StringComparison.OrdinalIgnoreCase))
            {
                return Redirect(PaymentHelper.PaySuccessUrl(returnOrderId));
            }
            else
            {
                return Redirect(PaymentHelper.PayFailUrl(returnOrderId));
            }
        }

        /// <summary>
        /// 5.B 先锋支付 异步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult UcfPayNotifyAsync()
        {
            string returnOrderId = string.Empty;
            UCFPAY.UcfPayItem ucfPayItem = getUcfPayResquestItem();
            if (ucfPayItem == null)
            {
                return Content(PaymentHelper.AliPayReturnTpye.FAIL);
            }
            string result = UcfPayCallBack(ref returnOrderId, ucfPayItem, false);
            return Content(result);
        }

        /// <summary>
        /// 先锋支付订单号加密
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult securityOrder(string id)
        {
            NLogHelper.LogInfo(id, "fcOrder");

            string result = PaymentHelper.ParseOrderNumber(id).ToString();
            string nResult = DateTime.Now.ToFileTimeUtc().ToString();
            foreach (char item in result)
            {
                nResult += item;
            }

            return Content(nResult);
        }




        #endregion

        #region "银联在线"
        /// <summary>
        /// 6.A 银联在线 同步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult UnionPayNotify()
        {
            return null;
        }
        /// <summary>
        /// 6.B 银联在线 异步回调
        /// </summary>
        /// <returns></returns>
        public ActionResult UnionPayNotifyAsync()
        {
            return null;
        }
        public ActionResult UnionOrder(string Id)
        {
            return null;
        }
        #endregion

        /// <summary>
        /// 支付成功 UI
        /// </summary>
        /// <returns></returns>
        public ActionResult PaySuccess()
        {
            NLogHelper.LogInfo("Action", "PaySuccess");

            return View("PaySuccess");
        }
        /// <summary>
        /// 支付失败 UI
        /// </summary>
        /// <returns></returns>
        public ActionResult PayFailed()
        {
            NLogHelper.LogInfo("Action", "PayFailed");

            return View("PayFailed");
        }


    }
}
